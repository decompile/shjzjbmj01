(function(r, n) {
"object" == typeof exports ? module.exports = exports = n() : "function" == typeof define && define.amd ? define(n) : window.BASE64 = n();
})(0, function() {
var r = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "/" ], n = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-", "_" ], e = function(r) {
for (var n = []; r > 0; ) {
var e = r % 2;
r = Math.floor(r / 2);
n.push(e);
}
n.reverse();
return n;
}, t = function(r) {
for (var n = 0, e = 0, t = r.length - 1; t >= 0; --t) {
1 == r[t] && (n += Math.pow(2, e));
++e;
}
return n;
}, o = function(r, n) {
for (var e = 8 - (r + 1) + 6 * (r - 1) - n.length; --e >= 0; ) n.unshift(0);
for (var t = [], o = r; --o >= 0; ) t.push(1);
t.push(0);
for (var f = 0, c = 8 - (r + 1); f < c; ++f) t.push(n[f]);
for (var a = 0; a < r - 1; ++a) {
t.push(1);
t.push(0);
for (var u = 6; --u >= 0; ) t.push(n[f++]);
}
return t;
}, f = function(r) {
for (var n = [], t = 0, f = r.length; t < f; ++t) {
var c = r.charCodeAt(t), a = e(c);
if (c < 128) {
for (var u = 8 - a.length; --u >= 0; ) a.unshift(0);
n = n.concat(a);
} else c >= 128 && c <= 2047 ? n = n.concat(o(2, a)) : c >= 2048 && c <= 65535 ? n = n.concat(o(3, a)) : c >= 65536 && c <= 2097151 ? n = n.concat(o(4, a)) : c >= 2097152 && c <= 67108863 ? n = n.concat(o(5, a)) : c >= 4e6 && c <= 2147483647 && (n = n.concat(o(6, a)));
}
return n;
}, c = function(r) {
for (var n, e = [], o = "", f = 0, c = r.length; f < c; ) if (0 == r[f]) {
n = t(r.slice(f, f + 8));
o += String.fromCharCode(n);
f += 8;
} else {
for (var a = 0; f < c && 1 == r[f]; ) {
++a;
++f;
}
e = e.concat(r.slice(f + 1, f + 8 - a));
f += 8 - a;
for (;a > 1; ) {
e = e.concat(r.slice(f + 2, f + 8));
f += 8;
--a;
}
n = t(e);
o += String.fromCharCode(n);
e = [];
}
return o;
}, a = function(e, o) {
for (var c = [], a = f(e), u = o ? n : r, i = 0, h = 0, s = a.length; h < s; h += 6) {
var l = h + 6 - s;
2 == l ? i = 2 : 4 == l && (i = 4);
for (var v = i; --v >= 0; ) a.push(0);
c.push(t(a.slice(h, h + 6)));
}
var g = "";
for (h = 0, s = c.length; h < s; ++h) g += u[c[h]];
for (h = 0, s = i / 2; h < s; ++h) g += "=";
return g;
}, u = function(t, o) {
var f = t.length, a = 0, u = o ? n : r;
if ("=" == t.charAt(f - 1)) if ("=" == t.charAt(f - 2)) {
a = 4;
t = t.substring(0, f - 2);
} else {
a = 2;
t = t.substring(0, f - 1);
}
for (var i = [], h = 0, s = t.length; h < s; ++h) for (var l = t.charAt(h), v = 0, g = u.length; v < g; ++v) if (l == u[v]) {
var d = e(v), p = d.length;
if (6 - p > 0) for (var A = 6 - p; A > 0; --A) d.unshift(0);
i = i.concat(d);
break;
}
a > 0 && (i = i.slice(0, i.length - a));
return c(i);
};
return {
encode: function(r) {
return a(r, !1);
},
decode: function(r) {
return u(r, !1);
},
urlsafe_encode: function(r) {
return a(r, !0);
},
urlsafe_decode: function(r) {
return u(r, !0);
}
};
});