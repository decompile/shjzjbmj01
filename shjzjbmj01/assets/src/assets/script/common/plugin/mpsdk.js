var mpsdk, __awaiter = this && this.__awaiter || function(e, t, n, i) {
return new (n || (n = Promise))(function(o, r) {
function a(e) {
try {
s(i.next(e));
} catch (e) {
r(e);
}
}
function c(e) {
try {
s(i.throw(e));
} catch (e) {
r(e);
}
}
function s(e) {
e.done ? o(e.value) : new n(function(t) {
t(e.value);
}).then(a, c);
}
s((i = i.apply(e, t || [])).next());
});
}, __generator = this && this.__generator || function(e, t) {
var n, i, o, r, a = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return r = {
next: c(0),
throw: c(1),
return: c(2)
}, "function" == typeof Symbol && (r[Symbol.iterator] = function() {
return this;
}), r;
function c(e) {
return function(t) {
return s([ e, t ]);
};
}
function s(r) {
if (n) throw new TypeError("Generator is already executing.");
for (;a; ) try {
if (n = 1, i && (o = 2 & r[0] ? i.return : r[0] ? i.throw || ((o = i.return) && o.call(i), 
0) : i.next) && !(o = o.call(i, r[1])).done) return o;
(i = 0, o) && (r = [ 2 & r[0], o.value ]);
switch (r[0]) {
case 0:
case 1:
o = r;
break;

case 4:
a.label++;
return {
value: r[1],
done: !1
};

case 5:
a.label++;
i = r[1];
r = [ 0 ];
continue;

case 7:
r = a.ops.pop();
a.trys.pop();
continue;

default:
if (!(o = a.trys, o = o.length > 0 && o[o.length - 1]) && (6 === r[0] || 2 === r[0])) {
a = 0;
continue;
}
if (3 === r[0] && (!o || r[1] > o[0] && r[1] < o[3])) {
a.label = r[1];
break;
}
if (6 === r[0] && a.label < o[1]) {
a.label = o[1];
o = r;
break;
}
if (o && a.label < o[2]) {
a.label = o[2];
a.ops.push(r);
break;
}
o[2] && a.ops.pop();
a.trys.pop();
continue;
}
r = t.call(e, a);
} catch (e) {
r = [ 6, e ];
i = 0;
} finally {
n = o = 0;
}
if (5 & r[0]) throw r[1];
return {
value: r[0] ? r[1] : void 0,
done: !0
};
}
}, __extends = this && this.__extends || function() {
var e = function(t, n) {
return (e = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
})(t, n);
};
return function(t, n) {
e(t, n);
function i() {
this.constructor = t;
}
t.prototype = null === n ? Object.create(n) : (i.prototype = n.prototype, new i());
};
}(), __assign = this && this.__assign || function() {
return (__assign = Object.assign || function(e) {
for (var t, n = 1, i = arguments.length; n < i; n++) {
t = arguments[n];
for (var o in t) Object.prototype.hasOwnProperty.call(t, o) && (e[o] = t[o]);
}
return e;
}).apply(this, arguments);
};

(function(e) {
var t = function() {
function e() {}
e.version = "3.4.6";
e.build = "202132219331";
e.storagePrefix = "mpsdk_";
e.loginServer = "https://xyxcck-auth.raink.com.cn";
e.adTagServer = "https://xyx-mainland-videotag.raink.com.cn";
e.userValueServer = "https://xyx-mainland-userroi.raink.com.cn";
e.newLoginServer = "https://xyx-app-login.raink.com.cn";
e.reportServer1 = "https://xyxcck-log.raink.com.cn";
e.reportServer2 = "https://xyxcck-log-ad.raink.com.cn";
e.publicServer = "https://xyx-public.raink.com.cn";
e.friendServer = "https://xyx-app-friend.raink.com.cn";
e.cdnServer = "https://cdn-xyx.raink.com.cn";
e.extServer = "https://xyx-pdd-api.raink.com.cn";
e.boxServer = "https://xyx-mainland-api.raink.com.cn";
e.trackAdServer = "https://mainland-xyxzsdl-wx.raink.com.cn";
e.photoServer = "https://photo-xyx.raink.com.cn";
e.qqGdtServer = "https://xyx-app-sq.raink.com.cn";
e.videoReportServer = "https://xyx-mainland-videoreport.raink.com.cn";
e.landingPageServer = "https://xyx-app-ksad.raink.com.cn/test/index";
e.userGYServer = "http://xyx-usera.raink.com.cn";
e.apiAdServer = "https://xyx-app-ad.raink.com.cn/user/check";
e.openStatusServer = "https://xyx-gameswitch.raink.com.cn";
e.showLog = !0;
e.init = !1;
e.isRewardedVideoAd2Back = !1;
e.isClickRewardedVideoAdDialogOk = !1;
e.isRewardedVideoAdDialogShow = !1;
e.phoneModel = "";
e.mpsdkChannel = "mpsdk";
e.mpsdkImei = "";
e.mpsdkAndroidId = "";
e.appLaunchOptions = "";
e.mpsdkOaid = "";
e.mpsdkMac = "";
e.onlineTimeTotal = 0;
e.onlineTimeCount = 0;
e.onlineTimeTotalLastReportDate = "";
e.shareUrl = "";
e.wxpublicUrl = "";
return e;
}();
e.Env = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.parseAccountSource = function(t) {
var n = e.Env.mpsdkChannel;
return "share" == t.query.type ? {
sourceType: n,
sourceId: t.query.shareid
} : "wxad" == t.query.type ? {
sourceType: n,
sourceId: t.query.adid + "." + t.query.weixinadinfo
} : "link" == t.query.type ? {
sourceType: n,
sourceId: t.query.type + "_" + t.query.sgid + "_" + t.query.adid
} : "ad" == t.query.type ? {
sourceType: n,
sourceId: t.query.adid
} : {
sourceType: n,
sourceId: n
};
};
t.value = function(e, t, n) {
void 0 === n && (n = void 0);
if ("undefined" == typeof e) return n;
for (var i = 0, o = t.split("."); i < o.length; i++) {
var r = o[i];
if ("undefined" == typeof e[r]) return n;
e = e[r];
}
return e;
};
t.getQueryString = function(e, t) {
var n = new RegExp("(\\?|&)" + t + "=([^&]*)(&|$)"), i = e.match(n);
return null != i ? i[2] : "";
};
t.httpBuildQuery = function(e) {
var t = "";
for (var n in e) t += n + "=" + e[n] + "&";
return t.substr(0, t.length - 1);
};
t.httpParseQuery = function(e) {
var t = {};
if (!e) return t;
var n = e.toString().split("?")[1] || e.toString();
if (-1 == n.indexOf("=")) return t;
for (var i = 0, o = n.split("&"); i < o.length; i++) {
var r = o[i].split("=");
t[r[0]] = r[1];
}
return t;
};
t.waitData = function(t, n, i, o) {
void 0 === i && (i = 200);
void 0 === o && (o = 3e4);
for (var r = [], a = 4; a < arguments.length; a++) r[a - 4] = arguments[a];
return new Promise(function(a, c) {
var s, d;
s = e.Platform.instance.setInterval(function() {
var i = t.call.apply(t, [ n ].concat(r));
if (i) {
e.Platform.instance.clearInterval(s);
e.Platform.instance.clearTimeout(d);
a(i);
}
}, i);
d = e.Platform.instance.setTimeout(function() {
e.Platform.instance.clearInterval(s);
}, o);
});
};
t.tableAlgorithm = function(e, t, n, i, o) {
void 0 === n && (n = !0);
void 0 === i && (i = Number.MAX_VALUE);
void 0 === o && (o = 0);
if (!e || !e.length) return [];
for (var r = [], a = [], c = 0, s = 0; s < e.length; s++) if (!isNaN(e[s][t])) {
e[s][t] = Number(e[s][t]);
if (!(e[s][t] <= o)) if (e[s][t] >= i) r.push(e[s]); else {
a.push(e[s]);
c += e[s][t];
}
}
if (!c || !a.length) return r;
for (var d = []; a.length; ) {
var u = Math.random(), l = 0;
for (s = 0; s < a.length; s++) {
var p = l + a[s][t] / c;
if (u >= l && u < p) {
d.push(a[s]);
c -= a[s][t];
a.splice(s, 1);
break;
}
l = p;
}
}
var f = r.concat(d);
return n ? f[0] : f;
};
t.string2Buffer = function(e) {
for (var t = new ArrayBuffer(e.length), n = new Uint8Array(t), i = 0; i < e.length; i++) n[i] = e.charCodeAt(i);
return t;
};
t.buffer2String = function(e) {
for (var t = new Array(e.byteLength), n = new Uint8Array(e), i = 0; i < e.byteLength; i++) t[i] = String.fromCharCode(n[i]);
return t.join("");
};
t.deepCompare = function(e, t) {
var n, i;
function o(e, t) {
if (isNaN(e) && isNaN(t) && "number" == typeof e && "number" == typeof t) return !0;
if (e === t) return !0;
if ("function" == typeof e && "function" == typeof t || e instanceof Date && t instanceof Date || e instanceof RegExp && t instanceof RegExp || e instanceof String && t instanceof String || e instanceof Number && t instanceof Number) return e.toString() === t.toString();
if (!(e instanceof Object && t instanceof Object)) return !1;
if (e.isPrototypeOf(t) || t.isPrototypeOf(e)) return !1;
if (e.constructor !== t.constructor) return !1;
if (e.prototype !== t.prototype) return !1;
if (n.indexOf(e) > -1 || i.indexOf(t) > -1) return !1;
for (var r in t) {
if (t.hasOwnProperty(r) !== e.hasOwnProperty(r)) return !1;
if (typeof t[r] != typeof e[r]) return !1;
}
for (var r in e) {
if (t.hasOwnProperty(r) !== e.hasOwnProperty(r)) return !1;
if (typeof t[r] != typeof e[r]) return !1;
switch (typeof e[r]) {
case "object":
case "function":
n.push(e);
i.push(t);
if (!o(e[r], t[r])) return !1;
n.pop();
i.pop();
break;

default:
if (e[r] !== t[r]) return !1;
}
}
return !0;
}
if (arguments.length < 1) return !0;
for (var r = 1, a = arguments.length; r < a; r++) {
n = [];
i = [];
if (!o(arguments[0], arguments[r])) return !1;
}
return !0;
};
t.drawRoundRect = function(e, t, n, i, o, r, a) {
e.beginPath();
e.strokeStyle = "#FFF";
e.lineWidth = a;
e.arc(t + r, n + r, r, Math.PI, 1.5 * Math.PI);
e.moveTo(t + r, n);
e.lineTo(t + i - r, n);
e.arc(t + i - r, n + r, r, 1.5 * Math.PI, 2 * Math.PI);
e.lineTo(t + i, n + o - r);
e.arc(t + i - r, n + o - r, r, 0, .5 * Math.PI);
e.lineTo(t + r, n + o);
e.arc(t + r, n + o - r, r, .5 * Math.PI, Math.PI);
e.lineTo(t, n + r);
e.stroke();
e.closePath();
};
t.generateUUID = function() {
var e = new Date().getTime();
return "xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx".replace(/[xy]/g, function(t) {
var n = (e + 16 * Math.random()) % 16 | 0;
e = Math.floor(e / 16);
return ("x" == t ? n : 3 & n | 8).toString(16);
});
};
t.createClientUUID = function() {
return new Date().getTime() + "" + (1e7 * Math.random()).toString(10).substr(0, 6);
};
t.setFirstLaunchOptions = function(t) {
if (t && t.scene) {
var n = e.Platform.instance.getStorage("mpsdk_first_into_launch_options");
n && n.scene || e.Platform.instance.setStorage("mpsdk_first_into_launch_options", t);
}
};
t.getFirstLaunchOptions = function() {
return e.Platform.instance.getStorage("mpsdk_first_into_launch_options");
};
t.checkAbroadCity = function(e) {
var n = t.value(e, "ipArea.province"), i = t.value(e, "ipArea.city"), o = t.value(e, "ipArea.country");
return o && -1 == o.indexOf("中国") || n && -1 != n.indexOf("台湾") || i && -1 != i.indexOf("台湾") || n && -1 != n.indexOf("香港") || i && -1 != i.indexOf("香港") || n && -1 != n.indexOf("澳门") || i && -1 != i.indexOf("澳门");
};
t.nativeAppLogin = function(n) {
var i = e.Account.getOpenId();
e.log("服务端登录开始openid", i);
var o = new Date().getTime(), r = this.httpParseQuery(e.Env.appLaunchOptions);
e.Env.mpsdkChannel || (e.Env.mpsdkChannel = "mpsdk");
e.Env.launchOptions = {
scene: e.Env.mpsdkChannel,
query: r
};
e.log("app launchOptions:", JSON.stringify(e.Env.launchOptions));
var a = t.parseAccountSource(e.Env.launchOptions);
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_PT_LOGIN_BEGIN);
var c = e.Env.phoneModel ? e.Env.phoneModel : e.Platform.instance.getSystem().platform, s = e.Env.mpsdkImei ? e.Env.mpsdkImei : "", d = e.Env.mpsdkAndroidId ? e.Env.mpsdkAndroidId : "", u = e.Env.mpsdkOaid ? e.Env.mpsdkOaid : "", l = e.Env.mpsdkMac ? e.Env.mpsdkMac : "", p = "";
e.Env.launchOptions.query && e.Env.launchOptions.query.userid && (p = e.Env.launchOptions.query.userid);
return new Promise(function(t, n) {
var r = {
gameId: e.Env.gameId,
unionId: "false",
sourceType: a.sourceType,
sourceId: a.sourceId,
model: encodeURIComponent(c),
openId: i,
imei: s,
sourceOpenId: p,
androidId: d,
oaId: u,
mac: l
}, f = e.Env.newLoginServer + "/MiniLogin/data/GetOpenIdApp.action";
e.log("nativeAppLogin 参数:", JSON.stringify(r));
e.Platform.instance.httpRequest(f, r, "get").then(function(n) {
e.log("服务端登录成功", JSON.stringify(n));
if (n && n.openid) {
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_PT_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_SUCCESS, "" + o, n.openid);
e.Account.reportDeviceInfo(n);
} else e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_PT_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_FAILS, o + "_服务器登录成功,但未获取到openid,errorcode=" + n.error);
t(n);
}).catch(function(t) {
e.log("服务端登录失败", JSON.stringify(t));
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_PT_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_FAILS, o + "_服务器登录失败：" + JSON.stringify(t));
n(t);
});
});
};
t.getFormatNowDate = function() {
var e = new Date(), t = e.getMonth() + 1, n = e.getDate();
t <= 9 && (t = "0" + t);
n <= 9 && (n = "0" + n);
return e.getFullYear() + "-" + t + "-" + n;
};
t.checkAndResetOnlineTimeTotal = function(t) {
t != this.getFormatNowDate() && (e.Env.onlineTimeTotal = 0);
};
t.base64encode = function(e) {
var t, n, i, o, r, a, c, s = "", d = 0;
e = this._utf8_encode(e);
for (;d < e.length; ) {
t = e.charCodeAt(d++);
n = e.charCodeAt(d++);
i = e.charCodeAt(d++);
o = t >> 2;
r = (3 & t) << 4 | n >> 4;
a = (15 & n) << 2 | i >> 6;
c = 63 & i;
isNaN(n) ? a = c = 64 : isNaN(i) && (c = 64);
s = s + this._keyStr.charAt(o) + this._keyStr.charAt(r) + this._keyStr.charAt(a) + this._keyStr.charAt(c);
}
return s;
};
t.base64decode = function(e) {
var t, n, i, o, r, a, c, s = "", d = 0;
e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
for (;d < e.length; ) {
o = this._keyStr.indexOf(e.charAt(d++));
r = this._keyStr.indexOf(e.charAt(d++));
a = this._keyStr.indexOf(e.charAt(d++));
c = this._keyStr.indexOf(e.charAt(d++));
t = o << 2 | r >> 4;
n = (15 & r) << 4 | a >> 2;
i = (3 & a) << 6 | c;
s += String.fromCharCode(t);
64 !== a && (s += String.fromCharCode(n));
64 !== c && (s += String.fromCharCode(i));
}
return s = this._utf8_decode(s);
};
t._utf8_encode = function(e) {
e = e.replace(/\r\n/g, "\n");
for (var t = "", n = 0; n < e.length; n++) {
var i = e.charCodeAt(n);
if (i < 128) t += String.fromCharCode(i); else if (i > 127 && i < 2048) {
t += String.fromCharCode(i >> 6 | 192);
t += String.fromCharCode(63 & i | 128);
} else {
t += String.fromCharCode(i >> 12 | 224);
t += String.fromCharCode(i >> 6 & 63 | 128);
t += String.fromCharCode(63 & i | 128);
}
}
return t;
};
t._utf8_decode = function(e) {
for (var t = "", n = 0, i = 0, o = 0, r = 0; n < e.length; ) if ((i = e.charCodeAt(n)) < 128) {
t += String.fromCharCode(i);
n++;
} else if (i > 191 && i < 224) {
o = e.charCodeAt(n + 1);
t += String.fromCharCode((31 & i) << 6 | 63 & o);
n += 2;
} else {
o = e.charCodeAt(n + 1);
r = e.charCodeAt(n + 2);
t += String.fromCharCode((15 & i) << 12 | (63 & o) << 6 | 63 & r);
n += 3;
}
return t;
};
t.checkMultiOpenTimesLimit = function(e) {
if (!e.openTimes) return !0;
for (var t = e.openTimes.split(","), n = 0; n < t.length; n++) if (this.checkOpenTimesLimit(t[n])) return !0;
return !1;
};
t.checkOpenTimesLimit = function(e) {
var t = !1;
if (e) {
var n = e.indexOf("_") > 0 ? e.split("_", 2) : e.split("-", 2), i = n[0], o = n[1], r = new Date().getHours();
parseInt(i || "0") <= r && r <= parseInt(o || "24") && (t = !0);
}
return t;
};
t.dec2bin = function(e) {
if (0 == e) return "0";
for (var t = new n(); e > 0; ) {
t.push(e % 2);
e = Math.floor(e / 2);
}
for (var i = ""; !t.isEmpty(); ) i += t.pop();
return i;
};
t.getAdUserStatus = function(e) {
e || (e = 0);
var t = this.dec2bin(e);
return "0" == t ? "-1" : "0" == t.slice(-2, -1) ? "-1" : t.slice(-1);
};
t._keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
return t;
}();
e.utils = t;
var n = function() {
function e() {
this.items = [];
}
e.prototype.push = function(e) {
this.items.push(e);
};
e.prototype.pop = function() {
return this.items.pop();
};
e.prototype.peek = function() {
return this.items[this.items.length - 1];
};
e.prototype.isEmpty = function() {
return 0 == this.items.length;
};
e.prototype.size = function() {
return this.items.length;
};
e.prototype.length = function() {
return this.size();
};
e.prototype.clear = function() {
this.items = [];
};
e.prototype.toString = function() {
return this.items;
};
return e;
}();
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.prototype.getDeviceInfo = function() {
return new Promise(function(e, t) {
e("");
});
};
t.prototype.sendCreateTime2Native = function(e) {};
t.prototype.sendLastLoginTime2Native = function(e) {};
t.prototype.writeFile = function(e, t) {
return "";
};
t.prototype.getStorage = function(t) {
var n = "GameSandBox://" + e.Env.storagePrefix + t;
if (!BK.FileUtil.isFileExist(n)) return "";
try {
var i = BK.FileUtil.readFile(n).readAsString();
return i ? JSON.parse(i) : "";
} catch (e) {
return "";
}
};
t.prototype.setStorage = function(t, n) {
BK.FileUtil.writeFile("GameSandBox://" + e.Env.storagePrefix + t, JSON.stringify(n));
};
t.prototype.httpRequest = function(e, t, n, i) {
void 0 === n && (n = "get");
void 0 === i && (i = 2);
var o = "";
if (t) {
for (var r in t) o += r + "=" + t[r] + "&";
if (o) {
o = o.substr(0, o.length - 1);
if ("get" == n) {
e += -1 == e.indexOf("?") ? "?" : "&";
e += o;
}
}
}
return new Promise(function(t, i) {
var r = function(e, n) {
Number(n);
for (var i = e.bufferLength(), o = e.readAsString(); 0 == o.length && i > 0; ) o += e.readAsString();
o.length > i && (o = o.substring(0, i));
var r = Math.max(o.lastIndexOf("]"), o.lastIndexOf("}")) + 1;
o = o.substring(0, r);
try {
t(JSON.parse(o));
} catch (e) {
t(o);
}
};
if ("get" == n) {
var a = new BK.HttpUtil(e);
a.setHttpMethod("get");
a.requestAsync(r);
} else {
var c = new BK.HttpUtil(e);
c.setHttpMethod("post");
c.setHttpPostData(o);
c.requestAsync(r);
}
});
};
t.prototype.getUserAccount = function(e) {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(e) {
return [ 2, {
openid: GameStatusInfo.openId
} ];
});
});
};
t.prototype.getUserInfo = function() {
var e = {
nickName: "",
avatarUrl: "1",
gender: GameStatusInfo.sex
};
return new Promise(function(t, n) {
BK.MQQ.Account.getNick(GameStatusInfo.openId, function(n, i) {
e.nickName = i;
e.nickName && e.avatarUrl && t(e);
});
});
};
t.prototype.getSystem = function() {
return {
platform: GameStatusInfo.platform
};
};
t.prototype.getLaunchOptions = function() {
return {
scene: GameStatusInfo.src || 0,
query: GameStatusInfo.gameParam ? e.utils.httpParseQuery(GameStatusInfo.gameParam) : {}
};
};
t.prototype.launchTo = function(t) {
return __awaiter(this, void 0, void 0, function() {
var n, i;
return __generator(this, function(o) {
i = t.page.indexOf("?");
n = -1 != i ? t.page.substring(i + 1) : t.page;
BK.QQ.skipGame(Number(t.appid), n);
e.log("打开其他程序，desGameId=", t.appid, "extendInfo=", n);
return [ 2 ];
});
});
};
t.prototype.launchToSync = function(t, n) {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(n) {
e.log("打开其他程序，appId=", t.appid, "path=", t.page);
return [ 2 ];
});
});
};
t.prototype.share = function(t, n, i, o) {
var r = e.Share.genShareInfo(t), a = {};
a.summary = r.title;
a.picUrl = r.image;
r.localPicPath && (a.localPicPath = r.localPicPath);
a.extendInfo = e.Share.getShareQuery(r.serial, r.params || {}, r.imageId);
e.log("分享外链参数 extendInfo =", a.extendInfo);
BK.QQ.share(a, function(a, c, s) {
e.log("分享结果：(retCode, shareDest, isFirstShare)=>", a, c, s);
if (0 == a) {
e.Share.reportShareOut(t.serial, r.imageId.toString(), c.toString(), s.toString());
n && n.call(o, c, s);
} else i && i.call(o, a);
});
return a;
};
t.prototype.getCacheRes = function(e, t) {
void 0 === t && (t = !1);
return Promise.resolve(e);
};
t.prototype.setTimeout = function(e, t) {
var n = new BK.Ticker();
n.interval = t / 1e3 * 60;
n.setTickerCallBack(function(t, i) {
n.paused = !0;
e.call();
n.dispose();
});
return n;
};
t.prototype.setInterval = function(e, t) {
var n = new BK.Ticker();
n.interval = t / 1e3 * 60;
n.setTickerCallBack(function(t, n) {
e.call();
});
return n;
};
t.prototype.clearTimeout = function(e) {
if (e) {
e.paused = !0;
e.dispose();
}
};
t.prototype.clearInterval = function(e) {
if (e) {
e.paused = !0;
e.dispose();
}
};
t.prototype.checkBannerClick = function(e) {
throw new Error("Method not implemented.");
};
t.prototype.onHide = function() {};
t.prototype.showModal = function(e) {};
t.prototype.handleAfterLogin = function(e, t) {};
t.prototype.setSkipParams = function(e) {};
t.prototype.getAppHotLaunchOptions = function() {
return new Promise(function(e, t) {
e("");
});
};
return t;
}();
e.BricksPlatform = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.prototype.writeFile = function(e, t) {
if (!e || !t) return "";
var n = wx.getFileSystemManager(), i = wx.env.USER_DATA_PATH + "/mpsdkadwx", o = i + "/" + e;
try {
n.accessSync(i);
} catch (e) {
n.mkdirSync(i);
}
try {
n.accessSync(o);
return o;
} catch (e) {}
var r = new Array();
try {
r = n.readdirSync(i);
for (var a = e.split("_")[0], c = 0; c < r.length; c++) {
if (0 == r[c].indexOf(a + "_")) {
n.unlinkSync(o);
break;
}
}
} catch (e) {}
try {
n.writeFileSync(o, t, "utf-8");
} catch (t) {
console.log("write file fail. filename:", e, " error:", t);
o = "";
}
return o;
};
t.prototype.getStorage = function(t) {
return wx.getStorageSync(e.Env.storagePrefix + t);
};
t.prototype.setStorage = function(t, n, i) {
void 0 === i && (i = !0);
try {
i ? wx.setStorageSync(e.Env.storagePrefix + t, n) : wx.setStorage({
key: e.Env.storagePrefix + t,
data: n
});
} catch (e) {}
};
t.prototype.httpRequest = function(t, n, i, o) {
void 0 === i && (i = "get");
void 0 === o && (o = 2);
return __awaiter(this, void 0, void 0, function() {
var r, a;
return __generator(this, function(c) {
switch (c.label) {
case 0:
return [ 4, new Promise(function(e, t) {
wx.getNetworkType({
success: function(t) {
e(t);
},
fail: function(e) {
t(e);
}
});
}) ];

case 1:
if ("none" == c.sent().networkType) throw "当前网络不可用";
r = "";
if (n) {
for (a in n) r += a + "=" + n[a] + "&";
if (r) {
r = r.substr(0, r.length - 1);
if ("get" == i) {
t += -1 == t.indexOf("?") ? "?" : "&";
t += r;
}
}
}
return [ 4, new Promise(function(r, a) {
var c = {
url: t,
header: {
"Content-Type": "application/json;charset=UTF-8"
}
};
if ("get" != i) {
c.method = "POST";
c.data = n;
}
var s = 0, d = function(n) {
if (++s <= o) {
wx.request(c);
e.log("HTTP请求重试(" + s + ")", t);
return !0;
}
a(n);
return !1;
};
c.success = function(e) {
e.statusCode && e.statusCode >= 200 && e.statusCode < 400 ? r(e.data) : d(e);
};
c.fail = function(e) {
d(e);
};
wx.request(c);
}) ];

case 2:
return [ 2, c.sent() ];
}
});
});
};
t.prototype.getUserAccount = function(t) {
var n = this;
return new Promise(function(i, o) {
n.getUserAccountOnce(t).then(function(e) {
i(e);
}).catch(function(r) {
n.getUserAccountOnce(t).then(function(e) {
i(e);
}).catch(function(r) {
n.getUserAccountOnce(t).then(function(e) {
i(e);
}).catch(function(t) {
e.log("login retry over.");
o(t);
});
});
});
});
};
t.prototype.getUserAccountOnce = function(t) {
var n = this;
return new Promise(function(i, o) {
var r = new Date().getTime();
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_WX_LOGIN_BEGIN);
wx.login({
success: function(a) {
if (e.Env.isNeedUnionid) wx.getSetting({
success: function(c) {
if (c.authSetting["scope.userInfo"]) wx.getUserInfo({
withCredentials: !0,
success: function(c) {
e.log("微信授权成功,继续服务器端登录 ", a.code);
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_WX_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_SUCCESS, "" + r);
var s = {
code: a.code,
encryptedData: c.encryptedData,
iv: c.iv
};
n.login(s, t).then(function(e) {
i(e);
}).catch(function(e) {
o(e);
});
}
}); else {
e.log("微信未授权,继续服务器端登录 ", a.code);
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_WX_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_SUCCESS, "" + r);
var s = {
code: a.code,
encryptedData: "",
iv: ""
};
n.login(s, t).then(function(e) {
i(e);
}).catch(function(e) {
o(e);
});
}
},
fail: function(c) {
e.log("微信获取授权状态失败,继续服务器端登录 ", a.code, c);
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_WX_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_SUCCESS, "" + r);
var s = {
code: a.code,
encryptedData: "",
iv: ""
};
n.login(s, t).then(function(e) {
i(e);
}).catch(function(e) {
o(e);
});
}
}); else {
e.log("微信登录成功,不需要获取授权信息,继续服务器端登录 ", a.code);
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_WX_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_SUCCESS, "" + r);
var c = {
code: a.code,
encryptedData: "",
iv: ""
};
n.login(c, t).then(function(e) {
i(e);
}).catch(function(e) {
o(e);
});
}
},
fail: function(t) {
e.log("微信登录失败: ", t);
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_WX_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_FAILS, "微信登录失败:" + t);
o(t);
}
});
});
};
t.prototype.login = function(t, n) {
var i = this, o = new Date().getTime(), r = e.utils.parseAccountSource(n);
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_PT_LOGIN_BEGIN);
return new Promise(function(n, a) {
var c = {
gameId: e.Env.gameId,
code: t.code,
encryptedData: encodeURIComponent(t.encryptedData),
iv: encodeURIComponent(t.iv),
unionId: t.encryptedData && t.iv ? "true" : "false",
sourceType: r.sourceType,
sourceId: r.sourceId,
model: encodeURIComponent(wx.getSystemInfoSync().model)
}, s = e.Env.newLoginServer + "/MiniLogin/data/getOpenId2.action";
i.httpRequest(s, c, "get", 0).then(function(t) {
e.log("服务端登录成功", t);
t && t.openid ? e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_PT_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_SUCCESS, "" + o, t.openid) : e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_PT_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_FAILS, o + "_服务器登录成功,但未获取到openid,errorcode=" + t.error);
n(t);
}).catch(function(t) {
e.log("服务端登录失败", t);
e.Report.reportNewUserLog(e.constant.NewUserLogEnum.ACTION_PT_LOGIN_END, e.constant.NewUserLogStatusEnum.STATUS_REPORT_FAILS, o + "_服务器登录失败：" + JSON.stringify(t));
a(t);
});
});
};
t.prototype.getUserInfo = function() {
return new Promise(function(e, t) {
wx.getSetting({
fail: function(e) {
t();
},
success: function(n) {
n.authSetting["scope.userInfo"] ? wx.getUserInfo({
fail: function(e) {
t();
},
success: function(t) {
e(t.userInfo);
}
}) : t();
}
});
});
};
t.prototype.getSystem = function() {
var e = wx.getSystemInfoSync();
return {
platform: e.platform,
model: e.model
};
};
t.prototype.getLaunchOptions = function() {
if (wx.getLaunchOptionsSync) return wx.getLaunchOptionsSync();
throw new Error("小程序启动参数请从app.js的onLaunch回调函数中获取");
};
t.prototype.launchTo = function(t) {
return new Promise(function(n, i) {
if (!wx.redirectTo && wx.getSystemInfoSync().SDKVersion < "2.2.0") {
wx.previewImage({
urls: [ t.ad_image ]
});
n();
} else {
wx.showLoading({
title: "请稍候...",
mask: !0
});
setTimeout(wx.hideLoading, 5e3);
wx.navigateToMiniProgram({
appId: t.appid,
path: t.page,
success: function(i) {
wx.hideLoading();
n();
e.log("打开其他程序成功，appId=", t.appid, "path=", t.page);
},
fail: function(o) {
wx.hideLoading();
if (t.ad_image && -1 != [ ".jpg", ".gif", ".png" ].indexOf(t.ad_image.substr(-4))) {
wx.previewImage({
urls: [ t.ad_image ]
});
n();
e.log("直接跳转失败，拉起小程序码，appId=", t.appid, "path=", t.page);
} else {
i();
e.log("直接跳转失败，小程序码未设置，appId=", t.appid, "path=", t.page, JSON.stringify(o));
}
}
});
}
});
};
t.prototype.launchToSync = function(t, n) {
if (!wx.redirectTo && wx.getSystemInfoSync().SDKVersion < "2.2.0") {
wx.previewImage({
urls: [ t.ad_image ]
});
n(!0);
} else {
wx.showLoading({
title: "请稍候...",
mask: !0
});
setTimeout(wx.hideLoading, 5e3);
wx.navigateToMiniProgram({
appId: t.appid,
path: t.page,
success: function(i) {
wx.hideLoading();
n(!0);
e.log("打开其他程序成功，appId=", t.appid, "path=", t.page);
},
fail: function(i) {
wx.hideLoading();
if (t.ad_image && -1 != [ ".jpg", ".gif", ".png" ].indexOf(t.ad_image.substr(-4))) {
wx.previewImage({
urls: [ t.ad_image ]
});
n(!0);
e.log("直接跳转失败，拉起小程序码，appId=", t.appid, "path=", t.page);
} else {
n(!1);
e.log("直接跳转失败，小程序码未设置，appId=", t.appid, "path=", t.page, JSON.stringify(i));
}
}
});
}
};
t.prototype.share = function(t, n, i, o) {
var r = e.Share.genShareInfo(t), a = {};
a.title = r.title;
r.image ? a.imageUrl = e.Share.genImagePath(r.image) : a.imageUrl = r.image;
a.prefix = r.prefix;
wx.redirectTo ? a.path = e.Share.getShareLink(t.serial, t.path || "/pages/index/index", t.params || {}, r.imageId) : a.query = e.Share.getShareQuery(r.serial, r.params || {}, r.imageId);
a.success = function(e) {
n && n.call(o, e);
};
a.fail = function(e) {
i && i.call(o, e);
};
a.cancel = function(e) {
i && i.call(o, e);
};
var c = void 0 == a.query || null == a.query || null == a.query.sharetime || void 0 == a.query.sharetime ? "" : a.query.sharetime;
e.Share.reportShareOut(t.serial, r.imageId.toString(), "", "", c);
return a;
};
t.prototype.getCacheRes = function(e, n) {
var i = this;
void 0 === n && (n = !1);
return new Promise(function(o, r) {
if ("https://" === e.substr(0, 8).toLowerCase()) {
var a = wx.env.USER_DATA_PATH + "/mpsdk/" + e.substr(8), c = wx.getFileSystemManager();
try {
c.accessSync(a);
if (c.statSync(a).isFile()) {
o(a);
return;
}
c.rmdirSync(a);
} catch (e) {}
if (t.downloadQueue.indexOf(e) >= 0) o(e); else {
!n && o(e);
t.downloadQueue.push(e);
wx.downloadFile({
url: e,
success: function(t) {
if (200 === t.statusCode) {
i.mkdirRecursiveSync(a.substring(0, a.lastIndexOf("/")));
c.saveFileSync(t.tempFilePath, a);
n && o(a);
} else n && o(e);
},
fail: function() {
n && o(e);
},
complete: function() {
t.downloadQueue.splice(t.downloadQueue.indexOf(e), 1);
}
});
}
} else o(e);
});
};
t.prototype.mkdirRecursiveSync = function(e) {
for (var t = wx.getFileSystemManager(), n = e.replace(wx.env.USER_DATA_PATH, "").split("/"), i = wx.env.USER_DATA_PATH, o = 0, r = n; o < r.length; o++) {
var a = r[o];
if (a) {
i += "/" + a;
try {
t.accessSync(i);
if (!t.statSync(i).isDirectory()) {
t.unlinkSync(i);
t.mkdirSync(i);
}
} catch (e) {
t.mkdirSync(i);
}
}
}
return i;
};
t.prototype.setTimeout = function(e, t) {
return setTimeout(e, t);
};
t.prototype.setInterval = function(e, t) {
return setInterval(e, t);
};
t.prototype.clearTimeout = function(e) {
clearTimeout(e);
};
t.prototype.clearInterval = function(e) {
clearInterval(e);
};
t.prototype.parseShareTicket = function(t) {
var n = this;
return new Promise(function(i, o) {
t ? wx.login({
fail: function(e) {
o(e);
},
success: function(r) {
wx.getShareInfo({
shareTicket: t,
fail: function(e) {
o(e);
},
success: function(t) {
var a = {
gameId: e.Env.gameId,
code: encodeURIComponent(r.code),
encryptedData: encodeURIComponent(t.encryptedData),
iv: t.iv
};
n.httpRequest(e.Env.loginServer + "/MiniGame/data/getShareInfo.action", a).then(function(e) {
e.openGId ? i(e) : o(e);
}).catch(function(e) {
o(e);
});
}
});
}
}) : o("shareTicket required");
});
};
t.prototype.subscribe = function(t, n, i) {
var o = this;
void 0 === i && (i = "");
return new Promise(function(r, a) {
"the formId is a mock one" != n ? o.subscribeEnable(t) ? e.Account.getAccountSafe().then(function(a) {
var c = {
clockId: t,
gameId: e.Env.gameId,
openId: a.openid,
formId: n,
parameters: i
};
o.httpRequest(e.Env.loginServer + "/MiniGame/data/regNotify.action", c).then(function(n) {
wx.setStorageSync("regnotify_" + t + "_endtime", n.time);
wx.setStorageSync("regnotify_" + t + "_starttime", new Date().getTime());
var i = new Date(n.time).toLocaleDateString() + " " + new Date(n.time).toLocaleTimeString();
e.log("注册模板消息成功", JSON.stringify(c), "有效期至", i);
r();
});
}) : e.log("订阅有效期内无需重复订阅") : e.log("只能在真机环境下才能订阅模板消息");
});
};
t.prototype.subscribeEnable = function(e) {
var t = +wx.getStorageSync("regnotify_" + e + "_starttime"), n = +wx.getStorageSync("regnotify_" + e + "_endtime"), i = new Date().getTime();
return !(t <= i && i <= n);
};
t.prototype.checkBannerClick = function(e) {
return !!(e && 1038 == e.scene && e.referrerInfo && e.referrerInfo.appId);
};
t.prototype.onHide = function() {
try {
wx.onAppShow(function(t) {
e.Report.reportAppRun(t);
e.Report.reportHotLaunch(t);
e.Hack.onShow();
});
wx.onAppHide(function(t) {
e.Report.reportOnlineTimeCountOnHide();
e.Hack.checkRewardedVideoAd2Back(t);
var n = e.Account.getAccount(), i = {
gameId: e.Env.gameId,
gamePath: e.Env.gamePath,
openid: n.openid,
unionid: n.unionid,
lastLoginTime: n.lastLoginTime,
createTime: n.createTime,
shareTime: n.shareTime,
sid: n.sid,
anonymous_openid: n.anonymous_openid,
options: t,
launchOptions: e.Env.launchOptions
};
e.Hack.onHide(i);
});
} catch (e) {}
try {
wx.onShow(function(t) {
e.Report.reportAppRun(t);
e.Report.reportHotLaunch(t);
e.Hack.onShow();
});
wx.onHide(function(t) {
e.Report.reportOnlineTimeCountOnHide();
e.Hack.checkRewardedVideoAd2Back(t);
var n = e.Account.getAccount(), i = {
gameId: e.Env.gameId,
gamePath: e.Env.gamePath,
openid: n.openid,
unionid: n.unionid,
lastLoginTime: n.lastLoginTime,
createTime: n.createTime,
shareTime: n.shareTime,
sid: n.sid,
anonymous_openid: n.anonymous_openid,
options: t,
launchOptions: e.Env.launchOptions
};
e.Hack.onHide(i);
});
} catch (e) {}
};
t.prototype.showModal = function(e) {
wx.showModal(e);
};
t.prototype.handleAfterLogin = function(e, t) {};
t.prototype.setSkipParams = function(e) {};
t.prototype.sendCreateTime2Native = function(e) {};
t.prototype.sendLastLoginTime2Native = function(e) {};
t.prototype.getAppHotLaunchOptions = function() {
return new Promise(function(e, t) {
e("");
});
};
t.prototype.getDeviceInfo = function() {
return new Promise(function(e, t) {
e("");
});
};
t.downloadQueue = [];
return t;
}();
e.MinaPlatform = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function t() {
this.width = 0;
this.height = 0;
this.scrollItemList = [];
this.showIndex = 0;
}
t.prototype.showSuggestBottom = function(e, t, n, i, o) {
void 0 === t && (t = 600);
void 0 === n && (n = 120);
void 0 === i && (i = 16777215);
void 0 === o && (o = 16777215);
this.width = t;
this.height = n;
var r = new egret.DisplayObjectContainer(), a = new egret.Shape();
a.graphics.lineStyle(2, o);
a.graphics.beginFill(i, .3);
a.graphics.drawRoundRect(0, 0, this.width, this.height, 20, 20);
a.graphics.endFill();
r.addChild(a);
var c = new egret.TextField();
c.width = 30;
c.size = 20;
c.textColor = 16777215;
c.lineSpacing = 5;
c.text = "猜你喜欢";
c.y = (this.height - c.height) / 2;
c.x = 15;
r.addChild(c);
this.scrollView = new egret.ScrollView();
this.scrollView.height = this.height;
this.scrollView.width = this.width - 70;
this.scrollView.x = 50;
this.scrollView.verticalScrollPolicy = "off";
r.addChild(this.scrollView);
this.addIcon(e);
this.scrollView.setContent(this.scrollContainer);
this.setTimeIcon();
return r;
};
t.prototype.addIcon = function(t) {
var n = this;
this.scrollContainer = new egret.DisplayObjectContainer();
for (var i = function(i) {
o.imageLoader(t[i].icon).then(function(o) {
var r = new egret.DisplayObjectContainer(), a = new egret.Bitmap(o);
a.width = n.height - 20;
a.height = n.height - 20;
var c = Math.floor(n.scrollView.width / a.width);
c > t.length && (c = t.length);
r.addChild(a);
var s = new egret.Shape();
s.graphics.beginFill(16777215, 1);
s.graphics.drawRoundRect(0, 0, a.width, a.height, 25, 25);
s.graphics.endFill();
r.addChild(s);
a.mask = s;
r.anchorOffsetX = r.width / 2;
r.anchorOffsetY = r.height / 2;
r.x = i * a.width + i * (n.scrollView.width - c * a.height) / (c - 1) + r.width / 2;
r.y = r.height / 2 + (n.scrollView.height - a.height) / 2;
r.touchEnabled = !0;
r.addEventListener(egret.TouchEvent.TOUCH_TAP, function() {
e.Ad.click(t[i]);
}, n);
n.scrollContainer.addChild(r);
n.scrollItemList[i] = r;
});
}, o = this, r = 0; r < t.length; r++) i(r);
};
t.prototype.setTimeIcon = function() {
var e = this;
clearTimeout(this.scrollTimer);
this.scrollTimer = setTimeout(function() {
var t = e.showIndex, n = e.scrollView.width;
if (e.scrollItemList[t].x > n + e.scrollView.scrollLeft) {
var i = e.scrollItemList[t].x - e.scrollItemList[t].width / 2;
i + n > e.scrollContainer.width && (i = e.scrollContainer.width - n);
e.scrollView.setScrollLeft(i, 200);
} else e.scrollView.scrollLeft > e.scrollItemList[t].x && e.scrollView.setScrollLeft(e.scrollItemList[t].x - e.scrollItemList[t].width / 2, 200);
e.scrollContainer.setChildIndex(e.scrollItemList[t], e.scrollItemList.length);
egret.Tween.get(e.scrollItemList[t]).to({
scaleX: 1.2,
scaleY: 1.2
}, 700).to({
scaleX: 1,
scaleY: 1
}, 700).call(function() {
e.scrollContainer.setChildIndex(e.scrollItemList[t], 1);
egret.Tween.removeTweens(e.scrollItemList[t]);
e.setTimeIcon();
e.showIndex++;
e.showIndex >= e.scrollItemList.length && (e.showIndex = 0);
});
}, 5e3);
};
t.prototype.imageLoader = function(e) {
var t = this;
return new Promise(function(n, i) {
var o = new egret.ImageLoader();
o.addEventListener(egret.Event.COMPLETE, function(e) {
var t = e.currentTarget, i = new egret.Texture();
i._setBitmapData(t.data);
n(i);
}, t);
o.addEventListener(egret.IOErrorEvent.IO_ERROR, function(e) {
i(e);
}, t);
o.load(e);
});
};
return t;
}();
t.EgretUI = n;
})(e.ext || (e.ext = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.prototype.writeFile = function(e, t) {
return "";
};
t.prototype.getStorage = function(t) {
try {
var n = egret.localStorage.getItem(e.Env.storagePrefix + t);
return n ? JSON.parse(n) : "";
} catch (e) {
return "";
}
};
t.prototype.setStorage = function(t, n) {
egret.localStorage.setItem(e.Env.storagePrefix + t, JSON.stringify(n));
};
t.prototype.httpRequest = function(e, t, n, i) {
void 0 === n && (n = "get");
void 0 === i && (i = 2);
var o = "";
if (t) {
for (var r in t) o += r + "=" + t[r] + "&";
if (o) {
o = o.substr(0, o.length - 1);
if ("get" == n) {
var a = new Date().getTime();
e += -1 == e.indexOf("?") ? "?" : "&";
e += "t=" + a + "&";
e += o;
}
}
}
return new Promise(function(t, i) {
var r = new egret.HttpRequest();
r.once(egret.Event.COMPLETE, function(e) {
var n = e.currentTarget.response;
try {
t(JSON.parse(n));
} catch (e) {
t(n);
}
}, null);
r.once(egret.IOErrorEvent.IO_ERROR, function(e) {}, null);
if ("get" == n) {
r.open(e, egret.HttpMethod.GET);
r.send();
} else {
r.open(e, egret.HttpMethod.POST);
r.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
r.send(o);
}
});
};
t.prototype.writeOpenId2Native = function(e) {
if (e && e.openid) try {
egret.ExternalInterface.call("getNativeAppOpenId", e.openid);
} catch (e) {}
};
t.prototype.getUserAccount = function(t) {
var n = this;
return new Promise(function(i, o) {
try {
if ("Android" != egret.Capabilities.os && "iOS" != egret.Capabilities.os) e.utils.nativeAppLogin(t).then(function(e) {
n.writeOpenId2Native(e);
i(e);
}).catch(function(e) {
o(e);
}); else {
egret.ExternalInterface.call("getNativeAppOpenId", "");
egret.ExternalInterface.addCallback("getNativeAppOpenId", function(r) {
e.log("egret receive message--getNativeAppOpenId : ", r);
if (r) {
var a = {
openid: r
};
e.Account.setAccount(a);
}
e.utils.nativeAppLogin(t).then(function(e) {
n.writeOpenId2Native(e);
i(e);
}).catch(function(e) {
o(e);
});
});
}
} catch (r) {
e.log("egret getUserAccount====>error==", r);
e.utils.nativeAppLogin(t).then(function(e) {
n.writeOpenId2Native(e);
i(e);
}).catch(function(e) {
o(e);
});
}
});
};
t.prototype.getUserInfo = function() {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(e) {
return [ 2, {
nickName: "TestNickName",
avatarUrl: "",
gender: -1
} ];
});
});
};
t.prototype.getSystem = function() {
return {
platform: egret.Capabilities.os
};
};
t.prototype.getLaunchOptions = function() {
return {
scene: 0,
query: {}
};
};
t.prototype.launchTo = function(t) {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(n) {
e.log("打开其他程序，appId=", t.appid, "path=", t.page);
return [ 2 ];
});
});
};
t.prototype.launchToSync = function(t, n) {
return __awaiter(this, void 0, void 0, function() {
var i;
return __generator(this, function(o) {
e.log("打开其他程序，appId=", t.appid, "path=", t.page);
if (t) try {
i = {
appid: t.appid,
page: e.Env.landingPageServer + t.page
};
e.log("打开其他程序，appinfo =", JSON.stringify(i));
egret.ExternalInterface.call("clickApp", JSON.stringify(i));
egret.ExternalInterface.addCallback("clickApp", function(t) {
e.log("isJumpOk==", t);
n("1" == t);
});
} catch (t) {
n(!1);
e.log("launch2Sync error==", t);
}
return [ 2 ];
});
});
};
t.prototype.setSkipParams = function(t) {
if (t && t.page) {
e.log("设置app跳转参数--appinfo =", JSON.stringify(t));
try {
var n = "";
n = t.page.toString().split("?")[1] || t.page.toString();
e.log("设置app跳转参数--query1 =", n);
if (-1 == n.indexOf("=")) {
e.log("设置app跳转参数--无查询参数");
return;
}
n = "qmkey" + e.utils.base64encode(n);
e.log("设置app跳转参数--query2 =", n);
egret.ExternalInterface.call("setSkipParams", n);
} catch (t) {
e.log("egret setSkipParams error--", t);
}
}
};
t.prototype.share = function(t, n, i, o) {
e.log("测试分享，success和fail都调用，方便调试");
n && n.call(o);
i && i.call(o);
};
t.prototype.getCacheRes = function(e, t) {
void 0 === t && (t = !1);
return Promise.resolve(e);
};
t.prototype.setTimeout = function(e, t) {
return setTimeout(e, t);
};
t.prototype.setInterval = function(e, t) {
return setInterval(e, t);
};
t.prototype.clearTimeout = function(e) {
clearTimeout(e);
};
t.prototype.clearInterval = function(e) {
clearInterval(e);
};
t.prototype.showSuggestBottom = function(t, n, i, o, r) {
void 0 === n && (n = 600);
void 0 === i && (i = 120);
void 0 === o && (o = 16777215);
void 0 === r && (r = 16777215);
return new e.ext.EgretUI().showSuggestBottom(t, n, i, o, r);
};
t.prototype.checkBannerClick = function(e) {
throw new Error("Method not implemented.");
};
t.prototype.onHide = function(t) {
void 0 === t && (t = "");
try {
egret.ExternalInterface.call("getPhoneModel", "");
egret.ExternalInterface.addCallback("getPhoneModel", function(t) {
e.log("phoneModel==", t);
-1 != t.indexOf("Exception") && (t = "");
if (t) {
var n = JSON.parse(t);
e.log("deviceInfo[oiad]==", n.oaid);
e.log("deviceInfo[androidId]==", n.androidId);
e.log("deviceInfo[mode]==", n.mode);
e.log("deviceInfo[imei]==", n.imei);
e.log("deviceInfo[mac]==", n.mac);
e.Env.phoneModel = n.mode;
e.Env.mpsdkImei = n.imei;
e.Env.mpsdkAndroidId = n.androidId;
e.Env.mpsdkMac = n.mac;
e.Env.mpsdkOaid = n.oaid;
}
});
} catch (e) {}
try {
t && egret.ExternalInterface.call("getNativeAppOpenId2", t);
} catch (e) {}
try {
egret.ExternalInterface.call("getMetaDataByKey", "MPSDK_CHANNEL");
egret.ExternalInterface.addCallback("getMetaDataByKey", function(t) {
e.Env.mpsdkChannel = t;
e.log("mpsdkChannel==", t);
});
} catch (e) {}
try {
egret.ExternalInterface.call("getAppLaunchOptions", "");
egret.ExternalInterface.addCallback("getAppLaunchOptions", function(t) {
e.Env.appLaunchOptions = t;
e.log("appLaunchOptions==", t);
});
} catch (e) {}
-1 != e.Env.appLaunchOptions.indexOf("Exception") && (e.Env.appLaunchOptions = "");
-1 != e.Env.mpsdkAndroidId.indexOf("Exception") && (e.Env.mpsdkAndroidId = "");
-1 != e.Env.mpsdkImei.indexOf("Exception") && (e.Env.mpsdkImei = "");
-1 != e.Env.phoneModel.indexOf("Exception") && (e.Env.phoneModel = "");
-1 != e.Env.mpsdkChannel.indexOf("Exception") && (e.Env.mpsdkChannel = "mpsdk");
};
t.prototype.showModal = function(e) {};
t.prototype.send2Native = function(e) {
egret.ExternalInterface.call("sendToNative", e);
};
t.prototype.handleAfterLogin = function(e, t) {};
t.prototype.sendCreateTime2Native = function(t) {
try {
egret.ExternalInterface.call("setAccountCreateTime", t);
} catch (t) {
e.log("egret sendCreateTime2Native error--", t);
}
};
t.prototype.sendLastLoginTime2Native = function(t) {
try {
egret.ExternalInterface.call("setAccountLastLoginTime", t);
} catch (t) {
e.log("egret sendLastLoginTime2Native error--", t);
}
};
t.prototype.getAppHotLaunchOptions = function() {
return new Promise(function(t, n) {
try {
egret.ExternalInterface.call("getAppLaunchOptions", "");
egret.ExternalInterface.addCallback("getAppLaunchOptions", function(t) {
e.Env.appLaunchOptions = t;
e.log("appLaunchOptions==", t);
});
} catch (e) {}
-1 != e.Env.appLaunchOptions.indexOf("Exception") && (e.Env.appLaunchOptions = "");
var i = e.utils.httpParseQuery(e.Env.appLaunchOptions);
e.Env.launchOptions = {
scene: e.Env.mpsdkChannel,
query: i
};
t(e.Env.appLaunchOptions);
});
};
t.prototype.getDeviceInfo = function() {
return new Promise(function(t, n) {
try {
egret.ExternalInterface.call("getPhoneModel", "");
egret.ExternalInterface.addCallback("getPhoneModel", function(n) {
e.log("phoneModel==", n);
-1 != n.indexOf("Exception") && (n = "");
if (n) {
var i = JSON.parse(n);
e.log("getDeviceInfo[oiad]==", i.oaid);
e.log("getDeviceInfo[androidId]==", i.androidId);
e.log("getDeviceInfo[mode]==", i.mode);
e.log("getDeviceInfo[imei]==", i.imei);
e.log("getDeviceInfo[mac]==", i.mac);
e.Env.phoneModel = i.mode;
e.Env.mpsdkImei = i.imei;
e.Env.mpsdkAndroidId = i.androidId;
e.Env.mpsdkMac = i.mac;
e.Env.mpsdkOaid = i.oaid;
}
t(n);
});
} catch (n) {
e.log("getDeviceInfo error==", n);
t("");
}
});
};
return t;
}();
e.EgretPlatform = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.prototype.writeFile = function(e, t) {
return "";
};
t.prototype.getStorage = function(t) {
try {
var n = cc.sys.localStorage.getItem(e.Env.storagePrefix + t);
return n ? JSON.parse(n) : "";
} catch (e) {
return "";
}
};
t.prototype.setStorage = function(t, n) {
cc.sys.localStorage.setItem(e.Env.storagePrefix + t, JSON.stringify(n));
};
t.prototype.httpRequest = function(e, t, n, i) {
void 0 === n && (n = "get");
void 0 === i && (i = 2);
var o = "";
if (t) {
for (var r in t) o += r + "=" + t[r] + "&";
if (o) {
o = o.substr(0, o.length - 1);
if ("get" == n) {
e += -1 == e.indexOf("?") ? "?" : "&";
e += o;
}
}
}
return new Promise(function(t, i) {
var r = cc.loader.getXMLHttpRequest();
r.onreadystatechange = function() {
if (4 == r.readyState && r.status >= 200 && r.status < 400) try {
t(JSON.parse(r.responseText));
} catch (e) {
t(r.responseText);
}
};
if ("get" == n) {
r.open("GET", e, !0);
r.send();
} else {
r.open("POST", e, !0);
r.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
r.send(o);
}
});
};
t.prototype.writeOpenId2Native = function(e) {
if (e && e.openid) try {
"android" == this.getSystem().platform.toLocaleLowerCase() ? jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "getNativeAppOpenId", "(Ljava/lang/String;)Ljava/lang/String;", e.openid) : "ios" == this.getSystem().platform.toLocaleLowerCase() && jsb.reflection.callStaticMethod("MpsdkNativeUtils", "getNativeAppOpenId:", e.openid);
} catch (e) {}
};
t.prototype.getNativeAppOpenId = function() {
var e = "";
try {
"android" == this.getSystem().platform.toLocaleLowerCase() ? e = jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "getNativeAppOpenId", "(Ljava/lang/String;)Ljava/lang/String;", "") : "ios" == this.getSystem().platform.toLocaleLowerCase() && (e = jsb.reflection.callStaticMethod("MpsdkNativeUtils", "getNativeAppOpenId:", ""));
} catch (e) {}
return e;
};
t.prototype.getUserAccount = function(t) {
var n = this;
return new Promise(function(i, o) {
if (e.Account.getOpenId()) e.utils.nativeAppLogin(t).then(function(e) {
n.writeOpenId2Native(e);
i(e);
}).catch(function(e) {
o(e);
}); else {
var r = n.getNativeAppOpenId();
e.log("cocoscreator receive message--getNativeAppOpenId : ", r);
if (r) {
var a = {
openid: r
};
e.Account.setAccount(a);
}
e.utils.nativeAppLogin(t).then(function(e) {
n.writeOpenId2Native(e);
i(e);
}).catch(function(e) {
o(e);
});
}
});
};
t.prototype.getUserInfo = function() {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(e) {
return [ 2, {
nickName: "TestNickName",
avatarUrl: "",
gender: -1
} ];
});
});
};
t.prototype.getSystem = function() {
return {
platform: cc.sys.os
};
};
t.prototype.getLaunchOptions = function() {
return {
scene: 0,
query: {}
};
};
t.prototype.launchTo = function(t) {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(n) {
e.log("打开其他程序，appId=", t.appid, "path=", t.page);
return [ 2 ];
});
});
};
t.prototype.launchToSync = function(t, n) {
return __awaiter(this, void 0, void 0, function() {
var i, o;
return __generator(this, function(r) {
e.log("打开其他程序，appId=", t.appid, "path=", t.page);
if (t) try {
i = {
appid: t.appid,
page: e.Env.landingPageServer + t.page
};
e.log("打开其他程序，appinfo =", JSON.stringify(i));
o = "0";
cc.sys.os == cc.sys.OS_ANDROID ? o = jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "clickApp", "()Ljava/lang/String;", JSON.stringify(i)) : cc.sys.os == cc.sys.OS_IOS && (o = jsb.reflection.callStaticMethod("MpsdkNativeUtils", "clickApp", JSON.stringify(i)));
e.log("isJumpOk==", o);
n("1" == o);
} catch (t) {
n(!1);
e.log("launch2Sync error==", t);
}
return [ 2 ];
});
});
};
t.prototype.setSkipParams = function(t) {
if (t && t.page) {
e.log("设置app跳转参数--appinfo =", JSON.stringify(t));
try {
var n = "";
n = t.page.toString().split("?")[1] || t.page.toString();
e.log("设置app跳转参数--query1 =", n);
if (-1 == n.indexOf("=")) {
e.log("设置app跳转参数--无查询参数");
return;
}
n = "qmkey" + e.utils.base64encode(n);
e.log("设置app跳转参数--query2 =", n);
"android" == this.getSystem().platform.toLocaleLowerCase() ? jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "setSkipParams", "(Ljava/lang/String;)V", n) : "ios" == this.getSystem().platform.toLocaleLowerCase() && jsb.reflection.callStaticMethod("MpsdkNativeUtils", "setSkipParams:", n);
} catch (t) {
e.log("cc setSkipParams error--", t);
}
}
};
t.prototype.share = function(t, n, i, o) {
e.log("测试分享，success和fail都调用，方便调试");
n && n.call(o);
i && i.call(o);
};
t.prototype.getCacheRes = function(e, t) {
void 0 === t && (t = !1);
return Promise.resolve(e);
};
t.prototype.setTimeout = function(e, t) {
return setTimeout(e, t);
};
t.prototype.setInterval = function(e, t) {
return setInterval(e, t);
};
t.prototype.clearTimeout = function(e) {
clearTimeout(e);
};
t.prototype.clearInterval = function(e) {
clearInterval(e);
};
t.prototype.checkBannerClick = function(e) {
throw new Error("Method not implemented.");
};
t.prototype.onHide = function(t) {
void 0 === t && (t = "");
try {
if ("android" == this.getSystem().platform.toLocaleLowerCase()) {
var n = jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "getPhoneModel", "()Ljava/lang/String;");
e.log("phoneModel==", n);
-1 != n.indexOf("Exception") && (n = "");
if (n) {
var i = JSON.parse(n);
e.log("deviceInfo[oiad]==", i.oaid);
e.log("deviceInfo[androidId]==", i.androidId);
e.log("deviceInfo[mode]==", i.mode);
e.log("deviceInfo[imei]==", i.imei);
e.log("deviceInfo[mac]==", i.mac);
e.Env.phoneModel = i.mode;
e.Env.mpsdkImei = i.imei;
e.Env.mpsdkAndroidId = i.androidId;
e.Env.mpsdkMac = i.mac;
e.Env.mpsdkOaid = i.oaid;
}
e.Env.appLaunchOptions = jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "getAppLaunchOptions", "()Ljava/lang/String;");
try {
t && jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "getNativeAppOpenId2", "(Ljava/lang/String;)V", t);
} catch (e) {}
e.Env.mpsdkChannel = jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "getMetaDataByKey", "(Ljava/lang/String;)Ljava/lang/String;", "MPSDK_CHANNEL");
} else if ("ios" == this.getSystem().platform.toLocaleLowerCase()) {
e.Env.phoneModel = jsb.reflection.callStaticMethod("MpsdkNativeUtils", "getPhoneModel");
e.Env.appLaunchOptions = jsb.reflection.callStaticMethod("MpsdkNativeUtils", "getAppLaunchOptions");
e.Env.mpsdkChannel = jsb.reflection.callStaticMethod("MpsdkNativeUtils", "getMetaDataByKey:", "MPSDK_CHANNEL");
e.Env.mpsdkImei = jsb.reflection.callStaticMethod("MpsdkNativeUtils", "getMetaDataByKey:", "MPSDK_IMEI");
}
-1 != e.Env.appLaunchOptions.indexOf("Exception") && (e.Env.appLaunchOptions = "");
-1 != e.Env.mpsdkAndroidId.indexOf("Exception") && (e.Env.mpsdkAndroidId = "");
-1 != e.Env.mpsdkImei.indexOf("Exception") && (e.Env.mpsdkImei = "");
-1 != e.Env.phoneModel.indexOf("Exception") && (e.Env.phoneModel = "");
-1 != e.Env.mpsdkChannel.indexOf("Exception") && (e.Env.mpsdkChannel = "mpsdk");
} catch (e) {}
};
t.prototype.showModal = function(e) {};
t.prototype.handleAfterLogin = function(e, t) {};
t.prototype.sendCreateTime2Native = function(e) {
try {
"android" == this.getSystem().platform.toLocaleLowerCase() ? jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "setAccountCreateTime", "(Ljava/lang/String;)V", e) : "ios" == this.getSystem().platform.toLocaleLowerCase() && jsb.reflection.callStaticMethod("MpsdkNativeUtils", "setAccountCreateTime:", e);
} catch (e) {}
};
t.prototype.sendLastLoginTime2Native = function(e) {
try {
"android" == this.getSystem().platform.toLocaleLowerCase() ? jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "setAccountLastLoginTime", "(Ljava/lang/String;)V", e) : "ios" == this.getSystem().platform.toLocaleLowerCase() && jsb.reflection.callStaticMethod("MpsdkNativeUtils", "setAccountLastLoginTime:", e);
} catch (e) {}
};
t.prototype.getAppHotLaunchOptions = function() {
var t = this;
return new Promise(function(n, i) {
try {
"android" == t.getSystem().platform.toLocaleLowerCase() ? e.Env.appLaunchOptions = jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "getAppLaunchOptions", "()Ljava/lang/String;") : "ios" == t.getSystem().platform.toLocaleLowerCase() && (e.Env.appLaunchOptions = jsb.reflection.callStaticMethod("MpsdkNativeUtils", "getAppLaunchOptions"));
-1 != e.Env.appLaunchOptions.indexOf("Exception") && (e.Env.appLaunchOptions = "");
} catch (e) {}
var o = e.utils.httpParseQuery(e.Env.appLaunchOptions);
e.Env.launchOptions = {
scene: e.Env.mpsdkChannel,
query: o
};
n(e.Env.appLaunchOptions);
});
};
t.prototype.getDeviceInfo = function() {
var t = this;
return new Promise(function(n, i) {
var o = "";
try {
if ("android" == t.getSystem().platform.toLocaleLowerCase()) {
o = jsb.reflection.callStaticMethod("com/qmo/game/mpsdk/utils/MpsdkNativeUtils", "getPhoneModel", "()Ljava/lang/String;");
e.log("phoneModel==", o);
-1 != o.indexOf("Exception") && (o = "");
if (o) {
var r = JSON.parse(o);
e.log("getDeviceInfo[oiad]==", r.oaid);
e.log("getDeviceInfo[androidId]==", r.androidId);
e.log("getDeviceInfo[mode]==", r.mode);
e.log("getDeviceInfo[imei]==", r.imei);
e.log("getDeviceInfo[mac]==", r.mac);
e.Env.phoneModel = r.mode;
e.Env.mpsdkImei = r.imei;
e.Env.mpsdkAndroidId = r.androidId;
e.Env.mpsdkMac = r.mac;
e.Env.mpsdkOaid = r.oaid;
}
}
n(o);
} catch (t) {
e.log("getDeviceInfo error==", t);
n("");
}
});
};
return t;
}();
e.CocosPlatform = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.prototype.writeFile = function(e, t) {
return "";
};
t.prototype.getStorage = function(t) {
try {
var n = window.localStorage.getItem(e.Env.storagePrefix + t);
return n ? JSON.parse(n) : "";
} catch (e) {
return "";
}
};
t.prototype.setStorage = function(t, n) {
window.localStorage.setItem(e.Env.storagePrefix + t, JSON.stringify(n));
};
t.prototype.httpRequest = function(e, t, n, i) {
void 0 === n && (n = "get");
void 0 === i && (i = 2);
var o = "";
if (t) {
for (var r in t) o += r + "=" + t[r] + "&";
if (o) {
o = o.substr(0, o.length - 1);
if ("get" == n) {
e += -1 == e.indexOf("?") ? "?" : "&";
e += o;
}
}
}
return new Promise(function(t, i) {
var r = new XMLHttpRequest();
r.onreadystatechange = function() {
if (4 == r.readyState) if (200 == r.status) try {
t(JSON.parse(r.responseText));
} catch (e) {
t(r.responseText);
} else i("HTTP请求失败，statusCode=" + r.status);
};
if ("get" == n) {
r.open("GET", e);
r.send();
} else {
r.open("POST", e);
r.send(o);
}
});
};
t.prototype.getUserAccount = function(e) {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(e) {
return [ 2, {
openid: "debug-user-openid"
} ];
});
});
};
t.prototype.getUserInfo = function() {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(e) {
return [ 2, {
nickName: "TestNickName",
avatarUrl: "",
gender: -1
} ];
});
});
};
t.prototype.getSystem = function() {
return {
platform: navigator.platform
};
};
t.prototype.getLaunchOptions = function() {
return {
scene: 0,
query: {}
};
};
t.prototype.launchTo = function(t) {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(n) {
e.log("打开其他程序，appId=", t.appid, "path=", t.page);
return [ 2 ];
});
});
};
t.prototype.launchToSync = function(t, n) {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(n) {
e.log("打开其他程序，appId=", t.appid, "path=", t.page);
return [ 2 ];
});
});
};
t.prototype.share = function(t, n, i, o) {
e.log("测试分享，success和fail都调用，方便调试");
n && n.call(o);
i && i.call(o);
};
t.prototype.getCacheRes = function(e, t) {
void 0 === t && (t = !1);
return Promise.resolve(e);
};
t.prototype.setTimeout = function(e, t) {
return setTimeout(e, t);
};
t.prototype.setInterval = function(e, t) {
return setInterval(e, t);
};
t.prototype.clearTimeout = function(e) {
clearTimeout(e);
};
t.prototype.clearInterval = function(e) {
clearInterval(e);
};
t.prototype.checkBannerClick = function(e) {
throw new Error("Method not implemented.");
};
t.prototype.onHide = function() {};
t.prototype.showModal = function(e) {};
t.prototype.handleAfterLogin = function(e, t) {};
t.prototype.setSkipParams = function(e) {};
t.prototype.sendCreateTime2Native = function(e) {};
t.prototype.sendLastLoginTime2Native = function(e) {};
t.prototype.getAppHotLaunchOptions = function() {
return new Promise(function(e, t) {
e("");
});
};
t.prototype.getDeviceInfo = function() {
return new Promise(function(e, t) {
e("");
});
};
return t;
}();
e.H5Platform = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function(t) {
__extends(n, t);
function n() {
return null !== t && t.apply(this, arguments) || this;
}
n.prototype.getUserAccount = function(t) {
var n = this;
return new Promise(function(i, o) {
n.getTTUserAccountOnce(t).then(function(e) {
i(e);
}).catch(function(r) {
n.getTTUserAccountOnce(t).then(function(e) {
i(e);
}).catch(function(r) {
n.getTTUserAccountOnce(t).then(function(e) {
i(e);
}).catch(function(t) {
e.log("login retry over.");
o(t);
});
});
});
});
};
n.prototype.getTTUserAccountOnce = function(t) {
var n = this;
return new Promise(function(i, o) {
wx.login({
force: !1,
success: function(r) {
wx.getSetting({
success: function(a) {
if (a.authSetting["scope.userInfo"]) wx.getUserInfo({
withCredentials: !0,
success: function(a) {
e.log("头条授权成功,继续服务器端登录 ", r);
var c = {
code: r.code ? r.code : "",
anonymous_code: r.anonymousCode ? r.anonymousCode : "",
encryptedData: a.encryptedData,
iv: a.iv
};
n.ttLogin(c, t).then(function(e) {
e.openid || (e.openid = e.anonymous_openid ? e.anonymous_openid : "");
i(e);
}).catch(function(e) {
o(e);
});
},
fail: function(a) {
e.log("头条授权成功,但获取userinfo失败,继续服务器端登录 ", r);
var c = {
code: r.code ? r.code : "",
anonymous_code: r.anonymousCode ? r.anonymousCode : "",
encryptedData: "",
iv: ""
};
n.ttLogin(c, t).then(function(e) {
e.openid || (e.openid = e.anonymous_openid ? e.anonymous_openid : "");
i(e);
}).catch(function(e) {
o(e);
});
}
}); else {
e.log("头条未授权,继续服务器端登录 ", r);
var c = {
code: r.code ? r.code : "",
anonymous_code: r.anonymousCode ? r.anonymousCode : "",
encryptedData: "",
iv: ""
};
n.ttLogin(c, t).then(function(e) {
e.openid || (e.openid = e.anonymous_openid ? e.anonymous_openid : "");
i(e);
}).catch(function(e) {
o(e);
});
}
},
fail: function(a) {
e.log("头条授权失败,继续服务器端登录 ", r, a);
var c = {
code: r.code ? r.code : "",
anonymous_code: r.anonymousCode ? r.anonymousCode : "",
encryptedData: "",
iv: ""
};
n.ttLogin(c, t).then(function(e) {
i(e);
}).catch(function(e) {
o(e);
});
}
});
},
fail: function(r) {
e.log("头条登录失败，继续匿名登录. ", r);
wx.login({
force: !1,
success: function(a) {
e.log("头条匿名登录成功,继续服务器端登录 ", a, r);
var c = {
code: a.code ? a.code : "",
anonymous_code: a.anonymousCode ? a.anonymousCode : "",
encryptedData: "",
iv: ""
};
n.ttLogin(c, t).then(function(e) {
e.openid || (e.openid = e.anonymous_openid ? e.anonymous_openid : "");
i(e);
}).catch(function(e) {
o(e);
});
},
fail: function(t) {
e.log("头条匿名登录失败. ", t);
o(t);
}
});
}
});
});
};
n.prototype.ttLogin = function(t, n) {
var i = this, o = e.utils.parseAccountSource(n);
return new Promise(function(n, r) {
var a = {
gameId: e.Env.gameId,
code: t.code,
anonymous_code: t.anonymous_code,
encryptedData: encodeURIComponent(t.encryptedData),
iv: encodeURIComponent(t.iv),
unionId: t.encryptedData && t.iv ? "true" : "false",
sourceType: o.sourceType,
sourceId: o.sourceId,
model: encodeURIComponent(wx.getSystemInfoSync().model)
}, c = e.Env.newLoginServer + "/MiniLogin/data/getTTOpenId.action";
i.httpRequest(c, a, "get", 0).then(function(t) {
e.log("服务端登录成功", t);
t && t.anonymous_openid && e.Report.reportEvent(0, t.anonymous_openid, "");
n(t);
}).catch(function(t) {
e.log("服务端登录失败", t);
r(t);
});
});
};
n.prototype.onHide = function() {};
return n;
}(e.MinaPlatform);
e.TTPlatform = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.prototype.writeFile = function(e, t) {
if (!e || !t) return "";
var n = qq.getFileSystemManager(), i = qq.env.USER_DATA_PATH + "/mpsdkadqq", o = i + "/" + e;
try {
n.accessSync(i);
} catch (e) {
n.mkdirSync(i);
}
try {
n.accessSync(o);
return o;
} catch (e) {}
var r = new Array();
try {
r = n.readdirSync(i);
for (var a = e.split("_")[0], c = 0; c < r.length; c++) {
if (0 == r[c].indexOf(a + "_")) {
n.unlinkSync(o);
break;
}
}
} catch (e) {}
try {
n.writeFileSync(o, t, "utf-8");
} catch (t) {
console.log("write file fail. filename:", e, " error:", t);
o = "";
}
return o;
};
t.prototype.onHide = function() {
qq.onShow(function(t) {
e.Report.reportAppRun(t);
e.Report.reportHotLaunch(t);
e.Hack.onShow();
});
qq.onHide(function(t) {
e.Report.reportOnlineTimeCountOnHide();
e.Hack.checkRewardedVideoAd2Back(t);
var n = e.Account.getAccount(), i = {
gameId: e.Env.gameId,
gamePath: e.Env.gamePath,
openid: n.openid,
unionid: n.unionid,
lastLoginTime: n.lastLoginTime,
createTime: n.createTime,
shareTime: n.shareTime,
sid: n.sid,
anonymous_openid: n.anonymous_openid,
options: t,
launchOptions: e.Env.launchOptions
};
e.Hack.onHide(i);
});
};
t.prototype.getStorage = function(t) {
return qq.getStorageSync(e.Env.storagePrefix + t);
};
t.prototype.setStorage = function(t, n) {
qq.setStorageSync(e.Env.storagePrefix + t, n);
};
t.prototype.httpRequest = function(t, n, i, o) {
void 0 === i && (i = "get");
void 0 === o && (o = 2);
return __awaiter(this, void 0, void 0, function() {
var r, a;
return __generator(this, function(c) {
switch (c.label) {
case 0:
return [ 4, new Promise(function(e, t) {
qq.getNetworkType({
success: function(t) {
e(t);
},
fail: function(e) {
t(e);
}
});
}) ];

case 1:
if ("none" == c.sent().networkType) throw "当前网络不可用";
r = "";
if (n) {
for (a in n) r += a + "=" + n[a] + "&";
if (r) {
r = r.substr(0, r.length - 1);
if ("get" == i) {
t += -1 == t.indexOf("?") ? "?" : "&";
t += r;
}
}
}
return [ 4, new Promise(function(r, a) {
var c = {
url: t
};
if ("get" != i) {
c.method = "POST";
c.data = n;
}
var s = 0, d = function(n) {
if (++s <= o) {
qq.request(c);
e.log("HTTP请求重试(" + s + ")", t);
return !0;
}
a(n);
return !1;
};
c.success = function(e) {
e.statusCode && e.statusCode >= 200 && e.statusCode < 400 ? r(e.data) : d(e);
};
c.fail = function(e) {
d(e);
};
qq.request(c);
}) ];

case 2:
return [ 2, c.sent() ];
}
});
});
};
t.prototype.getUserAccount = function(t) {
var n = this;
return new Promise(function(i, o) {
n.getUserAccountOnce(t).then(function(e) {
i(e);
}).catch(function(r) {
n.getUserAccountOnce(t).then(function(e) {
i(e);
}).catch(function(r) {
n.getUserAccountOnce(t).then(function(e) {
i(e);
}).catch(function(t) {
e.log("login retry over.");
o(t);
});
});
});
});
};
t.prototype.getUserAccountOnce = function(t) {
var n = this;
return new Promise(function(i, o) {
qq.login({
success: function(r) {
qq.getSetting({
success: function(a) {
if (a.authSetting["scope.userInfo"]) qq.getUserInfo({
withCredentials: !0,
success: function(a) {
e.log("QQ授权成功,继续服务器端登录 ", r.code);
var c = {
code: r.code,
encryptedData: a.encryptedData,
iv: a.iv
};
n.login(c, t).then(function(e) {
i(e);
}).catch(function(e) {
o(e);
});
}
}); else {
e.log("QQ未授权,继续服务器端登录 ", r.code);
var c = {
code: r.code,
encryptedData: "",
iv: ""
};
n.login(c, t).then(function(e) {
i(e);
}).catch(function(e) {
o(e);
});
}
},
fail: function(a) {
e.log("QQ授权失败,继续服务器端登录 ", r.code, a);
var c = {
code: r.code,
encryptedData: "",
iv: ""
};
n.login(c, t).then(function(e) {
i(e);
}).catch(function(e) {
o(e);
});
}
});
},
fail: function(t) {
e.log("QQ登录失败: ", t);
o(t);
}
});
});
};
t.prototype.login = function(t, n) {
var i = this, o = e.utils.parseAccountSource(n);
return new Promise(function(n, r) {
var a = {
gameId: e.Env.gameId,
code: t.code,
encryptedData: encodeURIComponent(t.encryptedData),
iv: encodeURIComponent(t.iv),
unionId: t.encryptedData && t.iv ? "true" : "false",
sourceType: o.sourceType,
sourceId: o.sourceId,
model: encodeURIComponent(qq.getSystemInfoSync().model)
}, c = e.Env.newLoginServer + "/MiniLogin/data/getQQOpenId.action";
i.httpRequest(c, a, "get", 0).then(function(t) {
e.log("服务端登录成功", t);
n(t);
}).catch(function(t) {
e.log("服务端登录失败", t);
r(t);
});
});
};
t.prototype.getUserInfo = function() {
return new Promise(function(e, t) {
qq.getSetting({
fail: function(e) {
t();
},
success: function(n) {
n.authSetting["scope.userInfo"] ? qq.getUserInfo({
fail: function(e) {
t();
},
success: function(t) {
e(t.userInfo);
}
}) : t();
}
});
});
};
t.prototype.getSystem = function() {
return {
platform: qq.getSystemInfoSync().platform
};
};
t.prototype.getLaunchOptions = function() {
if (qq.getLaunchOptionsSync) return qq.getLaunchOptionsSync();
throw new Error("小程序启动参数请从app.js的onLaunch回调函数中获取");
};
t.prototype.launchTo = function(t) {
return new Promise(function(n, i) {
if (!qq.redirectTo && qq.getSystemInfoSync().SDKVersion < "2.2.0") {
qq.previewImage({
urls: [ t.ad_image ]
});
n();
} else {
qq.showLoading({
title: "请稍候...",
mask: !0
});
setTimeout(qq.hideLoading, 5e3);
qq.navigateToMiniProgram({
appId: t.appid,
path: t.page,
success: function(i) {
qq.hideLoading();
n();
e.log("打开其他程序成功，appId=", t.appid, "path=", t.page);
},
fail: function(o) {
qq.hideLoading();
if (t.ad_image && -1 != [ ".jpg", ".gif", ".png" ].indexOf(t.ad_image.substr(-4))) {
qq.previewImage({
urls: [ t.ad_image ]
});
n();
e.log("直接跳转失败，拉起小程序码，appId=", t.appid, "path=", t.page);
} else {
i();
e.log("直接跳转失败，小程序码未设置，appId=", t.appid, "path=", t.page, JSON.stringify(o));
}
}
});
}
});
};
t.prototype.launchToSync = function(e, t) {};
t.prototype.share = function(t, n, i, o) {
var r = e.Share.genShareInfo(t), a = {};
a.title = r.title;
a.imageUrl = r.image;
qq.redirectTo ? a.path = e.Share.getShareLink(t.serial, t.path || "/pages/index/index", t.params || {}, r.imageId) : a.query = e.Share.getShareQuery(r.serial, r.params || {}, r.imageId);
a.success = function(e) {
n && n.call(o, e);
};
a.fail = function(e) {
i && i.call(o, e);
};
a.cancel = function(e) {
i && i.call(o, e);
};
var c = void 0 == a.query || null == a.query || null == a.query.sharetime || void 0 == a.query.sharetime ? "" : a.query.sharetime;
e.Share.reportShareOut(t.serial, r.imageId.toString(), "", "", c);
return a;
};
t.prototype.getCacheRes = function(e, n) {
var i = this;
void 0 === n && (n = !1);
return new Promise(function(o, r) {
if ("https://" === e.substr(0, 8).toLowerCase()) {
var a = qq.env.USER_DATA_PATH + "/mpsdk/" + e.substr(8), c = qq.getFileSystemManager();
try {
c.accessSync(a);
if (c.statSync(a).isFile()) {
o(a);
return;
}
c.rmdirSync(a);
} catch (e) {}
if (t.downloadQueue.indexOf(e) >= 0) o(e); else {
!n && o(e);
t.downloadQueue.push(e);
qq.downloadFile({
url: e,
success: function(t) {
if (200 === t.statusCode) {
i.mkdirRecursiveSync(a.substring(0, a.lastIndexOf("/")));
c.saveFileSync(t.tempFilePath, a);
n && o(a);
} else n && o(e);
},
fail: function() {
n && o(e);
},
complete: function() {
t.downloadQueue.splice(t.downloadQueue.indexOf(e), 1);
}
});
}
} else o(e);
});
};
t.prototype.mkdirRecursiveSync = function(e) {
for (var t = qq.getFileSystemManager(), n = e.replace(qq.env.USER_DATA_PATH, "").split("/"), i = qq.env.USER_DATA_PATH, o = 0, r = n; o < r.length; o++) {
var a = r[o];
if (a) {
i += "/" + a;
try {
t.accessSync(i);
if (!t.statSync(i).isDirectory()) {
t.unlinkSync(i);
t.mkdirSync(i, !0);
}
} catch (e) {
t.mkdirSync(i, !0);
}
}
}
return i;
};
t.prototype.setTimeout = function(e, t) {
return setTimeout(e, t);
};
t.prototype.setInterval = function(e, t) {
return setInterval(e, t);
};
t.prototype.clearTimeout = function(e) {
clearTimeout(e);
};
t.prototype.clearInterval = function(e) {
clearInterval(e);
};
t.prototype.parseShareTicket = function(t) {
var n = this;
return new Promise(function(i, o) {
t ? qq.login({
fail: function(e) {
o(e);
},
success: function(r) {
qq.getShareInfo({
shareTicket: t,
fail: function(e) {
o(e);
},
success: function(t) {
var a = {
gameId: e.Env.gameId,
code: encodeURIComponent(r.code),
encryptedData: encodeURIComponent(t.encryptedData),
iv: t.iv
};
n.httpRequest(e.Env.loginServer + "/MiniGame/data/getShareInfo.action", a).then(function(e) {
e.openGId ? i(e) : o(e);
}).catch(function(e) {
o(e);
});
}
});
}
}) : o("shareTicket required");
});
};
t.prototype.subscribe = function(t, n, i) {
var o = this;
void 0 === i && (i = "");
return new Promise(function(r, a) {
"the formId is a mock one" != n ? o.subscribeEnable(t) ? e.Account.getAccountSafe().then(function(a) {
var c = {
clockId: t,
gameId: e.Env.gameId,
openId: a.openid,
formId: n,
parameters: i
};
o.httpRequest(e.Env.loginServer + "/MiniGame/data/regNotify.action", c).then(function(n) {
qq.setStorageSync("regnotify_" + t + "_endtime", n.time);
qq.setStorageSync("regnotify_" + t + "_starttime", new Date().getTime());
var i = new Date(n.time).toLocaleDateString() + " " + new Date(n.time).toLocaleTimeString();
e.log("注册模板消息成功", JSON.stringify(c), "有效期至", i);
r();
});
}) : e.log("订阅有效期内无需重复订阅") : e.log("只能在真机环境下才能订阅模板消息");
});
};
t.prototype.subscribeEnable = function(e) {
var t = +qq.getStorageSync("regnotify_" + e + "_starttime"), n = +qq.getStorageSync("regnotify_" + e + "_endtime"), i = new Date().getTime();
return !(t <= i && i <= n);
};
t.prototype.checkBannerClick = function(e) {
return !!(e && 1038 == e.scene && e.referrerInfo && e.referrerInfo.appId);
};
t.prototype.showModal = function(e) {};
t.prototype.reportGDTRegistEvent = function(t, n) {
try {
if (null == n || void 0 == n) return;
if (null == n.query || void 0 == n.query) return;
if (null == n.query.qz_gdt || void 0 == n.query.qz_gdt || 0 == n.query.qz_gdt.length) return;
if (null == t || void 0 == t || null == t.openid || void 0 == t.openid || 0 == t.openid.length) return;
if (0 != t.lastLoginTime) {
e.log("非新用户无需上报QQ-GTD广告转化注册事件");
return;
}
var i = t.openid, o = "", r = "", a = "";
e.Env.launchOptions.query.qz_gdt && (r = e.Env.launchOptions.query.qz_gdt);
e.Env.launchOptions.query.gdt_acid && (o = e.Env.launchOptions.query.gdt_acid);
e.Env.launchOptions.query.adid && (a = e.Env.launchOptions.query.adid);
var c = {
click_id: r,
openid: i,
action_type: "REGISTER",
gdt_acid: o,
adid: a
};
e.log("上报QQ-GTD广告转化新用户注册事件参数", c);
e.Platform.instance.httpRequest(e.Env.qqGdtServer + "/push/add", c, "post").then(function(t) {
e.log("上报QQ-GTD广告转化新用户注册事件成功.", t, JSON.stringify(c));
});
} catch (t) {
e.log("上报QQ-GTD广告转化新用户注册事件失败;", t);
}
};
t.prototype.handleAfterLogin = function(e, t) {
this.reportGDTRegistEvent(e, t);
};
t.prototype.setSkipParams = function(e) {};
t.prototype.sendCreateTime2Native = function(e) {};
t.prototype.sendLastLoginTime2Native = function(e) {};
t.prototype.getAppHotLaunchOptions = function() {
return new Promise(function(e, t) {
e("");
});
};
t.prototype.getDeviceInfo = function() {
return new Promise(function(e, t) {
e("");
});
};
t.downloadQueue = [];
return t;
}();
e.QQminiPlatform = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
Object.defineProperty(t, "platformType", {
get: function() {
return "object" == typeof BK ? "bk" : "object" == typeof tt ? "tt" : "object" == typeof qq ? "qq" : "object" == typeof wx ? "wx" : "object" == typeof egret ? "egret" : "object" == typeof cc ? "cocos" : "object" == typeof Laya ? "laya" : "unknow";
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(t, "instance", {
get: function() {
if (!this._instance) switch (this.platformType) {
case "bk":
this._instance = new e.BricksPlatform();
e.log("识别到玩一玩/厘米游戏平台");
break;

case "qq":
this._instance = new e.QQminiPlatform();
e.log("识别到QQ平台");
break;

case "tt":
this._instance = new e.TTPlatform();
e.log("识别到字节跳动平台");
break;

case "wx":
this._instance = new e.MinaPlatform();
e.log("识别到微信平台");
break;

case "egret":
this._instance = new e.EgretPlatform();
e.log("识别到Egret引擎");
break;

case "cocos":
this._instance = new e.CocosPlatform();
e.log("识别到Cocos引擎");
break;

case "laya":
this._instance = new e.LayaPlatform();
e.log("识别到Laya引擎");
break;

default:
e.log("未能识别平台类型，默认当前处于H5环境");
this._instance = new e.H5Platform();
}
return this._instance;
},
enumerable: !0,
configurable: !0
});
return t;
}();
e.Platform = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.loadBlackNames = function() {
this.blackNamesPromise || (this.blackNamesPromise = new Promise(function(t, n) {
if ("wx" == e.Platform.platformType) {
var i = e.Platform.instance.getStorage("black_names_ver");
e.Platform.instance.getStorage("black_names_data");
e.Platform.instance.httpRequest(e.Env.cdnServer + "/black_names_ver.json").then(function(n) {
e.utils.value(n, e.Platform.platformType) && e.utils.value(n, e.Platform.platformType) != e.utils.value(i, e.Platform.platformType) ? e.Platform.instance.httpRequest(e.Env.cdnServer + "/black_names_" + e.Platform.platformType + ".json").then(function(i) {
if ("object" == typeof i) {
e.Platform.instance.setStorage("black_names_ver", n);
e.Platform.instance.setStorage("black_names_data", i);
}
t();
}) : t();
});
} else t();
}));
return this.blackNamesPromise;
};
t.setAdTag = function(e) {
this.adTag = e;
};
t.setAccount = function(t) {
if (!t.openid) {
e.log("设置用户帐号失败，请至少设置openid，account=", t);
return !1;
}
t.createTime && (t.sid = t.createTime.toString().slice(2, -4) + t.openid.substr(t.openid.length - 4, t.openid.length));
t.unionid || (t.unionid = "");
'"null"' != t.unionid && "null" != t.unionid || (t.unionid = "");
t.sourceOpenId || (t.sourceOpenId = "");
'"null"' != t.sourceOpenId && "null" != t.sourceOpenId || (t.sourceOpenId = "");
'"null"' != t.ip && "null" != t.ip || (t.ip = "");
e.Platform.instance.setStorage("user_account", t);
e.log("更新openid成功", t.openid);
return !0;
};
t.getLastShareTime = function() {
return __awaiter(this, void 0, void 0, function() {
var e;
return __generator(this, function(t) {
switch (t.label) {
case 0:
return [ 4, this.getAccountSafe() ];

case 1:
return (e = t.sent()).shareTime ? [ 2, Number(e.shareTime) ] : e.createTime && new Date(e.createTime).toLocaleDateString() == new Date().toLocaleDateString() ? [ 2, Math.floor(e.createTime / 1e3) ] : [ 2, void 0 ];
}
});
});
};
t.getAccount = function() {
var t = e.Platform.instance.getStorage("user_account");
if (t && t.openid) {
t.unionid = t.unionid || "";
return t;
}
return {
openid: "",
unionid: "",
lastLoginTime: 0,
createTime: 0
};
};
t.getAccountSafe = function() {
var t = this;
return new Promise(function(n, i) {
if (t.getAccount().openid) n(t.getAccount()); else {
var o, r;
o = e.Platform.instance.setInterval(function() {
if (t.getAccount().openid) {
e.Platform.instance.clearInterval(o);
e.Platform.instance.clearTimeout(r);
n(t.getAccount());
}
}, 200);
r = e.Platform.instance.setTimeout(function() {
e.Platform.instance.clearInterval(o);
}, 3e4);
}
});
};
t.getADTag = function() {
return this.adTag;
};
t.getADTagSafe = function() {
var t = this;
return new Promise(function(n, i) {
if (t.adTag) n(t.adTag); else {
var o, r;
o = e.Platform.instance.setInterval(function() {
if (t.adTag) {
e.Platform.instance.clearInterval(o);
e.Platform.instance.clearTimeout(r);
n(t.adTag);
}
}, 200);
r = e.Platform.instance.setTimeout(function() {
e.Platform.instance.clearInterval(o);
i();
}, 3e4);
}
});
};
t.getUserValueSafe = function() {
var t = this;
return new Promise(function(n, i) {
if (t.mUserValue) n(t.mUserValue); else {
var o, r;
o = e.Platform.instance.setInterval(function() {
if (t.mUserValue) {
e.Platform.instance.clearInterval(o);
e.Platform.instance.clearTimeout(r);
n(t.mUserValue);
}
}, 200);
r = e.Platform.instance.setTimeout(function() {
e.Platform.instance.clearInterval(o);
i();
}, 5e3);
}
});
};
t.setAccountSource = function(n, i) {
var o = this;
return new Promise(function(r, a) {
var c = e.utils.httpParseQuery(e.Env.appLaunchOptions);
e.Env.mpsdkChannel || (e.Env.mpsdkChannel = "mpsdk");
e.Env.launchOptions = {
scene: e.Env.mpsdkChannel,
query: c
};
e.log("app launchOptions:", JSON.stringify(e.Env.launchOptions));
var s = e.utils.parseAccountSource(e.Env.launchOptions), d = e.Env.mpsdkImei ? e.Env.mpsdkImei : "", u = e.Env.mpsdkAndroidId ? e.Env.mpsdkAndroidId : "", l = e.Env.mpsdkOaid ? e.Env.mpsdkOaid : "", p = e.Env.mpsdkMac ? e.Env.mpsdkMac : "", f = "";
e.Env.launchOptions.query && e.Env.launchOptions.query.userid && (f = e.Env.launchOptions.query.userid);
o.getAccountSafe().then(function(o) {
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/setAccountLogin.action", {
gameId: e.Env.gameId,
openId: o.openid,
unionId: o.unionid,
sourceType: s.sourceType,
sourceId: s.sourceId,
imei: d,
sourceOpenId: f,
androidId: u,
oaId: l,
mac: p
}).then(function(o) {
e.log("上报用户来源成功", "sourceType=", n, " , sourceId=", i, " , imei=", d, " , android=", u);
e.log("setAccountSource res---", JSON.stringify(o));
t.setAccountForAccountLogin(o);
r();
});
}).catch(function() {
e.log("openid超时未就绪，上报用户来源失败");
a();
});
});
};
t.setAccountForAccountLogin = function(e) {
if (e) {
var n = this.getAccount();
e.sourceId && '"null"' != e.sourceId && "null" != e.sourceId && (n.sourceId = e.sourceId);
e.sourceType && '"null"' != e.sourceType && "null" != e.sourceType && (n.sourceType = e.sourceType);
e.sourceOpenId && '"null"' != e.sourceOpenId && "null" != e.sourceOpenId && (n.sourceOpenId = e.sourceOpenId);
e.unionid && '"null"' != e.unionid && "null" != e.unionid && (n.unionid = e.unionid);
n.createTime = e.createTime;
n.lastLoginTime = e.lastLoginTime;
e.ip && '"null"' != e.ip && "null" != e.ip && (n.ip = e.ip);
t.setAccount(n);
t.reportDeviceInfo(n);
}
};
t.setAccountInfo = function(t) {
if (!e.utils.deepCompare(e.Platform.instance.getStorage("user_info"), t)) {
e.Platform.instance.setStorage("user_info", t);
this.getAccountSafe().then(function(n) {
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/setAccountShow.action", {
gameId: e.Env.gameId,
openId: n.openid,
unionId: n.unionid,
show: encodeURIComponent(JSON.stringify(t))
}).then(function(t) {
e.log("上报用户信息成功");
});
}).catch(function() {
e.log("openid超时未就绪，上报用户信息失败");
});
}
return new Promise(function(e, t) {
e(!1);
});
};
t.setAccountProperty = function(t, n) {
this.getAccountSafe().then(function(i) {
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/updateAccount.action", {
gameid: e.Env.gameId,
userid: i.openid,
column: t,
value: n
}).then(function(i) {
e.log("更新账号属性成功", "key=", t, "val=", n);
});
}).catch(function() {
e.log("openid超时未就绪，更新账号属性失败");
});
};
t.setAccountActive = function(t, n) {
var i = "activeType_" + t;
e.Platform.instance.getStorage(i) != n ? this.getAccountSafe().then(function(o) {
var r = {
gameId: e.Env.gameId,
openId: o.openid
};
t === e.constant.ActiveType.GET_USER_INFO && (r.acceptStatus = n);
t === e.constant.ActiveType.START_GAME && (r.startGame = n);
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/setAccountStatus.action", r).then(function(t) {
e.Platform.instance.setStorage(i, n);
e.log("账号激活成功", JSON.stringify(r));
});
}).catch(function() {
e.log("openid超时未就绪，账号激活失败");
}) : e.log("重复激活，不再上报数据");
};
t.saveData = function(t, n) {
var i = this;
return new Promise(function(o, r) {
i.getAccountSafe().then(function(i) {
var a = {
gameId: e.Env.gameId,
openId: i.openid,
dataKey: t,
dataValue: n
};
e.Platform.instance.httpRequest(e.Env.loginServer + "/MiniGame/data/saveData.action", a).then(function(i) {
if (i && 0 == i.error) {
o();
e.log("更新账号数据成功", "key=", t, "val=", n);
} else {
r(i);
e.log("更新账号数据失败", JSON.stringify(i));
}
}).catch(function() {
r("网络错误");
});
}).catch(function() {
r("openid超时未就绪，更新账号数据失败");
e.log("openid超时未就绪，更新账号数据失败");
});
});
};
t.getData = function(n, i) {
return new Promise(function(o, r) {
t.getAccountSafe().then(function(t) {
var a = i || t.openid, c = {
gameId: e.Env.gameId,
openId: a,
dataKey: n
};
e.Platform.instance.httpRequest(e.Env.loginServer + "/MiniGame/data/getData.action", c).then(function(t) {
if (t && 0 == t.error) {
o(t.value);
e.log("获取账号数据成功", JSON.stringify(t));
} else {
r(t);
e.log("获取账号数据失败", JSON.stringify(t));
}
}).catch(function() {
r("网络错误");
});
}).catch(function() {
r("openid超时未就绪，更新账号数据失败");
e.log("openid超时未就绪，获取账号数据失败");
});
});
};
t.isOpenIdExist = function() {
return !!t.getAccount().openid;
};
t.isUnionIdExist = function() {
return !!t.getAccount().unionid;
};
t.getClientUUID = function() {
var t = e.Platform.instance.getStorage("sdk-clent-uuid");
return t || "";
};
t.clearClientUUID = function() {
e.Platform.instance.setStorage("sdk-clent-uuid", "");
};
t.getClientUserId = function(n) {
void 0 === n && (n = e.constant.NewUserLogEnum.ACTION_PT_LOGIN_END);
if (e.constant.NewUserLogEnum.ACTION_PT_LOGIN_END < n && t.isOpenIdExist()) return t.getOpenId();
var i = t.getClientUUID();
if (i) return i;
var o = e.utils.createClientUUID();
e.Platform.instance.setStorage("sdk-clent-uuid", o);
return o;
};
t.getOpenId = function() {
return t.getAccount().openid;
};
t.setAccountParameter = function(n, i) {
void 0 === i && (i = 1);
null != n && void 0 != n ? t.getAccountSafe().then(function(t) {
var o = {
gameId: e.Env.gameId,
openId: t.openid,
type: i,
parameter: n
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/setAccountPar.action", o).then(function(t) {
if (t && null != t.error && void 0 != t.error) if (0 == t.error) {
e.Platform.instance.setStorage("mpsdk_user_parameter" + i, n);
e.log("设置用户parameter信息成功。", JSON.stringify(o));
} else 1 == t.error ? e.log("设置用户parameter信息失败，type参数错误。", JSON.stringify(o)) : 1 == t.error ? e.log("设置用户parameter信息失败，账号不存在。", JSON.stringify(o)) : e.log("设置用户parameter信息失败。error:", JSON.stringify(t), " ;data:", JSON.stringify(o)); else e.log("设置用户parameter信息失败。", JSON.stringify(o));
});
}).catch(function() {
e.log("openid超时未就绪，设置用户parameter信息失败");
}) : e.log("设置用户扩展信息失败: params is null.");
};
t.setAccountParameter1 = function(e) {
t.setAccountParameter(e, 1);
};
t.setAccountParameter2 = function(e) {
t.setAccountParameter(e, 2);
};
t.getAccountParameters = function() {
return new Promise(function(n, i) {
var o = {
parameter1: 0,
parameter2: 0
}, r = e.Platform.instance.getStorage("mpsdk_user_parameter1"), a = e.Platform.instance.getStorage("mpsdk_user_parameter2");
if (r || a) {
o.parameter1 = Number(r || 0);
o.parameter2 = Number(a || 0);
n(o);
} else t.getAccountSafe().then(function(t) {
var i = {
gameId: e.Env.gameId,
openId: t.openid
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getAccountPar.action", i).then(function(t) {
if (t && null != t.error && void 0 != t.error) {
if (0 == t.error) {
e.log("获取用户parameters信息成功。", JSON.stringify(i));
r = t.parameter1;
a = t.parameter2;
} else 2 == t.error ? e.log("获取用户parameters失败，账号不存在。", JSON.stringify(i)) : e.log("获取用户parameters失败。error:", JSON.stringify(t), " ;data:", JSON.stringify(i));
e.Platform.instance.setStorage("mpsdk_user_parameter1", Number(r || 0));
e.Platform.instance.setStorage("mpsdk_user_parameter2", Number(r || 0));
o.parameter1 = Number(r || 0);
o.parameter2 = Number(r || 0);
n(o);
} else e.log("获取用户parameters信息失败。", JSON.stringify(i));
});
}).catch(function() {
e.log("openid超时未就绪，获取用户parameters信息失败");
n(o);
});
});
};
t.fetchAgTag = function() {
var n = this, i = "6";
t.getAccountSafe().then(function(t) {
var o = {
gameId: e.Env.gameId,
openId: t.openid
};
e.Platform.instance.httpRequest(e.Env.adTagServer + "/MiniVideoTag/data/requestTag/v1", o).then(function(t) {
if (t && null != t.code && void 0 != t.code) {
e.log("获取ADTAG数据成功", JSON.stringify(t));
i = t.code;
} else e.log("获取ADTAG数据失败,采用默认值", JSON.stringify(t));
n.setAdTag(i);
}).catch(function() {
e.log("获取ADTAG数据失败(网络错误),采用默认值");
n.setAdTag(i);
});
}).catch(function() {
e.log("openid超时未就绪,获取ADTAG数据失败,采用默认值");
n.setAdTag(i);
});
};
t.fetchSingleUserValue = function() {
var n = this, i = {
ARPU_UserAccumulativeEcpm: -1,
ARPU_UserPreviousEcpm: -1,
ARPU_UserRoi: -1
};
t.getAccountSafe().then(function(t) {
var o = {
gameId: e.Env.gameId,
openId: t.openid
};
e.Platform.instance.httpRequest(e.Env.userValueServer + "/MiniUserRoi/data/requestARPU/v1", o).then(function(t) {
if (!t || 0 != t.error && 200 != t.error) {
e.log("获取单用户价值数据失败", JSON.stringify(t));
n.setSingleUserValue(i);
} else {
e.log("获取单用户价值数据成功", JSON.stringify(t));
n.setSingleUserValue(t);
}
}).catch(function() {
e.log("获取单用户价值数据失败(网络错误)");
n.setSingleUserValue(i);
});
}).catch(function() {
e.log("openid超时未就绪,获取单用户价值数据失败");
n.setSingleUserValue(i);
});
};
t.setSingleUserValue = function(t) {
e.log("获取单用户价值数据 userValue = ", JSON.stringify(t));
this.mUserValue = t;
this.mUserValue.ARPU_UserAccumulativeEcpm = t.ARPU_UserAccumulativeEcpm;
this.mUserValue.ARPU_UserPreviousEcpm = t.ARPU_UserPreviousEcpm;
this.mUserValue.ARPU_UserRoi = t.ARPU_UserRoi;
};
t.fetchUserEarliestTime = function() {
var t = this;
return new Promise(function(n, i) {
var o = e.Env.mpsdkImei ? e.Env.mpsdkImei : "", r = e.Env.mpsdkAndroidId ? e.Env.mpsdkAndroidId : "", a = e.Env.mpsdkOaid ? e.Env.mpsdkOaid : "", c = e.Env.mpsdkMac ? e.Env.mpsdkMac : "";
e.Platform.instance.httpRequest(e.Env.userGYServer + "/sdk/get.do", {
gameId: e.Env.gameId,
imei: o,
androidId: r,
oaId: a,
mac: c
}, "post").then(function(i) {
e.log("获取用户最早出现时间", "gameId=", e.Env.gameId, " , oaId=", a, " , mac=", c, " , imei=", o, " , android=", r);
e.log("获取用户最早出现时间 res---", JSON.stringify(i));
var s = 0;
if (i && 0 == i.code) {
e.log("获取用户最早出现时间成功 time---", i.data);
s = Number(i.data);
t.mUserEarliestTime = Number(i.data);
} else e.log("获取用户最早出现时间失败 code---", i.code, ", msg--", i.message);
n(s);
});
});
};
t.fetchUserEarliestTimeSafe = function() {
var t = this;
if (this.isReportDeviceInfo) {
this.fetchUserEarliestTime();
this.isReportDeviceInfo = !1;
} else {
var n, i;
n = e.Platform.instance.setInterval(function() {
if (t.isReportDeviceInfo) {
t.fetchUserEarliestTime();
e.Platform.instance.clearInterval(n);
e.Platform.instance.clearTimeout(i);
t.isReportDeviceInfo = !1;
}
}, 100);
i = e.Platform.instance.setTimeout(function() {
e.Platform.instance.clearInterval(n);
}, 3e4);
}
};
t.reportDeviceInfo = function(n) {
return new Promise(function(i, o) {
e.Platform.instance.getDeviceInfo().then(function(r) {
var a = e.Env.mpsdkImei ? e.Env.mpsdkImei : "", c = e.Env.mpsdkAndroidId ? e.Env.mpsdkAndroidId : "", s = e.Env.mpsdkOaid ? e.Env.mpsdkOaid : "", d = e.Env.mpsdkMac ? e.Env.mpsdkMac : "";
e.log("上报设备信息 begin---");
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/setAccount4info.action", {
gameId: e.Env.gameId,
openId: n.openid,
imei: a,
androidId: c,
oaId: s,
mac: d,
createTime: n.createTime
}).then(function(o) {
e.log("上报设备信息", "openId=", n.openid, " , oaId=", s, " , mac=", d, " , imei=", a, " , android=", c, " , createTime=", n.createTime);
e.log("上报设备信息 res---", JSON.stringify(o));
t.isReportDeviceInfo = !0;
i();
}).catch(function(t) {
e.log("上报设备信息失败 ", t);
o();
});
}).catch(function(t) {
e.log("openid超时未就绪，上报设备信息失败 ", t);
o();
});
});
};
t.getUserEarliestTime = function() {
return this.mUserEarliestTime;
};
t.is7DayUser = function() {
var e = new Date().getTime(), t = this.mUserEarliestTime + 6048e5 <= e;
return 0 != this.mUserEarliestTime && !t;
};
t.getWXOpenId = function(t) {
return new Promise(function(n, i) {
e.log("获取微信开放平台openid begin---gameId=", e.Env.gameId, " , code=", t);
e.Platform.instance.httpRequest(e.Env.newLoginServer + "/MiniLogin/data/GetWxOPlatId.action", {
gameId: e.Env.gameId,
code: t
}).then(function(t) {
e.log("获取微信开放平台openid res---", t ? JSON.stringify(t) : "");
if (t && 0 == t.error) {
e.log("获取微信开放平台openid成功");
n({
openid: t.openid,
access_token: t.access_token,
refresh_token: t.refresh_token
});
} else if (t && 1001 == t.error) {
e.log("获取微信开放平台openid失败--无账号配置，检查ums");
i();
} else if (t && 1002 == t.error) {
e.log("获取微信开放平台openid失败--登录方式配置错误，不是微信开放平台登录，检查ums和其他错误");
i();
} else if (t && 2001 == t.error) {
e.log("获取微信开放平台openid失败--获取不到openid");
i();
} else {
e.log("获取微信开放平台openid失败--其他");
i();
}
}).catch(function(t) {
e.log("获取微信开放平台openid失败 err--", t);
i();
});
});
};
t.checkExternalAdUser = function() {
var t = this;
return new Promise(function(n, i) {
e.log("检查投放用户 begin---gameId=", e.Env.gameId, " , imei=", e.Env.mpsdkImei, " , oaid=", e.Env.mpsdkOaid, " , androidid=", e.Env.mpsdkAndroidId, " , mac=", e.Env.mpsdkMac);
var o = e.Platform.instance.getStorage("isExternalAdUser");
if (o && 1 == o) {
e.log("检查投放用户成功,是投放用户--cache");
n(!0);
} else if (o && 2 == o) {
e.log("检查投放用户成功,非投放用户--cache");
n(!1);
} else {
var r = t.getAccount().userAttr, a = e.utils.getAdUserStatus(r);
if ("0" != a) if ("1" != a) e.Platform.instance.httpRequest(e.Env.apiAdServer, {
gameid: e.Env.gameId,
imei: e.Env.mpsdkImei,
oaid: e.Env.mpsdkOaid,
androidid: e.Env.mpsdkAndroidId,
mac: e.Env.mpsdkMac
}).then(function(t) {
e.log("检查投放用户 res---", t ? JSON.stringify(t) : "");
if (t) if (2e3 == t.code) {
e.log("检查投放用户成功,是投放用户.");
e.Platform.instance.setStorage("isExternalAdUser", 1);
e.Report.reportAppUserAttr("4", "1", "");
n(!0);
} else {
e.log("检查投放用户成功,非投放用户. code = ", t.code, " , message = ", t.message);
e.Platform.instance.setStorage("isExternalAdUser", 2);
e.Report.reportAppUserAttr("4", "0", "");
n(!1);
} else {
e.log("检查投放用户成功,非投放用户.--无响应内容");
e.Platform.instance.setStorage("isExternalAdUser", 3);
n(!1);
}
}).catch(function(t) {
e.log("检查投放用户失败,非投放用户. 失败原因：", t);
e.Platform.instance.setStorage("isExternalAdUser", 3);
n(!1);
}); else {
e.log("检查投放用户成功,是投放用户--server");
n(!1);
} else {
e.log("检查投放用户成功,非投放用户--server");
n(!1);
}
}
});
};
t.adTag = "";
t.isReportDeviceInfo = !1;
t.mUserEarliestTime = 0;
return t;
}();
e.Account = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.updateUserValue = function(e) {
e > this.userValue && (this.userValue = e);
};
t.loadAdData = function() {
this.loadRecommendData();
this.loadSuggestData();
};
t.loadFilterData = function() {
var t = this;
return new Promise(function(n, i) {
e.Platform.instance.getUserInfo().then(function(i) {
n({
os: e.Platform.instance.getSystem().platform,
gender: Number(i.gender),
userValue: t.userValue
});
}).catch(function() {
n({
os: e.Platform.instance.getSystem().platform,
gender: -1,
userValue: t.userValue
});
});
});
};
t.loadRecommendData = function() {
this.recommendPromise || (this.recommendPromise = new Promise(function(t, n) {
var i = e.Platform.instance.getStorage("recommendList") || [];
e.Platform.instance.httpRequest(e.Env.cdnServer + "/ad/" + e.Env.gamePath + "/advlist.json").then(function(n) {
if ("object" == typeof n) {
t(n);
e.Platform.instance.setStorage("recommendList", n);
e.log("加载线上浮标广告成功", "共" + n.length + "条");
} else {
e.log("加载线上浮标广告失败，服务器返回错误", n);
t(i);
}
}).catch(function(n) {
e.log("加载线上浮标广告失败，HTTP错误", n);
t(i);
});
}));
return this.recommendPromise;
};
t.loadSuggestData = function() {
this.suggestPromise || (this.suggestPromise = new Promise(function(t, n) {
var i = e.Platform.instance.getStorage("suggestList") || [];
e.Platform.instance.httpRequest(e.Env.cdnServer + "/ad/" + e.Env.gamePath + "/version.json").then(function(n) {
if ("object" == typeof n) if (e.Platform.instance.getStorage("suggestVer") != n.ver) e.Platform.instance.httpRequest(e.Env.cdnServer + "/ad/" + e.Env.gamePath + "/" + n.ver).then(function(o) {
if ("object" == typeof o) {
t(o);
e.Platform.instance.setStorage("suggestVer", n.ver);
e.Platform.instance.setStorage("suggestList", o);
e.log("加载线上推荐列表成功", "共" + o.length + "条");
} else {
e.log("加载线上推荐列表失败，服务器返回错误", o);
t(i);
}
}).catch(function() {
e.log("加载线上推荐列表失败，HTTP错误");
t(i);
}); else {
e.log("线上推荐列表版本和本地一致，无需更新");
t(i);
} else {
e.log("检查推荐列表版本失败，服务器返回错误", n);
t(i);
}
}).catch(function(n) {
e.log("检查推荐列表版本失败，HTTP错误", n);
t(i);
});
}));
return this.suggestPromise;
};
t.getBoxDataList = function(t, n, i, o) {
void 0 === t && (t = "");
void 0 === n && (n = -1);
void 0 === i && (i = 0);
void 0 === o && (o = []);
return __awaiter(this, void 0, void 0, function() {
var r, a, c, s, d, u, l, p, f, h, g, m, v;
return __generator(this, function(y) {
switch (y.label) {
case 0:
if (this.boxData) return [ 2, this.boxData ];
r = "cache_data_list";
return [ 4, e.Platform.instance.httpRequest(e.Env.boxServer + "/v1/index/list?box_id=" + e.Env.gameId).then(function(t) {
t && e.Platform.instance.setStorage(r, t);
return t;
}).catch(function() {
return e.Platform.instance.getStorage(r);
}) ];

case 1:
if (!(a = y.sent()).category || !a.data) throw "获取数据失败";
for (c = 0, s = a.data; c < s.length; c++) {
d = s[c];
u = e.utils.getQueryString(d.page, "bridge_appid");
l = e.utils.getQueryString(d.page, "bridge_path");
if (u) {
d.page = l + "?" + d.page.replace("bridge_appid=" + u, "bridge2appid=" + d.appid).replace("bridge_path=" + l, "bridge2path=" + d.page.split("?")[0]).split("?")[1];
d.appid = u;
}
}
p = {
os: e.Platform.instance.getSystem().platform,
gender: n,
userValue: i
};
a.data = a.data.filter(function(e) {
if (e.platform && e.platform != p.os && "devtools" != p.os) return !1;
if (e.sex && 3 != parseInt(e.sex) && parseInt(e.sex) != p.gender) return !1;
if (e.uservalue && parseInt(e.uservalue) < p.userValue) return !1;
if (parseInt(e.table)) {
var t = parseInt(e.table);
return 100 * Math.random() < t;
}
return !0;
});
return [ 4, e.Hack.getOpenLevel(t) ];

case 2:
f = y.sent();
"221.237.157.133" != e.utils.value(f, "serverConfig.ipAddr", "") && (a.data = a.data.filter(function(t) {
return "2" != e.utils.value(t, "audi", "1");
}));
if (f.codeVer == t && "0" == f.status) {
h = [];
a.data = a.data.filter(function(e) {
if (-1 == o.indexOf(e.appid)) return !1;
if (-1 == h.indexOf(e.appid)) {
h.push(e.appid);
return !0;
}
return !1;
});
g = [ "4", "5", "7" ];
for (m = 0; m < g.length; m++) for (v = 4 * m; v < 4 * (m + 1) && v < a.data.length; v++) a.data[v].category = g[m];
}
a.category.forEach(function(e) {
e.children = a.data.filter(function(t) {
return t.category == e.category;
});
});
this.boxData = a;
this.reportShowBat(a.data);
return [ 2, a ];
}
});
});
};
t.getRecommendInfo = function(t) {
var n = this;
void 0 === t && (t = 0);
return new Promise(function(i, o) {
Promise.all([ n.loadRecommendData(), e.Hack.getOpenLevel(""), n.loadFilterData() ]).then(function(r) {
var a = r[0], c = r[1], s = r[2];
if ((a = n.dataFilter(a, s, c, "icon")).length) {
a = n.dataFixPip(a, s.gender, s.userValue);
(n.checkLimit(c, t) ? new Promise(function(e, t) {
n.loadSuggestData().then(function(t) {
var n = t.map(function(e) {
return e.aid;
});
(a = a.filter(function(e) {
return -1 != n.indexOf(e.aid);
})).sort(function(e, t) {
return parseInt(e.aid) - parseInt(t.aid);
});
e(a.slice(0, 3));
}).catch(function() {
e(a);
});
}) : Promise.resolve(a)).then(n.dataFixCacheRes).then(function(t) {
var o = e.utils.tableAlgorithm(t, "table");
n.reportShowBat([ o ]);
i(o);
});
} else o("没有符合条件的项目");
}).catch(function(t) {
o("获取浮标广告出错");
e.log("获取浮标广告出错", t);
});
});
};
t.getSuggestList = function(t, n, i, o) {
var r = this;
void 0 === t && (t = !0);
void 0 === n && (n = 0);
void 0 === i && (i = 0);
void 0 === o && (o = "");
return new Promise(function(a, c) {
Promise.all([ r.loadSuggestData(), e.Hack.getOpenLevel(""), r.loadFilterData() ]).then(function(s) {
var d = s[0], u = s[1], l = s[2];
if ((d = r.dataFilter(d, l, u, o)).length) {
d = r.dataFixPip(d, l.gender, l.userValue);
(r.checkLimit(u, i) ? new Promise(function(e, t) {
r.loadRecommendData().then(function(t) {
var n = t.map(function(e) {
return e.aid;
});
(d = d.filter(function(e) {
return -1 != n.indexOf(e.aid);
})).sort(function(e, t) {
return parseInt(e.aid) - parseInt(t.aid);
});
e(d.slice(0, 3));
}).catch(function() {
e(d);
});
}) : Promise.resolve(d)).then(r.dataFixCacheRes).then(function(i) {
var o = t ? i : e.utils.tableAlgorithm(i, "table", !1);
n && (o = o.slice(0, n));
r.reportShowBat(o);
a(o);
});
} else c("没有符合条件的项目");
}).catch(function(t) {
c("获取推荐列表出错");
e.log("获取推荐列表出错", t);
});
});
};
t.getBannerInfoList = function(t, n, i) {
var o = this;
void 0 === t && (t = !0);
void 0 === n && (n = 0);
void 0 === i && (i = 0);
return new Promise(function(r, a) {
Promise.all([ o.loadSuggestData(), e.Hack.getOpenLevel(""), o.loadFilterData() ]).then(function(c) {
var s = c[0], d = c[1], u = c[2];
if ((s = o.dataFilter(s, u, d, "mp_banner")).length) {
s = o.dataFixPip(s, u.gender, u.userValue);
(o.checkLimit(d, i) ? new Promise(function(e, t) {
t();
}) : Promise.resolve(s)).then(o.dataFixCacheRes).then(function(i) {
var a = t ? i : e.utils.tableAlgorithm(i, "table", !1);
n && (a = a.slice(0, n));
o.reportShowBat(a);
r(a);
}).catch(function(t) {
a("审核状态下,屏蔽banner广告.");
e.log("审核状态下,屏蔽banner广告.");
});
} else {
a("没有符合条件的项目");
e.log("没有符合条件的banner广告.");
}
}).catch(function(t) {
a("获取banner广告出错");
e.log("获取banner广告出错", t);
});
});
};
t.getBannerInfo = function(t) {
var n = this;
void 0 === t && (t = 0);
return new Promise(function(i, o) {
Promise.all([ n.loadSuggestData(), e.Hack.getOpenLevel(""), n.loadFilterData() ]).then(function(r) {
var a = r[0], c = r[1], s = r[2];
if ((a = n.dataFilter(a, s, c, "mp_banner")).length) {
a = n.dataFixPip(a, s.gender, s.userValue);
(n.checkLimit(c, t) ? new Promise(function(e, t) {
t();
}) : Promise.resolve(a)).then(n.dataFixCacheRes).then(function(t) {
var o = e.utils.tableAlgorithm(t, "table");
n.reportShowBat([ o ]);
i(o);
}).catch(function(t) {
o("审核状态下,屏蔽banner广告.");
e.log("审核状态下,屏蔽banner广告.");
});
} else {
o("没有符合条件的项目");
e.log("没有符合条件的banner广告.");
}
}).catch(function(t) {
o("获取banner广告出错");
e.log("获取banner广告出错", t);
});
});
};
t.getESADList = function(t, n, i) {
var o = this;
void 0 === t && (t = !0);
void 0 === n && (n = 0);
void 0 === i && (i = 0);
return new Promise(function(r, a) {
Promise.all([ o.loadSuggestData(), e.Hack.getOpenLevel(""), o.loadFilterData() ]).then(function(c) {
var s = c[0], d = c[1], u = c[2];
if ((s = o.dataFilter(s, u, d, "esa")).length) {
s = o.dataFixPip(s, u.gender, u.userValue);
(o.checkLimit(d, i) ? new Promise(function(e, t) {
t();
}) : Promise.resolve(s)).then(o.dataFixCacheRes).then(function(i) {
var a = t ? i : e.utils.tableAlgorithm(i, "table", !1);
n && (a = a.slice(0, n));
o.reportShowBat(a);
r(a);
}).catch(function(t) {
a("审核状态下,屏蔽电商广告.");
e.log("审核状态下,屏蔽电商广告.");
});
} else {
a("没有符合条件的项目");
e.log("没有符合条件的电商广告.");
}
}).catch(function(t) {
a("获取电商广告出错");
e.log("获取电商广告出错", t);
});
});
};
t.getExcitationList = function(e, t, n) {
void 0 === e && (e = !0);
void 0 === t && (t = 0);
void 0 === n && (n = 0);
return this.getSuggestList(e, t, n, "excitation");
};
t.checkLimit = function(t, n) {
var i = e.Hack.checkAdTimeLimit(t) && e.Hack.checkAdCityLimit(t) && e.Hack.checkAdScoreLimit(t, n);
e.log("审查时段", t.link_time, "审查城市", t.link_city, "审查分值", t.link_enable_level || "1");
e.log("当前时段", new Date().getHours(), "当前城市", e.utils.value(t, "ipArea.city"), "当前分值", n);
e.log("审查结果", i);
return i;
};
t.dataFilter = function(t, n, i, o) {
var r = this;
void 0 === o && (o = "");
return t.filter(function(t) {
if (t.platform && n.os && t.platform != n.os && "devtools" != n.os) return !1;
if (t.sex && 3 != t.sex && t.sex != n.gender) return !1;
if (t.audi && "2" == t.audi && (!i.ipAddr || "221.237.157.133" != i.ipAddr)) return !1;
if (r.checkCityInvisible(i, t.city_hide)) {
e.log("广告屏蔽城市过滤：", t.city_hide, " ; 被过滤广告id:", t.adid);
return !1;
}
if (!r.checkCityVisible(i, t.city_show)) {
e.log("广告可见城市过滤：", t.city_show, " ; 被过滤广告id:", t.adid);
return !1;
}
if (!e.utils.checkMultiOpenTimesLimit(t)) {
e.log("广告开放时段过滤：", t.openTimes, " ; 被过滤广告id:", t.adid);
return !1;
}
if (!r.checkOnlySevenDayUserVisible(i, t.only_seven_day_show)) {
e.log("广告仅7日用户可见过滤：only_seven_day_show:", t.only_seven_day_show, " ; force_only_seven_day_show:", i.force_only_seven_day_show, " ; 被过滤广告id:", t.adid);
return !1;
}
return ("banner" != o || 2 === t.category) && (("icon" != o || 2 === t.category) && (("video" != o || 3 === t.category) && (("excitation" != o || 12 === t.category) && (("mp_banner" != o || 13 === t.category) && (("esa" != o || 14 === t.category) && !(!o && 1 != t.category && 2 != t.category && 3 != t.category && 0 != t.category && 4 != t.category))))));
});
};
t.checkOnlySevenDayUserVisible = function(t, n) {
var i = !(!t.force_only_seven_day_show || "1" != t.force_only_seven_day_show);
return n && "1" == n ? !!e.Account.is7DayUser() : !i || !e.Account.is7DayUser();
};
t.checkCityVisible = function(t, n) {
var i = e.utils.value(t, "ipArea.province"), o = e.utils.value(t, "ipArea.city");
return (!n || "all" == n) && !e.utils.checkAbroadCity(t) || n && o && -1 != n.indexOf(o) || n && i && -1 != n.indexOf(i);
};
t.checkCityInvisible = function(t, n) {
var i = e.utils.value(t, "ipArea.province"), o = e.utils.value(t, "ipArea.city");
return "all" == n || e.utils.checkAbroadCity(t) || n && o && -1 != n.indexOf(o) || n && i && -1 != n.indexOf(i);
};
t.dataFixPip = function(t, n, i) {
for (var o = JSON.parse(JSON.stringify(t)), r = 0, a = o; r < a.length; r++) {
var c = a[r];
if (!c.start_time || !c.end_time || !c.plan_number) {
c.page += -1 == c.page.indexOf("?") ? "?" : "&";
c.page += "type=link&adid=" + c.adid + "&gender=" + n + "&uservalue=" + i + "&sgid=" + e.Env.gameId + "&gameid=" + c.aid;
}
var s = e.utils.getQueryString(c.page, "bridge_appid"), d = e.utils.getQueryString(c.page, "bridge_path");
if (s) {
c.page = d + "?" + c.page.replace("bridge_appid=" + s, "bridge2appid=" + c.appid).replace("bridge_path=" + d, "bridge2path=" + c.page.split("?")[0]).split("?")[1];
c.appid = s;
}
c.ad_image && 0 != c.ad_image.indexOf("http") && (c.ad_image = e.Env.cdnServer + "/ad/images/" + c.ad_image);
c.icon && 0 != c.icon.indexOf("http") && (c.icon = e.Env.cdnServer + "/ad/images/" + c.icon);
c.image && 0 != c.image.indexOf("http") && (c.image = e.Env.cdnServer + "/ad/images/" + c.image);
c.banner && 0 != c.banner.indexOf("http") && (c.banner = e.Env.cdnServer + "/ad/images/" + c.banner);
c.atlas_photo && 0 != c.atlas_photo.indexOf("http") && (c.atlas_photo = e.Env.cdnServer + "/atlas/" + c.atlas_photo);
c.atlas_config && 0 != c.atlas_config.indexOf("http") && (c.atlas_config = e.Env.cdnServer + "/atlas/" + c.atlas_config);
c.title_bg && 0 != c.title_bg.indexOf("http") && (c.title_bg = e.Env.cdnServer + "/bgtext/" + c.title_bg);
c.clicked = !this.checkClickRecord(Number(c.adid));
c.clicked && (c.table = Number(c.table) / 2);
}
return o;
};
t.dataFixCacheRes = function(t) {
return __awaiter(this, void 0, void 0, function() {
var n, i, o, r, a, c, s, d, u;
return __generator(this, function(l) {
switch (l.label) {
case 0:
n = 0;
l.label = 1;

case 1:
if (!(n < t.length)) return [ 3, 14 ];
if (!t[n].ad_image) return [ 3, 3 ];
i = t[n];
return [ 4, e.Platform.instance.getCacheRes(t[n].ad_image, !1) ];

case 2:
i.ad_image = l.sent();
l.label = 3;

case 3:
if (!t[n].icon) return [ 3, 5 ];
o = t[n];
return [ 4, e.Platform.instance.getCacheRes(t[n].icon, !1) ];

case 4:
o.icon = l.sent();
l.label = 5;

case 5:
if (!t[n].image) return [ 3, 7 ];
r = t[n];
return [ 4, e.Platform.instance.getCacheRes(t[n].image, !1) ];

case 6:
r.image = l.sent();
l.label = 7;

case 7:
if (!t[n].banner) return [ 3, 9 ];
a = t[n];
return [ 4, e.Platform.instance.getCacheRes(t[n].banner, !1) ];

case 8:
a.banner = l.sent();
l.label = 9;

case 9:
if (!(c = t[n].atlas_photo)) return [ 3, 11 ];
s = t[n];
return [ 4, e.Platform.instance.getCacheRes(c, !1) ];

case 10:
s.atlas_photo = l.sent();
l.label = 11;

case 11:
if (!(d = t[n].title_bg)) return [ 3, 13 ];
u = t[n];
return [ 4, e.Platform.instance.getCacheRes(d, !1) ];

case 12:
u.title_bg = l.sent();
l.label = 13;

case 13:
n++;
return [ 3, 1 ];

case 14:
return [ 2, t ];
}
});
});
};
t.reportShowBat = function(t) {
e.Env.init && e.Account.getAccountSafe().then(function(n) {
for (var i = [], o = [], r = [], a = 0, c = t; a < c.length; a++) {
var s = c[a];
i[i.length] = s.aid;
o[o.length] = s.adid || s.id;
r[r.length] = s.category.toString();
}
var d = {
gameid: e.Env.gameId,
userid: n.openid,
show_game_id: i.join(","),
show_ad_id: o.join(","),
param1: r.join(",")
};
e.Platform.instance.httpRequest(e.Env.reportServer2 + "/MiniGameLog/log/showEvent2.action", d).then(function(t) {
e.log("批量上报展示事件成功", "共" + i.length + "条数据", JSON.stringify(d));
});
}).catch(function() {
e.log("openid超时未就绪，上报展示日志失败");
});
};
t.reportShow = function(e) {
this.reportShowBat([ e ]);
};
t.reportClick = function(t, n) {
void 0 === n && (n = 0);
e.Env.init && e.Account.getAccountSafe().then(function(i) {
var o = {
gameid: e.Env.gameId,
userid: i.openid,
link_to_game_id: t.aid,
link_ad_id: t.adid || t.id,
param1: n
};
e.Platform.instance.httpRequest(e.Env.reportServer2 + "/MiniGameLog/log/linkEvent.action", o).then(function(n) {
e.log("上报点击事件成功", t.title, JSON.stringify(o));
});
}).catch(function() {
e.log("openid超时未就绪，上报点击事件失败");
});
};
t.reportAppClick = function(t, n) {
void 0 === n && (n = 0);
if (t) {
this.checkClickRecord(Number(t.adid), !0);
e.Platform.instance.setSkipParams(t);
e.Env.init && e.Account.getAccountSafe().then(function(i) {
var o = {
gameid: e.Env.gameId,
userid: i.openid,
link_to_game_id: t.aid,
link_ad_id: t.adid || t.id,
param1: n
};
e.Platform.instance.httpRequest(e.Env.reportServer2 + "/MiniGameLog/log/linkEvent.action", o).then(function(n) {
e.log("上报App点击事件成功", t.title, JSON.stringify(o));
});
}).catch(function() {
e.log("openid超时未就绪，上报App点击事件失败");
});
} else e.log("上报App点击事件失败,请传入app数据");
};
t.reportSkipEvent = function(t, n) {
void 0 === n && (n = 0);
e.Env.init && e.Account.getAccountSafe().then(function(i) {
var o = {
gameid: e.Env.gameId,
userid: i.openid,
link_to_game_id: t.aid,
link_ad_id: t.adid || t.id,
param1: n
};
e.Platform.instance.httpRequest(e.Env.reportServer2 + "/MiniGameLog/log/skipEvent.action", o).then(function(n) {
e.log("上报App跳转事件成功", t.title, JSON.stringify(o));
});
}).catch(function() {
e.log("openid超时未就绪，上报App跳转事件失败");
});
};
t.click = function(t, n, i) {
var o = this;
void 0 === n && (n = 0);
void 0 === i && (i = !1);
var r = new Promise(function(i, r) {
wx.getNetworkType({
success: function(a) {
if ("none" != a.networkType) {
o.reportClick(t, n);
e.Platform.instance.launchTo(t).then(function(e) {
i(o.checkClickRecord(Number(t.adid), !0));
}).catch(function(e) {
r();
});
} else {
wx.showToast({
title: "网络已断开，无法跳转...",
icon: "none"
});
r();
}
},
fail: function() {
r();
}
});
});
return i ? r : this.checkClickRecord(Number(t.adid), !0);
};
t.clickSync = function(t, n, i) {
var o = this;
void 0 === n && (n = 0);
var r = this.checkClickRecord(Number(t.adid), !0), a = !1;
wx.getNetworkType({
success: function(c) {
if ("none" == c.networkType) {
wx.showToast({
title: "网络已断开，无法跳转...",
icon: "none"
});
a = !1;
if (i) {
i(a, r);
return;
}
} else {
o.reportClick(t, n);
e.Platform.instance.launchToSync(t, function(e) {
a = e;
i && i(a, r);
});
}
},
fail: function() {
a = !1;
i && i(a, r);
}
});
return r;
};
t.checkClickRecord = function(t, n) {
void 0 === n && (n = !1);
var i = !1, o = e.Platform.instance.getStorage("adClickRecord") || [];
if (-1 == o.indexOf(t)) {
i = !0;
if (n) {
o.push(t);
e.Platform.instance.setStorage("adClickRecord", o);
}
}
return i;
};
t.trackAdConversion = function(t, n) {
var i = this;
e.Env.init && e.Account.getAccountSafe().then(function(o) {
var r = "";
0 == o.lastLoginTime ? r = "REGISTER" : i.checkSecondaryRetention(o.createTime) && (r = "START_APP");
if (r) {
var a = "", c = "gdt_vid=" + t.gdt_vid + "&weixinadinfo=" + t.weixinadinfo, s = 0;
if (t.weixinadinfo) {
s = t.weixinadinfo.split(".")[0];
}
console.log("来源广告的广告id是:" + s);
a = n ? n + "&" + c : c;
var d = e.Env.gameId, u = o.openid, l = "";
e.Env.launchOptions.query.adid && (l = e.Env.launchOptions.query.adid);
var p = {
product_id: d,
openid: u,
action_type: r,
url: a,
source: l
};
e.log("上报广告转化事件参数", p);
e.Platform.instance.httpRequest(e.Env.trackAdServer + "/v1/adv/return", p, "post").then(function(t) {
e.log("上报广告转化事件成功", t, JSON.stringify(p));
});
}
}).catch(function() {
e.log("openid超时未就绪，上报广告转化事件失败");
});
};
t.checkSecondaryRetention = function(e) {
var t = new Date(), n = t.getFullYear(), i = t.getMonth() + 1 < 10 ? "0" + (t.getMonth() + 1) : t.getMonth() + 1, o = t.getDate() < 10 ? "0" + t.getDate() : t.getDate(), r = n + "/" + i + "/" + o + " 00:00:00", a = n + "/" + i + "/" + o + " 23:59:59", c = Date.parse(r) - 864e5, s = Date.parse(a) - 864e5;
return e >= c && e <= s;
};
t.showRewardedVideoAdDialog = function(n, i) {
void 0 === n && (n = "️️️⬇⬇⬇点击下方绿色按钮获得3倍奖励");
void 0 === i && (i = 5);
e.Env.isClickRewardedVideoAdDialogOk = !1;
e.Env.isRewardedVideoAdDialogShow = !1;
e.Env.isRewardedVideoAd2Back = !1;
Promise.all([ e.Hack.getOpenLevel("") ]).then(function(o) {
var r = o[0];
if ("0" != r.level) {
if (e.Hack.isStrongFission() && !e.Hack.checkCityLimit(r, "video_ad_city") && t.checkVideoAdShowByHitProb(r)) {
var a = {
content: n,
success: function(t) {
e.Env.isClickRewardedVideoAdDialogOk = !0;
if (t.confirm) {
e.Report.reportEvent(123463);
e.log("上报激励广告提示框点击确认事件,ID=123463");
} else if (t.cancel) {
e.Report.reportEvent(123462);
e.log("上报激励广告提示框点击取消事件,ID=123462");
}
},
fail: function(t) {
e.log("激励广告确认框事件：fail:", t);
}
};
t.crrentTimeout = e.Platform.instance.setTimeout(function() {
e.Env.isRewardedVideoAdDialogShow = !0;
e.Report.reportEvent(123461);
e.log("上报激励广告弹出提示框事件,ID=123461");
e.Platform.instance.showModal(a);
}, 1e3 * i);
}
} else e.log("level=0；激励广告提示框不会弹出。");
});
};
t.getRewardedVideoAdClickStatus = function() {
if (this.crrentTimeout) {
e.Platform.instance.clearTimeout(this.crrentTimeout);
this.crrentTimeout = null;
}
e.Env.isRewardedVideoAdDialogShow = !1;
if (!e.Env.isClickRewardedVideoAdDialogOk) return !1;
e.Env.isClickRewardedVideoAdDialogOk = !1;
return e.Env.isRewardedVideoAd2Back;
};
t.checkVideoAdShowByHitProb = function(t) {
if (!t.video_ad_show_prob) return !1;
if ("0" == t.video_ad_show_prob) {
e.log("激励广告提示框弹出概率；概率:0");
return !1;
}
if ("100" == t.video_ad_show_prob) {
e.log("激励广告提示框弹出概率；概率:100");
return !0;
}
if (isNaN(t.video_ad_show_prob)) return !1;
var n = 100 * Math.random(), i = Number(t.video_ad_show_prob);
e.log("激励广告提示框弹出概率；概率:", i, ";目标值:", n);
return n <= i;
};
t.userValue = 0;
return t;
}();
e.Ad = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function e() {}
e.assert = function(t) {
for (var n = [], i = 1; i < arguments.length; i++) n[i - 1] = arguments[i];
if (!t && this.bIsDebug) {
var o = "EngineAssert";
if (n.length > 0) {
var r = n[0];
n.shift();
o = e.fmt(r, n);
}
console.error(o);
throw new Error(o);
}
};
e.fmt = function(e) {
for (var t = [], n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
return t.length > 0 ? vsprintf(e, t) : e;
};
e.EncodeNumberToUtf8Character = function(t) {
var n = "";
if (t > 20901) {
var i = String(Math.floor(t / 20901)), o = String(t % 20901), r = Number(i), a = Number(o);
e.assert(r <= 20901);
e.assert(a <= 20901);
n = String.fromCharCode(19968 + r) + String.fromCharCode(19968 + a);
} else n = String.fromCharCode(19968 + t);
return n;
};
e.DecodeUtf8CharacterToNumber = function(e) {
var t = -1;
if (1 == e.length) {
t = e.charCodeAt(0) - 19968;
} else if (2 == e.length) {
var n = e.charCodeAt(0) - 19968, i = e.charCodeAt(1) - 19968;
t = 20901 * Number(String(n)) + Number(String(i));
}
return t;
};
e.bIsDebug = !0;
e.randomSeed = 0;
e.enablePerformDetect = !1;
e.performData = [];
e.lastResetTime = 0;
e.silencePerformMode = !0;
e.renderSubmitLog = !1;
e.InitCallback = [];
e.enableMultiTextureSampler = !0;
e.enableSpriteOutline = !0;
e.enableHitRectOutline = !0;
return e;
}();
e.EngineUtility = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.outRequest = function() {
this.outPermission = !0;
};
Object.defineProperty(t, "outCheckEnable", {
set: function(e) {
if (e) {
wx.offHide(this.outCheck.bind(this));
wx.onHide(this.outCheck.bind(this));
} else wx.offHide(this.outCheck.bind(this));
},
enumerable: !0,
configurable: !0
});
t.outCheck = function() {
if ("wx" == e.Platform.platformType && !wx.redirectTo) if (this.outPermission) this.outPermission = !1; else for (;!this.outPermission; ) wx.exitMiniProgram({
success: function(t) {
e.log(t);
},
fail: function(t) {
e.log(t);
}
});
};
t.getOpenLevel = function(t, n) {
var i = this;
e.Env.codeVer = t || "";
n = n || e.Env.gameId;
var o = new Promise(function(o, r) {
if (n) {
if (!i.serverConfigPromise) {
var a = e.Account.getAccount().ip;
e.log("获取功能限制级别--IP=", a);
if (a) {
e.log("获取功能限制级别--注册IP");
i.serverConfigPromise = e.Platform.instance.httpRequest(e.Env.loginServer + "/MiniGame/data/getIp.action?gameId=" + n + "&realIp=" + a);
} else {
e.log("获取功能限制级别--当前IP");
i.serverConfigPromise = e.Platform.instance.httpRequest(e.Env.loginServer + "/MiniGame/data/getIp.action?gameId=" + n);
}
}
i.serverConfigPromise.then(function(n) {
if (n.open) {
var i;
try {
(i = JSON.parse(n.open)).ipArea = {
country: e.utils.value(n, "country", ""),
province: e.utils.value(n, "province", ""),
city: e.utils.value(n, "city", "")
};
i.ipAddr = e.utils.value(n, "ip", "");
} catch (t) {
e.log("获取功能限制级别失败，默认启用高级限制保护（解析服务端参数错误）");
o({
level: "0"
});
e.Env.openLevelData = {
level: "0"
};
return;
}
if (i.codeVer != t || "0" != i.status) if (0 !== n.check) {
var r = new Date().getHours();
for (var a in i) {
var c = /^time(\d+)-(\d+)$/.exec(a);
if (c && "0" == i[a] && Number(c[1]) <= r && r <= Number(c[2])) {
e.log("当前时段受到中级限制保护");
i.level = "1";
o(i);
e.Env.openLevelData = i;
return;
}
}
e.log("当前处于低级保护");
i.level = "2";
o(i);
e.Env.openLevelData = i;
} else {
e.log("当前地域受到高级限制保护");
i.level = "0";
o(i);
e.Env.openLevelData = i;
} else {
e.log("当前应用版本受到高级限制保护");
i.level = "0";
o(i);
e.Env.openLevelData = i;
}
} else {
e.log("获取功能限制级别失败，默认启用高级限制保护（服务端参数未设置）");
o({
level: "0"
});
e.Env.openLevelData = {
level: "0"
};
}
}).catch(function() {
e.log("获取功能限制级别失败，默认启用高级限制保护（拉取服务端配置失败）");
o({
level: "0"
});
e.Env.openLevelData = {
level: "0"
};
});
} else {
e.log("获取功能限制级别失败，默认启用高级限制保护（请先初始化SDK或传入gameId参数）");
o({
level: "0"
});
e.Env.openLevelData = {
level: "0"
};
}
});
t && o.then(function(e) {
i.appStatus = e;
return e;
});
return o;
};
t.checkAdTimeLimit = function(e) {
var t = !1;
if (e.link_time) {
var n = e.link_time.indexOf("_") > 0 ? e.link_time.split("_", 2) : e.link_time.split("-", 2), i = n[0], o = n[1], r = new Date().getHours();
parseInt(i || "0") <= r && r <= parseInt(o || "24") && (t = !0);
}
return t;
};
t.checkAdCityLimit = function(t) {
var n = e.utils.value(t, "ipArea.province"), i = e.utils.value(t, "ipArea.city");
return "all" == t.link_city || e.utils.checkAbroadCity(t) || t.link_city && i && -1 != t.link_city.indexOf(i) || t.link_city && n && -1 != t.link_city.indexOf(n);
};
t.checkAdScoreLimit = function(e, t) {
return t < parseInt(e.link_enable_level || "1");
};
t.isStrongFission = function(t) {
void 0 === t && (t = "wxcff7381e631cf54e");
if (e.Env.launchOptions && "1037" == e.Env.launchOptions.scene && e.Env.launchOptions.referrerInfo && t == e.Env.launchOptions.referrerInfo.appId) {
e.log("小游戏助手进入，不可强烈变");
return !1;
}
if (e.Env.launchOptions && 1048 == e.Env.launchOptions.scene && (!e.Env.launchOptions.query || !e.Env.launchOptions.query.type)) {
e.log("本地强烈变检查：长按图片识别小程序码进入, 不可强烈变；当前场景值：", e.Env.launchOptions.scene);
return !1;
}
var n = [ 1e3, 1005, 1006, 1011, 1012, 1013, 1017, 1025, 1027, 1030, 1031, 1032, 1042, 1047, 1049, 1053, 1054, 1084, 1106, 2001, 2970 ];
if (!(-1 == n.indexOf(e.Env.launchOptions.scene))) {
e.log("本地强烈变检查：不可强烈变；当前场景值：", e.Env.launchOptions.scene);
return !1;
}
var i = e.Account.getAccount().sourceId ? Number(e.Account.getAccount().sourceId) : 0;
if (1048 == i) {
e.log("服务器端强烈变检查：长按图片识别小程序码进入，不可强烈变", " ; 当前服务器端场景值：", i, " ; 当前本地场景值：", e.Env.launchOptions.scene);
return !1;
}
var o = -1 == n.indexOf(i);
e.log("服务器端强烈变检查：强烈变状态：", o, " ; 当前服务器端场景值：", i, " ; 当前本地场景值：", e.Env.launchOptions.scene);
return o;
};
t.checkShareCityLimit = function(t) {
var n = e.utils.value(t, "ipArea.province"), i = e.utils.value(t, "ipArea.city");
return "all" == t.sharemodecity || e.utils.checkAbroadCity(t) || t.sharemodecity && i && -1 != t.sharemodecity.indexOf(i) || t.sharemodecity && n && -1 != t.sharemodecity.indexOf(n);
};
t.checkCityLimit = function(t, n) {
var i = e.utils.value(t, "ipArea.province"), o = e.utils.value(t, "ipArea.city");
e.log("目标城市：", t[n], " ; 当前城市：", o);
return "all" == t[n] || e.utils.checkAbroadCity(t) || t[n] && o && -1 != t[n].indexOf(o) || t[n] && i && -1 != t[n].indexOf(i);
};
t.onHide = function(t) {
e.log("[onHide] hideOptions : ", t);
var n = t.options;
if (n && n.targetAction && 8 == n.targetAction && n.targetPagePath) {
if (-1 != [ "NewWAProfileViewController", "com.tencent.mm.plugin.appbrand.ui.AppBrandProfileUI" ].indexOf(n.targetPagePath)) {
var i = e.Env.openLevelData;
if (i) {
if (i.ipAddr) {
var o = new Date();
this.lastOnHideTime = o.getTime();
this.isIntoAboutPage = !0;
e.Report.reportEvent(12345, i.ipAddr);
e.log("上报用户进入关于页面事件,ID=12345 ， ip:", i.ipAddr);
}
}
}
}
};
t.onShow = function() {
if (t.isIntoAboutPage) {
t.isIntoAboutPage = !1;
var n = e.Env.openLevelData;
if (n && n.ipAddr) {
var i = (new Date().getTime() - t.lastOnHideTime) / 1e3;
e.Report.reportEvent(123451, n.ipAddr, i + "");
e.log("上报用户进入关于页面事件,ID=123451 ， ip:", n.ipAddr, ", 耗时[s]：", i);
}
}
};
t.checkHideStatus = function(e) {
if (!e.sdk_hide_status || "0" == e.sdk_hide_status) return !1;
if ("1" == e.sdk_hide_status && !e.sdk_hide_time) return !0;
var t = new Date().getHours(), n = e.sdk_hide_time.split("-"), i = Number(n[0]), o = n[1] ? Number(n[1]) : 24;
return !(t < i || t > o);
};
t.checkRewardedVideoAd2Back = function(t) {
if (t && e.Env.isRewardedVideoAdDialogShow) if (t.targetPagePath) {
e.Env.isRewardedVideoAd2Back = !0;
e.Env.isRewardedVideoAdDialogShow = !1;
e.Report.reportEvent(123464);
e.log("上报激励广告切换后台事件,ID=123464");
} else if ("ios" == e.Platform.instance.getSystem().platform && !t.targetPagePath) {
e.Env.isRewardedVideoAd2Back = !0;
e.Env.isRewardedVideoAdDialogShow = !1;
e.Report.reportEvent(123464);
e.log("上报激励广告切换后台事件,ID=123464");
}
};
t.getOpenStatus = function(t, n, i) {
var o = this;
void 0 === i && (i = !1);
return new Promise(function(r, a) {
var c = new Date().getTime();
e.log("获取开放状态currTime=", c, ", lastFetchTime=", o.lastFetchTime, " , lastOpenStatus=", o.lastOpenStatus, " ,force=", i);
i && (o.lastForce = !0);
if (!i && o.lastOpenStatus && o.lastFetchTime > 0 && c - o.lastFetchTime < 6e4) {
e.log("获取开放状态，lastOpenStatus = ", o.lastOpenStatus);
r({
status: o.lastOpenStatus
});
} else if (t) if (n) {
if (!i && o.lastForce) {
e.log("获取开放状态，lastOpenStatus1 = ", o.lastOpenStatus);
r({
status: o.lastOpenStatus
});
}
o.lastFetchTime = c;
e.log("获取开放状态gameId=", t, ", openId=", n);
e.Platform.instance.httpRequest(e.Env.openStatusServer + "/MiniUserSwitchTagApp/data/requestShieldingResult/v1?gameId=" + t + "&openId=" + n).then(function(t) {
if (t) {
try {
e.log("获取开放状态res--", JSON.stringify(t));
} catch (e) {}
if (0 == t.error) {
var n = t.data.totalResult, i = t.data.msg, a = t.data.concretenessResult;
e.log("获取开放状态成功，status=", n, ", concretenessResult=", a, ", msg=", i);
o.lastOpenStatus = n + "";
o.lastForce = !1;
e.Report.reportEvent(88007, n + "_" + a);
r({
status: n
});
} else {
e.log("获取开放状态失败，默认关闭, error=", t.error);
o.lastForce = !1;
r({
status: "0"
});
}
} else {
e.log("获取开放状态失败，默认关闭（服务端参数未设置）");
o.lastForce = !1;
r({
status: "0"
});
}
}).catch(function() {
e.log("获取开放状态失败，默认关闭（拉取服务端配置失败）");
o.lastForce = !1;
r({
status: "0"
});
});
} else {
e.log("获取开放状态失败，默认关闭（请传入openId参数）");
r({
status: "0"
});
} else {
e.log("获取开放状态失败，默认关闭（请传入gameId参数）");
r({
status: "0"
});
}
});
};
t.forceFetchOpenStatus = function() {
try {
t.getOpenStatus(e.Env.gameId + "", e.Account.getOpenId(), !0);
} catch (e) {}
};
t.outPermission = !1;
t.isIntoAboutPage = !1;
t.lastOnHideTime = 0;
t.lastFetchTime = 0;
t.lastOpenStatus = "";
t.lastForce = !1;
return t;
}();
e.Hack = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {
this.mpsdkOs = "";
}
t.prototype.writeFile = function(e, t) {
return "";
};
t.prototype.getStorage = function(t) {
try {
var n = window.localStorage.getItem(e.Env.storagePrefix + t);
return n ? JSON.parse(n) : "";
} catch (e) {
return "";
}
};
t.prototype.setStorage = function(t, n) {
window.localStorage.setItem(e.Env.storagePrefix + t, JSON.stringify(n));
};
t.prototype.httpRequest = function(e, t, n, i) {
var o = this;
void 0 === n && (n = "get");
void 0 === i && (i = 2);
var r = "";
if (t) {
for (var a in t) r += a + "=" + t[a] + "&";
if (r) {
r = r.substr(0, r.length - 1);
if ("get" == n) {
e += -1 == e.indexOf("?") ? "?" : "&";
e += r;
}
}
}
return new Promise(function(t, i) {
var a = new Laya.HttpRequest();
a.once(Laya.Event.COMPLETE, o, function(e) {
try {
t(JSON.parse(e));
} catch (n) {
t(e);
}
});
a.once(Laya.Event.ERROR, o, function(e) {});
"get" == n ? a.send(e, "", "get", "text") : a.send(e, r, "post", "text");
});
};
t.prototype.writeOpenId2Native = function(e) {
if (e && e.openid) try {
"Conch-android" == this.mpsdkOs ? this.MpsdkNativeUtils.call("getNativeAppOpenId", e.openid) : "Conch-ios" == this.mpsdkOs && this.MpsdkNativeUtils.call("getNativeAppOpenId:", e.openid);
} catch (e) {}
};
t.prototype.getNativeAppOpenId = function() {
var e = "";
try {
"Conch-android" == this.mpsdkOs ? e = this.MpsdkNativeUtils.call("getNativeAppOpenId", "") : "Conch-ios" == this.mpsdkOs && (e = this.MpsdkNativeUtils.call("getNativeAppOpenId:", ""));
} catch (e) {}
return e;
};
t.prototype.getUserAccount = function(t) {
var n = this;
return new Promise(function(i, o) {
if (e.Account.getOpenId()) e.utils.nativeAppLogin(t).then(function(e) {
n.writeOpenId2Native(e);
i(e);
}).catch(function(e) {
o(e);
}); else {
var r = n.getNativeAppOpenId();
e.log("laya receive message--getNativeAppOpenId : ", r);
if (r) {
var a = {
openid: r
};
e.Account.setAccount(a);
}
e.utils.nativeAppLogin(t).then(function(e) {
n.writeOpenId2Native(e);
i(e);
}).catch(function(e) {
o(e);
});
}
});
};
t.prototype.getUserInfo = function() {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(e) {
return [ 2, {
nickName: "TestNickName",
avatarUrl: "",
gender: -1
} ];
});
});
};
t.prototype.getSystem = function() {
return {
platform: "debugPlatform"
};
};
t.prototype.getLaunchOptions = function() {
return {
scene: 0,
query: {}
};
};
t.prototype.launchTo = function(t) {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(n) {
e.log("打开其他程序，appId=", t.appid, "path=", t.page);
return [ 2 ];
});
});
};
t.prototype.launchToSync = function(t, n) {
return __awaiter(this, void 0, void 0, function() {
var i, o;
return __generator(this, function(r) {
if (t) try {
i = {
appid: t.appid,
page: e.Env.landingPageServer + t.page
};
e.log("打开其他程序，appinfo =", JSON.stringify(i));
o = this.MpsdkNativeUtils.call("clickApp", JSON.stringify(i));
e.log("isJumpOk==", o);
n("1" == o);
} catch (t) {
n(!1);
e.log("launch2Sync error==", t);
}
return [ 2 ];
});
});
};
t.prototype.setSkipParams = function(t) {
if (t && t.page) {
e.log("设置app跳转参数--appinfo =", JSON.stringify(t));
try {
var n = "";
n = t.page.toString().split("?")[1] || t.page.toString();
e.log("设置app跳转参数--query1 =", n);
if (-1 == n.indexOf("=")) {
e.log("设置app跳转参数--无查询参数");
return;
}
n = "qmkey" + e.utils.base64encode(n);
e.log("设置app跳转参数--query2 =", n);
"Conch-android" == this.mpsdkOs ? this.MpsdkNativeUtils.call("setSkipParams", n) : "Conch-ios" == this.mpsdkOs && this.MpsdkNativeUtils.call("setSkipParams:", n);
} catch (t) {
e.log("laya setSkipParams error--", t);
}
}
};
t.prototype.share = function(t, n, i, o) {
e.log("测试分享，success和fail都调用，方便调试");
n && n.call(o);
i && i.call(o);
};
t.prototype.setTimeout = function(e, t) {
return setTimeout(e, t);
};
t.prototype.setInterval = function(e, t) {
return setInterval(e, t);
};
t.prototype.clearTimeout = function(e) {
clearTimeout(e);
};
t.prototype.clearInterval = function(e) {
clearInterval(e);
};
t.prototype.getCacheRes = function(e, t) {
void 0 === t && (t = !1);
return Promise.resolve(e);
};
t.prototype.checkBannerClick = function(e) {
return !1;
};
t.prototype.onHide = function(t) {
void 0 === t && (t = "");
try {
if (window.conchConfig) {
this.mpsdkOs = window.conchConfig.getOS();
if ("Conch-android" == this.mpsdkOs) {
this.MpsdkNativeUtils = window.PlatformClass.createClass("com.qmo.game.mpsdk.utils.MpsdkNativeUtils");
var n = this.MpsdkNativeUtils.call("getPhoneModel");
e.log("phoneModel==", n);
-1 != n.indexOf("Exception") && (n = "");
if (n) {
var i = JSON.parse(n);
e.log("deviceInfo[oiad]==", i.oaid);
e.log("deviceInfo[androidId]==", i.androidId);
e.log("deviceInfo[mode]==", i.mode);
e.log("deviceInfo[imei]==", i.imei);
e.log("deviceInfo[mac]==", i.mac);
e.Env.phoneModel = i.mode;
e.Env.mpsdkImei = i.imei;
e.Env.mpsdkAndroidId = i.androidId;
e.Env.mpsdkMac = i.mac;
e.Env.mpsdkOaid = i.oaid;
}
try {
t && this.MpsdkNativeUtils.call("getNativeAppOpenId2", t);
} catch (e) {}
e.Env.mpsdkChannel = this.MpsdkNativeUtils.call("getMetaDataByKey", "MPSDK_CHANNEL");
e.Env.appLaunchOptions = this.MpsdkNativeUtils.call("getAppLaunchOptions");
-1 != e.Env.appLaunchOptions.indexOf("Exception") && (e.Env.appLaunchOptions = "");
-1 != e.Env.mpsdkAndroidId.indexOf("Exception") && (e.Env.mpsdkAndroidId = "");
-1 != e.Env.mpsdkImei.indexOf("Exception") && (e.Env.mpsdkImei = "");
-1 != e.Env.phoneModel.indexOf("Exception") && (e.Env.phoneModel = "");
-1 != e.Env.mpsdkChannel.indexOf("Exception") && (e.Env.mpsdkChannel = "mpsdk");
} else if ("Conch-ios" == this.mpsdkOs) {
this.MpsdkNativeUtils = window.PlatformClass.createClass("MpsdkNativeUtils");
e.Env.phoneModel = this.MpsdkNativeUtils.call("getPhoneModel") ? this.MpsdkNativeUtils.call("getPhoneModel") : "";
e.Env.mpsdkChannel = this.MpsdkNativeUtils.call("getMetaDataByKey:", "MPSDK_CHANNEL") ? this.MpsdkNativeUtils.call("getMetaDataByKey:", "MPSDK_CHANNEL") : "mpsdk";
e.Env.mpsdkImei = this.MpsdkNativeUtils.call("getMetaDataByKey:", "MPSDK_IMEI") ? this.MpsdkNativeUtils.call("getMetaDataByKey:", "MPSDK_IMEI") : "";
e.Env.appLaunchOptions = this.MpsdkNativeUtils.call("getAppLaunchOptions") ? this.MpsdkNativeUtils.call("getAppLaunchOptions") : "";
}
}
} catch (e) {}
};
t.prototype.showModal = function(e) {};
t.prototype.handleAfterLogin = function(e, t) {};
t.prototype.sendCreateTime2Native = function(t) {
try {
"Conch-android" == this.mpsdkOs ? this.MpsdkNativeUtils.call("setAccountCreateTime", t) : "Conch-ios" == this.mpsdkOs && this.MpsdkNativeUtils.call("setAccountCreateTime:", t);
} catch (t) {
e.log("laya sendCreateTime2Native error--", t);
}
};
t.prototype.sendLastLoginTime2Native = function(t) {
try {
"Conch-android" == this.mpsdkOs ? this.MpsdkNativeUtils.call("setAccountLastLoginTime", t) : "Conch-ios" == this.mpsdkOs && this.MpsdkNativeUtils.call("setAccountLastLoginTime:", t);
} catch (t) {
e.log("laya sendLastLoginTime2Native error--", t);
}
};
t.prototype.getAppHotLaunchOptions = function() {
var t = this;
return new Promise(function(n, i) {
try {
if (window.conchConfig) {
t.mpsdkOs = window.conchConfig.getOS();
if ("Conch-android" == t.mpsdkOs) {
e.Env.appLaunchOptions = t.MpsdkNativeUtils.call("getAppLaunchOptions");
-1 != e.Env.appLaunchOptions.indexOf("Exception") && (e.Env.appLaunchOptions = "");
} else "Conch-ios" == t.mpsdkOs && (e.Env.appLaunchOptions = t.MpsdkNativeUtils.call("getAppLaunchOptions") ? t.MpsdkNativeUtils.call("getAppLaunchOptions") : "");
}
} catch (e) {}
var o = e.utils.httpParseQuery(e.Env.appLaunchOptions);
e.Env.launchOptions = {
scene: e.Env.mpsdkChannel,
query: o
};
n(e.Env.appLaunchOptions);
});
};
t.prototype.getDeviceInfo = function() {
var t = this;
return new Promise(function(n, i) {
var o = "";
try {
if (window.conchConfig) {
t.mpsdkOs = window.conchConfig.getOS();
if ("Conch-android" == t.mpsdkOs) {
t.MpsdkNativeUtils = window.PlatformClass.createClass("com.qmo.game.mpsdk.utils.MpsdkNativeUtils");
o = t.MpsdkNativeUtils.call("getPhoneModel");
e.log("phoneModel==", o);
-1 != o.indexOf("Exception") && (o = "");
if (o) {
var r = JSON.parse(o);
e.log("getDeviceInfo[oiad]==", r.oaid);
e.log("getDeviceInfo[androidId]==", r.androidId);
e.log("getDeviceInfo[mode]==", r.mode);
e.log("getDeviceInfo[imei]==", r.imei);
e.log("getDeviceInfo[mac]==", r.mac);
e.Env.phoneModel = r.mode;
e.Env.mpsdkImei = r.imei;
e.Env.mpsdkAndroidId = r.androidId;
e.Env.mpsdkMac = r.mac;
e.Env.mpsdkOaid = r.oaid;
}
}
}
n(o);
} catch (t) {
e.log("getDeviceInfo error==", t);
n("");
}
});
};
return t;
}();
e.LayaPlatform = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.uploadCustomizeQuiz = function(t, n) {
n ? e.Account.getAccountSafe().then(function(i) {
var o = {
gameId: e.Env.gameId,
openId: i.openid,
show: t || "",
quiz: n
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/uploadQuiz.action", o, "POST").then(function(t) {
e.log("上传自定义题目成功", JSON.stringify(o));
});
}).catch(function() {
e.log("openid超时未就绪，上传自定义题目失败");
}) : e.log("上传自定义题目失败:题目内容为空");
};
t.getRandomQuiz = function() {
return new Promise(function(t, n) {
e.Account.getAccountSafe().then(function(n) {
var i = {
gameId: e.Env.gameId,
openId: n.openid
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getCustomizeQuiz.action", i).then(function(n) {
t(n);
e.log("获取随机题目结果", JSON.stringify(n));
});
}).catch(function() {
e.log("openid超时未就绪，获取随机题目失败");
});
});
};
t.getQuizRank = function(t) {
return new Promise(function(n, i) {
if (t) {
var o = {
gameId: e.Env.gameId,
openId: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getQuizRank.action", o).then(function(t) {
n(t);
e.log("获取答题排行结果", JSON.stringify(t));
});
} else e.log("题库作者openid未就绪，获取答题排行失败");
});
};
t.uploadAnswerQuizScore = function(t, n, i) {
t ? e.Account.getAccountSafe().then(function(o) {
var r = {
gameId: e.Env.gameId,
openId: o.openid,
quizOpenId: t,
score: n,
show: i
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/answerQuiz.action", r).then(function(t) {
e.log("上报答题分数成功", JSON.stringify(r));
});
}).catch(function() {
e.log("openid超时未就绪，上报答题分数失败");
}) : e.log("上报答题分数失败:题目作者openid为空");
};
return t;
}();
e.Quiz = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.reportAppRun = function(e) {
"share" == e.query.type ? this.reportShareIn(e.query.shareid, e.query.userid, e.shareTicket || "", e.query.shareinfoid || "", e.query.sharetime || "", e.query.spt || "", e.query.page_id || "") : "wxad" == e.query.type ? this.reportAdIn(e.query.adid, e.query.gdt_vid, e.query.weixinadinfo) : e.query.type && this.reportLinkIn(e.query.adid, e.query.sgid ? e.query.sgid : "");
};
t.reportLogin = function(t, n, i) {
void 0 === t && (t = 0);
void 0 === n && (n = 0);
void 0 === i && (i = 0);
e.Env.init && e.Account.getAccountSafe().then(function(o) {
var r = {
gameid: e.Env.gameId,
userid: o.openid,
gold: t,
level_kt: n,
level_jl: i
};
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/login.action", r).then(function(t) {
e.log("上报login事件成功", JSON.stringify(r));
});
}).catch(function() {
e.log("openid超时未就绪，上报login事件失败");
});
};
t.reportGold = function(t, n, i, o) {
void 0 === t && (t = 0);
void 0 === n && (n = 0);
void 0 === i && (i = "");
void 0 === o && (o = 0);
e.Env.init && e.Account.getAccountSafe().then(function(r) {
var a = {
gameid: e.Env.gameId,
userid: r.openid,
change_gold: t,
new_gold: n,
param1: i,
reason: o
};
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/gold.action", a).then(function(t) {
e.log("上报金币变化成功", JSON.stringify(a));
});
}).catch(function() {
e.log("openid超时未就绪，上报金币变化失败");
});
};
t.reportEvent = function(t, n, i) {
void 0 === t && (t = 0);
void 0 === n && (n = "");
void 0 === i && (i = "");
e.Env.init && e.Account.getAccountSafe().then(function(o) {
var r = {
gameid: e.Env.gameId,
userid: o.openid,
eventid: t,
param1: encodeURIComponent(n),
param2: encodeURIComponent(i)
};
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/event.action", r).then(function(t) {
e.log("上报事件日志成功", JSON.stringify(r));
});
}).catch(function() {
e.log("openid超时未就绪，上报事件日志失败");
});
};
t.reportEvent2 = function(t, n, i, o) {
void 0 === n && (n = 0);
void 0 === i && (i = "");
void 0 === o && (o = "");
e.Env.init && e.Account.getAccountSafe().then(function(r) {
var a = {
gameid: e.Env.gameId,
userid: t,
eventid: n,
param1: encodeURIComponent(i),
param2: encodeURIComponent(o)
};
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/event.action", a).then(function(t) {
e.log("上报事件日志2成功", JSON.stringify(a));
});
}).catch(function() {
e.log("openid超时未就绪，上报事件日志2失败");
});
};
t.reportAppUserAttr = function(t, n, i) {
void 0 === n && (n = "");
void 0 === i && (i = "");
e.Env.init && e.Account.getAccountSafe().then(function(o) {
var r = {
gameid: e.Env.gameId,
userid: o.openid,
attrId: t,
param1: encodeURIComponent(n),
param2: encodeURIComponent(i)
};
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/appUserAttr.action", r).then(function(t) {
e.log("上报用户属性成功", JSON.stringify(r));
});
}).catch(function() {
e.log("openid超时未就绪，上报用户属性失败");
});
};
t.reportEventVideoAdShow = function(e) {
void 0 === e && (e = "");
this.reportEvent(999331, e);
};
t.reportEventVideoAdEnd = function(e) {
void 0 === e && (e = "");
this.reportEvent(999332, e);
};
t.reportEventDownloadBegin = function(e, t) {
void 0 === t && (t = 0);
e && e.aid && e.adid && this.reportEvent(-991, e.aid, e.adid + "_" + t);
};
t.reportEventDownloadSuccess = function(e, t) {
void 0 === t && (t = 0);
e && e.aid && e.adid && this.reportEvent(-992, e.aid, e.adid + "_" + t);
};
t.reportEventDownloadFail = function(e, t) {
void 0 === t && (t = 0);
e && e.aid && e.adid && this.reportEvent(-993, e.aid, e.adid + "_" + t);
};
t.reportAppSkipEvent = function(t, n) {
void 0 === n && (n = 0);
e.Env.init && e.Account.getAccountSafe().then(function(i) {
var o = {
gameid: e.Env.gameId,
userid: i.openid,
link_to_game_id: t.aid,
link_ad_id: t.adid || t.id,
param1: n
};
e.Platform.instance.httpRequest(e.Env.reportServer2 + "/MiniGameLog/log/skipEvent.action", o).then(function(n) {
e.log("上报App跳转事件成功", t.title, JSON.stringify(o));
});
}).catch(function() {
e.log("openid超时未就绪，上报App跳转事件失败");
});
};
t.reportShareIn = function(t, n, i, o, r, a, c) {
void 0 === o && (o = "");
void 0 === r && (r = "");
e.Env.init && e.Account.getAccountSafe().then(function(s) {
var d = {
gameid: e.Env.gameId,
userid: s.openid,
from_share_id: t,
from_sing_or_group: i ? 1 : 0,
from_share_userid: n,
param1: o,
param2: r,
channel: a,
pageId: c
};
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/shareIn.action", d).then(function(t) {
e.log("上报shareIn事件成功", JSON.stringify(d));
});
}).catch(function() {
e.log("openid超时未就绪，上报shareIn事件失败");
});
};
t.reportLinkIn = function(t, n, i) {
void 0 === i && (i = "");
e.Env.init && e.Account.getAccountSafe().then(function(i) {
var o = {
gameid: e.Env.gameId,
userid: i.openid,
my_adid: t,
from_appid: n,
param1: i.anonymous_openid ? i.anonymous_openid : ""
};
e.Platform.instance.httpRequest(e.Env.reportServer2 + "/MiniGameLog/log/linkIn.action", o).then(function(t) {
e.log("上报linkIn事件成功", JSON.stringify(o));
});
}).catch(function() {
e.log("openid超时未就绪，上报linkIn事件失败");
});
};
t.reportAdIn = function(t, n, i, o) {
void 0 === o && (o = "");
e.Env.init && e.Account.getAccountSafe().then(function(o) {
var r = {
gameid: e.Env.gameId,
userid: o.openid,
my_adid: t,
gdt_vid: n,
weixinadinfo: i,
param1: o.anonymous_openid ? o.anonymous_openid : ""
};
e.Platform.instance.httpRequest(e.Env.reportServer2 + "/MiniGameLog/log/wxadIn.action", r).then(function(t) {
e.log("上报adIn事件成功", JSON.stringify(r));
});
}).catch(function() {
e.log("openid超时未就绪，上报adIn事件失败");
});
};
t.reportOnlineTimeCount = function() {
return __awaiter(this, void 0, void 0, function() {
var n, i, o, r, a, c;
return __generator(this, function(s) {
switch (s.label) {
case 0:
s.trys.push([ 0, 2, , 3 ]);
n = e.Env.onlineTimeTotal + 1;
i = e.Env.onlineTimeCount + 1;
e.Env.onlineTimeTotal = n;
e.Env.onlineTimeCount = i;
e.Platform.instance.setStorage("online_time_total", n, !1);
e.Platform.instance.setStorage("online_time_count", i, !1);
if (!e.Env.init) return [ 2 ];
e.utils.checkAndResetOnlineTimeTotal(e.Env.onlineTimeTotalLastReportDate);
if ((o = new Date()).getTime() - t.onlineTimeCountLastReportTime < 59e3) return [ 2 ];
if (!(n < 300 && i > 60) && !(n >= 300 && i > 300)) return [ 2 ];
t.onlineTimeCountLastReportTime = o.getTime();
e.Env.onlineTimeTotalLastReportDate = e.utils.getFormatNowDate();
e.Platform.instance.setStorage("online_time_total_last_report_date", e.Env.onlineTimeTotalLastReportDate, !1);
return [ 4, e.Account.getAccountSafe() ];

case 1:
r = s.sent();
a = {
gameId: e.Env.gameId,
openId: r.openid,
pastTime: n
};
e.Env.onlineTimeCount = 0;
e.Platform.instance.httpRequest(e.Env.publicServer + "/OnlineTimeReport/data/report.action", a).then(function(t) {
t.totalTime && !isNaN(t.totalTime) ? e.log("同步在线时长成功", "onlineTimeTotal", n, "onlineTimeCount", i) : e.log("同步在线时长失败，服务器返回错误，1分钟后再报", JSON.stringify(t));
}).catch(function(t) {
e.log("同步在线时长失败，HTTP错误，1分钟后再报", JSON.stringify(t));
});
return [ 3, 3 ];

case 2:
c = s.sent();
e.log("reportOnlineTimeCount error:", c);
return [ 3, 3 ];

case 3:
return [ 2 ];
}
});
});
};
t.reportOnlineTimeCountOnHide = function() {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(e) {
return [ 2 ];
});
});
};
t.reportNewUserLog = function(t, n, i, o) {
void 0 === n && (n = "");
void 0 === i && (i = "");
void 0 === o && (o = "");
if (e.Platform.instance.getStorage("sdk-new-user-log-pass")) e.log("已通过新用户打点，无需再打点。"); else if (e.Env.init) {
o = o || e.Account.getClientUserId();
if (t == e.constant.NewUserLogEnum.ACTION_WX_LOGIN_END) if (n == e.constant.NewUserLogStatusEnum.STATUS_REPORT_SUCCESS) {
i = "" + (a = new Date().getTime() - Number(i));
e.log("新用户打点: userid=" + o + ", stepid=" + t + ", param1=" + n + ", param2=" + i + ", 耗时[毫秒]=" + a);
} else e.log("新用户打点: userid=" + o + ", stepid=" + t + ", param1=" + n + ", param2=" + i); else if (t == e.constant.NewUserLogEnum.ACTION_PT_LOGIN_END) {
var r = i.split("_"), a = new Date().getTime() - Number(r[0]), c = e.Account.getClientUUID(), s = r[1];
i = n == e.constant.NewUserLogStatusEnum.STATUS_REPORT_SUCCESS ? c + "_success_" + a : c + "_fail_" + a + "_" + s;
e.log("新用户打点: userid=" + o + ", stepid=" + t + ", param1=" + n + ", param2=" + i + ", 耗时[毫秒]=" + a);
} else e.log("新用户打点: userid=" + o + ", stepid=" + t + ", param1=" + n + ", param2=" + i);
var d = {
gameid: e.Env.gameId,
userid: o,
stepid: t,
param1: n,
param2: encodeURIComponent(i)
};
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/newUser.action", d).then(function(t) {
e.log("上报新用户日志成功", JSON.stringify(d));
}).catch(function(t) {
e.log("上报新用户日志失败", JSON.stringify(d));
});
}
};
t.setNewUserLogPass = function(t) {
e.log("设置是否通过新用户打点:", t);
e.Platform.instance.setStorage("sdk-new-user-log-pass", t);
};
t.reportHotLaunch = function(t) {
e.Env.init && t && Promise.all([ e.Account.getAccountSafe(), e.Hack.getOpenLevel("") ]).then(function(n) {
var i = n[0], o = n[1], r = o && o.ipAddr ? o.ipAddr : "", a = e.utils.parseAccountSource(t), c = e.Platform.instance.getSystem(), s = {
gameid: e.Env.gameId,
userId: i.openid,
unionId: i.unionid,
sourceType: a.sourceType,
sourceId: a.sourceId,
ip: r,
model: encodeURIComponent(c.model ? c.model : ""),
loginTime: new Date().getTime(),
actionId: 1
};
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/sdkLogin.action", s).then(function(t) {
e.log("上报热启动来源事件成功", JSON.stringify(s));
});
}).catch(function(t) {
e.log("上报热启动来源事件失败", t);
});
};
t.reportVideoTimeEvent = function(t) {
void 0 === t && (t = !1);
e.Env.init && e.Account.getAccountSafe().then(function(n) {
var i = {
gameId: e.Env.gameId,
openId: n.openid,
pastTime: 1,
clickTime: t ? 1 : 0
};
e.Platform.instance.httpRequest(e.Env.videoReportServer + "/VideoTimeReport/data/report.action", i).then(function(t) {
e.log("上报视频播放完事件日志成功", JSON.stringify(i));
e.Account.fetchAgTag();
});
}).catch(function() {
e.log("openid超时未就绪，上报视频播放完事件日志失败");
});
};
t.onlineTimeCountLastReportTime = 0;
return t;
}();
e.Report = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.hbAdd = function(t) {
void 0 === t && (t = "hongbao");
return new Promise(function(n, i) {
e.Account.getAccountSafe().then(function(o) {
var r = {
gameId: e.Env.gameId,
openId: o.openid,
dataKey: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/addLotteryGift.action", r).then(function(t) {
if (t && t.giftValue && t.giftValue > 0) {
n(t);
e.log("发放红包成功", JSON.stringify(t));
} else {
i();
e.log("发放红包失败", JSON.stringify(t));
}
});
}).catch(function() {
i();
e.log("openid超时未就绪，发放红包失败");
});
});
};
t.hbList = function(t) {
void 0 === t && (t = "hongbao");
return new Promise(function(n, i) {
e.Account.getAccountSafe().then(function(i) {
var o = {
gameId: e.Env.gameId,
openId: i.openid,
dataKey: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getGiftHistory.action", o).then(function(t) {
if (t && t.gifts) {
n(t.gifts);
e.log("查询玩家红包记录成功，共", t.gifts.length, "条记录");
} else {
n([]);
e.log("查询玩家红包记录失败", JSON.stringify(t));
}
});
}).catch(function() {
n([]);
e.log("openid超时未就绪，查询玩家红包记录失败");
});
});
};
t.hbAmount = function(t) {
void 0 === t && (t = "hongbao");
return new Promise(function(n, i) {
e.Account.getAccountSafe().then(function(i) {
var o = {
gameId: e.Env.gameId,
openId: i.openid,
dataKey: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getGiftValue.action", o).then(function(t) {
if (t && void 0 != t.giftValue) {
n(t.giftValue);
e.log("查询玩家红包总额成功", JSON.stringify(t));
} else {
n(0);
e.log("查询玩家红包总额失败", JSON.stringify(t));
}
});
}).catch(function() {
n(0);
e.log("openid超时未就绪，查询玩家红包总额失败");
});
});
};
t.hbDouble = function(t, n) {
void 0 === n && (n = "hongbao");
return new Promise(function(i, o) {
e.Account.getAccountSafe().then(function(r) {
var a = {
gameId: e.Env.gameId,
openId: r.openid,
dataKey: n,
giftId: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/doubleGift.action", a).then(function(t) {
if (t && t.giftValue && t.giftValue > 0) {
i(t.giftValue);
e.log("红包翻倍成功", JSON.stringify(t));
} else {
o();
e.log("红包翻倍失败", JSON.stringify(t));
}
});
}).catch(function() {
o();
e.log("openid超时未就绪，红包翻倍失败");
});
});
};
return t;
}();
e.Reward = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.rankUpload = function(t, n, i) {
t && void 0 != n && null != n && e.Account.getAccountSafe().then(function(o) {
var r = {
gameId: e.Env.gameId,
openId: o.openid,
unionId: o.unionid,
show: i,
dataKey: t,
dataValue: n
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/setData.action", r).then(function(t) {
e.log("上报排行榜数据成功", JSON.stringify(r));
});
}).catch(function() {
e.log("openid超时未就绪，上报排行榜数据失败");
});
};
t.friendBind = function(t) {
t && e.Account.getAccountSafe().then(function(n) {
var i = {
gameId: e.Env.gameId,
openId1: t,
unionId1: "",
openId2: n.openid,
unionId2: n.unionid
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/addFriend.action", i).then(function(t) {
e.log("加好友结果", JSON.stringify(t));
});
}).catch(function() {
e.log("openid超时未就绪，加好友失败");
});
};
t.friendList = function(t) {
return new Promise(function(n, i) {
e.Account.getAccountSafe().then(function(i) {
var o = {
gameId: e.Env.gameId,
openId: i.openid,
unionId: i.unionid,
dataKey: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getFriend.action", o).then(function(t) {
n(t);
e.log("获取好友列表结果", JSON.stringify(t));
});
}).catch(function() {
e.log("openid超时未就绪，获取好友列表失败");
});
});
};
t.rankList = function(t) {
return new Promise(function(n, i) {
e.Account.getAccountSafe().then(function(i) {
var o = {
gameId: e.Env.gameId,
openId: i.openid,
unionId: i.unionid,
dataKey: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getWorld.action", o).then(function(t) {
n(t);
e.log("获取世界排行结果", JSON.stringify(t));
});
}).catch(function() {
e.log("openid超时未就绪，获取世界排行失败");
});
});
};
t.rankListHistory = function(t) {
return new Promise(function(n, i) {
e.Account.getAccountSafe().then(function(i) {
var o = {
gameId: e.Env.gameId,
openId: i.openid,
unionId: i.unionid,
dataKey: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getRank.action", o).then(function(t) {
n(t);
e.log("获取上期排行结果", JSON.stringify(t));
});
}).catch(function() {
e.log("openid超时未就绪，获取上期排行失败");
});
});
};
t.inviteAccept = function(t, n, i, o, r, a) {
void 0 === i && (i = 0);
return new Promise(function(c, s) {
e.Account.getAccountSafe().then(function(s) {
var d = {
gameId: e.Env.gameId,
openId: s.openid,
sourceId: t
};
a && (d.sourceGameId = a);
n && (d.param = n);
if (i) {
d.type = i;
d.lastLoginTime = Number(o);
}
r && (d.extend = r);
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/setSource.action", d).then(function(t) {
if (t && 0 == t.error) {
c();
e.log("接受邀请成功", JSON.stringify(t));
} else e.log("接受邀请失败", JSON.stringify(t));
});
}).catch(function() {
e.log("openid超时未就绪，接受邀请失败");
});
});
};
t.reportInviteeEvent = function(t, n, i) {
return new Promise(function(o, r) {
e.Account.getAccountSafe().then(function(r) {
var a = {
gameId: e.Env.gameId,
openId: r.openid,
sourceId: t,
taskKey: n ? encodeURIComponent(n) : "",
taskVal: i ? encodeURIComponent(i) : ""
};
e.log("上报受邀者信息: ", JSON.stringify(a));
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/setSourceTask.action", a).then(function(t) {
if (t && 0 == t.error) {
o();
e.log("上报受邀者信息成功");
} else e.log("上报受邀者信息失败", JSON.stringify(t));
});
}).catch(function() {
e.log("openid超时未就绪，上报受邀者信息失败");
});
});
};
t.inviteResultList = function(t) {
void 0 === t && (t = 0);
return new Promise(function(n, i) {
e.Account.getAccountSafe().then(function(i) {
var o = {
gameId: e.Env.gameId,
openId: i.openid,
type: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getFriendShow.action", o).then(function(t) {
if (t && 0 === t.error) {
n(t);
e.log("获取邀请结果列表成功", JSON.stringify(t));
} else e.log("获取邀请结果列表失败", JSON.stringify(t));
});
}).catch(function() {
e.log("openid超时未就绪，获取邀请结果列表失败");
});
});
};
t.inviteResult = function(t) {
void 0 === t && (t = 0);
return new Promise(function(n, i) {
e.Account.getAccountSafe().then(function(o) {
var r = {
gameId: e.Env.gameId,
openId: o.openid,
type: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getFriendShow2.action", r).then(function(t) {
if (t && 0 === t.error) {
n(t.friends);
e.log("获取邀请结果列表成功", JSON.stringify(t));
} else {
i(t);
e.log("获取邀请结果列表失败", JSON.stringify(t));
}
}).catch(function() {
i("获取邀请结果列表失败，HTTP请求失败");
});
}).catch(function() {
i("openid超时未就绪，获取邀请结果列表失败");
e.log("openid超时未就绪，获取邀请结果列表失败");
});
});
};
t.joinLottery = function(t, n, i) {
t && n && e.Account.getAccountSafe().then(function(o) {
var r = {
gameId: e.Env.gameId,
openId: o.openid,
unionId: o.unionid,
show: i,
parameter1: t,
parameter2: n
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/setAccount.action", r).then(function(t) {
e.log("参与抽奖结果", JSON.stringify(t));
});
}).catch(function() {
e.log("openid超时未就绪，参与抽奖失败");
});
};
t.getRandUserInfo = function() {
return __awaiter(this, void 0, void 0, function() {
var t;
return __generator(this, function(n) {
switch (n.label) {
case 0:
return [ 4, e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getRandAvatorInfo.action") ];

case 1:
if (0 !== (t = n.sent()).result) throw t;
return [ 2, t.data ];
}
});
});
};
t.saveEvent = function(t, n, i) {
return __awaiter(this, void 0, void 0, function() {
var o, r, a;
return __generator(this, function(c) {
switch (c.label) {
case 0:
return [ 4, e.Account.getAccountSafe() ];

case 1:
o = c.sent();
r = {
gameId: e.Env.gameId,
openId: n,
eventId: t,
eventUser: o.openid,
param: i
};
return [ 4, e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/saveEvent.action", r) ];

case 2:
if (0 !== (a = c.sent()).error) throw a;
return [ 2 ];
}
});
});
};
t.getEvents = function() {
return __awaiter(this, void 0, void 0, function() {
var t, n, i;
return __generator(this, function(o) {
switch (o.label) {
case 0:
return [ 4, e.Account.getAccountSafe() ];

case 1:
t = o.sent();
n = {
gameId: e.Env.gameId,
openId: t.openid
};
return [ 4, e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getEvents.action", n) ];

case 2:
if (0 !== (i = o.sent()).error) throw i;
return [ 2, i.list ];
}
});
});
};
t.checkPrize = function(t) {
var n = {
unionId: e.utils.value(t, "referrerInfo.extraData.unionId"),
prizeType: e.utils.value(t, "referrerInfo.extraData.prize_type"),
prizeId: e.utils.value(t, "referrerInfo.extraData.prize_id")
};
return n.unionId && n.prizeType && n.prizeId ? n : null;
};
t.consumePrize = function(t, n, i) {
return __awaiter(this, void 0, void 0, function() {
var o, r;
return __generator(this, function(a) {
switch (a.label) {
case 0:
if (!t || !n || !i) throw "参数错误";
o = {
gameId: e.Env.gameId,
unionId: t,
type: n,
bonusId: i
};
return [ 4, e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getBonus.action", o) ];

case 1:
if (0 !== (r = a.sent()).result) throw r;
return [ 2 ];
}
});
});
};
t.getMyPreviousRankAward = function(t) {
return new Promise(function(n, i) {
e.Account.getAccountSafe().then(function(i) {
var o = {
gameId: e.Env.gameId,
openId: i.openid,
dataKey: t
};
e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getRankAward.action", o).then(function(t) {
n(t);
e.log("获取自己上期排名结果", JSON.stringify(t));
});
}).catch(function() {
e.log("openid超时未就绪，获取上期排行失败");
});
});
};
return t;
}();
e.SNS = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
var t = function() {
function t() {}
t.loadShareInfoList = function() {
var t = this;
this.shareInfoPromise || (this.shareInfoPromise = new Promise(function(n, i) {
t.shareInfoList = e.Platform.instance.getStorage("shareInfoList") || [];
e.Platform.instance.httpRequest(e.Env.cdnServer + "/ad/" + e.Env.gamePath + "/share.json").then(function(i) {
if ("object" != typeof i) e.log("加载分享配置失败，服务器返回错误", i); else {
t.shareInfoList = i;
e.Platform.instance.setStorage("shareInfoList", i);
e.log("加载分享配置成功", "共" + i.length + "条");
}
n(t.shareInfoList);
}).catch(function() {
n(t.shareInfoList);
});
}));
return this.shareInfoPromise;
};
t.getShareImage = function(e) {
return __awaiter(this, void 0, void 0, function() {
var t, n, i;
return __generator(this, function(o) {
switch (o.label) {
case 0:
this.shareInfoPromise || this.loadShareInfoList();
return [ 4, this.shareInfoPromise ];

case 1:
o.sent();
for (t = 0, n = this.shareInfoList; t < n.length; t++) if ((i = n[t]).id == e) return [ 2, i ];
throw "未找到指定分享图：" + e;
}
});
});
};
t.getShareInfo = function(t) {
if (this.shareInfoList && this.shareInfoList.length && "object" == typeof this.shareInfoList) {
var n = this.shareInfoList.filter(function(t, n, i) {
return !e.Hack.appStatus || "0" != e.Hack.appStatus.level || 1 != Number(t.illegal);
}, this), i = e.utils.tableAlgorithm(n, "table");
t && (i.text = i.text.replace("{nickname}", t));
e.log("获取随机分享内容成功", JSON.stringify(i));
return i;
}
e.log("获取随机分享信息失败，分享配置尚未就绪，返回值为undefined，请注意容错");
};
t.genShareInfo = function(t) {
var n = this, i = e.Account.getAccount().sid, o = "";
if (i && e.Env.openLevelData && e.Env.openLevelData.sdkShareFrontWordOnOff && "1" === e.Env.openLevelData.sdkShareFrontWordOnOff) {
var r = parseInt(i.slice(0, 7)), a = i.slice(7);
o = "[" + e.EngineUtility.EncodeNumberToUtf8Character(r) + "@你," + a + "]";
e.log("分享前缀", o);
}
t.prefix = o;
t.title = o + t.title;
if (void 0 != t.imageId && 0 == t.imageId) {
e.log("指定imageId == 0，则使用默认分享信息");
return t;
}
if (t.imageId && this.shareInfoList && this.shareInfoList.length) {
for (var c = void 0, s = 0; s < this.shareInfoList.length; s++) if (this.shareInfoList[s].id == t.imageId) {
c = this.shareInfoList[s];
break;
}
if (c) {
var d = __assign({}, t);
d.prefix = o;
d.title = o + (d.nickName ? c.text.replace("{nickname}", d.nickName) : c.text);
d.image = c.image;
d.imageId = c.id;
d.text = c.content;
delete d.nickName;
delete d.scoreValue;
e.log("生成固定分享内容成功", JSON.stringify(d));
return d;
}
}
t.imageId = 0;
if (!this.shareInfoList || !this.shareInfoList.length || "object" != typeof this.shareInfoList) {
e.log("获取随机分享信息失败，使用默认分享信息");
return t;
}
var u = this.shareInfoList.filter(function(e) {
if (e.version) {
if (!t.version || e.version != t.version) return !1;
} else if (t.version) return !1;
return n.checkFilter(e, t);
}, this), l = u;
!u.length && t.version && (l = this.shareInfoList.filter(function(e) {
return !e.version && n.checkFilter(e, t);
}, this));
if (!l.length) {
e.log("随机分享被完全过滤，使用默认分享信息");
return t;
}
var p = e.utils.tableAlgorithm(l, "table");
if (!p) return t;
var f = __assign({}, t);
f.prefix = o;
f.title = o + (f.nickName ? p.text.replace("{nickname}", f.nickName) : p.text);
f.image = p.image;
f.imageId = p.id;
f.text = p.content;
delete f.nickName;
delete f.scoreValue;
e.log("生成随机分享内容成功", JSON.stringify(f));
return f;
};
t.checkFilter = function(t, n) {
if (1 === Number(t.illegal) && !e.Hack.isStrongFission()) {
e.log("分享图", t.id, "非强烈变用户，该分享图被过滤");
return !1;
}
if ((n.scoreValue || 0) < (Number(t.number) || 0)) {
e.log("分享图", t.id, "未对当前玩家开放");
return !1;
}
if (!e.utils.checkMultiOpenTimesLimit(t)) {
e.log("分享图", t.id, "当前时段未开放", " , 开放时段：", t.openTimes);
return !1;
}
if (!e.Hack.appStatus) {
e.log("分享图", t.id, "审核状态未就绪则开放");
return !0;
}
if (1 === Number(t.illegal) && 0 === Number(e.Hack.appStatus.level)) {
e.log("分享图", t.id, "不符合审核要求，被过滤");
return !1;
}
if (t.city) {
var i = e.utils.value(e.Hack.appStatus, "ipArea.city"), o = e.utils.value(e.Hack.appStatus, "ipArea.province");
if (e.utils.checkAbroadCity(e.Hack.appStatus)) {
e.log("分享图", t.id, "仅限中国大陆地区使用");
return !1;
}
if (i && -1 != t.city.indexOf(i)) {
e.log("分享图", t.id, "未对", i, "地区用户开放");
return !1;
}
if (o && -1 != t.city.indexOf(o)) {
e.log("分享图", t.id, "未对", o, "地区用户开放");
return !1;
}
}
return !0;
};
t.genShareImage = function(t, n) {
return __awaiter(this, void 0, void 0, function() {
var i, o, r, a, c, s, d, u, l, p, f, h;
return __generator(this, function(g) {
switch (g.label) {
case 0:
(i = wx.createImage()).src = t;
o = new Promise(function(e, t) {
i.onload = e;
i.onerror = t;
});
(r = wx.createImage()).src = n;
a = new Promise(function(e, t) {
r.onload = e;
r.onerror = t;
});
return [ 4, o ];

case 1:
g.sent();
return [ 4, a ];

case 2:
g.sent();
(c = wx.createCanvas()).width = i.width;
c.height = i.height;
s = c.getContext("2d");
d = .01 * i.height;
u = 10;
l = .1 * i.width;
p = .3 * i.height;
f = .4 * i.height;
h = f;
s.drawImage(i, 0, 0);
s.drawImage(r, l, p, f, h);
e.utils.drawRoundRect(s, l, p, f, h, u, d);
return [ 2, c.toTempFilePathSync({
x: 0,
y: 0,
width: i.width,
height: i.height,
destWidth: i.width,
destHeight: i.height
}) ];
}
});
});
};
t.getShareLink = function(e, t, n, i) {
void 0 === t && (t = "/pages/index/index");
return t + "?" + this.getShareQuery(e, n, i);
};
t.getShareQuery = function(t, n, i) {
"object" != typeof n && (n = {});
n.type = "share";
n.shareid = "share_" + e.Env.gameId + "_" + t;
n.userid = e.Account.getAccount().openid;
n.sharetime = new Date().getTime();
n.gameid = e.Env.gameId;
i && (n.shareinfoid = i);
return e.utils.httpBuildQuery(n);
};
t.reportShareOut = function(t, n, i, o, r, a) {
void 0 === n && (n = "");
void 0 === i && (i = "");
void 0 === o && (o = "");
void 0 === r && (r = "");
void 0 === a && (a = "");
e.Env.init && e.Account.getAccountSafe().then(function(c) {
var s = {
gameid: e.Env.gameId,
userid: c.openid,
share_char_id: "share_" + e.Env.gameId + "_" + t,
share_pic_id: n,
sing_or_group: i,
param1: o,
param2: r,
channel: a
};
e.Platform.instance.httpRequest(e.Env.reportServer1 + "/MiniGameLog/log/shareOut.action", s).then(function(t) {
e.log("上报shareOut事件成功", JSON.stringify(s));
});
}).catch(function() {
e.log("openid超时未就绪，上报shareOut事件失败");
});
};
t.commonShare = function(t, n, i, o) {
void 0 === n && (n = "wx");
var r = this.share(t, n);
e.log("获得分享信息", JSON.stringify(r));
return r;
};
t.commonAppShare = function(t, n) {
var i = this.share(t, n);
e.log("获得分享信息", JSON.stringify(i));
return i;
};
t.commonAppShareWithQRCode = function(t, n, i, o) {
void 0 === n && (n = "wx");
void 0 === i && (i = !1);
void 0 === o && (o = {
scale: 4
});
return __awaiter(this, void 0, void 0, function() {
var r;
return __generator(this, function(a) {
switch (a.label) {
case 0:
return [ 4, this.shareWithQRCode(t, n, i, o) ];

case 1:
r = a.sent();
e.log("获得分享信息", JSON.stringify(r));
return [ 2, r ];
}
});
});
};
t.share = function(n, i) {
e.log("获得app分享信息,sharePlatform=", i);
if (!i) {
e.log("获得app分享信息,请传入分享平台标识");
i = "";
}
var o = t.genShareInfo(n), r = {};
r.title = o.title;
r.imageUrl = o.image;
r.prefix = o.prefix;
r.text = null == o.text || void 0 == o.text ? "" : o.text;
r.query = t.getShareQuery(o.serial, o.params || {}, o.imageId);
e.Env.shareUrl ? r.url = e.Env.shareUrl + "?" + r.query + "&spt=" + i : r.url = e.Env.landingPageServer + "?" + r.query + "&spt=" + i;
var a = void 0 == r.query || null == r.query ? "" : e.utils.getQueryString(r.query, "sharetime");
t.reportShareOut(n.serial, o.imageId.toString(), "", "", a, i);
return r;
};
t.shareWithQRCode = function(n, i, o, r) {
void 0 === i && (i = "wx");
void 0 === o && (o = !1);
void 0 === r && (r = {
scale: 4
});
return __awaiter(this, void 0, void 0, function() {
var a, c, s, d, u, l, p;
return __generator(this, function(f) {
switch (f.label) {
case 0:
e.log("获得app分享信息,sharePlatform=", i);
if (!i) {
e.log("获得app分享信息,请传入分享平台标识");
i = "";
}
a = t.genShareInfo(n);
(c = {}).title = a.title;
c.imageUrl = a.image;
c.prefix = a.prefix;
c.text = null == a.text || void 0 == a.text ? "" : a.text;
c.query = t.getShareQuery(a.serial, a.params || {}, a.imageId);
s = e.Env.landingPageServer;
e.Env.shareUrl && (s = e.Env.shareUrl);
c.url = e.Env.landingPageServer + "?" + c.query + "&spt=" + i;
d = "https://xyx-gameshare.1024mo.com/share/in.do?" + c.query + "&spt=" + i + "&down_url=" + s;
e.Env.wxpublicUrl && (d = e.Env.wxpublicUrl + "/share/in.do?" + c.query + "&spt=" + i + "&down_url=" + s);
if (!o) return [ 3, 5 ];
f.label = 1;

case 1:
f.trys.push([ 1, 3, , 4 ]);
u = c;
return [ 4, this.fetchQRCode(encodeURIComponent(d), r) ];

case 2:
u.qrcodeUrl = f.sent();
console.log("info.qrcodeUrl===> ", c.qrcodeUrl);
return [ 3, 4 ];

case 3:
l = f.sent();
console.log("share with qrcode error: ", l);
return [ 3, 4 ];

case 4:
return [ 3, 6 ];

case 5:
c.qrcodeUrl = d;
f.label = 6;

case 6:
p = void 0 == c.query || null == c.query ? "" : e.utils.getQueryString(c.query, "sharetime");
t.reportShareOut(n.serial, a.imageId.toString(), "", "", p, i);
return [ 2, c ];
}
});
});
};
t.checkValidShare = function() {
var t = this;
return new Promise(function(n, i) {
0 != t.ran && "android" == e.Platform.instance.getSystem().platform.toLocaleLowerCase() ? e.Account.getAccountSafe().then(function(o) {
var r = {
openid: o.openid,
ran: t.ran
};
e.Platform.instance.httpRequest(e.Env.photoServer + "/check.php", r).then(function(e) {
null != e && void 0 != e ? n(1 == e || "1" == e) : i();
}).catch(function(t) {
i();
e.log("photo check fail:", t);
});
}).catch(function() {
i();
e.log("openid超时未就绪，查询失败");
}) : i();
});
};
t.genImagePath = function(t) {
if ("android" != e.Platform.instance.getSystem().platform.toLocaleLowerCase()) return t;
this.ran = new Date().getTime();
return t.replace("https://cdn-xyx.raink.com.cn", "https://photo-xyx.raink.com.cn") + "?openid=" + e.Account.getOpenId() + "&ran=" + this.ran;
};
t.fetchQRCode = function(t, n) {
return new Promise(function(i, o) {
if (t) {
n.url = t;
var r = "https://xyx-gameshare.1024mo.com/qrcode/create.do";
e.Env.wxpublicUrl && (r = e.Env.wxpublicUrl + "/qrcode/create.do");
e.log("fetchQRCode 参数:", JSON.stringify(n));
e.Platform.instance.httpRequest(r, n, "get").then(function(t) {
e.log("获取二维码response--", JSON.stringify(t));
if (t && 0 == t.code) {
e.log("获取二维码成功", t.data);
i(t.data);
} else {
e.log("获取二维码失败", JSON.stringify(t));
o(t);
}
}).catch(function(t) {
e.log("获取二维码失败", JSON.stringify(t));
o(t);
});
} else i("");
});
};
t.shareInfoList = [];
t.ran = 0;
return t;
}();
e.Share = t;
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(e) {
(function(e) {
e[e.GET_USER_INFO = 1] = "GET_USER_INFO";
e[e.START_GAME = 2] = "START_GAME";
})(e.ActiveType || (e.ActiveType = {}));
(function(e) {
e[e.UNKNOWN = 0] = "UNKNOWN";
e[e.ACTIVE = 1] = "ACTIVE";
e[e.INACTIVE = 2] = "INACTIVE";
})(e.ActiveValue || (e.ActiveValue = {}));
(function(e) {
e[e.ACTION_WX_LOGIN_BEGIN = 1] = "ACTION_WX_LOGIN_BEGIN";
e[e.ACTION_WX_LOGIN_END = 2] = "ACTION_WX_LOGIN_END";
e[e.ACTION_PT_LOGIN_BEGIN = 3] = "ACTION_PT_LOGIN_BEGIN";
e[e.ACTION_PT_LOGIN_END = 4] = "ACTION_PT_LOGIN_END";
})(e.NewUserLogEnum || (e.NewUserLogEnum = {}));
(function(e) {
e.STATUS_REPORT_SUCCESS = "1";
e.STATUS_REPORT_FAILS = "0";
})(e.NewUserLogStatusEnum || (e.NewUserLogStatusEnum = {}));
})(e.constant || (e.constant = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(e) {
var t = function() {
return function() {
this.identifier = "0";
this.sorted = !1;
this.interlace = !1;
this.transparentIndex = -1;
this.lzwMinCodeSize = 0;
this.comments = [];
this.text = "";
this.left = 0;
this.top = 0;
this.width = 0;
this.height = 0;
this.delay = 0;
this.disposal = 0;
};
}();
e.GifImage = t;
})(e.gif || (e.gif = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(e) {
var t = function() {
function e(e, t, n) {
this.width = e;
this.imageData = new Array(e * t);
for (var i = 0; i < this.imageData.length; i++) this.imageData[i] = n;
}
e.prototype.drawImage = function(e, t, n, i, o) {
for (var r = 0; r < e.length; r++) if (e[r] != o) {
var a = r % i + n, c = (Math.floor(r / i) + t) * this.width + a;
this.imageData[c] = e[r];
}
};
return e;
}();
e.ImageCanvas = t;
})(e.gif || (e.gif = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(e) {
var t = function() {
function e() {
this.__out = [];
this.__remNumBits = 0;
this.__remVal = 0;
}
e.prototype.push = function(e, t) {
for (;t > 0; ) {
this.__remVal = (e << this.__remNumBits & 255) + this.__remVal;
if (t + this.__remNumBits >= 8) {
this.__out[this.__out.length] = this.__remVal;
t -= 8 - this.__remNumBits;
e >>= 8 - this.__remNumBits;
this.__remVal = 0;
this.__remNumBits = 0;
} else {
this.__remNumBits = t + this.__remNumBits;
t = 0;
}
}
};
e.prototype.flush = function() {
this.push(0, 8);
this.__remNumBits = 0;
this.__remVal = 0;
var e = this.__out;
this.__out = [];
return e;
};
return e;
}(), n = function() {
function e() {}
e.encode = function(e, n) {
var i = new t(), o = 1 << n, r = o + 1, a = 0, c = 0, s = {};
function d() {
a = r + 1;
c = n + 1;
s = Object.create(null);
}
d();
i.push(o, c);
for (var u = "", l = 0, p = e.length; l < p; ++l) {
var f = e[l], h = String.fromCharCode(f);
h in s || (s[h] = f);
var g = u;
if (!((u += h) in s)) {
i.push(s[g], c);
if (a <= 4095) {
s[u] = a;
a === 1 << c && c++;
a++;
} else {
i.push(o, c);
d();
s[h] = f;
}
u = h;
}
}
i.push(s[u], c);
i.push(r, c);
return i.flush();
};
e.decode = function(e, t) {
for (var n = 0, i = function(e) {
for (var i = 0, o = 0; o < e; o++) {
t.charCodeAt(n >> 3) & 1 << (7 & n) && (i |= 1 << o);
n++;
}
return i;
}, o = [], r = 1 << e, a = r + 1, c = e + 1, s = [], d = function() {
s = [];
c = e + 1;
for (var t = 0; t < r; t++) s[t] = [ t ];
s[r] = [];
s[a] = null;
}, u = 0, l = 0; ;) {
l = u;
if ((u = i(c)) !== r) {
if (u === a) break;
if (u < s.length) l !== r && (s[s.length] = s[l].concat(s[u][0])); else {
if (u !== s.length) throw new Error("Invalid LZW code.");
s[s.length] = s[l].concat(s[l][0]);
}
o.splice.apply(o, [ o.length, 0 ].concat(s[u]));
s.length === 1 << c && c < 12 && c++;
} else d();
}
return o;
};
return e;
}();
e.lzw = n;
})(e.gif || (e.gif = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function n(t) {
this._valid = !1;
this._sorted = !1;
this._backgroundIndex = -1;
this._colorDepth = 0;
this._loopCount = 0;
this._height = 0;
this._width = 0;
this._animated = !1;
this._images = [];
if (t) if (t.byteLength) {
this._dv = new DataView(t);
this.parseGifInfo();
} else if (t._dv) {
t._animated && (this._animated = t._animated);
t._backgroundIndex && (this._backgroundIndex = t._backgroundIndex);
t._colorDepth && (this._colorDepth = t._colorDepth);
t._dv && (this._dv = new DataView(e.utils.string2Buffer(t._dv)));
t._globalHeadData && (this._globalHeadData = e.utils.string2Buffer(t._globalHeadData));
t._globalPalette && (this._globalPalette = e.utils.string2Buffer(t._globalPalette));
t._height && (this._height = t._height);
t._images && (this._images = t._images);
t._loopCount && (this._loopCount = t._loopCount);
t._sorted && (this._sorted = t._sorted);
t._valid && (this._valid = t._valid);
t._width && (this._width = t._width);
if (t._animated) for (var n = 0; n < t._images.length; n++) {
this._images[n].localPalette = e.utils.string2Buffer(t._images[n].localPalette);
this._images[n].frameData = e.utils.string2Buffer(t._images[n].frameData);
}
}
}
Object.defineProperty(n.prototype, "valid", {
get: function() {
return this._valid;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "globalPalette", {
get: function() {
return this._globalPalette;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "sorted", {
get: function() {
return this._sorted;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "backgroundIndex", {
get: function() {
return this._backgroundIndex;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "colorDepth", {
get: function() {
return this._colorDepth;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "globalHeadData", {
get: function() {
return this._globalHeadData;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "loopCount", {
get: function() {
return this._loopCount;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "height", {
get: function() {
return this._height;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "width", {
get: function() {
return this._width;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "animated", {
get: function() {
return this._animated;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "images", {
get: function() {
return this._images;
},
enumerable: !0,
configurable: !0
});
Object.defineProperty(n.prototype, "dataView", {
get: function() {
return this._dv;
},
enumerable: !0,
configurable: !0
});
n.prototype.parseGifInfo = function() {
if (-1 != [ "GIF89a", "GIF87a" ].indexOf(this.getString(this._dv, 0, 6))) {
this._valid = !0;
this._width = this._dv.getUint16(6, !0);
this._height = this._dv.getUint16(8, !0);
this._backgroundIndex = this._dv.getUint8(11);
var e = this.getBitArray(this._dv.getUint8(10));
this._colorDepth = this.bitToInt(e.slice(1, 4)) + 1;
this._sorted = !!e[4];
if (e[0]) {
var n = this.getPaletteSize(e);
this._globalPalette = this._dv.buffer.slice(13, 13 + n);
}
this._globalHeadData = this._dv.buffer.slice(0, this._globalPalette ? 13 + this._globalPalette.byteLength : 13);
for (var i = 0, o = new t.GifImage(), r = o.start = this._globalHeadData.byteLength; ;) {
try {
switch (this._dv.getUint8(r)) {
case 33:
o.start = r;
switch (this._dv.getUint8(r + 1)) {
case 249:
if (4 === this._dv.getUint8(r + 2)) {
var a = this.getBitArray(this._dv.getUint8(r + 3));
o.disposal = parseInt(a.slice(3, 6).join(""), 2);
o.delay = this.getDuration(this._dv.getUint16(r + 4, !0));
o.transparentIndex = this._dv.getUint8(r + 6);
r += 8;
} else r++;
break;

case 255:
var c = this.readSubBlock(this._dv, r + 2, !0);
"NETSCAPE" === c.data.substr(0, 8) && (this._loopCount = c.data.charCodeAt(11));
r += 2 + c.size;
break;

case 206:
var s = this.readSubBlock(this._dv, r + 2, !0);
o.identifier = s.data;
r += 2 + s.size;
break;

case 254:
var d = this.readSubBlock(this._dv, r + 2, !0);
o.comments[o.comments.length] = d.data;
r += 2 + d.size;
break;

case 1:
var u = this.readSubBlock(this._dv, r + 2, !0);
o.text = u.data;
r += 2 + u.size;
}
break;

case 44:
o.left = this._dv.getUint16(r + 1, !0);
o.top = this._dv.getUint16(r + 3, !0);
o.width = this._dv.getUint16(r + 5, !0);
o.height = this._dv.getUint16(r + 7, !0);
var l = this.getBitArray(this._dv.getUint8(r + 9));
o.interlace = !!l[1];
o.sorted = !!l[2];
if (l[0]) {
var p = this.getPaletteSize(l);
o.localPalette = this._dv.buffer.slice(r + 10, r + 10 + p);
r += 10 + p;
} else {
o.localPalette = this._globalPalette;
r += 10;
}
o.lzwMinCodeSize = this._dv.getUint8(r);
r++;
var f = this.readSubBlock(this._dv, r, !0);
o.rawData = f.data;
r += f.size;
this._images[this._images.length] = o;
i++;
(o = new t.GifImage()).start = r;
o.identifier = i.toString();
this._images.length > 1 && !this._animated && (this._animated = !0);
break;

case 59:
return;

default:
r++;
}
} catch (e) {
console.log("解析GIF数据发生异常", e);
this._valid = !1;
return;
}
if (r >= this._dv.byteLength) return;
}
} else this._valid = !1;
};
n.prototype.getPixels = function(e) {
e < 0 ? e = 0 : e > this._images.length - 1 && (e = this._images.length - 1);
var n = this._images[e];
if (n.pixels) return n.pixels;
var i = t.lzw.decode(n.lzwMinCodeSize, n.rawData);
n.interlace && (i = this.deinterlace(i, n.width));
var o = new t.ImageCanvas(this._width, this._height, this._backgroundIndex);
0 != e && o.drawImage(this.getPixels(e - 1), 0, 0, this._width, this._images[e - 1].transparentIndex);
o.drawImage(i, n.top, n.left, n.width, n.transparentIndex);
n.pixels = o.imageData;
return n.pixels;
};
n.prototype.getFrame = function(e) {
e < 0 ? e = 0 : e > this._images.length - 1 && (e = this._images.length - 1);
var n = this._images[e];
if (n.frameData) return n.frameData;
if (0 == e) {
n.frameData = this._dv.buffer.slice(0, this._images[1].start);
return n.frameData;
}
var i = new ArrayBuffer(11), o = new DataView(i);
o.setUint8(0, 44);
o.setUint16(1, 0, !0);
o.setUint16(3, 0, !0);
o.setUint16(5, this._width, !0);
o.setUint16(7, this._height, !0);
o.setUint8(9, 0);
o.setUint8(10, n.lzwMinCodeSize);
var r = t.lzw.encode(this.getPixels(e), this._colorDepth), a = this.makeSubBlock(r);
n.frameData = this.concatArrayBuffer(this._globalHeadData, i, a, new Uint8Array([ 59 ]).buffer);
return n.frameData;
};
n.prototype.concatArrayBuffer = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
for (var n = 0, i = 0, o = e; i < o.length; i++) {
n += (d = o[i]).byteLength;
}
for (var r = new ArrayBuffer(n), a = 0, c = 0, s = e; c < s.length; c++) {
var d = s[c];
new Uint8Array(r, a, d.byteLength).set(new Uint8Array(d));
a += d.byteLength;
}
return r;
};
n.prototype.getBitArray = function(e) {
for (var t = [], n = 7; n >= 0; n--) t[t.length] = e & 1 << n ? 1 : 0;
return t;
};
n.prototype.bitToInt = function(e) {
return e.reduce(function(e, t) {
return 2 * e + t;
}, 0);
};
n.prototype.getPaletteSize = function(e) {
return 3 * Math.pow(2, 1 + this.bitToInt(e.slice(5, 8)));
};
n.prototype.getDuration = function(e) {
return e / 100 * 1e3;
};
n.prototype.getString = function(e, t, n) {
for (var i = new Array(n), o = 0; o < n; o++) i[o] = String.fromCharCode(e.getUint8(t + o));
return i.join("");
};
n.prototype.readSubBlock = function(e, t, n) {
void 0 === n && (n = !1);
for (var i = {
size: 0,
data: ""
}; ;) {
var o = e.getUint8(t + i.size);
if (0 === o) {
i.size++;
break;
}
n && (i.data += this.getString(e, t + i.size + 1, o));
i.size += o + 1;
}
return i;
};
n.prototype.makeSubBlock = function(e) {
for (var t = [], n = 0; ;) {
var i = e.slice(n, n + 255);
n += 255;
if (255 != i.length) {
if (0 == i.length) {
t[t.length] = 0;
break;
}
t[t.length] = i.length;
t.splice.apply(t, [ t.length, 0 ].concat(i));
t[t.length] = 0;
break;
}
t[t.length] = 255;
t.splice.apply(t, [ t.length, 0 ].concat(i));
}
return new Uint8Array(t).buffer;
};
n.prototype.deinterlace = function(e, t) {
for (var n = new Array(e.length), i = e.length / t, o = function(i, o) {
var r = e.slice(o * t, (o + 1) * t);
n.splice.apply(n, [ i * t, t ].concat(r));
}, r = [ 0, 4, 2, 1 ], a = [ 8, 8, 4, 2 ], c = 0, s = 0; s < 4; s++) for (var d = r[s]; d < i; d += a[s]) {
o(d, c);
c++;
}
return n;
};
return n;
}();
t.GifReader = n;
})(e.gif || (e.gif = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function n(e, t, n) {
this.url = e;
this.textureCache = [];
this.frameIndex = 0;
this.callbackFunc = t;
this.callbackObject = n;
this.movie = new egret.Bitmap();
this.movie.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.destroy, this);
}
n.loadByUrl = function(i, o, r, a, c) {
void 0 === a && (a = "");
void 0 === c && (c = "");
var s = new n(i, o, r);
if (n.gifCache[i]) s.loadCache(n.gifCache[i]); else {
if (a && c) {
if ((a != n.workerType || c != n.workerPath) && "wx" == a && "wx" == e.Platform.platformType) {
n.workerType = a;
n.workerPath = c;
n.worker && worker.terminate();
n.worker = wx.createWorker(c);
n.worker.onMessage(function(i) {
if ("gif" == i.msg) {
n.gifCache[i.id] || (n.gifCache[i.id] = new t.GifReader(i.reader));
for (var o = n.workerListener.length; o--; ) if (n.workerListener[o].url == i.id) {
n.workerListener[o].loadCache(n.gifCache[i.id]);
n.workerListener.splice(o, 1);
}
} else e.log("收到worker进程错误消息", JSON.stringify(i));
});
}
n.worker && (n.workerListener[n.workerListener.length] = s);
}
RES.getResByUrl(i.trim(), s.loadBuffer, s, RES.ResourceItem.TYPE_BIN);
}
};
n.prototype.loadBuffer = function(e, i) {
if (n.worker && "wx" == n.workerType) n.worker.postMessage({
msg: "gif",
id: this.url,
buffer: e
}); else {
n.gifCache[i] = new t.GifReader(e);
this.reader = n.gifCache[i];
this.compFunc();
}
};
n.prototype.loadCache = function(e) {
this.reader = e;
this.compFunc();
};
n.prototype.compFunc = function() {
var e = this;
if (this.reader.valid && this.reader.animated) {
this.callbackFunc.call(this.callbackObject, this);
this.timer = new egret.Timer(1, 1);
this.timer.addEventListener(egret.TimerEvent.TIMER_COMPLETE, this.play, this);
this.timer.start();
} else egret.BitmapData.create("arraybuffer", this.reader.dataView.buffer, function(t) {
var n = new egret.Texture();
n.bitmapData = t;
e.movie.texture = n;
e.callbackFunc.call(e.callbackObject, e);
});
};
n.prototype.play = function() {
var e = this;
if (this.timer) {
var t = this.frameIndex;
++this.frameIndex >= this.reader.images.length && (this.frameIndex = 0);
this.timer.delay = this.reader.images[t].delay < 20 ? 100 : this.reader.images[t].delay;
if (this.textureCache[t]) {
this.movie.texture = this.textureCache[t];
this.timer.start();
} else {
var n = this.reader.getFrame(t);
egret.BitmapData.create("arraybuffer", n, function(n) {
if (e.timer) {
var i = new egret.Texture();
i.bitmapData = n;
e.textureCache[t] = i;
e.movie.texture = e.textureCache[t];
e.timer.start();
}
});
}
}
};
n.prototype.destroy = function() {
if (this.timer) {
this.timer.stop();
for (var e = 0, t = this.textureCache; e < t.length; e++) {
t[e].dispose();
}
delete this.textureCache;
delete this.reader;
delete this.movie;
delete this.timer;
}
};
n.gifCache = {};
n.workerListener = [];
return n;
}();
t.EgretGifMovie = n;
})(e.gif || (e.gif = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function n(e, t, n) {
this.url = e;
this.movie = new cc.Node();
this.sprite = this.movie.addComponent(cc.Sprite);
this.textureCache = [];
this.frameIndex = 0;
this.callbackFunc = t;
this.callbackObject = n;
}
n.loadByUrl = function(e, t, i) {
var o = new n(e, t, i);
if (n.gifCache[e]) o.loadCache(n.gifCache[e]); else {
var r = cc.loader.getXMLHttpRequest();
r.responseType = "arraybuffer";
r.onreadystatechange = function() {
4 == r.readyState && r.status >= 200 && r.status < 400 && o.loadBuffer(r.response, e);
};
r.open("GET", e, !0);
r.send();
}
};
n.prototype.loadBuffer = function(e, i) {
n.gifCache[i] = new t.GifReader(e);
this.reader = n.gifCache[i];
this.compFunc();
};
n.prototype.loadCache = function(e) {
this.reader = e;
this.compFunc();
};
n.prototype.compFunc = function() {
var e = this;
if (this.reader.valid && this.reader.animated) {
this.timer = 1;
this.callbackFunc.call(this.callbackObject, this);
this.play();
} else cc.loader.load(this.url, function(t, n) {
e.sprite.spriteFrame = new cc.SpriteFrame(n);
e.callbackFunc.call(e.callbackObject, e);
});
};
n.prototype.play = function() {
var t = this;
if (this.timer) {
var n = this.frameIndex;
++this.frameIndex >= this.reader.images.length && (this.frameIndex = 0);
var i = this.reader.images[n].delay < 20 ? 100 : this.reader.images[n].delay;
if (!this.textureCache[n]) {
for (var o = this.reader.getPixels(n), r = new Uint8Array(this.reader.images[n].localPalette), a = new Uint8Array(4 * o.length), c = 0; c < o.length; c++) {
a[4 * c] = r[3 * o[c]];
a[4 * c + 1] = r[3 * o[c] + 1];
a[4 * c + 2] = r[3 * o[c] + 2];
a[4 * c + 3] = 255;
}
var s = void 0;
cc.RenderTexture.initWithSize ? (s = new cc.RenderTexture()).initWithSize(this.reader.width, this.reader.height) : s = new cc.Texture2D();
s.initWithData(a, cc.Texture2D.PixelFormat.RGBA8888, this.reader.width, this.reader.height);
if (this.movie.width && this.movie.height) {
s.width = this.movie.width;
s.height = this.movie.height;
}
this.textureCache[n] = new cc.SpriteFrame(s);
}
var d = this.movie.width, u = this.movie.height;
this.sprite.spriteFrame = this.textureCache[n];
if (d && u) {
this.movie.width = d;
this.movie.height = u;
}
e.Platform.instance.setTimeout(function() {
t.play();
}, i);
}
};
n.prototype.dispose = function() {
if (this.timer) {
this.timer = 0;
for (var e = 0, t = this.textureCache; e < t.length; e++) {
t[e].destroy();
}
this.sprite.destroy();
delete this.textureCache;
delete this.reader;
}
};
n.gifCache = {};
return n;
}();
t.CocosGifMovie = n;
})(e.gif || (e.gif = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function t() {}
t.getGoodsList = function(t) {
return __awaiter(this, void 0, void 0, function() {
var n, i, o, r, a;
return __generator(this, function(c) {
switch (c.label) {
case 0:
return [ 4, e.Account.getAccountSafe() ];

case 1:
n = c.sent();
return [ 4, e.Platform.instance.httpRequest(e.Env.extServer + "/v1/pdd/recommend", {
openid: n.openid,
game_id: t || e.Env.gameId
}, "get", 0) ];

case 2:
if (!(i = c.sent()).data) return [ 2, [] ];
o = (o = new Array()).concat(i.data);
i.data.sort(function(e, t) {
return t.coupon_discount - e.coupon_discount;
});
r = i.data[0];
for (a = 0; a < o.length; a++) o[a].goods_id == r.goods_id && o.splice(a, 1);
o.unshift(r);
return [ 2, o ];
}
});
});
};
t.getRandomGoods = function(t, n) {
void 0 === n && (n = 0);
return __awaiter(this, void 0, void 0, function() {
var i, o, r;
return __generator(this, function(a) {
switch (a.label) {
case 0:
return [ 4, e.Account.getAccountSafe() ];

case 1:
i = a.sent();
o = {
openid: i.openid,
game_id: e.Env.gameId
};
return [ 4, e.Platform.instance.httpRequest(e.Env.extServer + "/v1/pdd/goods", o) ];

case 2:
r = a.sent();
return [ 2, this.getNeedGoods(r.data.goods_list, n, t) ];
}
});
});
};
t.getRedbagGoods = function() {
return __awaiter(this, void 0, void 0, function() {
var t, n, i;
return __generator(this, function(o) {
switch (o.label) {
case 0:
return [ 4, e.Account.getAccountSafe() ];

case 1:
t = o.sent();
n = {
openid: t.openid,
game_id: e.Env.gameId
};
return [ 4, e.Platform.instance.httpRequest(e.Env.extServer + "/v1/pdd/recommend", n, "get", 0) ];

case 2:
return [ 2, (i = o.sent()).data[Math.floor(Math.random() * i.data.length)] ];
}
});
});
};
t.getNeedGoods = function(t, n, i, o) {
void 0 === o && (o = !0);
var r, a, c = e.Platform.instance.getStorage("pdd_repeat_goods") || [];
if (c.length > 50) {
c = [];
e.Platform.instance.setStorage("pdd_repeat_goods", []);
}
if ((r = t.filter(function(e) {
return !(0 == e.coupon_discount || !e.coupon_discount) && (!(e.coupon_discount <= n || e.coupon_discount > i) && -1 == c.indexOf(e.goods_id));
})).length <= 0) return null;
a = o ? r[Math.floor(Math.random() * r.length)] : r[0];
c.push(a.goods_id);
e.Platform.instance.setStorage("pdd_repeat_goods", c);
return a;
};
return t;
}();
t.PDD = n;
})(e.ext || (e.ext = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
e.init = function(n, i, o, r) {
void 0 === r && (r = !1);
e.Env.init;
e.Env.gameId = n;
e.Env.gamePath = i;
e.Env.init = !0;
e.Env.isNeedUnionid = r;
e.Env.launchOptions = e.Platform.instance.getLaunchOptions();
e.Platform.instance.onHide();
t("SDK初始化/启动参数", "gameId=", n, "gamePath=", i, "launchOptions", JSON.stringify(e.Env.launchOptions));
return new Promise(function(i, o) {
e.Platform.instance.getUserAccount(e.Env.launchOptions).then(function(r) {
if (e.Account.setAccount(r)) {
var a = e.utils.parseAccountSource(e.Env.launchOptions);
t("上报用户来源成功", "sourceType=", a.sourceType, "sourceId=", a.sourceId);
e.Report.reportAppRun(e.Env.launchOptions);
i(r.openid);
e.Account.fetchAgTag();
e.Account.checkExternalAdUser();
e.Hack.getOpenStatus(n + "", r.openid);
e.Platform.instance.setTimeout(e.Hack.forceFetchOpenStatus, 1e4);
e.Share.loadShareInfoList();
e.Ad.loadAdData();
e.Account.fetchUserEarliestTimeSafe();
e.Hack.getOpenLevel("", n).then(function(t) {
e.Env.shareUrl = t && t.shareUrl ? t.shareUrl : "";
e.Env.wxpublicUrl = t && t.wxpublicUrl ? t.wxpublicUrl : "";
});
e.Platform.instance.handleAfterLogin(r, e.Env.launchOptions);
} else {
t("初始化用户账号失败", JSON.stringify(r));
o("初始化用户账号失败");
}
}).catch(function(e) {
t("初始化失败:", e);
o(e);
});
});
};
e.initWithAccount = function(n, i, o, r, a) {
void 0 === a && (a = !1);
return new Promise(function(r, c) {
e.Env.init;
e.Env.launchOptions = e.Platform.instance.getLaunchOptions();
e.Platform.instance.onHide(o.openid);
e.Env.gameId = n;
e.Env.gamePath = i;
e.Env.init = !0;
e.Env.isNeedUnionid = a;
e.Share.loadShareInfoList();
e.Ad.loadAdData();
t("SDK初始化/启动参数", "gameId=", n, "gamePath=", i, "account=", o, "launchOptions=", JSON.stringify(e.Env.appLaunchOptions));
if (e.Account.setAccount(o)) {
var s = e.utils.parseAccountSource(e.Env.launchOptions);
e.Account.setAccountSource(s.sourceType, s.sourceId).then(function() {
r();
e.Hack.getOpenLevel("", n).then(function(t) {
e.Env.shareUrl = t && t.shareUrl ? t.shareUrl : "";
e.Env.wxpublicUrl = t && t.wxpublicUrl ? t.wxpublicUrl : "";
});
e.Account.fetchUserEarliestTimeSafe();
}).catch(function() {
c();
});
e.Account.checkExternalAdUser();
e.Hack.getOpenStatus(n + "", o.openid);
e.Platform.instance.setTimeout(e.Hack.forceFetchOpenStatus, 1e4);
e.Account.fetchAgTag();
e.Report.reportAppRun(e.Env.launchOptions);
e.Platform.instance.handleAfterLogin(o, e.Env.launchOptions);
} else {
c();
t("初始化用户账号失败", JSON.stringify(o));
}
});
};
function t() {
for (var t = [], n = 0; n < arguments.length; n++) t[n] = arguments[n];
for (var i = "", o = 0; o < t.length; o++) i += t[o];
e.Env.showLog && console.log("mpsdk" + i);
}
e.log = t;
e.checkUpdate = function() {
var t = new Date(), n = t.toLocaleDateString() + "/" + Math.floor(t.getHours() / 12);
if (e.Platform.instance.getStorage("lastUpdate") != n) {
e.Platform.instance.setStorage("lastUpdate", n);
if (-1 != [ "egret", "cocos" ].indexOf(e.Platform.platformType) || "devtools" == e.Platform.instance.getSystem().platform) {
var i = "background:#CC3399;color:#FFFF99";
console.log("mpsdk %csdk.funcell123.com仅在调试环境检测SDK版本更新，如果发生错误可关闭合法域名校验或忽略此问题（不必添加到合法域名）", i);
e.Platform.instance.httpRequest("http://sdk.funcell123.com/mpsdk/media/version.php").then(function(t) {
if (t.version != e.Env.version || t.build != e.Env.build) {
console.log("mpsdk %c当前版本号：" + e.Env.version + " build " + e.Env.build, i);
console.log("mpsdk %c最新版本号：" + t.version + " build " + t.build, i);
console.log("mpsdk %c建议更新SDK，详情：http://sdk.funcell123.com/mpsdk", i);
}
});
}
}
};
e.getStatus = function(n) {
n = n || e.Env.gameId;
return new Promise(function(i, o) {
n ? e.Platform.instance.httpRequest(e.Env.loginServer + "/MiniGame/data/getIp.action?gameId=" + n).then(function(e) {
if (e.open) try {
var n = JSON.parse(e.open);
n.ipCheck = e.check.toString();
"0" == n.ipCheck && (n.status = "0");
i(n);
} catch (e) {
t("获取状态控制参数失败，参数解析错误");
return;
} else t("获取状态控制参数失败，服务端状态控制参数未设置");
}) : t("获取状态控制参数失败，请先初始化SDK或传入gameId参数");
});
};
e.getOpenLevel = function(t, n) {
return e.Hack.getOpenLevel(t, n);
};
e.getServerTime = function() {
return __awaiter(this, void 0, void 0, function() {
return __generator(this, function(t) {
switch (t.label) {
case 0:
return [ 4, e.Platform.instance.httpRequest(e.Env.friendServer + "/MiniFriend/data/getServerTime.action") ];

case 1:
return [ 2, t.sent().ServerTime ];
}
});
});
};
})(mpsdk || (mpsdk = {}));

mpsdk.log("version", mpsdk.Env.version, "build", mpsdk.Env.build);

if ("object" == typeof worker && worker.onMessage) worker.onMessage(function(e) {
if (e && "gif" == e.msg && e.id && e.buffer) {
var t = new mpsdk.gif.GifReader(e.buffer);
if (t.valid) for (var n = 0; n < t.images.length; n++) t.getFrame(n);
var i = t;
i._dv = mpsdk.utils.buffer2String(t.dataView.buffer);
if (t.valid) {
i._globalHeadData = mpsdk.utils.buffer2String(t.globalHeadData);
i._globalPalette = mpsdk.utils.buffer2String(t.globalPalette);
for (n = 0; n < t.images.length; n++) {
i._images[n].localPalette = mpsdk.utils.buffer2String(t.images[n].localPalette);
i._images[n].frameData = mpsdk.utils.buffer2String(t.images[n].frameData);
}
}
worker.postMessage({
msg: "gif",
id: e.id,
reader: i
});
}
}); else {
mpsdk.checkUpdate();
("object" != typeof wx || wx.redirectTo) && "undefined" != typeof module ? module.exports = mpsdk : window.mpsdk = mpsdk;
}

(function(e) {
(function(e) {
var t = function() {
function e() {}
e.getGifAnim = function(e, t, n) {
return new Promise(function(i, o) {
cc.loader.load([ e, t ], function(r, a) {
if (r) {
console.log(r);
o && o(r);
} else {
for (var c = a.getContent(t), s = a.getContent(e), d = [], u = 0; u < c.frames.length; u++) {
var l = parseInt(c.frames[u].filename.split(".")[0]), p = c.frames[u].frame, f = new cc.SpriteFrame(s, cc.rect(p.x, p.y, p.w, p.h));
d[l] = f;
}
var h = n.addComponent(cc.Animation), g = cc.AnimationClip.createWithSpriteFrames(d, 16);
g.name = "gifAnim";
g.wrapMode = cc.WrapMode.Loop;
h.addClip(g);
h.play("gifAnim");
i && i(h);
}
});
});
};
return e;
}();
e.CocosGifAnim = t;
})(e.gif || (e.gif = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(e) {
var t = function() {
function e() {}
e.Load = function(e, t, i, o, r) {
var a = this;
return new Promise(function(c, s) {
if (e.indexOf(".gif") >= 0) {
console.log("LayaGifAnimLoader::Load->不支持Gif格式的图", e);
s && s(null);
} else {
t ? Laya.loader.load([ e, t ], Laya.Handler.create(a, function(a) {
for (var s = Laya.loader.getRes(t), d = Laya.loader.getRes(e), u = [], l = 0; l < s.frames.length; l++) {
var p = parseInt(s.frames[l].filename.split(".")[0]), f = s.frames[l].frame;
u[p] = f;
}
var h = new n();
h.Init(u, d, i, o, r);
c && c(h);
})) : Laya.loader.load(e, Laya.Handler.create(a, function(e) {
var t = e, a = new n();
a.Init([], t, i, o, r);
c && c(a);
}));
Laya.loader.on(Laya.Event.ERROR, a, function(e) {
console.log("LayaGifAnimLoader::Load->加载动图出错 ", e);
s && s(e);
});
}
});
};
e.Pool = [];
return e;
}();
e.LayaGifAnimLoader = t;
var n = function() {
function e() {
this._nowpage = 0;
this._frame = 0;
}
e.prototype.Init = function(e, t, n, i, o) {
var r = this;
this._texList = [];
this.View = new Laya.Sprite();
this._icon = new Laya.Sprite();
this._title = new Laya.Sprite();
if (null == e || 0 == e.length) this._texList.push(t); else for (var a = 0; a < e.length; a++) {
var c = e[a], s = Laya.Texture.createFromTexture(t, c.x, c.y, c.w, c.h);
this._texList.push(s);
}
this._icon.width = this._texList[0].width;
this._icon.height = this._texList[0].height;
this.View.addChild(this._icon);
if (i && i.length > 0) {
n && Laya.loader.load(n, Laya.Handler.create(this, function(e) {
e.width = r._icon.width;
e.height = r._icon.height / 4;
r._title.texture = e;
}));
this.View.addChild(this._title);
this._title.y = this._icon.height;
this._title.width = this._icon.width;
this._title.height = this._icon.height / 4;
i.length > 5 && (i = i.substr(0, 4) + "...");
this._titleTxt = new Laya.Text();
this._titleTxt.width = this._title.width;
this._titleTxt.height = this._title.height;
this._titleTxt.fontSize = Math.floor(this._title.width - 10) / 5;
this._title.addChild(this._titleTxt);
this._titleTxt.align = "center";
this._titleTxt.valign = "middle";
this._titleTxt.zOrder = 10;
this._titleTxt.text = i;
this._titleTxt.bold = o;
}
this.View.width = this._icon.width;
this.View.height = this._icon.height + this._title.height;
this.changeFrame();
};
e.prototype.changeFrame = function() {
if (null != this._texList) {
this._nowpage >= this._texList.length && (this._nowpage = 0);
this._icon.graphics.clear();
this._icon.graphics.drawTexture(this._texList[this._nowpage], 0, 0);
this._nowpage++;
}
};
e.prototype.Update = function() {
this._frame++;
this._texList.length > 1 && this._frame % 3 == 0 && this.changeFrame();
};
e.prototype.Destroy = function() {
this.View.removeSelf();
this.View.destroy(!0);
for (var e = 0, t = this._texList; e < t.length; e++) {
t[e].destroy(!0);
}
this._texList = [];
};
return e;
}();
e.LayaGifAnim = n;
})(e.gif || (e.gif = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function t() {}
t.init = function(e) {
var t = this;
this.mWidgetList = [];
this.mRootNode = new cc.Node("SuggestBottom");
this.mRootNode.setContentSize(600, 500);
var n = new cc.Node("bg");
n.parent = this.mRootNode;
n.setPosition(cc.Vec2.ZERO);
var i = n.addComponent(cc.Widget);
this.initWidget(i, 0, 0, 0, 0);
this.mBgSprite = n.addComponent(cc.Sprite);
this.getBase64SpFrame(this.mBgBase64, function(e) {
e.insetLeft = 22;
e.insetTop = 22;
e.insetRight = 47;
e.insetBottom = 47;
t.mBgSprite.type = cc.Sprite.Type.SLICED;
t.mBgSprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
t.mBgSprite.spriteFrame = e;
});
var o = new cc.Node("bgFrame");
o.parent = this.mRootNode;
o.setPosition(cc.Vec2.ZERO);
var r = n.addComponent(cc.Widget);
this.initWidget(r, 0, 0, 0, 0);
this.mFrameSprite = o.addComponent(cc.Sprite);
this.getBase64SpFrame(this.mFrameBase64, function(e) {
e.insetLeft = 9;
e.insetTop = 9;
e.insetRight = 9;
e.insetBottom = 9;
t.mFrameSprite.type = cc.Sprite.Type.SLICED;
t.mFrameSprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
t.mFrameSprite.spriteFrame = e;
});
var a = new cc.Node("nodeScroll");
a.parent = this.mRootNode;
a.setPosition(cc.Vec2.ZERO);
var c = a.addComponent(cc.Widget);
this.initWidget(c, 0, 0, 0, 0);
this.mScrollView = a.addComponent(cc.ScrollView);
this.mScrollView.horizontal = !1;
this.mScrollView.vertical = !1;
this.mScrollView.inertia = !1;
var s = new cc.Node("nodeView");
s.parent = a;
s.setPosition(cc.Vec2.ZERO);
s.addComponent(cc.Mask);
var d = s.addComponent(cc.Widget);
this.initWidget(d, 0, 0, 0, 0);
this.mContent = new cc.Node("mContent");
this.mContent.parent = s;
this.mContent.setPosition(cc.Vec2.ZERO);
var u = this.mContent.addComponent(cc.Widget);
this.mScrollView.content = this.mContent;
this.initWidget(u, 0, 0, 0, 0);
console.log(this.mContent.getContentSize());
var l = this.mContent.addComponent(cc.Layout);
l.type = cc.Layout.Type.GRID;
l.resizeMode = cc.Layout.ResizeMode.CONTAINER;
l.startAxis = cc.Layout.AxisDirection.HORIZONTAL;
l.horizontalDirection = cc.Layout.HorizontalDirection.LEFT_TO_RIGHT;
l.verticalDirection = cc.Layout.VerticalDirection.TOP_TO_BOTTOM;
l.paddingLeft = 20;
l.paddingRight = 20;
l.paddingTop = 15;
l.paddingBottom = 15;
l.spacingX = 10;
l.spacingY = 10;
this.mSignleCache = new cc.Node("itemCacheNode");
this.mSignleCache.setContentSize(180, 230);
this.mSignleCache.setPosition(cc.Vec2.ZERO);
this.mSignleCache.active = !0;
var p = this.mSignleCache.addComponent(cc.Layout);
p.type = cc.Layout.Type.VERTICAL;
p.resizeMode = cc.Layout.ResizeMode.NONE;
p.verticalDirection = cc.Layout.VerticalDirection.TOP_TO_BOTTOM;
p.paddingTop = 0;
p.paddingBottom = 0;
p.spacingY = 0;
var f = new cc.Node("itemIconNode");
f.setContentSize(180, 180);
f.setPosition(cc.Vec2.ZERO);
f.parent = this.mSignleCache;
var h = f.addComponent(cc.Sprite);
h.sizeMode = cc.Sprite.SizeMode.CUSTOM;
h.enabled = !0;
var g = new cc.Node("itemTitleNode");
g.setContentSize(180, 50);
g.setPosition(cc.Vec2.ZERO);
g.parent = this.mSignleCache;
var m = g.addComponent(cc.Sprite);
m.sizeMode = cc.Sprite.SizeMode.CUSTOM;
m.enabled = !0;
var v = new cc.Node("itemTitleLabelNode");
v.setContentSize(180, 50);
v.setPosition(cc.Vec2.ZERO);
v.parent = g;
var y = v.addComponent(cc.RichText);
y.fontSize = 28;
y.horizontalAlign = cc.macro.TextAlignment.CENTER;
y.maxWidth = 180;
y.lineHeight = 50;
y.useSystemFont = !0;
y.enabled = !0;
};
t.create = function(n, i, o) {
var r = this, a = n.length > 6 ? 6 : n.length;
if (this.mRootNode) {
this.mRootNode.destroyAllChildren();
this.mRootNode.destroy();
}
this.init(a);
this.mHorizonList = [];
for (var c = function(t) {
var i = cc.instantiate(s.mSignleCache);
i.parent = s.mContent;
n[t].atlas_photo && n[t].atlas_config ? e.gif.CocosGifAnim.getGifAnim(n[t].atlas_photo, n[t].atlas_config, s.getItemIconNode(i)).then(function() {}) : s.loadUrlRes(n[t].icon, s.getItemIconNode(i));
s.setItemNameNode(n[t], i);
i.on(cc.Node.EventType.TOUCH_END, function() {
e.Ad.click(n[t]);
});
s.mHorizonList.push(i);
}, s = this, d = 0; d < a; d++) c(d);
t.mRootNode.on("size-changed", function() {
r.itemAdapt(a);
});
this.mScrollView.node.on(cc.Node.EventType.TOUCH_START, function() {});
this.mBgSprite.node.color = new cc.Color(i.getR(), i.getG(), i.getB());
this.mBgSprite.node.opacity = i.getA();
if (o) {
this.mFrameSprite.node.active = !0;
this.mFrameSprite.node.color = new cc.Color(o.getR(), o.getG(), o.getB());
this.mFrameSprite.node.opacity = o.getA();
} else this.mFrameSprite.node.active = !1;
this.itemAdapt(a);
return this.mRootNode;
};
t.itemAdapt = function(e) {
var t = 600, n = 500;
if (3 == e) {
t = 600;
n = 260;
} else if (2 == e) {
t = 410;
n = 260;
} else if (1 == e) {
t = 220;
n = 260;
}
this.mRootNode.setContentSize(t, n);
for (var i = 0, o = this.mWidgetList; i < o.length; i++) {
o[i].updateAlignment();
}
var r = this.mContent.getComponent(cc.Layout);
r.updateLayout();
r.getComponentsInChildren(cc.Layout).forEach(function(e) {
e.updateLayout();
});
this.mScrollView.scrollToLeft(0);
};
t.setItemNameNode = function(e, t) {
if (e && t && cc.isValid(t)) {
var n = t.getChildByName("itemTitleNode");
n.width = 180;
n.height = 50;
var i = n.getChildByName("itemTitleLabelNode"), o = e.title, r = e.title_bg, a = e.is_bold;
o && o.length > 5 && (o = o.substring(0, 4) + "...");
i.width = 180;
i.height = 50;
r && this.loadUrlRes(r, n);
a && "2" == a && (o = "<b>" + o + "</b>");
i.getComponent(cc.RichText).string = o;
}
};
t.getItemIconNode = function(e) {
var t = e.getChildByName("itemIconNode");
t.width = 180;
t.height = 180;
return t;
};
t.loadUrlRes = function(e, t) {
if (e && t) {
var n = function(e) {
if (cc.isValid(t)) {
var n = t.width, i = t.height;
t.getComponent(cc.Sprite).spriteFrame = e;
t.width = n;
t.height = i;
}
};
cc.loader.load({
url: e,
type: "png"
}, function(e, t) {
e ? cc.error(e.message || e) : n(new cc.SpriteFrame(t));
}.bind(this));
}
};
t.getBase64SpFrame = function(e, t) {
var n = new Image();
n.src = e;
n.onload = function() {
var e = new cc.Texture2D();
e.initWithElement(n);
e.handleLoadedTexture();
var i = new cc.SpriteFrame(e);
t && t(i);
};
};
t.initWidget = function(e, t, n, i, o) {
this.mWidgetList.push(e);
e.alignMode = cc.Widget.AlignMode.ON_WINDOW_RESIZE;
if (void 0 !== t) {
e.isAlignTop = !0;
e.top = t;
}
if (void 0 !== n) {
e.isAlignLeft = !0;
e.left = n;
}
if (void 0 !== i) {
e.isAlignRight = !0;
e.right = i;
}
if (void 0 !== o) {
e.isAlignBottom = !0;
e.bottom = o;
}
};
t.setBgFrame = function(e) {
this.mFrameSprite.spriteFrame = e;
};
t.setBg = function(e) {
this.mBgSprite.spriteFrame = e;
};
t.mBgBase64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAABoCAYAAABPP4jqAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFGRTc0RUYzM0NDMDExRTk5QzdFQTg3RTAxOTlBN0FEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFGRTc0RUY0M0NDMDExRTk5QzdFQTg3RTAxOTlBN0FEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QUZFNzRFRjEzQ0MwMTFFOTlDN0VBODdFMDE5OUE3QUQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QUZFNzRFRjIzQ0MwMTFFOTlDN0VBODdFMDE5OUE3QUQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6tPwHyAAADr0lEQVR42uzcS0hUYRQH8OPozEiSWWpWhBVJb9RKiyJt0dOVWBa1qEVKbwiqTbRo0SaIICLohQUJvegFERRZYDNqqaUTRTHRA9/DZEpkVk7YOTP32ud1amo1c6f/gf9iztyR+5t757ufmxOzfcV0+ovK5Czi5HEWcMZxrBS+6uE0cxo5Do6T8yzUh+JCvF/C2cyZR5FVCZxpWtZpvVrOaU7Zv2JTOCc4xWSemqelgLON4zUeYAnyoQ2cBpNB1VrNeao5/nhl5YDzQw6Ks1LmwiWUxZk0PZtGJI/298JV33q/UPeHDmp+85JcVRX0rOYB+Xx96iHjFUe53oxRFqg0Tr124EBlLVxKRaV7KXVcesReSm9bE90sO0KNVfeMb7VwcjgeeRGbm5FKyjcwf+D+tsRS4abdtHb7fkoYPiKi71s5v7mLC8hmjye3q5b6+/v1txI5kzmXVewOzh71Dwh02ZoSU/1YJ8+cQ1abnV411KhtuXU7ZbXWF6iN6ruz85abDqqXnLecf5C1yL8az1Wfo7LwFG/ZR2au4q37jAuo+HIs2s7o11XNX0FJKWmmxiYlp9Gc/JXG9iKLtgX8tfouWErRUPKYNFSeYHPVzoSps6ICmz5liMN/G49RO4kjU6ICmzgq1dgaK1jboN2S1RYV2CA7PKuF/qMCFlhggQUWWGCBBRZYYIEFFlhggQUWWGCBBRZYYIEFFlhggQUWWGCBBRZYYIEFFlhggQUWWGCBBRZYYIEFFlhggQUWWGCBBRZYYIEFFlhggQUWWGD/d+xntfG1tycqYIZpfVJ9gm1XO58+eqMCG8TRLthBM0Zb37qjAtvkfm5s1QvWoXZcNRVRgXVV3ze2HIJ1qp2Gh3ep+4PH1NDuTg89ddw1tp2CfUKBCbIDP+xrpw6ZGnvt5CHy9X1XW3X6bSw1aD6qfCsVV8+aEirnHeSq+n36RE2RZ1NgXrC/3I2P/WM5J07NNA208tYFunHmsDo+VOomadNC1VmplZz1FJgv6v/AizoHtb1/TekZMyJ6XqoMhr147ADdv3rOCG3lFOp7CRUrDXk4FalHdzS9Iefty+RpeSffAFnt8WSPH0YWS/g2Xz98PurytvPd94juXDpFV44f9F+UILWTU62/iAkyk1zmih7ljDLxGtXF2UXKbOPf7Y3lAPmhXjcp9IZ2/uV/+4+A3Osy7rpUfSxFeNVq57uKAkOch1SoafNlWmT6sz5aX6bmhnu0vuzy27Q9gj5avz7Uh34KMADL1+NYuYA3AwAAAABJRU5ErkJggg==";
t.mFrameBase64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAB5SURBVHja7M8xDkFBFEDRM+NXfqKg0SgswR6sSamyDrEGpUqvsgdKtYj8PI0VjKlk7gJOclNErLHFA4GkrB6nFBEX7HHEqBAcMMOuwx0HPP3WDef83Zyr0zirXAMb2MD/Al+VvHfGApNK4DJFxAYrXNEVQgOm6D8DAJXuGl33vXhMAAAAAElFTkSuQmCC";
t.mHorizonList = [];
t.mWidgetList = [];
return t;
}();
t.CCADGridUI = n;
})(e.ui || (e.ui = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function t() {}
t.init = function() {
var e = this;
this.nowIndex = 0;
this.mWidgetList = [];
this.mRootNode = new cc.Node("SuggestBottom");
this.mRootNode.setContentSize(630, 200);
var t = new cc.Node("bg");
t.parent = this.mRootNode;
var n = t.addComponent(cc.Widget);
this.initWidget(n, 0, 0, 0, 0);
this.mBgSprite = t.addComponent(cc.Sprite);
this.getBase64SpFrame(this.mBgBase64, function(t) {
t.insetLeft = 22;
t.insetTop = 22;
t.insetRight = 47;
t.insetBottom = 47;
e.mBgSprite.type = cc.Sprite.Type.SLICED;
e.mBgSprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
e.mBgSprite.spriteFrame = t;
});
var i = new cc.Node("bgFrame");
i.parent = this.mRootNode;
var o = t.addComponent(cc.Widget);
this.initWidget(o, 0, 0, 0, 0);
this.mFrameSprite = i.addComponent(cc.Sprite);
this.getBase64SpFrame(this.mFrameBase64, function(t) {
t.insetLeft = 9;
t.insetTop = 9;
t.insetRight = 9;
t.insetBottom = 9;
e.mFrameSprite.type = cc.Sprite.Type.SLICED;
e.mBgSprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
e.mFrameSprite.spriteFrame = t;
});
var r = new cc.Node("nodeScroll");
r.parent = this.mRootNode;
var a = r.addComponent(cc.Widget);
this.initWidget(a, 0, 50, 5, 0);
this.mScrollView = r.addComponent(cc.ScrollView);
this.mScrollView.horizontal = !0;
this.mScrollView.vertical = !1;
r.setPosition(cc.Vec2.ZERO);
var c = new cc.Node("nodeView");
c.parent = r;
c.setPosition(cc.Vec2.ZERO);
c.addComponent(cc.Mask);
this.mNodeView = c;
var s = c.addComponent(cc.Widget);
this.initWidget(s, 0, 0, 0, 0);
this.mContent = new cc.Node("mContent");
this.mContent.parent = c;
this.mContent.setPosition(cc.Vec2.ZERO);
var d = this.mContent.addComponent(cc.Widget);
this.mScrollView.content = this.mContent;
this.initWidget(d, 15, void 0, void 0, 15);
console.log(this.mContent.getContentSize());
var u = this.mContent.addComponent(cc.Layout);
u.type = cc.Layout.Type.HORIZONTAL;
u.resizeMode = cc.Layout.ResizeMode.NONE;
u.horizontalDirection = cc.Layout.HorizontalDirection.LEFT_TO_RIGHT;
u.paddingLeft = 0;
u.paddingRight = 0;
u.spacingX = 10;
var l = new cc.Node();
l.parent = this.mRootNode;
var p = l.addComponent(cc.Widget);
l.width = 40;
this.initWidget(p, 5, 5, void 0, 5);
l.color = cc.Color.WHITE;
this.mLikeLabel = l.addComponent(cc.Label);
this.mLikeLabel.string = "猜你喜欢";
this.mLikeLabel.verticalAlign = cc.Label.VerticalAlign.CENTER;
this.mLikeLabel.overflow = cc.Label.Overflow.SHRINK;
this.mSignleCache = new cc.Node("itemCacheNode");
this.mSignleCache.setContentSize(135, 170);
this.mSignleCache.setPosition(cc.Vec2.ZERO);
this.mSignleCache.active = !0;
var f = this.mSignleCache.addComponent(cc.Layout);
f.type = cc.Layout.Type.VERTICAL;
f.resizeMode = cc.Layout.ResizeMode.NONE;
f.verticalDirection = cc.Layout.VerticalDirection.TOP_TO_BOTTOM;
f.paddingTop = 0;
f.paddingBottom = 0;
f.spacingY = 0;
var h = new cc.Node("itemIconNode");
h.setContentSize(135, 135);
h.setPosition(cc.Vec2.ZERO);
h.parent = this.mSignleCache;
var g = h.addComponent(cc.Sprite);
g.sizeMode = cc.Sprite.SizeMode.CUSTOM;
g.enabled = !0;
var m = new cc.Node("itemTitleNode");
m.setContentSize(135, 35);
m.setPosition(cc.Vec2.ZERO);
m.parent = this.mSignleCache;
var v = m.addComponent(cc.Sprite);
v.sizeMode = cc.Sprite.SizeMode.CUSTOM;
v.enabled = !0;
var y = new cc.Node("itemTitleLabelNode");
y.setContentSize(135, 35);
y.setPosition(cc.Vec2.ZERO);
y.parent = m;
var S = y.addComponent(cc.RichText);
S.fontSize = 24;
S.horizontalAlign = cc.macro.TextAlignment.CENTER;
S.maxWidth = 135;
S.lineHeight = 35;
S.useSystemFont = !0;
S.enabled = !0;
this.actionAnim = cc.sequence(cc.scaleTo(.3, 1.3, 1.3), cc.scaleTo(1, 1, 1));
};
t.create = function(n, i, o) {
var r = this;
this.MaxCount = n.length;
this.MaxPage = Math.max(this.MaxCount - 4, 0);
this.nowShowCount = this.MaxCount > 4 ? 4 : this.MaxCount;
if (this.mRootNode) {
this.mRootNode.destroyAllChildren();
this.mRootNode.destroy();
}
this.init();
this.mHorizonList = [];
for (var a = function(t) {
var i = cc.instantiate(c.mSignleCache);
i.parent = c.mContent;
n[t].atlas_photo && n[t].atlas_config ? e.gif.CocosGifAnim.getGifAnim(n[t].atlas_photo, n[t].atlas_config, c.getItemIconNode(i)).then(function() {}) : c.loadUrlRes(n[t].icon, c.getItemIconNode(i));
c.setItemNameNode(n[t], i);
i.on(cc.Node.EventType.TOUCH_END, function() {
e.Ad.click(n[t]);
});
c.mHorizonList.push(i);
}, c = this, s = 0; s < this.MaxCount; s++) a(s);
t.mRootNode.on("size-changed", function() {
r.itemAdapt();
});
this.mScrollView.node.on(cc.Node.EventType.TOUCH_START, function() {
for (var e = t.mHorizonList, n = 0; n < e.length; n++) r.getItemIconNode(e[n]).getComponent(cc.Sprite).enabled = !0;
});
this.mBgSprite.node.color = new cc.Color(i.getR(), i.getG(), i.getB());
this.mBgSprite.node.opacity = i.getA();
if (o) {
this.mFrameSprite.node.active = !0;
this.mFrameSprite.node.color = new cc.Color(o.getR(), o.getG(), o.getB());
this.mFrameSprite.node.opacity = o.getA();
} else this.mFrameSprite.node.active = !1;
this.itemAdapt();
this.playAnim();
return this.mRootNode;
};
t.itemAdapt = function() {
for (var e = 0, t = this.mWidgetList; e < t.length; e++) {
t[e].updateAlignment();
}
var n = this.mNodeView.width, i = this.mNodeView.height, o = n / this.nowShowCount, r = Math.min(o - 10, i);
if (!((r -= 3) < 40)) {
for (var a = 0, c = this.mHorizonList; a < c.length; a++) {
c[a].setContentSize(135, 170);
}
var s = (n - (r = 135) * this.nowShowCount) / (this.nowShowCount + 1);
this.mContent.width = this.mHorizonList.length * (s + r) + r;
console.log("mContent.width:", this.mContent.width, " , spacing:", s, " , MaxWidth:", n, " , MaxHeight:", i);
var d = this.mContent.getComponent(cc.Layout);
d.spacingX = s;
d.updateLayout();
d.getComponentsInChildren(cc.Layout).forEach(function(e) {
e.updateLayout();
});
this.MaxPage = Math.max(this.mHorizonList.length - 4, 0);
this.MaxPage = this.MaxPage > 0 ? this.MaxPage : 0;
this.mScrollView.scrollToLeft(0);
}
};
t.playAnim = function() {
var e = this;
this.mScrollView.unscheduleAllCallbacks();
this.mScrollView.schedule(function() {
t.nowIndex <= 0 ? t.nowAngel = 1 : t.nowIndex >= t.mHorizonList.length - e.nowShowCount && (t.nowAngel = -1);
t.nowIndex += t.nowAngel > 0 ? 1 : -1;
t.mScrollView.scrollToPercentHorizontal(t.nowIndex / (t.mHorizonList.length - e.nowShowCount), 2);
for (var n = Math.floor(Math.random() * e.nowShowCount), i = t.mHorizonList, o = 0; o < i.length; o++) for (var r = 0; r < i.length; r++) r < t.nowIndex - 1 || r > t.nowIndex + e.nowShowCount ? e.getItemIconNode(i[r]).getComponent(cc.Sprite).enabled = !1 : e.getItemIconNode(i[r]).getComponent(cc.Sprite).enabled = !0;
var a = n + t.nowIndex;
a < 0 ? a = 0 : a >= i.length && (a = i.length - 1);
t.mHorizonList[a].runAction(t.actionAnim);
}, 5);
};
t.setItemNameNode = function(e, t) {
if (e && t && cc.isValid(t)) {
var n = t.getChildByName("itemTitleNode");
n.width = 135;
n.height = 35;
var i = n.getChildByName("itemTitleLabelNode"), o = e.title, r = e.title_bg, a = e.is_bold;
o && o.length > 5 && (o = o.substring(0, 4) + "...");
i.width = 135;
i.height = 135;
r && this.loadUrlRes(r, n);
a && "2" == a && (o = "<b>" + o + "</b>");
i.getComponent(cc.RichText).string = o;
}
};
t.getItemIconNode = function(e) {
var t = e.getChildByName("itemIconNode");
t.width = 135;
t.height = 135;
return t;
};
t.loadUrlRes = function(e, t) {
if (e && t) {
var n = function(e) {
if (cc.isValid(t)) {
var n = t.width, i = t.height;
t.getComponent(cc.Sprite).spriteFrame = e;
t.width = n;
t.height = i;
}
};
cc.loader.load({
url: e,
type: "png"
}, function(e, t) {
e ? cc.error(e.message || e) : n(new cc.SpriteFrame(t));
}.bind(this));
}
};
t.getBase64SpFrame = function(e, t) {
var n = new Image();
n.src = e;
n.onload = function() {
var e = new cc.Texture2D();
e.initWithElement(n);
e.handleLoadedTexture();
var i = new cc.SpriteFrame(e);
t && t(i);
};
};
t.initWidget = function(e, t, n, i, o) {
this.mWidgetList.push(e);
e.alignMode = cc.Widget.AlignMode.ON_WINDOW_RESIZE;
if (void 0 !== t) {
e.isAlignTop = !0;
e.top = t;
}
if (void 0 !== n) {
e.isAlignLeft = !0;
e.left = n;
}
if (void 0 !== i) {
e.isAlignRight = !0;
e.right = i;
}
if (void 0 !== o) {
e.isAlignBottom = !0;
e.bottom = o;
}
};
t.setBgFrame = function(e) {
this.mFrameSprite.spriteFrame = e;
};
t.setBg = function(e) {
this.mBgSprite.spriteFrame = e;
};
t.getLikeLabel = function() {
return this.mLikeLabel;
};
t.getGifAnim = function(e, t, n) {
return new Promise(function(i, o) {
cc.loader.load([ e, t ], function(r, a) {
if (r) {
console.log(r);
o && o(r);
} else {
for (var c = a.getContent(t), s = a.getContent(e), d = [], u = 0; u < c.frames.length; u++) {
var l = c.frames[u].frame, p = new cc.SpriteFrame(s, cc.rect(l.x, l.y, l.w, l.h));
d[u] = p;
}
var f = n.addComponent(cc.Animation), h = cc.AnimationClip.createWithSpriteFrames(d, 8);
h.name = "gifAnim";
h.wrapMode = cc.WrapMode.Loop;
f.addClip(h);
f.play("gifAnim");
i && i(f);
}
});
});
};
t.mBgBase64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAABoCAYAAABPP4jqAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFGRTc0RUYzM0NDMDExRTk5QzdFQTg3RTAxOTlBN0FEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFGRTc0RUY0M0NDMDExRTk5QzdFQTg3RTAxOTlBN0FEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QUZFNzRFRjEzQ0MwMTFFOTlDN0VBODdFMDE5OUE3QUQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QUZFNzRFRjIzQ0MwMTFFOTlDN0VBODdFMDE5OUE3QUQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6tPwHyAAADr0lEQVR42uzcS0hUYRQH8OPozEiSWWpWhBVJb9RKiyJt0dOVWBa1qEVKbwiqTbRo0SaIICLohQUJvegFERRZYDNqqaUTRTHRA9/DZEpkVk7YOTP32ud1amo1c6f/gf9iztyR+5t757ufmxOzfcV0+ovK5Czi5HEWcMZxrBS+6uE0cxo5Do6T8yzUh+JCvF/C2cyZR5FVCZxpWtZpvVrOaU7Zv2JTOCc4xWSemqelgLON4zUeYAnyoQ2cBpNB1VrNeao5/nhl5YDzQw6Ks1LmwiWUxZk0PZtGJI/298JV33q/UPeHDmp+85JcVRX0rOYB+Xx96iHjFUe53oxRFqg0Tr124EBlLVxKRaV7KXVcesReSm9bE90sO0KNVfeMb7VwcjgeeRGbm5FKyjcwf+D+tsRS4abdtHb7fkoYPiKi71s5v7mLC8hmjye3q5b6+/v1txI5kzmXVewOzh71Dwh02ZoSU/1YJ8+cQ1abnV411KhtuXU7ZbXWF6iN6ruz85abDqqXnLecf5C1yL8az1Wfo7LwFG/ZR2au4q37jAuo+HIs2s7o11XNX0FJKWmmxiYlp9Gc/JXG9iKLtgX8tfouWErRUPKYNFSeYHPVzoSps6ICmz5liMN/G49RO4kjU6ICmzgq1dgaK1jboN2S1RYV2CA7PKuF/qMCFlhggQUWWGCBBRZYYIEFFlhggQUWWGCBBRZYYIEFFlhggQUWWGCBBRZYYIEFFlhggQUWWGCBBRZYYIEFFlhggQUWWGCBBRZYYIEFFlhggQUWWGD/d+xntfG1tycqYIZpfVJ9gm1XO58+eqMCG8TRLthBM0Zb37qjAtvkfm5s1QvWoXZcNRVRgXVV3ze2HIJ1qp2Gh3ep+4PH1NDuTg89ddw1tp2CfUKBCbIDP+xrpw6ZGnvt5CHy9X1XW3X6bSw1aD6qfCsVV8+aEirnHeSq+n36RE2RZ1NgXrC/3I2P/WM5J07NNA208tYFunHmsDo+VOomadNC1VmplZz1FJgv6v/AizoHtb1/TekZMyJ6XqoMhr147ADdv3rOCG3lFOp7CRUrDXk4FalHdzS9Iefty+RpeSffAFnt8WSPH0YWS/g2Xz98PurytvPd94juXDpFV44f9F+UILWTU62/iAkyk1zmih7ljDLxGtXF2UXKbOPf7Y3lAPmhXjcp9IZ2/uV/+4+A3Osy7rpUfSxFeNVq57uKAkOch1SoafNlWmT6sz5aX6bmhnu0vuzy27Q9gj5avz7Uh34KMADL1+NYuYA3AwAAAABJRU5ErkJggg==";
t.mFrameBase64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAB5SURBVHja7M8xDkFBFEDRM+NXfqKg0SgswR6sSamyDrEGpUqvsgdKtYj8PI0VjKlk7gJOclNErLHFA4GkrB6nFBEX7HHEqBAcMMOuwx0HPP3WDef83Zyr0zirXAMb2MD/Al+VvHfGApNK4DJFxAYrXNEVQgOm6D8DAJXuGl33vXhMAAAAAElFTkSuQmCC";
t.mHorizonList = [];
t.MaxCount = 0;
t.MaxPage = 0;
t.nowIndex = 0;
t.animPosX = 0;
t.nowAngel = 1;
t.nowShowCount = 0;
t.mWidgetList = [];
return t;
}();
t.CCADListUI = n;
})(e.ui || (e.ui = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function t() {}
t.createBg = function() {
this.View.width = this._width;
this.View.height = this._height;
if (null == this._bg) {
this._bg = new Laya.Sprite();
this.View.addChild(this._bg);
}
this._bg.graphics.clear();
this._bg.graphics.drawRect(0, 0, this._width, this._height, this._bgColor, this._lineColor);
this._bg.width = this._width;
this._bg.height = this._height;
this._bg.alpha = .6;
if (null == this._iconPanel) {
this._iconPanel = new Laya.Sprite();
this._viewPortPanel = new Laya.Sprite();
this.View.addChild(this._viewPortPanel);
this._viewPortPanel.addChild(this._iconPanel);
Laya.timer.frameLoop(1, this, this.update);
}
this._viewPortPanel.width = this._iconPanel.width = .9 * this._width;
this._viewPortPanel.height = this._iconPanel.height = .8 * this._height;
this._viewPortPanel.x = .05 * this._width;
this._viewPortPanel.y = .1 * this._height;
this._viewPortPanel.scrollRect = new Laya.Rectangle(0, 0, this._viewPortPanel.width, this._viewPortPanel.height);
};
t.createMovieItem = function(n, i) {
var o = this, r = n.atlas_photo, a = n.atlas_config, c = n.title_bg, s = n.icon, d = n.title, u = !!n.is_bold && 2 == n.is_bold, l = r || s;
if (null != l) {
var p = this._viewPortPanel.width / t._hlen, f = this._viewPortPanel.height / t._vlen, h = p > f ? f : p;
e.gif.LayaGifAnimLoader.Load(l, a, c, d, u).then(function(r) {
var a = r.View.width, c = r.View.height, s = h / c, d = a * s, u = c * s, l = h - a;
r.View.scale(s, s);
r.View.pivot(a / 2, u / 2);
r.View.x = d / 2 + (d + l) * (i % t._hlen);
r.View.y = u / 2 + (u + l) * Math.floor(i / t._hlen);
o._iconPanel.addChild(r.View);
o._iconPanel.width = r.View.x + d - d / 2;
r.View.on(Laya.Event.CLICK, o, function() {
o._downX >= 0 || e.Ad.click(n);
});
o._movieList.push(r);
}).catch(function() {
if (null != r) {
var e = n;
e.atlas_photo = null;
e.atlas_config = null;
o.createMovieItem(e, i);
}
});
}
};
t.createMovieList = function(e) {
for (var t = e.length, n = 0; n < t; n++) this.createMovieItem(e[n], n);
};
t.removeMovieList = function() {
if (null != this._movieList) for (var e = this._movieList.length - 1; e >= 0; e--) {
this._movieList[e].Destroy();
this._movieList.splice(e, 1);
} else this._movieList = [];
};
t.updateIconPanel = function() {};
t.updateMovie = function() {
for (var e = 0, t = this._movieList; e < t.length; e++) {
t[e].Update();
}
};
t.update = function(e) {
this.updateIconPanel();
this.updateMovie();
};
t.Create = function(e, t, n) {
void 0 === t && (t = "#000000");
void 0 === n && (n = "");
this._bgColor = t;
this._lineColor = n;
this.removeMovieList();
this.createBg();
this.createMovieList(e);
return this.View;
};
Object.defineProperty(t, "View", {
get: function() {
null == this._root && (this._root = new Laya.Sprite());
return this._root;
},
enumerable: !0,
configurable: !0
});
t._width = 600;
t._height = 500;
t._hlen = 3;
t._vlen = 2;
t._bgColor = "#000000";
t._lineColor = "#ffffff";
t._downX = -1;
return t;
}();
t.LayaADGridUI = n;
})(e.ui || (e.ui = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function t() {}
t.createBg = function() {
this.View.width = this._width;
this.View.height = this._height;
if (null == this._bg) {
this._bg = new Laya.Sprite();
this.View.addChild(this._bg);
}
this._bg.graphics.clear();
this._bg.graphics.drawRect(0, 0, this._width, this._height, this._bgColor, this._lineColor);
this._bg.width = this._width;
this._bg.height = this._height;
this._bg.alpha = .6;
if (null == this._cnxhtitle) {
this._cnxhtitle = new Laya.Text();
this.View.addChild(this._cnxhtitle);
}
this._cnxhtitle.x = .02 * this._width;
this._cnxhtitle.y = .1 * this._height;
this._cnxhtitle.width = .06 * this._width;
this._cnxhtitle.height = .8 * this._height;
this._cnxhtitle.leading = 2;
this._cnxhtitle.fontSize = Math.floor(this._cnxhtitle.height / 4) - 2;
this._cnxhtitle.color = this._lineColor;
this._cnxhtitle.text = "猜\n你\n喜\n欢";
if (null == this._iconPanel) {
this._iconPanel = new Laya.Sprite();
this._viewPortPanel = new Laya.Sprite();
this.View.addChild(this._viewPortPanel);
this._viewPortPanel.addChild(this._iconPanel);
this._viewPortPanel.on(Laya.Event.MOUSE_DOWN, this, this.onDown);
this._viewPortPanel.on(Laya.Event.MOUSE_MOVE, this, this.onMove);
this._viewPortPanel.on(Laya.Event.MOUSE_OUT, this, this.onUp);
this._viewPortPanel.on(Laya.Event.MOUSE_UP, this, this.onUp);
Laya.timer.frameLoop(1, this, this.update);
}
this._viewPortPanel.width = this._iconPanel.width = .85 * this._width;
this._viewPortPanel.height = this._iconPanel.height = .8 * this._height;
this._viewPortPanel.x = .1 * this._width;
this._viewPortPanel.y = .1 * this._height;
this._viewPortPanel.scrollRect = new Laya.Rectangle(0, 0, this._viewPortPanel.width, this._viewPortPanel.height);
};
t.createMovieItem = function(n, i) {
var o = this, r = n.atlas_photo, a = n.atlas_config, c = n.title_bg, s = n.icon, d = n.title, u = !!n.is_bold && 2 == n.is_bold, l = r || s;
if (null != l) {
var p = (this._viewPortPanel.width - 10) / 4;
p = p > t._viewPortPanel.height ? t._viewPortPanel.height : p;
e.gif.LayaGifAnimLoader.Load(l, a, c, d, u).then(function(t) {
var r = t.View.width, a = p / r, c = r * a;
t.View.scale(a, a);
t.View.pivot(r / 2, r / 2);
t.View.y = 0 + c / 2;
t.View.x = c / 2 + (c + 10) * i;
o._iconPanel.addChild(t.View);
o._iconPanel.width = t.View.x + c - c / 2;
t.View.on(Laya.Event.CLICK, o, function() {
o._downX >= 0 || e.Ad.click(n);
});
o._movieList.push(t);
}).catch(function() {
if (null != r) {
var e = n;
e.atlas_photo = null;
e.atlas_config = null;
o.createMovieItem(e, i);
}
});
}
};
t.createMovieList = function(e) {
for (var t = e.length, n = 0; n < t; n++) this.createMovieItem(e[n], n);
};
t.removeMovieList = function() {
if (null != this._movieList) for (var e = this._movieList.length - 1; e >= 0; e--) {
this._movieList[e].Destroy();
this._movieList.splice(e, 1);
} else this._movieList = [];
};
t.updateIconPanel = function() {
if (!(this._downX >= 0)) {
var e = -(this._iconPanel.width - this._viewPortPanel.width);
this._anidelay -= Laya.timer.delta;
if (!(e > 0 || this._anidelay > 0)) {
this._iconPanel.x += this._speed;
if (this._iconPanel.x <= e) {
this._iconPanel.x = e;
this._speed = this.SPEED;
this._anidelay = 1e3;
}
if (this._iconPanel.x >= 0) {
this._iconPanel.x = 0;
this._speed = -this.SPEED;
this._anidelay = 1e3;
}
}
}
};
t.updateMovie = function() {
for (var e = 0, t = this._movieList; e < t.length; e++) {
t[e].Update();
}
};
t.update = function(e) {
this.updateIconPanel();
this.updateMovie();
};
t.Create = function(e, t, n) {
void 0 === t && (t = "#000000");
void 0 === n && (n = "#ffffff");
this._bgColor = t;
this._lineColor = n;
this.removeMovieList();
this.createBg();
this.createMovieList(e);
return this.View;
};
t.onDown = function(e) {
this._downX = e.stageX;
};
t.onMove = function(e) {
if (!(this._downX < 0)) {
var t = e.stageX - this._downX;
this._iconPanel.x += t;
this._downX = e.stageX;
t > 0 && (this._speed = this.SPEED);
t < 0 && (this._speed = -this.SPEED);
}
};
t.onUp = function() {
this._downX = -1;
};
Object.defineProperty(t, "View", {
get: function() {
null == this._root && (this._root = new Laya.Sprite());
return this._root;
},
enumerable: !0,
configurable: !0
});
t.SPEED = 1;
t._width = 600;
t._height = 200;
t._bgColor = "#000000";
t._lineColor = "#ffffff";
t._speed = -1;
t._downX = -1;
t._anidelay = 0;
return t;
}();
t.LayaADListUI = n;
})(e.ui || (e.ui = {}));
})(mpsdk || (mpsdk = {}));

(function(e) {
(function(t) {
var n = function() {
function t() {}
t.init = function() {
if (null == this._movieList) {
this._movieList = [];
Laya.timer.frameLoop(1, this, this.update);
}
};
t.createMovieItem = function(t, n) {
var i = this, o = t.atlas_photo, r = t.atlas_config, a = t.title_bg, c = t.icon, s = t.title, d = !!t.is_bold && 2 == t.is_bold;
0 == this._showName && (s = "");
var u = o || c;
null != u && e.gif.LayaGifAnimLoader.Load(u, r, a, s, d).then(function(o) {
var r = o.View.width, a = i._size / r;
o.View.scale(a, a);
o.View.x = 0;
o.View.y = 0;
n.addChild(o.View);
i._movieList.push(o);
o.View.on(Laya.Event.CLICK, i, function() {
e.Ad.clickSync(t, 0, i._clickCallback);
});
}).catch(function() {
if (null != o) {
var e = t;
e.atlas_photo = null;
e.atlas_config = null;
i.createMovieItem(e, n);
}
});
};
t.updateMovie = function() {
for (var e = this._movieList.length - 1; e >= 0; e--) if (this._movieList[e] && this._movieList[e].View.parent && this._movieList[e].View.parent.parent) this._movieList[e].Update(); else {
this._movieList[e] && this._movieList[e].Destroy();
this._movieList.splice(e, 1);
}
};
t.update = function(e) {
this.updateMovie();
};
t.Create = function(e, t, n, i) {
void 0 === t && (t = 100);
void 0 === n && (n = !0);
this._size = t;
this._showName = n;
this._clickCallback = i;
this.init();
var o = new Laya.Sprite();
o.width = o.height = t;
this.createMovieItem(e, o);
return o;
};
return t;
}();
t.LayaADSingleUI = n;
})(e.ui || (e.ui = {}));
})(mpsdk || (mpsdk = {}));