.class public Lcom/kwai/filedownloader/f/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kwai/filedownloader/f/e$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:J

.field public final c:Z

.field public final d:Z

.field public final e:I

.field public final f:Z

.field public final g:Z


# direct methods
.method private constructor <init>()V
    .locals 14

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/kwai/filedownloader/f/c;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-static {}, Lcom/kwai/filedownloader/f/c;->a()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    const-string v6, "filedownloader.properties"

    invoke-virtual {v5, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_0

    :try_start_1
    invoke-virtual {v2, v5}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v6, "http.lenient"

    invoke-virtual {v2, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    const-string v7, "download.min-progress-step"

    invoke-virtual {v2, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    const-string v8, "download.min-progress-time"

    invoke-virtual {v2, v8}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    const-string v9, "download.max-network-thread-count"

    invoke-virtual {v2, v9}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    const-string v10, "file.non-pre-allocation"

    invoke-virtual {v2, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    const-string v11, "broadcast.completed"

    invoke-virtual {v2, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object v3, v6

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_5

    :catch_1
    move-exception v2

    move-object v10, v3

    goto :goto_5

    :catch_2
    move-exception v2

    move-object v9, v3

    goto :goto_4

    :catch_3
    move-exception v2

    move-object v8, v3

    goto :goto_3

    :catch_4
    move-exception v2

    move-object v7, v3

    goto :goto_2

    :catch_5
    move-exception v2

    move-object v6, v3

    goto :goto_1

    :cond_0
    move-object v2, v3

    move-object v7, v2

    move-object v8, v7

    move-object v9, v8

    move-object v10, v9

    :goto_0
    if-eqz v5, :cond_4

    :try_start_7
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto :goto_8

    :catch_6
    move-exception v5

    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    :catchall_0
    move-exception v0

    move-object v5, v3

    goto/16 :goto_15

    :catch_7
    move-exception v2

    move-object v5, v3

    move-object v6, v5

    :goto_1
    move-object v7, v6

    :goto_2
    move-object v8, v7

    :goto_3
    move-object v9, v8

    :goto_4
    move-object v10, v9

    :goto_5
    :try_start_8
    instance-of v11, v2, Ljava/io/FileNotFoundException;

    if-eqz v11, :cond_1

    sget-boolean v2, Lcom/kwai/filedownloader/f/d;->a:Z

    if-eqz v2, :cond_2

    const-class v2, Lcom/kwai/filedownloader/f/e;

    const-string v11, "not found filedownloader.properties"

    new-array v12, v4, [Ljava/lang/Object;

    invoke-static {v2, v11, v12}, Lcom/kwai/filedownloader/f/d;->c(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    :cond_1
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_2
    :goto_6
    if-eqz v5, :cond_3

    :try_start_9
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    goto :goto_7

    :catch_8
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    :cond_3
    :goto_7
    move-object v2, v3

    move-object v3, v6

    :cond_4
    :goto_8
    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v11, 0x3

    if-eqz v3, :cond_7

    const-string v12, "true"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_6

    const-string v12, "false"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    goto :goto_9

    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-array v1, v11, [Ljava/lang/Object;

    const-string v2, "http.lenient"

    aput-object v2, v1, v4

    const-string v2, "true"

    aput-object v2, v1, v6

    const-string v2, "false"

    aput-object v2, v1, v5

    const-string v2, "the value of \'%s\' must be \'%s\' or \'%s\'"

    invoke-static {v2, v1}, Lcom/kwai/filedownloader/f/f;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_9
    const-string v12, "true"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/kwai/filedownloader/f/e;->c:Z

    goto :goto_a

    :cond_7
    iput-boolean v4, p0, Lcom/kwai/filedownloader/f/e;->c:Z

    :goto_a
    const-string v3, "true"

    const-string v12, "true"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string v3, "true"

    const-string v12, "false"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    goto :goto_b

    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    new-array v1, v11, [Ljava/lang/Object;

    const-string v2, "process.non-separate"

    aput-object v2, v1, v4

    const-string v2, "true"

    aput-object v2, v1, v6

    const-string v2, "false"

    aput-object v2, v1, v5

    const-string v2, "the value of \'%s\' must be \'%s\' or \'%s\'"

    invoke-static {v2, v1}, Lcom/kwai/filedownloader/f/f;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    :goto_b
    const-string v3, "true"

    const-string v12, "true"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/kwai/filedownloader/f/e;->d:Z

    if-eqz v7, :cond_a

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    :goto_c
    iput v3, p0, Lcom/kwai/filedownloader/f/e;->a:I

    goto :goto_d

    :cond_a
    const/high16 v3, 0x10000

    goto :goto_c

    :goto_d
    if-eqz v8, :cond_b

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    const-wide/16 v12, 0x0

    invoke-static {v12, v13, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v7

    :goto_e
    iput-wide v7, p0, Lcom/kwai/filedownloader/f/e;->b:J

    goto :goto_f

    :cond_b
    const-wide/16 v7, 0x7d0

    goto :goto_e

    :goto_f
    if-eqz v9, :cond_c

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/kwai/filedownloader/f/e;->a(I)I

    move-result v3

    iput v3, p0, Lcom/kwai/filedownloader/f/e;->e:I

    goto :goto_10

    :cond_c
    iput v11, p0, Lcom/kwai/filedownloader/f/e;->e:I

    :goto_10
    if-eqz v10, :cond_f

    const-string v3, "true"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string v3, "false"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    goto :goto_11

    :cond_d
    new-instance v0, Ljava/lang/IllegalStateException;

    new-array v1, v11, [Ljava/lang/Object;

    const-string v2, "file.non-pre-allocation"

    aput-object v2, v1, v4

    const-string v2, "true"

    aput-object v2, v1, v6

    const-string v2, "false"

    aput-object v2, v1, v5

    const-string v2, "the value of \'%s\' must be \'%s\' or \'%s\'"

    invoke-static {v2, v1}, Lcom/kwai/filedownloader/f/f;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    :goto_11
    const-string v3, "true"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/kwai/filedownloader/f/e;->f:Z

    goto :goto_12

    :cond_f
    iput-boolean v4, p0, Lcom/kwai/filedownloader/f/e;->f:Z

    :goto_12
    if-eqz v2, :cond_12

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    goto :goto_13

    :cond_10
    new-instance v0, Ljava/lang/IllegalStateException;

    new-array v1, v11, [Ljava/lang/Object;

    const-string v2, "broadcast.completed"

    aput-object v2, v1, v4

    const-string v2, "true"

    aput-object v2, v1, v6

    const-string v2, "false"

    aput-object v2, v1, v5

    const-string v2, "the value of \'%s\' must be \'%s\' or \'%s\'"

    invoke-static {v2, v1}, Lcom/kwai/filedownloader/f/f;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    :goto_13
    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/kwai/filedownloader/f/e;->g:Z

    goto :goto_14

    :cond_12
    iput-boolean v4, p0, Lcom/kwai/filedownloader/f/e;->g:Z

    :goto_14
    sget-boolean v2, Lcom/kwai/filedownloader/f/d;->a:Z

    if-eqz v2, :cond_13

    const-class v2, Lcom/kwai/filedownloader/f/e;

    const-string v3, "init properties %d\n load properties: %s=%B; %s=%B; %s=%d; %s=%d; %s=%d"

    const/16 v7, 0xb

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v4

    const-string v0, "http.lenient"

    aput-object v0, v7, v6

    iget-boolean v0, p0, Lcom/kwai/filedownloader/f/e;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v7, v5

    const-string v0, "process.non-separate"

    aput-object v0, v7, v11

    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/kwai/filedownloader/f/e;->d:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v7, v0

    const/4 v0, 0x5

    const-string v1, "download.min-progress-step"

    aput-object v1, v7, v0

    const/4 v0, 0x6

    iget v1, p0, Lcom/kwai/filedownloader/f/e;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v0

    const/4 v0, 0x7

    const-string v1, "download.min-progress-time"

    aput-object v1, v7, v0

    const/16 v0, 0x8

    iget-wide v4, p0, Lcom/kwai/filedownloader/f/e;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v7, v0

    const/16 v0, 0x9

    const-string v1, "download.max-network-thread-count"

    aput-object v1, v7, v0

    const/16 v0, 0xa

    iget v1, p0, Lcom/kwai/filedownloader/f/e;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v0

    invoke-static {v2, v3, v7}, Lcom/kwai/filedownloader/f/d;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_13
    return-void

    :catchall_1
    move-exception v0

    :goto_15
    if-eqz v5, :cond_14

    :try_start_a
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    goto :goto_16

    :catch_9
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    :cond_14
    :goto_16
    throw v0

    :cond_15
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Please invoke the \'FileDownloader#setup\' before using FileDownloader. If you want to register some components on FileDownloader please invoke the \'FileDownloader#setupOnApplicationOnCreate\' on the \'Application#onCreate\' first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method synthetic constructor <init>(Lcom/kwai/filedownloader/f/e$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/kwai/filedownloader/f/e;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 7

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/16 v4, 0xc

    if-le p0, v4, :cond_0

    const-class v5, Lcom/kwai/filedownloader/f/e;

    const-string v6, "require the count of network thread  is %d, what is more than the max valid count(%d), so adjust to %d auto"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v0

    invoke-static {v5, v6, v2}, Lcom/kwai/filedownloader/f/d;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    return v4

    :cond_0
    if-ge p0, v3, :cond_1

    const-class v4, Lcom/kwai/filedownloader/f/e;

    const-string v5, "require the count of network thread  is %d, what is less than the min valid count(%d), so adjust to %d auto"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v0

    invoke-static {v4, v5, v2}, Lcom/kwai/filedownloader/f/d;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    return v3

    :cond_1
    return p0
.end method

.method public static a()Lcom/kwai/filedownloader/f/e;
    .locals 1

    invoke-static {}, Lcom/kwai/filedownloader/f/e$a;->a()Lcom/kwai/filedownloader/f/e;

    move-result-object v0

    return-object v0
.end method
