.class public Lcom/kwai/filedownloader/d/d;
.super Ljava/lang/Object;


# direct methods
.method public static a(I)Z
    .locals 0

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static a(II)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    invoke-static {p0}, Lcom/kwai/filedownloader/d/d;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    return v0

    :cond_1
    const/4 v1, 0x6

    const/4 v2, 0x1

    if-lt p0, v2, :cond_2

    if-gt p0, v1, :cond_2

    const/16 v3, 0xa

    if-lt p1, v3, :cond_2

    const/16 v3, 0xb

    if-gt p1, v3, :cond_2

    return v0

    :cond_2
    packed-switch p0, :pswitch_data_0

    :pswitch_0
    return v2

    :pswitch_1
    packed-switch p1, :pswitch_data_1

    return v2

    :pswitch_2
    return v0

    :pswitch_3
    if-eq p1, v2, :cond_3

    if-eq p1, v1, :cond_3

    return v2

    :cond_3
    return v0

    :pswitch_4
    if-eq p1, v1, :cond_4

    packed-switch p1, :pswitch_data_2

    return v2

    :cond_4
    :pswitch_5
    return v0

    :pswitch_6
    if-eq p1, v1, :cond_5

    packed-switch p1, :pswitch_data_3

    return v2

    :cond_5
    :pswitch_7
    return v0

    :pswitch_8
    if-eqz p1, :cond_6

    return v2

    :cond_6
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Lcom/kwai/filedownloader/a;)Z
    .locals 1

    invoke-interface {p0}, Lcom/kwai/filedownloader/a;->v()B

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Lcom/kwai/filedownloader/a;->v()B

    move-result p0

    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static b(I)Z
    .locals 0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static b(II)Z
    .locals 5

    const/4 v0, 0x3

    const/4 v1, 0x5

    const/4 v2, 0x0

    if-eq p0, v0, :cond_0

    if-eq p0, v1, :cond_0

    if-ne p0, p1, :cond_0

    return v2

    :cond_0
    invoke-static {p0}, Lcom/kwai/filedownloader/d/d;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    return v2

    :cond_1
    const/4 v3, -0x2

    const/4 v4, 0x1

    if-ne p1, v3, :cond_2

    return v4

    :cond_2
    const/4 v3, -0x1

    if-ne p1, v3, :cond_3

    return v4

    :cond_3
    packed-switch p0, :pswitch_data_0

    :pswitch_0
    return v2

    :pswitch_1
    if-eq p1, v4, :cond_4

    packed-switch p1, :pswitch_data_1

    return v2

    :cond_4
    :pswitch_2
    return v4

    :pswitch_3
    const/16 p0, 0xb

    if-eq p1, p0, :cond_5

    return v2

    :cond_5
    return v4

    :pswitch_4
    const/4 p0, 0x2

    if-eq p1, p0, :cond_6

    if-eq p1, v1, :cond_6

    return v2

    :cond_6
    return v4

    :pswitch_5
    const/4 p0, -0x3

    if-eq p1, p0, :cond_7

    if-eq p1, v0, :cond_7

    if-eq p1, v1, :cond_7

    return v2

    :cond_7
    return v4

    :pswitch_6
    const/4 p0, 0x6

    if-eq p1, p0, :cond_8

    return v2

    :cond_8
    return v4

    :pswitch_7
    const/16 p0, 0xa

    if-eq p1, p0, :cond_9

    return v2

    :cond_9
    return v4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x4
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
