.class Lcom/kwai/sodler/lib/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/kwai/sodler/lib/a/d;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/kwai/sodler/lib/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/kwai/sodler/lib/d;->b:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/kwai/sodler/lib/d;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Lcom/kwai/sodler/lib/a/f;Lcom/kwai/sodler/lib/a/a;)Lcom/kwai/sodler/lib/a/a;
    .locals 6

    invoke-virtual {p2}, Lcom/kwai/sodler/lib/a/a;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v2, "Sodler.loader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loading plugin, path = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->b()Lcom/kwai/sodler/lib/a/e;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v1}, Lcom/kwai/sodler/lib/a/a;->b(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Lcom/kwai/sodler/lib/a/a;->a(Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/kwai/sodler/lib/a/e;->e()Lcom/kwai/sodler/lib/a/c;

    move-result-object v3

    invoke-interface {v3, v1, p1}, Lcom/kwai/sodler/lib/a/c;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Lcom/kwai/sodler/lib/a/e;->e()Lcom/kwai/sodler/lib/a/c;

    move-result-object v3

    invoke-interface {v3, v1, p1}, Lcom/kwai/sodler/lib/a/c;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/kwai/sodler/lib/c/a;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v0, "Sodler.loader"

    const-string v2, "The current version has been installed before."

    invoke-static {v0, v2}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v3}, Lcom/kwai/sodler/lib/a/a;->c(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/kwai/sodler/lib/d;->a(Ljava/lang/String;)Lcom/kwai/sodler/lib/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string p2, "Sodler.loader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The current plugin has been loaded, id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const-string p1, "Sodler.loader"

    const-string v0, "Load plugin from installed path."

    invoke-static {p1, v0}, Lcom/kwai/sodler/lib/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/kwai/sodler/lib/d;->a:Landroid/content/Context;

    invoke-virtual {p2, p1, v3}, Lcom/kwai/sodler/lib/a/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v1, p2}, Lcom/kwai/sodler/lib/d;->a(Ljava/lang/String;Lcom/kwai/sodler/lib/a/a;)V

    return-object p2

    :cond_1
    invoke-virtual {p0, v1}, Lcom/kwai/sodler/lib/d;->a(Ljava/lang/String;)Lcom/kwai/sodler/lib/a/a;

    move-result-object p1

    if-eqz p1, :cond_2

    return-object p1

    :cond_2
    const-string p1, "Sodler.loader"

    const-string v3, "Load plugin from dest path."

    invoke-static {p1, v3}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/kwai/sodler/lib/a/e;->e()Lcom/kwai/sodler/lib/a/c;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/kwai/sodler/lib/a/c;->b(Lcom/kwai/sodler/lib/a/a;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/kwai/sodler/lib/a/a;->c(Ljava/lang/String;)V

    const-string v3, "Sodler.loader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "installed ."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/kwai/sodler/lib/d;->a:Landroid/content/Context;

    invoke-virtual {p2, v3, p1}, Lcom/kwai/sodler/lib/a/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v1, p2}, Lcom/kwai/sodler/lib/d;->a(Ljava/lang/String;Lcom/kwai/sodler/lib/a/a;)V

    invoke-interface {v2}, Lcom/kwai/sodler/lib/a/e;->b()Lcom/kwai/sodler/lib/ext/c;

    move-result-object p1

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/ext/c;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-static {v0}, Lcom/kwai/sodler/lib/c/a;->a(Ljava/lang/String;)Z

    :cond_3
    return-object p2

    :cond_4
    new-instance p1, Lcom/kwai/sodler/lib/ext/PluginError$LoadError;

    const/16 p2, 0xbb9

    const-string v0, "Apk file not exist."

    invoke-direct {p1, v0, p2}, Lcom/kwai/sodler/lib/ext/PluginError$LoadError;-><init>(Ljava/lang/String;I)V

    throw p1
.end method

.method private a(Lcom/kwai/sodler/lib/a/f;Lcom/kwai/sodler/lib/ext/PluginError;)V
    .locals 3

    const-string v0, "Sodler.loader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwai/sodler/lib/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x5

    invoke-virtual {p1, v0}, Lcom/kwai/sodler/lib/a/f;->a(I)Lcom/kwai/sodler/lib/a/f;

    invoke-virtual {p1, p2}, Lcom/kwai/sodler/lib/a/f;->a(Ljava/lang/Throwable;)Lcom/kwai/sodler/lib/a/f;

    invoke-direct {p0, p1}, Lcom/kwai/sodler/lib/d;->d(Lcom/kwai/sodler/lib/a/f;)V

    return-void
.end method

.method private b(Lcom/kwai/sodler/lib/a/f;)V
    .locals 3

    const-string v0, "Sodler.loader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPreLoad state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwai/sodler/lib/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->b()Lcom/kwai/sodler/lib/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/kwai/sodler/lib/a/e;->g()Lcom/kwai/sodler/lib/ext/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/kwai/sodler/lib/ext/a;->e(Lcom/kwai/sodler/lib/a/f;)V

    return-void
.end method

.method private c(Lcom/kwai/sodler/lib/a/f;)V
    .locals 3

    const-string v0, "Sodler.loader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCanceled state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwai/sodler/lib/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x7

    invoke-virtual {p1, v0}, Lcom/kwai/sodler/lib/a/f;->a(I)Lcom/kwai/sodler/lib/a/f;

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->b()Lcom/kwai/sodler/lib/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/kwai/sodler/lib/a/e;->g()Lcom/kwai/sodler/lib/ext/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/kwai/sodler/lib/ext/a;->b(Lcom/kwai/sodler/lib/a/f;)V

    return-void
.end method

.method private d(Lcom/kwai/sodler/lib/a/f;)V
    .locals 4

    const-string v0, "Sodler.loader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPostLoad state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwai/sodler/lib/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->c()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->l()Lcom/kwai/sodler/lib/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->b()Lcom/kwai/sodler/lib/a/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/kwai/sodler/lib/a/e;->g()Lcom/kwai/sodler/lib/ext/a;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/kwai/sodler/lib/ext/a;->a(Lcom/kwai/sodler/lib/a/f;Lcom/kwai/sodler/lib/a/a;)V

    return-void

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/kwai/sodler/lib/a/f;->a(I)Lcom/kwai/sodler/lib/a/f;

    :cond_1
    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->g()Ljava/lang/Throwable;

    move-result-object v0

    const/16 v1, 0xfab

    if-eqz v0, :cond_2

    new-instance v0, Lcom/kwai/sodler/lib/ext/PluginError$LoadError;

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->g()Ljava/lang/Throwable;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/kwai/sodler/lib/ext/PluginError$LoadError;-><init>(Ljava/lang/Throwable;I)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/kwai/sodler/lib/ext/PluginError$LoadError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can not get plugin instance "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/kwai/sodler/lib/ext/PluginError$LoadError;-><init>(Ljava/lang/String;I)V

    :goto_0
    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->b()Lcom/kwai/sodler/lib/a/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/kwai/sodler/lib/a/e;->g()Lcom/kwai/sodler/lib/ext/a;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/kwai/sodler/lib/ext/a;->a(Lcom/kwai/sodler/lib/a/f;Lcom/kwai/sodler/lib/ext/PluginError;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Lcom/kwai/sodler/lib/a/a;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/kwai/sodler/lib/d;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/kwai/sodler/lib/a/a;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/a;->b()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    monitor-exit p0

    return-object p1

    :cond_0
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public a(Lcom/kwai/sodler/lib/a/f;)Lcom/kwai/sodler/lib/a/f;
    .locals 10
    .param p1    # Lcom/kwai/sodler/lib/a/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "Sodler.loader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loading plugin, id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Load"

    invoke-virtual {p1, v0}, Lcom/kwai/sodler/lib/a/f;->c(Ljava/lang/String;)Lcom/kwai/sodler/lib/a/f;

    invoke-direct {p0, p1}, Lcom/kwai/sodler/lib/d;->b(Lcom/kwai/sodler/lib/a/f;)V

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/kwai/sodler/lib/d;->c(Lcom/kwai/sodler/lib/a/f;)V

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->t()V

    iget-object v0, p0, Lcom/kwai/sodler/lib/d;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kwai/sodler/lib/a/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/kwai/sodler/lib/a/a;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1, v0}, Lcom/kwai/sodler/lib/a/f;->a(Lcom/kwai/sodler/lib/a/a;)V

    const-string v2, "Sodler.loader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Load plugin success, path = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/kwai/sodler/lib/a/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/kwai/sodler/lib/a/f;->a(I)Lcom/kwai/sodler/lib/a/f;

    invoke-direct {p0, p1}, Lcom/kwai/sodler/lib/d;->d(Lcom/kwai/sodler/lib/a/f;)V

    return-object p1

    :cond_1
    const-string v0, "Sodler.loader"

    const-string v2, "------choose best plugin------------"

    invoke-static {v0, v2}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Sodler.loader"

    const-string v2, "-------\u8fdc\u7a0b\u5b58\u5728------------"

    invoke-static {v0, v2}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->r()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/kwai/sodler/lib/d;->a:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/kwai/sodler/lib/f;->a(Landroid/content/Context;Lcom/kwai/sodler/lib/a/f;)Lcom/kwai/sodler/lib/b/b;

    move-result-object v2

    const/4 v3, -0x1

    if-nez v2, :cond_2

    invoke-virtual {p1, v3}, Lcom/kwai/sodler/lib/a/f;->a(I)Lcom/kwai/sodler/lib/a/f;

    invoke-direct {p0, p1}, Lcom/kwai/sodler/lib/d;->d(Lcom/kwai/sodler/lib/a/f;)V

    return-object p1

    :cond_2
    const/16 v4, 0x7d7

    const/4 v5, 0x1

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "Sodler.loader"

    const-string v7, "-------\u672c\u5730\u5b58\u5728------------"

    invoke-static {v6, v7}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/kwai/sodler/lib/b/a;

    iget-object v8, v2, Lcom/kwai/sodler/lib/b/b;->b:Ljava/lang/String;

    iget-object v9, v7, Lcom/kwai/sodler/lib/b/a;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move-object v6, v7

    :cond_4
    if-nez v6, :cond_5

    const-string v0, "Sodler.loader"

    const-string v6, "-------\u672c\u5730\u9700\u8981\u5347\u7ea7--------"

    invoke-static {v0, v6}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, v2, Lcom/kwai/sodler/lib/b/b;->g:Z

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/kwad/sdk/KsAdSDKImpl;->get()Lcom/kwad/sdk/KsAdSDKImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kwad/sdk/KsAdSDKImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/kwad/sdk/utils/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Lcom/kwai/sodler/lib/ext/PluginError$NotWifiDownloadError;

    const-string v1, "It can be downloaded only on WiFi"

    invoke-direct {v0, v1, v4}, Lcom/kwai/sodler/lib/ext/PluginError$NotWifiDownloadError;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, p1, v0}, Lcom/kwai/sodler/lib/d;->a(Lcom/kwai/sodler/lib/a/f;Lcom/kwai/sodler/lib/ext/PluginError;)V

    return-object p1

    :cond_5
    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->b()Lcom/kwai/sodler/lib/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/kwai/sodler/lib/a/e;->e()Lcom/kwai/sodler/lib/a/c;

    move-result-object v0

    iget-object v4, v6, Lcom/kwai/sodler/lib/b/a;->a:Ljava/lang/String;

    iget-object v7, v6, Lcom/kwai/sodler/lib/b/a;->b:Ljava/lang/String;

    invoke-interface {v0, v4, v7}, Lcom/kwai/sodler/lib/a/c;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/kwai/sodler/lib/a/f;->d(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/kwai/sodler/lib/a/f;->e(Ljava/lang/String;)V

    invoke-virtual {p1, v5}, Lcom/kwai/sodler/lib/a/f;->a(I)Lcom/kwai/sodler/lib/a/f;

    iget-object v4, v6, Lcom/kwai/sodler/lib/b/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/kwai/sodler/lib/a/f;->b(Ljava/lang/String;)V

    const-string v4, "Sodler.loader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "-------\u672c\u5730\u627e\u5230--------"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v0, "Sodler.loader"

    const-string v6, "-------\u672c\u5730\u4e0d\u5b58\u5728\uff0c\u89e6\u53d1\u66f4\u65b0------------"

    invoke-static {v0, v6}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, v2, Lcom/kwai/sodler/lib/b/b;->g:Z

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/kwad/sdk/KsAdSDKImpl;->get()Lcom/kwad/sdk/KsAdSDKImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kwad/sdk/KsAdSDKImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/kwad/sdk/utils/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Lcom/kwai/sodler/lib/ext/PluginError$NotWifiDownloadError;

    const-string v1, "It can be downloaded only on WiFi"

    invoke-direct {v0, v1, v4}, Lcom/kwai/sodler/lib/ext/PluginError$NotWifiDownloadError;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, p1, v0}, Lcom/kwai/sodler/lib/d;->a(Lcom/kwai/sodler/lib/a/f;Lcom/kwai/sodler/lib/ext/PluginError;)V

    return-object p1

    :cond_7
    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->b()Lcom/kwai/sodler/lib/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/kwai/sodler/lib/a/e;->d()Lcom/kwai/sodler/lib/a/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/kwai/sodler/lib/a/g;->a(Lcom/kwai/sodler/lib/a/f;)Lcom/kwai/sodler/lib/a/f;

    :goto_0
    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->c()I

    move-result v0

    if-ne v0, v5, :cond_a

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->k()Ljava/lang/String;

    move-result-object v0

    const-string v4, "Sodler.loader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "-------\u66f4\u65b0\u6210\u529f\u6216\u8005\u83b7\u53d6\u5230\u672c\u5730\u6210\u529f------------"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    invoke-virtual {p1, v0}, Lcom/kwai/sodler/lib/a/f;->a(Ljava/lang/String;)Lcom/kwai/sodler/lib/a/a;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/kwai/sodler/lib/a/a;->a(Lcom/kwai/sodler/lib/b/b;)Lcom/kwai/sodler/lib/a/a;

    move-result-object v2

    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->b()Lcom/kwai/sodler/lib/a/e;

    move-result-object v3

    invoke-interface {v3}, Lcom/kwai/sodler/lib/a/e;->b()Lcom/kwai/sodler/lib/ext/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kwai/sodler/lib/ext/c;->a()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/kwai/sodler/lib/a/f;->b(I)V

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->f()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-direct {p0, p1}, Lcom/kwai/sodler/lib/d;->c(Lcom/kwai/sodler/lib/a/f;)V

    return-object p1

    :cond_8
    :try_start_0
    invoke-direct {p0, p1, v2}, Lcom/kwai/sodler/lib/d;->a(Lcom/kwai/sodler/lib/a/f;Lcom/kwai/sodler/lib/a/a;)Lcom/kwai/sodler/lib/a/a;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/kwai/sodler/lib/a/f;->a(Lcom/kwai/sodler/lib/a/a;)V

    const-string v4, "Sodler.loader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Load plugin success, path = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/kwai/sodler/lib/a/f;->a(I)Lcom/kwai/sodler/lib/a/f;

    invoke-direct {p0, p1}, Lcom/kwai/sodler/lib/d;->d(Lcom/kwai/sodler/lib/a/f;)V
    :try_end_0
    .catch Lcom/kwai/sodler/lib/ext/PluginError$LoadError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/kwai/sodler/lib/ext/PluginError$InstallError; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v4

    const-string v5, "Sodler.loader"

    invoke-static {v5, v4}, Lcom/kwai/sodler/lib/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :try_start_1
    invoke-virtual {p1}, Lcom/kwai/sodler/lib/a/f;->h()V

    const-string v5, "Sodler.loader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Load fail, retry "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retry load "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/kwai/sodler/lib/a/f;->c(Ljava/lang/String;)Lcom/kwai/sodler/lib/a/f;
    :try_end_1
    .catch Lcom/kwai/sodler/lib/ext/PluginError$RetryError; {:try_start_1 .. :try_end_1} :catch_1

    move v3, v7

    goto :goto_1

    :catch_1
    const-string v0, "Sodler.loader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Load plugin fail, error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/kwai/sodler/lib/ext/PluginError;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/kwai/sodler/lib/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v4}, Lcom/kwai/sodler/lib/d;->a(Lcom/kwai/sodler/lib/a/f;Lcom/kwai/sodler/lib/ext/PluginError;)V

    return-object p1

    :cond_9
    invoke-virtual {p1, v3}, Lcom/kwai/sodler/lib/a/f;->a(I)Lcom/kwai/sodler/lib/a/f;

    invoke-direct {p0, p1}, Lcom/kwai/sodler/lib/d;->d(Lcom/kwai/sodler/lib/a/f;)V

    return-object p1

    :cond_a
    invoke-direct {p0, p1}, Lcom/kwai/sodler/lib/d;->d(Lcom/kwai/sodler/lib/a/f;)V

    return-object p1
.end method

.method public declared-synchronized a(Ljava/lang/String;Lcom/kwai/sodler/lib/a/a;)V
    .locals 1

    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p2}, Lcom/kwai/sodler/lib/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kwai/sodler/lib/d;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void
.end method
