.class public final Lcom/qm/shjzjb/mj01/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qm/shjzjb/mj01/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f040000

.field public static final abc_action_bar_up_container:I = 0x7f040001

.field public static final abc_action_menu_item_layout:I = 0x7f040002

.field public static final abc_action_menu_layout:I = 0x7f040003

.field public static final abc_action_mode_bar:I = 0x7f040004

.field public static final abc_action_mode_close_item_material:I = 0x7f040005

.field public static final abc_activity_chooser_view:I = 0x7f040006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f040007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f040008

.field public static final abc_alert_dialog_material:I = 0x7f040009

.field public static final abc_alert_dialog_title_material:I = 0x7f04000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f04000b

.field public static final abc_dialog_title_material:I = 0x7f04000c

.field public static final abc_expanded_menu_layout:I = 0x7f04000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f04000e

.field public static final abc_list_menu_item_icon:I = 0x7f04000f

.field public static final abc_list_menu_item_layout:I = 0x7f040010

.field public static final abc_list_menu_item_radio:I = 0x7f040011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f040012

.field public static final abc_popup_menu_item_layout:I = 0x7f040013

.field public static final abc_screen_content_include:I = 0x7f040014

.field public static final abc_screen_simple:I = 0x7f040015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f040016

.field public static final abc_screen_toolbar:I = 0x7f040017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f040018

.field public static final abc_search_view:I = 0x7f040019

.field public static final abc_select_dialog_material:I = 0x7f04001a

.field public static final abc_tooltip:I = 0x7f04001b

.field public static final activity_native:I = 0x7f04001c

.field public static final activity_native_express:I = 0x7f04001d

.field public static final activity_simulate:I = 0x7f04001e

.field public static final browser_actions_context_menu_page:I = 0x7f04001f

.field public static final browser_actions_context_menu_row:I = 0x7f040020

.field public static final custom_dialog:I = 0x7f040021

.field public static final ksad_activity_ad_video_webview:I = 0x7f040022

.field public static final ksad_activity_ad_webview:I = 0x7f040023

.field public static final ksad_activity_feed_download:I = 0x7f040024

.field public static final ksad_activity_fullscreen_video:I = 0x7f040025

.field public static final ksad_activity_reward_video:I = 0x7f040026

.field public static final ksad_ad_landingpage_layout:I = 0x7f040027

.field public static final ksad_ad_web_card_layout:I = 0x7f040028

.field public static final ksad_app_score:I = 0x7f040029

.field public static final ksad_content_alliance_toast:I = 0x7f04002a

.field public static final ksad_content_alliance_toast_2:I = 0x7f04002b

.field public static final ksad_content_alliance_toast_light:I = 0x7f04002c

.field public static final ksad_datail_webview_container:I = 0x7f04002d

.field public static final ksad_detail_webview:I = 0x7f04002e

.field public static final ksad_download_dialog_layout:I = 0x7f04002f

.field public static final ksad_download_progress_bar:I = 0x7f040030

.field public static final ksad_draw_card_app:I = 0x7f040031

.field public static final ksad_draw_card_h5:I = 0x7f040032

.field public static final ksad_draw_download_bar:I = 0x7f040033

.field public static final ksad_draw_layout:I = 0x7f040034

.field public static final ksad_draw_video_tailframe:I = 0x7f040035

.field public static final ksad_feed_app_download:I = 0x7f040036

.field public static final ksad_feed_label_dislike:I = 0x7f040037

.field public static final ksad_feed_label_dislike_bottom:I = 0x7f040038

.field public static final ksad_feed_open_h5:I = 0x7f040039

.field public static final ksad_feed_text_above_group_image:I = 0x7f04003a

.field public static final ksad_feed_text_above_image:I = 0x7f04003b

.field public static final ksad_feed_text_above_video:I = 0x7f04003c

.field public static final ksad_feed_text_below_image:I = 0x7f04003d

.field public static final ksad_feed_text_below_video:I = 0x7f04003e

.field public static final ksad_feed_text_immerse_image:I = 0x7f04003f

.field public static final ksad_feed_text_left_image:I = 0x7f040040

.field public static final ksad_feed_text_right_image:I = 0x7f040041

.field public static final ksad_feed_video:I = 0x7f040042

.field public static final ksad_feed_video_palyer_controller:I = 0x7f040043

.field public static final ksad_feed_webview:I = 0x7f040044

.field public static final ksad_fullscreen_detail_top_toolbar:I = 0x7f040045

.field public static final ksad_fullscreen_end_top_toolbar:I = 0x7f040046

.field public static final ksad_logo_layout:I = 0x7f040047

.field public static final ksad_native_video_layout:I = 0x7f040048

.field public static final ksad_notification_download_completed:I = 0x7f040049

.field public static final ksad_notification_download_progress:I = 0x7f04004a

.field public static final ksad_reward_detail_top_toolbar:I = 0x7f04004b

.field public static final ksad_reward_end_top_toolbar:I = 0x7f04004c

.field public static final ksad_splash_screen:I = 0x7f04004d

.field public static final ksad_video_actionbar_app_landscape:I = 0x7f04004e

.field public static final ksad_video_actionbar_app_portrait:I = 0x7f04004f

.field public static final ksad_video_actionbar_h5:I = 0x7f040050

.field public static final ksad_video_actionbar_landscape_vertical:I = 0x7f040051

.field public static final ksad_video_actionbar_portrait_horizontal:I = 0x7f040052

.field public static final ksad_video_close_dialog:I = 0x7f040053

.field public static final ksad_video_tf_bar_app_landscape:I = 0x7f040054

.field public static final ksad_video_tf_bar_app_portrait_horizontal:I = 0x7f040055

.field public static final ksad_video_tf_bar_app_portrait_vertical:I = 0x7f040056

.field public static final ksad_video_tf_bar_h5_landscape:I = 0x7f040057

.field public static final ksad_video_tf_bar_h5_portrait_horizontal:I = 0x7f040058

.field public static final ksad_video_tf_bar_h5_portrait_vertical:I = 0x7f040059

.field public static final ksad_video_tf_view_landscape_horizontal:I = 0x7f04005a

.field public static final ksad_video_tf_view_landscape_vertical:I = 0x7f04005b

.field public static final ksad_video_tf_view_portrait_horizontal:I = 0x7f04005c

.field public static final ksad_video_tf_view_portrait_vertical:I = 0x7f04005d

.field public static final mobads_cutom_notification_layout:I = 0x7f04005e

.field public static final native_ad_item:I = 0x7f04005f

.field public static final native_express:I = 0x7f040060

.field public static final notification_action:I = 0x7f040061

.field public static final notification_action_tombstone:I = 0x7f040062

.field public static final notification_template_custom_big:I = 0x7f040063

.field public static final notification_template_icon_group:I = 0x7f040064

.field public static final notification_template_part_chronometer:I = 0x7f040065

.field public static final notification_template_part_time:I = 0x7f040066

.field public static final select_dialog_item_material:I = 0x7f040067

.field public static final select_dialog_multichoice_material:I = 0x7f040068

.field public static final select_dialog_singlechoice_material:I = 0x7f040069

.field public static final support_simple_spinner_dropdown_item:I = 0x7f04006a

.field public static final tt_activity_full_video:I = 0x7f04006b

.field public static final tt_activity_full_video_new_bar_3_style:I = 0x7f04006c

.field public static final tt_activity_full_video_newstyle:I = 0x7f04006d

.field public static final tt_activity_lite_web_layout:I = 0x7f04006e

.field public static final tt_activity_middle_page:I = 0x7f04006f

.field public static final tt_activity_reward_and_full_video_bar:I = 0x7f040070

.field public static final tt_activity_reward_and_full_video_new_bar:I = 0x7f040071

.field public static final tt_activity_reward_video_newstyle:I = 0x7f040072

.field public static final tt_activity_rewardvideo:I = 0x7f040073

.field public static final tt_activity_rewardvideo_new_bar_3_style:I = 0x7f040074

.field public static final tt_activity_ttlandingpage:I = 0x7f040075

.field public static final tt_activity_ttlandingpage_playable:I = 0x7f040076

.field public static final tt_activity_video_scroll_landingpage:I = 0x7f040077

.field public static final tt_activity_videolandingpage:I = 0x7f040078

.field public static final tt_app_detail_dialog:I = 0x7f040079

.field public static final tt_app_detail_full_dialog:I = 0x7f04007a

.field public static final tt_app_detail_full_dialog_list_head:I = 0x7f04007b

.field public static final tt_app_detail_listview_item:I = 0x7f04007c

.field public static final tt_app_privacy_dialog:I = 0x7f04007d

.field public static final tt_appdownloader_notification_layout:I = 0x7f04007e

.field public static final tt_backup_ad:I = 0x7f04007f

.field public static final tt_backup_ad1:I = 0x7f040080

.field public static final tt_backup_ad2:I = 0x7f040081

.field public static final tt_backup_banner_layout1:I = 0x7f040082

.field public static final tt_backup_banner_layout2:I = 0x7f040083

.field public static final tt_backup_banner_layout3:I = 0x7f040084

.field public static final tt_backup_draw:I = 0x7f040085

.field public static final tt_backup_feed_horizontal:I = 0x7f040086

.field public static final tt_backup_feed_img_group:I = 0x7f040087

.field public static final tt_backup_feed_img_small:I = 0x7f040088

.field public static final tt_backup_feed_vertical:I = 0x7f040089

.field public static final tt_backup_feed_video:I = 0x7f04008a

.field public static final tt_backup_full_reward:I = 0x7f04008b

.field public static final tt_backup_insert_layout1:I = 0x7f04008c

.field public static final tt_backup_insert_layout2:I = 0x7f04008d

.field public static final tt_backup_insert_layout3:I = 0x7f04008e

.field public static final tt_browser_download_layout:I = 0x7f04008f

.field public static final tt_browser_titlebar:I = 0x7f040090

.field public static final tt_browser_titlebar_for_dark:I = 0x7f040091

.field public static final tt_common_download_dialog:I = 0x7f040092

.field public static final tt_custom_dailog_layout:I = 0x7f040093

.field public static final tt_dialog_listview_item:I = 0x7f040094

.field public static final tt_dislike_comment_layout:I = 0x7f040095

.field public static final tt_dislike_dialog_layout:I = 0x7f040096

.field public static final tt_dislike_dialog_layout1:I = 0x7f040097

.field public static final tt_dislike_dialog_layout2:I = 0x7f040098

.field public static final tt_dislike_flowlayout_tv:I = 0x7f040099

.field public static final tt_insert_ad_layout:I = 0x7f04009a

.field public static final tt_install_dialog_layout:I = 0x7f04009b

.field public static final tt_native_video_ad_view:I = 0x7f04009c

.field public static final tt_native_video_img_cover_layout:I = 0x7f04009d

.field public static final tt_playable_loading_layout:I = 0x7f04009e

.field public static final tt_playable_view_layout:I = 0x7f04009f

.field public static final tt_splash_view:I = 0x7f0400a0

.field public static final tt_top_full_1:I = 0x7f0400a1

.field public static final tt_top_reward_1:I = 0x7f0400a2

.field public static final tt_top_reward_dislike_2:I = 0x7f0400a3

.field public static final tt_video_ad_cover_layout:I = 0x7f0400a4

.field public static final tt_video_detail_layout:I = 0x7f0400a5

.field public static final tt_video_draw_btn_layout:I = 0x7f0400a6

.field public static final tt_video_play_layout_for_live:I = 0x7f0400a7

.field public static final tt_video_traffic_tip:I = 0x7f0400a8

.field public static final tt_video_traffic_tips_layout:I = 0x7f0400a9

.field public static final ttdownloader_activity_app_detail_info:I = 0x7f0400aa

.field public static final ttdownloader_activity_app_privacy_policy:I = 0x7f0400ab

.field public static final ttdownloader_dialog_appinfo:I = 0x7f0400ac

.field public static final ttdownloader_dialog_select_operation:I = 0x7f0400ad

.field public static final ttdownloader_item_permission:I = 0x7f0400ae


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
