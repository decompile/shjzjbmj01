.class public final Lcom/qm/shjzjb/mj01/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qm/shjzjb/mj01/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f080000

.field public static final abc_action_bar_up_description:I = 0x7f080001

.field public static final abc_action_menu_overflow_description:I = 0x7f080002

.field public static final abc_action_mode_done:I = 0x7f080003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f080004

.field public static final abc_activitychooserview_choose_application:I = 0x7f080005

.field public static final abc_capital_off:I = 0x7f080006

.field public static final abc_capital_on:I = 0x7f080007

.field public static final abc_menu_alt_shortcut_label:I = 0x7f080008

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f080009

.field public static final abc_menu_delete_shortcut_label:I = 0x7f08000a

.field public static final abc_menu_enter_shortcut_label:I = 0x7f08000b

.field public static final abc_menu_function_shortcut_label:I = 0x7f08000c

.field public static final abc_menu_meta_shortcut_label:I = 0x7f08000d

.field public static final abc_menu_shift_shortcut_label:I = 0x7f08000e

.field public static final abc_menu_space_shortcut_label:I = 0x7f08000f

.field public static final abc_menu_sym_shortcut_label:I = 0x7f080010

.field public static final abc_prepend_shortcut_label:I = 0x7f080011

.field public static final abc_search_hint:I = 0x7f080012

.field public static final abc_searchview_description_clear:I = 0x7f080013

.field public static final abc_searchview_description_query:I = 0x7f080014

.field public static final abc_searchview_description_search:I = 0x7f080015

.field public static final abc_searchview_description_submit:I = 0x7f080016

.field public static final abc_searchview_description_voice:I = 0x7f080017

.field public static final abc_shareactionprovider_share_with:I = 0x7f080018

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f080019

.field public static final abc_toolbar_collapse_description:I = 0x7f08001a

.field public static final app_name:I = 0x7f08002d

.field public static final done:I = 0x7f08001d

.field public static final go:I = 0x7f08001e

.field public static final hours_ago:I = 0x7f08002e

.field public static final just_now:I = 0x7f08002f

.field public static final ksad_ad_default_author:I = 0x7f080030

.field public static final ksad_ad_default_username:I = 0x7f080031

.field public static final ksad_ad_default_username_normal:I = 0x7f080032

.field public static final ksad_ad_function_disable:I = 0x7f080033

.field public static final ksad_click_to_next_video:I = 0x7f080034

.field public static final ksad_data_error_toast:I = 0x7f080035

.field public static final ksad_default_no_more_tip_or_toast_txt:I = 0x7f080036

.field public static final ksad_entry_tab_like_format:I = 0x7f080037

.field public static final ksad_half_page_loading_error_tip:I = 0x7f080038

.field public static final ksad_half_page_loading_no_comment_tip:I = 0x7f080039

.field public static final ksad_half_page_loading_no_related_tip:I = 0x7f08003a

.field public static final ksad_look_related_button:I = 0x7f08003b

.field public static final ksad_look_related_title:I = 0x7f08003c

.field public static final ksad_network_dataFlow_tip:I = 0x7f08003d

.field public static final ksad_network_error_toast:I = 0x7f08003e

.field public static final ksad_page_load_more_tip:I = 0x7f08003f

.field public static final ksad_page_load_no_more_tip:I = 0x7f080040

.field public static final ksad_page_loading_data_error_sub_title:I = 0x7f080041

.field public static final ksad_page_loading_data_error_title:I = 0x7f080042

.field public static final ksad_page_loading_data_limit_error_title:I = 0x7f080043

.field public static final ksad_page_loading_error_retry:I = 0x7f080044

.field public static final ksad_page_loading_network_error_sub_title:I = 0x7f080045

.field public static final ksad_page_loading_network_error_title:I = 0x7f080046

.field public static final ksad_photo_hot_enter_label_text:I = 0x7f080047

.field public static final ksad_photo_hot_enter_watch_count_format:I = 0x7f080048

.field public static final ksad_photo_hot_enter_watch_extra_button_format:I = 0x7f080049

.field public static final ksad_photo_hot_enter_watch_extra_button_format_v2:I = 0x7f08004a

.field public static final ksad_photo_hot_scroll_more_hot_label:I = 0x7f08004b

.field public static final ksad_reward_default_tip:I = 0x7f08004c

.field public static final ksad_reward_success_tip:I = 0x7f08004d

.field public static final ksad_slide_left_tips:I = 0x7f08004e

.field public static final ksad_slide_up_tips:I = 0x7f08004f

.field public static final ksad_text_placeholder:I = 0x7f080050

.field public static final ksad_trend_is_no_valid:I = 0x7f080051

.field public static final ksad_trend_list_item_photo_count_format:I = 0x7f080052

.field public static final ksad_trend_list_panel_title:I = 0x7f080053

.field public static final ksad_trend_title_info_format:I = 0x7f080054

.field public static final ksad_tube_author_name_label_text:I = 0x7f080055

.field public static final ksad_tube_enter_paly_count:I = 0x7f080056

.field public static final ksad_tube_episode_index:I = 0x7f080057

.field public static final ksad_tube_hot_list_label_string:I = 0x7f080058

.field public static final ksad_tube_more_episode:I = 0x7f080059

.field public static final ksad_tube_update_default:I = 0x7f08005a

.field public static final ksad_tube_update_finished_format_text:I = 0x7f08005b

.field public static final ksad_tube_update_unfinished_format_text:I = 0x7f08005c

.field public static final ksad_video_no_found:I = 0x7f08005d

.field public static final ksad_watch_next_video:I = 0x7f08005e

.field public static final minutes_ago:I = 0x7f08005f

.field public static final next:I = 0x7f08001f

.field public static final search:I = 0x7f080020

.field public static final search_menu_title:I = 0x7f08001b

.field public static final send:I = 0x7f080021

.field public static final sig_ad:I = 0x7f080022

.field public static final sig_back:I = 0x7f080023

.field public static final sig_close:I = 0x7f080024

.field public static final sig_close_ad_cancel:I = 0x7f080025

.field public static final sig_close_ad_message:I = 0x7f080026

.field public static final sig_close_ad_ok:I = 0x7f080027

.field public static final sig_close_ad_title:I = 0x7f080028

.field public static final sig_close_args:I = 0x7f080029

.field public static final sig_skip_ad_args:I = 0x7f08002a

.field public static final sig_skip_args_1:I = 0x7f08002b

.field public static final sig_skip_args_2:I = 0x7f08002c

.field public static final status_bar_notification_info_overflow:I = 0x7f08001c

.field public static final tt_00_00:I = 0x7f080060

.field public static final tt_ad:I = 0x7f080061

.field public static final tt_ad_logo_txt:I = 0x7f080062

.field public static final tt_app_name:I = 0x7f080063

.field public static final tt_app_privacy_dialog_title:I = 0x7f080064

.field public static final tt_appdownloader_button_cancel_download:I = 0x7f080065

.field public static final tt_appdownloader_button_queue_for_wifi:I = 0x7f080066

.field public static final tt_appdownloader_button_start_now:I = 0x7f080067

.field public static final tt_appdownloader_download_percent:I = 0x7f080068

.field public static final tt_appdownloader_download_remaining:I = 0x7f080069

.field public static final tt_appdownloader_download_unknown_title:I = 0x7f08006a

.field public static final tt_appdownloader_duration_hours:I = 0x7f08006b

.field public static final tt_appdownloader_duration_minutes:I = 0x7f08006c

.field public static final tt_appdownloader_duration_seconds:I = 0x7f08006d

.field public static final tt_appdownloader_jump_unknown_source:I = 0x7f08006e

.field public static final tt_appdownloader_label_cancel:I = 0x7f08006f

.field public static final tt_appdownloader_label_cancel_directly:I = 0x7f080070

.field public static final tt_appdownloader_label_ok:I = 0x7f080071

.field public static final tt_appdownloader_label_reserve_wifi:I = 0x7f080072

.field public static final tt_appdownloader_notification_download:I = 0x7f080073

.field public static final tt_appdownloader_notification_download_complete_open:I = 0x7f080074

.field public static final tt_appdownloader_notification_download_complete_with_install:I = 0x7f080075

.field public static final tt_appdownloader_notification_download_complete_without_install:I = 0x7f080076

.field public static final tt_appdownloader_notification_download_continue:I = 0x7f080077

.field public static final tt_appdownloader_notification_download_delete:I = 0x7f080078

.field public static final tt_appdownloader_notification_download_failed:I = 0x7f080079

.field public static final tt_appdownloader_notification_download_install:I = 0x7f08007a

.field public static final tt_appdownloader_notification_download_open:I = 0x7f08007b

.field public static final tt_appdownloader_notification_download_pause:I = 0x7f08007c

.field public static final tt_appdownloader_notification_download_restart:I = 0x7f08007d

.field public static final tt_appdownloader_notification_download_resume:I = 0x7f08007e

.field public static final tt_appdownloader_notification_download_space_failed:I = 0x7f08007f

.field public static final tt_appdownloader_notification_download_waiting_net:I = 0x7f080080

.field public static final tt_appdownloader_notification_download_waiting_wifi:I = 0x7f080081

.field public static final tt_appdownloader_notification_downloading:I = 0x7f080082

.field public static final tt_appdownloader_notification_install_finished_open:I = 0x7f080083

.field public static final tt_appdownloader_notification_insufficient_space_error:I = 0x7f080084

.field public static final tt_appdownloader_notification_need_wifi_for_size:I = 0x7f080085

.field public static final tt_appdownloader_notification_no_internet_error:I = 0x7f080086

.field public static final tt_appdownloader_notification_no_wifi_and_in_net:I = 0x7f080087

.field public static final tt_appdownloader_notification_paused_in_background:I = 0x7f080088

.field public static final tt_appdownloader_notification_pausing:I = 0x7f080089

.field public static final tt_appdownloader_notification_prepare:I = 0x7f08008a

.field public static final tt_appdownloader_notification_request_btn_no:I = 0x7f08008b

.field public static final tt_appdownloader_notification_request_btn_yes:I = 0x7f08008c

.field public static final tt_appdownloader_notification_request_message:I = 0x7f08008d

.field public static final tt_appdownloader_notification_request_title:I = 0x7f08008e

.field public static final tt_appdownloader_notification_waiting_download_complete_handler:I = 0x7f08008f

.field public static final tt_appdownloader_resume_in_wifi:I = 0x7f080090

.field public static final tt_appdownloader_tip:I = 0x7f080091

.field public static final tt_appdownloader_wifi_recommended_body:I = 0x7f080092

.field public static final tt_appdownloader_wifi_recommended_title:I = 0x7f080093

.field public static final tt_appdownloader_wifi_required_body:I = 0x7f080094

.field public static final tt_appdownloader_wifi_required_title:I = 0x7f080095

.field public static final tt_auto_play_cancel_text:I = 0x7f080096

.field public static final tt_cancel:I = 0x7f080097

.field public static final tt_comment_num:I = 0x7f080098

.field public static final tt_comment_num_backup:I = 0x7f080099

.field public static final tt_comment_score:I = 0x7f08009a

.field public static final tt_common_download_app_detail:I = 0x7f08009b

.field public static final tt_common_download_app_privacy:I = 0x7f08009c

.field public static final tt_common_download_cancel:I = 0x7f08009d

.field public static final tt_confirm_download:I = 0x7f08009e

.field public static final tt_confirm_download_have_app_name:I = 0x7f08009f

.field public static final tt_dislike_header_tv_back:I = 0x7f0800a0

.field public static final tt_dislike_header_tv_title:I = 0x7f0800a1

.field public static final tt_full_screen_skip_tx:I = 0x7f0800a2

.field public static final tt_label_cancel:I = 0x7f0800a3

.field public static final tt_label_ok:I = 0x7f0800a4

.field public static final tt_mediation_format_adapter_name:I = 0x7f0800a5

.field public static final tt_mediation_format_error_msg:I = 0x7f0800a6

.field public static final tt_mediation_format_no_ad_error_msg:I = 0x7f0800a7

.field public static final tt_mediation_format_setting_error_msg:I = 0x7f0800a8

.field public static final tt_mediation_format_show_success_msg:I = 0x7f0800a9

.field public static final tt_mediation_format_success_msg:I = 0x7f0800aa

.field public static final tt_mediation_label_cancel:I = 0x7f0800ab

.field public static final tt_mediation_label_ok:I = 0x7f0800ac

.field public static final tt_mediation_permission_denied:I = 0x7f0800ad

.field public static final tt_mediation_request_permission_descript_external_storage:I = 0x7f0800ae

.field public static final tt_mediation_request_permission_descript_location:I = 0x7f0800af

.field public static final tt_mediation_request_permission_descript_read_phone_state:I = 0x7f0800b0

.field public static final tt_no_network:I = 0x7f0800b1

.field public static final tt_open_app_detail_developer:I = 0x7f0800b2

.field public static final tt_open_app_detail_privacy:I = 0x7f0800b3

.field public static final tt_open_app_detail_privacy_list:I = 0x7f0800b4

.field public static final tt_open_app_name:I = 0x7f0800b5

.field public static final tt_open_app_version:I = 0x7f0800b6

.field public static final tt_open_landing_page_app_name:I = 0x7f0800b7

.field public static final tt_permission_denied:I = 0x7f0800b8

.field public static final tt_playable_btn_play:I = 0x7f0800b9

.field public static final tt_request_permission_descript_external_storage:I = 0x7f0800ba

.field public static final tt_request_permission_descript_location:I = 0x7f0800bb

.field public static final tt_request_permission_descript_read_phone_state:I = 0x7f0800bc

.field public static final tt_reward_feedback:I = 0x7f0800bd

.field public static final tt_reward_screen_skip_tx:I = 0x7f0800be

.field public static final tt_splash_skip_tv_text:I = 0x7f0800bf

.field public static final tt_tip:I = 0x7f0800c0

.field public static final tt_unlike:I = 0x7f0800c1

.field public static final tt_video_bytesize:I = 0x7f0800c2

.field public static final tt_video_bytesize_M:I = 0x7f0800c3

.field public static final tt_video_bytesize_MB:I = 0x7f0800c4

.field public static final tt_video_continue_play:I = 0x7f0800c5

.field public static final tt_video_dial_phone:I = 0x7f0800c6

.field public static final tt_video_dial_replay:I = 0x7f0800c7

.field public static final tt_video_download_apk:I = 0x7f0800c8

.field public static final tt_video_mobile_go_detail:I = 0x7f0800c9

.field public static final tt_video_retry_des_txt:I = 0x7f0800ca

.field public static final tt_video_without_wifi_tips:I = 0x7f0800cb

.field public static final tt_web_title_default:I = 0x7f0800cc

.field public static final tt_will_play:I = 0x7f0800cd


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
