.class public final Lcom/qm/shjzjb/mj01/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qm/shjzjb/mj01/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0c00b7

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0c00b8

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0c00b9

.field public static final abc_btn_colored_text_material:I = 0x7f0c00ba

.field public static final abc_color_highlight_material:I = 0x7f0c00bb

.field public static final abc_hint_foreground_material_dark:I = 0x7f0c00bc

.field public static final abc_hint_foreground_material_light:I = 0x7f0c00bd

.field public static final abc_input_method_navigation_guard:I = 0x7f0c0001

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0c00be

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0c00bf

.field public static final abc_primary_text_material_dark:I = 0x7f0c00c0

.field public static final abc_primary_text_material_light:I = 0x7f0c00c1

.field public static final abc_search_url_text:I = 0x7f0c00c2

.field public static final abc_search_url_text_normal:I = 0x7f0c0002

.field public static final abc_search_url_text_pressed:I = 0x7f0c0003

.field public static final abc_search_url_text_selected:I = 0x7f0c0004

.field public static final abc_secondary_text_material_dark:I = 0x7f0c00c3

.field public static final abc_secondary_text_material_light:I = 0x7f0c00c4

.field public static final abc_tint_btn_checkable:I = 0x7f0c00c5

.field public static final abc_tint_default:I = 0x7f0c00c6

.field public static final abc_tint_edittext:I = 0x7f0c00c7

.field public static final abc_tint_seek_thumb:I = 0x7f0c00c8

.field public static final abc_tint_spinner:I = 0x7f0c00c9

.field public static final abc_tint_switch_track:I = 0x7f0c00ca

.field public static final accent_material_dark:I = 0x7f0c0005

.field public static final accent_material_light:I = 0x7f0c0006

.field public static final background_floating_material_dark:I = 0x7f0c0007

.field public static final background_floating_material_light:I = 0x7f0c0008

.field public static final background_light_dark:I = 0x7f0c0009

.field public static final background_material_dark:I = 0x7f0c000a

.field public static final background_material_light:I = 0x7f0c000b

.field public static final black:I = 0x7f0c000c

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0c000d

.field public static final bright_foreground_disabled_material_light:I = 0x7f0c000e

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0c000f

.field public static final bright_foreground_inverse_material_light:I = 0x7f0c0010

.field public static final bright_foreground_material_dark:I = 0x7f0c0011

.field public static final bright_foreground_material_light:I = 0x7f0c0012

.field public static final browser_actions_bg_grey:I = 0x7f0c0013

.field public static final browser_actions_divider_color:I = 0x7f0c0014

.field public static final browser_actions_text_color:I = 0x7f0c0015

.field public static final browser_actions_title_color:I = 0x7f0c0016

.field public static final button_material_dark:I = 0x7f0c0017

.field public static final button_material_light:I = 0x7f0c0018

.field public static final colorAccent:I = 0x7f0c0019

.field public static final colorControlActivated:I = 0x7f0c001a

.field public static final colorPrimaryDark:I = 0x7f0c001b

.field public static final colorSplashBackground:I = 0x7f0c001c

.field public static final colorToolbarText:I = 0x7f0c001d

.field public static final colorTransparent:I = 0x7f0c001e

.field public static final defaultDivisionLine:I = 0x7f0c001f

.field public static final defaultHintText:I = 0x7f0c0020

.field public static final defaultLinkText:I = 0x7f0c0021

.field public static final defaultMainText:I = 0x7f0c0022

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0c0023

.field public static final dim_foreground_disabled_material_light:I = 0x7f0c0024

.field public static final dim_foreground_material_dark:I = 0x7f0c0025

.field public static final dim_foreground_material_light:I = 0x7f0c0026

.field public static final error_color_material_dark:I = 0x7f0c0027

.field public static final error_color_material_light:I = 0x7f0c0028

.field public static final foreground_material_dark:I = 0x7f0c0029

.field public static final foreground_material_light:I = 0x7f0c002a

.field public static final highlighted_text_material_dark:I = 0x7f0c002b

.field public static final highlighted_text_material_light:I = 0x7f0c002c

.field public static final ksad_black_alpha20:I = 0x7f0c002d

.field public static final ksad_black_alpha50:I = 0x7f0c002e

.field public static final ksad_feed_covert_finish:I = 0x7f0c002f

.field public static final ksad_feed_covert_idle:I = 0x7f0c0030

.field public static final ksad_hale_page_loading_error_title_dark_color:I = 0x7f0c0031

.field public static final ksad_hale_page_loading_error_title_light_color:I = 0x7f0c0032

.field public static final ksad_page_loading_error_container_dark_color:I = 0x7f0c0033

.field public static final ksad_page_loading_error_container_light_color:I = 0x7f0c0034

.field public static final ksad_page_loading_error_retry_dark_color:I = 0x7f0c0035

.field public static final ksad_page_loading_error_retry_light_color:I = 0x7f0c0036

.field public static final ksad_page_loading_error_sub_title_dark_color:I = 0x7f0c0037

.field public static final ksad_page_loading_error_sub_title_light_color:I = 0x7f0c0038

.field public static final ksad_page_loading_error_title_dark_color:I = 0x7f0c0039

.field public static final ksad_page_loading_error_title_light_color:I = 0x7f0c003a

.field public static final ksad_photo_hot_enter_bg:I = 0x7f0c003b

.field public static final ksad_photo_hot_enter_text:I = 0x7f0c003c

.field public static final ksad_photo_hot_list_item_count_text:I = 0x7f0c003d

.field public static final ksad_photo_hot_list_item_name_text:I = 0x7f0c003e

.field public static final ksad_profile_home_bg:I = 0x7f0c003f

.field public static final ksad_translucent:I = 0x7f0c0040

.field public static final ksad_tube_activity_bg:I = 0x7f0c0041

.field public static final ksad_tube_detail_description_text:I = 0x7f0c0042

.field public static final ksad_tube_detail_name_text:I = 0x7f0c0043

.field public static final ksad_tube_detail_no_more_text:I = 0x7f0c0044

.field public static final ksad_tube_enter_bg:I = 0x7f0c0045

.field public static final ksad_tube_enter_text:I = 0x7f0c0046

.field public static final ksad_tube_episode_photo_bg:I = 0x7f0c0047

.field public static final ksad_tube_episode_title:I = 0x7f0c0048

.field public static final ksad_tube_net_error_text:I = 0x7f0c0049

.field public static final ksad_tube_profile_no_more_text:I = 0x7f0c004a

.field public static final ksad_tube_trend_item_divider:I = 0x7f0c004b

.field public static final material_blue_grey_800:I = 0x7f0c004c

.field public static final material_blue_grey_900:I = 0x7f0c004d

.field public static final material_blue_grey_950:I = 0x7f0c004e

.field public static final material_deep_teal_200:I = 0x7f0c004f

.field public static final material_deep_teal_500:I = 0x7f0c0050

.field public static final material_grey_100:I = 0x7f0c0051

.field public static final material_grey_300:I = 0x7f0c0052

.field public static final material_grey_50:I = 0x7f0c0053

.field public static final material_grey_600:I = 0x7f0c0054

.field public static final material_grey_800:I = 0x7f0c0055

.field public static final material_grey_850:I = 0x7f0c0056

.field public static final material_grey_900:I = 0x7f0c0057

.field public static final notification_action_color_filter:I = 0x7f0c0000

.field public static final notification_icon_bg_color:I = 0x7f0c0058

.field public static final primary_dark_material_dark:I = 0x7f0c0059

.field public static final primary_dark_material_light:I = 0x7f0c005a

.field public static final primary_material_dark:I = 0x7f0c005b

.field public static final primary_material_light:I = 0x7f0c005c

.field public static final primary_text_default_material_dark:I = 0x7f0c005d

.field public static final primary_text_default_material_light:I = 0x7f0c005e

.field public static final primary_text_disabled_material_dark:I = 0x7f0c005f

.field public static final primary_text_disabled_material_light:I = 0x7f0c0060

.field public static final ripple_material_dark:I = 0x7f0c0061

.field public static final ripple_material_light:I = 0x7f0c0062

.field public static final secondary_text_default_material_dark:I = 0x7f0c0063

.field public static final secondary_text_default_material_light:I = 0x7f0c0064

.field public static final secondary_text_disabled_material_dark:I = 0x7f0c0065

.field public static final secondary_text_disabled_material_light:I = 0x7f0c0066

.field public static final switch_blue:I = 0x7f0c0067

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0c0068

.field public static final switch_thumb_disabled_material_light:I = 0x7f0c0069

.field public static final switch_thumb_material_dark:I = 0x7f0c00cb

.field public static final switch_thumb_material_light:I = 0x7f0c00cc

.field public static final switch_thumb_normal_material_dark:I = 0x7f0c006a

.field public static final switch_thumb_normal_material_light:I = 0x7f0c006b

.field public static final tooltip_background_dark:I = 0x7f0c006c

.field public static final tooltip_background_light:I = 0x7f0c006d

.field public static final tt_app_detail_bg:I = 0x7f0c006e

.field public static final tt_app_detail_line_bg:I = 0x7f0c006f

.field public static final tt_app_detail_privacy_text_bg:I = 0x7f0c0070

.field public static final tt_app_detail_stroke_bg:I = 0x7f0c0071

.field public static final tt_appdownloader_notification_material_background_color:I = 0x7f0c0072

.field public static final tt_appdownloader_notification_title_color:I = 0x7f0c0073

.field public static final tt_appdownloader_s1:I = 0x7f0c0074

.field public static final tt_appdownloader_s13:I = 0x7f0c0075

.field public static final tt_appdownloader_s18:I = 0x7f0c0076

.field public static final tt_appdownloader_s4:I = 0x7f0c0077

.field public static final tt_appdownloader_s8:I = 0x7f0c0078

.field public static final tt_cancle_bg:I = 0x7f0c0079

.field public static final tt_common_download_bg:I = 0x7f0c007a

.field public static final tt_common_download_btn_bg:I = 0x7f0c007b

.field public static final tt_dislike_dialog_background:I = 0x7f0c007c

.field public static final tt_dislike_transparent:I = 0x7f0c007d

.field public static final tt_divider:I = 0x7f0c007e

.field public static final tt_download_app_name:I = 0x7f0c007f

.field public static final tt_download_bar_background:I = 0x7f0c0080

.field public static final tt_download_bar_background_new:I = 0x7f0c0081

.field public static final tt_download_text_background:I = 0x7f0c0082

.field public static final tt_draw_btn_back:I = 0x7f0c0083

.field public static final tt_full_screen_skip_bg:I = 0x7f0c0084

.field public static final tt_header_font:I = 0x7f0c0085

.field public static final tt_heise3:I = 0x7f0c0086

.field public static final tt_listview:I = 0x7f0c0087

.field public static final tt_listview_press:I = 0x7f0c0088

.field public static final tt_mediation_transparent:I = 0x7f0c0089

.field public static final tt_rating_comment:I = 0x7f0c008a

.field public static final tt_rating_comment_vertical:I = 0x7f0c008b

.field public static final tt_rating_star:I = 0x7f0c008c

.field public static final tt_skip_red:I = 0x7f0c008d

.field public static final tt_ssxinbaise4:I = 0x7f0c008e

.field public static final tt_ssxinbaise4_press:I = 0x7f0c008f

.field public static final tt_ssxinheihui3:I = 0x7f0c0090

.field public static final tt_ssxinhongse1:I = 0x7f0c0091

.field public static final tt_ssxinmian1:I = 0x7f0c0092

.field public static final tt_ssxinmian11:I = 0x7f0c0093

.field public static final tt_ssxinmian15:I = 0x7f0c0094

.field public static final tt_ssxinmian6:I = 0x7f0c0095

.field public static final tt_ssxinmian7:I = 0x7f0c0096

.field public static final tt_ssxinmian8:I = 0x7f0c0097

.field public static final tt_ssxinxian11:I = 0x7f0c0098

.field public static final tt_ssxinxian11_selected:I = 0x7f0c0099

.field public static final tt_ssxinxian3:I = 0x7f0c009a

.field public static final tt_ssxinxian3_press:I = 0x7f0c009b

.field public static final tt_ssxinzi12:I = 0x7f0c009c

.field public static final tt_ssxinzi15:I = 0x7f0c009d

.field public static final tt_ssxinzi4:I = 0x7f0c009e

.field public static final tt_ssxinzi9:I = 0x7f0c009f

.field public static final tt_text_font:I = 0x7f0c00a0

.field public static final tt_titlebar_background_dark:I = 0x7f0c00a1

.field public static final tt_titlebar_background_ffffff:I = 0x7f0c00a2

.field public static final tt_titlebar_background_light:I = 0x7f0c00a3

.field public static final tt_trans_black:I = 0x7f0c00a4

.field public static final tt_trans_half_black:I = 0x7f0c00a5

.field public static final tt_transparent:I = 0x7f0c00a6

.field public static final tt_video_player_text:I = 0x7f0c00a7

.field public static final tt_video_player_text_withoutnight:I = 0x7f0c00a8

.field public static final tt_video_playerbg_color:I = 0x7f0c00a9

.field public static final tt_video_shadow_color:I = 0x7f0c00aa

.field public static final tt_video_shaoow_color_fullscreen:I = 0x7f0c00ab

.field public static final tt_video_time_color:I = 0x7f0c00ac

.field public static final tt_video_traffic_tip_background_color:I = 0x7f0c00ad

.field public static final tt_video_transparent:I = 0x7f0c00ae

.field public static final tt_white:I = 0x7f0c00af

.field public static final ttdownloader_transparent:I = 0x7f0c00b0

.field public static final w1:I = 0x7f0c00b1

.field public static final w2:I = 0x7f0c00b2

.field public static final w3:I = 0x7f0c00b3

.field public static final w4:I = 0x7f0c00b4

.field public static final w5:I = 0x7f0c00b5

.field public static final white:I = 0x7f0c00b6


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
