.class Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity$2;
.super Ljava/lang/Object;
.source "WXEntryActivity.java"

# interfaces
.implements Lorg/cocos2dx/javascript/NetUtils$MyNetCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->getUserInfo(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;


# direct methods
.method constructor <init>(Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity$2;->this$0:Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failed(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 6

    .line 158
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    const-string v4, "wx_get_user_info_fail"

    const-string v5, "java"

    const/16 v3, 0x3fb

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string p1, "<======getUserInfo:fail======>"

    .line 159
    invoke-static {p1}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    return-void
.end method

.method public success(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 150
    invoke-virtual {p2}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object p1

    invoke-virtual {p1}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object p1

    const-string p2, "<======getUserInfo:Succ:======>"

    .line 151
    invoke-static {p2}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    .line 152
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    const-string v4, "wx_get_user_info_succ"

    const-string v5, "java"

    const/16 v3, 0x3fa

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object p2, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity$2;->this$0:Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;

    invoke-static {p2, p1}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->access$100(Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;Ljava/lang/String;)V

    return-void
.end method
