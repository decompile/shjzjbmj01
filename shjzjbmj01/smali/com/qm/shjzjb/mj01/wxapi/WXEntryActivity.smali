.class public Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;
.super Landroid/app/Activity;
.source "WXEntryActivity.java"

# interfaces
.implements Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;


# static fields
.field public static WX_APP_ID:Ljava/lang/String; = "wx6d5367987fd69ed7"


# instance fields
.field private GetCodeRequest:Ljava/lang/String;

.field private GetUserInfo:Ljava/lang/String;

.field private WX_APP_SECRET:Ljava/lang/String;

.field private access_token:Ljava/lang/String;

.field private api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

.field private openId:Ljava/lang/String;

.field private requestTime:I

.field private resp:Lcom/tencent/mm/opensdk/modelbase/BaseResp;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->resp:Lcom/tencent/mm/opensdk/modelbase/BaseResp;

    const-string v0, "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code"

    .line 42
    iput-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetCodeRequest:Ljava/lang/String;

    const-string v0, "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID"

    .line 44
    iput-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetUserInfo:Ljava/lang/String;

    const-string v0, "667330a112d1a2cf885378458150de4b"

    .line 45
    iput-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->WX_APP_SECRET:Ljava/lang/String;

    const-string v0, ""

    .line 46
    iput-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->access_token:Ljava/lang/String;

    const-string v0, ""

    .line 47
    iput-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->openId:Ljava/lang/String;

    const/4 v0, 0x3

    .line 48
    iput v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->requestTime:I

    return-void
.end method

.method static synthetic access$000(Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->parseJSONWithGSON(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->parseJSONUser(Ljava/lang/String;)V

    return-void
.end method

.method private getCodeRequest(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 181
    iget-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetCodeRequest:Ljava/lang/String;

    const-string v1, "APPID"

    sget-object v2, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->WX_APP_ID:Ljava/lang/String;

    .line 182
    invoke-direct {p0, v2}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->urlEnodeUTF8(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 181
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetCodeRequest:Ljava/lang/String;

    .line 183
    iget-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetCodeRequest:Ljava/lang/String;

    const-string v1, "SECRET"

    iget-object v2, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->WX_APP_SECRET:Ljava/lang/String;

    .line 184
    invoke-direct {p0, v2}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->urlEnodeUTF8(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 183
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetCodeRequest:Ljava/lang/String;

    .line 185
    iget-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetCodeRequest:Ljava/lang/String;

    const-string v1, "CODE"

    invoke-direct {p0, p1}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->urlEnodeUTF8(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetCodeRequest:Ljava/lang/String;

    .line 186
    iget-object p1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetCodeRequest:Ljava/lang/String;

    return-object p1
.end method

.method private getUserInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "Android WXLogin"

    .line 197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access_token = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " ,openid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetUserInfo:Ljava/lang/String;

    const-string v1, "ACCESS_TOKEN"

    invoke-virtual {v0, v1, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetUserInfo:Ljava/lang/String;

    .line 201
    iget-object p1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetUserInfo:Ljava/lang/String;

    const-string v0, "OPENID"

    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetUserInfo:Ljava/lang/String;

    .line 202
    iget-object p1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->GetUserInfo:Ljava/lang/String;

    return-object p1
.end method

.method private getUserInfo(Ljava/lang/String;)V
    .locals 3

    const-string v0, "<======getUserInfo======>"

    .line 141
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    const-string v0, "\""

    const-string v1, ""

    .line 142
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Android WXLogin"

    .line 143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u767c\u8d77\u8acb\u6c42 URL = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    new-instance v0, Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentSkipListMap;-><init>()V

    .line 145
    invoke-static {}, Lorg/cocos2dx/javascript/NetUtils;->getInstance()Lorg/cocos2dx/javascript/NetUtils;

    move-result-object v1

    .line 146
    new-instance v2, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity$2;

    invoke-direct {v2, p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity$2;-><init>(Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;)V

    invoke-virtual {v1, p1, v0, v2}, Lorg/cocos2dx/javascript/NetUtils;->postDataAsynToNet(Ljava/lang/String;Ljava/util/Map;Lorg/cocos2dx/javascript/NetUtils$MyNetCall;)V

    return-void
.end method

.method private parseJSONUser(Ljava/lang/String;)V
    .locals 4

    const-string v0, "<==============\u89e3\u6790\u7528\u6237\u4fe1\u606fparseJSONUser,success============>"

    .line 233
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    const-string v0, "Android WXLogin"

    .line 234
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u83b7\u53d6\u7528\u6237\u79c1\u4eba\u4fe1\u606f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    new-instance v0, Lcom/google/gson/JsonParser;

    invoke-direct {v0}, Lcom/google/gson/JsonParser;-><init>()V

    .line 236
    invoke-virtual {v0, p1}, Lcom/google/gson/JsonParser;->parse(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    const-string v0, "errmsg"

    .line 237
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const-string v0, "errcode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->requestTime:I

    if-lez v0, :cond_0

    .line 238
    iget v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->requestTime:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->requestTime:I

    .line 239
    iget-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->access_token:Ljava/lang/String;

    iget-object v1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->openId:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->getUserInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Android WXLogin"

    .line 240
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u62fc\u63a5\u7ed3\u679c \u7b2c\u4e8c\u6b21"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-direct {p0, v0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->getUserInfo(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "<==========\u89e3\u6790\u7528\u6237\u4fe1\u606fparseJSONUser,end===========>"

    .line 244
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    .line 245
    invoke-static {p1}, Lorg/cocos2dx/javascript/AppActivity;->setUserMsg(Ljava/lang/String;)V

    return-void
.end method

.method private parseJSONWithGSON(Ljava/lang/String;)V
    .locals 4

    const-string v0, "<==========parseJSONWithGSON==========>"

    .line 219
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    const-string v0, "Android WXLogin"

    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u83b7\u53d6\u62fc\u63a5\u4fe1\u606f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    new-instance v0, Lcom/google/gson/JsonParser;

    invoke-direct {v0}, Lcom/google/gson/JsonParser;-><init>()V

    .line 222
    invoke-virtual {v0, p1}, Lcom/google/gson/JsonParser;->parse(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v0

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access_token"

    invoke-virtual {v0, v2}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "openid"

    invoke-virtual {v0, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->getUserInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Android WXLogin"

    .line 225
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u62fc\u63a5\u7ed3\u679c"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string p1, "<==========\u62fc\u63a5\u7ed3\u679c==========>"

    .line 226
    invoke-static {p1}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    .line 227
    invoke-direct {p0, v0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->getUserInfo(Ljava/lang/String;)V

    return-void
.end method

.method private urlEnodeUTF8(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    :try_start_0
    const-string v0, "UTF-8"

    .line 209
    invoke-static {p1, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 211
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object p1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    sget-object p1, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->WX_APP_ID:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/tencent/mm/opensdk/openapi/WXAPIFactory;->createWXAPI(Landroid/content/Context;Ljava/lang/String;Z)Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    move-result-object p1

    iput-object p1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    .line 55
    :try_start_0
    iget-object p1, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-virtual {p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-interface {p1, v0, p0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->handleIntent(Landroid/content/Intent;Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 60
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 168
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 169
    invoke-virtual {p0, p1}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->setIntent(Landroid/content/Intent;)V

    .line 170
    iget-object v0, p0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {v0, p1, p0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->handleIntent(Landroid/content/Intent;Lcom/tencent/mm/opensdk/openapi/IWXAPIEventHandler;)Z

    .line 171
    invoke-virtual {p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->finish()V

    return-void
.end method

.method public onReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)V
    .locals 0

    .line 67
    invoke-virtual {p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->finish()V

    return-void
.end method

.method public onResp(Lcom/tencent/mm/opensdk/modelbase/BaseResp;)V
    .locals 9

    .line 77
    iget v0, p1, Lcom/tencent/mm/opensdk/modelbase/BaseResp;->errCode:I

    const/4 v1, -0x4

    if-eq v0, v1, :cond_2

    const/4 v1, -0x2

    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_0

    .line 127
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v2

    sget-object v3, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sget-object v4, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    const/16 v5, 0x3f9

    const-string v6, "auth_defalt"

    const-string v7, ""

    invoke-virtual/range {v2 .. v7}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string p1, "\u53d1\u9001\u8fd4\u56de"

    const-string v0, "Android WXLogin"

    .line 129
    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string p1, "\u56de\u590d\u4e86onResp,default"

    .line 130
    invoke-static {p1}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->finish()V

    goto/16 :goto_0

    .line 79
    :cond_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    const/16 v3, 0x3f6

    const-string v4, "auth_agree"

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string v0, "\u53d1\u9001\u6210\u529f"

    const-string v1, "Android WXLogin"

    .line 81
    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    check-cast p1, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Resp;

    iget-object p1, p1, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Resp;->code:Ljava/lang/String;

    const-string v0, "TGA"

    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "------------"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-direct {p0, p1}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->getCodeRequest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 88
    new-instance v0, Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentSkipListMap;-><init>()V

    .line 89
    invoke-static {}, Lorg/cocos2dx/javascript/NetUtils;->getInstance()Lorg/cocos2dx/javascript/NetUtils;

    move-result-object v1

    .line 90
    new-instance v2, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity$1;

    invoke-direct {v2, p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity$1;-><init>(Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;)V

    invoke-virtual {v1, p1, v0, v2}, Lorg/cocos2dx/javascript/NetUtils;->postDataAsynToNet(Ljava/lang/String;Ljava/util/Map;Lorg/cocos2dx/javascript/NetUtils$MyNetCall;)V

    .line 110
    invoke-virtual {p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->finish()V

    goto :goto_0

    .line 113
    :cond_1
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v3

    sget-object v4, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sget-object v5, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    const/16 v6, 0x3f8

    const-string v7, "auth_cancel"

    const-string v8, ""

    invoke-virtual/range {v3 .. v8}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string p1, "\u53d1\u9001\u53d6\u6d88"

    const-string v0, "Android WXLogin"

    .line 115
    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string p1, "\u56de\u590d\u4e86onResp,\u53d1\u9001\u53d6\u6d88"

    .line 116
    invoke-static {p1}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->finish()V

    goto :goto_0

    .line 120
    :cond_2
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    const/16 v3, 0x3f7

    const-string v4, "auth_denied"

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string p1, "\u53d1\u9001\u88ab\u62d2\u7edd"

    const-string v0, "Android WXLogin"

    .line 122
    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string p1, "\u56de\u590d\u4e86onResp,\u53d1\u9001\u88ab\u62d2\u7edd"

    .line 123
    invoke-static {p1}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->finish()V

    :goto_0
    return-void
.end method
