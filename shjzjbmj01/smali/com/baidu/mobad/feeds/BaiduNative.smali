.class public Lcom/baidu/mobad/feeds/BaiduNative;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeLoadAdListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$NativeADEventListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeEventListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;,
        Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/baidu/mobad/feeds/NativeResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private d:Lcom/baidu/mobads/production/b/e;

.field private e:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

.field private f:Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;

.field private g:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeEventListener;

.field private h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;)V
    .locals 1

    .line 250
    new-instance v0, Lcom/baidu/mobads/production/b/e;

    invoke-direct {v0, p1, p2}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/b/e;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;I)V
    .locals 1

    .line 272
    new-instance v0, Lcom/baidu/mobads/production/b/e;

    invoke-direct {v0, p1, p2, p4}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/b/e;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/b/e;)V
    .locals 2

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->b:Landroid/content/Context;

    .line 312
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 313
    iput-object p2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Ljava/lang/String;

    .line 314
    iput-object p3, p0, Lcom/baidu/mobad/feeds/BaiduNative;->e:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    .line 315
    invoke-static {p1}, Lcom/baidu/mobads/g/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/g/q;->a()V

    .line 316
    iput-object p4, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Z)V
    .locals 1

    .line 261
    new-instance v0, Lcom/baidu/mobads/production/b/e;

    invoke-direct {v0, p1, p2, p4}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/b/e;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;ZI)V
    .locals 1

    .line 284
    new-instance v0, Lcom/baidu/mobads/production/b/e;

    invoke-direct {v0, p1, p2, p4, p5}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/b/e;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;ZILjava/lang/String;)V
    .locals 6

    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->b:Landroid/content/Context;

    .line 291
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 292
    iput-object p2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Ljava/lang/String;

    .line 293
    iput-object p3, p0, Lcom/baidu/mobad/feeds/BaiduNative;->e:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    .line 294
    invoke-static {p1}, Lcom/baidu/mobads/g/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;

    move-result-object p3

    invoke-virtual {p3}, Lcom/baidu/mobads/g/q;->a()V

    .line 295
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_2

    const-string p3, "insite"

    .line 296
    invoke-virtual {p6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 297
    new-instance p3, Lcom/baidu/mobads/production/b/e;

    sget-object v3, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INSITE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;ZI)V

    iput-object p3, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    goto :goto_0

    :cond_0
    const-string p3, "sug"

    .line 299
    invoke-virtual {p6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 300
    new-instance p3, Lcom/baidu/mobads/production/b/e;

    sget-object v3, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SUG:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;ZI)V

    iput-object p3, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    goto :goto_0

    .line 303
    :cond_1
    new-instance p3, Lcom/baidu/mobads/production/b/e;

    invoke-direct {p3, p1, p2, p4, p5}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    iput-object p3, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/BaiduNative;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)I
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)I

    move-result p0

    return p0
.end method

.method private a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)I
    .locals 2

    if-eqz p1, :cond_0

    .line 433
    :try_start_0
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v0

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    .line 435
    new-instance v0, Lorg/json/JSONObject;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppOpenStrs()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "fb_act"

    .line 436
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->e:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/BaiduNative;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->a:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/b/e;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/baidu/mobad/feeds/BaiduNative;)Ljava/util/List;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->a:Ljava/util/List;

    return-object p0
.end method

.method static synthetic e(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    return-object p0
.end method

.method static synthetic f(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->f:Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;

    return-object p0
.end method

.method public static setAppSid(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 428
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/h;->setAppId(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method protected handleClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/mobads/production/b/e;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/b/e;->b(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnClickAd(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/b/e;->d(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnClose(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 411
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/mobads/production/b/e;->a(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnComplete(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 406
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/b/e;->c(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnError(Landroid/content/Context;IILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 1

    .line 402
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/mobads/production/b/e;->a(Landroid/content/Context;IILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method protected handleOnFullScreen(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 420
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/baidu/mobads/production/b/e;->b(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected handleOnStart(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 398
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/b/e;->b(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected isAdAvailable(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/b/e;->a(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z

    move-result p1

    return p1
.end method

.method public makeRequest()V
    .locals 1

    const/4 v0, 0x0

    .line 338
    check-cast v0, Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {p0, v0}, Lcom/baidu/mobad/feeds/BaiduNative;->makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method public makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 3

    if-nez p1, :cond_0

    .line 346
    new-instance p1, Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    invoke-direct {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->build()Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object p1

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->c:Ljava/lang/String;

    iput-object v0, p1, Lcom/baidu/mobad/feeds/RequestParameters;->mPlacementId:Ljava/lang/String;

    .line 352
    new-instance v0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-direct {v0, p0, p1}, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;-><init>(Lcom/baidu/mobad/feeds/BaiduNative;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    iput-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    .line 354
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    const-string v1, "AdStarted"

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/b/e;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 356
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    const-string v1, "AdStartLp"

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/b/e;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 358
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    const-string v1, "AdUserClick"

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/b/e;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 359
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    const-string v1, "AdError"

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/b/e;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 360
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    const-string v1, "vdieoCacheSucc"

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/b/e;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 361
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    const-string v1, "vdieoCacheFailed"

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/b/e;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 362
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    const-string v1, "AdImpression"

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/b/e;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 363
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    const-string v1, "AdStatusChange"

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative;->h:Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/b/e;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 364
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/b/e;->a(Lcom/baidu/mobad/feeds/RequestParameters;)V

    .line 365
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/b/e;->request()V

    return-void
.end method

.method public preloadVideoMaterial()V
    .locals 2

    .line 372
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobad/feeds/NativeResponse;

    .line 374
    check-cast v1, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->preloadVideoMaterial()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected recordImpression(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/production/b/e;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public setCacheVideoOnlyWifi(Z)V
    .locals 1

    .line 332
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative;->d:Lcom/baidu/mobads/production/b/e;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/b/e;->b(Z)V

    :cond_0
    return-void
.end method

.method public setDownloadListener(Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;)V
    .locals 0

    .line 328
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->f:Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;

    return-void
.end method

.method public setNativeEventListener(Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeEventListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 324
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative;->g:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeEventListener;

    return-void
.end method
