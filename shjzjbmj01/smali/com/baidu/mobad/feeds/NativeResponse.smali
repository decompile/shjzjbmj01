.class public interface abstract Lcom/baidu/mobad/feeds/NativeResponse;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;,
        Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;,
        Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;
    }
.end annotation


# static fields
.field public static final INFO_FLOW_GROUP_PIC:I = 0x23

.field public static final INFO_FLOW_GROUP_PIC_LOGO:I = 0x24

.field public static final INFO_FLOW_LEFT_PIC:I = 0x21

.field public static final INFO_FLOW_PIC_BOTTOM_TITLE:I = 0x1c

.field public static final INFO_FLOW_PIC_LOGO:I = 0x1e

.field public static final INFO_FLOW_PIC_TOP_TITLE:I = 0x1d

.field public static final INFO_FLOW_RIGHT_PIC:I = 0x22

.field public static final INFO_FLOW_VIDEO_TOP_TITLE:I = 0x25


# virtual methods
.method public abstract getAdLogoUrl()Ljava/lang/String;
.end method

.method public abstract getAdMaterialType()Ljava/lang/String;
.end method

.method public abstract getAppPackage()Ljava/lang/String;
.end method

.method public abstract getAppPermissionLink()Ljava/lang/String;
.end method

.method public abstract getAppPrivacyLink()Ljava/lang/String;
.end method

.method public abstract getAppSize()J
.end method

.method public abstract getAppVersion()Ljava/lang/String;
.end method

.method public abstract getBaiduLogoUrl()Ljava/lang/String;
.end method

.method public abstract getBrandName()Ljava/lang/String;
.end method

.method public abstract getContainerHeight()I
.end method

.method public abstract getContainerSizeType()I
.end method

.method public abstract getContainerWidth()I
.end method

.method public abstract getDesc()Ljava/lang/String;
.end method

.method public abstract getDownloadStatus()I
.end method

.method public abstract getDuration()I
.end method

.method public abstract getECPMLevel()Ljava/lang/String;
.end method

.method public abstract getExtras()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getHtmlSnippet()Ljava/lang/String;
.end method

.method public abstract getIconUrl()Ljava/lang/String;
.end method

.method public abstract getImageUrl()Ljava/lang/String;
.end method

.method public abstract getMainPicHeight()I
.end method

.method public abstract getMainPicWidth()I
.end method

.method public abstract getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;
.end method

.method public abstract getMultiPicUrls()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPublisher()Ljava/lang/String;
.end method

.method public abstract getStyleType()I
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getVideoUrl()Ljava/lang/String;
.end method

.method public abstract getWebView()Landroid/webkit/WebView;
.end method

.method public abstract handleClick(Landroid/view/View;)V
.end method

.method public abstract handleClick(Landroid/view/View;I)V
.end method

.method public abstract isAdAvailable(Landroid/content/Context;)Z
.end method

.method public abstract isAutoPlay()Z
.end method

.method public abstract isDownloadApp()Z
.end method

.method public abstract isNonWifiAutoPlay()Z
.end method

.method public abstract onClickAd(Landroid/content/Context;)V
.end method

.method public abstract onClose(Landroid/content/Context;I)V
.end method

.method public abstract onComplete(Landroid/content/Context;)V
.end method

.method public abstract onError(Landroid/content/Context;II)V
.end method

.method public abstract onFullScreen(Landroid/content/Context;I)V
.end method

.method public abstract onStart(Landroid/content/Context;)V
.end method

.method public abstract pauseAppDownload()V
.end method

.method public abstract permissionClick()V
.end method

.method public abstract privacyClick()V
.end method

.method public abstract recordImpression(Landroid/view/View;)V
.end method

.method public abstract registerViewForInteraction(Landroid/view/View;Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;)V
.end method

.method public abstract resumeAppDownload()V
.end method

.method public abstract setAdPrivacyListener(Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;)V
.end method

.method public abstract unionLogoClick()V
.end method
