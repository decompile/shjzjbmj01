.class public final Lcom/baidu/mobad/feeds/RequestParameters;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/feeds/RequestParameters$Builder;
    }
.end annotation


# static fields
.field public static final ADS_TYPE_DOWNLOAD:I = 0x2

.field public static final ADS_TYPE_OPENPAGE:I = 0x1

.field public static final DOWNLOAD_APP_CONFIRM_ALWAYS:I = 0x2

.field public static final DOWNLOAD_APP_CONFIRM_CUSTOM_BY_APP:I = 0x4

.field public static final DOWNLOAD_APP_CONFIRM_NEVER:I = 0x3

.field public static final DOWNLOAD_APP_CONFIRM_ONLY_MOBILE:I = 0x1

.field public static final MAX_ASSETS_RESERVED:I = 0xf

.field public static final TAG:Ljava/lang/String; = "RequestParameters"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:I

.field private c:Z

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field protected mPlacementId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)V
    .locals 1

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->e:I

    .line 42
    iput v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->f:I

    .line 134
    invoke-static {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->a(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->a:Ljava/lang/String;

    .line 135
    invoke-static {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->b(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->b:I

    .line 136
    invoke-static {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->c(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->e:I

    .line 137
    invoke-static {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->d(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->f:I

    .line 138
    invoke-static {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->e(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->h:Z

    .line 139
    invoke-static {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->f(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->c:Z

    .line 140
    invoke-static {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->g(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->g:I

    .line 141
    invoke-static {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->h(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobad/feeds/RequestParameters;->setExtras(Ljava/util/Map;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/baidu/mobad/feeds/RequestParameters$Builder;Lcom/baidu/mobad/feeds/RequestParameters$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/baidu/mobad/feeds/RequestParameters;-><init>(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)V

    return-void
.end method


# virtual methods
.method public getAPPConfirmPolicy()I
    .locals 1

    .line 239
    iget v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->g:I

    return v0
.end method

.method public getAdPlacementId()Ljava/lang/String;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->mPlacementId:Ljava/lang/String;

    return-object v0
.end method

.method public getAdsType()I
    .locals 1

    .line 163
    iget v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->b:I

    return v0
.end method

.method public getExtras()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->d:Ljava/util/Map;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .line 154
    iget v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->f:I

    return v0
.end method

.method public final getKeywords()Ljava/lang/String;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .line 150
    iget v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->e:I

    return v0
.end method

.method public isConfirmDownloading()Z
    .locals 1

    .line 172
    iget-boolean v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->c:Z

    return v0
.end method

.method public isCustomSize()Z
    .locals 1

    .line 158
    iget-boolean v0, p0, Lcom/baidu/mobad/feeds/RequestParameters;->h:Z

    return v0
.end method

.method public setAdsType(I)V
    .locals 0

    .line 167
    iput p1, p0, Lcom/baidu/mobad/feeds/RequestParameters;->b:I

    return-void
.end method

.method public setExtras(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 185
    iput-object p1, p0, Lcom/baidu/mobad/feeds/RequestParameters;->d:Ljava/util/Map;

    return-void
.end method

.method public toHashMap()Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 214
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "mKeywords"

    .line 215
    iget-object v2, p0, Lcom/baidu/mobad/feeds/RequestParameters;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "adsType"

    .line 216
    iget v2, p0, Lcom/baidu/mobad/feeds/RequestParameters;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "confirmDownloading"

    .line 217
    iget-boolean v2, p0, Lcom/baidu/mobad/feeds/RequestParameters;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 219
    iget-object v2, p0, Lcom/baidu/mobad/feeds/RequestParameters;->d:Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 221
    iget-object v2, p0, Lcom/baidu/mobad/feeds/RequestParameters;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 222
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 224
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 225
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v2, "extras"

    .line 228
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method
