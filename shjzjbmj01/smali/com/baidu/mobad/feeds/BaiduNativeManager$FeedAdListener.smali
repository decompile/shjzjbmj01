.class public interface abstract Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobad/feeds/BaiduNativeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FeedAdListener"
.end annotation


# virtual methods
.method public abstract onLpClosed()V
.end method

.method public abstract onNativeFail(Lcom/baidu/mobad/feeds/NativeErrorCode;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onNativeLoad(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/baidu/mobad/feeds/NativeResponse;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onVideoDownloadFailed()V
.end method

.method public abstract onVideoDownloadSuccess()V
.end method
