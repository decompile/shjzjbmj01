.class public final enum Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobad/feeds/NativeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MaterialType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum HTML:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

.field public static final enum NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

.field public static final enum VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

.field private static final synthetic b:[Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 29
    new-instance v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    const-string v1, "NORMAL"

    const-string v2, "normal"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    new-instance v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    const-string v1, "VIDEO"

    const-string v2, "video"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    new-instance v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    const-string v1, "HTML"

    const-string v2, "html"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->HTML:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    const/4 v0, 0x3

    .line 28
    new-array v0, v0, [Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    sget-object v1, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->HTML:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->b:[Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->a:Ljava/lang/String;

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;
    .locals 5

    .line 42
    invoke-static {}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->values()[Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 43
    iget-object v4, v3, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;
    .locals 1

    .line 28
    const-class v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;
    .locals 1

    .line 28
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->b:[Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    invoke-virtual {v0}, [Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->a:Ljava/lang/String;

    return-object v0
.end method
