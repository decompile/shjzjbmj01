.class public Lcom/baidu/mobad/feeds/BaiduNativeManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/feeds/BaiduNativeManager$PortraitVideoAdListener;,
        Lcom/baidu/mobad/feeds/BaiduNativeManager$NativeLoadListener;,
        Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "BaiduNativeManager"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private d:Z

.field private e:I

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x1f40

    .line 35
    invoke-direct {p0, p1, p2, v0}, Lcom/baidu/mobad/feeds/BaiduNativeManager;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x1

    .line 44
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/baidu/mobad/feeds/BaiduNativeManager;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    const/16 v0, 0x1f40

    .line 53
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobad/feeds/BaiduNativeManager;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZI)V
    .locals 2

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 25
    iput-boolean v0, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->d:Z

    const/16 v0, 0x1f40

    .line 26
    iput v0, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->e:I

    const/4 v0, 0x0

    .line 28
    iput-boolean v0, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->f:Z

    .line 63
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->b:Landroid/content/Context;

    .line 64
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 65
    iput-object p2, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->c:Ljava/lang/String;

    .line 66
    iput-boolean p3, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->d:Z

    .line 67
    iput p4, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->e:I

    .line 68
    invoke-static {p1}, Lcom/baidu/mobads/g/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/g/q;->a()V

    return-void
.end method


# virtual methods
.method public loadContentAd(Lcom/baidu/mobad/feeds/RequestParameters;Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;)V
    .locals 10

    .line 90
    new-instance v0, Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->c:Ljava/lang/String;

    new-instance v3, Lcom/baidu/mobads/production/b/a;

    invoke-direct {v3, p2}, Lcom/baidu/mobads/production/b/a;-><init>(Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;)V

    new-instance p2, Lcom/baidu/mobads/production/b/e;

    iget-object v5, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->b:Landroid/content/Context;

    iget-object v6, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->c:Ljava/lang/String;

    sget-object v7, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CONTENT:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iget-boolean v8, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->d:Z

    iget v9, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->e:I

    move-object v4, p2

    invoke-direct/range {v4 .. v9}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;ZI)V

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/b/e;)V

    .line 93
    iget-boolean p2, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->f:Z

    invoke-virtual {v0, p2}, Lcom/baidu/mobad/feeds/BaiduNative;->setCacheVideoOnlyWifi(Z)V

    .line 94
    new-instance p2, Lcom/baidu/mobads/production/b/b;

    invoke-direct {p2}, Lcom/baidu/mobads/production/b/b;-><init>()V

    invoke-virtual {v0, p2}, Lcom/baidu/mobad/feeds/BaiduNative;->setDownloadListener(Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;)V

    .line 95
    invoke-virtual {v0, p1}, Lcom/baidu/mobad/feeds/BaiduNative;->makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method public loadFeedAd(Lcom/baidu/mobad/feeds/RequestParameters;Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;)V
    .locals 7

    .line 81
    new-instance v6, Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->c:Ljava/lang/String;

    new-instance v3, Lcom/baidu/mobads/production/b/a;

    invoke-direct {v3, p2}, Lcom/baidu/mobads/production/b/a;-><init>(Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;)V

    iget-boolean v4, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->d:Z

    iget v5, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->e:I

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;ZI)V

    .line 83
    iget-boolean p2, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->f:Z

    invoke-virtual {v6, p2}, Lcom/baidu/mobad/feeds/BaiduNative;->setCacheVideoOnlyWifi(Z)V

    .line 84
    new-instance p2, Lcom/baidu/mobads/production/b/b;

    invoke-direct {p2}, Lcom/baidu/mobads/production/b/b;-><init>()V

    invoke-virtual {v6, p2}, Lcom/baidu/mobad/feeds/BaiduNative;->setDownloadListener(Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;)V

    .line 85
    invoke-virtual {v6, p1}, Lcom/baidu/mobad/feeds/BaiduNative;->makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method public loadPortraitVideoAd(Lcom/baidu/mobad/feeds/RequestParameters;Lcom/baidu/mobad/feeds/BaiduNativeManager$PortraitVideoAdListener;)V
    .locals 7

    .line 101
    new-instance v6, Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->c:Ljava/lang/String;

    sget-object v3, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PORTRAIT_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iget-boolean v4, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->d:Z

    iget v5, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->e:I

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;ZI)V

    .line 104
    new-instance v0, Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->c:Ljava/lang/String;

    new-instance v3, Lcom/baidu/mobads/production/b/a;

    invoke-direct {v3, p2}, Lcom/baidu/mobads/production/b/a;-><init>(Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;)V

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/b/e;)V

    .line 106
    iget-boolean p2, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->f:Z

    invoke-virtual {v0, p2}, Lcom/baidu/mobad/feeds/BaiduNative;->setCacheVideoOnlyWifi(Z)V

    .line 107
    new-instance p2, Lcom/baidu/mobads/production/b/b;

    invoke-direct {p2}, Lcom/baidu/mobads/production/b/b;-><init>()V

    invoke-virtual {v0, p2}, Lcom/baidu/mobad/feeds/BaiduNative;->setDownloadListener(Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;)V

    .line 108
    invoke-virtual {v0, p1}, Lcom/baidu/mobad/feeds/BaiduNative;->makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method public setCacheVideoOnlyWifi(Z)V
    .locals 0

    .line 76
    iput-boolean p1, p0, Lcom/baidu/mobad/feeds/BaiduNativeManager;->f:Z

    return-void
.end method
