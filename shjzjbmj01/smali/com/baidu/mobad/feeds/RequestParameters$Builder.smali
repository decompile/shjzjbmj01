.class public Lcom/baidu/mobad/feeds/RequestParameters$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobad/feeds/RequestParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->b:Ljava/util/Map;

    const/4 v0, 0x3

    .line 56
    iput v0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->c:I

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->d:Z

    const/16 v1, 0x280

    .line 58
    iput v1, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->e:I

    const/16 v1, 0x1e0

    .line 59
    iput v1, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->f:I

    const/4 v1, 0x1

    .line 60
    iput v1, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->g:I

    .line 61
    iput-boolean v0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->h:Z

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)Ljava/lang/String;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)I
    .locals 0

    .line 48
    iget p0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->c:I

    return p0
.end method

.method static synthetic c(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)I
    .locals 0

    .line 48
    iget p0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->e:I

    return p0
.end method

.method static synthetic d(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)I
    .locals 0

    .line 48
    iget p0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->f:I

    return p0
.end method

.method static synthetic e(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)Z
    .locals 0

    .line 48
    iget-boolean p0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->h:Z

    return p0
.end method

.method static synthetic f(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)Z
    .locals 0

    .line 48
    iget-boolean p0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->d:Z

    return p0
.end method

.method static synthetic g(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)I
    .locals 0

    .line 48
    iget p0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->g:I

    return p0
.end method

.method static synthetic h(Lcom/baidu/mobad/feeds/RequestParameters$Builder;)Ljava/util/Map;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->b:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public final addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobad/feeds/RequestParameters$Builder;
    .locals 3

    const-string v0, "page_title"

    .line 115
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->b:Ljava/util/Map;

    const-string v1, "mpt"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final build()Lcom/baidu/mobad/feeds/RequestParameters;
    .locals 2

    .line 128
    new-instance v0, Lcom/baidu/mobad/feeds/RequestParameters;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobad/feeds/RequestParameters;-><init>(Lcom/baidu/mobad/feeds/RequestParameters$Builder;Lcom/baidu/mobad/feeds/RequestParameters$1;)V

    return-object v0
.end method

.method public final confirmDownloading(Z)Lcom/baidu/mobad/feeds/RequestParameters$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    .line 85
    invoke-virtual {p0, p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->downloadAppConfirmPolicy(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    goto :goto_0

    :cond_0
    const/4 p1, 0x3

    .line 87
    invoke-virtual {p0, p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->downloadAppConfirmPolicy(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    :goto_0
    return-object p0
.end method

.method public final downloadAppConfirmPolicy(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;
    .locals 0

    .line 102
    iput p1, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->g:I

    return-object p0
.end method

.method public final setHeight(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;
    .locals 0

    .line 70
    iput p1, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->f:I

    const/4 p1, 0x1

    .line 71
    iput-boolean p1, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->h:Z

    return-object p0
.end method

.method public final setWidth(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;
    .locals 0

    .line 64
    iput p1, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->e:I

    const/4 p1, 0x1

    .line 65
    iput-boolean p1, p0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->h:Z

    return-object p0
.end method
