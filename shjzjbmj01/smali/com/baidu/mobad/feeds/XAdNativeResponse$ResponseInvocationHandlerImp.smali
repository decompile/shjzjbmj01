.class Lcom/baidu/mobad/feeds/XAdNativeResponse$ResponseInvocationHandlerImp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobad/feeds/XAdNativeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ResponseInvocationHandlerImp"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobad/feeds/XAdNativeResponse;


# direct methods
.method constructor <init>(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V
    .locals 0

    .line 702
    iput-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse$ResponseInvocationHandlerImp;->a:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 706
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object p1

    .line 707
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 p3, 0x0

    if-eqz p2, :cond_0

    return-object p3

    :cond_0
    const-string p2, "onShow"

    .line 710
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 711
    iget-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse$ResponseInvocationHandlerImp;->a:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 712
    iget-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse$ResponseInvocationHandlerImp;->a:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;->onADPermissionShow()V

    goto :goto_0

    :cond_1
    const-string p2, "onDismiss"

    .line 714
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 715
    iget-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse$ResponseInvocationHandlerImp;->a:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 716
    iget-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse$ResponseInvocationHandlerImp;->a:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;->onADPermissionClose()V

    :cond_2
    :goto_0
    return-object p3
.end method
