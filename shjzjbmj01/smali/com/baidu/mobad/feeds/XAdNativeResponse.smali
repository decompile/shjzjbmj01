.class public Lcom/baidu/mobad/feeds/XAdNativeResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobad/feeds/NativeResponse;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/feeds/XAdNativeResponse$ResponseInvocationHandlerImp;
    }
.end annotation


# instance fields
.field private a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

.field private b:Lcom/baidu/mobads/production/b/e;

.field private c:Z

.field private d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

.field private e:Lcom/baidu/mobads/interfaces/IXAdContainer;

.field private f:Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

.field private g:Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

.field private h:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/production/b/e;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Lcom/baidu/mobads/interfaces/IXAdContainer;)V
    .locals 1

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 55
    iput-boolean v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    .line 64
    iput-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 65
    iput-object p2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    .line 66
    iput-object p4, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    .line 67
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object p1

    .line 68
    iget-object p2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result p2

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result p1

    if-ne p2, p1, :cond_0

    const/4 p1, 0x1

    .line 69
    iput-boolean p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    .line 71
    :cond_0
    iput-object p3, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    .line 72
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->h:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .line 441
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    .line 442
    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 444
    iget-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setActionOnlyWifi(Z)V

    goto :goto_0

    .line 447
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setActionOnlyWifi(Z)V

    :goto_0
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 2

    .line 690
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "msg"

    .line 691
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "prod"

    const-string v1, "feed"

    .line 692
    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "downType"

    .line 693
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "dl_type"

    const-string p3, "ac_feed"

    .line 694
    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    iget-object p2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    if-eqz p2, :cond_0

    const-string p2, "apid"

    .line 696
    iget-object p3, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {p3}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAdPlacementId()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "confirmPolicy"

    .line 697
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAPPConfirmPolicy()I

    move-result v1

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 699
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object p2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const/16 p3, 0x416

    invoke-virtual {p2, p1, p3, p4, v0}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/util/HashMap;)V

    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 4

    .line 454
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 456
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p1

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-class v1, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x1

    const-string v1, "showConfirmDialog context is null"

    aput-object v1, p2, v0

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/utils/q;->e([Ljava/lang/Object;)I

    return-void

    .line 460
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "\u5f53\u524d\u662f\u79fb\u52a8\u7f51\u7edc,\u662f\u5426\u7ee7\u7eed\u4e0b\u8f7d?"

    .line 461
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v2, "\u6e29\u99a8\u63d0\u793a"

    .line 462
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v2, "\u786e\u8ba4"

    .line 463
    new-instance v3, Lcom/baidu/mobad/feeds/XAdNativeResponse$1;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/baidu/mobad/feeds/XAdNativeResponse$1;-><init>(Lcom/baidu/mobad/feeds/XAdNativeResponse;Landroid/content/Context;Landroid/view/View;I)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string p1, "\u53d6\u6d88"

    .line 472
    new-instance p2, Lcom/baidu/mobad/feeds/XAdNativeResponse$2;

    invoke-direct {p2, p0, v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse$2;-><init>(Lcom/baidu/mobad/feeds/XAdNativeResponse;Landroid/content/Context;)V

    invoke-virtual {v1, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 479
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 483
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception p1

    .line 481
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private a(Landroid/view/View;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 11

    .line 366
    invoke-virtual {p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->isDownloadApp()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 367
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 372
    :try_start_0
    invoke-interface {p3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "notice_dl_non_wifi"

    invoke-virtual {v1, v2, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v8, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move v10, v1

    goto :goto_1

    :catch_0
    const/4 v10, 0x0

    .line 381
    :goto_1
    :try_start_1
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    move-result-object v1

    .line 382
    invoke-interface {p3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/baidu/mobads/utils/r;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 385
    invoke-interface {p3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v3, "app_store_link"

    .line 387
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 388
    invoke-interface {p3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x16e

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, v0

    invoke-virtual/range {v1 .. v7}, Lcom/baidu/mobads/utils/r;->sendAPOInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;III)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    const/4 v10, 0x0

    goto :goto_2

    :catch_1
    :cond_2
    const/4 v1, 0x0

    .line 401
    :goto_2
    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAPPConfirmPolicy()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 404
    invoke-interface {p3, v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setActionOnlyWifi(Z)V

    .line 405
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p3, p2, v1}, Lcom/baidu/mobads/production/b/e;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    goto/16 :goto_3

    .line 406
    :cond_3
    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAPPConfirmPolicy()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 410
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0, v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setActionOnlyWifi(Z)V

    .line 411
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p3, p2, v1}, Lcom/baidu/mobads/production/b/e;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    goto :goto_3

    .line 412
    :cond_4
    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAPPConfirmPolicy()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    if-eqz v1, :cond_5

    .line 416
    iget-object p3, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {p3, v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setActionOnlyWifi(Z)V

    .line 417
    iget-object p3, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {p3, p1, v0, p2, v1}, Lcom/baidu/mobads/production/b/e;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    goto :goto_3

    .line 419
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/view/View;I)V

    goto :goto_3

    .line 421
    :cond_6
    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAPPConfirmPolicy()I

    move-result v2

    if-ne v2, v8, :cond_9

    .line 423
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v2

    .line 425
    invoke-interface {v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    if-eqz v10, :cond_7

    if-nez v1, :cond_7

    .line 427
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/view/View;I)V

    goto :goto_3

    .line 430
    :cond_7
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0, v9}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setActionOnlyWifi(Z)V

    .line 431
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p3, p2, v1}, Lcom/baidu/mobads/production/b/e;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    goto :goto_3

    .line 436
    :cond_8
    iget-object p3, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {p3, p1, v0, p2, v1}, Lcom/baidu/mobads/production/b/e;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    :cond_9
    :goto_3
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/XAdNativeResponse;Landroid/content/Context;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/feeds/XAdNativeResponse;Landroid/content/Context;Ljava/lang/String;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/content/Context;Ljava/lang/String;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .line 281
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 284
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/baidu/mobads/AppActivity;->getActivityClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "privacy_link"

    .line 285
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    if-eqz p2, :cond_1

    .line 289
    instance-of p1, p2, Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    if-eqz p1, :cond_0

    .line 290
    check-cast p2, Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;->onAdUnionClick()V

    goto :goto_0

    .line 291
    :cond_0
    instance-of p1, p2, Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    if-eqz p1, :cond_1

    .line 292
    check-cast p2, Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;->onADPrivacyClick()V

    .line 296
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    new-instance p2, Lcom/baidu/mobads/openad/a/b;

    const-string v0, "AdStartLp"

    invoke-direct {p2, v0}, Lcom/baidu/mobads/openad/a/b;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b/e;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobads/production/b/e;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    return-object p0
.end method

.method static synthetic d(Lcom/baidu/mobad/feeds/XAdNativeResponse;)Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->g:Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    return-object p0
.end method


# virtual methods
.method public getAdLogInfo()Lcom/baidu/mobads/component/AdLogInfo;
    .locals 2

    .line 678
    new-instance v0, Lcom/baidu/mobads/component/AdLogInfo;

    invoke-direct {v0}, Lcom/baidu/mobads/component/AdLogInfo;-><init>()V

    .line 679
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    if-eqz v1, :cond_0

    .line 680
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;->getAdPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/AdLogInfo;->setAdPlaceId(Ljava/lang/String;)V

    .line 682
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    if-eqz v1, :cond_1

    .line 683
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getQueryKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/AdLogInfo;->setQk(Ljava/lang/String;)V

    .line 684
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/AdLogInfo;->setVideoUrl(Ljava/lang/String;)V

    :cond_1
    return-object v0
.end method

.method public getAdLogoUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "https://cpro.baidustatic.com/cpro/logo/sdk/mob-adIcon_2x.png"

    return-object v0
.end method

.method public getAdMaterialType()Ljava/lang/String;
    .locals 2

    .line 547
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v0, v1, :cond_0

    .line 548
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 549
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v0, v1, :cond_1

    .line 550
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->HTML:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 552
    :cond_1
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppPackage()Ljava/lang/String;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppPermissionLink()Ljava/lang/String;
    .locals 3

    .line 342
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "permission_link"

    const-string v2, ""

    .line 346
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getAppPrivacyLink()Ljava/lang/String;
    .locals 3

    .line 330
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "privacy_link"

    const-string v2, ""

    .line 334
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getAppSize()J
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 3

    .line 354
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "app_version"

    const-string v2, ""

    .line 358
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getBaiduLogoUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "https://cpro.baidustatic.com/cpro/logo/sdk/new-bg-logo.png"

    return-object v0
.end method

.method public getBrandName()Ljava/lang/String;
    .locals 1

    .line 615
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContainerHeight()I
    .locals 1

    .line 568
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdContainerHeight()I

    move-result v0

    return v0
.end method

.method public getContainerSizeType()I
    .locals 1

    .line 573
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdContainerSizeType()I

    move-result v0

    return v0
.end method

.method public getContainerWidth()I
    .locals 1

    .line 563
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdContainerWidth()I

    move-result v0

    return v0
.end method

.method public getDesc()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDownloadStatus()I
    .locals 3

    .line 150
    iget-boolean v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/utils/c;->a(Landroid/content/Context;)Lcom/baidu/mobads/utils/c;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    .line 153
    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v1

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 154
    invoke-virtual {p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getAppPackage()Ljava/lang/String;

    move-result-object v2

    .line 153
    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/utils/c;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public getDuration()I
    .locals 1

    .line 520
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoDuration()I

    move-result v0

    return v0
.end method

.method public getECPMLevel()Ljava/lang/String;
    .locals 3

    .line 578
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "bidlayer"

    const-string v2, ""

    .line 582
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getExtras()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getHtmlSnippet()Ljava/lang/String;
    .locals 1

    .line 536
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getHtmlSnippet()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, ""

    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMainPicHeight()I
    .locals 1

    .line 610
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainMaterialHeight()I

    move-result v0

    return v0
.end method

.method public getMainPicWidth()I
    .locals 1

    .line 605
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainMaterialWidth()I

    move-result v0

    return v0
.end method

.method public getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;
    .locals 2

    .line 525
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v0, v1, :cond_0

    .line 526
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    return-object v0

    .line 527
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v0, v1, :cond_1

    .line 528
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->HTML:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    return-object v0

    .line 530
    :cond_1
    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    return-object v0
.end method

.method public getMultiPicUrls()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 194
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "morepics"

    .line 195
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 196
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 197
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x0

    .line 198
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 199
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    :cond_0
    move-object v0, v2

    :catch_1
    :cond_1
    return-object v0
.end method

.method public getMute()Ljava/lang/String;
    .locals 1

    .line 623
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMute()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 3

    .line 318
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "publisher"

    const-string v2, ""

    .line 322
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getStyleType()I
    .locals 1

    .line 558
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getFeedAdStyleType()I

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueId()Ljava/lang/String;
    .locals 1

    .line 631
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getUniqueId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoUrl()Ljava/lang/String;
    .locals 1

    .line 515
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebView()Landroid/webkit/WebView;
    .locals 1

    .line 541
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    return-object v0
.end method

.method public handleClick(Landroid/view/View;)V
    .locals 1

    const/4 v0, -0x1

    .line 307
    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->handleClick(Landroid/view/View;I)V

    return-void
.end method

.method public handleClick(Landroid/view/View;I)V
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-direct {p0, p1, p2, v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/view/View;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method protected handleClickDownloadDirect(Landroid/view/View;)V
    .locals 2

    .line 662
    invoke-virtual {p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->supportDownloadDirect()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 666
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    check-cast v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    const-string v1, ""

    .line 667
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setAction(Ljava/lang/String;)V

    const/4 v1, -0x1

    .line 668
    invoke-direct {p0, p1, v1, v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Landroid/view/View;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public isAdAvailable(Landroid/content/Context;)Z
    .locals 3

    .line 162
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobads/production/b/e;->a(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z

    move-result p1

    return p1
.end method

.method public isAutoPlay()Z
    .locals 3

    .line 172
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "auto_play"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isDownloadApp()Z
    .locals 1

    .line 113
    iget-boolean v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    return v0
.end method

.method public isNonWifiAutoPlay()Z
    .locals 3

    const/4 v0, 0x1

    .line 178
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "auto_play_non_wifi"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    return v0
.end method

.method public isVideoMuted()Z
    .locals 1

    .line 619
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->isVideoMuted()Z

    move-result v0

    return v0
.end method

.method public onADExposed()V
    .locals 1

    .line 641
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->f:Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->f:Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;->onADExposed()V

    :cond_0
    return-void
.end method

.method public onADStatusChanged()V
    .locals 1

    .line 647
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->f:Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    if-eqz v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->f:Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;->onADStatusChanged()V

    :cond_0
    return-void
.end method

.method public onAdClick()V
    .locals 1

    .line 635
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->f:Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    if-eqz v0, :cond_0

    .line 636
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->f:Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;->onAdClick()V

    :cond_0
    return-void
.end method

.method public onClickAd(Landroid/content/Context;)V
    .locals 3

    .line 600
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobads/production/b/e;->d(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public onClose(Landroid/content/Context;I)V
    .locals 3

    .line 505
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/baidu/mobads/production/b/e;->a(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public onComplete(Landroid/content/Context;)V
    .locals 3

    .line 500
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobads/production/b/e;->c(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public onError(Landroid/content/Context;II)V
    .locals 2

    .line 495
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/baidu/mobads/production/b/e;->a(Landroid/content/Context;IILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method public onFullScreen(Landroid/content/Context;I)V
    .locals 3

    .line 510
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/baidu/mobads/production/b/e;->b(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public onStart(Landroid/content/Context;)V
    .locals 3

    .line 490
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobads/production/b/e;->b(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public pauseAppDownload()V
    .locals 2

    .line 122
    iget-boolean v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/utils/c;->a(Landroid/content/Context;)Lcom/baidu/mobads/utils/c;

    move-result-object v0

    .line 125
    invoke-virtual {p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getAppPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/c;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public permissionClick()V
    .locals 12

    .line 216
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v0

    .line 218
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v1

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "com.style.widget.VerifyPerDialog"

    .line 220
    invoke-static {}, Lcom/baidu/mobads/g/b;->d()Ldalvik/system/DexClassLoader;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v8, 0x1

    if-nez v4, :cond_0

    .line 223
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 225
    :try_start_1
    invoke-static {v1}, Lcom/baidu/mobads/g/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 226
    invoke-virtual {v1}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    .line 227
    new-instance v9, Ldalvik/system/DexClassLoader;

    invoke-direct {v9, v6, v7, v3, v5}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 228
    invoke-static {v2, v8, v9}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    .line 230
    :try_start_2
    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    move-object v5, v3

    goto :goto_0

    .line 233
    :cond_0
    invoke-static {v2, v8, v4}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v5

    :goto_0
    move-object v9, v5

    const/4 v10, 0x0

    if-eqz v9, :cond_1

    const-string v5, "createVerPerDialog"

    const/4 v6, 0x2

    .line 237
    new-array v7, v6, [Ljava/lang/Class;

    const-class v11, Landroid/content/Context;

    aput-object v11, v7, v10

    const-class v11, Ljava/lang/String;

    aput-object v11, v7, v8

    invoke-virtual {v9, v5, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 238
    invoke-virtual {v5, v8}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 239
    iget-object v7, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v7

    const-string v11, "permission_link"

    .line 240
    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 241
    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v10

    aput-object v7, v6, v8

    invoke-virtual {v5, v3, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, v3

    :goto_1
    const-string v3, "com.style.widget.OnDialogListener"

    .line 245
    invoke-static {v3, v4}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v3

    .line 246
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Class;

    aput-object v3, v6, v10

    new-instance v7, Lcom/baidu/mobad/feeds/XAdNativeResponse$ResponseInvocationHandlerImp;

    invoke-direct {v7, p0}, Lcom/baidu/mobad/feeds/XAdNativeResponse$ResponseInvocationHandlerImp;-><init>(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V

    invoke-static {v5, v6, v7}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v5

    .line 248
    iget-object v6, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->g:Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    if-eqz v6, :cond_2

    if-eqz v1, :cond_2

    const-string v6, "setOnDialogListener"

    .line 249
    new-array v7, v8, [Ljava/lang/Class;

    aput-object v3, v7, v10

    new-array v11, v8, [Ljava/lang/Object;

    aput-object v5, v11, v10

    move-object v3, v1

    move-object v5, v6

    move-object v6, v7

    move-object v7, v11

    invoke-static/range {v2 .. v7}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    if-eqz v1, :cond_3

    if-eqz v9, :cond_3

    const-string v2, "show"

    .line 255
    new-array v3, v10, [Ljava/lang/Class;

    invoke-virtual {v9, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 256
    invoke-virtual {v2, v8}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 257
    new-array v3, v10, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    .line 261
    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    :cond_3
    :goto_2
    return-void
.end method

.method public preloadVideoMaterial()V
    .locals 2

    .line 627
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/b/e;->d(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method public privacyClick()V
    .locals 2

    .line 267
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "privacy_link"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 268
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 269
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->g:Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public recordImpression(Landroid/view/View;)V
    .locals 3

    .line 302
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->b:Lcom/baidu/mobads/production/b/e;

    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->d:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    invoke-virtual {v0, p1, v1, v2}, Lcom/baidu/mobads/production/b/e;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method public registerViewForInteraction(Landroid/view/View;Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;)V
    .locals 0

    .line 589
    invoke-virtual {p0, p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->recordImpression(Landroid/view/View;)V

    .line 590
    iput-object p2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->f:Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    return-void
.end method

.method public resumeAppDownload()V
    .locals 6

    .line 132
    iget-boolean v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, ""

    .line 136
    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v2

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 137
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->e:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v1

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v1

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v1

    .line 139
    :cond_0
    invoke-static {v0}, Lcom/baidu/mobads/utils/c;->a(Landroid/content/Context;)Lcom/baidu/mobads/utils/c;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ac_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 140
    invoke-virtual {v2, v0, v3, v1, v4}, Lcom/baidu/mobads/utils/c;->a(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_1
    return-void
.end method

.method public setAdPrivacyListener(Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;)V
    .locals 0

    .line 595
    iput-object p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->g:Lcom/baidu/mobad/feeds/NativeResponse$AdPrivacyListener;

    return-void
.end method

.method public setIsDownloadApp(Z)V
    .locals 0

    .line 117
    iput-boolean p1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->c:Z

    return-void
.end method

.method protected supportDownloadDirect()Z
    .locals 4

    .line 653
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v0

    .line 654
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v1

    .line 655
    iget-object v2, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "video"

    .line 656
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 657
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/l;->getActTypeDownload()I

    move-result v2

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public unionLogoClick()V
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->h:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    const-string v1, "http://union.baidu.com/"

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->replaceURLWithSupportProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 276
    iget-object v1, p0, Lcom/baidu/mobad/feeds/XAdNativeResponse;->f:Lcom/baidu/mobad/feeds/NativeResponse$AdInteractionListener;

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
