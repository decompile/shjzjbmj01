.class public Lcom/baidu/mobad/feeds/ArticleInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/feeds/ArticleInfo$ValidSexValue;
    }
.end annotation


# static fields
.field public static final CONTENT_CATEGORY:Ljava/lang/String; = "page_content_category"

.field public static final CONTENT_LABEL:Ljava/lang/String; = "page_content_label"

.field public static final FAVORITE_BOOK:Ljava/lang/String; = "fav_book"

.field public static final PAGE_ID:Ljava/lang/String; = "page_content_id"

.field public static final PAGE_TITLE:Ljava/lang/String; = "page_title"

.field public static final PREDEFINED_KEYS:[Ljava/lang/String;

.field public static final QUERY_WORD:Ljava/lang/String; = "qw"

.field public static final USER_SEX:Ljava/lang/String; = "sex"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x7

    .line 13
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sex"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "fav_book"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "page_title"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "page_content_id"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "page_content_category"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "page_content_label"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "qw"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/baidu/mobad/feeds/ArticleInfo;->PREDEFINED_KEYS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
