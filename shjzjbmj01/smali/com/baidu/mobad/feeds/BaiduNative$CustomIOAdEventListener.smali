.class Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobad/feeds/BaiduNative;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CustomIOAdEventListener"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobad/feeds/BaiduNative;

.field private b:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;


# direct methods
.method public constructor <init>(Lcom/baidu/mobad/feeds/BaiduNative;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p2, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->b:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    return-void
.end method


# virtual methods
.method public run(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 11

    const-string v0, "AdStarted"

    .line 55
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_6

    .line 58
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    if-eqz p1, :cond_13

    .line 60
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 62
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object v3

    const/4 v4, 0x0

    .line 64
    :goto_0
    iget-object v5, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v5}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/b/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/production/b/e;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 65
    iget-object v5, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v5}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/b/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/production/b/e;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 66
    invoke-interface {v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v6

    .line 70
    invoke-interface {v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v7

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result v8

    if-ne v7, v8, :cond_2

    if-eqz v6, :cond_1

    const-string v7, ""

    .line 71
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "null"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v0, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_1

    .line 74
    :cond_0
    invoke-virtual {v0, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 75
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    move-result-object v7

    iget-object v8, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 76
    invoke-static {v8}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Lcom/baidu/mobads/utils/r;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v6, 0x1

    goto :goto_3

    .line 78
    :cond_2
    invoke-interface {v5}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v7

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeOpenExternalApp()I

    move-result v8

    if-ne v7, v8, :cond_3

    .line 79
    iget-object v7, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v7, v5}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)I

    move-result v7

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result v8

    if-ne v7, v8, :cond_3

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 80
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    move-result-object v7

    iget-object v8, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 81
    invoke-static {v8}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Lcom/baidu/mobads/utils/r;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    :goto_2
    xor-int/2addr v6, v2

    move v7, v6

    const/4 v6, 0x0

    goto :goto_4

    :cond_3
    const/4 v6, 0x0

    :goto_3
    const/4 v7, 0x0

    :goto_4
    if-nez v6, :cond_4

    .line 85
    new-instance v6, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    iget-object v8, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 86
    invoke-static {v8}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/b/e;

    move-result-object v8

    iget-object v9, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->b:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    iget-object v10, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 87
    invoke-static {v10}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/b/e;

    move-result-object v10

    invoke-virtual {v10}, Lcom/baidu/mobads/production/b/e;->getCurrentXAdContainer()Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object v10

    invoke-direct {v6, v5, v8, v9, v10}, Lcom/baidu/mobad/feeds/XAdNativeResponse;-><init>(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/production/b/e;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Lcom/baidu/mobads/interfaces/IXAdContainer;)V

    .line 88
    invoke-virtual {v6, v7}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->setIsDownloadApp(Z)V

    .line 89
    invoke-interface {p1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 93
    :cond_5
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0, p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;Ljava/util/List;)Ljava/util/List;

    .line 94
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$1;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$1;-><init>(Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_8

    :cond_6
    const-string v0, "AdError"

    .line 102
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 103
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/b/e;->removeAllListeners()V

    .line 104
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v0

    const-string v1, "error_message"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 105
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    const-string v1, "error_code"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 107
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    new-instance v2, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$2;

    invoke-direct {v2, p0}, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$2;-><init>(Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;)V

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/Runnable;)V

    .line 116
    :cond_7
    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object v1

    instance-of v1, v1, Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeLoadAdListener;

    if-eqz v1, :cond_13

    .line 117
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    new-instance v2, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$3;

    invoke-direct {v2, p0, v0, p1}, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener$3;-><init>(Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_8

    :cond_8
    const-string v0, "AdUserClick"

    .line 124
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 125
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    if-eqz v0, :cond_a

    .line 126
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_a

    const-string v0, "instanceInfo"

    .line 128
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 129
    :goto_5
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->d(Lcom/baidu/mobad/feeds/BaiduNative;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 130
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->d(Lcom/baidu/mobad/feeds/BaiduNative;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    .line 131
    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 132
    iget-object v3, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v3}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object v3

    check-cast v3, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    invoke-interface {v3, v0}, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;->onAdClick(Lcom/baidu/mobad/feeds/NativeResponse;)V

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 137
    :cond_a
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    instance-of p1, p1, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    if-eqz p1, :cond_13

    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 138
    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->b(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobads/production/b/e;

    move-result-object p1

    iget-object p1, p1, Lcom/baidu/mobads/production/b/e;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result p1

    if-ne p1, v2, :cond_13

    .line 139
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/command/b/a;->b()V

    .line 140
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object p1

    const-string v0, "AdLpClosed"

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 141
    invoke-static {v1}, Lcom/baidu/mobad/feeds/BaiduNative;->e(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/command/b/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    goto/16 :goto_8

    :cond_b
    const-string v0, "AdStartLp"

    .line 143
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 144
    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    if-eqz v0, :cond_c

    .line 145
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/command/b/a;->b()V

    .line 146
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object p1

    const-string v0, "AdLpClosed"

    iget-object v1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 147
    invoke-static {v1}, Lcom/baidu/mobad/feeds/BaiduNative;->e(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/command/b/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    goto/16 :goto_8

    :cond_c
    const-string v0, "AdLpClosed"

    .line 148
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    .line 149
    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    if-eqz v0, :cond_d

    .line 150
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object p1

    const-string v0, "AdLpClosed"

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/command/b/a;->removeEventListeners(Ljava/lang/String;)V

    .line 151
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->c(Lcom/baidu/mobad/feeds/BaiduNative;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/command/b/a;->c()V

    .line 152
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/BaiduNative$FeedLpCloseListener;->onLpClosed()V

    goto/16 :goto_8

    :cond_d
    const-string v0, "vdieoCacheSucc"

    .line 153
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 154
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    instance-of p1, p1, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;

    if-eqz p1, :cond_13

    .line 155
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;->onVideoDownloadSuccess()V

    goto/16 :goto_8

    :cond_e
    const-string v0, "vdieoCacheFailed"

    .line 157
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 158
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    instance-of p1, p1, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;

    if-eqz p1, :cond_13

    .line 159
    iget-object p1, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/BaiduNative$VideoCacheListener;->onVideoDownloadFailed()V

    goto/16 :goto_8

    :cond_f
    const-string v0, "AdImpression"

    .line 161
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 162
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobad/feeds/BaiduNative$NativeADEventListener;

    if-eqz v0, :cond_13

    .line 163
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_13

    const-string v0, "instanceInfo"

    .line 165
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 166
    :goto_6
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->d(Lcom/baidu/mobad/feeds/BaiduNative;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_13

    .line 167
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->d(Lcom/baidu/mobad/feeds/BaiduNative;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    .line 168
    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 169
    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v2}, Lcom/baidu/mobad/feeds/BaiduNative;->a(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobad/feeds/BaiduNative$NativeADEventListener;

    invoke-interface {v2, v0}, Lcom/baidu/mobad/feeds/BaiduNative$NativeADEventListener;->onADExposed(Lcom/baidu/mobad/feeds/NativeResponse;)V

    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_11
    const-string v0, "AdStatusChange"

    .line 174
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 175
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->f(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 176
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v0

    .line 177
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_13

    .line 178
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 181
    :goto_7
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->d(Lcom/baidu/mobad/feeds/BaiduNative;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_13

    .line 182
    iget-object v0, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v0}, Lcom/baidu/mobad/feeds/BaiduNative;->d(Lcom/baidu/mobad/feeds/BaiduNative;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    .line 183
    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->isDownloadApp()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getAppPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 184
    iget-object v2, p0, Lcom/baidu/mobad/feeds/BaiduNative$CustomIOAdEventListener;->a:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-static {v2}, Lcom/baidu/mobad/feeds/BaiduNative;->f(Lcom/baidu/mobad/feeds/BaiduNative;)Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/baidu/mobad/feeds/BaiduNative$NativeDownloadListener;->onADStatusChanged(Lcom/baidu/mobad/feeds/NativeResponse;)V

    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_13
    :goto_8
    return-void
.end method
