.class public Lcom/baidu/mobad/nativevideo/PatchVideoNative;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Landroid/widget/RelativeLayout;

.field private d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

.field private e:Lcom/baidu/mobad/nativevideo/a;

.field private f:Lcom/baidu/mobads/i/b;

.field private g:Lcom/baidu/mobad/nativevideo/e;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->a:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->b:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->c:Landroid/widget/RelativeLayout;

    .line 50
    iput-object p4, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    .line 51
    new-instance p1, Lcom/baidu/mobad/nativevideo/a;

    iget-object p2, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->a:Landroid/content/Context;

    iget-object p3, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->b:Ljava/lang/String;

    new-instance p4, Lcom/baidu/mobad/nativevideo/b;

    invoke-direct {p4, p0}, Lcom/baidu/mobad/nativevideo/b;-><init>(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)V

    invoke-direct {p1, p2, p3, p4}, Lcom/baidu/mobad/nativevideo/a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/nativevideo/a$a;)V

    iput-object p1, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->e:Lcom/baidu/mobad/nativevideo/a;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/nativevideo/PatchVideoNative;Lcom/baidu/mobad/nativevideo/e;)Lcom/baidu/mobad/nativevideo/e;
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->g:Lcom/baidu/mobad/nativevideo/e;

    return-object p1
.end method

.method static synthetic a(Lcom/baidu/mobad/nativevideo/PatchVideoNative;Lcom/baidu/mobads/i/b;)Lcom/baidu/mobads/i/b;
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->f:Lcom/baidu/mobads/i/b;

    return-object p1
.end method

.method private a()V
    .locals 2

    .line 105
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobad/nativevideo/c;

    invoke-direct {v1, p0}, Lcom/baidu/mobad/nativevideo/c;-><init>(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lcom/baidu/mobad/feeds/NativeErrorCode;)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    invoke-interface {v0, p1}, Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;->onAdFailed(Lcom/baidu/mobad/feeds/NativeErrorCode;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->a()V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobad/nativevideo/PatchVideoNative;Lcom/baidu/mobad/feeds/NativeErrorCode;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->a(Lcom/baidu/mobad/feeds/NativeErrorCode;)V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    return-object p0
.end method

.method private b()V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    invoke-interface {v0}, Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;->playCompletion()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)Lcom/baidu/mobad/nativevideo/e;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->g:Lcom/baidu/mobad/nativevideo/e;

    return-object p0
.end method

.method private c()V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    invoke-interface {v0}, Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;->playError()V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)Landroid/content/Context;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->a:Landroid/content/Context;

    return-object p0
.end method

.method private d()V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d:Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;

    invoke-interface {v0}, Lcom/baidu/mobad/nativevideo/PatchVideoNative$IPatchVideoNativeListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)Z
    .locals 0

    .line 20
    iget-boolean p0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->h:Z

    return p0
.end method

.method static synthetic f(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)Lcom/baidu/mobads/i/b;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->f:Lcom/baidu/mobads/i/b;

    return-object p0
.end method

.method static synthetic g(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->c:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic h(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->b()V

    return-void
.end method

.method static synthetic i(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->c()V

    return-void
.end method

.method static synthetic j(Lcom/baidu/mobad/nativevideo/PatchVideoNative;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->d()V

    return-void
.end method


# virtual methods
.method public getCurrentPosition()J
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->f:Lcom/baidu/mobads/i/b;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->f:Lcom/baidu/mobads/i/b;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/b;->a()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->f:Lcom/baidu/mobads/i/b;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->f:Lcom/baidu/mobads/i/b;

    invoke-virtual {v0}, Lcom/baidu/mobads/i/b;->b()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public requestAd(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->e:Lcom/baidu/mobad/nativevideo/a;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->e:Lcom/baidu/mobad/nativevideo/a;

    invoke-virtual {v0, p1}, Lcom/baidu/mobad/nativevideo/a;->a(Lcom/baidu/mobad/feeds/RequestParameters;)V

    :cond_0
    return-void
.end method

.method public setVideoMute(Z)V
    .locals 1

    .line 82
    iput-boolean p1, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->h:Z

    .line 83
    iget-object p1, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->f:Lcom/baidu/mobads/i/b;

    if-eqz p1, :cond_0

    .line 84
    iget-object p1, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->f:Lcom/baidu/mobads/i/b;

    iget-boolean v0, p0, Lcom/baidu/mobad/nativevideo/PatchVideoNative;->h:Z

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/i/b;->a(Z)V

    :cond_0
    return-void
.end method
