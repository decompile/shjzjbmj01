.class public Lcom/baidu/mobad/nativevideo/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobad/nativevideo/a$a;
    }
.end annotation


# instance fields
.field private a:Lcom/baidu/mobads/production/b/e;

.field private b:Lcom/baidu/mobad/nativevideo/a$a;

.field private c:Lcom/baidu/mobad/feeds/BaiduNative;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/nativevideo/a$a;)V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/baidu/mobads/production/h/a;

    invoke-direct {v0, p1, p2}, Lcom/baidu/mobads/production/h/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/baidu/mobad/nativevideo/a;->a:Lcom/baidu/mobads/production/b/e;

    .line 35
    iput-object p3, p0, Lcom/baidu/mobad/nativevideo/a;->b:Lcom/baidu/mobad/nativevideo/a$a;

    .line 36
    new-instance p3, Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/a;->a:Lcom/baidu/mobads/production/b/e;

    invoke-direct {p3, p1, p2, p0, v0}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;Lcom/baidu/mobads/production/b/e;)V

    iput-object p3, p0, Lcom/baidu/mobad/nativevideo/a;->c:Lcom/baidu/mobad/feeds/BaiduNative;

    return-void
.end method


# virtual methods
.method public a(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/a;->c:Lcom/baidu/mobad/feeds/BaiduNative;

    invoke-virtual {v0, p1}, Lcom/baidu/mobad/feeds/BaiduNative;->makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method public onNativeFail(Lcom/baidu/mobad/feeds/NativeErrorCode;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/a;->b:Lcom/baidu/mobad/nativevideo/a$a;

    invoke-interface {v0, p1}, Lcom/baidu/mobad/nativevideo/a$a;->a(Lcom/baidu/mobad/feeds/NativeErrorCode;)V

    return-void
.end method

.method public onNativeLoad(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/baidu/mobad/feeds/NativeResponse;",
            ">;)V"
        }
    .end annotation

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 46
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 47
    new-instance v2, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/baidu/mobad/feeds/NativeResponse;

    iget-object v4, p0, Lcom/baidu/mobad/nativevideo/a;->a:Lcom/baidu/mobads/production/b/e;

    iget-object v4, v4, Lcom/baidu/mobads/production/b/e;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v5, p0, Lcom/baidu/mobad/nativevideo/a;->a:Lcom/baidu/mobads/production/b/e;

    iget-object v5, v5, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-direct {v2, v3, v4, v5}, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;-><init>(Lcom/baidu/mobad/feeds/NativeResponse;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdContainer;)V

    .line 49
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobad/nativevideo/a;->b:Lcom/baidu/mobad/nativevideo/a$a;

    invoke-interface {p1, v0}, Lcom/baidu/mobad/nativevideo/a$a;->a(Ljava/util/List;)V

    return-void
.end method
