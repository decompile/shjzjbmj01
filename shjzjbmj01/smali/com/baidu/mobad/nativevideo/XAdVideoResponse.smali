.class public Lcom/baidu/mobad/nativevideo/XAdVideoResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobad/nativevideo/e;


# instance fields
.field a:Lcom/baidu/mobad/feeds/NativeResponse;

.field private b:Lcom/baidu/mobads/interfaces/IXAdContainer;

.field private c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;


# direct methods
.method public constructor <init>(Lcom/baidu/mobad/feeds/NativeResponse;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdContainer;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    .line 16
    iput-object p3, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->b:Lcom/baidu/mobads/interfaces/IXAdContainer;

    .line 17
    iput-object p2, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-void
.end method


# virtual methods
.method public getAdLogoUrl()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getAdLogoUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBaiduLogoUrl()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getBaiduLogoUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDesc()Ljava/lang/String;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getDesc()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaterialType()Ljava/lang/String;
    .locals 3

    const-string v0, "normal"

    .line 44
    iget-object v1, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v1, :cond_0

    .line 45
    sget-object v1, Lcom/baidu/mobad/nativevideo/f;->a:[I

    iget-object v2, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v2}, Lcom/baidu/mobad/feeds/NativeResponse;->getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 50
    :pswitch_0
    iget-object v1, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v1}, Lcom/baidu/mobad/feeds/NativeResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".gif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "gif"

    goto :goto_0

    :pswitch_1
    const-string v0, "video"

    :cond_0
    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoUrl()Ljava/lang/String;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleClick(Landroid/view/View;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0, p1}, Lcom/baidu/mobad/feeds/NativeResponse;->handleClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public handleClick(Landroid/view/View;I)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobad/feeds/NativeResponse;->handleClick(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public recordImpression(Landroid/view/View;)V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/baidu/mobad/nativevideo/XAdVideoResponse;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0, p1}, Lcom/baidu/mobad/feeds/NativeResponse;->recordImpression(Landroid/view/View;)V

    :cond_0
    return-void
.end method
