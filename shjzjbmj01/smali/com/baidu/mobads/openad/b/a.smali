.class public Lcom/baidu/mobads/openad/b/a;
.super Lcom/baidu/mobads/openad/a/c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/openad/b/a$a;
    }
.end annotation


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private f:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, v0}, Lcom/baidu/mobads/openad/b/a;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 55
    invoke-direct {p0}, Lcom/baidu/mobads/openad/a/c;-><init>()V

    const/4 v0, 0x0

    .line 48
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/a;->c:Ljava/lang/Boolean;

    .line 50
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/a;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 52
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/a;->e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 53
    new-instance v0, Lcom/baidu/mobads/utils/aa;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/aa;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/openad/b/a;->f:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    .line 56
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/a;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/openad/b/a;)Ljava/lang/Boolean;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/openad/b/a;->c:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 0

    .line 37
    invoke-static {p0}, Lcom/baidu/mobads/openad/b/a;->b(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/openad/b/a;->e:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    return-object p0
.end method

.method private static b(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    .line 198
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 199
    new-instance p0, Ljava/io/BufferedReader;

    invoke-direct {p0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 201
    :goto_0
    invoke-virtual {p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 202
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static synthetic c(Lcom/baidu/mobads/openad/b/a;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/openad/b/a;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic d(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/openad/b/a;->f:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lcom/baidu/mobads/openad/b/b;)V
    .locals 2

    const-wide v0, 0x40d3880000000000L    # 20000.0

    .line 70
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/b;D)V

    return-void
.end method

.method public a(Lcom/baidu/mobads/openad/b/b;D)V
    .locals 2

    .line 75
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/openad/b/a$a;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/baidu/mobads/openad/b/a$a;-><init>(Lcom/baidu/mobads/openad/b/a;Lcom/baidu/mobads/openad/b/b;D)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/openad/b/b;Ljava/lang/Boolean;)V
    .locals 2

    .line 65
    iput-object p2, p0, Lcom/baidu/mobads/openad/b/a;->c:Ljava/lang/Boolean;

    const-wide v0, 0x40d3880000000000L    # 20000.0

    .line 66
    invoke-virtual {p0, p1, v0, v1}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/b;D)V

    return-void
.end method
