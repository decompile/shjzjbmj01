.class Lcom/baidu/mobads/openad/b/a$a;
.super Lcom/baidu/mobads/f/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/openad/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/b/a;

.field private c:Lcom/baidu/mobads/openad/b/b;

.field private d:D


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/openad/b/a;Lcom/baidu/mobads/openad/b/b;D)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-direct {p0}, Lcom/baidu/mobads/f/a;-><init>()V

    .line 86
    iput-object p2, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    .line 87
    iput-wide p3, p0, Lcom/baidu/mobads/openad/b/a$a;->d:D

    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 9

    const/4 v0, 0x0

    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget-wide v1, v1, Lcom/baidu/mobads/openad/b/b;->c:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_0

    .line 97
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget-wide v1, v1, Lcom/baidu/mobads/openad/b/b;->c:J

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    const/4 v1, -0x1

    .line 100
    :try_start_1
    iget-object v2, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget-object v2, v2, Lcom/baidu/mobads/openad/b/b;->a:Ljava/lang/String;

    const-string v3, "mobads.baidu.com"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-le v2, v1, :cond_1

    .line 101
    invoke-static {}, Lcom/baidu/mobads/j;->a()Lcom/baidu/mobads/j;

    move-result-object v2

    const/16 v3, 0x3eb

    invoke-virtual {v2, v3}, Lcom/baidu/mobads/j;->a(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v2

    .line 105
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 108
    :cond_1
    :goto_0
    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget-object v3, v3, Lcom/baidu/mobads/openad/b/b;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 109
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v3

    .line 110
    invoke-interface {v3, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->getHttpURLConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v2
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 111
    :try_start_3
    iget-wide v3, p0, Lcom/baidu/mobads/openad/b/a$a;->d:D

    double-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 112
    iget-wide v3, p0, Lcom/baidu/mobads/openad/b/a$a;->d:D

    double-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const/4 v3, 0x0

    .line 114
    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 115
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget-object v3, v3, Lcom/baidu/mobads/openad/b/b;->b:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget-object v3, v3, Lcom/baidu/mobads/openad/b/b;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    const-string v3, "User-Agent"

    .line 116
    iget-object v4, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget-object v4, v4, Lcom/baidu/mobads/openad/b/b;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v3, "Content-type"

    .line 118
    iget-object v4, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget-object v4, v4, Lcom/baidu/mobads/openad/b/b;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "Connection"

    const-string v4, "keep-alive"

    .line 119
    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "Cache-Control"

    const-string v4, "no-cache"

    .line 120
    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x8

    if-ge v3, v4, :cond_3

    const-string v3, "http.keepAlive"

    const-string v4, "false"

    .line 123
    invoke-static {v3, v4}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 126
    :cond_3
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget v3, v3, Lcom/baidu/mobads/openad/b/b;->e:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    const-string v3, "GET"

    .line 127
    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 131
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    .line 132
    iget-object v4, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v4}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_5

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_5

    .line 133
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 135
    :try_start_4
    invoke-static {v3}, Lcom/baidu/mobads/openad/b/a;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 137
    :try_start_5
    iget-object v5, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget-object v5, v5, Lcom/baidu/mobads/openad/b/b;->a:Ljava/lang/String;

    const-string v6, "mobads.baidu.com"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-le v5, v1, :cond_4

    .line 138
    invoke-static {}, Lcom/baidu/mobads/j;->a()Lcom/baidu/mobads/j;

    move-result-object v1

    const/16 v5, 0x3ec

    invoke-virtual {v1, v5}, Lcom/baidu/mobads/j;->a(I)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 143
    :catch_1
    :cond_4
    :try_start_6
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    new-instance v5, Lcom/baidu/mobads/openad/a/d;

    const-string v6, "URLLoader.Load.Complete"

    iget-object v7, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    invoke-virtual {v7}, Lcom/baidu/mobads/openad/b/b;->a()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v4, v7}, Lcom/baidu/mobads/openad/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Lcom/baidu/mobads/openad/b/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto/16 :goto_2

    :catch_2
    move-exception v1

    goto/16 :goto_4

    .line 145
    :cond_5
    :try_start_7
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    new-instance v4, Lcom/baidu/mobads/openad/a/a;

    const-string v5, "URLLoader.Load.Error"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RequestError: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v5, v3}, Lcom/baidu/mobads/openad/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lcom/baidu/mobads/openad/b/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_1

    .line 148
    :cond_6
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    iget v1, v1, Lcom/baidu/mobads/openad/b/b;->e:I

    if-nez v1, :cond_8

    const-string v1, "POST"

    .line 149
    invoke-virtual {v2, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 151
    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 153
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/b;->b()Landroid/net/Uri$Builder;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 154
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->c:Lcom/baidu/mobads/openad/b/b;

    invoke-virtual {v1}, Lcom/baidu/mobads/openad/b/b;->b()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    .line 156
    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    const-string v6, "UTF-8"

    invoke-direct {v5, v3, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 158
    invoke-virtual {v4, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->flush()V

    .line 160
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V

    .line 161
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 163
    :cond_7
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 164
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    .line 165
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v1}, Lcom/baidu/mobads/openad/b/a;->b(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    const-string v3, "OAdURLLoader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Post connect code :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_8
    :goto_1
    move-object v3, v0

    :goto_2
    if-eqz v3, :cond_9

    .line 178
    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_3

    :catch_3
    move-exception v1

    .line 184
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v3}, Lcom/baidu/mobads/openad/b/a;->b(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v3

    const-string v4, "OAdURLLoader"

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v4, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_9
    :goto_3
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v1}, Lcom/baidu/mobads/openad/b/a;->d(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v1

    if-eqz v1, :cond_c

    goto/16 :goto_6

    :catchall_0
    move-exception v1

    goto/16 :goto_7

    :catch_4
    move-exception v1

    move-object v3, v0

    goto :goto_4

    :catchall_1
    move-exception v1

    move-object v2, v0

    goto/16 :goto_7

    :catch_5
    move-exception v1

    move-object v2, v0

    move-object v3, v2

    .line 168
    :goto_4
    :try_start_9
    iget-object v4, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v4}, Lcom/baidu/mobads/openad/b/a;->b(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v4

    const-string v5, "OAdURLLoader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "load throwable :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v4, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v4}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/a;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_a

    iget-object v4, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v4}, Lcom/baidu/mobads/openad/b/a;->c(Lcom/baidu/mobads/openad/b/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-nez v4, :cond_a

    .line 170
    iget-object v4, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    new-instance v5, Lcom/baidu/mobads/openad/a/a;

    const-string v6, "URLLoader.Load.Error"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "RuntimeError: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v6, v1}, Lcom/baidu/mobads/openad/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {v4, v5}, Lcom/baidu/mobads/openad/b/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :cond_a
    if-eqz v3, :cond_b

    .line 178
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_6

    goto :goto_5

    :catch_6
    move-exception v1

    .line 184
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v3}, Lcom/baidu/mobads/openad/b/a;->b(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v3

    const-string v4, "OAdURLLoader"

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v4, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_b
    :goto_5
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v1}, Lcom/baidu/mobads/openad/b/a;->d(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 188
    :goto_6
    iget-object v1, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v1}, Lcom/baidu/mobads/openad/b/a;->d(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_c
    return-object v0

    :catchall_2
    move-exception v1

    move-object v0, v3

    :goto_7
    if-eqz v0, :cond_d

    .line 178
    :try_start_b
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_7

    goto :goto_8

    :catch_7
    move-exception v0

    .line 184
    iget-object v3, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v3}, Lcom/baidu/mobads/openad/b/a;->b(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v3

    const-string v4, "OAdURLLoader"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_d
    :goto_8
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->d(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 188
    iget-object v0, p0, Lcom/baidu/mobads/openad/b/a$a;->a:Lcom/baidu/mobads/openad/b/a;

    invoke-static {v0}, Lcom/baidu/mobads/openad/b/a;->d(Lcom/baidu/mobads/openad/b/a;)Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_e
    throw v1
.end method
