.class public Lcom/baidu/mobads/openad/download/b;
.super Ljava/util/Observable;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;
.implements Ljava/lang/Runnable;


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Ljava/net/URL;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field protected e:I

.field protected f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

.field protected g:I

.field protected h:I

.field protected i:[B

.field protected j:Landroid/graphics/Bitmap;

.field private k:Z

.field private l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .line 83
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    const/4 v0, 0x0

    .line 64
    iput-boolean v0, p0, Lcom/baidu/mobads/openad/download/b;->k:Z

    .line 68
    new-instance v1, Lcom/baidu/mobads/utils/aa;

    invoke-direct {v1}, Lcom/baidu/mobads/utils/aa;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/openad/download/b;->l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    .line 84
    iput-object p1, p0, Lcom/baidu/mobads/openad/download/b;->a:Landroid/content/Context;

    .line 85
    iput-object p2, p0, Lcom/baidu/mobads/openad/download/b;->b:Ljava/net/URL;

    .line 86
    iput-object p3, p0, Lcom/baidu/mobads/openad/download/b;->c:Ljava/lang/String;

    .line 87
    iput-boolean p5, p0, Lcom/baidu/mobads/openad/download/b;->k:Z

    if-eqz p4, :cond_0

    .line 89
    invoke-virtual {p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    .line 90
    iput-object p4, p0, Lcom/baidu/mobads/openad/download/b;->d:Ljava/lang/String;

    goto :goto_0

    .line 92
    :cond_0
    invoke-virtual {p2}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object p1

    const/16 p2, 0x2f

    .line 93
    invoke-virtual {p1, p2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result p2

    add-int/lit8 p2, p2, 0x1

    invoke-virtual {p1, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/openad/download/b;->d:Ljava/lang/String;

    :goto_0
    const/4 p1, -0x1

    .line 95
    iput p1, p0, Lcom/baidu/mobads/openad/download/b;->e:I

    .line 96
    sget-object p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    iput-object p1, p0, Lcom/baidu/mobads/openad/download/b;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    .line 97
    iput v0, p0, Lcom/baidu/mobads/openad/download/b;->g:I

    .line 98
    iput v0, p0, Lcom/baidu/mobads/openad/download/b;->h:I

    return-void
.end method

.method private a(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;
    .locals 2

    .line 354
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_0

    const/16 v1, 0x12d

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "Location"

    .line 356
    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 357
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/baidu/mobads/openad/download/b;->b:Ljava/net/URL;

    .line 358
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->b:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/16 p1, 0x2710

    .line 359
    :try_start_1
    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const/4 p1, 0x0

    .line 360
    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    const-string p1, "Range"

    const-string v1, "bytes=0-"

    .line 361
    invoke-virtual {v0, p1, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object p1, v0

    goto :goto_0

    :catch_0
    move-object p1, v0

    :catch_1
    :cond_1
    return-object p1
.end method

.method private d()V
    .locals 1

    .line 195
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/download/b;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    .line 170
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/f/c;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected a(IF)V
    .locals 0

    .line 177
    iget p2, p0, Lcom/baidu/mobads/openad/download/b;->g:I

    add-int/2addr p2, p1

    iput p2, p0, Lcom/baidu/mobads/openad/download/b;->g:I

    .line 178
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/download/b;->b()V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/baidu/mobads/openad/download/b;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    .line 163
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/download/b;->b()V

    return-void
.end method

.method protected b()V
    .locals 0

    .line 185
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/download/b;->setChanged()V

    .line 186
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/download/b;->notifyObservers()V

    return-void
.end method

.method protected c()V
    .locals 4

    .line 378
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/baidu/mobads/openad/download/b;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/openad/download/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/mobads/openad/download/b;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/openad/download/b;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 379
    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->renameFile(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public cancel()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public getFileSize()I
    .locals 1

    .line 141
    iget v0, p0, Lcom/baidu/mobads/openad/download/b;->e:I

    return v0
.end method

.method public getOutputPath()Ljava/lang/String;
    .locals 2

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/baidu/mobads/openad/download/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/openad/download/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProgress()F
    .locals 2

    .line 148
    iget v0, p0, Lcom/baidu/mobads/openad/download/b;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/baidu/mobads/openad/download/b;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method public getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    return-object v0
.end method

.method public getTargetURL()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->b:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPausedManually()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public removeObservers()V
    .locals 0

    return-void
.end method

.method public resume()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public run()V
    .locals 15

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 207
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v4

    iget-object v5, p0, Lcom/baidu/mobads/openad/download/b;->b:Ljava/net/URL;

    invoke-interface {v4, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->getHttpURLConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    const/16 v5, 0x2710

    .line 208
    :try_start_1
    invoke-virtual {v4, v5}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 209
    invoke-virtual {v4, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 210
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    .line 213
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    const/16 v6, 0x12e

    if-eq v5, v6, :cond_1

    const/16 v6, 0x12d

    if-ne v5, v6, :cond_0

    goto :goto_0

    :cond_0
    move v14, v5

    move-object v5, v4

    move v4, v14

    goto :goto_1

    .line 217
    :cond_1
    :goto_0
    invoke-virtual {v4, v3}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 218
    invoke-direct {p0, v4}, Lcom/baidu/mobads/openad/download/b;->a(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;

    move-result-object v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 219
    :try_start_2
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    .line 222
    :goto_1
    div-int/lit8 v4, v4, 0x64

    if-eq v4, v2, :cond_2

    .line 223
    invoke-direct {p0}, Lcom/baidu/mobads/openad/download/b;->d()V

    .line 225
    :cond_2
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v4

    if-lez v4, :cond_3

    .line 227
    iput v4, p0, Lcom/baidu/mobads/openad/download/b;->e:I

    .line 231
    :cond_3
    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lcom/baidu/mobads/openad/download/b;->c:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 232
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_4

    .line 233
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 235
    :cond_4
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 236
    :try_start_3
    iget-object v6, p0, Lcom/baidu/mobads/openad/download/b;->b:Ljava/net/URL;

    invoke-virtual {v6}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "mobads.baidu.com"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lcom/baidu/mobads/openad/download/b;->b:Ljava/net/URL;

    invoke-virtual {v6}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v8, ".mp4"

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lcom/baidu/mobads/openad/download/b;->b:Ljava/net/URL;

    .line 237
    invoke-virtual {v6}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v8, ".gif"

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-ne v6, v7, :cond_6

    .line 240
    :try_start_4
    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/baidu/mobads/openad/download/b;->j:Landroid/graphics/Bitmap;

    .line 241
    iget-object v6, p0, Lcom/baidu/mobads/openad/download/b;->j:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_6

    .line 242
    iget-object v6, p0, Lcom/baidu/mobads/openad/download/b;->d:Ljava/lang/String;

    .line 243
    invoke-static {}, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->getInstance()Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;

    move-result-object v8

    iget-object v9, p0, Lcom/baidu/mobads/openad/download/b;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v6, v9}, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 245
    sget-object v6, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/openad/download/b;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    .line 247
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v6

    new-instance v8, Lcom/baidu/mobads/openad/download/c;

    invoke-direct {v8, p0}, Lcom/baidu/mobads/openad/download/c;-><init>(Lcom/baidu/mobads/openad/download/b;)V

    invoke-virtual {v6, v8}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 339
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 341
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v6, "OAdSimpleFileDownloader"

    aput-object v6, v2, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-interface {v4, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 344
    :goto_2
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_5

    .line 345
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_5
    return-void

    .line 286
    :catch_1
    :cond_6
    :try_start_6
    new-instance v6, Ljava/io/BufferedOutputStream;

    new-instance v8, Ljava/io/FileOutputStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/baidu/mobads/openad/download/b;->getOutputPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ".tmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v8}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    const/16 v8, 0x2800

    .line 287
    :try_start_7
    new-array v9, v8, [B

    .line 288
    iget-boolean v10, p0, Lcom/baidu/mobads/openad/download/b;->k:Z

    if-eqz v10, :cond_7

    .line 289
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move-object v0, v10

    :cond_7
    const/4 v10, 0x0

    .line 295
    :goto_3
    iget-object v11, p0, Lcom/baidu/mobads/openad/download/b;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v12, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v11, v12, :cond_9

    invoke-virtual {v4, v9, v3, v8}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v11

    if-eq v11, v7, :cond_9

    .line 296
    invoke-virtual {v6, v9, v3, v11}, Ljava/io/BufferedOutputStream;->write([BII)V

    if-eqz v0, :cond_8

    .line 298
    invoke-virtual {v0, v9, v3, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :cond_8
    add-int/2addr v10, v11

    int-to-float v12, v10

    .line 302
    iget v13, p0, Lcom/baidu/mobads/openad/download/b;->e:I

    int-to-float v13, v13

    div-float/2addr v12, v13

    invoke-virtual {p0, v11, v12}, Lcom/baidu/mobads/openad/download/b;->a(IF)V

    goto :goto_3

    .line 304
    :cond_9
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->flush()V

    if-eqz v0, :cond_a

    .line 307
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 311
    :cond_a
    iget-object v7, p0, Lcom/baidu/mobads/openad/download/b;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v8, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne v7, v8, :cond_b

    .line 313
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/download/b;->c()V

    .line 315
    sget-object v7, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v7}, Lcom/baidu/mobads/openad/download/b;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    goto :goto_4

    .line 316
    :cond_b
    iget-object v7, p0, Lcom/baidu/mobads/openad/download/b;->f:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    sget-object v7, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 324
    :goto_4
    :try_start_8
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_5

    :catch_2
    move-exception v6

    .line 326
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v7

    new-array v8, v2, [Ljava/lang/Object;

    const-string v9, "OAdSimpleFileDownloader"

    aput-object v9, v8, v3

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v1

    invoke-interface {v7, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :goto_5
    if-eqz v0, :cond_c

    .line 331
    :try_start_9
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_6

    :catch_3
    move-exception v0

    .line 333
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v6

    invoke-virtual {v6}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v6

    new-array v7, v2, [Ljava/lang/Object;

    const-string v8, "OAdSimpleFileDownloader"

    aput-object v8, v7, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-interface {v6, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 339
    :cond_c
    :goto_6
    :try_start_a
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4

    goto :goto_7

    :catch_4
    move-exception v0

    .line 341
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v6, "OAdSimpleFileDownloader"

    aput-object v6, v2, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-interface {v4, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 344
    :goto_7
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_10

    .line 345
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    goto/16 :goto_e

    :catchall_0
    move-exception v7

    move-object v14, v4

    move-object v4, v0

    move-object v0, v6

    move-object v6, v7

    move-object v7, v5

    move-object v5, v14

    goto/16 :goto_f

    :catch_5
    move-exception v7

    move-object v14, v4

    move-object v4, v0

    move-object v0, v6

    move-object v6, v7

    move-object v7, v5

    move-object v5, v14

    goto :goto_a

    :catchall_1
    move-exception v6

    move-object v7, v5

    move-object v5, v4

    move-object v4, v0

    goto/16 :goto_f

    :catch_6
    move-exception v6

    move-object v7, v5

    move-object v5, v4

    move-object v4, v0

    goto :goto_a

    :catchall_2
    move-exception v4

    move-object v6, v4

    move-object v7, v5

    goto :goto_8

    :catch_7
    move-exception v4

    move-object v6, v4

    move-object v7, v5

    goto :goto_9

    :catchall_3
    move-exception v5

    move-object v7, v4

    move-object v6, v5

    :goto_8
    move-object v4, v0

    move-object v5, v4

    goto/16 :goto_f

    :catch_8
    move-exception v5

    move-object v7, v4

    move-object v6, v5

    :goto_9
    move-object v4, v0

    move-object v5, v4

    goto :goto_a

    :catchall_4
    move-exception v4

    move-object v5, v0

    move-object v7, v5

    move-object v6, v4

    move-object v4, v7

    goto/16 :goto_f

    :catch_9
    move-exception v4

    move-object v5, v0

    move-object v7, v5

    move-object v6, v4

    move-object v4, v7

    .line 319
    :goto_a
    :try_start_b
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v8

    invoke-virtual {v8}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v8

    new-array v9, v2, [Ljava/lang/Object;

    const-string v10, "OAdSimpleFileDownloader"

    aput-object v10, v9, v3

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v1

    invoke-interface {v8, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 320
    invoke-direct {p0}, Lcom/baidu/mobads/openad/download/b;->d()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    if-eqz v0, :cond_d

    .line 324
    :try_start_c
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_a

    goto :goto_b

    :catch_a
    move-exception v0

    .line 326
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v6

    invoke-virtual {v6}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v6

    new-array v8, v2, [Ljava/lang/Object;

    const-string v9, "OAdSimpleFileDownloader"

    aput-object v9, v8, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v1

    invoke-interface {v6, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :cond_d
    :goto_b
    if-eqz v4, :cond_e

    .line 331
    :try_start_d
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_b

    goto :goto_c

    :catch_b
    move-exception v0

    .line 333
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v4

    new-array v6, v2, [Ljava/lang/Object;

    const-string v8, "OAdSimpleFileDownloader"

    aput-object v8, v6, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-interface {v4, v6}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :cond_e
    :goto_c
    if-eqz v5, :cond_f

    .line 339
    :try_start_e
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_c

    goto :goto_d

    :catch_c
    move-exception v0

    .line 341
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "OAdSimpleFileDownloader"

    aput-object v5, v2, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-interface {v4, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 344
    :cond_f
    :goto_d
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_10

    .line 345
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    invoke-interface {v0, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_10
    :goto_e
    return-void

    :catchall_5
    move-exception v6

    :goto_f
    if-eqz v0, :cond_11

    .line 324
    :try_start_f
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_d

    goto :goto_10

    :catch_d
    move-exception v0

    .line 326
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v8

    invoke-virtual {v8}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v8

    new-array v9, v2, [Ljava/lang/Object;

    const-string v10, "OAdSimpleFileDownloader"

    aput-object v10, v9, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v1

    invoke-interface {v8, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :cond_11
    :goto_10
    if-eqz v4, :cond_12

    .line 331
    :try_start_10
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_e

    goto :goto_11

    :catch_e
    move-exception v0

    .line 333
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v4

    new-array v8, v2, [Ljava/lang/Object;

    const-string v9, "OAdSimpleFileDownloader"

    aput-object v9, v8, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v1

    invoke-interface {v4, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :cond_12
    :goto_11
    if-eqz v5, :cond_13

    .line 339
    :try_start_11
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_f

    goto :goto_12

    :catch_f
    move-exception v0

    .line 341
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "OAdSimpleFileDownloader"

    aput-object v5, v2, v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-interface {v4, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 344
    :cond_13
    :goto_12
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    if-eqz v0, :cond_14

    .line 345
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/b;->l:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    invoke-interface {v0, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->closeHttpURLConnection(Ljava/net/HttpURLConnection;)V

    :cond_14
    throw v6
.end method

.method public setPausedManually(Z)V
    .locals 0

    return-void
.end method

.method public start()V
    .locals 1

    .line 126
    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->DOWNLOADING:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/openad/download/b;->a(Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;)V

    .line 127
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/download/b;->a()V

    return-void
.end method
