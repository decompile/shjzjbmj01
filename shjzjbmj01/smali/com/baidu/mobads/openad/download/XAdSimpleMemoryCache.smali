.class public Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;

.field private static b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->b:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;
    .locals 2

    .line 15
    sget-object v0, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->a:Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;

    if-nez v0, :cond_1

    .line 16
    const-class v0, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;

    monitor-enter v0

    .line 17
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->a:Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;

    if-nez v1, :cond_0

    .line 18
    new-instance v1, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;

    invoke-direct {v1}, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;-><init>()V

    sput-object v1, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->a:Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;

    .line 20
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 22
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->a:Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;

    return-object v0
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .line 47
    sget-object v0, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->clear()V

    return-void
.end method

.method public get(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    if-eqz p1, :cond_0

    .line 35
    sget-object v0, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Bitmap;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public put(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 28
    sget-object v0, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public remove(Ljava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 42
    sget-object v0, Lcom/baidu/mobads/openad/download/XAdSimpleMemoryCache;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
