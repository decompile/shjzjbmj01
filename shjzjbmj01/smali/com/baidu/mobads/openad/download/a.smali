.class public Lcom/baidu/mobads/openad/download/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;


# static fields
.field private static b:Lcom/baidu/mobads/openad/download/a;


# instance fields
.field protected a:Landroid/content/Context;

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/openad/download/a;->c:Ljava/util/HashMap;

    .line 25
    iput-object p1, p0, Lcom/baidu/mobads/openad/download/a;->a:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/baidu/mobads/openad/download/a;
    .locals 1

    .line 34
    sget-object v0, Lcom/baidu/mobads/openad/download/a;->b:Lcom/baidu/mobads/openad/download/a;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/baidu/mobads/openad/download/a;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/openad/download/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/baidu/mobads/openad/download/a;->b:Lcom/baidu/mobads/openad/download/a;

    .line 37
    :cond_0
    sget-object p0, Lcom/baidu/mobads/openad/download/a;->b:Lcom/baidu/mobads/openad/download/a;

    return-object p0
.end method


# virtual methods
.method public declared-synchronized createAdsApkDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;
    .locals 0

    monitor-enter p0

    .line 92
    monitor-exit p0

    const/4 p1, 0x0

    return-object p1
.end method

.method public createImgHttpDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/interfaces/download/IXAdStaticImgDownloader;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public createSimpleFileDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;
    .locals 7

    .line 98
    new-instance v6, Lcom/baidu/mobads/openad/download/b;

    iget-object v1, p0, Lcom/baidu/mobads/openad/download/a;->a:Landroid/content/Context;

    move-object v0, v6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/openad/download/b;-><init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v6
.end method

.method public getAdsApkDownloader(Ljava/lang/String;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/a;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 51
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/download/a;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 52
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getAllAdsApkDownloaderes()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;",
            ">;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/a;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/download/a;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 79
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 80
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 81
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    .line 82
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 85
    :cond_1
    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeAdsApkDownloader(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/a;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 43
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/download/a;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x1

    .line 45
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    .line 44
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public removeAllAdsApkDownloaderes()V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/baidu/mobads/openad/download/a;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 70
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/openad/download/a;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 71
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public resumeUndownloadedAfterRestartApp(J)V
    .locals 0

    return-void
.end method
