.class Lcom/baidu/mobads/openad/download/c;
.super Lcom/baidu/mobads/f/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/download/b;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/openad/download/b;)V
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    invoke-direct {p0}, Lcom/baidu/mobads/f/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic a()Ljava/lang/Object;
    .locals 1

    .line 247
    invoke-virtual {p0}, Lcom/baidu/mobads/openad/download/c;->d()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected d()Ljava/lang/Void;
    .locals 5

    .line 251
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 252
    iget-object v1, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    iget-object v1, v1, Lcom/baidu/mobads/openad/download/b;->j:Landroid/graphics/Bitmap;

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 253
    iget-object v1, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, v1, Lcom/baidu/mobads/openad/download/b;->i:[B

    const/4 v0, 0x0

    .line 256
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    iget-object v4, v4, Lcom/baidu/mobads/openad/download/b;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    iget-object v4, v4, Lcom/baidu/mobads/openad/download/b;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 259
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    .line 264
    :try_start_1
    iget-object v2, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    iget-object v2, v2, Lcom/baidu/mobads/openad/download/b;->i:[B

    invoke-virtual {v1, v2}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 265
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    .line 266
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    iget-object v3, v3, Lcom/baidu/mobads/openad/download/b;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    iget-object v3, v3, Lcom/baidu/mobads/openad/download/b;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".tmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    iget-object v4, v4, Lcom/baidu/mobads/openad/download/b;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/baidu/mobads/openad/download/c;->a:Lcom/baidu/mobads/openad/download/b;

    iget-object v4, v4, Lcom/baidu/mobads/openad/download/b;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 267
    invoke-interface {v1, v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->renameFile(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    .line 271
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    :cond_0
    :goto_1
    return-object v0
.end method
