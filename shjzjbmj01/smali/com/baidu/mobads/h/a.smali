.class public Lcom/baidu/mobads/h/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/h/a$a;
    }
.end annotation


# static fields
.field private static c:Ljava/lang/reflect/Method;

.field private static d:Ljava/lang/reflect/Method;

.field private static e:Ljava/lang/reflect/Method;

.field private static f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private static h:[C


# instance fields
.field private a:Landroid/telephony/TelephonyManager;

.field private b:Lcom/baidu/mobads/h/a$a;

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."

    .line 39
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/h/a;->h:[C

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/baidu/mobads/h/a;->a:Landroid/telephony/TelephonyManager;

    .line 32
    new-instance v1, Lcom/baidu/mobads/h/a$a;

    invoke-direct {v1, p0, v0}, Lcom/baidu/mobads/h/a$a;-><init>(Lcom/baidu/mobads/h/a;Lcom/baidu/mobads/h/b;)V

    iput-object v1, p0, Lcom/baidu/mobads/h/a;->b:Lcom/baidu/mobads/h/a$a;

    .line 38
    iput-object v0, p0, Lcom/baidu/mobads/h/a;->g:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "&null"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/h/a;->g:Ljava/lang/String;

    .line 45
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/telephony/TelephonyManager;

    iput-object p1, p0, Lcom/baidu/mobads/h/a;->a:Landroid/telephony/TelephonyManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private a(Landroid/telephony/CellInfo;)Lcom/baidu/mobads/h/a$a;
    .locals 6

    .line 223
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x11

    if-ge v0, v2, :cond_0

    return-object v1

    .line 228
    :cond_0
    new-instance v2, Lcom/baidu/mobads/h/a$a;

    invoke-direct {v2, p0, v1}, Lcom/baidu/mobads/h/a$a;-><init>(Lcom/baidu/mobads/h/a;Lcom/baidu/mobads/h/b;)V

    const/4 v1, 0x0

    .line 230
    instance-of v3, p1, Landroid/telephony/CellInfoGsm;

    const/4 v4, 0x1

    const/16 v5, 0x67

    if-eqz v3, :cond_1

    .line 232
    move-object v1, p1

    check-cast v1, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v1}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v1

    .line 237
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/h/a$a;->c:I

    .line 238
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/h/a$a;->d:I

    .line 239
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/h/a$a;->a:I

    .line 240
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v1

    iput v1, v2, Lcom/baidu/mobads/h/a$a;->b:I

    .line 241
    iput-char v5, v2, Lcom/baidu/mobads/h/a$a;->e:C

    goto :goto_0

    .line 243
    :cond_1
    instance-of v3, p1, Landroid/telephony/CellInfoCdma;

    if-eqz v3, :cond_2

    .line 245
    move-object v1, p1

    check-cast v1, Landroid/telephony/CellInfoCdma;

    invoke-virtual {v1}, Landroid/telephony/CellInfoCdma;->getCellIdentity()Landroid/telephony/CellIdentityCdma;

    move-result-object v1

    .line 252
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/h/a$a;->d:I

    .line 253
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/h/a$a;->a:I

    .line 254
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v1

    iput v1, v2, Lcom/baidu/mobads/h/a$a;->b:I

    const/16 v1, 0x77

    .line 255
    iput-char v1, v2, Lcom/baidu/mobads/h/a$a;->e:C

    goto :goto_0

    .line 258
    :cond_2
    instance-of v3, p1, Landroid/telephony/CellInfoLte;

    if-eqz v3, :cond_3

    .line 260
    move-object v1, p1

    check-cast v1, Landroid/telephony/CellInfoLte;

    invoke-virtual {v1}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v1

    .line 265
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/h/a$a;->c:I

    .line 266
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/h/a$a;->d:I

    .line 267
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v3

    iput v3, v2, Lcom/baidu/mobads/h/a$a;->a:I

    .line 268
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v1

    iput v1, v2, Lcom/baidu/mobads/h/a$a;->b:I

    .line 269
    iput-char v5, v2, Lcom/baidu/mobads/h/a$a;->e:C

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    :goto_0
    const/16 v1, 0x12

    if-lt v0, v1, :cond_4

    if-nez v4, :cond_4

    .line 275
    :try_start_0
    instance-of v0, p1, Landroid/telephony/CellInfoWcdma;

    if-eqz v0, :cond_4

    .line 278
    check-cast p1, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {p1}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object p1

    .line 283
    invoke-virtual {p1}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v0

    iput v0, v2, Lcom/baidu/mobads/h/a$a;->c:I

    .line 284
    invoke-virtual {p1}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v0

    iput v0, v2, Lcom/baidu/mobads/h/a$a;->d:I

    .line 285
    invoke-virtual {p1}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result v0

    iput v0, v2, Lcom/baidu/mobads/h/a$a;->a:I

    .line 286
    invoke-virtual {p1}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/baidu/mobads/h/a;->b(I)I

    move-result p1

    iput p1, v2, Lcom/baidu/mobads/h/a$a;->b:I

    .line 287
    iput-char v5, v2, Lcom/baidu/mobads/h/a$a;->e:C
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    return-object v2
.end method

.method private a(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x3

    const/4 p1, 0x0

    .line 75
    :try_start_0
    invoke-direct {p0}, Lcom/baidu/mobads/h/a;->b()Lcom/baidu/mobads/h/a$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 76
    invoke-static {v0}, Lcom/baidu/mobads/h/a$a;->a(Lcom/baidu/mobads/h/a$a;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 82
    :cond_0
    iput-object v0, p0, Lcom/baidu/mobads/h/a;->b:Lcom/baidu/mobads/h/a$a;

    goto :goto_1

    .line 77
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/h/a;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/h/a;->a(Landroid/telephony/CellLocation;)V

    .line 84
    :goto_1
    iget-object v0, p0, Lcom/baidu/mobads/h/a;->b:Lcom/baidu/mobads/h/a$a;

    invoke-virtual {v0}, Lcom/baidu/mobads/h/a$a;->a()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-object v0, p1

    :goto_2
    if-nez v0, :cond_2

    const-string v0, "Z"

    :cond_2
    const-string v1, "Z"

    .line 96
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    return-object p1

    .line 100
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "t"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/baidu/mobads/h/a;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/baidu/mobads/h/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 348
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    .line 350
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    int-to-byte v0, v0

    .line 351
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    int-to-byte v1, v1

    .line 352
    array-length v2, p0

    add-int/lit8 v2, v2, 0x2

    new-array v2, v2, [B

    .line 354
    array-length v3, p0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-byte v6, p0, v4

    add-int/lit8 v7, v5, 0x1

    xor-int/2addr v6, v0

    int-to-byte v6, v6

    .line 355
    aput-byte v6, v2, v5

    add-int/lit8 v4, v4, 0x1

    move v5, v7

    goto :goto_0

    :cond_1
    add-int/lit8 p0, v5, 0x1

    .line 357
    aput-byte v0, v2, v5

    .line 358
    aput-byte v1, v2, p0

    .line 359
    invoke-static {v2}, Lcom/baidu/mobads/h/a;->a([B)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a([B)Ljava/lang/String;
    .locals 10

    .line 363
    array-length v0, p0

    add-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x4

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 364
    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_4

    .line 367
    aget-byte v4, p0, v2

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    add-int/lit8 v5, v2, 0x1

    .line 369
    array-length v6, p0

    const/4 v7, 0x1

    if-ge v5, v6, :cond_0

    .line 370
    aget-byte v5, p0, v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    shl-int/lit8 v4, v4, 0x8

    add-int/lit8 v6, v2, 0x2

    .line 374
    array-length v8, p0

    if-ge v6, v8, :cond_1

    .line 375
    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v4, v6

    goto :goto_2

    :cond_1
    const/4 v7, 0x0

    :goto_2
    add-int/lit8 v6, v3, 0x3

    .line 378
    sget-object v8, Lcom/baidu/mobads/h/a;->h:[C

    const/16 v9, 0x40

    if-eqz v7, :cond_2

    and-int/lit8 v7, v4, 0x3f

    rsub-int/lit8 v7, v7, 0x3f

    goto :goto_3

    :cond_2
    const/16 v7, 0x40

    :goto_3
    aget-char v7, v8, v7

    aput-char v7, v0, v6

    shr-int/lit8 v4, v4, 0x6

    add-int/lit8 v6, v3, 0x2

    .line 380
    sget-object v7, Lcom/baidu/mobads/h/a;->h:[C

    if-eqz v5, :cond_3

    and-int/lit8 v5, v4, 0x3f

    rsub-int/lit8 v9, v5, 0x3f

    :cond_3
    aget-char v5, v7, v9

    aput-char v5, v0, v6

    shr-int/lit8 v4, v4, 0x6

    add-int/lit8 v5, v3, 0x1

    .line 382
    sget-object v6, Lcom/baidu/mobads/h/a;->h:[C

    and-int/lit8 v7, v4, 0x3f

    rsub-int/lit8 v7, v7, 0x3f

    aget-char v6, v6, v7

    aput-char v6, v0, v5

    shr-int/lit8 v4, v4, 0x6

    add-int/lit8 v5, v3, 0x0

    .line 384
    sget-object v6, Lcom/baidu/mobads/h/a;->h:[C

    and-int/lit8 v4, v4, 0x3f

    rsub-int/lit8 v4, v4, 0x3f

    aget-char v4, v6, v4

    aput-char v4, v0, v5

    add-int/lit8 v2, v2, 0x3

    add-int/lit8 v3, v3, 0x4

    goto :goto_0

    .line 386
    :cond_4
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>([C)V

    return-object p0
.end method

.method private a(Landroid/telephony/CellLocation;)V
    .locals 7

    if-eqz p1, :cond_c

    .line 104
    iget-object v0, p0, Lcom/baidu/mobads/h/a;->a:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 107
    :cond_0
    new-instance v0, Lcom/baidu/mobads/h/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobads/h/a$a;-><init>(Lcom/baidu/mobads/h/a;Lcom/baidu/mobads/h/b;)V

    .line 108
    iget-object v2, p0, Lcom/baidu/mobads/h/a;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_6

    .line 109
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_6

    .line 114
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-lt v4, v5, :cond_2

    .line 115
    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-gez v4, :cond_1

    .line 116
    iget-object v4, p0, Lcom/baidu/mobads/h/a;->b:Lcom/baidu/mobads/h/a$a;

    iget v4, v4, Lcom/baidu/mobads/h/a$a;->c:I

    :cond_1
    iput v4, v0, Lcom/baidu/mobads/h/a$a;->c:I

    .line 118
    :cond_2
    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 120
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    const/4 v5, 0x0

    .line 121
    :goto_0
    array-length v6, v4

    if-ge v5, v6, :cond_4

    .line 122
    aget-char v6, v4, v5

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_3

    goto :goto_1

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 126
    :cond_4
    :goto_1
    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gez v2, :cond_5

    .line 127
    iget-object v2, p0, Lcom/baidu/mobads/h/a;->b:Lcom/baidu/mobads/h/a$a;

    iget v2, v2, Lcom/baidu/mobads/h/a$a;->d:I

    :cond_5
    iput v2, v0, Lcom/baidu/mobads/h/a$a;->d:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :catch_0
    :cond_6
    instance-of v2, p1, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v2, :cond_7

    .line 137
    check-cast p1, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual {p1}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v1

    iput v1, v0, Lcom/baidu/mobads/h/a$a;->a:I

    .line 138
    invoke-virtual {p1}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result p1

    iput p1, v0, Lcom/baidu/mobads/h/a$a;->b:I

    const/16 p1, 0x67

    .line 139
    iput-char p1, v0, Lcom/baidu/mobads/h/a$a;->e:C

    goto/16 :goto_3

    .line 140
    :cond_7
    instance-of v2, p1, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v2, :cond_a

    const/16 v2, 0x77

    .line 141
    iput-char v2, v0, Lcom/baidu/mobads/h/a$a;->e:C

    .line 142
    sget-object v2, Lcom/baidu/mobads/h/a;->f:Ljava/lang/Class;

    if-nez v2, :cond_8

    :try_start_1
    const-string v2, "android.telephony.cdma.CdmaCellLocation"

    .line 144
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lcom/baidu/mobads/h/a;->f:Ljava/lang/Class;

    .line 145
    sget-object v2, Lcom/baidu/mobads/h/a;->f:Ljava/lang/Class;

    const-string v4, "getBaseStationId"

    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/baidu/mobads/h/a;->c:Ljava/lang/reflect/Method;

    .line 146
    sget-object v2, Lcom/baidu/mobads/h/a;->f:Ljava/lang/Class;

    const-string v4, "getNetworkId"

    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/baidu/mobads/h/a;->d:Ljava/lang/reflect/Method;

    .line 147
    sget-object v2, Lcom/baidu/mobads/h/a;->f:Ljava/lang/Class;

    const-string v4, "getSystemId"

    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/baidu/mobads/h/a;->e:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 149
    :catch_1
    sput-object v1, Lcom/baidu/mobads/h/a;->f:Ljava/lang/Class;

    return-void

    .line 153
    :cond_8
    :goto_2
    sget-object v1, Lcom/baidu/mobads/h/a;->f:Ljava/lang/Class;

    if-eqz v1, :cond_a

    sget-object v1, Lcom/baidu/mobads/h/a;->f:Ljava/lang/Class;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 156
    :try_start_2
    sget-object v1, Lcom/baidu/mobads/h/a;->e:Ljava/lang/reflect/Method;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 157
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gez v1, :cond_9

    .line 158
    iget-object v1, p0, Lcom/baidu/mobads/h/a;->b:Lcom/baidu/mobads/h/a$a;

    iget v1, v1, Lcom/baidu/mobads/h/a$a;->d:I

    :cond_9
    iput v1, v0, Lcom/baidu/mobads/h/a$a;->d:I

    .line 159
    sget-object v1, Lcom/baidu/mobads/h/a;->c:Ljava/lang/reflect/Method;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 160
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/baidu/mobads/h/a$a;->b:I

    .line 161
    sget-object v1, Lcom/baidu/mobads/h/a;->d:Ljava/lang/reflect/Method;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 162
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, v0, Lcom/baidu/mobads/h/a$a;->a:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    :catch_2
    return-void

    .line 169
    :cond_a
    :goto_3
    invoke-static {v0}, Lcom/baidu/mobads/h/a$a;->a(Lcom/baidu/mobads/h/a$a;)Z

    move-result p1

    if-eqz p1, :cond_b

    .line 170
    iput-object v0, p0, Lcom/baidu/mobads/h/a;->b:Lcom/baidu/mobads/h/a$a;

    :cond_b
    return-void

    :cond_c
    :goto_4
    return-void
.end method

.method private b(I)I
    .locals 1

    const v0, 0x7fffffff

    if-ne p1, v0, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    return p1
.end method

.method private b()Lcom/baidu/mobads/h/a$a;
    .locals 5

    .line 176
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x11

    if-ge v0, v2, :cond_0

    return-object v1

    .line 181
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/h/a;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 182
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 184
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_2

    move-object v2, v1

    :cond_1
    :goto_0
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/CellInfo;

    .line 185
    invoke-virtual {v3}, Landroid/telephony/CellInfo;->isRegistered()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 186
    invoke-direct {p0, v3}, Lcom/baidu/mobads/h/a;->a(Landroid/telephony/CellInfo;)Lcom/baidu/mobads/h/a$a;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v3, :cond_2

    move-object v2, v3

    goto :goto_0

    .line 190
    :cond_2
    :try_start_2
    invoke-static {v3}, Lcom/baidu/mobads/h/a$a;->a(Lcom/baidu/mobads/h/a$a;)Z

    move-result v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_2 .. :try_end_2} :catch_2

    if-nez v0, :cond_3

    goto :goto_1

    :cond_3
    move-object v1, v3

    :goto_1
    return-object v1

    :catch_0
    move-object v1, v3

    goto :goto_2

    :catch_1
    :cond_4
    move-object v1, v2

    :catch_2
    :cond_5
    :goto_2
    return-object v1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "permission_location"

    .line 56
    invoke-static {v0}, Lcom/baidu/mobads/MobadsPermissionSettings;->hasPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    const/16 v0, 0xa

    .line 60
    :try_start_0
    invoke-direct {p0, v0}, Lcom/baidu/mobads/h/a;->a(I)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method
