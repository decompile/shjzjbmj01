.class public Lcom/baidu/mobads/AppActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;
    }
.end annotation


# static fields
.field private static a:Z

.field public static activityName:Ljava/lang/String;

.field private static c:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;


# instance fields
.field private b:Lcom/baidu/mobads/AppActivityImp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 157
    sget-object v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->ACTION_BAR_WHITE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    sput-object v0, Lcom/baidu/mobads/AppActivity;->c:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 60
    new-instance v0, Lcom/baidu/mobads/AppActivityImp;

    invoke-direct {v0}, Lcom/baidu/mobads/AppActivityImp;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    return-void
.end method

.method public static canLpShowWhenLocked(Z)V
    .locals 0

    .line 179
    sput-boolean p0, Lcom/baidu/mobads/AppActivity;->a:Z

    .line 180
    invoke-static {p0}, Lcom/baidu/mobads/AppActivityImp;->canLpShowWhenLocked(Z)V

    return-void
.end method

.method public static getActionBarColorTheme()Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;
    .locals 1

    .line 160
    sget-object v0, Lcom/baidu/mobads/AppActivity;->c:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    return-object v0
.end method

.method public static getActivityClass()Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 48
    const-class v0, Lcom/baidu/mobads/AppActivity;

    .line 49
    sget-object v1, Lcom/baidu/mobads/AppActivity;->activityName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/AppActivity;->activityName:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 53
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-object v0
.end method

.method public static getLpShowWhenLocked()Z
    .locals 1

    .line 184
    sget-boolean v0, Lcom/baidu/mobads/AppActivity;->a:Z

    return v0
.end method

.method public static isAnti()Z
    .locals 1

    .line 44
    sget-object v0, Lcom/baidu/mobads/AppActivity;->activityName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static setActionBarColor(IIII)V
    .locals 1

    .line 171
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(IIII)V

    sput-object v0, Lcom/baidu/mobads/AppActivity;->c:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    return-void
.end method

.method public static setActionBarColorTheme(Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;)V
    .locals 1

    if-eqz p0, :cond_0

    .line 165
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;)V

    sput-object v0, Lcom/baidu/mobads/AppActivity;->c:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    :cond_0
    return-void
.end method

.method public static setActivityName(Ljava/lang/String;)V
    .locals 0

    .line 35
    sput-object p0, Lcom/baidu/mobads/AppActivity;->activityName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 192
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 200
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 208
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onActivityResult(IILandroid/content/Intent;)V

    .line 213
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method protected onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    .line 218
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    return-void
.end method

.method protected onChildTitleChanged(Landroid/app/Activity;Ljava/lang/CharSequence;)V
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onChildTitleChanged(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    .line 223
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onChildTitleChanged(Landroid/app/Activity;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 228
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onContentChanged()V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onContentChanged()V

    .line 233
    invoke-super {p0}, Landroid/app/Activity;->onContentChanged()V

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 241
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onContextMenuClosed(Landroid/view/Menu;)V
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onContextMenuClosed(Landroid/view/Menu;)V

    .line 246
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextMenuClosed(Landroid/view/Menu;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 252
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 253
    invoke-virtual {p0}, Lcom/baidu/mobads/AppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 255
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v1, p0}, Lcom/baidu/mobads/AppActivityImp;->setActivity(Landroid/app/Activity;)V

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onCreate(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 260
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .line 265
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 266
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    return-void
.end method

.method public onCreateDescription()Ljava/lang/CharSequence;
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onCreateDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 274
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onCreateDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 282
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 290
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 298
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 306
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreatePanelView(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onCreateThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;)Z
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onCreateThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 314
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreateThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;)Z

    move-result p1

    return p1
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 322
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method protected onDestroy()V
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onDestroy()V

    .line 327
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 335
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 343
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 347
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 351
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onLowMemory()V
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onLowMemory()V

    .line 356
    invoke-super {p0}, Landroid/app/Activity;->onLowMemory()V

    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 364
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 372
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 376
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onNewIntent(Landroid/content/Intent;)V

    .line 377
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 385
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onOptionsMenuClosed(Landroid/view/Menu;)V

    .line 390
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsMenuClosed(Landroid/view/Menu;)V

    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onPanelClosed(ILandroid/view/Menu;)V

    .line 395
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPanelClosed(ILandroid/view/Menu;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 399
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onPause()V

    .line 400
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 404
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 405
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onPostCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .line 409
    invoke-super {p0}, Landroid/app/Activity;->onPostResume()V

    .line 410
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onPostResume()V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1

    .line 414
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 415
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onPrepareDialog(ILandroid/app/Dialog;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .line 419
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 423
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .line 427
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/baidu/mobads/AppActivityImp;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 431
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onRestart()V
    .locals 1

    .line 435
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 436
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onRestart()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 440
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 441
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 445
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 446
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onResume()V

    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .line 450
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 454
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 458
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 459
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .line 463
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onSearchRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 467
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 1

    .line 471
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 472
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    .line 476
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onStop()V

    .line 477
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .line 481
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 482
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/AppActivityImp;->onTitleChanged(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 486
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 490
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 494
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 498
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onUserInteraction()V
    .locals 1

    .line 502
    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    .line 503
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onUserInteraction()V

    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .line 507
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 508
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0}, Lcom/baidu/mobads/AppActivityImp;->onUserLeaveHint()V

    return-void
.end method

.method public onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
    .locals 1

    .line 512
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    .line 513
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 517
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 518
    iget-object v0, p0, Lcom/baidu/mobads/AppActivity;->b:Lcom/baidu/mobads/AppActivityImp;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/AppActivityImp;->onWindowFocusChanged(Z)V

    return-void
.end method
