.class public Lcom/baidu/mobads/InterstitialAd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final TAG:Ljava/lang/String; = "InterstitialAd"


# instance fields
.field a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

.field private b:Lcom/baidu/mobads/AdSize;

.field private c:Lcom/baidu/mobads/production/e/a;

.field private final d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private e:Lcom/baidu/mobads/InterstitialAdListener;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/AdSize;Ljava/lang/String;)V
    .locals 7

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 32
    new-instance v0, Lcom/baidu/mobads/n;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/n;-><init>(Lcom/baidu/mobads/InterstitialAd;)V

    iput-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->e:Lcom/baidu/mobads/InterstitialAdListener;

    .line 56
    new-instance v0, Lcom/baidu/mobads/o;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/o;-><init>(Lcom/baidu/mobads/InterstitialAd;)V

    iput-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    .line 101
    new-instance v3, Lcom/baidu/mobads/component/XAdView;

    invoke-direct {v3, p1}, Lcom/baidu/mobads/component/XAdView;-><init>(Landroid/content/Context;)V

    .line 102
    new-instance v0, Lcom/baidu/mobads/q;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/q;-><init>(Lcom/baidu/mobads/InterstitialAd;)V

    invoke-virtual {v3, v0}, Lcom/baidu/mobads/component/XAdView;->setListener(Lcom/baidu/mobads/component/XAdView$Listener;)V

    .line 131
    iput-object p2, p0, Lcom/baidu/mobads/InterstitialAd;->b:Lcom/baidu/mobads/AdSize;

    .line 132
    invoke-direct {p0}, Lcom/baidu/mobads/InterstitialAd;->a()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 133
    new-instance p2, Lcom/baidu/mobads/production/e/b;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p2, p1, v3, v0, p3}, Lcom/baidu/mobads/production/e/b;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/Boolean;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    goto :goto_0

    .line 134
    :cond_0
    invoke-direct {p0}, Lcom/baidu/mobads/InterstitialAd;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    new-instance v0, Lcom/baidu/mobads/production/d/b;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object v1, v0

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/baidu/mobads/production/d/b;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/Boolean;Lcom/baidu/mobads/AdSize;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    .line 137
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    const-string p2, "AdLoaded"

    iget-object p3, p0, Lcom/baidu/mobads/InterstitialAd;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-interface {p1, p2, p3}, Lcom/baidu/mobads/production/e/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 138
    iget-object p1, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    const-string p2, "AdError"

    iget-object p3, p0, Lcom/baidu/mobads/InterstitialAd;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-interface {p1, p2, p3}, Lcom/baidu/mobads/production/e/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 139
    iget-object p1, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    const-string p2, "AdStopped"

    iget-object p3, p0, Lcom/baidu/mobads/InterstitialAd;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-interface {p1, p2, p3}, Lcom/baidu/mobads/production/e/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 140
    iget-object p1, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    const-string p2, "AdUserClose"

    iget-object p3, p0, Lcom/baidu/mobads/InterstitialAd;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-interface {p1, p2, p3}, Lcom/baidu/mobads/production/e/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 141
    iget-object p1, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    const-string p2, "AdStarted"

    iget-object p3, p0, Lcom/baidu/mobads/InterstitialAd;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-interface {p1, p2, p3}, Lcom/baidu/mobads/production/e/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 142
    iget-object p1, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    const-string p2, "AdUserClick"

    iget-object p3, p0, Lcom/baidu/mobads/InterstitialAd;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-interface {p1, p2, p3}, Lcom/baidu/mobads/production/e/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 144
    iget-object p1, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    invoke-interface {p1}, Lcom/baidu/mobads/production/e/a;->request()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .line 90
    sget-object v0, Lcom/baidu/mobads/AdSize;->InterstitialGame:Lcom/baidu/mobads/AdSize;

    invoke-direct {p0, p1, v0, p2}, Lcom/baidu/mobads/InterstitialAd;-><init>(Landroid/content/Context;Lcom/baidu/mobads/AdSize;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/InterstitialAd;)Lcom/baidu/mobads/InterstitialAdListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/baidu/mobads/InterstitialAd;->e:Lcom/baidu/mobads/InterstitialAdListener;

    return-object p0
.end method

.method private a()Z
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->b:Lcom/baidu/mobads/AdSize;

    invoke-virtual {v0}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v0

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialOther:Lcom/baidu/mobads/AdSize;

    invoke-virtual {v1}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v1

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->b:Lcom/baidu/mobads/AdSize;

    .line 149
    invoke-virtual {v0}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v0

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialGame:Lcom/baidu/mobads/AdSize;

    invoke-virtual {v1}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic b(Lcom/baidu/mobads/InterstitialAd;)Lcom/baidu/mobads/production/e/a;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    return-object p0
.end method

.method private b()Z
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->b:Lcom/baidu/mobads/AdSize;

    invoke-virtual {v0}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v0

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialForVideoBeforePlay:Lcom/baidu/mobads/AdSize;

    invoke-virtual {v1}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->b:Lcom/baidu/mobads/AdSize;

    .line 154
    invoke-virtual {v0}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v0

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialForVideoPausePlay:Lcom/baidu/mobads/AdSize;

    invoke-virtual {v1}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static setAppSec(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public static setAppSid(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 214
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/h;->setAppId(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    invoke-interface {v0}, Lcom/baidu/mobads/production/e/a;->p()V

    return-void
.end method

.method public isAdReady()Z
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    invoke-interface {v0}, Lcom/baidu/mobads/production/e/a;->v()Z

    move-result v0

    return v0
.end method

.method public loadAd()V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    invoke-interface {v0}, Lcom/baidu/mobads/production/e/a;->a()V

    return-void
.end method

.method public loadAdForVideoApp(II)V
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/production/e/a;->a(II)V

    return-void
.end method

.method public setListener(Lcom/baidu/mobads/InterstitialAdListener;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 193
    iput-object p1, p0, Lcom/baidu/mobads/InterstitialAd;->e:Lcom/baidu/mobads/InterstitialAdListener;

    return-void

    .line 191
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    invoke-interface {v0, p1}, Lcom/baidu/mobads/production/e/a;->a(Landroid/app/Activity;)V

    return-void
.end method

.method public showAdInParentForVideoApp(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 247
    iget-object v0, p0, Lcom/baidu/mobads/InterstitialAd;->c:Lcom/baidu/mobads/production/e/a;

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/production/e/a;->a(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V

    return-void

    .line 245
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method
