.class Lcom/baidu/mobads/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

.field final synthetic b:Lcom/baidu/mobads/o;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/o;Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/baidu/mobads/p;->b:Lcom/baidu/mobads/o;

    iput-object p2, p0, Lcom/baidu/mobads/p;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const-string v0, "AdLoaded"

    .line 64
    iget-object v1, p0, Lcom/baidu/mobads/p;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/baidu/mobads/p;->b:Lcom/baidu/mobads/o;

    iget-object v0, v0, Lcom/baidu/mobads/o;->a:Lcom/baidu/mobads/InterstitialAd;

    invoke-static {v0}, Lcom/baidu/mobads/InterstitialAd;->a(Lcom/baidu/mobads/InterstitialAd;)Lcom/baidu/mobads/InterstitialAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/InterstitialAdListener;->onAdReady()V

    goto/16 :goto_0

    :cond_0
    const-string v0, "AdError"

    .line 66
    iget-object v1, p0, Lcom/baidu/mobads/p;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/baidu/mobads/p;->b:Lcom/baidu/mobads/o;

    iget-object v0, v0, Lcom/baidu/mobads/o;->a:Lcom/baidu/mobads/InterstitialAd;

    invoke-static {v0}, Lcom/baidu/mobads/InterstitialAd;->a(Lcom/baidu/mobads/InterstitialAd;)Lcom/baidu/mobads/InterstitialAdListener;

    move-result-object v0

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/p;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    .line 68
    invoke-interface {v2}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->getMessage(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-interface {v0, v1}, Lcom/baidu/mobads/InterstitialAdListener;->onAdFailed(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "AdStopped"

    .line 69
    iget-object v1, p0, Lcom/baidu/mobads/p;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    iget-object v0, p0, Lcom/baidu/mobads/p;->b:Lcom/baidu/mobads/o;

    iget-object v0, v0, Lcom/baidu/mobads/o;->a:Lcom/baidu/mobads/InterstitialAd;

    invoke-static {v0}, Lcom/baidu/mobads/InterstitialAd;->a(Lcom/baidu/mobads/InterstitialAd;)Lcom/baidu/mobads/InterstitialAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/InterstitialAdListener;->onAdDismissed()V

    goto :goto_0

    :cond_2
    const-string v0, "AdStarted"

    .line 71
    iget-object v1, p0, Lcom/baidu/mobads/p;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 72
    iget-object v0, p0, Lcom/baidu/mobads/p;->b:Lcom/baidu/mobads/o;

    iget-object v0, v0, Lcom/baidu/mobads/o;->a:Lcom/baidu/mobads/InterstitialAd;

    invoke-static {v0}, Lcom/baidu/mobads/InterstitialAd;->a(Lcom/baidu/mobads/InterstitialAd;)Lcom/baidu/mobads/InterstitialAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/InterstitialAdListener;->onAdPresent()V

    goto :goto_0

    :cond_3
    const-string v0, "AdUserClick"

    .line 73
    iget-object v1, p0, Lcom/baidu/mobads/p;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/p;->b:Lcom/baidu/mobads/o;

    iget-object v0, v0, Lcom/baidu/mobads/o;->a:Lcom/baidu/mobads/InterstitialAd;

    invoke-static {v0}, Lcom/baidu/mobads/InterstitialAd;->a(Lcom/baidu/mobads/InterstitialAd;)Lcom/baidu/mobads/InterstitialAdListener;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/p;->b:Lcom/baidu/mobads/o;

    iget-object v1, v1, Lcom/baidu/mobads/o;->a:Lcom/baidu/mobads/InterstitialAd;

    invoke-interface {v0, v1}, Lcom/baidu/mobads/InterstitialAdListener;->onAdClick(Lcom/baidu/mobads/InterstitialAd;)V

    :cond_4
    :goto_0
    return-void
.end method
