.class Lcom/baidu/mobads/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/component/XAdView$Listener;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/baidu/mobads/component/XAdView;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Z

.field final synthetic e:I

.field final synthetic f:Lcom/baidu/mobad/feeds/RequestParameters;

.field final synthetic g:Lcom/baidu/mobads/SplashAd;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/SplashAd;Landroid/content/Context;Lcom/baidu/mobads/component/XAdView;Ljava/lang/String;ZILcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    iput-object p2, p0, Lcom/baidu/mobads/v;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/baidu/mobads/v;->b:Lcom/baidu/mobads/component/XAdView;

    iput-object p4, p0, Lcom/baidu/mobads/v;->c:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/baidu/mobads/v;->d:Z

    iput p6, p0, Lcom/baidu/mobads/v;->e:I

    iput-object p7, p0, Lcom/baidu/mobads/v;->f:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 1

    .line 371
    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->n()V

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .line 364
    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->o()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onLayoutComplete(II)V
    .locals 10

    .line 313
    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    if-eqz v0, :cond_0

    return-void

    .line 316
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v1}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->getScreenDensity(Landroid/content/Context;)F

    move-result v0

    .line 319
    iget-object v1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v1}, Lcom/baidu/mobads/SplashAd;->e(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 320
    iget-object v1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v1}, Lcom/baidu/mobads/SplashAd;->e(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobad/feeds/RequestParameters;->isCustomSize()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 321
    iget-object v1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v1}, Lcom/baidu/mobads/SplashAd;->e(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobad/feeds/RequestParameters;->getWidth()I

    move-result v1

    if-lez v1, :cond_1

    .line 322
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->e(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getWidth()I

    move-result p1

    int-to-float p1, p1

    mul-float p1, p1, v0

    float-to-int p1, p1

    .line 324
    :cond_1
    iget-object v1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v1}, Lcom/baidu/mobads/SplashAd;->e(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobad/feeds/RequestParameters;->getHeight()I

    move-result v1

    if-lez v1, :cond_2

    .line 325
    iget-object p2, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p2}, Lcom/baidu/mobads/SplashAd;->e(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobad/feeds/RequestParameters;->getHeight()I

    move-result p2

    int-to-float p2, p2

    mul-float p2, p2, v0

    float-to-int p2, p2

    :cond_2
    move v6, p1

    move v7, p2

    int-to-float p1, v6

    const/high16 p2, 0x43480000    # 200.0f

    mul-float p2, p2, v0

    cmpg-float p1, p1, p2

    if-ltz p1, :cond_5

    int-to-float p1, v7

    const/high16 p2, 0x43160000    # 150.0f

    mul-float v0, v0, p2

    cmpg-float p1, p1, v0

    if-gez p1, :cond_3

    goto/16 :goto_0

    .line 338
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    new-instance p2, Lcom/baidu/mobads/production/f/a;

    iget-object v2, p0, Lcom/baidu/mobads/v;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/baidu/mobads/v;->b:Lcom/baidu/mobads/component/XAdView;

    iget-object v4, p0, Lcom/baidu/mobads/v;->c:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/baidu/mobads/v;->d:Z

    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    .line 340
    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->f(Lcom/baidu/mobads/SplashAd;)I

    move-result v8

    iget v9, p0, Lcom/baidu/mobads/v;->e:I

    move-object v1, p2

    invoke-direct/range {v1 .. v9}, Lcom/baidu/mobads/production/f/a;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;ZIIII)V

    .line 338
    invoke-static {p1, p2}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;Lcom/baidu/mobads/production/f/a;)Lcom/baidu/mobads/production/f/a;

    .line 343
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    iget-object p2, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p2}, Lcom/baidu/mobads/SplashAd;->g(Lcom/baidu/mobads/SplashAd;)Ljava/util/HashMap;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/f/a;->a(Ljava/util/HashMap;)V

    .line 345
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    const/4 p2, 0x0

    iput-boolean p2, p1, Lcom/baidu/mobads/production/f/a;->A:Z

    .line 346
    iget-object p1, p0, Lcom/baidu/mobads/v;->f:Lcom/baidu/mobad/feeds/RequestParameters;

    if-eqz p1, :cond_4

    .line 347
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    iget-object p2, p0, Lcom/baidu/mobads/v;->f:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/f/a;->a(Lcom/baidu/mobad/feeds/RequestParameters;)V

    .line 349
    :cond_4
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    const-string p2, "AdUserClick"

    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 350
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    const-string p2, "AdLoaded"

    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 351
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    const-string p2, "AdStarted"

    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 352
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    const-string p2, "AdStopped"

    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 355
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    const-string p2, "AdError"

    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 357
    invoke-static {}, Lcom/baidu/mobads/j;->a()Lcom/baidu/mobads/j;

    move-result-object p1

    const/16 p2, 0x3ea

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/j;->a(I)V

    .line 358
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/production/f/a;->request()V

    return-void

    .line 331
    :cond_5
    :goto_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object p1

    .line 332
    sget-object p2, Lcom/baidu/mobads/interfaces/error/XAdErrorCode;->SHOW_STANDARD_UNFIT:Lcom/baidu/mobads/interfaces/error/XAdErrorCode;

    const-string v0, "\u5f00\u5c4f\u663e\u793a\u533a\u57df\u592a\u5c0f,\u5bbd\u5ea6\u81f3\u5c11200dp,\u9ad8\u5ea6\u81f3\u5c11150dp"

    invoke-interface {p1, p2, v0}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->genCompleteErrorMessage(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 334
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    .line 335
    iget-object p1, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/SplashAdListener;->onAdDismissed()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 305
    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/f/a;->a(Z)V

    :cond_0
    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/baidu/mobads/v;->g:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/f/a;->b(I)V

    :cond_0
    return-void
.end method
