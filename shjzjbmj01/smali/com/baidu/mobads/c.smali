.class Lcom/baidu/mobads/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/component/XAdView$Listener;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/AdView;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/AdView;)V
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/baidu/mobads/c;->a:Lcom/baidu/mobads/AdView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/baidu/mobads/c;->a:Lcom/baidu/mobads/AdView;

    invoke-static {v0}, Lcom/baidu/mobads/AdView;->c(Lcom/baidu/mobads/AdView;)V

    .line 142
    iget-object v0, p0, Lcom/baidu/mobads/c;->a:Lcom/baidu/mobads/AdView;

    invoke-static {v0}, Lcom/baidu/mobads/AdView;->b(Lcom/baidu/mobads/AdView;)Lcom/baidu/mobads/production/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/a/a;->n()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/baidu/mobads/c;->a:Lcom/baidu/mobads/AdView;

    invoke-static {v0}, Lcom/baidu/mobads/AdView;->b(Lcom/baidu/mobads/AdView;)Lcom/baidu/mobads/production/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/a/a;->o()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/baidu/mobads/c;->a:Lcom/baidu/mobads/AdView;

    invoke-static {v0}, Lcom/baidu/mobads/AdView;->b(Lcom/baidu/mobads/AdView;)Lcom/baidu/mobads/production/a/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/production/a/a;->a(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onLayoutComplete(II)V
    .locals 0

    .line 130
    iget-object p1, p0, Lcom/baidu/mobads/c;->a:Lcom/baidu/mobads/AdView;

    invoke-static {p1}, Lcom/baidu/mobads/AdView;->c(Lcom/baidu/mobads/AdView;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/baidu/mobads/c;->a:Lcom/baidu/mobads/AdView;

    invoke-static {v0}, Lcom/baidu/mobads/AdView;->b(Lcom/baidu/mobads/AdView;)Lcom/baidu/mobads/production/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/a/a;->a(Z)V

    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/c;->a:Lcom/baidu/mobads/AdView;

    invoke-static {v0}, Lcom/baidu/mobads/AdView;->b(Lcom/baidu/mobads/AdView;)Lcom/baidu/mobads/production/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/a/a;->b(I)V

    return-void
.end method
