.class public Lcom/baidu/mobads/CpuInfoManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/CpuInfoManager$UrlListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Runnable;)V
    .locals 2

    .line 115
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 116
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/m;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/m;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    goto :goto_0

    .line 124
    :cond_0
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method

.method public static getCpuInfoUrl(Landroid/content/Context;Ljava/lang/String;ILcom/baidu/mobads/CpuInfoManager$UrlListener;)V
    .locals 1

    .line 61
    new-instance v0, Lcom/baidu/mobads/k;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/baidu/mobads/k;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/baidu/mobads/CpuInfoManager$UrlListener;)V

    invoke-static {v0}, Lcom/baidu/mobads/CpuInfoManager;->a(Ljava/lang/Runnable;)V

    return-void
.end method
