.class public Lcom/baidu/mobads/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    .line 16
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0100000"

    const-string v2, "\u8bf7\u6c42\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0101000"

    const-string v2, "\u5e7f\u544a\u914d\u7f6e\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0101001"

    const-string v2, "\u5e7f\u544a\u914d\u7f6e\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0101002"

    const-string v2, "\u5e7f\u544a\u914d\u7f6e\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0101003"

    const-string v2, "\u5e7f\u544a\u914d\u7f6e\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0101004"

    const-string v2, "\u5e7f\u544a\u914d\u7f6e\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0101005"

    const-string v2, "\u5e7f\u544a\u914d\u7f6e\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0103000"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0103010"

    const-string v2, "APPSID\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0103011"

    const-string v2, "APPSID\u5931\u6548\u6216\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0103012"

    const-string v2, "APPSID\u5931\u6548\u6216\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0103020"

    const-string v2, "APPSID\u5931\u6548\u6216\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0103030"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0103040"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0103050"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0103060"

    const-string v2, "\u5e94\u7528\u5305\u540d\u4fe1\u606f\u9519\u8bef\uff0c\u8bf7\u4fdd\u8bc1\u6ce8\u518c\u5305\u540d\u548c\u5b9e\u9645\u8bf7\u6c42\u5305\u540d\u4e00\u81f4"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104000"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104010"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104011"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104020"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104021"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104030"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104040"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104050"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104060"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104070"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104071"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104080"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104081"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104090"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104100"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0104110"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105000"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105010"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105011"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105020"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105021"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105030"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105031"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105040"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105041"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105050"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105060"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0105070"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0106000"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0106001"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0106010"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0106020"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0106030"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107000"

    const-string v2, "\u5e7f\u544a\u4f4dID\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107001"

    const-string v2, "\u5e7f\u544a\u4f4dID\u9519\u8bef\u6216\u5931\u6548"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107002"

    const-string v2, "\u5e7f\u544a\u4f4dID\u9519\u8bef\u6216\u5931\u6548"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107003"

    const-string v2, "\u5e7f\u544a\u4f4dID\u4e0eAPPSID\u4e0d\u5339\u914d"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107010"

    const-string v2, "\u5e7f\u544a\u4f4d\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107020"

    const-string v2, "\u5e7f\u544a\u4f4d\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107030"

    const-string v2, "\u5e7f\u544a\u4f4d\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107040"

    const-string v2, "\u5e7f\u544a\u4f4d\u4fe1\u606f\u7f3a\u5931"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107050"

    const-string v2, "\u89c6\u9891\u65e0\u6cd5\u64ad\u653e"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107051"

    const-string v2, "\u89c6\u9891\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0107052"

    const-string v2, "\u5e7f\u544a\u4f4d\u5c3a\u5bf8\u548cMSSP\u6ce8\u518c\u5c3a\u5bf8\u4e0d\u7b26"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0200000"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201000"

    const-string v2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201010"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201020"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201021"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201030"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201031"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201040"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201050"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201060"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201070"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201080"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201090"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201100"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    const-string v1, "0201110"

    const-string v2, "\u5e7f\u544a\u4fe1\u606f\u9519\u8bef"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 102
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, ""

    return-object p0

    .line 106
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 109
    :cond_1
    sget-object v0, Lcom/baidu/mobads/d/a;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    if-nez p0, :cond_2

    const-string p0, ""

    :cond_2
    return-object p0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 117
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    .line 121
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_1
    return-object p0
.end method
