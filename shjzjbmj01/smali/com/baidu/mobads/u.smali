.class Lcom/baidu/mobads/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

.field final synthetic b:Lcom/baidu/mobads/t;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/t;Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iput-object p2, p0, Lcom/baidu/mobads/u;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "AdLoaded"

    .line 123
    iget-object v1, p0, Lcom/baidu/mobads/u;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/SplashAdListener;->onADLoaded()V

    .line 126
    invoke-static {}, Lcom/baidu/mobads/j;->a()Lcom/baidu/mobads/j;

    move-result-object v0

    const/16 v1, 0x3ed

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/j;->a(I)V

    goto/16 :goto_0

    :cond_1
    const-string v0, "AdStarted"

    .line 127
    iget-object v1, p0, Lcom/baidu/mobads/u;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    invoke-static {}, Lcom/baidu/mobads/j;->a()Lcom/baidu/mobads/j;

    move-result-object v0

    const/16 v1, 0x3ee

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/j;->a(I)V

    .line 129
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/SplashAdListener;->onAdPresent()V

    .line 130
    invoke-static {}, Lcom/baidu/mobads/j;->a()Lcom/baidu/mobads/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/j;->b()V

    goto/16 :goto_0

    :cond_2
    const-string v0, "AdUserClick"

    .line 131
    iget-object v1, p0, Lcom/baidu/mobads/u;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 132
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/SplashAdListener;->onAdClick()V

    .line 133
    iget-object v0, p0, Lcom/baidu/mobads/u;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v0}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    const-string v1, "actionType"

    .line 136
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Integer;

    .line 138
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobads/SplashLpCloseListener;

    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    .line 139
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 140
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/command/b/a;->b()V

    .line 141
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object v0

    const-string v1, "AdLpClosed"

    iget-object v2, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v2, v2, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v2}, Lcom/baidu/mobads/SplashAd;->c(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/command/b/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    goto/16 :goto_0

    :cond_4
    const-string v0, "AdStopped"

    .line 143
    iget-object v1, p0, Lcom/baidu/mobads/u;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 145
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->removeAllListeners()V

    .line 146
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/SplashAdListener;->onAdDismissed()V

    goto/16 :goto_0

    :cond_5
    const-string v0, "AdError"

    .line 147
    iget-object v1, p0, Lcom/baidu/mobads/u;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 149
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->removeAllListeners()V

    .line 150
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/u;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    .line 151
    invoke-interface {v2}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->getMessage(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 150
    invoke-interface {v0, v1}, Lcom/baidu/mobads/SplashAdListener;->onAdFailed(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v0, "AdLpClosed"

    .line 152
    iget-object v1, p0, Lcom/baidu/mobads/u;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    .line 153
    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobads/SplashLpCloseListener;

    if-eqz v0, :cond_7

    .line 154
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object v0

    const-string v1, "AdLpClosed"

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/command/b/a;->removeEventListeners(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->b(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/command/b/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/command/b/a;->c()V

    .line 156
    iget-object v0, p0, Lcom/baidu/mobads/u;->b:Lcom/baidu/mobads/t;

    iget-object v0, v0, Lcom/baidu/mobads/t;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/SplashLpCloseListener;

    invoke-interface {v0}, Lcom/baidu/mobads/SplashLpCloseListener;->onLpClosed()V

    :cond_7
    :goto_0
    return-void
.end method
