.class public Lcom/baidu/mobads/vo/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXAdProdInfo;


# instance fields
.field private a:Lcom/baidu/mobads/vo/d;

.field private b:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field private c:Lorg/json/JSONObject;

.field private d:Landroid/view/View;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/vo/d;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V
    .locals 1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 22
    iput-boolean v0, p0, Lcom/baidu/mobads/vo/b;->e:Z

    .line 54
    iput-object p1, p0, Lcom/baidu/mobads/vo/b;->a:Lcom/baidu/mobads/vo/d;

    .line 55
    iput-object p2, p0, Lcom/baidu/mobads/vo/b;->b:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/baidu/mobads/vo/b;->c:Lorg/json/JSONObject;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 40
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/b;->e:Z

    return-void
.end method

.method public getAdPlacementId()Ljava/lang/String;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->a:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->getApid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAdRequestURL()Ljava/lang/String;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->a:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApt()I
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->a:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->getApt()I

    move-result v0

    return v0
.end method

.method public getAttribute()Lorg/json/JSONObject;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->c:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->c:Lorg/json/JSONObject;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/baidu/mobads/AdSettings;->getAttr()Lorg/json/JSONObject;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getClickView()Landroid/view/View;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->d:Landroid/view/View;

    return-object v0
.end method

.method public getInstanceCount()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getProdType()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->a:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->getProd()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestAdHeight()I
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->a:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->getH()I

    move-result v0

    return v0
.end method

.method public getRequestAdWidth()I
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->a:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->getW()I

    move-result v0

    return v0
.end method

.method public getType()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/baidu/mobads/vo/b;->b:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-object v0
.end method

.method public isAutoPlay()Z
    .locals 1

    .line 26
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/b;->e:Z

    return v0
.end method

.method public isMsspTagAvailable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setClickView(Landroid/view/View;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/baidu/mobads/vo/b;->d:Landroid/view/View;

    return-void
.end method
