.class public abstract Lcom/baidu/mobads/vo/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXAdRequestInfo;


# instance fields
.field private a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Landroid/content/Context;

.field protected e:Landroid/app/Activity;

.field protected f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field protected g:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

.field protected h:Lcom/baidu/mobads/interfaces/utils/IXAdConstants;

.field protected i:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

.field protected j:Z

.field protected k:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:I

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:I

.field private t:I

.field private u:Z

.field private v:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V
    .locals 2

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "TODO"

    .line 39
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->c:Ljava/lang/String;

    const-string v0, "android"

    .line 51
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->a:Ljava/lang/String;

    const-string v0, ""

    .line 466
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->l:Ljava/lang/String;

    .line 470
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/l;->getAdCreativeTypeImage()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/vo/d;->o:I

    const-string v0, "LP,DL"

    .line 471
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->p:Ljava/lang/String;

    const-string v0, ""

    .line 472
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->q:Ljava/lang/String;

    const/4 v0, 0x0

    .line 479
    iput v0, p0, Lcom/baidu/mobads/vo/d;->s:I

    const/4 v0, 0x1

    .line 483
    iput-boolean v0, p0, Lcom/baidu/mobads/vo/d;->u:Z

    .line 503
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/vo/d;->v:J

    const-string v0, ""

    .line 69
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->b:Ljava/lang/String;

    .line 70
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->h:Lcom/baidu/mobads/interfaces/utils/IXAdConstants;

    .line 71
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->i:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    .line 72
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getActivityUtils()Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->k:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    .line 78
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 79
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 81
    :goto_0
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->e:Landroid/app/Activity;

    if-nez v0, :cond_1

    move-object v0, p1

    goto :goto_1

    .line 82
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    .line 83
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->e:Landroid/app/Activity;

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 84
    iput-object p2, p0, Lcom/baidu/mobads/vo/d;->e:Landroid/app/Activity;

    .line 86
    :cond_2
    iget-object p2, p0, Lcom/baidu/mobads/vo/d;->k:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->e:Landroid/app/Activity;

    invoke-interface {p2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;->webviewMultiProcess(Landroid/app/Activity;)Z

    move-result p2

    iput-boolean p2, p0, Lcom/baidu/mobads/vo/d;->j:Z

    .line 87
    iput-object p3, p0, Lcom/baidu/mobads/vo/d;->f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 111
    new-instance p2, Lcom/baidu/mobads/vo/b;

    iget-object p3, p0, Lcom/baidu/mobads/vo/d;->f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p2, p0, p3}, Lcom/baidu/mobads/vo/b;-><init>(Lcom/baidu/mobads/vo/d;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p2, p0, Lcom/baidu/mobads/vo/d;->g:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    .line 112
    iget-object p2, p0, Lcom/baidu/mobads/vo/d;->f:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/vo/d;->c(Ljava/lang/String;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/baidu/mobads/vo/d;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    const-string v0, "0.0"

    .line 605
    sget-object v1, Lcom/baidu/mobads/constants/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    invoke-static {p1}, Lcom/baidu/mobads/g/g;->b(Landroid/content/Context;)D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double p1, v0, v2

    if-lez p1, :cond_0

    .line 608
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/baidu/mobads/constants/a;->c:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 500
    iput-object p1, p0, Lcom/baidu/mobads/vo/d;->a:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 492
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/d;->u:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .line 452
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->e()Ljava/util/HashMap;

    move-result-object v0

    .line 453
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->a()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 454
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/vo/d;->b:Ljava/lang/String;

    .line 455
    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->getRequestAdUrl(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 526
    iput-object p1, p0, Lcom/baidu/mobads/vo/d;->p:Ljava/lang/String;

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .line 534
    iput-object p1, p0, Lcom/baidu/mobads/vo/d;->q:Ljava/lang/String;

    return-void
.end method

.method public d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->g:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    return-object v0
.end method

.method public d(I)V
    .locals 0

    .line 510
    iput p1, p0, Lcom/baidu/mobads/vo/d;->m:I

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .line 593
    iput-object p1, p0, Lcom/baidu/mobads/vo/d;->l:Ljava/lang/String;

    return-void
.end method

.method protected e()Ljava/util/HashMap;
    .locals 14
    .annotation build Landroid/annotation/TargetApi;
        value = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 135
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    .line 136
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    .line 137
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    :try_start_0
    const-string v3, "net"

    .line 140
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetworkCatagory(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "n"

    .line 143
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getN()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "at"

    .line 155
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getAt()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "v"

    .line 159
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v5, Lcom/baidu/mobads/constants/a;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "_"

    .line 160
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "4.1.30"

    .line 161
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 159
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "cs"

    const-string v4, ""

    .line 166
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "pk"

    .line 172
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v1, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getAppPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "trftp"

    const-string v4, "sdk_8.8427"

    .line 173
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v3, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v1, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "q"

    .line 177
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "_cpr"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "appid"

    .line 181
    invoke-virtual {v2, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "tp"

    .line 185
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "brd"

    .line 193
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getPhoneOSBrand()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object v3, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v1, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v3

    const-string v4, "den"

    .line 197
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "w"

    .line 199
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getW()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "h"

    .line 200
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getH()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v1, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getScreenRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v4

    const-string v5, "sw"

    .line 204
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "sh"

    .line 208
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "lw"

    .line 210
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getW()I

    move-result v5

    int-to-float v5, v5

    iget v6, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "lh"

    .line 211
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getH()I

    move-result v5

    int-to-float v5, v5

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v5, v3

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "sn"

    .line 233
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getSn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    :try_start_1
    const-string v7, ""

    .line 249
    iget-object v8, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getCell(Landroid/content/Context;)Ljava/util/List;

    move-result-object v8

    .line 250
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_1

    .line 251
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    .line 252
    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_0

    .line 253
    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    const-string v11, "%s_%s_%s|"

    .line 254
    new-array v12, v3, [Ljava/lang/Object;

    aget-object v13, v10, v6

    aput-object v13, v12, v6

    aget-object v13, v10, v5

    aput-object v13, v12, v5

    aget-object v10, v10, v4

    aput-object v10, v12, v4

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 256
    :cond_0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    sub-int/2addr v8, v5

    invoke-virtual {v7, v6, v8}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    :try_start_2
    const-string v7, ""

    :cond_1
    :goto_1
    const-string v8, "cid"

    .line 262
    invoke-virtual {v2, v8, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "nop"

    .line 274
    iget-object v8, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetworkOperator(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "im"

    .line 277
    iget-object v8, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v1, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getSubscriberId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 285
    :try_start_3
    iget-object v7, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getGPS(Landroid/content/Context;)[D

    move-result-object v7

    const-string v8, ""

    if-eqz v7, :cond_2

    .line 288
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    const-string v8, "%s_%s_%s"

    .line 289
    new-array v3, v3, [Ljava/lang/Object;

    aget-wide v9, v7, v6

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v3, v6

    aget-wide v9, v7, v5

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v3, v5

    aget-wide v9, v7, v4

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v3, v4

    invoke-static {v8, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    :try_start_4
    const-string v8, ""

    :cond_2
    :goto_2
    const-string v3, "g"

    .line 294
    invoke-virtual {v2, v3, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    :try_start_5
    const-string v3, ""
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 301
    :try_start_6
    iget-object v7, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getWIFI(Landroid/content/Context;)Ljava/util/List;

    move-result-object v7

    .line 302
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_4

    .line 303
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    .line 304
    :goto_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_3

    .line 305
    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    const-string v11, "%s_%s|"

    .line 306
    new-array v12, v4, [Ljava/lang/Object;

    aget-object v13, v10, v6

    aput-object v13, v12, v6

    aget-object v10, v10, v5

    aput-object v10, v12, v5

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 308
    :cond_3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    sub-int/2addr v4, v5

    invoke-virtual {v8, v6, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    move-object v3, v4

    goto :goto_4

    :catch_2
    move-object v3, v8

    :catch_3
    :cond_4
    :goto_4
    :try_start_7
    const-string v4, "wi"

    .line 313
    invoke-virtual {v2, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "swi"

    .line 324
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "wifi"

    iget-object v8, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v8}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetworkType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "tab"

    .line 328
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "1"

    goto :goto_5

    :cond_5
    const-string v4, "0"

    :goto_5
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "sdc"

    .line 335
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getAppSDC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ","

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getMem()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "act"

    .line 349
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getAct()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "prod"

    .line 356
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getProd()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "os"

    const-string v4, "android"

    .line 358
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "osv"

    .line 359
    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "bdr"

    .line 360
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "apinfo"

    .line 363
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v1, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getBaiduMapsInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "apid"

    .line 371
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getApid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "chid"

    .line 373
    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getChannelId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "apt"

    const-string v4, "0"

    .line 377
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "ap"

    .line 378
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getAp()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "nt"

    .line 380
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "udid"

    const-string v4, ""

    .line 381
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "ses"

    .line 385
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getSes()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "android_id"

    .line 387
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "imei"

    .line 388
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "mac"

    .line 389
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "cuid"

    .line 390
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getCUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "snfrom"

    .line 391
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getSnFrom(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "p_ver"

    const-string v4, "8.8427"

    .line 392
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "req_id"

    .line 393
    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/baidu/mobads/vo/d;->getApid()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v4, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->createRequestId(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "cssid"

    .line 394
    iget-object v3, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-interface {v0, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getWifiConnected(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_6
    const-string v0, ""

    :goto_6
    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "msa"

    .line 396
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    invoke-static {}, Lcom/baidu/mobads/AdSettings;->getSupportHttps()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/AdSettings$b;->c:Lcom/baidu/mobads/AdSettings$b;

    invoke-virtual {v1}, Lcom/baidu/mobads/AdSettings$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "rpt"

    .line 406
    sget-object v1, Lcom/baidu/mobads/AdSettings$b;->c:Lcom/baidu/mobads/AdSettings$b;

    invoke-virtual {v1}, Lcom/baidu/mobads/AdSettings$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    :cond_7
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    const-string v1, "mobads_aplist_status"

    iget-object v3, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    .line 433
    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v1, "iadex"

    const-string v3, "n_iad_sniff_result"

    const-string v4, ""

    .line 435
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    :cond_8
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    const-string v1, "mobads_uniqueidentifier"

    iget-object v3, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    .line 438
    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string v1, "oaid"

    const-string v3, "oaid"

    const-string v4, ""

    .line 440
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    const-string v0, "imei2"

    .line 442
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/baidu/mobads/utils/t;->a()Lcom/baidu/mobads/utils/t;

    move-result-object v3

    iget-object v4, p0, Lcom/baidu/mobads/vo/d;->d:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/baidu/mobads/utils/t;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    return-object v2
.end method

.method public e(I)V
    .locals 0

    .line 518
    iput p1, p0, Lcom/baidu/mobads/vo/d;->n:I

    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 496
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method public f(I)V
    .locals 0

    .line 542
    iput p1, p0, Lcom/baidu/mobads/vo/d;->r:I

    return-void
.end method

.method public g(I)V
    .locals 0

    .line 554
    iput p1, p0, Lcom/baidu/mobads/vo/d;->t:I

    return-void
.end method

.method public getAct()Ljava/lang/String;
    .locals 1

    .line 522
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getAp()I
    .locals 1

    .line 581
    iget v0, p0, Lcom/baidu/mobads/vo/d;->s:I

    return v0
.end method

.method public getApid()Ljava/lang/String;
    .locals 1

    .line 589
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getApt()I
    .locals 1

    .line 538
    iget v0, p0, Lcom/baidu/mobads/vo/d;->r:I

    return v0
.end method

.method public getAt()I
    .locals 1

    .line 597
    iget v0, p0, Lcom/baidu/mobads/vo/d;->o:I

    return v0
.end method

.method public getH()I
    .locals 1

    .line 514
    iget v0, p0, Lcom/baidu/mobads/vo/d;->n:I

    return v0
.end method

.method public getN()I
    .locals 1

    .line 546
    iget v0, p0, Lcom/baidu/mobads/vo/d;->t:I

    return v0
.end method

.method public getProd()Ljava/lang/String;
    .locals 1

    .line 530
    iget-object v0, p0, Lcom/baidu/mobads/vo/d;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getSes()J
    .locals 2

    .line 570
    iget-wide v0, p0, Lcom/baidu/mobads/vo/d;->v:J

    return-wide v0
.end method

.method public getSex()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getUk()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getW()I
    .locals 1

    .line 506
    iget v0, p0, Lcom/baidu/mobads/vo/d;->m:I

    return v0
.end method

.method public getZip()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public h(I)V
    .locals 0

    .line 585
    iput p1, p0, Lcom/baidu/mobads/vo/d;->s:I

    return-void
.end method

.method public i(I)V
    .locals 0

    .line 601
    iput p1, p0, Lcom/baidu/mobads/vo/d;->o:I

    return-void
.end method

.method public isCanClick()Z
    .locals 1

    .line 488
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/d;->u:Z

    return v0
.end method
