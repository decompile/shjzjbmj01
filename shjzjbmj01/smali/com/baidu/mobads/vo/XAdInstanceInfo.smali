.class public Lcom/baidu/mobads/vo/XAdInstanceInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/baidu/mobads/vo/XAdInstanceInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final TAG:Ljava/lang/String; = "XAdInstanceInfo"


# instance fields
.field private A:I

.field private B:I

.field private C:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private D:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private E:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private G:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private O:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private Q:I

.field private R:I

.field private S:I

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Z

.field private X:Ljava/lang/String;

.field private Y:Ljava/lang/String;

.field private Z:Ljava/lang/String;

.field private a:Ljava/lang/String;

.field private aA:I

.field private aB:I

.field private aC:I

.field private aD:I

.field private aE:Z

.field private aF:Ljava/lang/String;

.field private aa:Ljava/lang/String;

.field private ab:Ljava/lang/String;

.field private ac:J

.field private ad:I

.field private ae:Ljava/lang/String;

.field private af:I

.field private ag:Z

.field private ah:J

.field private ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

.field private aj:Ljava/lang/String;

.field private ak:I

.field private al:Z

.field private am:Z

.field private an:Z

.field private ao:Z

.field private ap:Z

.field private aq:Z

.field private ar:Z

.field private as:Z

.field private at:Ljava/lang/String;

.field private au:Ljava/lang/String;

.field private av:Ljava/lang/String;

.field private aw:Lorg/json/JSONArray;

.field private ax:Z

.field private ay:Ljava/lang/String;

.field private az:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:I

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:I

.field private u:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private v:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Lorg/json/JSONObject;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1276
    new-instance v0, Lcom/baidu/mobads/vo/a;

    invoke-direct {v0}, Lcom/baidu/mobads/vo/a;-><init>()V

    sput-object v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .line 1218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "-1"

    .line 28
    iput-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    const-string v0, ""

    .line 58
    iput-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    const-string v0, ""

    .line 59
    iput-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    const/4 v0, 0x0

    .line 60
    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    .line 61
    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    .line 100
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    .line 104
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    .line 105
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    .line 106
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    .line 107
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    .line 108
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    .line 109
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    .line 110
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    .line 111
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    .line 112
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    .line 113
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    .line 114
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    .line 115
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    const/4 v1, 0x1

    .line 125
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    .line 149
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->NONE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 156
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->al:Z

    .line 158
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->an:Z

    .line 159
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ao:Z

    .line 170
    iput-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ax:Z

    .line 1303
    iput-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aE:Z

    const/4 v2, 0x0

    .line 1316
    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aF:Ljava/lang/String;

    .line 1219
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    .line 1220
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    .line 1221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    .line 1222
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    .line 1223
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    .line 1224
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    .line 1225
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->au:Ljava/lang/String;

    .line 1226
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    .line 1227
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    .line 1228
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    .line 1229
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    .line 1230
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a:Ljava/lang/String;

    .line 1231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    .line 1232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    .line 1233
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    .line 1234
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    .line 1235
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    .line 1236
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    .line 1237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    .line 1238
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    .line 1239
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    .line 1240
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    .line 1241
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    .line 1242
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aj:Ljava/lang/String;

    .line 1243
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    .line 1244
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    .line 1245
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->at:Ljava/lang/String;

    .line 1246
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->av:Ljava/lang/String;

    .line 1247
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    .line 1248
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    .line 1249
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    .line 1250
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    .line 1251
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    .line 1252
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    .line 1253
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    .line 1254
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    .line 1255
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    .line 1256
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1257
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1258
    invoke-virtual {p0, v2}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setStartTrackers(Ljava/util/List;)V

    .line 1259
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1260
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1261
    invoke-virtual {p0, v2}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->setCloseTrackers(Ljava/util/List;)V

    .line 1263
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->y:Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 1265
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "XAdInstanceInfo"

    aput-object v5, v4, v0

    .line 1266
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-interface {v3, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 1268
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    .line 1270
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aA:I

    .line 1271
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aB:I

    .line 1272
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aC:I

    .line 1273
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aD:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/baidu/mobads/vo/a;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/baidu/mobads/vo/XAdInstanceInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .line 732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "-1"

    .line 28
    iput-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    const-string v0, ""

    .line 58
    iput-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    const-string v0, ""

    .line 59
    iput-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    const/4 v0, 0x0

    .line 60
    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    .line 61
    iput v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    .line 100
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    .line 104
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    .line 105
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    .line 106
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    .line 107
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    .line 108
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    .line 109
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    .line 110
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    .line 111
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    .line 112
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    .line 113
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    .line 114
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    .line 115
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    const/4 v1, 0x1

    .line 125
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    .line 149
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->NONE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 156
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->al:Z

    .line 158
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->an:Z

    .line 159
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ao:Z

    .line 170
    iput-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ax:Z

    .line 1303
    iput-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aE:Z

    const/4 v2, 0x0

    .line 1316
    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aF:Ljava/lang/String;

    .line 733
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->y:Lorg/json/JSONObject;

    .line 735
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ah:J

    const-string v3, "act"

    .line 736
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    const-string v3, "html"

    .line 737
    invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    const-string v2, "id"

    const-string v3, "-1"

    .line 741
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    const-string v2, "src"

    const-string v3, ""

    .line 742
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    const-string v2, "tit"

    const-string v3, ""

    .line 743
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    const-string v2, "desc"

    const-string v3, ""

    .line 744
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    const-string v2, "surl"

    const-string v3, ""

    .line 745
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    const-string v2, "phone"

    const-string v3, ""

    .line 746
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    const-string v2, "w_picurl"

    const-string v3, ""

    .line 747
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    const-string v2, "icon"

    const-string v3, ""

    .line 748
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    const-string v2, "exp2"

    const-string v3, "{}"

    .line 749
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    const-string v2, "anti_tag"

    .line 750
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->l:I

    const-string v2, "vurl"

    const-string v3, ""

    .line 753
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    const-string v2, "duration"

    .line 754
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    const-string v2, "sound"

    .line 755
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->o:Z

    const-string v2, "iv"

    .line 756
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->u:Z

    const-string v2, "dur"

    .line 758
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->v:I

    const-string v2, "curl"

    const-string v3, ""

    .line 761
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    const-string v2, "ori_curl"

    const-string v3, ""

    .line 762
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    const-string v2, "closetype"

    .line 764
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    const-string v2, "expiration"

    .line 765
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    const-string v2, "mute"

    .line 766
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    const-string v2, "ad_html"

    .line 767
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 769
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 770
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "banner_snippet"

    .line 771
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 772
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const-string v5, "int_snippet"

    .line 773
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 774
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    goto :goto_2

    :cond_4
    const-string v2, "type"

    .line 779
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    .line 781
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    .line 782
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 783
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto/16 :goto_3

    .line 784
    :cond_5
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 785
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    const-string v3, "text"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 786
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->TEXT:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto :goto_3

    .line 787
    :cond_6
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    const-string v3, "image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 788
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    const-string v3, ""

    .line 789
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 790
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    .line 791
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .line 790
    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x2e

    .line 791
    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    const-string v3, ""

    if-ltz v2, :cond_7

    .line 794
    iget-object v3, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    .line 795
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .line 794
    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 795
    invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    :cond_7
    const-string v2, ".gif"

    .line 797
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 798
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->GIF:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto :goto_3

    .line 800
    :cond_8
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->STATIC_IMAGE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto :goto_3

    .line 803
    :cond_9
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    const-string v3, "rm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 804
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    goto :goto_3

    .line 805
    :cond_a
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    const-string v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 806
    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    :cond_b
    :goto_3
    const-string v2, "w"

    .line 809
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    const-string v2, "h"

    .line 810
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    const-string v2, "lb_phone"

    const-string v3, ""

    .line 811
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    const-string v2, "nwinurl"

    .line 813
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 814
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_c

    const/4 v3, 0x0

    .line 815
    :goto_4
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_d

    .line 816
    iget-object v4, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_c
    const-string v2, "winurl"

    const-string v3, ""

    .line 819
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    .line 820
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 821
    iget-object v3, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_d
    const-string v2, "clklogurl"

    const-string v3, ""

    .line 824
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    .line 825
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 826
    iget-object v3, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_e
    const-string v2, "mon"

    .line 828
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 829
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_f

    const/4 v3, 0x0

    .line 830
    :goto_5
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_f

    .line 831
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "s"

    const-string v6, ""

    .line 832
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "c"

    const-string v7, ""

    .line 833
    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 834
    invoke-virtual {p0, v5}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a(Ljava/lang/String;)V

    .line 835
    invoke-virtual {p0, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_f
    const-string v2, "monitors"

    .line 840
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_1c

    .line 842
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    :cond_10
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 843
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "s"

    .line 845
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 846
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 847
    :goto_6
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 848
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_11
    const-string v5, "vskip"

    .line 850
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 851
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 852
    :goto_7
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 853
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addSkipMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_12
    const-string v5, "scard"

    .line 855
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 856
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 857
    :goto_8
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 858
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addScardMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    :cond_13
    const-string v5, "ccard"

    .line 860
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 861
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 862
    :goto_9
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 863
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addCcardMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    :cond_14
    const-string v5, "vstart"

    .line 865
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 866
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 867
    :goto_a
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 868
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addStartMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    :cond_15
    const-string v5, "vfullscreen"

    .line 870
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 871
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 872
    :goto_b
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 873
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addFullScreenMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    :cond_16
    const-string v5, "vclose"

    .line 875
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 876
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 877
    :goto_c
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 878
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addCloseMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_c

    :cond_17
    const-string v5, "cstartcard"

    .line 880
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 881
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 882
    :goto_d
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 883
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->addCstartcardMonitorTrackers(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_d

    :cond_18
    const-string v5, "c"

    .line 885
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 886
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 887
    :goto_e
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 888
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_e

    :cond_19
    const-string v5, "vcache_succ"

    .line 890
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 891
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 892
    :goto_f
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 893
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_f

    :cond_1a
    const-string v5, "vcache_fail"

    .line 895
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 896
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 897
    :goto_10
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 898
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_10

    :cond_1b
    const-string v5, "vcache_expire"

    .line 900
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 901
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    const/4 v5, 0x0

    .line 902
    :goto_11
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_10

    .line 903
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_11

    .line 911
    :cond_1c
    iput-boolean v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    const-string v2, "cf"

    const-string v3, ""

    .line 912
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    const-string v2, "qk"

    const-string v3, ""

    .line 913
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    .line 915
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-virtual {v3}, Ljava/util/Random;->nextLong()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 916
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Z:Ljava/lang/String;

    const-string v2, "appname"

    const-string v3, ""

    .line 917
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    const-string v2, "pk"

    const-string v3, ""

    .line 918
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    const-string v2, "sz"

    const-wide/16 v3, 0x0

    .line 919
    invoke-virtual {p1, v2, v3, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ac:J

    const-string v2, "sb"

    .line 920
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ad:I

    const-string v2, "apo"

    const-string v3, ""

    .line 921
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    const-string v2, "po"

    .line 922
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->af:I

    const-string v2, "st"

    .line 923
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_1d

    const/4 v2, 0x1

    goto :goto_12

    :cond_1d
    const/4 v2, 0x0

    :goto_12
    iput-boolean v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ag:Z

    const-string v2, "murl"

    const-string v3, ""

    .line 925
    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    .line 926
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    const-string v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1e

    const-string v2, "w"

    .line 927
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    const-string v2, "h"

    .line 928
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    const-string v2, "video"

    .line 929
    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    .line 931
    iget v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    .line 932
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/l;->getActTypeLandingPage()I

    move-result v3

    if-ne v2, v3, :cond_1e

    .line 933
    iget-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    iput-object v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    :cond_1e
    const-string v2, "container_width"

    .line 937
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aA:I

    const-string v2, "container_height"

    .line 938
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aB:I

    const-string v2, "size_type"

    .line 939
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aC:I

    const-string v2, "style_type"

    .line 940
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aD:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_13

    :catch_0
    move-exception p1

    .line 943
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "XAdInstanceInfo"

    aput-object v4, v3, v0

    .line 944
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v1

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    :goto_13
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)V
    .locals 1

    .line 949
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 950
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addCcardMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    .line 1082
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1083
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addCloseMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, ""

    .line 1114
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1115
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addCstartcardMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, ""

    .line 1131
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1132
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addFullScreenMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, ""

    .line 1098
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1099
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addScardMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    .line 1067
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1068
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addSkipMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    .line 1051
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1052
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addStartMonitorTrackers(Ljava/lang/String;)V
    .locals 1

    .line 1030
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1031
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 1

    .line 955
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 956
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method final c(Ljava/lang/String;)V
    .locals 1

    .line 961
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1153
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final d(Ljava/lang/String;)V
    .locals 1

    .line 982
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 983
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method final e(Ljava/lang/String;)V
    .locals 1

    .line 1003
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1004
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public getAPOOpen()Z
    .locals 1

    .line 1307
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aE:Z

    return v0
.end method

.method public getAction()Ljava/lang/String;
    .locals 1

    .line 1364
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getActionType()I
    .locals 1

    .line 584
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    return v0
.end method

.method public getAdContainerHeight()I
    .locals 1

    .line 193
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aB:I

    return v0
.end method

.method public getAdContainerSizeType()I
    .locals 1

    .line 201
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aC:I

    return v0
.end method

.method public getAdContainerWidth()I
    .locals 1

    .line 185
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aA:I

    return v0
.end method

.method public getAdHasDisplayed()Z
    .locals 1

    .line 1435
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->az:Z

    return v0
.end method

.method public getAdId()Ljava/lang/String;
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getAdSource()Ljava/lang/String;
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getAntiTag()I
    .locals 1

    .line 423
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->l:I

    return v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .line 632
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public getAppOpenStrs()Ljava/lang/String;
    .locals 1

    .line 662
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    return-object v0
.end method

.method public getAppPackageName()Ljava/lang/String;
    .locals 1

    .line 622
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public getAppSize()J
    .locals 2

    .line 642
    iget-wide v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ac:J

    return-wide v0
.end method

.method public getBannerHtmlSnippet()Ljava/lang/String;
    .locals 1

    .line 1410
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    return-object v0
.end method

.method public getCacheExpireTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1010
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getCacheFailTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 989
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getCacheSuccTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 968
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getCcardTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1089
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getClickThroughUrl()Ljava/lang/String;
    .locals 1

    .line 493
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    return-object v0
.end method

.method public getClklogurl()Ljava/lang/String;
    .locals 1

    .line 704
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->au:Ljava/lang/String;

    return-object v0
.end method

.method public getCloseTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1121
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getCloseType()I
    .locals 1

    .line 1380
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    return v0
.end method

.method public getConfirmBorderPercent()Ljava/lang/String;
    .locals 1

    .line 602
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    .line 1157
    iget-wide v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ah:J

    return-wide v0
.end method

.method public getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    return-object v0
.end method

.method public getCstartcardTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1138
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .line 353
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getDlTunnel()I
    .locals 1

    .line 225
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ak:I

    return v0
.end method

.method public getExp2ForSingleAd()Ljava/lang/String;
    .locals 1

    .line 413
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiration()I
    .locals 1

    .line 1390
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    return v0
.end method

.method public getFeedAdStyleType()I
    .locals 1

    .line 209
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aD:I

    return v0
.end method

.method public getFullScreenTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1105
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getFwt()Ljava/lang/String;
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getHoursInADayToShowAd()I
    .locals 1

    .line 483
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->v:I

    return v0
.end method

.method public getHtmlSnippet()Ljava/lang/String;
    .locals 1

    .line 513
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getImpressionUrls()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 553
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    return-object v0
.end method

.method public getIntHtmlSnippet()Ljava/lang/String;
    .locals 1

    .line 1420
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalCreativeURL()Ljava/lang/String;
    .locals 1

    .line 433
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getMainMaterialHeight()I
    .locals 1

    .line 533
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    return v0
.end method

.method public getMainMaterialWidth()I
    .locals 1

    .line 523
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    return v0
.end method

.method public getMainPictureUrl()Ljava/lang/String;
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getMaterialType()Ljava/lang/String;
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getMute()Ljava/lang/String;
    .locals 1

    .line 1400
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    return-object v0
.end method

.method public getNwinurl()Lorg/json/JSONArray;
    .locals 1

    .line 720
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aw:Lorg/json/JSONArray;

    return-object v0
.end method

.method public getOriginClickUrl()Ljava/lang/String;
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    return-object v0
.end method

.method public getOriginJsonObject()Lorg/json/JSONObject;
    .locals 1

    .line 692
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->y:Lorg/json/JSONObject;

    return-object v0
.end method

.method public getPage()Ljava/lang/String;
    .locals 1

    .line 1320
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aF:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneForLocalBranding()Ljava/lang/String;
    .locals 1

    .line 543
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .line 383
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getPointsForWall()I
    .locals 1

    .line 672
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->af:I

    return v0
.end method

.method public getQueryKey()Ljava/lang/String;
    .locals 1

    .line 612
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    return-object v0
.end method

.method public getScardTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1073
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSkipTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1058
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSplash3DLocalUrl()Ljava/lang/String;
    .locals 1

    .line 1334
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ay:Ljava/lang/String;

    return-object v0
.end method

.method public getSponsorUrl()Ljava/lang/String;
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTrackers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1037
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSwitchButton()I
    .locals 1

    .line 652
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ad:I

    return v0
.end method

.method public getThirdClickTrackingUrls()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 574
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getThirdImpressionTrackingUrls()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 563
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getUniqueId()Ljava/lang/String;
    .locals 1

    .line 1289
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Z:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aj:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoDuration()I
    .locals 1

    .line 463
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    return v0
.end method

.method public getVideoHeight()I
    .locals 1

    .line 1349
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    return v0
.end method

.method public getVideoUrl()Ljava/lang/String;
    .locals 1

    .line 443
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoWidth()I
    .locals 1

    .line 1339
    iget v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    return v0
.end method

.method public getVurl()Ljava/lang/String;
    .locals 1

    .line 696
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->at:Ljava/lang/String;

    return-object v0
.end method

.method public getWebUrl()Ljava/lang/String;
    .locals 1

    .line 1369
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    return-object v0
.end method

.method public getWinurl()Ljava/lang/String;
    .locals 1

    .line 712
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->av:Ljava/lang/String;

    return-object v0
.end method

.method public isActionOnlyWifi()Z
    .locals 1

    .line 593
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    return v0
.end method

.method public isAutoOpen()Z
    .locals 1

    .line 249
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->an:Z

    return v0
.end method

.method public isCanCancel()Z
    .locals 1

    .line 281
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ar:Z

    return v0
.end method

.method public isCanDelete()Z
    .locals 1

    .line 289
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->as:Z

    return v0
.end method

.method public isClose()Z
    .locals 1

    .line 241
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->am:Z

    return v0
.end method

.method public isIconVisibleForImageType()Z
    .locals 1

    .line 473
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->u:Z

    return v0
.end method

.method public isInapp()Z
    .locals 1

    .line 233
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->al:Z

    return v0
.end method

.method public isPopNotif()Z
    .locals 1

    .line 257
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ao:Z

    return v0
.end method

.method public isSecondConfirmed()Z
    .locals 1

    .line 1294
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ax:Z

    return v0
.end method

.method public isTaskDoneForWall()Z
    .locals 1

    .line 682
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ag:Z

    return v0
.end method

.method public isTooLarge()Z
    .locals 1

    .line 273
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aq:Z

    return v0
.end method

.method public isValid()Ljava/lang/Boolean;
    .locals 2

    const-string v0, "-1"

    .line 328
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->getAdId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isVideoMuted()Z
    .locals 1

    .line 453
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->o:Z

    return v0
.end method

.method public isWifiTargeted()Z
    .locals 1

    .line 265
    iget-boolean v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ap:Z

    return v0
.end method

.method public setAPOOpen(Z)V
    .locals 0

    .line 1312
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aE:Z

    return-void
.end method

.method public setAction(Ljava/lang/String;)V
    .locals 0

    .line 1359
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    return-void
.end method

.method public setActionOnlyWifi(Z)V
    .locals 0

    .line 597
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->W:Z

    return-void
.end method

.method public setActionType(I)V
    .locals 0

    .line 589
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    return-void
.end method

.method public setAdContainerHeight(I)V
    .locals 0

    .line 197
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aB:I

    return-void
.end method

.method public setAdContainerSizeType(I)V
    .locals 0

    .line 205
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aC:I

    return-void
.end method

.method public setAdContainerWidth(I)V
    .locals 0

    .line 189
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aA:I

    return-void
.end method

.method public setAdHasDisplayed(Z)V
    .locals 0

    .line 1430
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->az:Z

    return-void
.end method

.method public setAdId(Ljava/lang/String;)V
    .locals 0

    .line 323
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    return-void
.end method

.method public setAdSource(Ljava/lang/String;)V
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    return-void
.end method

.method public setAntiTag(I)V
    .locals 0

    .line 428
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->l:I

    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0

    .line 637
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    return-void
.end method

.method public setAppOpenStrs(Ljava/lang/String;)V
    .locals 0

    .line 667
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    return-void
.end method

.method public setAppPackageName(Ljava/lang/String;)V
    .locals 0

    .line 627
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    return-void
.end method

.method public setAppSize(J)V
    .locals 0

    .line 647
    iput-wide p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ac:J

    return-void
.end method

.method public setAutoOpen(Z)V
    .locals 0

    .line 253
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->an:Z

    return-void
.end method

.method public setBannerHtmlSnippet(Ljava/lang/String;)V
    .locals 0

    .line 1415
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    return-void
.end method

.method public setCacheExpireTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1016
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1017
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->P:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1019
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setCacheFailTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 995
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 996
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->O:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 998
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setCacheSuccTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 974
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 975
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->N:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 977
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setCanCancel(Z)V
    .locals 0

    .line 285
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ar:Z

    return-void
.end method

.method public setCanDelete(Z)V
    .locals 0

    .line 293
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->as:Z

    return-void
.end method

.method public setCcardTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1094
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->J:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setClickThroughUrl(Ljava/lang/String;)V
    .locals 0

    .line 498
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    return-void
.end method

.method public setClklogurl(Ljava/lang/String;)V
    .locals 0

    .line 708
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->au:Ljava/lang/String;

    return-void
.end method

.method public setClose(Z)V
    .locals 0

    .line 245
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->am:Z

    return-void
.end method

.method public setCloseTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1144
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1145
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->L:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1147
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setCloseType(I)V
    .locals 0

    .line 1385
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    return-void
.end method

.method public setConfirmBorderPercent(Ljava/lang/String;)V
    .locals 0

    .line 607
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    return-void
.end method

.method public setCreateTime(J)V
    .locals 0

    .line 1161
    iput-wide p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ah:J

    return-void
.end method

.method public setCreativeType(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;)V
    .locals 0

    .line 303
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ai:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    return-void
.end method

.method public setCstartcardTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1126
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1127
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->M:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    return-void
.end method

.method public setDlTunnel(I)V
    .locals 0

    .line 229
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ak:I

    return-void
.end method

.method public setExp2ForSingleAd(Ljava/lang/String;)V
    .locals 0

    .line 418
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    return-void
.end method

.method public setExpiration(I)V
    .locals 0

    .line 1395
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    return-void
.end method

.method public setFeedAdStyleType(I)V
    .locals 0

    .line 213
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aD:I

    return-void
.end method

.method public setFullScreenTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1110
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->K:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setFwt(Ljava/lang/String;)V
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a:Ljava/lang/String;

    return-void
.end method

.method public setHoursInADayToShowAd(I)V
    .locals 0

    .line 488
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->v:I

    return-void
.end method

.method public setHtmlSnippet(Ljava/lang/String;)V
    .locals 0

    .line 518
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    return-void
.end method

.method public setIconUrl(Ljava/lang/String;)V
    .locals 0

    .line 408
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    return-void
.end method

.method public setIconVisibleForImageType(Z)V
    .locals 0

    .line 478
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->u:Z

    return-void
.end method

.method public setImpressionUrls(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 558
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->D:Ljava/util/Set;

    return-void
.end method

.method public setInapp(Z)V
    .locals 0

    .line 237
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->al:Z

    return-void
.end method

.method public setIntHtmlSnippet(Ljava/lang/String;)V
    .locals 0

    .line 1425
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    return-void
.end method

.method public setLocalCreativeURL(Ljava/lang/String;)V
    .locals 0

    .line 438
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->m:Ljava/lang/String;

    return-void
.end method

.method public setMainMaterialHeight(I)V
    .locals 0

    .line 538
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    return-void
.end method

.method public setMainMaterialWidth(I)V
    .locals 0

    .line 528
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    return-void
.end method

.method public setMainPictureUrl(Ljava/lang/String;)V
    .locals 0

    .line 398
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    return-void
.end method

.method public setMaterialType(Ljava/lang/String;)V
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    return-void
.end method

.method public setMute(Ljava/lang/String;)V
    .locals 0

    .line 1405
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    return-void
.end method

.method public setNwinurl(Lorg/json/JSONArray;)V
    .locals 0

    .line 724
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aw:Lorg/json/JSONArray;

    return-void
.end method

.method public setOriginClickUrl(Ljava/lang/String;)V
    .locals 0

    .line 508
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    return-void
.end method

.method public setPage(Ljava/lang/String;)V
    .locals 0

    .line 1325
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aF:Ljava/lang/String;

    return-void
.end method

.method public setPhoneForLocalBranding(Ljava/lang/String;)V
    .locals 0

    .line 548
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    return-void
.end method

.method public setPointsForWall(I)V
    .locals 0

    .line 677
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->af:I

    return-void
.end method

.method public setPopNotif(Z)V
    .locals 0

    .line 261
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ao:Z

    return-void
.end method

.method public setQueryKey(Ljava/lang/String;)V
    .locals 0

    .line 617
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    return-void
.end method

.method public setScardTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1078
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->I:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setSecondConfirmed(Z)V
    .locals 0

    .line 1299
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ax:Z

    return-void
.end method

.method public setSkipTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1063
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->H:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public setSplash3DLocalUrl(Ljava/lang/String;)V
    .locals 0

    .line 1330
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ay:Ljava/lang/String;

    return-void
.end method

.method public setSponsorUrl(Ljava/lang/String;)V
    .locals 0

    .line 368
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    return-void
.end method

.method public setStartTrackers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1043
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1044
    iget-object v0, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->G:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1046
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public setSwitchButton(I)V
    .locals 0

    .line 657
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ad:I

    return-void
.end method

.method public setTaskDoneForWall(Z)V
    .locals 0

    .line 687
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ag:Z

    return-void
.end method

.method public setThirdClickTrackingUrls(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 579
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->F:Ljava/util/Set;

    return-void
.end method

.method public setThirdImpressionTrackingUrls(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 569
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->E:Ljava/util/Set;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    return-void
.end method

.method public setTooLarge(Z)V
    .locals 0

    .line 277
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aq:Z

    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    .line 221
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aj:Ljava/lang/String;

    return-void
.end method

.method public setVideoDuration(I)V
    .locals 0

    .line 468
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    return-void
.end method

.method public setVideoHeight(I)V
    .locals 0

    .line 1354
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    return-void
.end method

.method public setVideoMuted(Z)V
    .locals 0

    .line 458
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->o:Z

    return-void
.end method

.method public setVideoUrl(Ljava/lang/String;)V
    .locals 0

    .line 448
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    return-void
.end method

.method public setVideoWidth(I)V
    .locals 0

    .line 1344
    iput p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    return-void
.end method

.method public setVurl(Ljava/lang/String;)V
    .locals 0

    .line 700
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->at:Ljava/lang/String;

    return-void
.end method

.method public setWebUrl(Ljava/lang/String;)V
    .locals 0

    .line 1374
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    return-void
.end method

.method public setWifiTargeted(Z)V
    .locals 0

    .line 269
    iput-boolean p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ap:Z

    return-void
.end method

.method public setWinurl(Ljava/lang/String;)V
    .locals 0

    .line 716
    iput-object p1, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->av:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1171
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1172
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1173
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ab:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1174
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->ae:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1175
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aa:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1176
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->w:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1177
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->au:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->X:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1179
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->e:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1180
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1181
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->k:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1182
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1183
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->z:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1184
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->j:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1185
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->i:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1186
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->g:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1187
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->B:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1188
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->A:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1189
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->x:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1190
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->C:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1191
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->h:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1192
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Y:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1193
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->f:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1194
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aj:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1195
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->n:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1196
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->p:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1197
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->at:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1198
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->av:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1199
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->q:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1200
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->r:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1201
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->s:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1202
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->t:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1203
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->R:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1204
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->S:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1205
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->T:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1206
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->U:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1207
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->V:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1208
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->getStartTrackers()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1209
    invoke-virtual {p0}, Lcom/baidu/mobads/vo/XAdInstanceInfo;->getCloseTrackers()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1210
    iget-object p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->y:Lorg/json/JSONObject;

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1211
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->Q:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1212
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aA:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1213
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aB:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1214
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aC:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1215
    iget p2, p0, Lcom/baidu/mobads/vo/XAdInstanceInfo;->aD:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
