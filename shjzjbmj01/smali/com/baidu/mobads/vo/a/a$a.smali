.class public Lcom/baidu/mobads/vo/a/a$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/vo/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;)V
    .locals 1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 63
    iput-object v0, p0, Lcom/baidu/mobads/vo/a/a$a;->a:Ljava/lang/String;

    const-string v0, ""

    .line 64
    iput-object v0, p0, Lcom/baidu/mobads/vo/a/a$a;->b:Ljava/lang/String;

    const-string v0, ""

    .line 65
    iput-object v0, p0, Lcom/baidu/mobads/vo/a/a$a;->c:Ljava/lang/String;

    const-string v0, ""

    .line 66
    iput-object v0, p0, Lcom/baidu/mobads/vo/a/a$a;->d:Ljava/lang/String;

    const-string v0, ""

    .line 67
    iput-object v0, p0, Lcom/baidu/mobads/vo/a/a$a;->e:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 77
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/vo/a/a$a;->a:Ljava/lang/String;

    .line 78
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getQueryKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/vo/a/a$a;->b:Ljava/lang/String;

    .line 79
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->getValue()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/a/a$a;->d:Ljava/lang/String;

    :cond_0
    if-eqz p2, :cond_1

    .line 82
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getAdPlacementId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/a/a$a;->e:Ljava/lang/String;

    .line 83
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/a/a$a;->c:Ljava/lang/String;

    :cond_1
    return-void
.end method
