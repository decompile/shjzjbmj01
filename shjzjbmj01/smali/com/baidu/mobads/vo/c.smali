.class public Lcom/baidu/mobads/vo/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXAdResponseInfo;


# instance fields
.field private a:I

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private q:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 7

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 27
    iput v0, p0, Lcom/baidu/mobads/vo/c;->a:I

    .line 30
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/vo/c;->b:Ljava/lang/Boolean;

    const/4 v1, -0x1

    .line 52
    iput v1, p0, Lcom/baidu/mobads/vo/c;->i:I

    .line 57
    iput v1, p0, Lcom/baidu/mobads/vo/c;->j:I

    .line 71
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/baidu/mobads/vo/c;->p:Ljava/util/ArrayList;

    .line 81
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->o:Ljava/lang/String;

    .line 82
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/baidu/mobads/vo/c;->q:J

    .line 83
    sput-object p1, Lcom/baidu/mobads/b/a;->c:Ljava/lang/String;

    .line 85
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    :try_start_0
    const-string p1, "ad"

    .line 87
    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    .line 89
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-ge v3, v4, :cond_0

    .line 91
    :try_start_1
    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 92
    iget-object v5, p0, Lcom/baidu/mobads/vo/c;->p:Ljava/util/ArrayList;

    new-instance v6, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    invoke-direct {v6, v4}, Lcom/baidu/mobads/vo/XAdInstanceInfo;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 99
    :catch_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->p:Ljava/util/ArrayList;

    :cond_0
    const-string p1, "n"

    .line 101
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/vo/c;->a:I

    const-string p1, "x"

    .line 102
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/vo/c;->d:I

    const-string p1, "y"

    .line 103
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/vo/c;->e:I

    const-string p1, "m"

    .line 104
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->b:Ljava/lang/Boolean;

    const-string p1, "u"

    const-string v0, ""

    .line 105
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->c:Ljava/lang/String;

    const-string p1, "exp2"

    const-string v0, "{}"

    .line 106
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->f:Ljava/lang/String;

    const-string p1, "ext_act"

    const-string v0, "{}"

    .line 107
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->g:Ljava/lang/String;

    const-string p1, "lunpan"

    .line 108
    invoke-virtual {v2, p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/vo/c;->i:I

    const-string p1, "intIcon"

    .line 109
    invoke-virtual {v2, p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/vo/c;->j:I

    const-string p1, "ck"

    const-string v0, ""

    .line 110
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->k:Ljava/lang/String;

    const-string p1, "req_id"

    .line 111
    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->l:Ljava/lang/String;

    const-string p1, "error_code"

    const-string v0, ""

    .line 112
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->m:Ljava/lang/String;

    const-string p1, "error_msg"

    const-string v0, ""

    .line 113
    invoke-virtual {v2, p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->n:Ljava/lang/String;

    :try_start_2
    const-string p1, "theme"

    .line 116
    invoke-virtual {v2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->h:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method


# virtual methods
.method public getAdInstanceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ">;"
        }
    .end annotation

    .line 259
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->p:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAdsNum()I
    .locals 1

    .line 127
    iget v0, p0, Lcom/baidu/mobads/vo/c;->a:I

    return v0
.end method

.method public getBaiduidOfCookie()Ljava/lang/String;
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultFillInThemeForStaticAds()Ljava/lang/String;
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorCode()Ljava/lang/String;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->m:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/d/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->n:Ljava/lang/String;

    return-object v0

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->m:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 237
    iput-object v0, p0, Lcom/baidu/mobads/vo/c;->n:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, ""

    .line 239
    iput-object v0, p0, Lcom/baidu/mobads/vo/c;->n:Ljava/lang/String;

    .line 242
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getExp2()Ljava/lang/String;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getExtentionActionExp()Ljava/lang/String;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getIntIcon()I
    .locals 1

    .line 199
    iget v0, p0, Lcom/baidu/mobads/vo/c;->j:I

    return v0
.end method

.method public getLatitude()I
    .locals 1

    .line 159
    iget v0, p0, Lcom/baidu/mobads/vo/c;->e:I

    return v0
.end method

.method public getLongitude()I
    .locals 1

    .line 151
    iget v0, p0, Lcom/baidu/mobads/vo/c;->d:I

    return v0
.end method

.method public getLunpan()I
    .locals 1

    .line 191
    iget v0, p0, Lcom/baidu/mobads/vo/c;->i:I

    return v0
.end method

.method public getOpenPointModeForWall()Ljava/lang/Boolean;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->b:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getOriginResponseStr()Ljava/lang/String;
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->o:Ljava/lang/String;

    return-object v0
.end method

.method public getPointUnitForWall()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->p:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/baidu/mobads/vo/c;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()J
    .locals 2

    .line 269
    iget-wide v0, p0, Lcom/baidu/mobads/vo/c;->q:J

    return-wide v0
.end method

.method public setAdInstanceList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ">;)V"
        }
    .end annotation

    .line 264
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->p:Ljava/util/ArrayList;

    return-void
.end method

.method public setAdsNum(I)V
    .locals 0

    .line 131
    iput p1, p0, Lcom/baidu/mobads/vo/c;->a:I

    return-void
.end method

.method public setBaiduidOfCookie(Ljava/lang/String;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->k:Ljava/lang/String;

    return-void
.end method

.method public setDefaultFillInThemeForStaticAds(Ljava/lang/String;)V
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->h:Ljava/lang/String;

    return-void
.end method

.method public setErrorCode(Ljava/lang/String;)V
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->m:Ljava/lang/String;

    return-void
.end method

.method public setErrorMessage(Ljava/lang/String;)V
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->n:Ljava/lang/String;

    return-void
.end method

.method public setExp2(Ljava/lang/String;)V
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->f:Ljava/lang/String;

    return-void
.end method

.method public setExtentionActionExp(Ljava/lang/String;)V
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->g:Ljava/lang/String;

    return-void
.end method

.method public setIntIcon(I)V
    .locals 0

    .line 203
    iput p1, p0, Lcom/baidu/mobads/vo/c;->j:I

    return-void
.end method

.method public setLatitude(I)V
    .locals 0

    .line 163
    iput p1, p0, Lcom/baidu/mobads/vo/c;->e:I

    return-void
.end method

.method public setLongitude(I)V
    .locals 0

    .line 155
    iput p1, p0, Lcom/baidu/mobads/vo/c;->d:I

    return-void
.end method

.method public setLunpan(I)V
    .locals 0

    .line 195
    iput p1, p0, Lcom/baidu/mobads/vo/c;->i:I

    return-void
.end method

.method public setOpenPointModeForWall(Ljava/lang/Boolean;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->b:Ljava/lang/Boolean;

    return-void
.end method

.method public setOriginResponseStr(Ljava/lang/String;)V
    .locals 0

    .line 254
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->o:Ljava/lang/String;

    return-void
.end method

.method public setPointUnitForWall(Ljava/lang/String;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->c:Ljava/lang/String;

    return-void
.end method

.method public setRequestId(Ljava/lang/String;)V
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/baidu/mobads/vo/c;->l:Ljava/lang/String;

    return-void
.end method
