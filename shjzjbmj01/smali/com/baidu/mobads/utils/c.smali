.class public Lcom/baidu/mobads/utils/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Lcom/baidu/mobads/utils/c;


# instance fields
.field private a:Lcom/baidu/mobads/nativecpu/a/c;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "com.component.interfaces.RemoteReflectInterface"

    .line 23
    invoke-static {p1, v0}, Lcom/baidu/mobads/nativecpu/a/c;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/baidu/mobads/nativecpu/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/utils/c;->a:Lcom/baidu/mobads/nativecpu/a/c;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/baidu/mobads/utils/c;
    .locals 2

    .line 27
    sget-object v0, Lcom/baidu/mobads/utils/c;->b:Lcom/baidu/mobads/utils/c;

    if-nez v0, :cond_1

    .line 28
    const-class v0, Lcom/baidu/mobads/utils/c;

    monitor-enter v0

    .line 29
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/utils/c;->b:Lcom/baidu/mobads/utils/c;

    if-nez v1, :cond_0

    .line 30
    new-instance v1, Lcom/baidu/mobads/utils/c;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/utils/c;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/baidu/mobads/utils/c;->b:Lcom/baidu/mobads/utils/c;

    .line 32
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 34
    :cond_1
    :goto_0
    sget-object p0, Lcom/baidu/mobads/utils/c;->b:Lcom/baidu/mobads/utils/c;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;)I
    .locals 4

    .line 56
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/utils/r;->isInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0x67

    return p1

    .line 60
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/utils/c;->a:Lcom/baidu/mobads/nativecpu/a/c;

    const/4 v0, 0x0

    const-string v1, "getDownloadStatus"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {p1, v0, v1, v2}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 61
    instance-of p2, p1, Ljava/lang/Integer;

    if-eqz p2, :cond_1

    .line 62
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .line 38
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/baidu/mobads/utils/c;->a:Lcom/baidu/mobads/nativecpu/a/c;

    const/4 v1, 0x0

    const-string v2, "pauseDlByPk"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 46
    iget-object v1, p0, Lcom/baidu/mobads/utils/c;->a:Lcom/baidu/mobads/nativecpu/a/c;

    const/4 v2, 0x0

    const-string v3, "resumeDownload"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v0

    const/4 p1, 0x1

    aput-object p2, v4, p1

    const/4 p1, 0x2

    aput-object p3, v4, p1

    const/4 p1, 0x3

    aput-object p4, v4, p1

    invoke-virtual {v1, v2, v3, v4}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 48
    instance-of p2, p1, Ljava/lang/Boolean;

    if-eqz p2, :cond_0

    .line 49
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    :cond_0
    return v0
.end method
