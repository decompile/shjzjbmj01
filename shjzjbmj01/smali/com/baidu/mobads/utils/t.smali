.class public Lcom/baidu/mobads/utils/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;


# static fields
.field static a:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static b:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static c:Lorg/json/JSONArray; = null

.field private static e:Ljava/lang/String; = null

.field private static f:Ljava/lang/String; = null

.field private static g:Ljava/lang/String; = null

.field private static h:J = -0x1L

.field private static i:Ljava/lang/String;

.field private static j:Ljava/lang/String;

.field private static k:Ljava/lang/String;

.field private static volatile l:Lcom/baidu/mobads/utils/t;


# instance fields
.field public d:Lorg/json/JSONArray;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:I

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/baidu/mobads/utils/t;->b:Ljava/util/List;

    .line 74
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    sput-object v0, Lcom/baidu/mobads/utils/t;->c:Lorg/json/JSONArray;

    const/4 v0, 0x0

    .line 80
    sput-object v0, Lcom/baidu/mobads/utils/t;->l:Lcom/baidu/mobads/utils/t;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/t;->d:Lorg/json/JSONArray;

    const-string v0, ""

    .line 104
    iput-object v0, p0, Lcom/baidu/mobads/utils/t;->m:Ljava/lang/String;

    const-string v0, ""

    .line 105
    iput-object v0, p0, Lcom/baidu/mobads/utils/t;->n:Ljava/lang/String;

    const/4 v0, -0x1

    .line 480
    iput v0, p0, Lcom/baidu/mobads/utils/t;->p:I

    const-string v0, ""

    .line 1031
    iput-object v0, p0, Lcom/baidu/mobads/utils/t;->t:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/utils/t;)I
    .locals 2

    .line 45
    iget v0, p0, Lcom/baidu/mobads/utils/t;->u:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/baidu/mobads/utils/t;->u:I

    return v0
.end method

.method private a(Ljava/io/File;)J
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .line 432
    :try_start_0
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 440
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result p1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    mul-int p1, p1, v0

    div-int/lit16 p1, p1, 0x400

    div-int/lit16 p1, p1, 0x400
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v0, p1

    return-wide v0

    :catch_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public static a()Lcom/baidu/mobads/utils/t;
    .locals 2

    .line 87
    sget-object v0, Lcom/baidu/mobads/utils/t;->l:Lcom/baidu/mobads/utils/t;

    if-nez v0, :cond_1

    .line 88
    const-class v0, Lcom/baidu/mobads/utils/t;

    monitor-enter v0

    .line 89
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/utils/t;->l:Lcom/baidu/mobads/utils/t;

    if-nez v1, :cond_0

    .line 90
    new-instance v1, Lcom/baidu/mobads/utils/t;

    invoke-direct {v1}, Lcom/baidu/mobads/utils/t;-><init>()V

    sput-object v1, Lcom/baidu/mobads/utils/t;->l:Lcom/baidu/mobads/utils/t;

    .line 92
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 94
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobads/utils/t;->l:Lcom/baidu/mobads/utils/t;

    return-object v0
.end method

.method private a(Landroid/content/Context;I)Ljava/lang/Boolean;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "connectivity"

    .line 697
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    const-string v2, "android.permission.ACCESS_NETWORK_STATE"

    .line 699
    invoke-virtual {p1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result p1

    const/4 v2, 0x1

    if-eqz p1, :cond_0

    .line 700
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p1

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const-string v1, "Utils"

    aput-object v1, p2, v0

    const-string v1, "no permission android.permission.ACCESS_NETWORK_STATE"

    aput-object v1, p2, v2

    .line 701
    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e([Ljava/lang/Object;)I

    .line 702
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 704
    :cond_0
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 705
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, p2, :cond_1

    .line 706
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 705
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 708
    :catch_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/baidu/mobads/utils/t;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/utils/t;->m:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 45
    sput-object p0, Lcom/baidu/mobads/utils/t;->f:Ljava/lang/String;

    return-object p0
.end method

.method private a(Ljava/io/Closeable;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1024
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1027
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method private b(Ljava/io/File;)J
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .line 452
    :try_start_0
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 461
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result p1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    mul-int p1, p1, v0

    div-int/lit16 p1, p1, 0x400

    div-int/lit16 p1, p1, 0x400
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v0, p1

    return-wide v0

    :catch_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .line 45
    sget-object v0, Lcom/baidu/mobads/utils/t;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/baidu/mobads/utils/t;)Ljava/lang/String;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/baidu/mobads/utils/t;->t:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 45
    sput-object p0, Lcom/baidu/mobads/utils/t;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobads/utils/t;Ljava/lang/String;)Z
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/baidu/mobads/utils/t;->e(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .line 45
    sget-object v0, Lcom/baidu/mobads/utils/t;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/baidu/mobads/utils/t;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/utils/t;->n:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 45
    sput-object p0, Lcom/baidu/mobads/utils/t;->j:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .line 45
    sget-object v0, Lcom/baidu/mobads/utils/t;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/baidu/mobads/utils/t;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/utils/t;->t:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 45
    sput-object p0, Lcom/baidu/mobads/utils/t;->k:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .line 45
    sget-object v0, Lcom/baidu/mobads/utils/t;->k:Ljava/lang/String;

    return-object v0
.end method

.method private e(Ljava/lang/String;)Z
    .locals 4

    .line 114
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    goto :goto_0

    .line 118
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_2

    .line 119
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-eq v2, v3, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_2
    return v1

    :cond_3
    :goto_0
    return v1
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1035
    iget-object v0, p0, Lcom/baidu/mobads/utils/t;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/baidu/mobads/utils/t;->u:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    goto :goto_0

    .line 1038
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/utils/z;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/utils/z;-><init>(Lcom/baidu/mobads/utils/t;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    .line 1053
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->t:Ljava/lang/String;

    return-object p1

    .line 1036
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->t:Ljava/lang/String;

    return-object p1
.end method

.method public canSupportSdcardStroage(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    .line 891
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    .line 892
    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    .line 893
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1c

    if-gt v2, v3, :cond_0

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-interface {v1, p1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->hasPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 895
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/t;->isUseOldStoragePath()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    :catch_0
    return v0
.end method

.method public getAllExternalMemorySize()J
    .locals 4

    const-wide/16 v0, -0x1

    .line 371
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mounted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 374
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 373
    invoke-direct {p0, v2}, Lcom/baidu/mobads/utils/t;->a(Ljava/io/File;)J

    move-result-wide v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v2

    :cond_0
    return-wide v0

    :catch_0
    return-wide v0
.end method

.method public getAllInternalMemorySize()J
    .locals 2

    .line 390
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/utils/t;->a(Ljava/io/File;)J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getAndroidId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    :try_start_0
    const-string v0, "permission_read_phone_state"

    .line 290
    invoke-static {v0}, Lcom/baidu/mobads/MobadsPermissionSettings;->hasPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/utils/t;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    .line 294
    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 298
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    .line 299
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/utils/t;->o:Ljava/lang/String;

    .line 301
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->o:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const-string p1, ""

    return-object p1
.end method

.method public getAppSDC()Ljava/lang/String;
    .locals 6

    .line 314
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    const-string v1, "sysSdc"

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 316
    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 319
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    .line 321
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    .line 325
    :try_start_1
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/t;->getAvailableExternalMemorySize()J

    move-result-wide v1

    .line 326
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/t;->getAllExternalMemorySize()J

    move-result-wide v3

    .line 327
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 328
    :try_start_2
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    const-string v2, "sysSdc"

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object v1

    :catch_1
    move-object v0, v1

    :catch_2
    return-object v0

    :cond_1
    const-string v0, "0,0"

    return-object v0
.end method

.method public getAvailableExternalMemorySize()J
    .locals 4

    const-wide/16 v0, -0x1

    .line 403
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mounted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 406
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 405
    invoke-direct {p0, v2}, Lcom/baidu/mobads/utils/t;->b(Ljava/io/File;)J

    move-result-wide v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v2

    :cond_0
    return-wide v0

    :catch_0
    return-wide v0
.end method

.method public getAvailableInternalMemorySize()J
    .locals 2

    .line 421
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/utils/t;->b(Ljava/io/File;)J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getBackgroundBrowsers(Landroid/content/Context;)Lorg/json/JSONArray;
    .locals 7

    .line 924
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    .line 926
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    .line 927
    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/l;->getSupportedBrowsers()[Ljava/lang/String;

    move-result-object v1

    const-string v2, "activity"

    .line 929
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    .line 931
    invoke-virtual {v2}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 932
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 933
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    .line 934
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 935
    iget-object v4, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 936
    iget-object v4, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    const/16 v5, 0x80

    invoke-virtual {p1, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    .line 939
    :goto_0
    array-length v5, v1

    if-ge v4, v5, :cond_0

    .line 940
    iget-object v5, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    aget-object v6, v1, v4

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 941
    iget-object v5, p0, Lcom/baidu/mobads/utils/t;->d:Lorg/json/JSONArray;

    iget-object v6, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 948
    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    .line 950
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bgBrowsers:"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/utils/t;->d:Lorg/json/JSONArray;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;)I

    .line 951
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->d:Lorg/json/JSONArray;

    return-object p1
.end method

.method public getCUID(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 208
    sget-object v0, Lcom/baidu/mobads/utils/t;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/utils/v;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/utils/v;-><init>(Lcom/baidu/mobads/utils/t;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    .line 237
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    .line 238
    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/utils/t;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getCell(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "permission_location"

    .line 804
    invoke-static {v0}, Lcom/baidu/mobads/MobadsPermissionSettings;->hasPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 805
    sget-object p1, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    if-nez p1, :cond_0

    .line 806
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    sput-object p1, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    goto :goto_0

    .line 808
    :cond_0
    sget-object p1, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 810
    :goto_0
    sget-object p1, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    return-object p1

    .line 813
    :cond_1
    sget-object v0, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    :try_start_0
    const-string v0, "phone"

    .line 816
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/telephony/TelephonyManager;

    .line 817
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object p1

    .line 818
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/utils/y;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/utils/y;-><init>(Lcom/baidu/mobads/utils/t;Landroid/telephony/CellLocation;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 876
    :catch_0
    :cond_3
    sget-object p1, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    return-object p1
.end method

.method public getCurrentProcessId(Landroid/content/Context;)I
    .locals 0

    .line 1014
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method

.method public getCurrentProcessName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    :try_start_0
    const-string v0, "activity"

    .line 990
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/ActivityManager;

    .line 991
    iget-object v0, p0, Lcom/baidu/mobads/utils/t;->s:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 992
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    if-eqz p1, :cond_1

    .line 995
    invoke-virtual {p1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 997
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 998
    iget v2, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v2, v0, :cond_0

    .line 999
    iget-object v1, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    iput-object v1, p0, Lcom/baidu/mobads/utils/t;->s:Ljava/lang/String;

    goto :goto_0

    .line 1005
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->s:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 1007
    :catch_0
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->s:Ljava/lang/String;

    return-object p1
.end method

.method public getDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 11

    const-string v0, "permission_read_phone_state"

    .line 719
    invoke-static {v0}, Lcom/baidu/mobads/MobadsPermissionSettings;->hasPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 723
    :cond_0
    sget-object v0, Lcom/baidu/mobads/utils/t;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    :try_start_0
    const-string v0, "__x_adsdk_agent_header__"

    const/4 v1, 0x0

    .line 725
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 727
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;

    move-result-object v2

    const-string v3, ""

    .line 729
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 731
    sget-wide v6, Lcom/baidu/mobads/utils/t;->h:J

    const-wide/16 v8, 0x0

    cmp-long v10, v6, v8

    if-gez v10, :cond_3

    const-string v3, "IA-Vuifb"

    .line 732
    invoke-interface {v0, v3, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    sput-wide v6, Lcom/baidu/mobads/utils/t;->h:J

    .line 734
    sget-wide v6, Lcom/baidu/mobads/utils/t;->h:J

    cmp-long v3, v6, v8

    if-nez v3, :cond_2

    const-string v3, "deviceid"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "deviceid"

    const-string v6, ""

    .line 735
    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 736
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "deviceid"

    .line 737
    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 738
    invoke-direct {p0, v3}, Lcom/baidu/mobads/utils/t;->e(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "uANvpyP-pyfb"

    .line 739
    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IBase64;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v6, v7, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 741
    :cond_1
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    const-string v3, "uANvpyP-pyfb"

    const-string v6, ""

    .line 744
    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/utils/IBase64;->decodeStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 745
    invoke-direct {p0, v3}, Lcom/baidu/mobads/utils/t;->e(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 746
    sput-object v3, Lcom/baidu/mobads/utils/t;->g:Ljava/lang/String;

    .line 750
    :cond_3
    sget-wide v6, Lcom/baidu/mobads/utils/t;->h:J

    const/4 v8, 0x0

    sub-long v6, v4, v6

    const-wide/32 v8, 0x7b98a00

    cmp-long v10, v6, v8

    if-lez v10, :cond_5

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x1d

    if-ge v6, v7, :cond_5

    .line 752
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v6, "phone"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/telephony/TelephonyManager;

    const-string v6, "uvNYwANvpyP-iyfb"

    .line 753
    invoke-interface {v2, v6}, Lcom/baidu/mobads/interfaces/utils/IBase64;->decodeStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 754
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v7

    invoke-virtual {v7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v7

    .line 755
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7, p1, v6, v1}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 756
    sput-wide v4, Lcom/baidu/mobads/utils/t;->h:J

    .line 757
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "IA-Vuifb"

    .line 758
    sget-wide v4, Lcom/baidu/mobads/utils/t;->h:J

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 760
    invoke-direct {p0, p1}, Lcom/baidu/mobads/utils/t;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "uANvpyP-pyfb"

    .line 762
    invoke-interface {v2, p1}, Lcom/baidu/mobads/interfaces/utils/IBase64;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 764
    sput-object p1, Lcom/baidu/mobads/utils/t;->g:Ljava/lang/String;

    .line 766
    :cond_4
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 769
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    .line 772
    :cond_5
    :goto_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/utils/t;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getEncodedSN(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 780
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/utils/t;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;

    move-result-object v0

    .line 782
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/t;->getSn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 783
    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IBase64;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/utils/t;->r:Ljava/lang/String;

    .line 785
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->r:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 787
    :catch_0
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->r:Ljava/lang/String;

    return-object p1
.end method

.method public getGPS(Landroid/content/Context;)[D
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getGUID(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 251
    sget-object v0, Lcom/baidu/mobads/utils/t;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/utils/w;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/utils/w;-><init>(Lcom/baidu/mobads/utils/t;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    .line 281
    :cond_0
    sget-object p1, Lcom/baidu/mobads/utils/t;->j:Ljava/lang/String;

    return-object p1
.end method

.method public getHttpConnection(Landroid/content/Context;Ljava/lang/String;II)Ljava/net/HttpURLConnection;
    .locals 1

    const/4 p1, 0x0

    .line 959
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p2

    check-cast p2, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 960
    :try_start_1
    invoke-virtual {p2, p3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 961
    invoke-virtual {p2, p4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object p2

    :catch_0
    move-object p1, p2

    :catch_1
    return-object p1
.end method

.method public getIMEI(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "permission_read_phone_state"

    .line 130
    invoke-static {v0}, Lcom/baidu/mobads/MobadsPermissionSettings;->hasPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 133
    :cond_0
    sget-object v0, Lcom/baidu/mobads/utils/t;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 136
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    const-string v1, "android.permission.READ_PHONE_STATE"

    .line 137
    invoke-interface {v0, p1, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->hasPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1"

    .line 139
    iput-object v0, p0, Lcom/baidu/mobads/utils/t;->n:Ljava/lang/String;

    .line 140
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/t;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 142
    invoke-direct {p0, v0}, Lcom/baidu/mobads/utils/t;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string p1, "0"

    .line 143
    iput-object p1, p0, Lcom/baidu/mobads/utils/t;->m:Ljava/lang/String;

    .line 144
    sput-object v0, Lcom/baidu/mobads/utils/t;->f:Ljava/lang/String;

    .line 145
    sget-object p1, Lcom/baidu/mobads/utils/t;->f:Ljava/lang/String;

    return-object p1

    .line 149
    :cond_1
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/utils/u;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/utils/u;-><init>(Lcom/baidu/mobads/utils/t;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    .line 188
    :cond_2
    sget-object p1, Lcom/baidu/mobads/utils/t;->f:Ljava/lang/String;

    return-object p1
.end method

.method public getIp(Landroid/content/Context;)Ljava/lang/String;
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x3
    .end annotation

    const-string p1, ""

    return-object p1
.end method

.method public getMacAddress(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    const-string p1, ""

    return-object p1
.end method

.method public getMaxCpuFreq()Ljava/lang/String;
    .locals 5

    .line 489
    iget v0, p0, Lcom/baidu/mobads/utils/t;->p:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    .line 494
    :try_start_0
    new-instance v1, Ljava/io/FileReader;

    const-string v2, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"

    invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 495
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 497
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/baidu/mobads/utils/t;->p:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v2

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_2

    :catch_1
    move-exception v2

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_2

    :catch_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    move-object v1, v2

    .line 499
    :goto_0
    :try_start_3
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 501
    :goto_1
    invoke-direct {p0, v1}, Lcom/baidu/mobads/utils/t;->a(Ljava/io/Closeable;)V

    .line 502
    invoke-direct {p0, v2}, Lcom/baidu/mobads/utils/t;->a(Ljava/io/Closeable;)V

    goto :goto_3

    :catchall_2
    move-exception v0

    .line 501
    :goto_2
    invoke-direct {p0, v1}, Lcom/baidu/mobads/utils/t;->a(Ljava/io/Closeable;)V

    .line 502
    invoke-direct {p0, v2}, Lcom/baidu/mobads/utils/t;->a(Ljava/io/Closeable;)V

    throw v0

    .line 505
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/baidu/mobads/utils/t;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMem()Ljava/lang/String;
    .locals 6

    .line 345
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    const-string v1, "sysMem"

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 347
    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 350
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    :cond_0
    const-string v0, ""

    .line 354
    :try_start_1
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/t;->getAvailableInternalMemorySize()J

    move-result-wide v1

    .line 355
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/t;->getAllInternalMemorySize()J

    move-result-wide v3

    .line 356
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 357
    :try_start_2
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    const-string v2, "sysMem"

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object v1

    :catch_1
    move-object v0, v1

    :catch_2
    return-object v0
.end method

.method public getNetType(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    const-string p1, ""

    return-object p1
.end method

.method public getNetworkCatagory(Landroid/content/Context;)I
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "connectivity"

    .line 615
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    .line 616
    invoke-virtual {p1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object p1

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-eqz p1, :cond_3

    .line 617
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 618
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    if-ne v3, v2, :cond_0

    const/16 v0, 0x64

    goto :goto_2

    .line 620
    :cond_0
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    if-nez v3, :cond_3

    .line 621
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v3

    .line 624
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    if-eqz v3, :cond_2

    const-string p1, "TD-SCDMA"

    goto :goto_1

    :cond_1
    :goto_0
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_2

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_2

    :cond_2
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_2

    .line 660
    :goto_1
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "WCDMA"

    .line 661
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "CDMA2000"

    .line 662
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_3
    :goto_2
    return v0

    :catch_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getNetworkOperator(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    :try_start_0
    const-string v0, "phone"

    .line 558
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/telephony/TelephonyManager;

    .line 559
    iget-object v0, p0, Lcom/baidu/mobads/utils/t;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    .line 562
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/utils/t;->q:Ljava/lang/String;

    .line 564
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->q:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 566
    :catch_0
    iget-object p1, p0, Lcom/baidu/mobads/utils/t;->q:Ljava/lang/String;

    return-object p1
.end method

.method public getNetworkOperatorName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 511
    sget-object v0, Lcom/baidu/mobads/utils/t;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "phone"

    .line 512
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/telephony/TelephonyManager;

    .line 513
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/utils/x;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/utils/x;-><init>(Lcom/baidu/mobads/utils/t;Landroid/telephony/TelephonyManager;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    .line 549
    :cond_0
    sget-object p1, Lcom/baidu/mobads/utils/t;->k:Ljava/lang/String;

    return-object p1
.end method

.method public getNetworkType(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0x3
    .end annotation

    const-string v0, "none"

    const/4 v1, 0x1

    :try_start_0
    const-string v2, "connectivity"

    .line 588
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    .line 589
    invoke-virtual {p1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 590
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 591
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v1, :cond_0

    const-string p1, "wifi"

    move-object v0, p1

    goto :goto_1

    :cond_0
    const-string v2, "unknown"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 595
    :try_start_1
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 596
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_1
    move-object v0, v2

    goto :goto_1

    :catch_0
    move-exception p1

    move-object v0, v2

    goto :goto_0

    :catch_1
    move-exception p1

    .line 601
    :goto_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    invoke-interface {v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i([Ljava/lang/Object;)I

    :cond_2
    :goto_1
    return-object v0
.end method

.method public getPhoneOSBrand()Ljava/lang/String;
    .locals 2

    .line 798
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneOSBuildVersionSdk()Ljava/lang/String;
    .locals 2

    .line 575
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSn(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 194
    :try_start_0
    sget-object v0, Lcom/baidu/mobads/utils/t;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/t;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/t;->getMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 197
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/baidu/mobads/utils/t;->i:Ljava/lang/String;

    .line 199
    :cond_1
    sget-object p1, Lcom/baidu/mobads/utils/t;->i:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const-string p1, ""

    return-object p1
.end method

.method public getSnFrom(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 109
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/baidu/mobads/utils/t;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/baidu/mobads/utils/t;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getWIFI(Landroid/content/Context;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 881
    sget-object p1, Lcom/baidu/mobads/utils/t;->b:Ljava/util/List;

    return-object p1
.end method

.method public getWifiConnected(Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    const-string p1, ""

    return-object p1
.end method

.method public getWifiScans(Landroid/content/Context;)Lorg/json/JSONArray;
    .locals 0

    .line 919
    sget-object p1, Lcom/baidu/mobads/utils/t;->c:Lorg/json/JSONArray;

    return-object p1
.end method

.method public is3GConnected(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    .line 685
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/utils/t;->a(Landroid/content/Context;I)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public isCurrentNetworkAvailable(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "connectivity"

    .line 974
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    .line 975
    invoke-virtual {p1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 976
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 978
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    const-string v2, "isCurrentNetworkAvailable"

    .line 979
    invoke-interface {v1, v2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return v0
.end method

.method public isTablet(Landroid/content/Context;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x4
    .end annotation

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 p1, p1, 0xf

    const/4 v0, 0x3

    if-lt p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isUseOldStoragePath()Z
    .locals 2

    .line 909
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x1

    .line 680
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/utils/t;->a(Landroid/content/Context;I)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
