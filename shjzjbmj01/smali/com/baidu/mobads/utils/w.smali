.class Lcom/baidu/mobads/utils/w;
.super Lcom/baidu/mobads/f/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/baidu/mobads/f/a<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic c:Lcom/baidu/mobads/utils/t;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/utils/t;Landroid/content/Context;)V
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/baidu/mobads/utils/w;->c:Lcom/baidu/mobads/utils/t;

    iput-object p2, p0, Lcom/baidu/mobads/utils/w;->a:Landroid/content/Context;

    invoke-direct {p0}, Lcom/baidu/mobads/f/a;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a()Ljava/lang/Object;
    .locals 1

    .line 253
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/w;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 5

    .line 258
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/t;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/baidu/mobads/utils/w;->a:Landroid/content/Context;

    if-eqz v0, :cond_3

    .line 259
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    .line 260
    iget-object v1, p0, Lcom/baidu/mobads/utils/w;->a:Landroid/content/Context;

    const-string v2, "__x_adsdk_agent_header__"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "guid"

    const-string v4, ""

    .line 261
    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 260
    invoke-static {v1}, Lcom/baidu/mobads/utils/t;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 262
    invoke-static {}, Lcom/baidu/mobads/utils/t;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/baidu/mobads/utils/t;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_3

    .line 263
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/baidu/mobads/utils/w;->c:Lcom/baidu/mobads/utils/t;

    iget-object v4, p0, Lcom/baidu/mobads/utils/w;->a:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/baidu/mobads/utils/t;->getMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/utils/w;->c:Lcom/baidu/mobads/utils/t;

    iget-object v4, p0, Lcom/baidu/mobads/utils/w;->a:Landroid/content/Context;

    .line 264
    invoke-virtual {v2, v4}, Lcom/baidu/mobads/utils/t;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 263
    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/utils/t;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 265
    invoke-static {}, Lcom/baidu/mobads/utils/t;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/baidu/mobads/utils/t;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    goto :goto_0

    .line 269
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/utils/w;->a:Landroid/content/Context;

    const-string v1, "__x_adsdk_agent_header__"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 270
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "guid"

    invoke-static {}, Lcom/baidu/mobads/utils/t;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    :cond_2
    :goto_0
    const-string v0, ""

    .line 266
    invoke-static {v0}, Lcom/baidu/mobads/utils/t;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 267
    invoke-static {}, Lcom/baidu/mobads/utils/t;->d()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const-string v0, ""

    .line 274
    invoke-static {v0}, Lcom/baidu/mobads/utils/t;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 277
    :cond_3
    :goto_1
    invoke-static {}, Lcom/baidu/mobads/utils/t;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
