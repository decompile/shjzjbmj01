.class Lcom/baidu/mobads/utils/y;
.super Lcom/baidu/mobads/f/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/baidu/mobads/f/a<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/telephony/CellLocation;

.field final synthetic c:Lcom/baidu/mobads/utils/t;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/utils/t;Landroid/telephony/CellLocation;)V
    .locals 0

    .line 818
    iput-object p1, p0, Lcom/baidu/mobads/utils/y;->c:Lcom/baidu/mobads/utils/t;

    iput-object p2, p0, Lcom/baidu/mobads/utils/y;->a:Landroid/telephony/CellLocation;

    invoke-direct {p0}, Lcom/baidu/mobads/f/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 8

    .line 824
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    const-string v1, "cell"

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 826
    check-cast v0, Ljava/util/List;

    sput-object v0, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    .line 827
    sget-object v0, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 830
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    .line 835
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobads/utils/y;->a:Landroid/telephony/CellLocation;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    .line 836
    new-array v1, v0, [Ljava/lang/String;

    .line 838
    iget-object v2, p0, Lcom/baidu/mobads/utils/y;->a:Landroid/telephony/CellLocation;

    instance-of v2, v2, Landroid/telephony/gsm/GsmCellLocation;

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_1

    .line 839
    iget-object v0, p0, Lcom/baidu/mobads/utils/y;->a:Landroid/telephony/CellLocation;

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    .line 840
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 841
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    const-string v0, "0"

    aput-object v0, v1, v3

    goto :goto_0

    .line 855
    :cond_1
    iget-object v2, p0, Lcom/baidu/mobads/utils/y;->a:Landroid/telephony/CellLocation;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v6, "["

    const-string v7, ""

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "]"

    const-string v7, ""

    .line 856
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 857
    aget-object v6, v2, v5

    aput-object v6, v1, v5

    .line 858
    aget-object v0, v2, v0

    aput-object v0, v1, v4

    const/4 v0, 0x4

    .line 859
    aget-object v0, v2, v0

    aput-object v0, v1, v3

    .line 862
    :goto_0
    sget-object v0, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 864
    :cond_2
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    const-string v1, "cell"

    sget-object v2, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    .line 865
    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 869
    :catch_1
    sget-object v0, Lcom/baidu/mobads/utils/t;->a:Ljava/util/List;

    return-object v0
.end method
