.class Lcom/baidu/mobads/utils/m$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/utils/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobads/utils/m;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/String;

.field private d:J


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/utils/m;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 2

    .line 190
    iput-object p1, p0, Lcom/baidu/mobads/utils/m$a;->a:Lcom/baidu/mobads/utils/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/utils/m$a;->d:J

    .line 191
    iput-object p2, p0, Lcom/baidu/mobads/utils/m$a;->b:Landroid/os/Handler;

    .line 192
    iput-object p3, p0, Lcom/baidu/mobads/utils/m$a;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 10

    .line 197
    check-cast p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    .line 198
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/baidu/mobads/utils/m$a;->d:J

    sub-long/2addr v0, v2

    .line 199
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p2

    sget-object v2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne p2, v2, :cond_0

    .line 200
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getOutputPath()Ljava/lang/String;

    move-result-object p2

    .line 201
    iget-object v2, p0, Lcom/baidu/mobads/utils/m$a;->a:Lcom/baidu/mobads/utils/m;

    invoke-virtual {v2, p2}, Lcom/baidu/mobads/utils/m;->c(Ljava/lang/String;)V

    .line 202
    iget-object p2, p0, Lcom/baidu/mobads/utils/m$a;->a:Lcom/baidu/mobads/utils/m;

    iget-object v2, p0, Lcom/baidu/mobads/utils/m$a;->b:Landroid/os/Handler;

    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getOutputPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v2, v3, v0, v1}, Lcom/baidu/mobads/utils/m;->a(Lcom/baidu/mobads/utils/m;Landroid/os/Handler;Ljava/lang/String;J)V

    .line 203
    iget-object p2, p0, Lcom/baidu/mobads/utils/m$a;->a:Lcom/baidu/mobads/utils/m;

    iget-object v2, p0, Lcom/baidu/mobads/utils/m$a;->c:Ljava/lang/String;

    invoke-virtual {p2, v2}, Lcom/baidu/mobads/utils/m;->a(Ljava/lang/String;)V

    .line 205
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p1

    sget-object p2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne p1, p2, :cond_1

    .line 206
    iget-object v4, p0, Lcom/baidu/mobads/utils/m$a;->a:Lcom/baidu/mobads/utils/m;

    iget-object v5, p0, Lcom/baidu/mobads/utils/m$a;->b:Landroid/os/Handler;

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    move-wide v8, v0

    invoke-static/range {v4 .. v9}, Lcom/baidu/mobads/utils/m;->a(Lcom/baidu/mobads/utils/m;Landroid/os/Handler;Ljava/lang/Boolean;Ljava/lang/String;J)V

    .line 207
    iget-object p1, p0, Lcom/baidu/mobads/utils/m$a;->a:Lcom/baidu/mobads/utils/m;

    iget-object p2, p0, Lcom/baidu/mobads/utils/m$a;->b:Landroid/os/Handler;

    invoke-static {p1, p2, v0, v1}, Lcom/baidu/mobads/utils/m;->a(Lcom/baidu/mobads/utils/m;Landroid/os/Handler;J)V

    .line 208
    iget-object p1, p0, Lcom/baidu/mobads/utils/m$a;->a:Lcom/baidu/mobads/utils/m;

    iget-object p2, p0, Lcom/baidu/mobads/utils/m$a;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/utils/m;->a(Ljava/lang/String;)V

    :cond_1
    return-void
.end method
