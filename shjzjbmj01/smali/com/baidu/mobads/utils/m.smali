.class public Lcom/baidu/mobads/utils/m;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/utils/m$a;
    }
.end annotation


# static fields
.field private static a:J = 0x1c9c380L


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/baidu/mobads/utils/m;->b:Landroid/content/Context;

    return-void
.end method

.method static synthetic a()J
    .locals 2

    .line 21
    sget-wide v0, Lcom/baidu/mobads/utils/m;->a:J

    return-wide v0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "__bidu_cache_dir"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/baidu/mobads/utils/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/baidu/mobads/utils/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(I)V
    .locals 2

    mul-int/lit16 p0, p0, 0x3e8

    mul-int/lit16 p0, p0, 0x3e8

    int-to-long v0, p0

    .line 56
    sput-wide v0, Lcom/baidu/mobads/utils/m;->a:J

    return-void
.end method

.method private a(Landroid/os/Handler;J)V
    .locals 7

    const/4 v0, 0x0

    .line 139
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/baidu/mobads/utils/m;->a(Landroid/os/Handler;Ljava/lang/Boolean;Ljava/lang/String;J)V

    return-void
.end method

.method private a(Landroid/os/Handler;Ljava/lang/Boolean;Ljava/lang/String;J)V
    .locals 3

    .line 157
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    .line 158
    iput v1, v0, Landroid/os/Message;->what:I

    .line 159
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "caching_result"

    .line 160
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string p2, "caching_time_consume"

    .line 161
    invoke-virtual {v1, p2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string p2, "local_creative_url"

    .line 162
    invoke-virtual {v1, p2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 164
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 166
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method private a(Landroid/os/Handler;Ljava/lang/String;J)V
    .locals 7

    const/4 v0, 0x1

    .line 135
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-wide v5, p3

    invoke-direct/range {v1 .. v6}, Lcom/baidu/mobads/utils/m;->a(Landroid/os/Handler;Ljava/lang/Boolean;Ljava/lang/String;J)V

    return-void
.end method

.method private a(Landroid/os/Handler;Z)V
    .locals 3

    .line 144
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    .line 145
    iput v1, v0, Landroid/os/Message;->what:I

    .line 146
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "caching_file_exist"

    .line 147
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 148
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 149
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 151
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/utils/m;Landroid/os/Handler;J)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/utils/m;->a(Landroid/os/Handler;J)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/utils/m;Landroid/os/Handler;Ljava/lang/Boolean;Ljava/lang/String;J)V
    .locals 0

    .line 21
    invoke-direct/range {p0 .. p5}, Lcom/baidu/mobads/utils/m;->a(Landroid/os/Handler;Ljava/lang/Boolean;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/utils/m;Landroid/os/Handler;Ljava/lang/String;J)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/baidu/mobads/utils/m;->a(Landroid/os/Handler;Ljava/lang/String;J)V

    return-void
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 105
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/utils/h;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 3

    const-wide/16 v0, -0x1

    .line 115
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-static {p1, p2}, Lcom/baidu/mobads/utils/m;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 117
    invoke-direct {p0, p5, p1}, Lcom/baidu/mobads/utils/m;->a(Landroid/os/Handler;Z)V

    .line 118
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 119
    iget-object p2, p0, Lcom/baidu/mobads/utils/m;->b:Landroid/content/Context;

    invoke-static {p2}, Lcom/baidu/mobads/openad/download/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/openad/download/a;

    move-result-object p2

    .line 120
    invoke-virtual {p2, v2, p3, p4, p1}, Lcom/baidu/mobads/openad/download/a;->createSimpleFileDownloader(Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    move-result-object p1

    .line 121
    new-instance p2, Lcom/baidu/mobads/utils/m$a;

    invoke-direct {p2, p0, p5, p3}, Lcom/baidu/mobads/utils/m$a;-><init>(Lcom/baidu/mobads/utils/m;Landroid/os/Handler;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->addObserver(Ljava/util/Observer;)V

    .line 122
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->start()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 124
    invoke-direct {p0, p5, p1}, Lcom/baidu/mobads/utils/m;->a(Landroid/os/Handler;Z)V

    .line 125
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p5, p1, v0, v1}, Lcom/baidu/mobads/utils/m;->a(Landroid/os/Handler;Ljava/lang/String;J)V

    .line 126
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    invoke-virtual {v2, p1, p2}, Ljava/io/File;->setLastModified(J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 130
    :catch_0
    invoke-direct {p0, p5, v0, v1}, Lcom/baidu/mobads/utils/m;->a(Landroid/os/Handler;J)V

    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .line 60
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/utils/n;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/utils/n;-><init>(Lcom/baidu/mobads/utils/m;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 3

    .line 176
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 178
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setLastModified(J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method
