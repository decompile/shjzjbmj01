.class public Lcom/baidu/mobads/utils/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/utils/IXAdLogger;


# static fields
.field private static volatile a:Lcom/baidu/mobads/utils/q;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/baidu/mobads/utils/q;
    .locals 2

    .line 10
    sget-object v0, Lcom/baidu/mobads/utils/q;->a:Lcom/baidu/mobads/utils/q;

    if-nez v0, :cond_1

    .line 11
    const-class v0, Lcom/baidu/mobads/utils/q;

    monitor-enter v0

    .line 12
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/utils/q;->a:Lcom/baidu/mobads/utils/q;

    if-nez v1, :cond_0

    .line 13
    new-instance v1, Lcom/baidu/mobads/utils/q;

    invoke-direct {v1}, Lcom/baidu/mobads/utils/q;-><init>()V

    sput-object v1, Lcom/baidu/mobads/utils/q;->a:Lcom/baidu/mobads/utils/q;

    .line 15
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 17
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobads/utils/q;->a:Lcom/baidu/mobads/utils/q;

    return-object v0
.end method

.method private a([Ljava/lang/Object;)Ljava/lang/String;
    .locals 4

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 33
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 35
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public d(Ljava/lang/String;)I
    .locals 1

    const-string v0, "BaiduXAdSDK"

    .line 49
    invoke-virtual {p0, v0, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x3

    .line 54
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    .line 58
    :cond_0
    :try_start_0
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method public d(Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 2

    const/4 v0, 0x3

    .line 71
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    const-string v0, "BaiduXAdSDK"

    .line 75
    invoke-static {v0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method public d(Ljava/lang/Throwable;)I
    .locals 1

    const-string v0, ""

    .line 66
    invoke-virtual {p0, v0, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    move-result p1

    return p1
.end method

.method public varargs d([Ljava/lang/Object;)I
    .locals 1

    const/4 v0, 0x3

    .line 40
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 44
    :cond_0
    invoke-direct {p0, p1}, Lcom/baidu/mobads/utils/q;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public e(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x6

    .line 130
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    const-string v0, "BaiduXAdSDK"

    .line 134
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method public e(Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 2

    const/4 v0, 0x6

    .line 147
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    const-string v0, "BaiduXAdSDK"

    .line 151
    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method public e(Ljava/lang/Throwable;)I
    .locals 1

    const-string v0, ""

    .line 142
    invoke-virtual {p0, v0, p1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    move-result p1

    return p1
.end method

.method public varargs e([Ljava/lang/Object;)I
    .locals 1

    const/4 v0, 0x6

    .line 121
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 125
    :cond_0
    invoke-direct {p0, p1}, Lcom/baidu/mobads/utils/q;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public i(Ljava/lang/String;)I
    .locals 1

    const-string v0, "BaiduXAdSDK"

    .line 159
    invoke-virtual {p0, v0, p1}, Lcom/baidu/mobads/utils/q;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x4

    .line 164
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    .line 168
    :cond_0
    :try_start_0
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method public i(Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 2

    const/4 v0, 0x4

    .line 185
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    const-string v0, "BaiduXAdSDK"

    .line 189
    invoke-static {v0, p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method public varargs i([Ljava/lang/Object;)I
    .locals 1

    const/4 v0, 0x4

    .line 176
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 180
    :cond_0
    invoke-direct {p0, p1}, Lcom/baidu/mobads/utils/q;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/q;->i(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public isLoggable(I)Z
    .locals 1

    const-string v0, "BaiduXAdSDK"

    .line 27
    invoke-virtual {p0, v0, p1}, Lcom/baidu/mobads/utils/q;->isLoggable(Ljava/lang/String;I)Z

    move-result p1

    return p1
.end method

.method public isLoggable(Ljava/lang/String;I)Z
    .locals 0

    .line 22
    sget p1, Lcom/baidu/mobads/constants/a;->b:I

    if-lt p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public w(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x5

    .line 83
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    const-string v0, "BaiduXAdSDK"

    .line 87
    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method public w(Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 2

    const/4 v0, 0x5

    .line 104
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    const/4 v1, -0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    const-string v0, "BaiduXAdSDK"

    .line 108
    invoke-static {v0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method public w(Ljava/lang/Throwable;)I
    .locals 1

    const-string v0, ""

    .line 116
    invoke-virtual {p0, v0, p1}, Lcom/baidu/mobads/utils/q;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    move-result p1

    return p1
.end method

.method public varargs w([Ljava/lang/Object;)I
    .locals 1

    const/4 v0, 0x5

    .line 95
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/q;->isLoggable(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 99
    :cond_0
    invoke-direct {p0, p1}, Lcom/baidu/mobads/utils/q;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/q;->w(Ljava/lang/String;)I

    move-result p1

    return p1
.end method
