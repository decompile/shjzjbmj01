.class public Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final o:Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;


# instance fields
.field private a:Lcom/baidu/mobads/utils/m;

.field private b:Lcom/baidu/mobads/utils/s;

.field private c:Lcom/baidu/mobads/interfaces/utils/IBase64;

.field private d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private e:Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;

.field private f:Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

.field private g:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

.field private h:Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

.field private i:Lcom/baidu/mobads/utils/r;

.field private j:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

.field private k:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

.field private l:Lcom/baidu/mobads/utils/h;

.field private m:Lcom/baidu/mobads/utils/l;

.field private n:Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

.field private p:Landroid/content/Context;

.field private q:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 59
    new-instance v0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;-><init>()V

    sput-object v0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->o:Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lcom/baidu/mobads/utils/a;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/a;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->c:Lcom/baidu/mobads/interfaces/utils/IBase64;

    .line 76
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 77
    new-instance v0, Lcom/baidu/mobads/utils/s;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/s;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->b:Lcom/baidu/mobads/utils/s;

    .line 78
    new-instance v0, Lcom/baidu/mobads/utils/ab;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/ab;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->e:Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;

    .line 79
    new-instance v0, Lcom/baidu/mobads/utils/g;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/g;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->f:Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

    .line 80
    new-instance v0, Lcom/baidu/mobads/utils/aa;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/aa;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->g:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    .line 81
    invoke-static {}, Lcom/baidu/mobads/utils/t;->a()Lcom/baidu/mobads/utils/t;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->k:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    .line 82
    new-instance v0, Lcom/baidu/mobads/utils/h;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/h;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->l:Lcom/baidu/mobads/utils/h;

    .line 83
    new-instance v0, Lcom/baidu/mobads/utils/p;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/p;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->h:Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    .line 84
    new-instance v0, Lcom/baidu/mobads/utils/r;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/r;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->i:Lcom/baidu/mobads/utils/r;

    .line 85
    new-instance v0, Lcom/baidu/mobads/utils/f;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/f;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->j:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    .line 87
    new-instance v0, Lcom/baidu/mobads/utils/l;

    invoke-direct {v0}, Lcom/baidu/mobads/utils/l;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->m:Lcom/baidu/mobads/utils/l;

    .line 89
    new-instance v0, Lcom/baidu/mobads/d/b;

    iget-object v1, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/d/b;-><init>(Lcom/baidu/mobads/interfaces/utils/IXAdLogger;)V

    iput-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->n:Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    return-void
.end method

.method public static getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;
    .locals 1

    .line 69
    sget-object v0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->o:Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    return-object v0
.end method


# virtual methods
.method public downloadApp(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 3

    .line 205
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download apk in proxy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, ""

    const-string v0, ""

    .line 208
    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public downloadAppSilence(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    .line 217
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->downloadApp(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method public getActivityUtils()Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->j:Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    return-object v0
.end method

.method public getAdConstants()Lcom/baidu/mobads/utils/l;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->m:Lcom/baidu/mobads/utils/l;

    return-object v0
.end method

.method public getAdContainerFactory()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->q:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    return-object v0
.end method

.method public getAdCreativeCacheManager()Lcom/baidu/mobads/utils/m;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->a:Lcom/baidu/mobads/utils/m;

    return-object v0
.end method

.method public getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    return-object v0
.end method

.method public getAdResource()Lcom/baidu/mobads/utils/s;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->b:Lcom/baidu/mobads/utils/s;

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->p:Landroid/content/Context;

    return-object v0
.end method

.method public getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->c:Lcom/baidu/mobads/interfaces/utils/IBase64;

    return-object v0
.end method

.method public getBitmapUtils()Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->f:Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

    return-object v0
.end method

.method public getCommonUtils()Lcom/baidu/mobads/utils/h;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->l:Lcom/baidu/mobads/utils/h;

    return-object v0
.end method

.method public getDownloaderManager()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;
    .locals 1

    .line 184
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/openad/download/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/openad/download/a;

    move-result-object v0

    return-object v0
.end method

.method public getDownloaderManager(Landroid/content/Context;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;
    .locals 0

    .line 180
    invoke-static {p1}, Lcom/baidu/mobads/openad/download/a;->a(Landroid/content/Context;)Lcom/baidu/mobads/openad/download/a;

    move-result-object p1

    return-object p1
.end method

.method public getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->n:Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    return-object v0
.end method

.method public getInstallIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .line 226
    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    move-result-object v0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/baidu/mobads/utils/r;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    return-object p1
.end method

.method public getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->h:Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    return-object v0
.end method

.method public getPackageUtils()Lcom/baidu/mobads/utils/r;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->i:Lcom/baidu/mobads/utils/r;

    return-object v0
.end method

.method public getProxyVer()Ljava/lang/String;
    .locals 1

    const-string v0, "8.8427"

    return-object v0
.end method

.method public getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->k:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    return-object v0
.end method

.method public getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->g:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    return-object v0
.end method

.method public getViewUtils()Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->e:Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;

    return-object v0
.end method

.method public initializeAdContainerFactory(Lcom/baidu/mobads/interfaces/IXAdContainerFactory;)V
    .locals 0

    if-nez p1, :cond_0

    .line 111
    iput-object p1, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->q:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    :cond_0
    return-void
.end method

.method public initializeApplicationContext(Landroid/content/Context;)V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->p:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 99
    iput-object p1, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->p:Landroid/content/Context;

    .line 101
    :cond_0
    new-instance p1, Lcom/baidu/mobads/utils/m;

    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->p:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/baidu/mobads/utils/m;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->a:Lcom/baidu/mobads/utils/m;

    return-void
.end method

.method public makeRequest(Ljava/lang/String;)V
    .locals 2

    .line 192
    new-instance v0, Lcom/baidu/mobads/openad/b/b;

    const-string v1, ""

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/openad/b/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 193
    iput p1, v0, Lcom/baidu/mobads/openad/b/b;->e:I

    .line 194
    new-instance p1, Lcom/baidu/mobads/openad/b/a;

    invoke-direct {p1}, Lcom/baidu/mobads/openad/b/a;-><init>()V

    .line 195
    invoke-virtual {p1, v0}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/b;)V

    return-void
.end method

.method public sendLog(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 221
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v2, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Ljava/util/HashMap;)V

    return-void
.end method

.method public setMobileConfirmed(Ljava/lang/String;)V
    .locals 3

    .line 188
    iget-object v0, p0, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->d:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMobileConfirmed in proxy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->i(Ljava/lang/String;)I

    return-void
.end method
