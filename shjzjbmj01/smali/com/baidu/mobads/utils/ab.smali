.class public Lcom/baidu/mobads/utils/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;)Z
    .locals 2

    .line 159
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    if-le p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public getViewState(Landroid/view/View;)I
    .locals 1

    .line 145
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/utils/ab;->isScreenOn(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    .line 147
    :cond_0
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/ab;->isAdViewShown(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    .line 149
    :cond_1
    invoke-direct {p0, p1}, Lcom/baidu/mobads/utils/ab;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 p1, 0x6

    goto :goto_0

    :cond_2
    const/16 v0, 0x32

    .line 151
    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/utils/ab;->isVisible(Landroid/view/View;I)Z

    move-result p1

    if-nez p1, :cond_3

    const/4 p1, 0x3

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getVisiblePercent(Landroid/view/View;Landroid/content/Context;)I
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 121
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    .line 123
    iget v1, p2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 124
    iget p2, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 125
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 126
    invoke-virtual {p1, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 127
    iget v3, v2, Landroid/graphics/Rect;->top:I

    if-gt v3, v1, :cond_0

    iget v1, v2, Landroid/graphics/Rect;->left:I

    if-gt v1, p2, :cond_0

    .line 128
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result p2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v1

    mul-int p2, p2, v1

    int-to-double v1, p2

    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    mul-int p2, p2, p1

    int-to-double p1, p2

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    .line 130
    invoke-static {v1, v2}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v1, v1, v3

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v1, p1

    double-to-int p1, v1

    return p1

    :cond_0
    return v0

    :catch_0
    return v0

    :cond_1
    return v0
.end method

.method public isAdViewOutsideScreen(Landroid/view/View;)Z
    .locals 6

    const/4 v0, 0x2

    .line 26
    new-array v1, v0, [I

    .line 27
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 28
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v2

    .line 29
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/baidu/mobads/utils/h;->getWindowRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v2

    const/4 v3, 0x0

    .line 30
    aget v4, v1, v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/2addr v5, v0

    add-int/2addr v4, v5

    const/4 v5, 0x1

    .line 31
    aget v1, v1, v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    div-int/2addr p1, v0

    add-int/2addr v1, p1

    if-lez v4, :cond_0

    .line 33
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result p1

    if-ge v4, p1, :cond_0

    if-lez v1, :cond_0

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result p1

    if-lt v1, p1, :cond_1

    :cond_0
    const/4 v3, 0x1

    :cond_1
    return v3
.end method

.method public isAdViewShown(Landroid/view/View;)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public isAdViewTooSmall(Landroid/view/View;)Z
    .locals 2

    if-eqz p1, :cond_1

    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    if-ge p1, v1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isScreenOn(Landroid/content/Context;)Z
    .locals 1

    :try_start_0
    const-string v0, "power"

    .line 56
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/PowerManager;

    .line 57
    invoke-virtual {p1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 59
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    const/4 p1, 0x1

    return p1
.end method

.method public isVisible(Landroid/view/View;I)Z
    .locals 8

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 98
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    .line 99
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 102
    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 103
    invoke-virtual {p1, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_1

    return v0

    .line 106
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-long v2, v2

    .line 107
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-long v4, v1

    mul-long v2, v2, v4

    .line 108
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    int-to-long v6, p1

    mul-long v4, v4, v6

    const-wide/16 v6, 0x0

    cmp-long p1, v4, v6

    if-gtz p1, :cond_2

    return v0

    :cond_2
    const-wide/16 v6, 0x64

    mul-long v2, v2, v6

    int-to-long p1, p2

    mul-long p1, p1, v4

    cmp-long v1, v2, p1

    if-ltz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0

    :cond_4
    :goto_0
    return v0
.end method
