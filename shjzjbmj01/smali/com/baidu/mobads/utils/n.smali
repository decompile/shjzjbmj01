.class Lcom/baidu/mobads/utils/n;
.super Lcom/baidu/mobads/f/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic c:Lcom/baidu/mobads/utils/m;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/utils/m;Ljava/lang/String;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/baidu/mobads/utils/n;->c:Lcom/baidu/mobads/utils/m;

    iput-object p2, p0, Lcom/baidu/mobads/utils/n;->a:Ljava/lang/String;

    invoke-direct {p0}, Lcom/baidu/mobads/f/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 11

    const/4 v0, 0x0

    .line 64
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/baidu/mobads/utils/n;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 65
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 66
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    return-object v0

    .line 69
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 70
    array-length v2, v1

    if-lez v2, :cond_4

    .line 71
    new-instance v2, Lcom/baidu/mobads/utils/o;

    invoke-direct {v2, p0}, Lcom/baidu/mobads/utils/o;-><init>(Lcom/baidu/mobads/utils/n;)V

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    const-wide/16 v2, 0x0

    .line 77
    array-length v4, v1

    add-int/lit8 v4, v4, -0x1

    :goto_0
    if-ltz v4, :cond_4

    .line 78
    aget-object v5, v1, v4

    .line 79
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    const/4 v10, 0x0

    sub-long/2addr v6, v8

    const-wide v8, 0x9a7ec800L

    cmp-long v10, v6, v8

    if-lez v10, :cond_1

    .line 81
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 82
    :cond_1
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    const/4 v8, 0x0

    add-long/2addr v6, v2

    invoke-static {}, Lcom/baidu/mobads/utils/m;->a()J

    move-result-wide v8

    cmp-long v10, v6, v8

    if-lez v10, :cond_2

    .line 83
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v7, 0x0

    add-long/2addr v2, v5

    :cond_3
    :goto_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :catch_0
    :cond_4
    return-object v0
.end method
