.class public Lcom/baidu/mobads/utils/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/utils/IXAdConstants;


# static fields
.field private static a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x18

    .line 259
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.android.chrome"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "com.UCMobile"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "com.uc.browser"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "com.uc.browser.hd"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "com.tencent.mtt"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "com.tencent.padbrowser"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "com.baidu.browser.apps"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "com.android.browser"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "com.oupeng.mini.android"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "com.oupeng.mobile"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string v1, "com.oupeng.browser"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string v1, "com.opera.mini.android"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string v1, "com.opera.browser"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string v1, "com.opera.browser.beta"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string v1, "com.mediawoz.xbrowser"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string v1, "com.mx.browser"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string v1, "com.mx.browser.tablet"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string v1, "org.mozilla.firefox"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string v1, "com.tiantianmini.android.browser"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    const-string v1, "com.ijinshan.browser_fast"

    const/16 v2, 0x13

    aput-object v1, v0, v2

    const-string v1, "sogou.mobile.explorer"

    const/16 v2, 0x14

    aput-object v1, v0, v2

    const-string v1, "com.dolphin.browser.cn"

    const/16 v2, 0x15

    aput-object v1, v0, v2

    const-string v1, "com.qihoo.browser"

    const/16 v2, 0x16

    aput-object v1, v0, v2

    const-string v1, "com.baidu.searchbox"

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sput-object v0, Lcom/baidu/mobads/utils/l;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "APO"

    return-object v0
.end method

.method public deviceNetworkTypeCdma()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeCdma"

    return-object v0
.end method

.method public deviceNetworkTypeEdge()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeEdge"

    return-object v0
.end method

.method public deviceNetworkTypeEhrpd()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeEhrpd"

    return-object v0
.end method

.method public deviceNetworkTypeEvdo0()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeEvdo0"

    return-object v0
.end method

.method public deviceNetworkTypeEvdoA()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeEvdoA"

    return-object v0
.end method

.method public deviceNetworkTypeEvdoB()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeEvdoB"

    return-object v0
.end method

.method public deviceNetworkTypeGprs()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeGprs"

    return-object v0
.end method

.method public deviceNetworkTypeHsdpa()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeHsdpa"

    return-object v0
.end method

.method public deviceNetworkTypeHspa()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeHspa"

    return-object v0
.end method

.method public deviceNetworkTypeHspaPlus()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeHspaPlus"

    return-object v0
.end method

.method public deviceNetworkTypeHsupa()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeHsupa"

    return-object v0
.end method

.method public deviceNetworkTypeIden()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeIden"

    return-object v0
.end method

.method public deviceNetworkTypeLte()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeLte"

    return-object v0
.end method

.method public deviceNetworkTypeLxRtt()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeLxRtt"

    return-object v0
.end method

.method public deviceNetworkTypeUmts()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeUmts"

    return-object v0
.end method

.method public deviceNetworkTypeUnknown()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeUnknown"

    return-object v0
.end method

.method public deviceNetworkTypeWifi()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceNetworkTypeWifi"

    return-object v0
.end method

.method public errorIo()Ljava/lang/String;
    .locals 1

    const-string v0, "errorIo"

    return-object v0
.end method

.method public errorNullAsset()Ljava/lang/String;
    .locals 1

    const-string v0, "errorNullAsset"

    return-object v0
.end method

.method public errorTimeout()Ljava/lang/String;
    .locals 1

    const-string v0, "errorTimeout"

    return-object v0
.end method

.method public errorUnknown()Ljava/lang/String;
    .locals 1

    const-string v0, "errorUnknown"

    return-object v0
.end method

.method public feedsTrackerParameterKeyList()Ljava/lang/String;
    .locals 1

    const-string v0, "trackerUrl"

    return-object v0
.end method

.method public feedsTrackerParameterKeyProgress()Ljava/lang/String;
    .locals 1

    const-string v0, "progress"

    return-object v0
.end method

.method public getActTypeDownload()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getActTypeLandingPage()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getActTypeMakeCall()I
    .locals 1

    const/16 v0, 0x20

    return v0
.end method

.method public getActTypeNothing2Do()I
    .locals 1

    const/16 v0, 0x100

    return v0
.end method

.method public getActTypeOpenExternalApp()I
    .locals 1

    const/16 v0, 0x200

    return v0
.end method

.method public getActTypeOpenMap()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public getActTypePlayVideo()I
    .locals 1

    const/16 v0, 0x40

    return v0
.end method

.method public getActTypeRichMedia()I
    .locals 1

    const/16 v0, 0x80

    return v0
.end method

.method public getActTypeSendMail()I
    .locals 1

    const/16 v0, 0x10

    return v0
.end method

.method public getActTypeSendSMS()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public getAdCreativeTypeImage()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getAdCreativeTypeRichmedia()I
    .locals 1

    const/16 v0, 0x10

    return v0
.end method

.method public getAdCreativeTypeText()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAdCreativeTypeVideo()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public getAppPackageNameOfPublisher()Ljava/lang/String;
    .locals 1

    .line 149
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppSec()Ljava/lang/String;
    .locals 2

    .line 135
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 136
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    .line 137
    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getAppSec(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppSid()Ljava/lang/String;
    .locals 2

    .line 142
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 143
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    .line 144
    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCanSendCalender()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public getCanSendEmail()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCanSendSMS()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getCanShowDownload()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public getCanShowMap()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getInfoKeyErrorCode()Ljava/lang/String;
    .locals 1

    const-string v0, "INFO_KEY_ERROR_CODE"

    return-object v0
.end method

.method public getInfoKeyErrorMessage()Ljava/lang/String;
    .locals 1

    const-string v0, "INFO_KEY_ERROR_MESSAGE"

    return-object v0
.end method

.method public getInfoKeyErrorModule()Ljava/lang/String;
    .locals 1

    const-string v0, "INFO_KEY_ERROR_MODULE"

    return-object v0
.end method

.method public getProductionTypeBanner()Ljava/lang/String;
    .locals 1

    .line 185
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_BANNER:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductionTypeFeeds()Ljava/lang/String;
    .locals 1

    .line 255
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductionTypeFullScreenVideo()Ljava/lang/String;
    .locals 1

    .line 206
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FULLSCREEN_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductionTypeIcon()Ljava/lang/String;
    .locals 1

    const-string v0, "icon"

    return-object v0
.end method

.method public getProductionTypeInterstitial()Ljava/lang/String;
    .locals 1

    .line 241
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductionTypeNRWall()Ljava/lang/String;
    .locals 1

    const-string v0, "nrwall"

    return-object v0
.end method

.method public getProductionTypeRWall()Ljava/lang/String;
    .locals 1

    const-string v0, "rwall"

    return-object v0
.end method

.method public getProductionTypeRewardVideo()Ljava/lang/String;
    .locals 1

    .line 199
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_REWARD_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductionTypeSplash()Ljava/lang/String;
    .locals 1

    .line 192
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductionTypeVideo()Ljava/lang/String;
    .locals 1

    const-string v0, "video"

    return-object v0
.end method

.method public getProductionTypeWall()Ljava/lang/String;
    .locals 1

    const-string v0, "wall"

    return-object v0
.end method

.method public getRemoteVersion()Ljava/lang/String;
    .locals 4

    .line 123
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdContainerFactory()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->getRemoteVersion()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, "0.0.0"

    :goto_0
    return-object v0
.end method

.method public getSN()Ljava/lang/String;
    .locals 2

    .line 129
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    .line 130
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getEncodedSN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedActionType4RequestingDownload()Ljava/lang/String;
    .locals 1

    const-string v0, "DL"

    return-object v0
.end method

.method public getSupportedActionType4RequestingLandingPage()Ljava/lang/String;
    .locals 1

    const-string v0, "LP"

    return-object v0
.end method

.method public getSupportedActionType4RequestingMail()Ljava/lang/String;
    .locals 1

    const-string v0, "MAIL"

    return-object v0
.end method

.method public getSupportedActionType4RequestingMakeCall()Ljava/lang/String;
    .locals 1

    const-string v0, "PHONE"

    return-object v0
.end method

.method public getSupportedActionType4RequestingMap()Ljava/lang/String;
    .locals 1

    const-string v0, "MAP"

    return-object v0
.end method

.method public getSupportedActionType4RequestingNone()Ljava/lang/String;
    .locals 1

    const-string v0, "NA"

    return-object v0
.end method

.method public getSupportedActionType4RequestingRichMedia()Ljava/lang/String;
    .locals 1

    const-string v0, "RM"

    return-object v0
.end method

.method public getSupportedActionType4RequestingSMS()Ljava/lang/String;
    .locals 1

    const-string v0, "SMS"

    return-object v0
.end method

.method public getSupportedActionType4RequestingVideo()Ljava/lang/String;
    .locals 1

    const-string v0, "VIDEO"

    return-object v0
.end method

.method public getSupportedBrowsers()[Ljava/lang/String;
    .locals 1

    .line 290
    sget-object v0, Lcom/baidu/mobads/utils/l;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public mraidNetworkTypeCell()Ljava/lang/String;
    .locals 1

    const-string v0, "mraidNetworkTypeCell"

    return-object v0
.end method

.method public mraidNetworkTypeOffline()Ljava/lang/String;
    .locals 1

    const-string v0, "mraidNetworkTypeOffline"

    return-object v0
.end method

.method public mraidNetworkTypeUnknown()Ljava/lang/String;
    .locals 1

    const-string v0, "mraidNetworkTypeUnknown"

    return-object v0
.end method

.method public mraidNetworkTypeWifi()Ljava/lang/String;
    .locals 1

    const-string v0, "mraidNetworkTypeWifi"

    return-object v0
.end method

.method public resourceRequestStateFailed()Ljava/lang/String;
    .locals 1

    const-string v0, "resourceRequestStateFailed"

    return-object v0
.end method

.method public resourceRequestStateIdel()Ljava/lang/String;
    .locals 1

    const-string v0, "resourceRequestStateIdel"

    return-object v0
.end method

.method public resourceRequestStateRequesting()Ljava/lang/String;
    .locals 1

    const-string v0, "resourceRequestStateRequesting"

    return-object v0
.end method

.method public resourceRequestStateSuccess()Ljava/lang/String;
    .locals 1

    const-string v0, "resourceRequestStateSuccess"

    return-object v0
.end method

.method public setSupportedBrowsers([Ljava/lang/String;)V
    .locals 0

    .line 295
    sput-object p1, Lcom/baidu/mobads/utils/l;->a:[Ljava/lang/String;

    return-void
.end method

.method public videoStateError()Ljava/lang/String;
    .locals 1

    const-string v0, "videoStateError"

    return-object v0
.end method

.method public videoStateIdle()Ljava/lang/String;
    .locals 1

    const-string v0, "videoStateIdle"

    return-object v0
.end method

.method public videoStatePause()Ljava/lang/String;
    .locals 1

    const-string v0, "videoStatePause"

    return-object v0
.end method

.method public videoStatePerparing()Ljava/lang/String;
    .locals 1

    const-string v0, "videoStatePerparing"

    return-object v0
.end method

.method public videoStatePlaybackCompleted()Ljava/lang/String;
    .locals 1

    const-string v0, "videoStatePlaybackCompleted"

    return-object v0
.end method

.method public videoStatePlaying()Ljava/lang/String;
    .locals 1

    const-string v0, "videoStatePlaying"

    return-object v0
.end method

.method public videoStatePrepared()Ljava/lang/String;
    .locals 1

    const-string v0, "videoStatePrepared"

    return-object v0
.end method
