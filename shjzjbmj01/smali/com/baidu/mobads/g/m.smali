.class Lcom/baidu/mobads/g/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# instance fields
.field final synthetic a:D

.field final synthetic b:Lcom/baidu/mobads/g/g;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/g/g;D)V
    .locals 0

    .line 482
    iput-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    iput-wide p2, p0, Lcom/baidu/mobads/g/m;->a:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 8

    .line 485
    iget-object v0, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {v0}, Lcom/baidu/mobads/g/g;->f(Lcom/baidu/mobads/g/g;)V

    const-string v0, "URLLoader.Load.Complete"

    .line 486
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 487
    iget-object v0, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    new-instance v2, Lcom/baidu/mobads/g/e;

    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    const-string v3, "message"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {v2, p1}, Lcom/baidu/mobads/g/e;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/e;)Lcom/baidu/mobads/g/e;

    .line 488
    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getVersion()D

    move-result-wide v2

    .line 489
    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    .line 490
    invoke-static {p1}, Lcom/baidu/mobads/g/g;->g(Lcom/baidu/mobads/g/g;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "__badApkVersion__8.8427"

    const/4 v4, 0x0

    .line 491
    invoke-interface {p1, v0, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result p1

    .line 492
    iget-object v0, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {v0}, Lcom/baidu/mobads/g/g;->h(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/g/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v4

    double-to-float v0, v4

    cmpl-float v0, v0, p1

    const/4 v4, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 504
    iget-object v5, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {v5}, Lcom/baidu/mobads/g/g;->h(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/g/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v5

    cmpg-double v7, v2, v5

    if-gtz v7, :cond_1

    .line 505
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    iget-object v5, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {v5}, Lcom/baidu/mobads/g/g;->h(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/g/e;

    move-result-object v5

    .line 506
    invoke-virtual {v5}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v5

    .line 505
    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    cmpl-double v7, v2, v5

    if-nez v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 504
    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 508
    iget-object v3, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {v3}, Lcom/baidu/mobads/g/g;->e(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v3

    const-string v4, "XAdApkLoader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "try to download apk badVer="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p1, ", isBad="

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", compatible="

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v3, v4, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    iget-wide v3, p0, Lcom/baidu/mobads/g/m;->a:D

    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {p1}, Lcom/baidu/mobads/g/g;->h(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/g/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v5

    cmpg-double p1, v3, v5

    if-gez p1, :cond_2

    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    .line 515
    invoke-static {p1}, Lcom/baidu/mobads/g/g;->h(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/g/e;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    .line 516
    invoke-static {p1}, Lcom/baidu/mobads/g/g;->h(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/g/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/g/e;->a()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 517
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_2

    .line 518
    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    iget-object v0, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {v0}, Lcom/baidu/mobads/g/g;->h(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/g/e;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/baidu/mobads/g/g;->b(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/e;)V

    goto :goto_2

    .line 520
    :cond_2
    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {p1}, Lcom/baidu/mobads/g/g;->c(Lcom/baidu/mobads/g/g;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 521
    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {p1, v1}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;Z)Z

    .line 522
    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    const-string v0, "Refused to download remote for version..."

    invoke-static {p1, v1, v0}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;ZLjava/lang/String;)V

    goto :goto_2

    .line 526
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {p1}, Lcom/baidu/mobads/g/g;->c(Lcom/baidu/mobads/g/g;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 527
    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    invoke-static {p1, v1}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;Z)Z

    .line 528
    iget-object p1, p0, Lcom/baidu/mobads/g/m;->b:Lcom/baidu/mobads/g/g;

    const-string v0, "remote update Network access failed"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;ZLjava/lang/String;)V

    :cond_4
    :goto_2
    return-void
.end method
