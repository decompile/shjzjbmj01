.class public Lcom/baidu/mobads/g/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/g/g$b;,
        Lcom/baidu/mobads/g/g$a;,
        Lcom/baidu/mobads/g/g$c;
    }
.end annotation


# static fields
.field protected static a:Ljava/lang/Thread$UncaughtExceptionHandler;

.field protected static volatile b:Lcom/baidu/mobads/g/a;

.field protected static volatile c:Lcom/baidu/mobads/g/a;

.field protected static volatile d:Ljava/lang/Class;

.field protected static e:Ljava/lang/String;

.field protected static final f:Landroid/os/Handler;

.field private static i:Ljava/lang/String;


# instance fields
.field protected g:Landroid/os/Handler;

.field protected final h:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private j:Lcom/baidu/mobads/openad/b/a;

.field private k:Lcom/baidu/mobads/g/e;

.field private final l:Landroid/content/Context;

.field private m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private n:Z

.field private o:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/baidu/mobads/g/g$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 116
    new-instance v0, Lcom/baidu/mobads/g/h;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/baidu/mobads/g/h;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/baidu/mobads/g/g;->f:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 v0, 0x0

    .line 114
    iput-boolean v0, p0, Lcom/baidu/mobads/g/g;->n:Z

    .line 122
    sget-object v0, Lcom/baidu/mobads/g/g;->f:Landroid/os/Handler;

    iput-object v0, p0, Lcom/baidu/mobads/g/g;->g:Landroid/os/Handler;

    .line 123
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/g/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 141
    new-instance v0, Lcom/baidu/mobads/g/i;

    .line 142
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobads/g/i;-><init>(Lcom/baidu/mobads/g/g;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/baidu/mobads/g/g;->h:Landroid/os/Handler;

    .line 211
    sget-object v0, Lcom/baidu/mobads/g/g;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 212
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://mobads.baidu.com/ads/pa/"

    invoke-interface {v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->replaceURLWithSupportProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getMajorVersionNumber()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "/__pasys_remote_banner.php"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/g/g;->i:Ljava/lang/String;

    .line 217
    :cond_0
    iput-object p1, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    .line 218
    invoke-static {p1}, Lcom/baidu/mobads/g/g;->c(Landroid/content/Context;)V

    .line 220
    sget-object v0, Lcom/baidu/mobads/g/g;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-nez v0, :cond_1

    .line 221
    invoke-static {p1}, Lcom/baidu/mobads/g/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/g/g;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 222
    invoke-static {p1}, Lcom/baidu/mobads/g/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;

    move-result-object p1

    new-instance v0, Lcom/baidu/mobads/g/j;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/g/j;-><init>(Lcom/baidu/mobads/g/g;)V

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/g/q;->a(Lcom/baidu/mobads/g/q$a;)V

    .line 238
    :cond_1
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object p1

    instance-of p1, p1, Lcom/baidu/mobads/g/q;

    if-nez p1, :cond_2

    .line 239
    sget-object p1, Lcom/baidu/mobads/g/g;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {p1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :cond_2
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)D
    .locals 4

    const-wide/16 v0, 0x0

    .line 895
    :try_start_0
    new-instance p0, Ljava/io/File;

    invoke-direct {p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 896
    invoke-static {p0}, Lcom/baidu/mobads/utils/p;->a(Ljava/io/File;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 897
    new-instance p1, Ljava/util/jar/JarFile;

    invoke-direct {p1, p0}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V

    .line 898
    invoke-virtual {p1}, Ljava/util/jar/JarFile;->getManifest()Ljava/util/jar/Manifest;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/jar/Manifest;->getMainAttributes()Ljava/util/jar/Attributes;

    move-result-object p0

    const-string v2, "Implementation-Version"

    .line 899
    invoke-virtual {p0, v2}, Ljava/util/jar/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 898
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 900
    invoke-virtual {p1}, Ljava/util/jar/JarFile;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double p0, v2, v0

    if-lez p0, :cond_0

    return-wide v2

    :catch_0
    :cond_0
    return-wide v0
.end method

.method static synthetic a(Lcom/baidu/mobads/g/g;)Landroid/content/Context;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/e;)Lcom/baidu/mobads/g/e;
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/baidu/mobads/g/g;->k:Lcom/baidu/mobads/g/e;

    return-object p1
.end method

.method private a(Lcom/baidu/mobads/g/a;)Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 812
    :try_start_0
    invoke-virtual {p1}, Lcom/baidu/mobads/g/a;->a()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, p1

    :catch_0
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 604
    sget-object v0, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "baidu_ad_sdk"

    const/4 v1, 0x0

    .line 605
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object p0

    .line 606
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    .line 608
    :cond_0
    sget-object p0, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    const-string p0, ""

    return-object p0

    .line 611
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "__xadsdk__remote__final__running__.jar"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/baidu/mobads/g/b;)V
    .locals 6

    .line 368
    invoke-virtual {p1}, Lcom/baidu/mobads/g/b;->b()Ljava/lang/Class;

    move-result-object v1

    .line 369
    monitor-enter p0

    .line 370
    :try_start_0
    new-instance p1, Lcom/baidu/mobads/g/a;

    iget-object v2, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    .line 371
    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getVersion()D

    move-result-wide v3

    sget-object v5, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->DEBUG:Ljava/lang/Boolean;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/g/a;-><init>(Ljava/lang/Class;Landroid/content/Context;DLjava/lang/Boolean;)V

    sput-object p1, Lcom/baidu/mobads/g/g;->c:Lcom/baidu/mobads/g/a;

    .line 373
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private a(Lcom/baidu/mobads/g/e;)V
    .locals 4

    .line 690
    invoke-virtual {p1}, Lcom/baidu/mobads/g/e;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 691
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    sget-object v1, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/g/g;->h:Landroid/os/Handler;

    .line 692
    invoke-static {v0, p1, v1, v2}, Lcom/baidu/mobads/g/c;->a(Landroid/content/Context;Lcom/baidu/mobads/g/e;Ljava/lang/String;Landroid/os/Handler;)Lcom/baidu/mobads/g/c;

    move-result-object v0

    .line 694
    invoke-virtual {v0}, Lcom/baidu/mobads/g/c;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 695
    iget-object p1, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAdApkLoader"

    const-string v2, "XApkDownloadThread starting ..."

    invoke-interface {p1, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    invoke-virtual {v0}, Lcom/baidu/mobads/g/c;->start()V

    goto :goto_0

    .line 698
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAdApkLoader"

    const-string v3, "XApkDownloadThread already started"

    invoke-interface {v1, v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    invoke-virtual {p1}, Lcom/baidu/mobads/g/e;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/g/c;->a(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/b;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/b;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/g$c;Landroid/os/Handler;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/g/g;->b(Lcom/baidu/mobads/g/g$c;Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/g/g;ZLjava/lang/String;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/g/g;->a(ZLjava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .line 288
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->g:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 289
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "success"

    .line 290
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 291
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    .line 292
    iput p1, v0, Landroid/os/Message;->what:I

    .line 293
    iget-object p1, p0, Lcom/baidu/mobads/g/g;->g:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private declared-synchronized a(ZLjava/lang/String;)V
    .locals 2

    monitor-enter p0

    .line 585
    :try_start_0
    iget-object p2, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    invoke-static {p2}, Lcom/baidu/mobads/g/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/g/q;->b()V

    .line 590
    iget-object p2, p0, Lcom/baidu/mobads/g/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/baidu/mobads/g/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result p2

    if-lez p2, :cond_0

    .line 591
    iget-object p2, p0, Lcom/baidu/mobads/g/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/g/g$c;

    .line 592
    invoke-interface {v0, p1}, Lcom/baidu/mobads/g/g$c;->a(Z)V

    .line 593
    iget-object v1, p0, Lcom/baidu/mobads/g/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 596
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 584
    monitor-exit p0

    throw p1
.end method

.method static synthetic a(Lcom/baidu/mobads/g/g;Z)Z
    .locals 0

    .line 46
    iput-boolean p1, p0, Lcom/baidu/mobads/g/g;->n:Z

    return p1
.end method

.method public static b(Landroid/content/Context;)D
    .locals 8

    .line 856
    :try_start_0
    invoke-static {p0}, Lcom/baidu/mobads/g/g;->c(Landroid/content/Context;)V

    .line 857
    invoke-static {}, Lcom/baidu/mobads/g/g;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/baidu/mobads/g/g;->a(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v0

    .line 858
    invoke-static {}, Lcom/baidu/mobads/g/g;->d()Ljava/lang/String;

    move-result-object v2

    .line 859
    invoke-static {p0, v2}, Lcom/baidu/mobads/g/g;->a(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v3

    const-string v5, "8.8427"

    .line 861
    invoke-static {v5}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    cmpl-double v7, v5, v3

    if-lez v7, :cond_1

    .line 862
    new-instance v3, Lcom/baidu/mobads/g/b;

    invoke-direct {v3, v2, p0}, Lcom/baidu/mobads/g/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 863
    invoke-virtual {v3}, Lcom/baidu/mobads/g/b;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 864
    invoke-virtual {v3}, Lcom/baidu/mobads/g/b;->delete()Z

    .line 866
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v3

    const-string v4, "bdxadsdk.jar"

    .line 875
    invoke-interface {v3, p0, v4, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->copyFileFromAssetsTo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    :cond_1
    invoke-static {}, Lcom/baidu/mobads/g/g;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/baidu/mobads/g/g;->a(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v2

    .line 879
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private b(Lcom/baidu/mobads/g/b;)V
    .locals 9

    .line 385
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAdApkLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "len="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/g/b;->length()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, ", path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/g/b;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    sget-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    if-nez v0, :cond_1

    .line 387
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/g/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 388
    new-instance v1, Lcom/baidu/mobads/g/b;

    iget-object v2, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Lcom/baidu/mobads/g/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 389
    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 390
    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->delete()Z

    .line 394
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 395
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v3

    .line 396
    invoke-interface {v3, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->copyFileInputStream(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 399
    iget-object v2, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    .line 402
    :goto_0
    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->b()Ljava/lang/Class;

    move-result-object v4

    .line 404
    new-instance v0, Lcom/baidu/mobads/g/a;

    iget-object v5, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    .line 405
    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getVersion()D

    move-result-wide v6

    sget-object v8, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->DEBUG:Ljava/lang/Boolean;

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Lcom/baidu/mobads/g/a;-><init>(Ljava/lang/Class;Landroid/content/Context;DLjava/lang/Boolean;)V

    sput-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    .line 409
    :try_start_1
    sget-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/a;->a()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    .line 410
    iget-object v1, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAdApkLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "preloaded apk.version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->getRemoteVersion()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/baidu/mobads/g/g$a; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 412
    iget-object v1, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preload local apk "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/g/b;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " failed, msg:"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/g$a;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", v="

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    iget-wide v3, p1, Lcom/baidu/mobads/g/a;->a:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "XAdApkLoader"

    invoke-interface {v1, v2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    invoke-virtual {v0}, Lcom/baidu/mobads/g/g$a;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/g/g;->a(Ljava/lang/String;)V

    .line 416
    throw v0

    .line 419
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "XAdApkLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mApkBuilder already initialized, version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    iget-wide v2, v2, Lcom/baidu/mobads/g/a;->a:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void
.end method

.method private b(Lcom/baidu/mobads/g/g$c;Landroid/os/Handler;)V
    .locals 1

    .line 707
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/g/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 710
    :cond_0
    iput-object p2, p0, Lcom/baidu/mobads/g/g;->g:Landroid/os/Handler;

    .line 711
    sget-object p1, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    if-nez p1, :cond_1

    .line 713
    invoke-virtual {p0}, Lcom/baidu/mobads/g/g;->g()V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    .line 715
    invoke-direct {p0, p1}, Lcom/baidu/mobads/g/g;->b(Z)V

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/g/g;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->j()V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/e;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/e;)V

    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/g/g;Z)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/baidu/mobads/g/g;->a(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    if-nez p1, :cond_0

    .line 446
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->m()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 448
    iput-boolean v0, p0, Lcom/baidu/mobads/g/g;->n:Z

    goto :goto_1

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "apk Successfully Loaded"

    goto :goto_0

    :cond_1
    const-string v0, "apk Load Failed"

    .line 450
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/g/g;->a(ZLjava/lang/String;)V

    .line 453
    :goto_1
    iget-boolean v0, p0, Lcom/baidu/mobads/g/g;->n:Z

    if-eqz v0, :cond_2

    .line 454
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/g/k;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/g/k;-><init>(Lcom/baidu/mobads/g/g;Z)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    goto :goto_2

    .line 462
    :cond_2
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/g/l;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/g/l;-><init>(Lcom/baidu/mobads/g/g;Z)V

    const-wide/16 v2, 0x5

    sget-object p1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;JLjava/util/concurrent/TimeUnit;)V

    :goto_2
    return-void
.end method

.method protected static c()Ljava/lang/String;
    .locals 2

    .line 301
    sget-object v0, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 304
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__xadsdk__remote__final__builtin__.jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/content/Context;)V
    .locals 2

    .line 249
    sget-object v0, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "baidu_ad_sdk"

    const/4 v1, 0x0

    .line 250
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object p0

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/baidu/mobads/g/g;Z)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/baidu/mobads/g/g;->c(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 7

    .line 475
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 476
    sget-object p1, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    iget-wide v1, p1, Lcom/baidu/mobads/g/a;->a:D

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    .line 482
    :goto_0
    new-instance p1, Lcom/baidu/mobads/g/m;

    invoke-direct {p1, p0, v1, v2}, Lcom/baidu/mobads/g/m;-><init>(Lcom/baidu/mobads/g/g;D)V

    .line 533
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v4, "v"

    .line 534
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "os"

    const-string v2, "android"

    .line 535
    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "tp"

    .line 536
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    .line 537
    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v2

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/baidu/mobads/utils/h;->getTextEncoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 536
    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "bdr"

    .line 539
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v2

    sget-object v4, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    .line 540
    invoke-virtual {v2, v4}, Lcom/baidu/mobads/utils/h;->getTextEncoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 538
    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    sget-object v1, Lcom/baidu/mobads/g/g;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->addParameters(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    .line 543
    new-instance v1, Lcom/baidu/mobads/openad/b/b;

    const-string v2, ""

    invoke-direct {v1, v0, v2}, Lcom/baidu/mobads/openad/b/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 544
    iput v0, v1, Lcom/baidu/mobads/openad/b/b;->e:I

    .line 545
    new-instance v0, Lcom/baidu/mobads/openad/b/a;

    invoke-direct {v0}, Lcom/baidu/mobads/openad/b/a;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/g/g;->j:Lcom/baidu/mobads/openad/b/a;

    .line 546
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->j:Lcom/baidu/mobads/openad/b/a;

    const-string v2, "URLLoader.Load.Complete"

    .line 547
    invoke-virtual {v0, v2, p1}, Lcom/baidu/mobads/openad/b/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 548
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->j:Lcom/baidu/mobads/openad/b/a;

    const-string v2, "URLLoader.Load.Error"

    invoke-virtual {v0, v2, p1}, Lcom/baidu/mobads/openad/b/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 549
    iget-object p1, p0, Lcom/baidu/mobads/g/g;->j:Lcom/baidu/mobads/openad/b/a;

    invoke-virtual {p1, v1}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private c(Lcom/baidu/mobads/g/b;)Z
    .locals 4

    .line 424
    monitor-enter p0

    .line 425
    :try_start_0
    invoke-direct {p0, p1}, Lcom/baidu/mobads/g/g;->b(Lcom/baidu/mobads/g/b;)V

    .line 426
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAdApkLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loaded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/g/b;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x1

    .line 427
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    .line 428
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic c(Lcom/baidu/mobads/g/g;)Z
    .locals 0

    .line 46
    iget-boolean p0, p0, Lcom/baidu/mobads/g/g;->n:Z

    return p0
.end method

.method protected static d()Ljava/lang/String;
    .locals 2

    .line 309
    sget-object v0, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 312
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__xadsdk__remote__final__builtinversion__.jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized d(Landroid/content/Context;)V
    .locals 7

    const-class v0, Lcom/baidu/mobads/g/g;

    monitor-enter v0

    .line 341
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/g/g;->c()Ljava/lang/String;

    move-result-object v1

    .line 342
    invoke-static {p0, v1}, Lcom/baidu/mobads/g/g;->a(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v2

    const-string v4, "8.8427"

    .line 344
    invoke-static {v4}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v6, v4, v2

    if-lez v6, :cond_1

    .line 345
    new-instance v2, Lcom/baidu/mobads/g/b;

    invoke-direct {v2, v1, p0}, Lcom/baidu/mobads/g/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 346
    invoke-virtual {v2}, Lcom/baidu/mobads/g/b;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 347
    invoke-virtual {v2}, Lcom/baidu/mobads/g/b;->delete()Z

    .line 349
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v2

    const-string v3, "bdxadsdk.jar"

    .line 358
    invoke-interface {v2, p0, v3, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->copyFileFromAssetsTo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 361
    :try_start_1
    new-instance v1, Lcom/baidu/mobads/g/g$b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadBuiltInApk failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/baidu/mobads/g/g$b;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 340
    :goto_0
    monitor-exit v0

    throw p0
.end method

.method static synthetic d(Lcom/baidu/mobads/g/g;Z)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/baidu/mobads/g/g;->b(Z)V

    return-void
.end method

.method static synthetic d(Lcom/baidu/mobads/g/g;)Z
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->o()Z

    move-result p0

    return p0
.end method

.method static synthetic e(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    return-object p0
.end method

.method protected static f()Ljava/lang/String;
    .locals 2

    .line 621
    sget-object v0, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 624
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/baidu/mobads/g/g;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__xadsdk__remote__final__downloaded__.jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/baidu/mobads/g/g;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->n()V

    return-void
.end method

.method static synthetic g(Lcom/baidu/mobads/g/g;)Landroid/content/SharedPreferences;
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->k()Landroid/content/SharedPreferences;

    move-result-object p0

    return-object p0
.end method

.method static synthetic h(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/g/e;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/baidu/mobads/g/g;->k:Lcom/baidu/mobads/g/e;

    return-object p0
.end method

.method private j()V
    .locals 4

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 130
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 131
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "__xadsdk__remote__final__"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, v0, v1

    .line 132
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "dex"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 137
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    :cond_1
    return-void
.end method

.method private k()Landroid/content/SharedPreferences;
    .locals 3

    .line 256
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    const-string v1, "com.baidu.mobads.loader"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private l()Z
    .locals 3

    .line 282
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->k()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "previousProxyVersion"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    invoke-virtual {p0}, Lcom/baidu/mobads/g/g;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    .line 284
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private m()Z
    .locals 3

    const/4 v0, 0x0

    .line 433
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/g/g;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/baidu/mobads/utils/p;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 434
    invoke-static {}, Lcom/baidu/mobads/g/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/baidu/mobads/utils/p;->b(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    :catch_0
    move-exception v1

    .line 436
    iget-object v2, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {v2, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    return v0
.end method

.method private declared-synchronized n()V
    .locals 1

    monitor-enter p0

    .line 557
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->j:Lcom/baidu/mobads/openad/b/a;

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->j:Lcom/baidu/mobads/openad/b/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/openad/b/a;->removeAllListeners()V

    .line 559
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->j:Lcom/baidu/mobads/openad/b/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/openad/b/a;->a()V

    :cond_0
    const/4 v0, 0x0

    .line 561
    iput-object v0, p0, Lcom/baidu/mobads/g/g;->j:Lcom/baidu/mobads/openad/b/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 556
    monitor-exit p0

    throw v0

    .line 565
    :catch_0
    :goto_0
    monitor-exit p0

    return-void
.end method

.method private o()Z
    .locals 8

    .line 648
    invoke-static {}, Lcom/baidu/mobads/g/g;->f()Ljava/lang/String;

    move-result-object v0

    .line 649
    new-instance v1, Lcom/baidu/mobads/g/b;

    iget-object v2, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Lcom/baidu/mobads/g/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 650
    invoke-static {v1}, Lcom/baidu/mobads/utils/p;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 654
    :try_start_0
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 659
    monitor-enter p0
    :try_end_0
    .catch Lcom/baidu/mobads/g/g$a; {:try_start_0 .. :try_end_0} :catch_0

    .line 660
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAdApkLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadDownloadedOrBuiltInApk len="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, ", path="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 661
    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 660
    invoke-interface {v0, v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    invoke-direct {p0, v1}, Lcom/baidu/mobads/g/g;->b(Lcom/baidu/mobads/g/b;)V

    .line 664
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->k()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "__badApkVersion__8.8427"

    const/high16 v3, -0x40800000    # -1.0f

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    float-to-double v2, v0

    .line 665
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v4, "XAdApkLoader"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "downloadedApkFile.getApkVersion(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->c()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v6, ", badApkVersion: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->c()D

    move-result-wide v4

    cmpl-double v0, v4, v2

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAdApkLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loaded: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    .line 672
    monitor-exit p0

    return v0

    .line 669
    :cond_0
    new-instance v0, Lcom/baidu/mobads/g/g$a;

    const-string v2, "downloaded file marked bad, drop it and use built-in"

    invoke-direct {v0, v2}, Lcom/baidu/mobads/g/g$a;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    .line 675
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 656
    :cond_1
    new-instance v0, Lcom/baidu/mobads/g/g$a;

    const-string v2, "XAdApkLoader upgraded, drop stale downloaded file, use built-in instead"

    invoke-direct {v0, v2}, Lcom/baidu/mobads/g/g$a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/baidu/mobads/g/g$a; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    .line 677
    iget-object v2, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v3, "XAdApkLoader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "load downloaded apk failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/g$a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", fallback to built-in"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 679
    invoke-virtual {v1}, Lcom/baidu/mobads/g/b;->delete()Z

    .line 681
    :cond_2
    invoke-virtual {p0}, Lcom/baidu/mobads/g/g;->i()V

    :cond_3
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "8.8427"

    return-object v0
.end method

.method public a(Lcom/baidu/mobads/g/g$c;)V
    .locals 1

    .line 792
    sget-object v0, Lcom/baidu/mobads/g/g;->f:Landroid/os/Handler;

    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g$c;Landroid/os/Handler;)V

    return-void
.end method

.method public a(Lcom/baidu/mobads/g/g$c;Landroid/os/Handler;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .line 725
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/g/n;

    invoke-direct {v1, p0, p1, p2}, Lcom/baidu/mobads/g/n;-><init>(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/g$c;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .line 266
    sget-object p1, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    if-eqz p1, :cond_1

    .line 267
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->k()Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "__badApkVersion__8.8427"

    .line 268
    sget-object v1, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    iget-wide v1, v1, Lcom/baidu/mobads/g/a;->a:D

    double-to-float v1, v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 273
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 274
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 276
    :cond_0
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    :goto_0
    return-void
.end method

.method protected b()V
    .locals 2

    .line 260
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/baidu/mobads/g/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 261
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method protected e()V
    .locals 4

    .line 317
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/g/g;->d(Landroid/content/Context;)V

    .line 318
    invoke-static {}, Lcom/baidu/mobads/g/g;->c()Ljava/lang/String;

    move-result-object v0

    .line 319
    new-instance v1, Lcom/baidu/mobads/g/b;

    iget-object v2, p0, Lcom/baidu/mobads/g/g;->l:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Lcom/baidu/mobads/g/b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 320
    invoke-static {v1}, Lcom/baidu/mobads/utils/p;->a(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 321
    invoke-direct {p0, v1}, Lcom/baidu/mobads/g/g;->c(Lcom/baidu/mobads/g/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 322
    invoke-direct {p0, v0}, Lcom/baidu/mobads/g/g;->b(Z)V

    :cond_0
    return-void

    .line 325
    :cond_1
    new-instance v1, Lcom/baidu/mobads/g/g$b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadBuiltInApk failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/baidu/mobads/g/g$b;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected g()V
    .locals 4

    .line 633
    invoke-direct {p0}, Lcom/baidu/mobads/g/g;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 635
    invoke-direct {p0, v0}, Lcom/baidu/mobads/g/g;->b(Z)V

    goto :goto_0

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAdApkLoader"

    const-string v2, "no downloaded file yet, use built-in apk file"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/g/g;->e()V
    :try_end_0
    .catch Lcom/baidu/mobads/g/g$b; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 641
    iget-object v1, p0, Lcom/baidu/mobads/g/g;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadBuiltInApk failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/g$b;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "XAdApkLoader"

    invoke-interface {v1, v3, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    new-instance v1, Lcom/baidu/mobads/g/g$a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load built-in apk failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/g$b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/baidu/mobads/g/g$a;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public h()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 1

    .line 801
    sget-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    invoke-direct {p0, v0}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/a;)Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 1

    .line 821
    sget-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    if-eqz v0, :cond_0

    .line 822
    sget-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/a;->b()V

    const/4 v0, 0x0

    .line 823
    sput-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    :cond_0
    return-void
.end method
