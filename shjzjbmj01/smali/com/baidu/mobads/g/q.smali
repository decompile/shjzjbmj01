.class public Lcom/baidu/mobads/g/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/g/q$a;
    }
.end annotation


# static fields
.field public static a:Ljava/lang/String; = ""

.field private static b:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private static volatile c:Lcom/baidu/mobads/g/q;


# instance fields
.field private d:Landroid/content/Context;

.field private e:Lcom/baidu/mobads/g/q$a;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/g/q;->d:Landroid/content/Context;

    .line 44
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object p1

    sput-object p1, Lcom/baidu/mobads/g/q;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 45
    invoke-static {}, Lcom/baidu/mobads/f/c;->a()Lcom/baidu/mobads/f/c;

    move-result-object p1

    new-instance v0, Lcom/baidu/mobads/g/r;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/g/r;-><init>(Lcom/baidu/mobads/g/q;)V

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/f/c;->a(Lcom/baidu/mobads/f/a;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;
    .locals 2

    .line 32
    sget-object v0, Lcom/baidu/mobads/g/q;->c:Lcom/baidu/mobads/g/q;

    if-nez v0, :cond_1

    .line 33
    const-class v0, Lcom/baidu/mobads/g/q;

    monitor-enter v0

    .line 34
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/g/q;->c:Lcom/baidu/mobads/g/q;

    if-nez v1, :cond_0

    .line 35
    new-instance v1, Lcom/baidu/mobads/g/q;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/g/q;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/baidu/mobads/g/q;->c:Lcom/baidu/mobads/g/q;

    .line 37
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 39
    :cond_1
    :goto_0
    sget-object p0, Lcom/baidu/mobads/g/q;->c:Lcom/baidu/mobads/g/q;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/g/q;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/baidu/mobads/g/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 171
    invoke-direct {p0}, Lcom/baidu/mobads/g/q;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4

    .line 112
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, v0

    .line 116
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_6

    .line 117
    array-length v1, p1

    if-lez v1, :cond_6

    const/4 v1, 0x0

    .line 118
    :goto_1
    array-length v2, p1

    if-ge v1, v2, :cond_6

    .line 119
    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "junit.framework"

    .line 120
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_2

    :cond_1
    const-string v3, "com.baidu.mobads.container"

    .line 124
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v0, "remote"

    goto :goto_2

    :cond_2
    const-string v3, "com.baidu.mobads.loader"

    .line 127
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v0, "loader"

    goto :goto_2

    :cond_3
    const-string v3, "com.baidu.mobads"

    .line 130
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v0, "proxy"

    goto :goto_2

    :cond_4
    const-string v3, "com.baidu.mobstat.forbes"

    .line 133
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v0, "third-mtj"

    goto :goto_2

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    :goto_2
    return-object v0
.end method

.method static synthetic a(Lcom/baidu/mobads/g/q;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/baidu/mobads/g/q;->e()V

    return-void
.end method

.method private c()Landroid/content/SharedPreferences;
    .locals 3

    .line 145
    iget-object v0, p0, Lcom/baidu/mobads/g/q;->d:Landroid/content/Context;

    const-string v1, "baidu_mobads_crash"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private d()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .line 149
    invoke-direct {p0}, Lcom/baidu/mobads/g/q;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 3

    .line 175
    invoke-direct {p0}, Lcom/baidu/mobads/g/q;->d()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 176
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 177
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    .line 178
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 180
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 65
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    instance-of v0, v0, Lcom/baidu/mobads/g/q;

    if-nez v0, :cond_0

    .line 66
    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/baidu/mobads/g/q$a;)V
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/baidu/mobads/g/q;->e:Lcom/baidu/mobads/g/q$a;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 157
    invoke-direct {p0}, Lcom/baidu/mobads/g/q;->d()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 158
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "crashtime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "key_crash_source"

    .line 159
    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string p1, "key_crash_trace"

    .line 160
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string p1, "key_crash_ad"

    .line 162
    sget-object p2, Lcom/baidu/mobads/g/q;->a:Ljava/lang/String;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 163
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x9

    if-lt p1, p2, :cond_0

    .line 164
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 166
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_0
    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    .line 102
    iput-object v0, p0, Lcom/baidu/mobads/g/q;->e:Lcom/baidu/mobads/g/q$a;

    return-void
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .line 73
    :try_start_0
    invoke-direct {p0, p2}, Lcom/baidu/mobads/g/q;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    .line 76
    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/g/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/baidu/mobads/g/q;->e:Lcom/baidu/mobads/g/q$a;

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/baidu/mobads/g/q;->e:Lcom/baidu/mobads/g/q$a;

    invoke-interface {v1, v0}, Lcom/baidu/mobads/g/q$a;->a(Ljava/lang/String;)V

    .line 83
    :cond_0
    sget-object v0, Lcom/baidu/mobads/g/q;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_1

    .line 84
    sget-object v0, Lcom/baidu/mobads/g/q;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 87
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void
.end method
