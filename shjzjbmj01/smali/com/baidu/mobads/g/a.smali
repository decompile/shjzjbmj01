.class public Lcom/baidu/mobads/g/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;


# instance fields
.field public a:D

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private d:D

.field private f:Ljava/lang/Boolean;

.field private g:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Landroid/content/Context;DLjava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Landroid/content/Context;",
            "D",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 26
    iput-object v0, p0, Lcom/baidu/mobads/g/a;->c:Ljava/lang/Class;

    const-wide v0, 0x3fb999999999999aL    # 0.1

    .line 30
    iput-wide v0, p0, Lcom/baidu/mobads/g/a;->a:D

    .line 36
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/g/a;->g:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/g/a;->c:Ljava/lang/Class;

    .line 46
    iput-object p2, p0, Lcom/baidu/mobads/g/a;->b:Landroid/content/Context;

    .line 47
    iput-wide p3, p0, Lcom/baidu/mobads/g/a;->d:D

    .line 48
    iput-object p5, p0, Lcom/baidu/mobads/g/a;->f:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public a()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 6

    .line 53
    sget-object v0, Lcom/baidu/mobads/g/a;->e:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 56
    :try_start_0
    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v0

    .line 57
    iget-object v3, p0, Lcom/baidu/mobads/g/a;->c:Ljava/lang/Class;

    invoke-virtual {v3, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 58
    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/baidu/mobads/g/a;->b:Landroid/content/Context;

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    sput-object v2, Lcom/baidu/mobads/g/a;->e:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    .line 61
    sget-object v2, Lcom/baidu/mobads/g/a;->e:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    invoke-interface {v2}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->getRemoteVersion()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/baidu/mobads/g/a;->a:D

    .line 63
    sget-object v2, Lcom/baidu/mobads/g/a;->e:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    iget-object v3, p0, Lcom/baidu/mobads/g/a;->f:Ljava/lang/Boolean;

    invoke-interface {v2, v3}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->setDebugMode(Ljava/lang/Boolean;)V

    .line 65
    sget-object v2, Lcom/baidu/mobads/g/a;->e:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    iget-wide v3, p0, Lcom/baidu/mobads/g/a;->d:D

    const-string v5, "8.8427"

    invoke-interface {v2, v3, v4, v5}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->handleShakeVersion(DLjava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 67
    iget-object v3, p0, Lcom/baidu/mobads/g/a;->g:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "XAdContainerFactoryBuilder"

    aput-object v5, v4, v0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-interface {v3, v4}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w([Ljava/lang/Object;)I

    .line 69
    new-instance v0, Lcom/baidu/mobads/g/g$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "newXAdContainerFactory() failed, possibly API incompatible: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/baidu/mobads/g/g$a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    :goto_0
    sget-object v0, Lcom/baidu/mobads/g/a;->e:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    return-object v0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    .line 77
    sput-object v0, Lcom/baidu/mobads/g/a;->e:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    return-void
.end method
