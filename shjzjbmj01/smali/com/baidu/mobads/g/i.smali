.class Lcom/baidu/mobads/g/i;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/baidu/mobads/g/g;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/g/g;Landroid/os/Looper;)V
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .line 145
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "APK_INFO"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/g/e;

    const-string v1, "OK"

    .line 148
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 149
    new-instance v0, Lcom/baidu/mobads/g/b;

    invoke-virtual {p1}, Lcom/baidu/mobads/g/e;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {v3}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v1, v3, p1}, Lcom/baidu/mobads/g/b;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/baidu/mobads/g/e;)V

    .line 158
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    iget-object v1, v1, Lcom/baidu/mobads/g/g;->g:Landroid/os/Handler;

    sget-object v3, Lcom/baidu/mobads/g/g;->f:Landroid/os/Handler;

    if-ne v1, v3, :cond_1

    .line 160
    invoke-virtual {v0}, Lcom/baidu/mobads/g/b;->a()V

    .line 162
    invoke-static {}, Lcom/baidu/mobads/g/g;->f()Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/g/b;->a(Ljava/lang/String;)V

    .line 164
    sget-object v1, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    if-eqz v1, :cond_0

    .line 165
    sget-object v1, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    invoke-virtual {p1}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v3

    iput-wide v3, v1, Lcom/baidu/mobads/g/a;->a:D

    .line 168
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {p1}, Lcom/baidu/mobads/g/g;->b(Lcom/baidu/mobads/g/g;)V

    .line 169
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {p1}, Lcom/baidu/mobads/g/g;->c(Lcom/baidu/mobads/g/g;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 171
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {p1, v2}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;Z)Z

    .line 172
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    iget-object v1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {v1}, Lcom/baidu/mobads/g/g;->d(Lcom/baidu/mobads/g/g;)Z

    move-result v1

    const-string v3, "load remote file just downloaded"

    invoke-static {p1, v1, v3}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;ZLjava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {p1, v0}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/b;)V

    .line 177
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {}, Lcom/baidu/mobads/g/g;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/g/b;->a(Ljava/lang/String;)V

    .line 178
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/baidu/mobads/g/g;->b(Lcom/baidu/mobads/g/g;Z)V
    :try_end_0
    .catch Lcom/baidu/mobads/g/g$a; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :cond_2
    :goto_0
    invoke-virtual {v0}, Lcom/baidu/mobads/g/b;->delete()Z

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 181
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download apk file failed: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/baidu/mobads/g/g$a;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 182
    iget-object v1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {v1, v2}, Lcom/baidu/mobads/g/g;->b(Lcom/baidu/mobads/g/g;Z)V

    .line 183
    iget-object v1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {v1}, Lcom/baidu/mobads/g/g;->e(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    const-string v2, "XAdApkLoader"

    invoke-interface {v1, v2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 185
    :goto_1
    invoke-virtual {v0}, Lcom/baidu/mobads/g/b;->delete()Z

    throw p1

    .line 188
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {p1}, Lcom/baidu/mobads/g/g;->e(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p1

    const-string v1, "XAdApkLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mOnApkDownloadCompleted: download failed, code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {p1, v2}, Lcom/baidu/mobads/g/g;->b(Lcom/baidu/mobads/g/g;Z)V

    .line 190
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {p1}, Lcom/baidu/mobads/g/g;->c(Lcom/baidu/mobads/g/g;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 191
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    invoke-static {p1, v2}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;Z)Z

    .line 192
    iget-object p1, p0, Lcom/baidu/mobads/g/i;->a:Lcom/baidu/mobads/g/g;

    const-string v0, "Refused to download remote for version..."

    invoke-static {p1, v2, v0}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;ZLjava/lang/String;)V

    :cond_4
    :goto_2
    return-void
.end method
