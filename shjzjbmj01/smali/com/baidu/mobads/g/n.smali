.class Lcom/baidu/mobads/g/n;
.super Lcom/baidu/mobads/f/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/baidu/mobads/g/g$c;

.field final synthetic c:Landroid/os/Handler;

.field final synthetic d:Lcom/baidu/mobads/g/g;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/g$c;Landroid/os/Handler;)V
    .locals 0

    .line 725
    iput-object p1, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    iput-object p2, p0, Lcom/baidu/mobads/g/n;->a:Lcom/baidu/mobads/g/g$c;

    iput-object p3, p0, Lcom/baidu/mobads/g/n;->c:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/baidu/mobads/f/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 5

    const/16 v0, 0x9

    .line 737
    :try_start_0
    const-class v1, Lcom/baidu/mobads/g/g;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 738
    :try_start_1
    iget-object v2, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    iget-object v3, p0, Lcom/baidu/mobads/g/n;->a:Lcom/baidu/mobads/g/g$c;

    iget-object v4, p0, Lcom/baidu/mobads/g/n;->c:Landroid/os/Handler;

    invoke-static {v2, v3, v4}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g;Lcom/baidu/mobads/g/g$c;Landroid/os/Handler;)V

    .line 739
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 761
    :try_start_2
    iget-object v1, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    invoke-static {v1}, Lcom/baidu/mobads/g/g;->g(Lcom/baidu/mobads/g/g;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "previousProxyVersion"

    .line 762
    iget-object v3, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    invoke-virtual {v3}, Lcom/baidu/mobads/g/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 767
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v0, :cond_0

    .line 768
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 770
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 773
    iget-object v1, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    invoke-static {v1}, Lcom/baidu/mobads/g/g;->e(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    const-string v2, "XAdApkLoader"

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v2

    .line 739
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v1

    goto :goto_1

    :catch_1
    move-exception v1

    .line 741
    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Load APK Failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 742
    iget-object v2, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    invoke-static {v2}, Lcom/baidu/mobads/g/g;->e(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const-string v3, "XAdApkLoader"

    invoke-interface {v2, v3, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    iget-object v1, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/baidu/mobads/g/g;->d(Lcom/baidu/mobads/g/g;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 761
    :try_start_6
    iget-object v1, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    invoke-static {v1}, Lcom/baidu/mobads/g/g;->g(Lcom/baidu/mobads/g/g;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "previousProxyVersion"

    .line 762
    iget-object v3, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    invoke-virtual {v3}, Lcom/baidu/mobads/g/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 767
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v0, :cond_1

    .line 768
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 770
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0

    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 761
    :goto_1
    :try_start_7
    iget-object v2, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    invoke-static {v2}, Lcom/baidu/mobads/g/g;->g(Lcom/baidu/mobads/g/g;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "previousProxyVersion"

    .line 762
    iget-object v4, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    invoke-virtual {v4}, Lcom/baidu/mobads/g/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 767
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v0, :cond_2

    .line 768
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_2

    .line 770
    :cond_2
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    .line 773
    iget-object v2, p0, Lcom/baidu/mobads/g/n;->d:Lcom/baidu/mobads/g/g;

    invoke-static {v2}, Lcom/baidu/mobads/g/g;->e(Lcom/baidu/mobads/g/g;)Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v2

    const-string v3, "XAdApkLoader"

    invoke-interface {v2, v3, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 774
    :goto_2
    throw v1
.end method
