.class public Lcom/baidu/mobads/g/c;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field private static volatile f:Lcom/baidu/mobads/g/c;


# instance fields
.field a:Lcom/baidu/mobads/g/o$a;

.field private volatile b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:D

.field private e:Landroid/os/Handler;

.field private final g:Landroid/content/Context;

.field private h:Lcom/baidu/mobads/g/o;

.field private final i:Lcom/baidu/mobads/g/e;

.field private j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/g/e;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 1

    .line 78
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    .line 30
    iput-object v0, p0, Lcom/baidu/mobads/g/c;->c:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/baidu/mobads/g/c;->h:Lcom/baidu/mobads/g/o;

    .line 44
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/g/c;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 49
    new-instance v0, Lcom/baidu/mobads/g/d;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/g/d;-><init>(Lcom/baidu/mobads/g/c;)V

    iput-object v0, p0, Lcom/baidu/mobads/g/c;->a:Lcom/baidu/mobads/g/o$a;

    .line 79
    iput-object p1, p0, Lcom/baidu/mobads/g/c;->g:Landroid/content/Context;

    .line 80
    iput-object p2, p0, Lcom/baidu/mobads/g/c;->i:Lcom/baidu/mobads/g/e;

    .line 82
    invoke-virtual {p2}, Lcom/baidu/mobads/g/e;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/g/c;->a(Ljava/lang/String;)V

    .line 84
    iput-object p4, p0, Lcom/baidu/mobads/g/c;->e:Landroid/os/Handler;

    .line 85
    iput-object p3, p0, Lcom/baidu/mobads/g/c;->c:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/baidu/mobads/g/e;Ljava/lang/String;Landroid/os/Handler;)Lcom/baidu/mobads/g/c;
    .locals 1

    .line 72
    sget-object v0, Lcom/baidu/mobads/g/c;->f:Lcom/baidu/mobads/g/c;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/baidu/mobads/g/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/baidu/mobads/g/c;-><init>(Landroid/content/Context;Lcom/baidu/mobads/g/e;Ljava/lang/String;Landroid/os/Handler;)V

    sput-object v0, Lcom/baidu/mobads/g/c;->f:Lcom/baidu/mobads/g/c;

    .line 75
    :cond_0
    sget-object p0, Lcom/baidu/mobads/g/c;->f:Lcom/baidu/mobads/g/c;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/g/c;)Lcom/baidu/mobads/g/c;
    .locals 0

    .line 23
    sput-object p0, Lcom/baidu/mobads/g/c;->f:Lcom/baidu/mobads/g/c;

    return-object p0
.end method

.method private a()Ljava/lang/String;
    .locals 5

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "__xadsdk__remote__final__"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/baidu/mobads/g/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 185
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 187
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 188
    iget-object v3, p0, Lcom/baidu/mobads/g/c;->h:Lcom/baidu/mobads/g/o;

    iget-object v4, p0, Lcom/baidu/mobads/g/c;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/baidu/mobads/g/o;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    .line 190
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 191
    throw v0
.end method

.method static synthetic a(Lcom/baidu/mobads/g/c;Ljava/lang/String;Lcom/baidu/mobads/g/e;Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/g/c;->a(Ljava/lang/String;Lcom/baidu/mobads/g/e;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/baidu/mobads/g/e;Ljava/lang/String;)V
    .locals 2

    const-string p3, "OK"

    .line 168
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_0

    const-string p3, "ERROR"

    .line 169
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 170
    :cond_0
    iget-object p3, p0, Lcom/baidu/mobads/g/c;->e:Landroid/os/Handler;

    invoke-virtual {p3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p3

    .line 171
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "APK_INFO"

    .line 173
    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string p2, "CODE"

    .line 174
    invoke-virtual {v0, p2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-virtual {p3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 177
    iget-object p1, p0, Lcom/baidu/mobads/g/c;->e:Landroid/os/Handler;

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void
.end method

.method private b()Z
    .locals 9

    .line 205
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/baidu/mobads/g/c;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 206
    new-instance v1, Lcom/baidu/mobads/g/o;

    iget-object v2, p0, Lcom/baidu/mobads/g/c;->g:Landroid/content/Context;

    iget-object v3, p0, Lcom/baidu/mobads/g/c;->i:Lcom/baidu/mobads/g/e;

    iget-object v4, p0, Lcom/baidu/mobads/g/c;->a:Lcom/baidu/mobads/g/o$a;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/baidu/mobads/g/o;-><init>(Landroid/content/Context;Ljava/net/URL;Lcom/baidu/mobads/g/e;Lcom/baidu/mobads/g/o$a;)V

    iput-object v1, p0, Lcom/baidu/mobads/g/c;->h:Lcom/baidu/mobads/g/o;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_2

    .line 208
    :catch_1
    :try_start_1
    new-instance v0, Lcom/baidu/mobads/g/o;

    iget-object v1, p0, Lcom/baidu/mobads/g/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/g/c;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/baidu/mobads/g/c;->i:Lcom/baidu/mobads/g/e;

    iget-object v4, p0, Lcom/baidu/mobads/g/c;->a:Lcom/baidu/mobads/g/o$a;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/baidu/mobads/g/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/g/e;Lcom/baidu/mobads/g/o$a;)V

    iput-object v0, p0, Lcom/baidu/mobads/g/c;->h:Lcom/baidu/mobads/g/o;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 219
    :goto_0
    sget-object v0, Lcom/baidu/mobads/g/g;->c:Lcom/baidu/mobads/g/a;

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    .line 220
    sget-object v0, Lcom/baidu/mobads/g/g;->c:Lcom/baidu/mobads/g/a;

    iget-wide v3, v0, Lcom/baidu/mobads/g/a;->a:D

    goto :goto_1

    .line 221
    :cond_0
    sget-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    if-eqz v0, :cond_2

    .line 222
    sget-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    iget-wide v3, v0, Lcom/baidu/mobads/g/a;->a:D

    cmpl-double v0, v3, v1

    if-lez v0, :cond_1

    .line 223
    sget-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    iget-wide v3, v0, Lcom/baidu/mobads/g/a;->a:D

    goto :goto_1

    .line 225
    :cond_1
    sget-object v0, Lcom/baidu/mobads/g/g;->b:Lcom/baidu/mobads/g/a;

    iget-wide v3, v0, Lcom/baidu/mobads/g/a;->a:D

    goto :goto_1

    :cond_2
    move-wide v3, v1

    .line 230
    :goto_1
    iget-object v0, p0, Lcom/baidu/mobads/g/c;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v5, "XAdApkDownloadThread"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isNewApkAvailable: local apk version is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v7, ", remote apk version: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/baidu/mobads/g/c;->i:Lcom/baidu/mobads/g/e;

    .line 232
    invoke-virtual {v7}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 230
    invoke-interface {v0, v5, v6}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    cmpl-double v0, v3, v1

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-lez v0, :cond_4

    .line 235
    iget-object v0, p0, Lcom/baidu/mobads/g/c;->i:Lcom/baidu/mobads/g/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v3

    cmpl-double v0, v3, v1

    if-lez v0, :cond_3

    .line 236
    iget-object v0, p0, Lcom/baidu/mobads/g/c;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAdApkDownloadThread"

    const-string v2, "remote not null, local apk version is null, force upgrade"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v0, p0, Lcom/baidu/mobads/g/c;->i:Lcom/baidu/mobads/g/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/g/c;->d:D

    return v5

    .line 240
    :cond_3
    iget-object v0, p0, Lcom/baidu/mobads/g/c;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAdApkDownloadThread"

    const-string v2, "remote is null, local apk version is null, do not upgrade"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    .line 244
    :cond_4
    iget-object v0, p0, Lcom/baidu/mobads/g/c;->i:Lcom/baidu/mobads/g/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v7

    cmpg-double v0, v7, v1

    if-gtz v0, :cond_5

    .line 245
    iget-object v0, p0, Lcom/baidu/mobads/g/c;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAdApkDownloadThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "remote apk version is: null, local apk version is: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v3, ", do not upgrade"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    .line 249
    :cond_5
    iget-object v0, p0, Lcom/baidu/mobads/g/c;->i:Lcom/baidu/mobads/g/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v0

    cmpl-double v2, v0, v3

    if-lez v2, :cond_6

    .line 251
    iget-object v0, p0, Lcom/baidu/mobads/g/c;->i:Lcom/baidu/mobads/g/e;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/e;->b()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/g/c;->d:D

    return v5

    :cond_6
    return v6

    .line 212
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "parse apk failed, error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/baidu/mobads/g/c;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAdApkDownloadThread"

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    new-instance v1, Lcom/baidu/mobads/g/g$a;

    invoke-direct {v1, v0}, Lcom/baidu/mobads/g/g$a;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/baidu/mobads/g/c;->b:Ljava/lang/String;

    .line 90
    invoke-virtual {p0}, Lcom/baidu/mobads/g/c;->interrupt()V

    return-void
.end method

.method public run()V
    .locals 6

    .line 97
    :try_start_0
    invoke-direct {p0}, Lcom/baidu/mobads/g/c;->b()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    const-string v1, "download apk successfully, downloader exit"

    .line 102
    invoke-direct {p0}, Lcom/baidu/mobads/g/c;->a()Ljava/lang/String;

    .line 105
    iget-object v2, p0, Lcom/baidu/mobads/g/c;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v3, "XAdApkDownloadThread"

    invoke-interface {v2, v3, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    sput-object v0, Lcom/baidu/mobads/g/c;->f:Lcom/baidu/mobads/g/c;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 108
    :try_start_2
    iget-object v2, p0, Lcom/baidu/mobads/g/c;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v3, "XAdApkDownloadThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "create File or HTTP Get failed, exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :goto_0
    iget-object v1, p0, Lcom/baidu/mobads/g/c;->j:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAdApkDownloadThread"

    const-string v3, "no newer apk, downloader exit"

    invoke-interface {v1, v2, v3}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    sput-object v0, Lcom/baidu/mobads/g/c;->f:Lcom/baidu/mobads/g/c;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    return-void

    :catch_1
    return-void
.end method
