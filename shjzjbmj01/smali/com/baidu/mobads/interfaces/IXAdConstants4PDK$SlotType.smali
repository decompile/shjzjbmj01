.class public final enum Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/interfaces/IXAdConstants4PDK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SlotType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SLOT_TYPE_BANNER:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_CONTENT:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_FULLSCREEN_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_INSITE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_JSSDK:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_MIDROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_OVERLAY:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_PAUSE_ROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_PORTRAIT_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_POSTROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_PREROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_REWARD_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field public static final enum SLOT_TYPE_SUG:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field private static final synthetic b:[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 223
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_JSSDK"

    const-string v2, "jssdk"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_JSSDK:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 227
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_CPU"

    const-string v2, "cpu"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 228
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_BANNER"

    const-string v2, "banner"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_BANNER:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 229
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_SPLASH"

    const-string v2, "rsplash"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 230
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_INTERSTITIAL"

    const-string v2, "int"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 231
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_FEEDS"

    const-string v2, "feed"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 232
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_INSITE"

    const-string v2, "insite"

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INSITE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 233
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_SUG"

    const-string v2, "sug"

    const/4 v10, 0x7

    invoke-direct {v0, v1, v10, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SUG:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 235
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_FULLSCREEN_VIDEO"

    const-string v2, "fvideo"

    const/16 v11, 0x8

    invoke-direct {v0, v1, v11, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FULLSCREEN_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 237
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_REWARD_VIDEO"

    const-string v2, "rvideo"

    const/16 v12, 0x9

    invoke-direct {v0, v1, v12, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_REWARD_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 239
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_PORTRAIT_VIDEO"

    const-string v2, "pvideo"

    const/16 v13, 0xa

    invoke-direct {v0, v1, v13, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PORTRAIT_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 243
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_PREROLL"

    const-string v2, "preroll"

    const/16 v14, 0xb

    invoke-direct {v0, v1, v14, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PREROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 247
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_MIDROLL"

    const-string v2, "midroll"

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_MIDROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 251
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_POSTROLL"

    const-string v2, "postroll"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_POSTROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 255
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_OVERLAY"

    const-string v2, "overlay"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_OVERLAY:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 259
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_PAUSE_ROLL"

    const-string v2, "pauseroll"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PAUSE_ROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 263
    new-instance v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const-string v1, "SLOT_TYPE_CONTENT"

    const-string v2, "content"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15, v2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CONTENT:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v0, 0x11

    .line 218
    new-array v0, v0, [Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_JSSDK:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_BANNER:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INSITE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SUG:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FULLSCREEN_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_REWARD_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PORTRAIT_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PREROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    aput-object v1, v0, v14

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_MIDROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_POSTROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_OVERLAY:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PAUSE_ROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CONTENT:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sput-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->b:[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 265
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 266
    iput-object p3, p0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->a:Ljava/lang/String;

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
    .locals 5

    .line 276
    invoke-static {}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->values()[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 277
    iget-object v4, v3, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
    .locals 1

    .line 218
    const-class v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
    .locals 1

    .line 218
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->b:[Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v0}, [Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->a:Ljava/lang/String;

    return-object v0
.end method
