.class public Lcom/baidu/mobads/AdService;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static channelId:Ljava/lang/String; = ""

.field protected static instanceCount:I = -0x1


# instance fields
.field private a:Lcom/baidu/mobads/AdView;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;Lcom/baidu/mobads/AdViewListener;)V
    .locals 7

    .line 41
    sget-object v5, Lcom/baidu/mobads/AdSize;->Banner:Lcom/baidu/mobads/AdSize;

    const-string v6, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/baidu/mobads/AdService;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;Lcom/baidu/mobads/AdViewListener;Lcom/baidu/mobads/AdSize;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;Lcom/baidu/mobads/AdViewListener;Lcom/baidu/mobads/AdSize;Ljava/lang/String;)V
    .locals 2

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    .line 62
    new-instance v0, Lcom/baidu/mobads/AdView;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p5, p6}, Lcom/baidu/mobads/AdView;-><init>(Landroid/content/Context;ZLcom/baidu/mobads/AdSize;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    .line 63
    iget-object p1, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    invoke-virtual {p1, p4}, Lcom/baidu/mobads/AdView;->setListener(Lcom/baidu/mobads/AdViewListener;)V

    .line 64
    invoke-direct {p0, p2, p3}, Lcom/baidu/mobads/AdService;->a(Landroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    sget p1, Lcom/baidu/mobads/AdService;->instanceCount:I

    add-int/lit8 p1, p1, 0x1

    sput p1, Lcom/baidu/mobads/AdService;->instanceCount:I

    return-void

    .line 55
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "One of arguments is null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Landroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    invoke-virtual {v0}, Lcom/baidu/mobads/AdView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p1, :cond_1

    .line 72
    iget-object v0, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    invoke-virtual {v0}, Lcom/baidu/mobads/AdView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    invoke-virtual {v0}, Lcom/baidu/mobads/AdView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    invoke-virtual {p1, v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 79
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void
.end method

.method public static setChannelId(Ljava/lang/String;)V
    .locals 1

    .line 29
    sput-object p0, Lcom/baidu/mobads/AdService;->channelId:Ljava/lang/String;

    .line 30
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/utils/h;->setChannelId(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    invoke-virtual {v0}, Lcom/baidu/mobads/AdView;->destroy()V

    const/4 v0, 0x0

    .line 86
    iput-object v0, p0, Lcom/baidu/mobads/AdService;->a:Lcom/baidu/mobads/AdView;

    :cond_0
    return-void
.end method
