.class public Lcom/baidu/mobads/BaiduNativeH5AdView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;
    }
.end annotation


# instance fields
.field a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

.field private b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

.field private c:Lcom/baidu/mobads/production/b/c;

.field private d:Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

.field private e:Lcom/baidu/mobad/feeds/RequestParameters;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .line 80
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->d:Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    const/4 v0, 0x0

    .line 25
    iput-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->f:Z

    .line 29
    iput-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->g:Z

    .line 31
    new-instance v0, Lcom/baidu/mobads/h;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/h;-><init>(Lcom/baidu/mobads/BaiduNativeH5AdView;)V

    iput-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    .line 81
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/BaiduNativeH5AdView;->a(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 85
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 23
    iput-object p2, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->d:Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    const/4 p2, 0x0

    .line 25
    iput-boolean p2, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->f:Z

    .line 29
    iput-boolean p2, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->g:Z

    .line 31
    new-instance v0, Lcom/baidu/mobads/h;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/h;-><init>(Lcom/baidu/mobads/BaiduNativeH5AdView;)V

    iput-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    .line 86
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/BaiduNativeH5AdView;->a(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 90
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 23
    iput-object p2, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->d:Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    const/4 p2, 0x0

    .line 25
    iput-boolean p2, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->f:Z

    .line 29
    iput-boolean p2, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->g:Z

    .line 31
    new-instance p3, Lcom/baidu/mobads/h;

    invoke-direct {p3, p0}, Lcom/baidu/mobads/h;-><init>(Lcom/baidu/mobads/BaiduNativeH5AdView;)V

    iput-object p3, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    .line 91
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/BaiduNativeH5AdView;->a(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeAdPlacement;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    return-object p0
.end method

.method private a()V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/b/c;->a()V

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;I)V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 75
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/BaiduNativeH5AdView;->setBackgroundResource(I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/BaiduNativeH5AdView;Z)Z
    .locals 0

    .line 19
    iput-boolean p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->d:Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    return-object p0
.end method

.method private b()V
    .locals 1

    .line 162
    invoke-direct {p0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->a()V

    .line 163
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/b/c;->p()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/BaiduNativeH5AdView;Z)Z
    .locals 0

    .line 19
    iput-boolean p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->g:Z

    return p1
.end method


# virtual methods
.method public getAdPlacement()Lcom/baidu/mobads/BaiduNativeAdPlacement;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    return-object v0
.end method

.method public isAdDataLoaded()Z
    .locals 1

    .line 69
    iget-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->g:Z

    return v0
.end method

.method public makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->hasValidResponse()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->f:Z

    if-eqz v0, :cond_2

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 110
    iput-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->f:Z

    .line 111
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getRequestStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->setRequestStarted(Z)V

    :cond_2
    if-nez p1, :cond_3

    .line 120
    new-instance p1, Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    invoke-direct {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->build()Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object p1

    .line 123
    :cond_3
    iput-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->e:Lcom/baidu/mobad/feeds/RequestParameters;

    .line 124
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    if-eqz v0, :cond_4

    .line 125
    invoke-direct {p0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b()V

    .line 128
    :cond_4
    new-instance v0, Lcom/baidu/mobads/production/b/c;

    invoke-virtual {p0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/baidu/mobads/production/b/c;-><init>(Landroid/content/Context;Lcom/baidu/mobads/BaiduNativeH5AdView;)V

    iput-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    .line 129
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/b/c;->a(Lcom/baidu/mobad/feeds/RequestParameters;)V

    .line 130
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    const-string v0, "AdError"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/b/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 131
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    const-string v0, "AdStarted"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/b/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 132
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    const-string v0, "AdUserClick"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/b/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 133
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    const-string v0, "AdImpression"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/b/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 134
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    const-string v0, "AdLoadData"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/b/c;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 135
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-virtual {p1}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getAdResponse()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 136
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getAdResponse()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/b/c;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    .line 139
    :cond_5
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getSessionId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/b/c;->a(I)V

    .line 140
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getPosistionId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/b/c;->c(I)V

    .line 141
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getSequenceId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/b/c;->d(I)V

    .line 143
    iget-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/b/c;->request()V

    return-void
.end method

.method public recordImpression()V
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    .line 149
    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getAdResponse()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->isWinSended()Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->c:Lcom/baidu/mobads/production/b/c;

    iget-object v1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-virtual {v1}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getAdResponse()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object v1

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->e:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {v0, p0, v1, v2}, Lcom/baidu/mobads/production/b/c;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    :cond_0
    return-void
.end method

.method public setAdPlacement(Lcom/baidu/mobads/BaiduNativeAdPlacement;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    return-void
.end method

.method public setAdPlacementData(Ljava/lang/Object;)V
    .locals 5

    .line 169
    new-instance v0, Lcom/baidu/mobads/BaiduNativeAdPlacement;

    invoke-direct {v0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;-><init>()V

    const-string v1, "getApId"

    const/4 v2, 0x0

    .line 170
    new-array v3, v2, [Ljava/lang/Class;

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, v3, v4}, Lcom/baidu/mobads/r;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->setApId(Ljava/lang/String;)V

    const-string v1, "getAppSid"

    .line 171
    new-array v3, v2, [Ljava/lang/Class;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, v3, v2}, Lcom/baidu/mobads/r;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 172
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/baidu/mobads/utils/h;->setAppId(Ljava/lang/String;)V

    .line 173
    iput-object v0, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->b:Lcom/baidu/mobads/BaiduNativeAdPlacement;

    return-void
.end method

.method public setEventListener(Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/baidu/mobads/BaiduNativeH5AdView;->d:Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    return-void
.end method
