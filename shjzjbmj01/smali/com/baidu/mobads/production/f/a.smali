.class public Lcom/baidu/mobads/production/f/a;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"


# static fields
.field private static F:I


# instance fields
.field public A:Z

.field private B:Lcom/baidu/mobads/production/f/b;

.field private C:Landroid/content/Context;

.field private D:Z

.field private E:I

.field protected final z:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;ZIIII)V
    .locals 1

    .line 66
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 47
    iput-boolean v0, p0, Lcom/baidu/mobads/production/f/a;->D:Z

    .line 52
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/f/a;->z:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 68
    invoke-virtual {p0, p3}, Lcom/baidu/mobads/production/f/a;->setId(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/f/a;->setActivity(Landroid/content/Context;)V

    .line 71
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/f/a;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 73
    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p2, p0, Lcom/baidu/mobads/production/f/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 75
    iput-object p1, p0, Lcom/baidu/mobads/production/f/a;->C:Landroid/content/Context;

    .line 77
    iput p8, p0, Lcom/baidu/mobads/production/f/a;->m:I

    .line 78
    iput p7, p0, Lcom/baidu/mobads/production/f/a;->E:I

    .line 79
    new-instance p2, Lcom/baidu/mobads/production/f/b;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/f/a;->getApplicationContext()Landroid/content/Context;

    move-result-object p7

    iget-object p8, p0, Lcom/baidu/mobads/production/f/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p2, p7, p8}, Lcom/baidu/mobads/production/f/b;-><init>(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p2, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    .line 80
    iget-object p2, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    invoke-virtual {p2, p4}, Lcom/baidu/mobads/production/f/b;->a(Z)V

    .line 82
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object p2

    if-eqz p4, :cond_1

    .line 86
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    .line 87
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingNone()Ljava/lang/String;

    move-result-object p7

    invoke-interface {p4, p7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingLandingPage()Ljava/lang/String;

    move-result-object p7

    invoke-interface {p4, p7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/l;->a()Ljava/lang/String;

    move-result-object p7

    invoke-interface {p4, p7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p7

    invoke-virtual {p7}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    invoke-static {p1}, Lcom/baidu/mobads/utils/r;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 91
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingDownload()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/baidu/mobads/utils/h;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 96
    :cond_1
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingNone()Ljava/lang/String;

    move-result-object p1

    .line 98
    :goto_0
    iget-object p2, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/production/f/b;->b(Ljava/lang/String;)V

    .line 99
    iget-object p1, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    invoke-virtual {p1, p5}, Lcom/baidu/mobads/production/f/b;->d(I)V

    .line 100
    iget-object p1, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    invoke-virtual {p1, p6}, Lcom/baidu/mobads/production/f/b;->e(I)V

    .line 102
    iget-object p1, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    invoke-virtual {p1, p3}, Lcom/baidu/mobads/production/f/b;->d(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0, p3}, Lcom/baidu/mobads/production/f/a;->d(Ljava/lang/String;)V

    return-void
.end method

.method private a(Landroid/os/Handler;)V
    .locals 0

    return-void
.end method


# virtual methods
.method public a()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    return-object v0
.end method

.method public a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;
    .locals 1

    .line 318
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/f/a;->b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 321
    :cond_0
    invoke-super {p0, p1}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 151
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/baidu/mobads/constants/a;->r:J

    .line 153
    :try_start_0
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    const-string p1, "m_new_rsplash"

    .line 154
    sget-wide v0, Lcom/baidu/mobads/constants/a;->l:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_start_request"

    .line 155
    sget-wide v0, Lcom/baidu/mobads/constants/a;->m:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_end_request"

    .line 156
    sget-wide v0, Lcom/baidu/mobads/constants/a;->n:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_start_dex"

    .line 157
    sget-wide v0, Lcom/baidu/mobads/constants/a;->o:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_end_dex"

    .line 158
    sget-wide v0, Lcom/baidu/mobads/constants/a;->p:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_start_load"

    .line 159
    sget-wide v0, Lcom/baidu/mobads/constants/a;->q:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "m_end_load"

    .line 160
    sget-wide v0, Lcom/baidu/mobads/constants/a;->r:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "isRequestAndLoadAdTimeout"

    .line 161
    iget-boolean v0, p0, Lcom/baidu/mobads/production/f/a;->D:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    invoke-virtual {p0}, Lcom/baidu/mobads/production/f/a;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 166
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    move-object v5, p1

    goto :goto_0

    :cond_0
    move-object v5, v0

    .line 168
    :goto_0
    iget-object p1, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/f/b;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getAttribute()Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_1

    .line 170
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :try_start_1
    const-string v0, "splashTipStyle"

    .line 173
    iget v1, p0, Lcom/baidu/mobads/production/f/a;->E:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 177
    :catch_0
    :try_start_2
    iget-object v0, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/b;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/vo/b;

    .line 178
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/vo/b;->a(Lorg/json/JSONObject;)V

    .line 179
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/f/a;->C:Landroid/content/Context;

    const-string v4, "386"

    iget-object p1, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    .line 180
    invoke-virtual {p1}, Lcom/baidu/mobads/production/f/b;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v6

    .line 179
    invoke-virtual/range {v2 .. v7}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Ljava/util/HashMap;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 184
    :catch_1
    iget-boolean p1, p0, Lcom/baidu/mobads/production/f/a;->D:Z

    if-eqz p1, :cond_2

    return-void

    .line 193
    :cond_2
    invoke-virtual {p0}, Lcom/baidu/mobads/production/f/a;->start()V

    .line 194
    new-instance p1, Landroid/os/Handler;

    iget-object v0, p0, Lcom/baidu/mobads/production/f/a;->C:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    if-nez p2, :cond_3

    .line 199
    :try_start_3
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/f/a;->a(Landroid/os/Handler;)V

    goto :goto_1

    :cond_3
    const-string v0, "AdInstance"

    .line 201
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v3, p2

    check-cast v3, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 202
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p2

    .line 203
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-eq p2, v0, :cond_4

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-eq p2, v0, :cond_4

    .line 204
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/f/a;->a(Landroid/os/Handler;)V

    goto :goto_1

    .line 206
    :cond_4
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/f/a;->C:Landroid/content/Context;

    const-string v2, "383"

    iget-object p1, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    .line 208
    invoke-virtual {p1}, Lcom/baidu/mobads/production/f/b;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v4

    const/4 p1, 0x1

    new-array v5, p1, [Ljava/lang/Object;

    const/4 p1, 0x0

    const-string p2, "processAdLoaded"

    aput-object p2, v5, p1

    .line 206
    invoke-virtual/range {v0 .. v5}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    :goto_1
    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 4

    .line 133
    iget-object v0, p0, Lcom/baidu/mobads/production/f/a;->k:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/vo/b;

    .line 134
    invoke-virtual {v0}, Lcom/baidu/mobads/vo/b;->getAttribute()Lorg/json/JSONObject;

    move-result-object v1

    if-nez v1, :cond_0

    .line 136
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    :try_start_0
    const-string v2, "bitmapDisplayMode"

    .line 139
    sget v3, Lcom/baidu/mobads/production/f/a;->F:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "countDownNew"

    const/4 v3, 0x1

    .line 141
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :catch_0
    invoke-virtual {v0, v1}, Lcom/baidu/mobads/vo/b;->a(Lorg/json/JSONObject;)V

    int-to-double v0, p3

    .line 146
    invoke-virtual {p2, p1, v0, v1}, Lcom/baidu/mobads/production/p;->a(Lcom/baidu/mobads/openad/b/b;D)V

    return-void
.end method

.method public a(ZLcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    return-void
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    if-eqz p2, :cond_1

    const-string p1, "AdInstance"

    .line 249
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 250
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    .line 251
    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-eq p1, p2, :cond_0

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, p2, :cond_1

    .line 252
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/f/a;->C:Landroid/content/Context;

    const-string v2, "383"

    iget-object p1, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    .line 254
    invoke-virtual {p1}, Lcom/baidu/mobads/production/f/b;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v4

    const/4 p1, 0x1

    new-array v5, p1, [Ljava/lang/Object;

    const/4 p1, 0x0

    const-string p2, "processAdStart"

    aput-object p2, v5, p1

    .line 252
    invoke-virtual/range {v0 .. v5}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 2

    .line 266
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/baidu/mobads/constants/a;->q:J

    .line 267
    iget-object v0, p0, Lcom/baidu/mobads/production/f/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobads/production/f/a;->A:Z

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/baidu/mobads/production/f/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    :cond_0
    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 301
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/production/a;->e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    if-eqz p2, :cond_0

    :try_start_0
    const-string p1, "video_close_reason"

    .line 304
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 305
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 306
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/f/a;->C:Landroid/content/Context;

    const-string v2, "383"

    const/4 v3, 0x0

    iget-object p2, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    .line 308
    invoke-virtual {p2}, Lcom/baidu/mobads/production/f/b;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v4

    const/4 p2, 0x2

    new-array v5, p2, [Ljava/lang/Object;

    const/4 p2, 0x0

    const-string v6, "closead"

    aput-object v6, v5, p2

    const/4 p2, 0x1

    aput-object p1, v5, p2

    .line 306
    invoke-virtual/range {v0 .. v5}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 0

    .line 327
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/f/a;->b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result p1

    return p1
.end method

.method public f()V
    .locals 7

    const/4 v0, 0x1

    .line 232
    iput-boolean v0, p0, Lcom/baidu/mobads/production/f/a;->D:Z

    .line 234
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/f/a;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 237
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v1

    :cond_0
    move-object v4, v1

    .line 239
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/f/a;->C:Landroid/content/Context;

    const-string v3, "382"

    iget-object v0, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    .line 240
    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/b;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v5

    const/4 v6, 0x0

    .line 239
    invoke-virtual/range {v1 .. v6}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Ljava/util/HashMap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 242
    iget-object v1, p0, Lcom/baidu/mobads/production/f/a;->z:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/baidu/mobads/production/f/a;->a()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public r()Ljava/util/HashMap;
    .locals 2

    .line 344
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 345
    invoke-super {p0}, Lcom/baidu/mobads/production/a;->r()Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    return-object v1

    :cond_0
    return-object v0
.end method

.method public request()V
    .locals 8

    .line 113
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/baidu/mobads/constants/a;->m:J

    .line 114
    invoke-virtual {p0}, Lcom/baidu/mobads/production/f/a;->m()V

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/production/f/a;->B:Lcom/baidu/mobads/production/f/b;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/f/a;->a(Lcom/baidu/mobads/vo/d;)Z

    .line 121
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/baidu/mobads/production/f/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/f/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/baidu/mobads/utils/e;->a(Landroid/app/Activity;Landroid/content/Context;)Landroid/webkit/WebView;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, ""

    const-string v5, "text/html"

    const-string v6, "UTF-8"

    const/4 v7, 0x0

    .line 123
    invoke-virtual/range {v2 .. v7}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method
