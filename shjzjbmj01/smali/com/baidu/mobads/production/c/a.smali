.class public Lcom/baidu/mobads/production/c/a;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;


# instance fields
.field private A:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

.field private B:Landroid/webkit/WebView;

.field private z:Lcom/baidu/mobads/production/c/b;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;)V
    .locals 3

    .line 43
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/production/c/a;->B:Landroid/webkit/WebView;

    .line 46
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/c/a;->setActivity(Landroid/content/Context;)V

    .line 48
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_JSSDK:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/c/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 49
    new-instance p1, Lcom/baidu/mobads/production/c/b;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/c/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p1, v0, v1, v2}, Lcom/baidu/mobads/production/c/b;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/c/a;->z:Lcom/baidu/mobads/production/c/b;

    return-void
.end method


# virtual methods
.method public a()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/baidu/mobads/production/c/a;->z:Lcom/baidu/mobads/production/c/b;

    return-object v0
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 93
    :try_start_0
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast p1, Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    iput-object p1, p0, Lcom/baidu/mobads/production/c/a;->A:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    .line 94
    iget-object p1, p0, Lcom/baidu/mobads/production/c/a;->A:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    iget-object p2, p0, Lcom/baidu/mobads/production/c/a;->B:Landroid/webkit/WebView;

    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;->setCustomerWebView(Landroid/webkit/WebView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    .line 96
    iput-object p1, p0, Lcom/baidu/mobads/production/c/a;->A:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    .line 100
    :goto_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/a;->start()V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 0

    .line 77
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "{\'ad\':[{\'id\':99999999,\'url\':\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/baidu/mobads/production/c/a;->z:Lcom/baidu/mobads/production/c/b;

    .line 78
    invoke-virtual {p2}, Lcom/baidu/mobads/production/c/b;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\', type=\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HYBRID:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 79
    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'}],\'n\':1}"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 82
    :try_start_0
    new-instance p2, Lcom/baidu/mobads/vo/c;

    invoke-direct {p2, p1}, Lcom/baidu/mobads/vo/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/c/a;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string p1, "XAdMouldeLoader ad-server requesting success"

    .line 87
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/c/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/baidu/mobads/production/c/a;->A:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/c/a;->A:Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/interfaces/IXHybridAdRenderer;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p1

    :goto_0
    return p1
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method protected b(Lcom/baidu/mobads/vo/d;)V
    .locals 1

    .line 69
    iput-object p1, p0, Lcom/baidu/mobads/production/c/a;->k:Lcom/baidu/mobads/vo/d;

    .line 70
    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/a;->k()V

    const/4 p1, 0x0

    const/16 v0, 0x1388

    .line 71
    invoke-virtual {p0, p1, p1, v0}, Lcom/baidu/mobads/production/c/a;->a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V

    return-void
.end method

.method public c()V
    .locals 0

    .line 54
    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/a;->load()V

    return-void
.end method

.method protected d()V
    .locals 1

    const/16 v0, 0x2710

    .line 59
    iput v0, p0, Lcom/baidu/mobads/production/a;->m:I

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 113
    invoke-super {p0}, Lcom/baidu/mobads/production/a;->p()V

    .line 114
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdUserClose"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/c/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/baidu/mobads/production/c/a;->a()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public request()V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/baidu/mobads/production/c/a;->z:Lcom/baidu/mobads/production/c/b;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/c/a;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method
