.class public Lcom/baidu/mobads/production/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Lcom/baidu/mobads/production/a;

.field private c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Lcom/baidu/mobads/interfaces/IXAdContainer;

.field private h:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

.field private i:Lcom/baidu/mobads/utils/h;

.field private j:Lcom/baidu/mobads/utils/t;

.field private k:Lcom/baidu/mobads/interfaces/IXAdResource;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/production/a;)V
    .locals 3

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 66
    iput v0, p0, Lcom/baidu/mobads/production/l;->l:I

    const/4 v1, 0x2

    .line 68
    iput v1, p0, Lcom/baidu/mobads/production/l;->m:I

    const/16 v2, 0xf

    .line 69
    iput v2, p0, Lcom/baidu/mobads/production/l;->n:I

    .line 100
    iput v0, p0, Lcom/baidu/mobads/production/l;->o:I

    .line 102
    iput v1, p0, Lcom/baidu/mobads/production/l;->p:I

    .line 103
    iput v2, p0, Lcom/baidu/mobads/production/l;->q:I

    .line 56
    iput-object p1, p0, Lcom/baidu/mobads/production/l;->a:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    .line 60
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/production/l;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 61
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/production/l;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 62
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 63
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/production/l;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/l;)I
    .locals 0

    .line 39
    iget p0, p0, Lcom/baidu/mobads/production/l;->o:I

    return p0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    const/4 v0, 0x0

    .line 106
    :try_start_0
    iput v0, p0, Lcom/baidu/mobads/production/l;->o:I

    .line 107
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 108
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    move-result-object v3

    .line 109
    new-instance v7, Lcom/baidu/mobads/production/m;

    move-object v1, v7

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/baidu/mobads/production/m;-><init>(Lcom/baidu/mobads/production/l;Lcom/baidu/mobads/interfaces/utils/IXAdPackageUtils;Landroid/content/Context;Ljava/lang/String;Ljava/util/Timer;)V

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x3e8

    move-object v1, v0

    move-object v2, v7

    .line 129
    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 511
    new-instance v0, Lcom/baidu/mobads/openad/b/a;

    invoke-direct {v0}, Lcom/baidu/mobads/openad/b/a;-><init>()V

    .line 512
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 513
    new-instance v2, Lcom/baidu/mobads/openad/b/b;

    const-string v3, ""

    invoke-direct {v2, v1, v3}, Lcom/baidu/mobads/openad/b/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 514
    iput v1, v2, Lcom/baidu/mobads/openad/b/b;->e:I

    .line 515
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/b;Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/production/l;)I
    .locals 0

    .line 39
    iget p0, p0, Lcom/baidu/mobads/production/l;->p:I

    return p0
.end method

.method static synthetic c(Lcom/baidu/mobads/production/l;)I
    .locals 0

    .line 39
    iget p0, p0, Lcom/baidu/mobads/production/l;->q:I

    return p0
.end method

.method static synthetic d(Lcom/baidu/mobads/production/l;)I
    .locals 2

    .line 39
    iget v0, p0, Lcom/baidu/mobads/production/l;->o:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/baidu/mobads/production/l;->o:I

    return v0
.end method

.method static synthetic e(Lcom/baidu/mobads/production/l;)Lcom/baidu/mobads/production/a;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/util/HashMap;Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdResource;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;",
            "Lcom/baidu/mobads/interfaces/IXAdResource;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ")Z"
        }
    .end annotation

    if-eqz p1, :cond_0

    const-string v0, "lpShoubaiStyle"

    .line 249
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "lpShoubaiStyle"

    .line 250
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "video_and_web"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    new-instance v0, Lcom/baidu/mobads/command/b/c;

    .line 252
    invoke-interface {p4}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getWebUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p2, p4, p3, v1}, Lcom/baidu/mobads/command/b/c;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;Ljava/lang/String;)V

    const-string p2, "lpShoubaiStyle"

    .line 253
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/baidu/mobads/command/b/c;->f:Ljava/lang/String;

    const-string p2, "lpMurlStyle"

    .line 254
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, v0, Lcom/baidu/mobads/command/b/c;->g:Ljava/lang/String;

    .line 255
    invoke-virtual {v0}, Lcom/baidu/mobads/command/b/c;->a()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onAdClicked(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    .line 141
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v3

    iput-object v3, v0, Lcom/baidu/mobads/production/l;->i:Lcom/baidu/mobads/utils/h;

    .line 142
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v3

    check-cast v3, Lcom/baidu/mobads/utils/t;

    iput-object v3, v0, Lcom/baidu/mobads/production/l;->j:Lcom/baidu/mobads/utils/t;

    .line 143
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v3

    invoke-virtual {v3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object v3

    .line 145
    iget-object v4, v0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    move-object/from16 v5, p1

    .line 146
    iput-object v5, v0, Lcom/baidu/mobads/production/l;->g:Lcom/baidu/mobads/interfaces/IXAdContainer;

    .line 147
    iput-object v1, v0, Lcom/baidu/mobads/production/l;->h:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 148
    invoke-interface/range {p1 .. p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v6

    invoke-interface {v6}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdResource()Lcom/baidu/mobads/interfaces/IXAdResource;

    move-result-object v6

    iput-object v6, v0, Lcom/baidu/mobads/production/l;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    .line 152
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v6

    .line 154
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v7

    .line 156
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 157
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getThirdClickTrackingUrls()Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 158
    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v12

    if-ge v11, v12, :cond_0

    .line 160
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    const-string v13, "\\$\\{PROGRESS\\}"

    .line 162
    invoke-interface/range {p1 .. p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getPlayheadTime()D

    move-result-wide v14

    double-to-int v14, v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    .line 160
    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 163
    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 165
    :cond_0
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 166
    invoke-interface {v9, v8}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 167
    invoke-direct {v0, v9}, Lcom/baidu/mobads/production/l;->a(Ljava/util/Set;)V

    .line 170
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeOpenExternalApp()I

    move-result v8

    const/4 v9, 0x1

    if-ne v7, v8, :cond_2

    .line 173
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v2

    const-string v3, "XAdContainerEventListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "APO in proxy: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/baidu/mobads/utils/q;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    const/4 v10, 0x1

    goto/16 :goto_6

    .line 174
    :cond_2
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result v8

    if-ne v7, v8, :cond_3

    .line 175
    iget-object v3, v0, Lcom/baidu/mobads/production/l;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-virtual {v0, v2, v4, v3, v1}, Lcom/baidu/mobads/production/l;->a(Ljava/util/HashMap;Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdResource;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 178
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v2

    const-string v3, "XAdContainerEventListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "download apk in proxy: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAppName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 178
    invoke-virtual {v2, v3, v4}, Lcom/baidu/mobads/utils/q;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 181
    :cond_3
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeLandingPage()I

    move-result v8

    if-eq v7, v8, :cond_b

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeOpenMap()I

    move-result v8

    if-ne v7, v8, :cond_4

    goto/16 :goto_5

    .line 200
    :cond_4
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeMakeCall()I

    move-result v2

    if-eq v7, v2, :cond_7

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeSendSMS()I

    move-result v2

    if-eq v7, v2, :cond_7

    .line 201
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeSendMail()I

    move-result v2

    if-ne v7, v2, :cond_5

    goto :goto_2

    .line 230
    :cond_5
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeNothing2Do()I

    move-result v2

    if-ne v7, v2, :cond_6

    goto/16 :goto_6

    .line 232
    :cond_6
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeRichMedia()I

    goto/16 :goto_6

    .line 204
    :cond_7
    :goto_2
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 205
    new-instance v2, Lcom/baidu/mobads/command/a/a;

    iget-object v5, v0, Lcom/baidu/mobads/production/l;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-direct {v2, v4, v1, v5, v6}, Lcom/baidu/mobads/command/a/a;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/baidu/mobads/command/a/a;->a()V

    .line 208
    :cond_8
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeMakeCall()I

    move-result v2

    if-ne v7, v2, :cond_1

    .line 209
    invoke-interface {v4}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 210
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/16 v5, 0x40

    .line 212
    invoke-virtual {v2, v3, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 213
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v3, 0x0

    move-object v5, v3

    const/4 v3, 0x0

    .line 216
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_a

    if-lt v3, v9, :cond_9

    if-eqz v5, :cond_9

    .line 218
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v6, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    goto :goto_4

    .line 223
    :cond_9
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_a
    const/4 v10, 0x1

    :goto_4
    if-eqz v10, :cond_1

    .line 226
    invoke-interface {v4}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v5}, Lcom/baidu/mobads/production/l;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 182
    :cond_b
    :goto_5
    iget-object v7, v0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    invoke-virtual {v7}, Lcom/baidu/mobads/production/a;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v7

    invoke-interface {v7}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getProductionTypeSplash()Ljava/lang/String;

    move-result-object v3

    if-eq v7, v3, :cond_c

    const/4 v10, 0x1

    .line 185
    :cond_c
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 186
    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->isInapp()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 187
    iget-object v3, v0, Lcom/baidu/mobads/production/l;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-virtual {v0, v2, v4, v3, v1}, Lcom/baidu/mobads/production/l;->a(Ljava/util/HashMap;Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdResource;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 188
    new-instance v3, Lcom/baidu/mobads/command/b/c;

    iget-object v5, v0, Lcom/baidu/mobads/production/l;->k:Lcom/baidu/mobads/interfaces/IXAdResource;

    invoke-direct {v3, v4, v1, v5, v6}, Lcom/baidu/mobads/command/b/c;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;Ljava/lang/String;)V

    if-eqz v2, :cond_d

    const-string v4, "lpShoubaiStyle"

    .line 191
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "lpShoubaiStyle"

    .line 192
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v3, Lcom/baidu/mobads/command/b/c;->f:Ljava/lang/String;

    .line 194
    :cond_d
    invoke-virtual {v3}, Lcom/baidu/mobads/command/b/c;->a()V

    goto :goto_6

    .line 197
    :cond_e
    iget-object v2, v0, Lcom/baidu/mobads/production/l;->i:Lcom/baidu/mobads/utils/h;

    invoke-interface/range {p1 .. p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v3

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v6}, Lcom/baidu/mobads/utils/h;->browserOutside(Landroid/content/Context;Ljava/lang/String;)V

    :cond_f
    :goto_6
    if-eqz v10, :cond_10

    .line 239
    iget-object v2, v0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance v3, Lcom/baidu/mobads/e/a;

    const-string v4, "AdClickThru"

    invoke-direct {v3, v4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 241
    :cond_10
    new-instance v2, Lcom/baidu/mobads/e/a;

    const-string v3, "AdUserClick"

    invoke-direct {v2, v3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    .line 242
    invoke-virtual {v2, v1}, Lcom/baidu/mobads/e/a;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    .line 243
    invoke-virtual {v2}, Lcom/baidu/mobads/e/a;->a()Ljava/util/HashMap;

    move-result-object v3

    const-string v4, "actionType"

    invoke-interface/range {p2 .. p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getActionType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    iget-object v1, v0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdCustomEvent(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string p2, "onrvideocachesucc"

    .line 495
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 496
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdRvdieoCacheSucc"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    :cond_0
    const-string p2, "onrvideocachefailed"

    .line 497
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 498
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdRvdieoCacheFailed"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    :cond_1
    const-string p2, "playCompletion"

    .line 499
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 500
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "PlayCompletion"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    :cond_2
    const-string p2, "AdRvdieoPlayError"

    .line 501
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    .line 502
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdRvdieoPlayError"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    :cond_3
    const-string p2, "AdVideoSkip"

    .line 503
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 504
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdVideoSkip"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    :cond_4
    const-string p2, "AdStatusChange"

    .line 505
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 506
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdStatusChange"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public onAdDurationChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 383
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdError(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 335
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_0

    .line 336
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 337
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdError"

    invoke-direct {p2, p3, p4}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_0
    return-void
.end method

.method public onAdExpandedChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 368
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdImpression(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 300
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getImpressionUrls()Ljava/util/Set;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/l;->a(Ljava/util/Set;)V

    .line 301
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p3, "AdImpression"

    invoke-direct {p1, p3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p1, p2}, Lcom/baidu/mobads/e/a;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    .line 303
    iget-object p2, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdInteraction(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 431
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdLinearChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 361
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdLoaded(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 265
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    if-ne p2, p3, :cond_0

    .line 266
    iget-object p2, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    invoke-virtual {p2, p1, p4}, Lcom/baidu/mobads/production/a;->c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    goto :goto_0

    .line 268
    :cond_0
    new-instance p2, Landroid/os/Handler;

    iget-object p3, p0, Lcom/baidu/mobads/production/l;->a:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p3, Lcom/baidu/mobads/production/n;

    invoke-direct {p3, p0, p1, p4}, Lcom/baidu/mobads/production/n;-><init>(Lcom/baidu/mobads/production/l;Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    invoke-virtual {p2, p3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public onAdPaused(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 356
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdPaused"

    invoke-direct {p2, p3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdPlaying(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 347
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdPlaying"

    invoke-direct {p2, p3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdRemainingTimeChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 391
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdSizeChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 407
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdSkippableStateChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 415
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdSkipped(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 423
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdStarted(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 280
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    if-ne p2, p3, :cond_0

    .line 281
    iget-object p2, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    invoke-virtual {p2, p1, p4}, Lcom/baidu/mobads/production/a;->d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    goto :goto_0

    .line 283
    :cond_0
    new-instance p2, Landroid/os/Handler;

    iget-object p3, p0, Lcom/baidu/mobads/production/l;->a:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p3, Lcom/baidu/mobads/production/o;

    invoke-direct {p3, p0, p1, p4}, Lcom/baidu/mobads/production/o;-><init>(Lcom/baidu/mobads/production/l;Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    invoke-virtual {p2, p3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public onAdStoped(Lcom/baidu/mobads/interfaces/IXAdContainer;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 315
    new-instance p3, Ljava/util/HashSet;

    invoke-direct {p3}, Ljava/util/HashSet;-><init>()V

    .line 316
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCloseTrackers()Ljava/util/List;

    move-result-object p2

    invoke-interface {p3, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 317
    invoke-direct {p0, p3}, Lcom/baidu/mobads/production/l;->a(Ljava/util/Set;)V

    .line 320
    :cond_0
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 321
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object p1

    .line 322
    iget-object p2, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object p3

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    invoke-virtual {p2, p3, p1}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    goto :goto_0

    .line 324
    :cond_1
    iget-object p2, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    invoke-virtual {p2, p1, p5}, Lcom/baidu/mobads/production/a;->e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    .line 325
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdStopped"

    invoke-direct {p2, p3, p5}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :goto_0
    return-void
.end method

.method public onAdUserAcceptInvitation(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 439
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdUserClosed(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 378
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->b:Lcom/baidu/mobads/production/a;

    new-instance p2, Lcom/baidu/mobads/e/a;

    const-string p3, "AdUserClose"

    invoke-direct {p2, p3}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public onAdUserMinimize(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 447
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdVideoComplete(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 487
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdVideoFirstQuartile(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 463
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdVideoMidpoint(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 471
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdVideoStart(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 455
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdVideoThirdQuartile(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 479
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public onAdVolumeChange(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/lang/Boolean;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/lang/Boolean;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 399
    iget-object p1, p0, Lcom/baidu/mobads/production/l;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    return-void
.end method
