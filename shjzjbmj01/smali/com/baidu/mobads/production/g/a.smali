.class public Lcom/baidu/mobads/production/g/a;
.super Lcom/baidu/mobads/vo/d;
.source "SourceFile"


# instance fields
.field protected a:Lcom/baidu/mobads/interfaces/IXAdProd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Lcom/baidu/mobads/interfaces/IXAdProd;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/vo/d;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    .line 26
    iget-object p1, p0, Lcom/baidu/mobads/production/g/a;->i:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    const-string p2, "http://mobads.baidu.com/cpro/ui/mads.php"

    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->replaceURLWithSupportProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/g/a;->b:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/baidu/mobads/production/g/a;->a:Lcom/baidu/mobads/interfaces/IXAdProd;

    return-void
.end method


# virtual methods
.method protected a()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "mimetype"

    const-string v2, "video/mp4,image/jpg,image/gif,image/png"

    .line 42
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "prod"

    const-string v2, "video"

    .line 43
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "fet"

    const-string v2, "ANTI,HTML,MSSP,VIDEO,NMON"

    .line 44
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "at"

    const-string v2, "10"

    .line 45
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "n"

    const-string v2, "1"

    .line 46
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v1, p0, Lcom/baidu/mobads/production/g/a;->a:Lcom/baidu/mobads/interfaces/IXAdProd;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdProd;->getProdBase()Landroid/view/ViewGroup;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/g/a;->getApt()I

    move-result v1

    sget-object v2, Lcom/baidu/mobads/AdSize;->PrerollVideoNative:Lcom/baidu/mobads/AdSize;

    invoke-virtual {v2}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v2

    if-eq v1, v2, :cond_0

    const-string v1, "lw"

    const-string v2, "640"

    .line 49
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "lh"

    const-string v2, "480"

    .line 50
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 59
    invoke-super {p0}, Lcom/baidu/mobads/vo/d;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
