.class public Lcom/baidu/mobads/production/k;
.super Lcom/baidu/mobads/openad/a/c;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXAdContainerContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/production/k$a;
    }
.end annotation


# instance fields
.field public b:Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;

.field private c:Landroid/content/Context;

.field private d:Landroid/app/Activity;

.field private e:Landroid/widget/RelativeLayout;

.field private f:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

.field private g:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

.field private h:Landroid/view/View;

.field private i:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

.field private j:J

.field private k:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Landroid/widget/RelativeLayout;Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;Lcom/baidu/mobads/interfaces/IXAdResponseInfo;Landroid/view/View;)V
    .locals 2

    .line 100
    invoke-direct {p0}, Lcom/baidu/mobads/openad/a/c;-><init>()V

    const-wide/16 v0, 0x0

    .line 156
    iput-wide v0, p0, Lcom/baidu/mobads/production/k;->j:J

    .line 166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/production/k;->k:Ljava/util/HashMap;

    .line 101
    iput-object p1, p0, Lcom/baidu/mobads/production/k;->c:Landroid/content/Context;

    .line 102
    iput-object p2, p0, Lcom/baidu/mobads/production/k;->d:Landroid/app/Activity;

    .line 104
    iput-object p3, p0, Lcom/baidu/mobads/production/k;->i:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    .line 106
    iput-object p4, p0, Lcom/baidu/mobads/production/k;->e:Landroid/widget/RelativeLayout;

    .line 108
    iput-object p5, p0, Lcom/baidu/mobads/production/k;->b:Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;

    .line 110
    iput-object p6, p0, Lcom/baidu/mobads/production/k;->f:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    .line 112
    invoke-interface {p6}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/k;->g:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 114
    iput-object p7, p0, Lcom/baidu/mobads/production/k;->h:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public createOAdTimer(I)Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public createOAdTimer(II)Lcom/baidu/mobads/openad/interfaces/utils/IOAdTimer;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public fireAdMetrics(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 189
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    .line 190
    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->addParameters(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object p1

    .line 193
    new-instance p2, Lcom/baidu/mobads/openad/b/a;

    invoke-direct {p2}, Lcom/baidu/mobads/openad/b/a;-><init>()V

    .line 195
    new-instance v0, Lcom/baidu/mobads/openad/b/b;

    const-string v1, ""

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/openad/b/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 196
    iput p1, v0, Lcom/baidu/mobads/openad/b/b;->e:I

    .line 197
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Lcom/baidu/mobads/openad/b/a;->a(Lcom/baidu/mobads/openad/b/b;Ljava/lang/Boolean;)V

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->d:Landroid/app/Activity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/k;->e:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/baidu/mobads/production/k;->d:Landroid/app/Activity;

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->d:Landroid/app/Activity;

    return-object v0
.end method

.method public getAdConstants()Lcom/baidu/mobads/interfaces/utils/IXAdConstants;
    .locals 1

    .line 202
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object v0

    return-object v0
.end method

.method public getAdContainerListener()Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->b:Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;

    return-object v0
.end method

.method public getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->g:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-object v0
.end method

.method public getAdLeadingView()Landroid/view/View;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->h:Landroid/view/View;

    return-object v0
.end method

.method public getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;
    .locals 1

    .line 257
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    return-object v0
.end method

.method public getAdProdBase()Landroid/widget/RelativeLayout;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->e:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getAdProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->i:Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    return-object v0
.end method

.method public getAdResource()Lcom/baidu/mobads/interfaces/IXAdResource;
    .locals 1

    .line 262
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdResource()Lcom/baidu/mobads/utils/s;

    move-result-object v0

    return-object v0
.end method

.method public getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->f:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    return-object v0
.end method

.method public getAdUitls4URI()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;
    .locals 1

    .line 207
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getURIUitls()Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4Activity()Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;
    .locals 1

    .line 232
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getActivityUtils()Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4Bitmap()Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;
    .locals 1

    .line 212
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBitmapUtils()Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4Common()Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;
    .locals 1

    .line 237
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4IO()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;
    .locals 1

    .line 222
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4Package()Lcom/baidu/mobads/interfaces/utils/IXAdPackageUtils;
    .locals 1

    .line 227
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4System()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;
    .locals 1

    .line 242
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    return-object v0
.end method

.method public getAdUtils4View()Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;
    .locals 1

    .line 217
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getViewUtils()Lcom/baidu/mobads/interfaces/utils/IXAdViewUtils;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->c:Landroid/content/Context;

    return-object v0
.end method

.method public getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;
    .locals 1

    .line 252
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBase64()Lcom/baidu/mobads/interfaces/utils/IBase64;

    move-result-object v0

    return-object v0
.end method

.method public getDownloaderManager(Landroid/content/Context;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;
    .locals 1

    .line 247
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getDownloaderManager(Landroid/content/Context;)Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloaderManager;

    move-result-object p1

    return-object p1
.end method

.method public getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;
    .locals 1

    .line 267
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    return-object v0
.end method

.method public getProxyVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "8.8427"

    return-object v0
.end method

.method public processCommand(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 160
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/baidu/mobads/production/k;->j:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 161
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/production/k;->j:J

    .line 162
    new-instance v0, Lcom/baidu/mobads/production/k$a;

    const-string v1, "process_command"

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/baidu/mobads/production/k$a;-><init>(Lcom/baidu/mobads/production/k;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/k;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_0
    return-void
.end method

.method public registerAdService(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 170
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->k:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    new-instance v0, Lcom/baidu/mobads/production/k$a;

    const-string v1, "regsiter_adservice"

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/baidu/mobads/production/k$a;-><init>(Lcom/baidu/mobads/production/k;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/k;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_0
    return-void
.end method

.method public unregisterAdService(Ljava/lang/String;)V
    .locals 3

    .line 178
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/baidu/mobads/production/k;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    new-instance v0, Lcom/baidu/mobads/production/k$a;

    const-string v1, "unregsiter_adservice"

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/baidu/mobads/production/k$a;-><init>(Lcom/baidu/mobads/production/k;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/k;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_0
    return-void
.end method
