.class public Lcom/baidu/mobads/production/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeLoadAdListener;


# instance fields
.field private a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;


# direct methods
.method public constructor <init>(Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    return-void
.end method


# virtual methods
.method public onADExposed(Lcom/baidu/mobad/feeds/NativeResponse;)V
    .locals 1

    .line 35
    instance-of v0, p1, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    if-eqz v0, :cond_0

    .line 36
    check-cast p1, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->onADExposed()V

    :cond_0
    return-void
.end method

.method public onAdClick(Lcom/baidu/mobad/feeds/NativeResponse;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    instance-of v0, v0, Lcom/baidu/mobad/feeds/BaiduNativeManager$PortraitVideoAdListener;

    if-eqz v0, :cond_0

    .line 44
    iget-object p1, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    check-cast p1, Lcom/baidu/mobad/feeds/BaiduNativeManager$PortraitVideoAdListener;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/BaiduNativeManager$PortraitVideoAdListener;->onAdClick()V

    return-void

    .line 48
    :cond_0
    instance-of v0, p1, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    if-eqz v0, :cond_1

    .line 49
    check-cast p1, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->onAdClick()V

    :cond_1
    return-void
.end method

.method public onLoadFail(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    instance-of v0, v0, Lcom/baidu/mobad/feeds/BaiduNativeManager$NativeLoadListener;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    check-cast v0, Lcom/baidu/mobad/feeds/BaiduNativeManager$NativeLoadListener;

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobad/feeds/BaiduNativeManager$NativeLoadListener;->onLoadFail(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onLpClosed()V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;->onLpClosed()V

    :cond_0
    return-void
.end method

.method public onNativeFail(Lcom/baidu/mobad/feeds/NativeErrorCode;)V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    invoke-interface {v0, p1}, Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;->onNativeFail(Lcom/baidu/mobad/feeds/NativeErrorCode;)V

    :cond_0
    return-void
.end method

.method public onNativeLoad(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/baidu/mobad/feeds/NativeResponse;",
            ">;)V"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    invoke-interface {v0, p1}, Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;->onNativeLoad(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public onVideoDownloadFailed()V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;->onVideoDownloadFailed()V

    :cond_0
    return-void
.end method

.method public onVideoDownloadSuccess()V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/baidu/mobads/production/b/a;->a:Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/BaiduNativeManager$FeedAdListener;->onVideoDownloadSuccess()V

    :cond_0
    return-void
.end method
