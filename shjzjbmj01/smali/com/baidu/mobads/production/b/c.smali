.class public Lcom/baidu/mobads/production/b/c;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"


# instance fields
.field private A:Lcom/baidu/mobads/BaiduNativeH5AdView;

.field private z:Lcom/baidu/mobads/production/b/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/BaiduNativeH5AdView;)V
    .locals 2

    .line 40
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    .line 41
    iput-object p2, p0, Lcom/baidu/mobads/production/b/c;->A:Lcom/baidu/mobads/BaiduNativeH5AdView;

    .line 42
    iget-object p2, p0, Lcom/baidu/mobads/production/b/c;->A:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-virtual {p2}, Lcom/baidu/mobads/BaiduNativeH5AdView;->getAdPlacement()Lcom/baidu/mobads/BaiduNativeAdPlacement;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getApId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/b/c;->setId(Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b/c;->setActivity(Landroid/content/Context;)V

    .line 44
    iget-object p1, p0, Lcom/baidu/mobads/production/b/c;->A:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b/c;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 45
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/b/c;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 47
    new-instance p1, Lcom/baidu/mobads/production/b/d;

    .line 48
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/c;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/c;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p1, p2, v0, v1}, Lcom/baidu/mobads/production/b/d;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    .line 49
    iget-object p1, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    iget-object p2, p0, Lcom/baidu/mobads/production/b/c;->A:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-virtual {p2}, Lcom/baidu/mobads/BaiduNativeH5AdView;->getAdPlacement()Lcom/baidu/mobads/BaiduNativeAdPlacement;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getApId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b/d;->d(Ljava/lang/String;)V

    .line 51
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object p1

    .line 52
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 53
    invoke-virtual {p1}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingLandingPage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    invoke-virtual {p1}, Lcom/baidu/mobads/utils/l;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/baidu/mobads/utils/r;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p1}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingDownload()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/utils/h;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    .line 59
    iget-object p2, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/production/b/d;->b(Ljava/lang/String;)V

    .line 61
    iget-object p1, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b/d;->h(I)V

    .line 62
    iget-object p1, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    sget-object p2, Lcom/baidu/mobads/AdSize;->FeedH5TemplateNative:Lcom/baidu/mobads/AdSize;

    invoke-virtual {p2}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b/d;->f(I)V

    .line 64
    iget-object p1, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b/d;->g(I)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/b/d;->a(I)V

    return-void
.end method

.method public a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 2

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onImpression(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 2

    .line 80
    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getWidth()I

    move-result v0

    .line 81
    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getHeight()I

    move-result p1

    if-lez v0, :cond_0

    if-lez p1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/production/b/d;->d(I)V

    .line 84
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/b/d;->e(I)V

    :cond_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 115
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->start()V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 2

    .line 101
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/c;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string p1, "XAdMouldeLoader ad-server requesting success"

    .line 103
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    int-to-double v0, p3

    .line 106
    invoke-virtual {p2, p1, v0, v1}, Lcom/baidu/mobads/production/p;->a(Lcom/baidu/mobads/openad/b/b;D)V

    :goto_0
    return-void
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 1

    .line 138
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getAdInstanceList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 140
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getHtmlSnippet()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 141
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getHtmlSnippet()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->A:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->getAdPlacement()Lcom/baidu/mobads/BaiduNativeAdPlacement;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->setAdResponse(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    .line 146
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string v0, "AdLoadData"

    invoke-direct {p1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b/c;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_1

    .line 142
    :cond_1
    :goto_0
    sget-object p1, Lcom/baidu/mobads/interfaces/error/XAdErrorCode;->REQUEST_PARAM_ERROR:Lcom/baidu/mobads/interfaces/error/XAdErrorCode;

    const-string v0, "\u4ee3\u7801\u4f4d\u9519\u8bef\uff0c\u8bf7\u68c0\u67e5\u4ee3\u7801\u4f4d\u662f\u5426\u662f\u4fe1\u606f\u6d41\u6a21\u677f"

    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/production/b/c;->a(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public c()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    goto :goto_0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "container is null"

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public c(I)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/b/d;->b(I)V

    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method public d(I)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/b/d;->c(I)V

    return-void
.end method

.method public getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    return-object v0
.end method

.method public request()V
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/c;->m()V

    .line 91
    iget-object v0, p0, Lcom/baidu/mobads/production/b/c;->z:Lcom/baidu/mobads/production/b/d;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/b/c;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method
