.class public Lcom/baidu/mobads/production/b/d;
.super Lcom/baidu/mobads/vo/d;
.source "SourceFile"


# instance fields
.field private a:I

.field private l:I

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/vo/d;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    const/4 p1, 0x1

    .line 19
    iput p1, p0, Lcom/baidu/mobads/production/b/d;->a:I

    .line 20
    iput p1, p0, Lcom/baidu/mobads/production/b/d;->l:I

    .line 21
    iput p1, p0, Lcom/baidu/mobads/production/b/d;->m:I

    .line 25
    iget-object p1, p0, Lcom/baidu/mobads/production/b/d;->i:Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;

    const-string p2, "http://mobads.baidu.com/cpro/ui/mads.php"

    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdURIUitls;->replaceURLWithSupportProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/b/d;->b:Ljava/lang/String;

    const-string p1, "androidfeed"

    .line 26
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b/d;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected a()Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 32
    iget-boolean v1, p0, Lcom/baidu/mobads/production/b/d;->j:Z

    if-eqz v1, :cond_0

    const-string v1, "fet"

    const-string v2, "ANTI,MSSP,NMON,HTML,VIDEO"

    .line 33
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v1, "fet"

    const-string v2, "ANTI,MSSP,NMON,HTML,CLICK2VIDEO,PAUSE,VIDEO"

    .line 35
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const-string v1, "pos"

    .line 37
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/baidu/mobads/production/b/d;->l:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "seq"

    .line 38
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/baidu/mobads/production/b/d;->m:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "viewid"

    .line 39
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/baidu/mobads/production/b/d;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .line 44
    iput p1, p0, Lcom/baidu/mobads/production/b/d;->a:I

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 57
    invoke-super {p0}, Lcom/baidu/mobads/vo/d;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .line 48
    iput p1, p0, Lcom/baidu/mobads/production/b/d;->l:I

    return-void
.end method

.method public c(I)V
    .locals 0

    .line 52
    iput p1, p0, Lcom/baidu/mobads/production/b/d;->m:I

    return-void
.end method
