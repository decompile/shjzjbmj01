.class public Lcom/baidu/mobads/production/b/e;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"


# instance fields
.field private A:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private B:Z

.field private C:I

.field private z:Lcom/baidu/mobads/production/b/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 40
    iput-boolean p1, p0, Lcom/baidu/mobads/production/b/e;->B:Z

    const/16 p1, 0x1f40

    .line 45
    iput p1, p0, Lcom/baidu/mobads/production/b/e;->C:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 53
    invoke-direct {p0, p1, p2, v0}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x1

    .line 61
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;ZI)V
    .locals 2

    .line 70
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 40
    iput-boolean v0, p0, Lcom/baidu/mobads/production/b/e;->B:Z

    const/16 v1, 0x1f40

    .line 45
    iput v1, p0, Lcom/baidu/mobads/production/b/e;->C:I

    .line 71
    iput-boolean p4, p0, Lcom/baidu/mobads/production/b/e;->B:Z

    .line 72
    iput p5, p0, Lcom/baidu/mobads/production/b/e;->C:I

    .line 73
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/b/e;->setId(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/b/e;->setActivity(Landroid/content/Context;)V

    const/4 p4, 0x0

    .line 75
    invoke-virtual {p0, p4}, Lcom/baidu/mobads/production/b/e;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 76
    iput-object p3, p0, Lcom/baidu/mobads/production/b/e;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 77
    new-instance p3, Lcom/baidu/mobads/production/b/f;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/e;->getApplicationContext()Landroid/content/Context;

    move-result-object p4

    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/e;->getActivity()Landroid/app/Activity;

    move-result-object p5

    iget-object v1, p0, Lcom/baidu/mobads/production/b/e;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p3, p4, p5, v1}, Lcom/baidu/mobads/production/b/f;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p3, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    .line 81
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p3

    invoke-virtual {p3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object p3

    .line 83
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    .line 84
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingNone()Ljava/lang/String;

    move-result-object p5

    invoke-interface {p4, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingLandingPage()Ljava/lang/String;

    move-result-object p5

    invoke-interface {p4, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->a()Ljava/lang/String;

    move-result-object p5

    invoke-interface {p4, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p5

    invoke-virtual {p5}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    invoke-static {p1}, Lcom/baidu/mobads/utils/r;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 88
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingDownload()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/baidu/mobads/utils/h;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    .line 92
    iget-object p3, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    invoke-virtual {p3, p1}, Lcom/baidu/mobads/production/b/f;->b(Ljava/lang/String;)V

    .line 93
    iget-object p1, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    const/16 p3, 0x258

    invoke-virtual {p1, p3}, Lcom/baidu/mobads/production/b/f;->d(I)V

    .line 94
    iget-object p1, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    const/16 p3, 0x1f4

    invoke-virtual {p1, p3}, Lcom/baidu/mobads/production/b/f;->e(I)V

    .line 95
    iget-object p1, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Lcom/baidu/mobads/production/b/f;->h(I)V

    .line 96
    iget-object p1, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b/f;->d(Ljava/lang/String;)V

    .line 97
    iget-object p1, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    sget-object p2, Lcom/baidu/mobads/AdSize;->FeedNative:Lcom/baidu/mobads/AdSize;

    invoke-virtual {p2}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b/f;->f(I)V

    .line 98
    iget-object p1, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/b/f;->g(I)V

    .line 99
    iget-object p1, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object p2

    .line 100
    invoke-virtual {p2}, Lcom/baidu/mobads/utils/l;->getAdCreativeTypeImage()I

    move-result p2

    .line 99
    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/b/f;->i(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    const/16 v0, 0x1f40

    .line 57
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;ZI)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZI)V
    .locals 6

    .line 65
    sget-object v3, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;ZI)V

    return-void
.end method

.method private a(ILjava/util/List;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 264
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 265
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/l;->feedsTrackerParameterKeyProgress()Ljava/lang/String;

    move-result-object v1

    .line 266
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 265
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/l;->feedsTrackerParameterKeyList()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ">;"
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->A:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Landroid/content/Context;IILcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    .line 235
    :try_start_0
    invoke-interface {p3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCloseTrackers()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/baidu/mobads/production/b/e;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object p2

    .line 236
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v0, p1, p3, p4, p2}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onClose(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 7

    .line 197
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/e;->b()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->setClickView(Landroid/view/View;)V

    .line 198
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getThirdClickTrackingUrls()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p3, v0}, Lcom/baidu/mobads/production/b/e;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v6

    .line 199
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-object v1, v0

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v1 .. v6}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onClick(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 2

    const/4 v0, -0x1

    .line 157
    :try_start_0
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getThirdImpressionTrackingUrls()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/b/e;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v1, p1, p2, p3, v0}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onImpression(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 3

    .line 104
    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getWidth()I

    move-result v0

    .line 105
    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getHeight()I

    move-result v1

    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 107
    iget-object v2, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    invoke-virtual {v2, v0}, Lcom/baidu/mobads/production/b/f;->d(I)V

    .line 108
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/b/f;->e(I)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getExtras()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/b/f;->a(Ljava/util/Map;)V

    .line 112
    invoke-super {p0, p1}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 132
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->start()V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 2

    int-to-double v0, p3

    .line 127
    invoke-virtual {p2, p1, v0, v1}, Lcom/baidu/mobads/production/p;->a(Lcom/baidu/mobads/openad/b/b;D)V

    return-void
.end method

.method public a(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z
    .locals 2

    .line 166
    :try_start_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 167
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getHtmlSnippet()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "_&_"

    .line 168
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 169
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getQueryKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "_&_"

    .line 170
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 171
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "_&_"

    .line 172
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 173
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "_&_"

    .line 174
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 175
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "_&_"

    .line 176
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/g/q;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :catch_0
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v0, p1, p2, p3}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->isAdAvailable(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return p1

    :catch_1
    const/4 p1, 0x0

    return p1
.end method

.method public b()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    return-object v0
.end method

.method public b(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 7

    .line 254
    :try_start_0
    invoke-interface {p3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getFullScreenTrackers()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/baidu/mobads/production/b/e;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v6

    .line 255
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-object v1, v0

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v1 .. v6}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onFullScreen(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public b(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 2

    const/4 v0, 0x0

    .line 207
    :try_start_0
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getStartTrackers()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/b/e;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v1, p1, p2, p3, v0}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onStart(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public b(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 1

    const/4 v0, -0x1

    .line 190
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/baidu/mobads/production/b/e;->a(Landroid/view/View;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;ILcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V

    return-void
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 137
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getAdInstanceList()Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/b/e;->A:Ljava/util/ArrayList;

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    return-void
.end method

.method public c(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 2

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onComplete(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method protected d()V
    .locals 1

    .line 117
    iget v0, p0, Lcom/baidu/mobads/production/b/e;->C:I

    iput v0, p0, Lcom/baidu/mobads/production/a;->m:I

    return-void
.end method

.method public d(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;)V
    .locals 2

    const/4 v0, 0x0

    .line 244
    :try_start_0
    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCstartcardTrackers()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/b/e;->a(ILjava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 245
    iget-object v1, p0, Lcom/baidu/mobads/production/b/e;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v1, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;

    invoke-interface {v1, p1, p2, p3, v0}, Lcom/baidu/mobads/interfaces/feeds/IXAdDummyContainer;->onCstartcard(Landroid/content/Context;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/baidu/mobads/production/b/e;->b()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public q()Z
    .locals 1

    .line 283
    iget-boolean v0, p0, Lcom/baidu/mobads/production/b/e;->B:Z

    return v0
.end method

.method public request()V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/baidu/mobads/production/b/e;->z:Lcom/baidu/mobads/production/b/f;

    invoke-super {p0, v0}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method
