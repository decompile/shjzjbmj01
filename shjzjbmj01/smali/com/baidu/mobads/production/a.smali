.class public abstract Lcom/baidu/mobads/production/a;
.super Lcom/baidu/mobads/openad/a/c;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;


# static fields
.field public static b:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

.field private static final z:[Ljava/lang/String;


# instance fields
.field private A:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

.field private C:Ljava/lang/String;

.field private D:Landroid/os/Handler;

.field private E:Ljava/lang/Runnable;

.field private F:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

.field protected c:Ljava/lang/Boolean;

.field public d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

.field protected e:Landroid/widget/RelativeLayout;

.field protected f:Landroid/content/Context;

.field protected g:I

.field public h:Lcom/baidu/mobads/interfaces/IXAdContainer;

.field protected i:Ljava/lang/String;

.field protected j:Lcom/baidu/mobads/production/p;

.field protected k:Lcom/baidu/mobads/vo/d;

.field protected l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

.field protected m:I

.field protected n:I

.field protected o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

.field protected p:Z

.field protected q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected r:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected s:Ljava/lang/String;

.field protected t:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

.field protected u:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field protected w:J

.field protected x:J

.field protected y:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    .line 76
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/baidu/mobads/production/a;->z:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 268
    invoke-direct {p0}, Lcom/baidu/mobads/openad/a/c;-><init>()V

    const/4 p1, 0x0

    .line 88
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->c:Ljava/lang/Boolean;

    .line 90
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    .line 92
    iput-object v0, p0, Lcom/baidu/mobads/production/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 98
    iput p1, p0, Lcom/baidu/mobads/production/a;->g:I

    .line 109
    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->IDEL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    iput-object v1, p0, Lcom/baidu/mobads/production/a;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    const/16 v1, 0x1388

    .line 112
    iput v1, p0, Lcom/baidu/mobads/production/a;->m:I

    .line 114
    iput p1, p0, Lcom/baidu/mobads/production/a;->n:I

    .line 119
    iput-boolean p1, p0, Lcom/baidu/mobads/production/a;->p:Z

    .line 123
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/production/a;->q:Ljava/util/HashMap;

    .line 125
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/production/a;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 129
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/a;->D:Landroid/os/Handler;

    .line 132
    iput-object v0, p0, Lcom/baidu/mobads/production/a;->E:Ljava/lang/Runnable;

    const-string p1, ""

    .line 133
    iput-object p1, p0, Lcom/baidu/mobads/production/a;->s:Ljava/lang/String;

    .line 134
    iput-object v0, p0, Lcom/baidu/mobads/production/a;->t:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    .line 137
    iput-object v0, p0, Lcom/baidu/mobads/production/a;->u:Ljava/util/HashMap;

    .line 211
    new-instance p1, Lcom/baidu/mobads/production/b;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/production/b;-><init>(Lcom/baidu/mobads/production/a;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/a;->F:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    .line 261
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    return-void
.end method

.method private a()V
    .locals 4

    const-string v0, "XAdMouldeLoader ad-server requesting success"

    .line 145
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sput-wide v1, Lcom/baidu/mobads/constants/a;->n:J

    .line 146
    iget-object v1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/a;->c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    .line 147
    iget-object v1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getAdInstanceList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/baidu/mobads/production/a;->a(Ljava/util/ArrayList;)V

    .line 148
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->h()Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->a(Ljava/lang/String;)V

    return-void

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v1

    .line 155
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;

    move-result-object v2

    .line 156
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 158
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->a(Ljava/lang/String;)V

    return-void

    .line 162
    :cond_1
    invoke-virtual {p0, v2, v1}, Lcom/baidu/mobads/production/a;->a(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v2

    .line 163
    invoke-virtual {p0, v2, v1}, Lcom/baidu/mobads/production/a;->a(ZLcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    if-eqz v2, :cond_2

    const-string v0, "download the splash picture successfully"

    .line 166
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->a(Ljava/lang/String;)V

    return-void

    .line 176
    :cond_2
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/a;->e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 178
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->a(Ljava/lang/String;)V

    .line 180
    :cond_3
    invoke-direct {p0, v1}, Lcom/baidu/mobads/production/a;->h(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method private a(Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 2

    .line 708
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "caching_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 710
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "local_creative_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 711
    invoke-interface {p2, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V

    .line 712
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/a;->e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "download the splash picture successfully"

    .line 713
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->a(Ljava/lang/String;)V

    .line 715
    :cond_0
    invoke-direct {p0, p2}, Lcom/baidu/mobads/production/a;->g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 716
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "vdieoCacheSucc"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    .line 719
    :cond_1
    invoke-direct {p0, p2}, Lcom/baidu/mobads/production/a;->g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 720
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "vdieoCacheFailed"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_2
    const/4 v0, 0x0

    .line 722
    invoke-interface {p2, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V

    .line 723
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/a;->e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "download the splash picture successfully"

    .line 724
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->a(Ljava/lang/String;)V

    .line 727
    :cond_3
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/production/a;->b(Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method private a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 677
    :try_start_0
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;->f(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 680
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    .line 681
    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdCreativeCacheManager()Lcom/baidu/mobads/utils/m;

    move-result-object v1

    .line 682
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v6, Lcom/baidu/mobads/production/f;

    .line 683
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v6, p0, v0, p1}, Lcom/baidu/mobads/production/f;-><init>(Lcom/baidu/mobads/production/a;Landroid/os/Looper;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 682
    invoke-virtual/range {v1 .. v6}, Lcom/baidu/mobads/utils/m;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/a;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lcom/baidu/mobads/production/a;->b()V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/a;Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    .line 70
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/production/a;->a(Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/a;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 70
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 578
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 579
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 580
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/a;->g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581
    invoke-direct {p0, v0}, Lcom/baidu/mobads/production/a;->h(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lcom/baidu/mobads/c/a;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;Ljava/lang/String;)Z
    .locals 1

    .line 659
    :try_start_0
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p2, v0, :cond_0

    iget-object p2, p0, Lcom/baidu/mobads/production/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    if-eqz p2, :cond_0

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 660
    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/baidu/mobads/production/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 661
    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 663
    invoke-virtual {p1, p3}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    .line 668
    iget-object p2, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string p3, "XAbstractAdProdTemplate"

    invoke-interface {p2, p3, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)Z
    .locals 1

    .line 536
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object p2

    .line 537
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object p2

    .line 536
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_1

    .line 539
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;->g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result p2

    if-nez p2, :cond_1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method private b(Lcom/baidu/mobads/interfaces/IXAdContainerContext;)Lcom/baidu/mobads/interfaces/IXAdContainer;
    .locals 5

    .line 981
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "createAdContainer"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    sget-object v0, Lcom/baidu/mobads/production/a;->b:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 986
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->u:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/a;->u:Ljava/util/HashMap;

    const-string v2, "Display_Down_Info"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 987
    sget-object v0, Lcom/baidu/mobads/production/a;->b:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    iget-object v1, p0, Lcom/baidu/mobads/production/a;->u:Ljava/util/HashMap;

    invoke-interface {v0, p1, v1}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->createXAdContainer(Lcom/baidu/mobads/interfaces/IXAdContainerContext;Ljava/util/HashMap;)Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object p1

    goto :goto_0

    .line 989
    :cond_0
    sget-object v0, Lcom/baidu/mobads/production/a;->b:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    invoke-interface {v0, p1, v1}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->createXAdContainer(Lcom/baidu/mobads/interfaces/IXAdContainerContext;Ljava/util/HashMap;)Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object p1

    :goto_0
    move-object v1, p1

    if-eqz v1, :cond_1

    .line 993
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "XAbstractAdProdTemplate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createAdContainer() apk.version="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/baidu/mobads/production/a;->b:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdContainerFactory;->getRemoteVersion()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v1
.end method

.method private b()V
    .locals 1

    const/4 v0, 0x1

    .line 426
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->isRemoteLoadSuccess:Ljava/lang/Boolean;

    const-string v0, "XAdMouldeLoader load success"

    .line 427
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Landroid/os/Message;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 10

    .line 734
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/a;->c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 737
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "caching_time_consume"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 738
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;

    move-result-object v1

    .line 739
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "caching_result"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "success"

    goto :goto_0

    :cond_1
    const-string p1, "failed"

    .line 741
    :goto_0
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/a;->f:Landroid/content/Context;

    const-string v4, "383"

    iget-object v5, p0, Lcom/baidu/mobads/production/a;->k:Lcom/baidu/mobads/vo/d;

    .line 743
    invoke-virtual {v5}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v6

    const/4 v5, 0x3

    new-array v7, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file_dl_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v7, v5

    const/4 p1, 0x1

    aput-object v1, v7, p1

    const/4 p1, 0x2

    aput-object v0, v7, p1

    move-object v5, p2

    .line 741
    invoke-virtual/range {v2 .. v7}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V

    return-void
.end method

.method private b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)V
    .locals 7

    .line 588
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/a;->f:Landroid/content/Context;

    const-string v3, "383"

    iget-object v0, p0, Lcom/baidu/mobads/production/a;->k:Lcom/baidu/mobads/vo/d;

    .line 592
    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v5

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v4, "file_dl_failed_not_wifi"

    aput-object v4, v6, v0

    const/4 v0, 0x1

    aput-object p2, v6, v0

    move-object v4, p1

    .line 590
    invoke-virtual/range {v1 .. v6}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdProdInfo;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private f(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 2

    if-eqz p1, :cond_0

    .line 554
    :try_start_0
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, v0, :cond_0

    .line 555
    iget-boolean p1, p0, Lcom/baidu/mobads/production/a;->p:Z

    if-eqz p1, :cond_0

    .line 556
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object p1

    .line 557
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object p1

    .line 556
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 561
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method private g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 2

    if-eqz p1, :cond_0

    .line 568
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FEEDS:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 569
    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {v1}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private h(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 9

    .line 615
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "cacheCreativeAsset"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;

    move-result-object v6

    .line 618
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 621
    :cond_0
    invoke-direct {p0, p1, v6}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 622
    invoke-direct {p0, p1, v6}, Lcom/baidu/mobads/production/a;->b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    .line 625
    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V

    .line 626
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/baidu/mobads/utils/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 627
    invoke-static {v6}, Lcom/baidu/mobads/utils/m;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 629
    invoke-static {}, Lcom/baidu/mobads/c/a;->a()Lcom/baidu/mobads/c/a;

    move-result-object v0

    .line 630
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_2

    .line 632
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v2

    invoke-direct {p0, v0, v2, v1}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/c/a;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 634
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/baidu/mobads/production/e;

    move-object v3, v2

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v3 .. v8}, Lcom/baidu/mobads/production/e;-><init>(Lcom/baidu/mobads/production/a;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Ljava/util/Observer;)V

    goto :goto_0

    .line 650
    :cond_2
    invoke-direct {p0, p1, v6, v7, v8}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public a(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 508
    :cond_0
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v1

    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v1, v2, :cond_1

    .line 509
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 510
    :cond_1
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v1

    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->RM:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne v1, v2, :cond_2

    .line 511
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getMainPictureUrl()Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_0
    return-object v0
.end method

.method protected a(Landroid/content/Context;)V
    .locals 2

    .line 374
    sget-object v0, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/g/g;

    if-nez v0, :cond_1

    .line 375
    const-class v0, Lcom/baidu/mobads/g/g;

    monitor-enter v0

    .line 376
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/g/g;

    if-nez v1, :cond_0

    .line 377
    new-instance v1, Lcom/baidu/mobads/g/g;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/baidu/mobads/g/g;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/g/g;

    .line 379
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 386
    :cond_1
    :goto_0
    sget-object p1, Lcom/baidu/mobads/production/a;->b:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    if-eqz p1, :cond_2

    .line 387
    invoke-direct {p0}, Lcom/baidu/mobads/production/a;->b()V

    goto :goto_1

    .line 391
    :cond_2
    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/g/g;

    if-eqz p1, :cond_3

    .line 392
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "XAbstractAdProdTemplate"

    const-string v1, "BaiduXAdSDKContext.mApkLoader != null,load apk"

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/g/g;

    new-instance v0, Lcom/baidu/mobads/production/d;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/production/d;-><init>(Lcom/baidu/mobads/production/a;)V

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/g/g;->a(Lcom/baidu/mobads/g/g$c;)V

    goto :goto_1

    .line 419
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "XAbstractAdProdTemplate"

    const-string v1, "BaiduXAdSDKContext.mApkLoader == null,not load apk"

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void
.end method

.method public a(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 0

    .line 1192
    iput-object p1, p0, Lcom/baidu/mobads/production/a;->t:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    return-void
.end method

.method protected abstract a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V
    .locals 4

    .line 928
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "processAllReadyOnUIThread()"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/production/a;->x:J

    .line 930
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;->b(Lcom/baidu/mobads/interfaces/IXAdContainerContext;)Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    .line 932
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/production/a;->y:J

    .line 934
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-nez p1, :cond_0

    .line 935
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "XAbstractAdProdTemplate"

    const-string v1, "processAllReadyOnUIThread(), mAdContainer is null"

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string v0, "AdError"

    invoke-direct {p1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto/16 :goto_0

    .line 938
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "XAbstractAdProdTemplate"

    const-string v1, "processAllReadyOnUIThread(), mAdContainer be created"

    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 940
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->q:Ljava/util/HashMap;

    const-string v0, "start"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/baidu/mobads/production/a;->w:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 941
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->q:Ljava/util/HashMap;

    const-string v0, "container_before_created"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/baidu/mobads/production/a;->x:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 942
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->q:Ljava/util/HashMap;

    const-string v0, "container_after_created"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/baidu/mobads/production/a;->y:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 943
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    iget-object v0, p0, Lcom/baidu/mobads/production/a;->q:Ljava/util/HashMap;

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->setParameters(Ljava/util/HashMap;)V

    .line 945
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getRemoteVersion()Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/baidu/mobads/constants/a;->c:Ljava/lang/String;

    .line 946
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "XAbstractAdProdTemplate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processAllReadyOnUIThread(), mAdContainer be created, hasCalledLoadAtAppSide="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/production/a;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 948
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 946
    invoke-interface {p1, v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 952
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    .line 955
    :cond_1
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {p1}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobads/production/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 956
    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 957
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string v0, "AdLoaded"

    invoke-direct {p1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 959
    :cond_2
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 964
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v1

    sget-object v2, Lcom/baidu/mobads/interfaces/error/XAdErrorCode;->PERMISSION_PROBLEM:Lcom/baidu/mobads/interfaces/error/XAdErrorCode;

    .line 965
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v2, p1}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->genCompleteErrorMessage(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 964
    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    .line 967
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string v0, "AdError"

    invoke-direct {p1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :goto_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 9

    .line 858
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "handleAllReady"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    iget v0, p0, Lcom/baidu/mobads/production/a;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/baidu/mobads/production/a;->g:I

    .line 861
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 863
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 864
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 865
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->k:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v4

    .line 866
    new-instance v6, Lcom/baidu/mobads/production/l;

    invoke-direct {v6, v2, p0}, Lcom/baidu/mobads/production/l;-><init>(Landroid/content/Context;Lcom/baidu/mobads/production/a;)V

    .line 867
    new-instance v0, Lcom/baidu/mobads/production/k;

    iget-object v5, p0, Lcom/baidu/mobads/production/a;->e:Landroid/widget/RelativeLayout;

    const/4 v8, 0x0

    move-object v1, v0

    move-object v7, p1

    invoke-direct/range {v1 .. v8}, Lcom/baidu/mobads/production/k;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Landroid/widget/RelativeLayout;Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;Lcom/baidu/mobads/interfaces/IXAdResponseInfo;Landroid/view/View;)V

    .line 868
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 869
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    goto :goto_0

    .line 871
    :cond_0
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/g;

    invoke-direct {v1, p0, v0}, Lcom/baidu/mobads/production/g;-><init>(Lcom/baidu/mobads/production/a;Lcom/baidu/mobads/interfaces/IXAdContainerContext;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    .line 888
    iput-object p2, p0, Lcom/baidu/mobads/production/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)V
    .locals 3

    .line 355
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "msg"

    .line 356
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    new-instance v1, Lcom/baidu/mobads/e/a;

    const-string v2, "AdError"

    invoke-direct {v1, v2, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 358
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)V

    return-void
.end method

.method protected abstract a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
.end method

.method protected a(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;Ljava/lang/String;)V
    .locals 2

    .line 228
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    const-string p2, "message"

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 229
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->l()V

    .line 231
    :try_start_0
    new-instance p2, Lcom/baidu/mobads/vo/c;

    invoke-direct {p2, p1}, Lcom/baidu/mobads/vo/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/a;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    .line 232
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getAdInstanceList()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 233
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    .line 234
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object p1

    const-string p2, "mimetype"

    .line 235
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/a;->s:Ljava/lang/String;

    .line 236
    invoke-direct {p0}, Lcom/baidu/mobads/production/a;->a()V

    .line 237
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->e()V

    goto :goto_2

    .line 239
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getErrorCode()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string p1, ""

    .line 240
    :goto_0
    iget-object p2, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getErrorMessage()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_2
    const-string p2, ""

    :goto_1
    const-string v0, "0"

    .line 241
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string p2, "\u65e0\u5e7f\u544a\u8fd4\u56de"

    .line 245
    :cond_3
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    .line 246
    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    const-string v1, ""

    .line 247
    invoke-interface {v0, p1, p2, v1}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "response ad list empty: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2, p1}, Lcom/baidu/mobads/production/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    const-string p1, "response json parsing error"

    .line 252
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object p2

    const-string v0, ""

    const-string v1, ""

    invoke-interface {p2, v0, p1, v1}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->c(Ljava/lang/String;)V

    :goto_2
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    .line 431
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->c:Ljava/lang/Boolean;

    .line 432
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 433
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 845
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    const-string v1, "error_message"

    .line 846
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "error_code"

    .line 847
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 848
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdError"

    invoke-direct {p1, p2, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    .line 849
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 140
    iput-object p1, p0, Lcom/baidu/mobads/production/a;->u:Ljava/util/HashMap;

    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 1149
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1150
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->onWindowFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public a(ZLcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1155
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0, p1, p2}, Lcom/baidu/mobads/interfaces/IXAdContainer;->processKeyEvent(ILandroid/view/KeyEvent;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method protected a(Lcom/baidu/mobads/vo/d;)Z
    .locals 3

    .line 757
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "doRequest()"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->f:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->a(Landroid/content/Context;)V

    .line 778
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->b(Lcom/baidu/mobads/vo/d;)V

    const/4 p1, 0x1

    return p1
.end method

.method a(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 2

    .line 193
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 197
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/baidu/mobads/utils/m;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 198
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdCreativeCacheManager()Lcom/baidu/mobads/utils/m;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/m;->c(Ljava/lang/String;)V

    .line 202
    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setLocalCreativeURL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    .line 206
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    :cond_1
    return v1
.end method

.method public b(I)V
    .locals 1

    .line 1143
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1144
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0, p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->onWindowVisibilityChanged(I)V

    :cond_0
    return-void
.end method

.method protected abstract b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
.end method

.method protected b(Lcom/baidu/mobads/vo/d;)V
    .locals 3

    .line 784
    iput-object p1, p0, Lcom/baidu/mobads/production/a;->k:Lcom/baidu/mobads/vo/d;

    .line 785
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->k()V

    const/4 v0, 0x0

    .line 787
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->c:Ljava/lang/Boolean;

    .line 789
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/baidu/mobads/vo/d;->b()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->i:Ljava/lang/String;

    .line 791
    :goto_0
    new-instance v0, Lcom/baidu/mobads/production/p;

    invoke-direct {v0}, Lcom/baidu/mobads/production/p;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->j:Lcom/baidu/mobads/production/p;

    .line 793
    sput-object p1, Lcom/baidu/mobads/b/a;->b:Ljava/lang/String;

    .line 795
    new-instance v0, Lcom/baidu/mobads/openad/b/b;

    const-string v1, ""

    invoke-direct {v0, p1, v1}, Lcom/baidu/mobads/openad/b/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 796
    iput p1, v0, Lcom/baidu/mobads/openad/b/b;->e:I

    .line 797
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->j:Lcom/baidu/mobads/production/p;

    const-string v1, "URLLoader.Load.Complete"

    iget-object v2, p0, Lcom/baidu/mobads/production/a;->F:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v1, v2}, Lcom/baidu/mobads/production/p;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 798
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->j:Lcom/baidu/mobads/production/p;

    const-string v1, "URLLoader.Load.Error"

    iget-object v2, p0, Lcom/baidu/mobads/production/a;->F:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v1, v2}, Lcom/baidu/mobads/production/p;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 799
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->j:Lcom/baidu/mobads/production/p;

    iget v1, p0, Lcom/baidu/mobads/production/a;->m:I

    invoke-virtual {p0, v0, p1, v1}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V

    return-void
.end method

.method protected declared-synchronized b(Ljava/lang/String;)V
    .locals 4

    monitor-enter p0

    .line 437
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doubleCheck:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", bfp="

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/baidu/mobads/production/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", apk="

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->isRemoteLoadSuccess:Ljava/lang/Boolean;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz p1, :cond_0

    .line 440
    monitor-exit p0

    return-void

    .line 442
    :cond_0
    :try_start_1
    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->isRemoteLoadSuccess:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 444
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getAdContainerFactory()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeAdContainerFactory(Lcom/baidu/mobads/interfaces/IXAdContainerFactory;)V

    .line 453
    :cond_1
    sget-object p1, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->isRemoteLoadSuccess:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/baidu/mobads/production/a;->c:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    .line 455
    :try_start_2
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 457
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    goto :goto_0

    .line 460
    :cond_2
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "AdError"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 461
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "doubleCheck IXAdResponseInfo is null, but isBFP4APPRequestSuccess is true"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 468
    :goto_0
    :try_start_3
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    :goto_1
    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 464
    :try_start_4
    iget-object v1, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAbstractAdProdTemplate"

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 466
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "AdError"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 468
    :try_start_5
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto :goto_1

    :goto_2
    iget-object v1, p0, Lcom/baidu/mobads/production/a;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 471
    :cond_3
    :goto_3
    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    .line 436
    monitor-exit p0

    throw p1
.end method

.method public b(Z)V
    .locals 0

    .line 1212
    iput-boolean p1, p0, Lcom/baidu/mobads/production/a;->p:Z

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 2

    .line 517
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->STATIC_IMAGE:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-eq v0, v1, :cond_1

    .line 518
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->GIF:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public abstract c()V
.end method

.method protected c(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 808
    invoke-virtual {p0, p1, p2}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    .line 810
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_SPLASH:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {p1}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/baidu/mobads/production/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 811
    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 812
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdLoaded"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :cond_0
    return-void
.end method

.method protected c(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    .line 833
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V

    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 2

    .line 837
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    const-string v1, "error_message"

    .line 838
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 839
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string v1, "AdError"

    invoke-direct {p1, v1, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    .line 840
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public c(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 1

    .line 523
    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreativeType()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    move-result-object p1

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->VIDEO:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected abstract d()V
.end method

.method protected d(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 818
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->PLAYING:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    .line 820
    invoke-virtual {p0, p1, p2}, Lcom/baidu/mobads/production/a;->b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V

    .line 821
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdStarted"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public d(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 529
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;->g(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;->h(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    :cond_0
    return-void
.end method

.method protected d(Ljava/lang/String;)V
    .locals 1

    .line 1167
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1168
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "\u4ee3\u7801\u4f4did(adPlaceId)\u4e0d\u53ef\u4ee5\u4e3a\u7a7a"

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public e(Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method protected g()V
    .locals 0

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .line 902
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->f:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 903
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->f:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    .line 904
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->e:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/production/a;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 905
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getAdContainerFactory()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;
    .locals 1

    .line 915
    sget-object v0, Lcom/baidu/mobads/production/a;->b:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    return-object v0
.end method

.method public getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;
    .locals 1

    .line 1180
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 896
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 897
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->f:Landroid/content/Context;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getCurrentAdInstance()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;
    .locals 1

    .line 1028
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->d:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    return-object v0
.end method

.method public getCurrentXAdContainer()Lcom/baidu/mobads/interfaces/IXAdContainer;
    .locals 1

    .line 911
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 346
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->C:Ljava/lang/String;

    return-object v0
.end method

.method public getParameter()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1016
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->q:Ljava/util/HashMap;

    return-object v0
.end method

.method public getPlayheadTime()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getProdBase()Landroid/view/ViewGroup;
    .locals 1

    .line 1032
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->e:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;
    .locals 1

    .line 1004
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->k:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v0

    return-object v0
.end method

.method public getRequestParameters()Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;
    .locals 1

    .line 1197
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->t:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    if-nez v0, :cond_0

    .line 1198
    new-instance v0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    invoke-direct {v0}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->build()Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->t:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    .line 1200
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->t:Lcom/baidu/mobads/interfaces/feeds/IXAdFeedsRequestParameters;

    return-object v0
.end method

.method public getSlotState()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    return-object v0
.end method

.method public getType()Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;
    .locals 1

    .line 351
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    return-object v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected i()V
    .locals 2

    .line 1059
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1061
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->PAUSED:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    .line 1063
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/i;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/i;-><init>(Lcom/baidu/mobads/production/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public isAdServerRequestingSuccess()Ljava/lang/Boolean;
    .locals 1

    .line 1188
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->c:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected j()V
    .locals 2

    .line 1085
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1087
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;->PLAYING:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    iput-object v0, p0, Lcom/baidu/mobads/production/a;->l:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotState;

    .line 1089
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/j;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/j;-><init>(Lcom/baidu/mobads/production/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method protected k()V
    .locals 1

    .line 1110
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->j:Lcom/baidu/mobads/production/p;

    if-eqz v0, :cond_0

    .line 1111
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->j:Lcom/baidu/mobads/production/p;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/p;->removeAllListeners()V

    .line 1112
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->j:Lcom/baidu/mobads/production/p;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/p;->a()V

    :cond_0
    return-void
.end method

.method protected l()V
    .locals 2

    .line 1117
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->E:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1118
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/baidu/mobads/production/a;->E:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    const/4 v0, 0x0

    .line 1120
    iput-object v0, p0, Lcom/baidu/mobads/production/a;->E:Ljava/lang/Runnable;

    return-void
.end method

.method public load()V
    .locals 2

    .line 1036
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1037
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    goto :goto_0

    .line 1039
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    return-void
.end method

.method protected m()V
    .locals 4

    .line 1124
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->E:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1125
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/baidu/mobads/production/a;->E:Ljava/lang/Runnable;

    iget v2, p0, Lcom/baidu/mobads/production/a;->m:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public n()V
    .locals 1

    .line 1130
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1131
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->onAttachedToWindow()V

    :cond_0
    return-void
.end method

.method public o()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .line 1137
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1138
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->onDetachedFromWindow()V

    :cond_0
    return-void
.end method

.method public p()V
    .locals 1

    .line 1173
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->destroy()V

    .line 1176
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->exit()V

    return-void
.end method

.method public pause()V
    .locals 0

    .line 1055
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->i()V

    return-void
.end method

.method public q()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public r()Ljava/util/HashMap;
    .locals 5

    .line 1216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1218
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1219
    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1220
    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getOriginJsonObject()Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "custom_ext_data"

    .line 1221
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1222
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .line 1223
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1225
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1226
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 1227
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_0
    return-object v0
.end method

.method public resize()V
    .locals 2

    .line 1044
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1045
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/h;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/h;-><init>(Lcom/baidu/mobads/production/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public resume()V
    .locals 0

    .line 1081
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->j()V

    return-void
.end method

.method public setActivity(Landroid/content/Context;)V
    .locals 1

    .line 292
    iput-object p1, p0, Lcom/baidu/mobads/production/a;->f:Landroid/content/Context;

    .line 293
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->g()V

    .line 294
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 296
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->d()V

    .line 299
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object p1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;)V

    .line 300
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 302
    new-instance p1, Lcom/baidu/mobads/production/c;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/production/c;-><init>(Lcom/baidu/mobads/production/a;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/a;->E:Ljava/lang/Runnable;

    .line 317
    iget-object p1, p0, Lcom/baidu/mobads/production/a;->f:Landroid/content/Context;

    invoke-static {p1}, Lcom/baidu/mobads/g/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/g/q;->a()V

    return-void
.end method

.method public setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    .line 1184
    iput-object p1, p0, Lcom/baidu/mobads/production/a;->B:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    return-void
.end method

.method public setAdSlotBase(Landroid/widget/RelativeLayout;)V
    .locals 0

    .line 332
    iput-object p1, p0, Lcom/baidu/mobads/production/a;->e:Landroid/widget/RelativeLayout;

    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/baidu/mobads/production/a;->C:Ljava/lang/String;

    return-void
.end method

.method public setParameter(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1012
    iput-object p1, p0, Lcom/baidu/mobads/production/a;->q:Ljava/util/HashMap;

    return-void
.end method

.method public start()V
    .locals 1

    .line 1075
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1076
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->start()V

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .line 1101
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const-string v1, "XAbstractAdProdTemplate"

    const-string v2, "stop"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    if-eqz v0, :cond_0

    .line 1103
    iget-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->stop()V

    const/4 v0, 0x0

    .line 1104
    iput-object v0, p0, Lcom/baidu/mobads/production/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    :cond_0
    return-void
.end method
