.class Lcom/baidu/mobads/production/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/g/g$c;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/production/a;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/production/a;)V
    .locals 0

    .line 393
    iput-object p1, p0, Lcom/baidu/mobads/production/d;->a:Lcom/baidu/mobads/production/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 4

    if-eqz p1, :cond_0

    .line 397
    :try_start_0
    sget-object v0, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/g/g;

    if-eqz v0, :cond_0

    .line 398
    sget-object v0, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/g/g;

    invoke-virtual {v0}, Lcom/baidu/mobads/g/g;->h()Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/production/a;->b:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    .line 399
    sget-object v0, Lcom/baidu/mobads/production/a;->b:Lcom/baidu/mobads/interfaces/IXAdContainerFactory;

    if-eqz v0, :cond_0

    .line 400
    iget-object p1, p0, Lcom/baidu/mobads/production/d;->a:Lcom/baidu/mobads/production/a;

    invoke-static {p1}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/production/a;)V

    return-void

    :catch_0
    move-exception p1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 406
    sput-object v0, Lcom/baidu/mobads/production/BaiduXAdSDKContext;->mApkLoader:Lcom/baidu/mobads/g/g;

    .line 407
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "error_message"

    .line 408
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u56de\u8c03onLoad,succcess="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    iget-object p1, p0, Lcom/baidu/mobads/production/d;->a:Lcom/baidu/mobads/production/a;

    new-instance v1, Lcom/baidu/mobads/e/a;

    const-string v2, "AdError"

    invoke-direct {v1, v2, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p1, v1}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 411
    :goto_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "error_message"

    .line 412
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "async apk on load exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    iget-object v1, p0, Lcom/baidu/mobads/production/d;->a:Lcom/baidu/mobads/production/a;

    new-instance v2, Lcom/baidu/mobads/e/a;

    const-string v3, "AdError"

    invoke-direct {v2, v3, v0}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/production/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    .line 414
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    :goto_1
    return-void
.end method
