.class Lcom/baidu/mobads/production/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/baidu/mobads/production/a;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/production/a;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 634
    iput-object p1, p0, Lcom/baidu/mobads/production/e;->e:Lcom/baidu/mobads/production/a;

    iput-object p2, p0, Lcom/baidu/mobads/production/e;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iput-object p3, p0, Lcom/baidu/mobads/production/e;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/baidu/mobads/production/e;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/baidu/mobads/production/e;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3

    .line 637
    check-cast p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    .line 639
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->CANCELLED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq p2, v0, :cond_0

    .line 640
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq p2, v0, :cond_0

    .line 641
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->ERROR:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq p2, v0, :cond_0

    .line 642
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETE_BUT_FILE_REMOVED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-eq p2, v0, :cond_0

    .line 643
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p1

    sget-object p2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->PAUSED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne p1, p2, :cond_1

    .line 645
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/production/e;->e:Lcom/baidu/mobads/production/a;

    iget-object p2, p0, Lcom/baidu/mobads/production/e;->a:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    iget-object v0, p0, Lcom/baidu/mobads/production/e;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/baidu/mobads/production/e;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/production/e;->d:Ljava/lang/String;

    invoke-static {p1, p2, v0, v1, v2}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/production/a;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
