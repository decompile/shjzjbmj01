.class public Lcom/baidu/mobads/production/e/b;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/production/e/a;


# instance fields
.field protected final A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private B:Lcom/baidu/mobads/production/e/d;

.field private C:Lcom/baidu/mobads/AdSize;

.field private D:Z

.field private E:Z

.field private F:Landroid/app/Activity;

.field private G:Landroid/widget/RelativeLayout;

.field private H:Ljava/lang/Boolean;

.field public final z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 2

    .line 71
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    const-string v0, "html5_intersitial"

    .line 41
    iput-object v0, p0, Lcom/baidu/mobads/production/e/b;->z:Ljava/lang/String;

    const/4 v0, 0x0

    .line 46
    iput-boolean v0, p0, Lcom/baidu/mobads/production/e/b;->D:Z

    .line 47
    iput-boolean v0, p0, Lcom/baidu/mobads/production/e/b;->E:Z

    .line 59
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/e/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 72
    invoke-virtual {p0, p4}, Lcom/baidu/mobads/production/e/b;->setId(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/e/b;->setActivity(Landroid/content/Context;)V

    .line 75
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/e/b;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 77
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/e/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 79
    iput-object p3, p0, Lcom/baidu/mobads/production/e/b;->H:Ljava/lang/Boolean;

    .line 80
    sget-object p1, Lcom/baidu/mobads/AdSize;->InterstitialGame:Lcom/baidu/mobads/AdSize;

    iput-object p1, p0, Lcom/baidu/mobads/production/e/b;->C:Lcom/baidu/mobads/AdSize;

    .line 81
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    .line 82
    new-instance p1, Lcom/baidu/mobads/production/e/d;

    .line 83
    invoke-virtual {p0}, Lcom/baidu/mobads/production/e/b;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p0}, Lcom/baidu/mobads/production/e/b;->getActivity()Landroid/app/Activity;

    move-result-object p3

    iget-object v0, p0, Lcom/baidu/mobads/production/e/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p1, p2, p3, v0, v1}, Lcom/baidu/mobads/production/e/d;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Ljava/lang/Boolean;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/e/b;->B:Lcom/baidu/mobads/production/e/d;

    .line 84
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->B:Lcom/baidu/mobads/production/e/d;

    invoke-virtual {p1, p4}, Lcom/baidu/mobads/production/e/d;->d(Ljava/lang/String;)V

    .line 86
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->B:Lcom/baidu/mobads/production/e/d;

    sget-object p2, Lcom/baidu/mobads/AdSize;->InterstitialGame:Lcom/baidu/mobads/AdSize;

    invoke-virtual {p2}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/e/d;->f(I)V

    .line 87
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->B:Lcom/baidu/mobads/production/e/d;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/e/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/vo/b;

    .line 88
    invoke-virtual {p1}, Lcom/baidu/mobads/vo/b;->getAttribute()Lorg/json/JSONObject;

    move-result-object p2

    if-nez p2, :cond_0

    .line 90
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    :try_start_0
    const-string p3, "ABILITY"

    const-string v0, "PAUSE,"

    .line 93
    invoke-virtual {p2, p3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :catch_0
    invoke-virtual {p1, p2}, Lcom/baidu/mobads/vo/b;->a(Lorg/json/JSONObject;)V

    .line 98
    invoke-virtual {p0, p4}, Lcom/baidu/mobads/production/e/b;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/e/b;Landroid/content/Context;)Landroid/view/ViewGroup;
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/e/b;->b(Landroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/production/e/b;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/e/b;->e:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/production/e/b;Z)Z
    .locals 0

    .line 39
    iput-boolean p1, p0, Lcom/baidu/mobads/production/e/b;->E:Z

    return p1
.end method

.method private b(Landroid/content/Context;)Landroid/view/ViewGroup;
    .locals 0

    .line 190
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic b(Lcom/baidu/mobads/production/e/b;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/e/b;->e:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobads/production/e/b;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/baidu/mobads/production/e/b;->G:Landroid/widget/RelativeLayout;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 0

    .line 258
    invoke-virtual {p0}, Lcom/baidu/mobads/production/e/b;->load()V

    return-void
.end method

.method public a(II)V
    .locals 0

    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 5

    .line 146
    iget-boolean v0, p0, Lcom/baidu/mobads/production/e/b;->D:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobads/production/e/b;->E:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 147
    iput-boolean v0, p0, Lcom/baidu/mobads/production/e/b;->E:Z

    const/4 v1, 0x0

    .line 148
    iput-boolean v1, p0, Lcom/baidu/mobads/production/e/b;->D:Z

    .line 157
    iput-object p1, p0, Lcom/baidu/mobads/production/e/b;->F:Landroid/app/Activity;

    .line 158
    invoke-virtual {p0}, Lcom/baidu/mobads/production/e/b;->start()V

    .line 162
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 163
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 165
    iget-object v3, p0, Lcom/baidu/mobads/production/e/b;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 167
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 168
    iget v2, v2, Landroid/graphics/Rect;->top:I

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 173
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/e/b;->b(Landroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v2

    .line 175
    new-instance v4, Landroid/widget/RelativeLayout;

    invoke-direct {v4, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/baidu/mobads/production/e/b;->G:Landroid/widget/RelativeLayout;

    .line 176
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->G:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/baidu/mobads/production/e/b;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v4, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 177
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->G:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setFocusableInTouchMode(Z)V

    .line 180
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 181
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->requestFocus()Z

    return-void

    .line 150
    :cond_0
    iget-boolean p1, p0, Lcom/baidu/mobads/production/e/b;->E:Z

    if-eqz p1, :cond_1

    .line 151
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "interstitial ad is showing now"

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w(Ljava/lang/String;)I

    goto :goto_0

    .line 152
    :cond_1
    iget-boolean p1, p0, Lcom/baidu/mobads/production/e/b;->D:Z

    if-nez p1, :cond_2

    .line 153
    iget-object p1, p0, Lcom/baidu/mobads/production/e/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "interstitial ad is not ready"

    invoke-interface {p1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w(Ljava/lang/String;)I

    :cond_2
    :goto_0
    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x1

    .line 195
    iput-boolean p1, p0, Lcom/baidu/mobads/production/e/b;->D:Z

    if-eqz p2, :cond_0

    const-string p1, "type"

    .line 197
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 199
    check-cast p1, Ljava/lang/String;

    const-string p2, "video"

    .line 200
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 1

    .line 134
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "{\'ad\':[{\'id\':99999999,\'url\':\'"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/baidu/mobads/production/e/b;->B:Lcom/baidu/mobads/production/e/d;

    .line 135
    invoke-virtual {v0}, Lcom/baidu/mobads/production/e/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\', type=\'"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 136
    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'}],\'n\':1}"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 137
    invoke-virtual {p2, p1, p3}, Lcom/baidu/mobads/production/p;->a(Lcom/baidu/mobads/openad/b/b;Ljava/lang/String;)V

    return-void
.end method

.method public b()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/baidu/mobads/production/e/b;->B:Lcom/baidu/mobads/production/e/d;

    return-object v0
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 1

    const/16 v0, 0x1f40

    .line 107
    iput v0, p0, Lcom/baidu/mobads/production/a;->m:I

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 248
    invoke-virtual {p0}, Lcom/baidu/mobads/production/e/b;->s()V

    const/4 p1, 0x0

    .line 249
    iput-boolean p1, p0, Lcom/baidu/mobads/production/e/b;->E:Z

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/baidu/mobads/production/e/b;->b()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public request()V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/baidu/mobads/production/e/b;->B:Lcom/baidu/mobads/production/e/d;

    invoke-super {p0, v0}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method

.method public s()V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/baidu/mobads/production/e/b;->F:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/e/b;->F:Landroid/app/Activity;

    new-instance v1, Lcom/baidu/mobads/production/e/c;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/e/c;-><init>(Lcom/baidu/mobads/production/e/b;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public start()V
    .locals 0

    .line 142
    invoke-super {p0}, Lcom/baidu/mobads/production/a;->start()V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 244
    iget-boolean v0, p0, Lcom/baidu/mobads/production/e/b;->D:Z

    return v0
.end method
