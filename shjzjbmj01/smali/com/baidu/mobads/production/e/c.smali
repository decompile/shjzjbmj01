.class Lcom/baidu/mobads/production/e/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/production/e/b;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/production/e/b;)V
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/baidu/mobads/production/e/c;->a:Lcom/baidu/mobads/production/e/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 229
    iget-object v0, p0, Lcom/baidu/mobads/production/e/c;->a:Lcom/baidu/mobads/production/e/b;

    iget-object v0, v0, Lcom/baidu/mobads/production/e/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "remote Interstitial.removeAd"

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;)I

    .line 230
    iget-object v0, p0, Lcom/baidu/mobads/production/e/c;->a:Lcom/baidu/mobads/production/e/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/baidu/mobads/production/e/b;->a(Lcom/baidu/mobads/production/e/b;Z)Z

    .line 232
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/e/c;->a:Lcom/baidu/mobads/production/e/b;

    invoke-static {v0}, Lcom/baidu/mobads/production/e/b;->a(Lcom/baidu/mobads/production/e/b;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 233
    iget-object v0, p0, Lcom/baidu/mobads/production/e/c;->a:Lcom/baidu/mobads/production/e/b;

    iget-object v1, p0, Lcom/baidu/mobads/production/e/c;->a:Lcom/baidu/mobads/production/e/b;

    invoke-static {v1}, Lcom/baidu/mobads/production/e/b;->b(Lcom/baidu/mobads/production/e/b;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/baidu/mobads/production/e/b;->a(Lcom/baidu/mobads/production/e/b;Landroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 234
    iget-object v1, p0, Lcom/baidu/mobads/production/e/c;->a:Lcom/baidu/mobads/production/e/b;

    invoke-static {v1}, Lcom/baidu/mobads/production/e/b;->c(Lcom/baidu/mobads/production/e/b;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 235
    iget-object v1, p0, Lcom/baidu/mobads/production/e/c;->a:Lcom/baidu/mobads/production/e/b;

    invoke-static {v1}, Lcom/baidu/mobads/production/e/b;->c(Lcom/baidu/mobads/production/e/b;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 237
    iget-object v1, p0, Lcom/baidu/mobads/production/e/c;->a:Lcom/baidu/mobads/production/e/b;

    iget-object v1, v1, Lcom/baidu/mobads/production/e/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "Interstitial.removeAd"

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method
