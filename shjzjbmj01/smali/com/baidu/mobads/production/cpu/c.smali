.class public Lcom/baidu/mobads/production/cpu/c;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;


# instance fields
.field private z:Lcom/baidu/mobads/production/cpu/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;ILcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V
    .locals 7

    .line 43
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/cpu/c;->setActivity(Landroid/content/Context;)V

    .line 46
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/cpu/c;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 48
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/c;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 49
    new-instance p1, Lcom/baidu/mobads/production/cpu/e;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/cpu/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/cpu/c;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/cpu/c;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    move-object v0, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/baidu/mobads/production/cpu/e;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Ljava/lang/String;ILcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/c;->z:Lcom/baidu/mobads/production/cpu/e;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V
    .locals 7

    .line 31
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/cpu/c;->setActivity(Landroid/content/Context;)V

    .line 34
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/cpu/c;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 36
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/c;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 37
    new-instance p1, Lcom/baidu/mobads/production/cpu/e;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/cpu/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/baidu/mobads/production/cpu/c;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/production/cpu/c;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    move-object v0, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/baidu/mobads/production/cpu/e;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/c;->z:Lcom/baidu/mobads/production/cpu/e;

    return-void
.end method


# virtual methods
.method public a()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/c;->z:Lcom/baidu/mobads/production/cpu/e;

    return-object v0
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 100
    invoke-virtual {p0}, Lcom/baidu/mobads/production/cpu/c;->start()V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 0

    .line 85
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "{\'ad\':[{\'id\':99999999,\'html\':\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/baidu/mobads/production/cpu/c;->z:Lcom/baidu/mobads/production/cpu/e;

    .line 86
    invoke-virtual {p2}, Lcom/baidu/mobads/production/cpu/e;->c()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\', type=\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 87
    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'}],\'n\':1}"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 89
    :try_start_0
    new-instance p2, Lcom/baidu/mobads/vo/c;

    invoke-direct {p2, p1}, Lcom/baidu/mobads/vo/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/cpu/c;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string p1, "XAdMouldeLoader ad-server requesting success"

    .line 95
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/cpu/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method protected b(Lcom/baidu/mobads/vo/d;)V
    .locals 1

    .line 77
    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/c;->k:Lcom/baidu/mobads/vo/d;

    .line 78
    invoke-virtual {p0}, Lcom/baidu/mobads/production/cpu/c;->k()V

    const/4 p1, 0x0

    const/16 v0, 0x1388

    .line 79
    invoke-virtual {p0, p1, p1, v0}, Lcom/baidu/mobads/production/cpu/c;->a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V

    return-void
.end method

.method public c()V
    .locals 0

    .line 55
    invoke-virtual {p0}, Lcom/baidu/mobads/production/cpu/c;->load()V

    return-void
.end method

.method protected d()V
    .locals 1

    const/16 v0, 0x2710

    .line 60
    iput v0, p0, Lcom/baidu/mobads/production/a;->m:I

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 113
    invoke-super {p0}, Lcom/baidu/mobads/production/a;->p()V

    .line 114
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdUserClose"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/cpu/c;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/baidu/mobads/production/cpu/c;->a()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public request()V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/c;->z:Lcom/baidu/mobads/production/cpu/e;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/cpu/c;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method
