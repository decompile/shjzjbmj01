.class public Lcom/baidu/mobads/production/cpu/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/webkit/CookieManager;

.field private e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

.field private f:Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;

.field private g:Landroid/content/Context;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    .line 52
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->f:Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;

    .line 53
    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    .line 54
    iput p2, p0, Lcom/baidu/mobads/production/cpu/d;->h:I

    .line 55
    iput-object p3, p0, Lcom/baidu/mobads/production/cpu/d;->i:Ljava/lang/String;

    const/4 p1, 0x0

    .line 56
    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/d;->j:Ljava/lang/String;

    .line 57
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->b()V

    .line 58
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->c()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    .line 63
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->f:Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;

    .line 64
    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/baidu/mobads/production/cpu/d;->j:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/baidu/mobads/production/cpu/d;->i:Ljava/lang/String;

    const/4 p1, -0x1

    .line 67
    iput p1, p0, Lcom/baidu/mobads/production/cpu/d;->h:I

    .line 68
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->b()V

    .line 69
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->c()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .line 193
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 194
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p1, "="

    .line 195
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 196
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string p1, ";"

    .line 197
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 199
    :try_start_0
    iget-object p1, p0, Lcom/baidu/mobads/production/cpu/d;->d:Landroid/webkit/CookieManager;

    const-string p2, "https://cpu.baidu.com/"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const-string v0, ":"

    const-string v1, "-"

    .line 268
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private b()V
    .locals 2

    .line 126
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :catch_0
    :try_start_1
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->d:Landroid/webkit/CookieManager;

    .line 132
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->d:Landroid/webkit/CookieManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method

.method private c()V
    .locals 2

    .line 147
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->a:Ljava/util/Set;

    .line 148
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->a:Ljava/util/Set;

    const-string v1, "46000"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 149
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->a:Ljava/util/Set;

    const-string v1, "46002"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->a:Ljava/util/Set;

    const-string v1, "46007"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->b:Ljava/util/Set;

    .line 152
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->b:Ljava/util/Set;

    const-string v1, "46001"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->b:Ljava/util/Set;

    const-string v1, "46006"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 154
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->c:Ljava/util/Set;

    .line 155
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->c:Ljava/util/Set;

    const-string v1, "46003"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->c:Ljava/util/Set;

    const-string v1, "46005"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private d()V
    .locals 11

    .line 160
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->f:Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;

    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdCommonUtils;->getScreenRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 162
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 163
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->e()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 164
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->h()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/baidu/mobads/production/cpu/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    move-object v4, v3

    :goto_0
    if-eqz v2, :cond_1

    .line 165
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->f()I

    move-result v5

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v2, :cond_2

    .line 166
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->g()Ljava/lang/String;

    move-result-object v3

    .line 168
    :cond_2
    iget-object v6, p0, Lcom/baidu/mobads/production/cpu/d;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v7, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-interface {v6, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getCUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "v"

    .line 169
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->i()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v7, "im"

    const-string v8, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvz5XO+wDhxUaIDOtrp72fUeIfTYXUSkZXNbA0REQzFGXPFqeMvKEOacgixdfeb/1jWif6dE2pzX1kwMAaOCenIjP9MSw8ZRgR3bZmRq8IuiBPDLI68tFDE6jpA8WjTlcaSkBy06iPtPckAT3LQiPFQroz4Dsoxnrw1QFO82QyWoFfUhGZjj895BQSjfjJjZajOoEY6GBtcRmI30XlVUwMJT9JAqf8GjyvoOMDR3Tjp226UepBIF/NhJKMrW3M5a0SHWo6r+KiAuG6pSVCHPXdP6MaQ/6W2W62wxRqrf24hi407qyKOu4MiEAPbEP3UjdIV3AW1nADjUzg2nxSjRFKQIDAQAB"

    .line 171
    iget-object v9, p0, Lcom/baidu/mobads/production/cpu/d;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v10, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-interface {v9, v10}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/baidu/mobads/utils/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v7, "aid"

    const-string v8, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvz5XO+wDhxUaIDOtrp72fUeIfTYXUSkZXNbA0REQzFGXPFqeMvKEOacgixdfeb/1jWif6dE2pzX1kwMAaOCenIjP9MSw8ZRgR3bZmRq8IuiBPDLI68tFDE6jpA8WjTlcaSkBy06iPtPckAT3LQiPFQroz4Dsoxnrw1QFO82QyWoFfUhGZjj895BQSjfjJjZajOoEY6GBtcRmI30XlVUwMJT9JAqf8GjyvoOMDR3Tjp226UepBIF/NhJKMrW3M5a0SHWo6r+KiAuG6pSVCHPXdP6MaQ/6W2W62wxRqrf24hi407qyKOu4MiEAPbEP3UjdIV3AW1nADjUzg2nxSjRFKQIDAQAB"

    .line 172
    iget-object v9, p0, Lcom/baidu/mobads/production/cpu/d;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v10, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-interface {v9, v10}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/baidu/mobads/utils/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v7, "m"

    .line 173
    iget-object v8, p0, Lcom/baidu/mobads/production/cpu/d;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v9, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-interface {v8, v9}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/baidu/mobads/production/cpu/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v7, "cuid"

    const-string v8, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvz5XO+wDhxUaIDOtrp72fUeIfTYXUSkZXNbA0REQzFGXPFqeMvKEOacgixdfeb/1jWif6dE2pzX1kwMAaOCenIjP9MSw8ZRgR3bZmRq8IuiBPDLI68tFDE6jpA8WjTlcaSkBy06iPtPckAT3LQiPFQroz4Dsoxnrw1QFO82QyWoFfUhGZjj895BQSjfjJjZajOoEY6GBtcRmI30XlVUwMJT9JAqf8GjyvoOMDR3Tjp226UepBIF/NhJKMrW3M5a0SHWo6r+KiAuG6pSVCHPXdP6MaQ/6W2W62wxRqrf24hi407qyKOu4MiEAPbEP3UjdIV3AW1nADjUzg2nxSjRFKQIDAQAB"

    .line 174
    invoke-static {v8, v6}, Lcom/baidu/mobads/utils/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v7, v6}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v6, "ct"

    .line 175
    iget-object v7, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-static {v7}, Lcom/baidu/mobads/production/cpu/b;->a(Landroid/content/Context;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v6, "oi"

    .line 176
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->j()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v6, "src"

    const/4 v7, 0x1

    .line 177
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v6, "h"

    .line 178
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v6, v1}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "w"

    .line 179
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "apm"

    .line 180
    invoke-direct {p0, v0, v4}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "rssi"

    .line 181
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "apn"

    .line 182
    invoke-direct {p0, v0, v3}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "isc"

    .line 183
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "sdk_version"

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/baidu/mobads/g/g;->b(Landroid/content/Context;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 187
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "outerId"

    const-string v1, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvz5XO+wDhxUaIDOtrp72fUeIfTYXUSkZXNbA0REQzFGXPFqeMvKEOacgixdfeb/1jWif6dE2pzX1kwMAaOCenIjP9MSw8ZRgR3bZmRq8IuiBPDLI68tFDE6jpA8WjTlcaSkBy06iPtPckAT3LQiPFQroz4Dsoxnrw1QFO82QyWoFfUhGZjj895BQSjfjJjZajOoEY6GBtcRmI30XlVUwMJT9JAqf8GjyvoOMDR3Tjp226UepBIF/NhJKMrW3M5a0SHWo6r+KiAuG6pSVCHPXdP6MaQ/6W2W62wxRqrf24hi407qyKOu4MiEAPbEP3UjdIV3AW1nADjUzg2nxSjRFKQIDAQAB"

    .line 188
    iget-object v2, p0, Lcom/baidu/mobads/production/cpu/d;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/baidu/mobads/utils/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method private e()Z
    .locals 3

    const/4 v0, 0x0

    .line 207
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    const-string v2, "connectivity"

    .line 208
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 209
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 210
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    return v0
.end method

.method private f()I
    .locals 3

    const/4 v0, 0x0

    .line 219
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 220
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 221
    :cond_0
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :goto_0
    return v0

    :catch_0
    return v0
.end method

.method private g()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method private h()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method private i()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    .line 239
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    if-nez v1, :cond_0

    move-object v1, v0

    goto :goto_0

    .line 240
    :cond_0
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    :goto_0
    if-eqz v1, :cond_1

    const-string v2, "."

    const-string v3, "-"

    .line 242
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    :cond_1
    return-object v0
.end method

.method private j()I
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->e:Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->g:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getNetworkOperator(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    return v0

    .line 258
    :cond_1
    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    return v0

    .line 261
    :cond_2
    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    return v0

    :cond_3
    const/16 v0, 0x63

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .line 138
    invoke-direct {p0}, Lcom/baidu/mobads/production/cpu/d;->d()V

    .line 139
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/d;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://cpu.baidu.com/block/app/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 142
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://cpu.baidu.com/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/baidu/mobads/production/cpu/d;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/baidu/mobads/production/cpu/d;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/d;->k:Ljava/lang/String;

    return-void
.end method
