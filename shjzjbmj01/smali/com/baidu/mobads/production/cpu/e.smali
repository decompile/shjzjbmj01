.class public Lcom/baidu/mobads/production/cpu/e;
.super Lcom/baidu/mobads/vo/d;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Ljava/lang/String;ILcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/vo/d;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    .line 81
    new-instance p2, Lcom/baidu/mobads/production/cpu/d;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1, p5, p4}, Lcom/baidu/mobads/production/cpu/d;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    const/16 p1, 0x438

    if-eq p5, p1, :cond_0

    .line 84
    invoke-virtual {p6}, Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;->getParameters()Ljava/util/Map;

    move-result-object p1

    const-string p3, "city"

    invoke-interface {p1, p3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    :cond_0
    invoke-direct {p0, p2, p6}, Lcom/baidu/mobads/production/cpu/e;->a(Lcom/baidu/mobads/production/cpu/d;Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)Ljava/util/Map;

    move-result-object p1

    .line 87
    invoke-virtual {p2}, Lcom/baidu/mobads/production/cpu/d;->a()Ljava/lang/String;

    move-result-object p2

    .line 88
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/production/cpu/e;->a(Ljava/util/Map;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/e;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/vo/d;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    .line 26
    new-instance p2, Lcom/baidu/mobads/production/cpu/d;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1, p5, p4}, Lcom/baidu/mobads/production/cpu/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p2, p6}, Lcom/baidu/mobads/production/cpu/e;->a(Lcom/baidu/mobads/production/cpu/d;Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)Ljava/util/Map;

    move-result-object p1

    .line 30
    invoke-virtual {p2}, Lcom/baidu/mobads/production/cpu/d;->a()Ljava/lang/String;

    move-result-object p2

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/production/cpu/e;->a(Ljava/util/Map;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/production/cpu/e;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/util/Map;Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/net/Uri$Builder;"
        }
    .end annotation

    .line 44
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p2

    if-eqz p1, :cond_0

    .line 45
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 46
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 47
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 49
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 50
    invoke-virtual {p2, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_0
    return-object p2
.end method

.method private a(Lcom/baidu/mobads/production/cpu/d;Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/production/cpu/d;",
            "Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 67
    invoke-virtual {p2}, Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;->getParameters()Ljava/util/Map;

    move-result-object p2

    if-eqz p2, :cond_1

    const-string v0, "outerId"

    .line 69
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "outerId"

    .line 70
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/cpu/d;->a(Ljava/lang/String;)V

    const-string p1, "outerId"

    .line 71
    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :cond_1
    :goto_0
    return-object p2
.end method


# virtual methods
.method protected a()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string v0, "http://127.0.0.1"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/baidu/mobads/production/cpu/e;->b:Ljava/lang/String;

    return-object v0
.end method
