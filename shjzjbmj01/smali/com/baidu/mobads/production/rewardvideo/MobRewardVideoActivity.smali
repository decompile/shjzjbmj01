.class public Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field a:Lcom/baidu/mobads/MobRewardVideoImpl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 11
    new-instance v0, Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/MobRewardVideoImpl;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .line 74
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 75
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->finish()V

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .line 62
    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    .line 63
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->onAttachedToWindow()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 15
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/MobRewardVideoImpl;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 33
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 34
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->onDestroy()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onDetachedFromWindow()V

    .line 69
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->onDetachedFromWindow()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 21
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 22
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 27
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 28
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->onResume()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/MobRewardVideoImpl;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 57
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 48
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;->a:Lcom/baidu/mobads/MobRewardVideoImpl;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/MobRewardVideoImpl;->onWindowFocusChanged(Z)V

    return-void
.end method
