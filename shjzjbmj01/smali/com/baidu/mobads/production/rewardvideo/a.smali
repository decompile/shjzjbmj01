.class public Lcom/baidu/mobads/production/rewardvideo/a;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"


# instance fields
.field private A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

.field private B:Z

.field private C:Z

.field private z:Lcom/baidu/mobads/production/rewardvideo/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    .line 53
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_REWARD_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/production/rewardvideo/a;-><init>(Landroid/content/Context;Ljava/lang/String;ZLcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZLcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V
    .locals 3

    .line 65
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 38
    iput-boolean v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->B:Z

    .line 66
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/rewardvideo/a;->setId(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/rewardvideo/a;->setActivity(Landroid/content/Context;)V

    const/4 v1, 0x0

    .line 68
    invoke-virtual {p0, v1}, Lcom/baidu/mobads/production/rewardvideo/a;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 69
    iput-boolean p3, p0, Lcom/baidu/mobads/production/rewardvideo/a;->C:Z

    .line 70
    iput-object p4, p0, Lcom/baidu/mobads/production/rewardvideo/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 71
    new-instance p3, Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->getApplicationContext()Landroid/content/Context;

    move-result-object p4

    invoke-virtual {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/production/rewardvideo/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p3, p4, v1, v2}, Lcom/baidu/mobads/production/rewardvideo/b;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p3, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    .line 75
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p3

    invoke-virtual {p3}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object p3

    .line 77
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    .line 78
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingNone()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingLandingPage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    invoke-static {p1}, Lcom/baidu/mobads/utils/r;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->getSupportedActionType4RequestingDownload()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/baidu/mobads/utils/h;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p4

    .line 86
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {v1, p4}, Lcom/baidu/mobads/production/rewardvideo/b;->b(Ljava/lang/String;)V

    .line 87
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p4

    invoke-virtual {p4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p4

    invoke-virtual {p4, p1}, Lcom/baidu/mobads/utils/h;->getScreenRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object p4

    .line 89
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/production/rewardvideo/b;->d(I)V

    .line 90
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/production/rewardvideo/b;->e(I)V

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    .line 92
    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 93
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/baidu/mobads/production/rewardvideo/b;->d(I)V

    .line 94
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result p4

    invoke-virtual {p1, p4}, Lcom/baidu/mobads/production/rewardvideo/b;->e(I)V

    .line 96
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/rewardvideo/b;->h(I)V

    .line 97
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/rewardvideo/b;->d(Ljava/lang/String;)V

    .line 98
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/rewardvideo/b;->g(I)V

    .line 99
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    sget-object p2, Lcom/baidu/mobads/AdSize;->RewardVideo:Lcom/baidu/mobads/AdSize;

    invoke-virtual {p2}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/rewardvideo/b;->f(I)V

    .line 100
    iget-object p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->getAdCreativeTypeImage()I

    move-result p2

    invoke-virtual {p3}, Lcom/baidu/mobads/utils/l;->getAdCreativeTypeVideo()I

    move-result p3

    add-int/2addr p2, p3

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/rewardvideo/b;->i(I)V

    return-void
.end method

.method private w()V
    .locals 3

    .line 211
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mAdContainer:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 212
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mContext:Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    .line 213
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    invoke-static {}, Lcom/baidu/mobads/MobRewardVideoImpl;->getActivityClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-nez v1, :cond_0

    const/high16 v1, 0x10000000

    .line 216
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    const-string v1, "orientation"

    .line 218
    invoke-direct {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "useSurfaceView"

    .line 219
    iget-boolean v2, p0, Lcom/baidu/mobads/production/rewardvideo/a;->C:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 220
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private x()Ljava/lang/String;
    .locals 3

    const-string v0, "portrait"

    .line 226
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 228
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 229
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const-string v0, "landscape"

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const-string v0, "portrait"

    :cond_1
    :goto_0
    return-object v0
.end method


# virtual methods
.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 172
    iget-boolean p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->B:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 173
    iput-boolean p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->B:Z

    .line 174
    invoke-direct {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->w()V

    :cond_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 2

    int-to-double v0, p3

    .line 167
    invoke-virtual {p2, p1, v0, v1}, Lcom/baidu/mobads/production/p;->a(Lcom/baidu/mobads/openad/b/b;D)V

    return-void
.end method

.method public a()Z
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    iput-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 113
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->isExpired()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method public b()Z
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    iput-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->isVideoDownloaded()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public c()V
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->load()V

    return-void
.end method

.method public c(Z)V
    .locals 0

    .line 199
    iput-boolean p1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->B:Z

    return-void
.end method

.method protected d()V
    .locals 1

    const/16 v0, 0x1f40

    .line 157
    iput v0, p0, Lcom/baidu/mobads/production/a;->m:I

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->v()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public o()V
    .locals 1

    .line 204
    invoke-super {p0}, Lcom/baidu/mobads/production/a;->o()V

    const/4 v0, 0x0

    .line 205
    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mAdContainer:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 206
    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mContext:Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    return-void
.end method

.method public request()V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    invoke-super {p0, v0}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method

.method public s()Z
    .locals 5

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    iput-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 130
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdHasDisplayed()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    xor-int/lit8 v0, v0, 0x1

    return v0

    :catch_0
    move-exception v0

    .line 134
    iget-object v1, p0, Lcom/baidu/mobads/production/rewardvideo/a;->v:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v2, "XAbstractAdProdTemplate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notPlayedBefore-exception="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public t()Z
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    check-cast v0, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    iput-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 141
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 142
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 143
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->A:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    .line 145
    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;->getAdContainerContext()Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getVideoUrl()Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public u()V
    .locals 0

    .line 152
    invoke-direct {p0}, Lcom/baidu/mobads/production/rewardvideo/a;->w()V

    return-void
.end method

.method public v()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/baidu/mobads/production/rewardvideo/a;->z:Lcom/baidu/mobads/production/rewardvideo/b;

    return-object v0
.end method
