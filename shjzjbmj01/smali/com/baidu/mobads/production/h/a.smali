.class public Lcom/baidu/mobads/production/h/a;
.super Lcom/baidu/mobads/production/b/e;
.source "SourceFile"


# instance fields
.field private z:Lcom/baidu/mobads/production/g/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .line 21
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/b/e;-><init>(Landroid/content/Context;)V

    .line 22
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/h/a;->setId(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/h/a;->setActivity(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 24
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/h/a;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 25
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PREROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/h/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 26
    new-instance p1, Lcom/baidu/mobads/production/g/a;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_PREROLL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p1, v0, v1, v2, p0}, Lcom/baidu/mobads/production/g/a;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Lcom/baidu/mobads/interfaces/IXAdProd;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/h/a;->z:Lcom/baidu/mobads/production/g/a;

    .line 28
    iget-object p1, p0, Lcom/baidu/mobads/production/h/a;->z:Lcom/baidu/mobads/production/g/a;

    sget-object v0, Lcom/baidu/mobads/AdSize;->PrerollVideoNative:Lcom/baidu/mobads/AdSize;

    invoke-virtual {v0}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/production/g/a;->f(I)V

    .line 29
    iget-object p1, p0, Lcom/baidu/mobads/production/h/a;->z:Lcom/baidu/mobads/production/g/a;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/g/a;->d(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 2

    .line 34
    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getWidth()I

    move-result v0

    .line 35
    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/RequestParameters;->getHeight()I

    move-result p1

    if-lez v0, :cond_0

    if-lez p1, :cond_0

    .line 37
    iget-object v1, p0, Lcom/baidu/mobads/production/h/a;->z:Lcom/baidu/mobads/production/g/a;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/production/g/a;->d(I)V

    .line 38
    iget-object v0, p0, Lcom/baidu/mobads/production/h/a;->z:Lcom/baidu/mobads/production/g/a;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/g/a;->e(I)V

    :cond_0
    return-void
.end method

.method public b()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/baidu/mobads/production/h/a;->z:Lcom/baidu/mobads/production/g/a;

    return-object v0
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/baidu/mobads/production/h/a;->b()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public request()V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/baidu/mobads/production/h/a;->z:Lcom/baidu/mobads/production/g/a;

    invoke-super {p0, v0}, Lcom/baidu/mobads/production/b/e;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method
