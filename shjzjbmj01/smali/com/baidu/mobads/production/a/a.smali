.class public Lcom/baidu/mobads/production/a/a;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;


# instance fields
.field private z:Lcom/baidu/mobads/production/a/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Z)V
    .locals 2

    .line 39
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    .line 40
    invoke-virtual {p0, p3}, Lcom/baidu/mobads/production/a/a;->setId(Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a/a;->setActivity(Landroid/content/Context;)V

    .line 43
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/a/a;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 45
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_BANNER:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/a/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 47
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    .line 48
    new-instance p1, Lcom/baidu/mobads/production/a/b;

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/a/a;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p1, p2, v0, v1}, Lcom/baidu/mobads/production/a/b;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/a/a;->z:Lcom/baidu/mobads/production/a/b;

    .line 50
    iget-object p1, p0, Lcom/baidu/mobads/production/a/a;->z:Lcom/baidu/mobads/production/a/b;

    sget-object p2, Lcom/baidu/mobads/AdSize;->Banner:Lcom/baidu/mobads/AdSize;

    invoke-virtual {p2}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/a/b;->f(I)V

    .line 51
    iget-object p1, p0, Lcom/baidu/mobads/production/a/a;->z:Lcom/baidu/mobads/production/a/b;

    invoke-virtual {p1, p3}, Lcom/baidu/mobads/production/a/b;->d(Ljava/lang/String;)V

    .line 53
    iget-object p1, p0, Lcom/baidu/mobads/production/a/a;->z:Lcom/baidu/mobads/production/a/b;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/a/b;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object p1

    check-cast p1, Lcom/baidu/mobads/vo/b;

    .line 54
    invoke-virtual {p1, p4}, Lcom/baidu/mobads/vo/b;->a(Z)V

    .line 55
    invoke-virtual {p1}, Lcom/baidu/mobads/vo/b;->getAttribute()Lorg/json/JSONObject;

    move-result-object p2

    if-nez p2, :cond_0

    .line 57
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    :try_start_0
    const-string p4, "ABILITY"

    const-string v0, "BANNER_CLOSE,PAUSE,UNLIMITED_BANNER_SIZE,"

    .line 60
    invoke-virtual {p2, p4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :catch_0
    invoke-virtual {p1, p2}, Lcom/baidu/mobads/vo/b;->a(Lorg/json/JSONObject;)V

    .line 65
    invoke-virtual {p0, p3}, Lcom/baidu/mobads/production/a/a;->d(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/baidu/mobads/production/a/a;->z:Lcom/baidu/mobads/production/a/b;

    return-object v0
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 129
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->start()V

    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 0

    .line 108
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "{\'ad\':[{\'id\':99999999,\'url\':\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/baidu/mobads/production/a/a;->z:Lcom/baidu/mobads/production/a/b;

    .line 109
    invoke-virtual {p2}, Lcom/baidu/mobads/production/a/b;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\', type=\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 110
    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'}],\'n\':1}"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 113
    :try_start_0
    new-instance p2, Lcom/baidu/mobads/vo/c;

    invoke-direct {p2, p1}, Lcom/baidu/mobads/vo/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/a/a;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string p1, "XAdMouldeLoader ad-server requesting success"

    .line 123
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method protected b(Lcom/baidu/mobads/vo/d;)V
    .locals 1

    .line 100
    iput-object p1, p0, Lcom/baidu/mobads/production/a/a;->k:Lcom/baidu/mobads/vo/d;

    .line 101
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->k()V

    const/4 p1, 0x0

    const/16 v0, 0x1388

    .line 102
    invoke-virtual {p0, p1, p1, v0}, Lcom/baidu/mobads/production/a/a;->a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V

    return-void
.end method

.method public c()V
    .locals 0

    .line 70
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->load()V

    return-void
.end method

.method protected d()V
    .locals 1

    const/16 v0, 0x2710

    .line 75
    iput v0, p0, Lcom/baidu/mobads/production/a;->m:I

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 142
    invoke-super {p0}, Lcom/baidu/mobads/production/a;->p()V

    .line 143
    new-instance p1, Lcom/baidu/mobads/e/a;

    const-string p2, "AdUserClose"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/a/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->a()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public request()V
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/baidu/mobads/production/a/a;->z:Lcom/baidu/mobads/production/a/b;

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a/a;->a(Lcom/baidu/mobads/vo/d;)Z

    .line 83
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/baidu/mobads/production/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/baidu/mobads/utils/e;->a(Landroid/app/Activity;Landroid/content/Context;)Landroid/webkit/WebView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    :catch_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "error_message"

    const-string v2, "init webview,error"

    .line 93
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "AdError"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    goto :goto_0

    .line 87
    :catch_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "error_message"

    const-string v2, "init webview,exception"

    .line 88
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    new-instance v0, Lcom/baidu/mobads/e/a;

    const-string v1, "AdError"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/e/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/production/a/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    :goto_0
    return-void
.end method
