.class public Lcom/baidu/mobads/production/d/b;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/production/d/a;


# instance fields
.field protected final A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private B:Landroid/widget/RelativeLayout;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/os/CountDownTimer;

.field private E:Lcom/baidu/mobads/production/d/f;

.field private F:Z

.field private G:Z

.field private H:Landroid/app/Activity;

.field private I:Ljava/lang/Boolean;

.field public final z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/Boolean;Lcom/baidu/mobads/AdSize;Ljava/lang/String;)V
    .locals 2

    .line 75
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    const-string v0, "html5_intersitial"

    .line 38
    iput-object v0, p0, Lcom/baidu/mobads/production/d/b;->z:Ljava/lang/String;

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lcom/baidu/mobads/production/d/b;->F:Z

    .line 58
    iput-boolean v0, p0, Lcom/baidu/mobads/production/d/b;->G:Z

    .line 63
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/d/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 76
    invoke-virtual {p0, p5}, Lcom/baidu/mobads/production/d/b;->setId(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/production/d/b;->setActivity(Landroid/content/Context;)V

    .line 79
    invoke-virtual {p0, p2}, Lcom/baidu/mobads/production/d/b;->setAdSlotBase(Landroid/widget/RelativeLayout;)V

    .line 81
    sget-object p1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    iput-object p1, p0, Lcom/baidu/mobads/production/d/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    .line 83
    iput-object p3, p0, Lcom/baidu/mobads/production/d/b;->I:Ljava/lang/Boolean;

    .line 85
    new-instance p1, Lcom/baidu/mobads/production/d/f;

    .line 86
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/b;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/b;->getActivity()Landroid/app/Activity;

    move-result-object p3

    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->o:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p1, p2, p3, v0, v1}, Lcom/baidu/mobads/production/d/f;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;Ljava/lang/Boolean;)V

    iput-object p1, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    .line 87
    iget-object p1, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    sget-object p2, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_INTERSTITIAL:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->getValue()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/d/f;->c(Ljava/lang/String;)V

    .line 88
    iget-object p1, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    invoke-virtual {p4}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/d/f;->f(I)V

    .line 89
    iget-object p1, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    invoke-virtual {p1, p5}, Lcom/baidu/mobads/production/d/f;->d(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0, p5}, Lcom/baidu/mobads/production/d/b;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/production/d/b;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/baidu/mobads/production/d/b;->e:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobads/production/d/b;)Z
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/baidu/mobads/production/d/b;->w()Z

    move-result p0

    return p0
.end method

.method static synthetic c(Lcom/baidu/mobads/production/d/b;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/baidu/mobads/production/d/b;->y()V

    return-void
.end method

.method static synthetic d(Lcom/baidu/mobads/production/d/b;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/baidu/mobads/production/d/b;->x()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method static synthetic e(Lcom/baidu/mobads/production/d/b;)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/baidu/mobads/production/d/b;->z()Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/baidu/mobads/production/d/b;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/baidu/mobads/production/d/b;->e:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic g(Lcom/baidu/mobads/production/d/b;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/baidu/mobads/production/d/b;->e:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic h(Lcom/baidu/mobads/production/d/b;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/baidu/mobads/production/d/b;->e:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic i(Lcom/baidu/mobads/production/d/b;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/baidu/mobads/production/d/b;->e:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic j(Lcom/baidu/mobads/production/d/b;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/baidu/mobads/production/d/b;->e:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method static synthetic k(Lcom/baidu/mobads/production/d/b;)Landroid/widget/TextView;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/baidu/mobads/production/d/b;->C:Landroid/widget/TextView;

    return-object p0
.end method

.method private w()Z
    .locals 1

    .line 221
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/b;->s()Z

    move-result v0

    return v0
.end method

.method private x()Landroid/view/View;
    .locals 9

    .line 285
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->B:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/baidu/mobads/production/d/b;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/production/d/b;->B:Landroid/widget/RelativeLayout;

    .line 287
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->B:Landroid/widget/RelativeLayout;

    const/16 v1, 0x2a

    const/4 v2, 0x0

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 288
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/baidu/mobads/production/d/b;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/production/d/b;->C:Landroid/widget/TextView;

    .line 289
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->C:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 290
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    .line 292
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 293
    iget-object v1, p0, Lcom/baidu/mobads/production/d/b;->B:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/baidu/mobads/production/d/b;->C:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 295
    :cond_0
    new-instance v0, Lcom/baidu/mobads/production/d/e;

    const-wide/16 v5, 0x1770

    const-wide/16 v7, 0x3e8

    move-object v3, v0

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/baidu/mobads/production/d/e;-><init>(Lcom/baidu/mobads/production/d/b;JJ)V

    .line 313
    invoke-virtual {v0}, Lcom/baidu/mobads/production/d/e;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/production/d/b;->D:Landroid/os/CountDownTimer;

    .line 314
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->B:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private y()V
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->B:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->B:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->B:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/baidu/mobads/production/d/b;->B:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->D:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "cancel countDownTimer before it finished"

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;)I

    .line 324
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->D:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 326
    iget-object v1, p0, Lcom/baidu/mobads/production/d/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void
.end method

.method private z()Landroid/widget/RelativeLayout$LayoutParams;
    .locals 2

    .line 337
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/production/d/b;->f:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->getScreenDensity(Landroid/content/Context;)F

    move-result v0

    const/high16 v1, 0x41a00000    # 20.0f

    mul-float v0, v0, v1

    float-to-int v0, v0

    .line 338
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xb

    .line 339
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xa

    .line 340
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    return-object v1
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(II)V
    .locals 1

    .line 146
    iget-boolean v0, p0, Lcom/baidu/mobads/production/d/b;->F:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobads/production/d/b;->G:Z

    if-nez v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/d/f;->d(I)V

    .line 148
    iget-object p1, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/production/d/f;->e(I)V

    .line 150
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/b;->load()V

    :cond_0
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V
    .locals 3

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v1, "showInterstitialAdInit"

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;)I

    .line 169
    iget-boolean v0, p0, Lcom/baidu/mobads/production/d/b;->F:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/baidu/mobads/production/d/b;->G:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 170
    iput-boolean v0, p0, Lcom/baidu/mobads/production/d/b;->G:Z

    const/4 v0, 0x0

    .line 171
    iput-boolean v0, p0, Lcom/baidu/mobads/production/d/b;->F:Z

    .line 180
    iput-object p1, p0, Lcom/baidu/mobads/production/d/b;->H:Landroid/app/Activity;

    .line 181
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/b;->start()V

    .line 182
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/b;->u()V

    .line 183
    iget-object v1, p0, Lcom/baidu/mobads/production/d/b;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 184
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 185
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 186
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p1, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v1, p1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 188
    iget-object p1, p0, Lcom/baidu/mobads/production/d/b;->e:Landroid/widget/RelativeLayout;

    iget-object p2, p0, Lcom/baidu/mobads/production/d/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdView()Landroid/view/View;

    move-result-object p2

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, p2, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 189
    iget-object p1, p0, Lcom/baidu/mobads/production/d/b;->e:Landroid/widget/RelativeLayout;

    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, p1, p2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 191
    iget-object p1, p0, Lcom/baidu/mobads/production/d/b;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {p1}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdView()Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 173
    :cond_0
    iget-boolean p1, p0, Lcom/baidu/mobads/production/d/b;->G:Z

    if-eqz p1, :cond_1

    .line 174
    iget-object p1, p0, Lcom/baidu/mobads/production/d/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string p2, "interstitial ad is showing now"

    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w(Ljava/lang/String;)I

    goto :goto_0

    .line 175
    :cond_1
    iget-boolean p1, p0, Lcom/baidu/mobads/production/d/b;->F:Z

    if-nez p1, :cond_2

    .line 176
    iget-object p1, p0, Lcom/baidu/mobads/production/d/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string p2, "interstitial ad is not ready"

    invoke-interface {p1, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->w(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception p1

    .line 193
    iget-object p2, p0, Lcom/baidu/mobads/production/d/b;->A:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    :goto_1
    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x1

    .line 233
    iput-boolean p1, p0, Lcom/baidu/mobads/production/d/b;->F:Z

    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 1

    .line 130
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "{\'ad\':[{\'id\':99999999,\'url\':\'"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    .line 131
    invoke-virtual {v0}, Lcom/baidu/mobads/production/d/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\', type=\'"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->HTML:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;

    .line 132
    invoke-virtual {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo$CreativeType;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'}],\'n\':1}"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 133
    invoke-virtual {p2, p1, p3}, Lcom/baidu/mobads/production/p;->a(Lcom/baidu/mobads/openad/b/b;Ljava/lang/String;)V

    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public b()V
    .locals 2

    .line 198
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/baidu/mobads/production/d/c;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/d/c;-><init>(Lcom/baidu/mobads/production/d/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 238
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/b;->b()V

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 1

    const/16 v0, 0x1f40

    .line 103
    iput v0, p0, Lcom/baidu/mobads/production/a;->m:I

    return-void
.end method

.method protected e(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 273
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/b;->u()V

    const/4 p1, 0x0

    .line 275
    iput-boolean p1, p0, Lcom/baidu/mobads/production/d/b;->G:Z

    return-void
.end method

.method public synthetic getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/baidu/mobads/production/d/b;->t()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    return-object v0
.end method

.method public request()V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    invoke-super {p0, v0}, Lcom/baidu/mobads/production/a;->a(Lcom/baidu/mobads/vo/d;)Z

    return-void
.end method

.method protected s()Z
    .locals 2

    .line 228
    sget-object v0, Lcom/baidu/mobads/AdSize;->InterstitialForVideoBeforePlay:Lcom/baidu/mobads/AdSize;

    invoke-virtual {v0}, Lcom/baidu/mobads/AdSize;->getValue()I

    move-result v0

    iget-object v1, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    invoke-virtual {v1}, Lcom/baidu/mobads/production/d/f;->getApt()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public start()V
    .locals 0

    .line 138
    invoke-super {p0}, Lcom/baidu/mobads/production/a;->start()V

    return-void
.end method

.method public t()Lcom/baidu/mobads/vo/d;
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->E:Lcom/baidu/mobads/production/d/f;

    return-object v0
.end method

.method protected u()V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->H:Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/production/d/b;->H:Landroid/app/Activity;

    new-instance v1, Lcom/baidu/mobads/production/d/d;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/production/d/d;-><init>(Lcom/baidu/mobads/production/d/b;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 269
    iget-boolean v0, p0, Lcom/baidu/mobads/production/d/b;->F:Z

    return v0
.end method
