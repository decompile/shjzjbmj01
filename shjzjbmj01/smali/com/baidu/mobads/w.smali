.class Lcom/baidu/mobads/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/component/XAdView$Listener;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/SplashAd;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/SplashAd;)V
    .locals 0

    .line 464
    iput-object p1, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 1

    .line 499
    iget-object v0, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->n()V

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .line 492
    iget-object v0, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->o()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onLayoutComplete(II)V
    .locals 0

    .line 481
    iget-object p1, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 482
    iget-object p1, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    const/4 p2, 0x0

    iput-boolean p2, p1, Lcom/baidu/mobads/production/f/a;->A:Z

    .line 483
    iget-object p1, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {p1}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/production/f/a;->c()V

    goto :goto_0

    .line 485
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    const-string p2, "\u5c55\u73b0\u5931\u8d25\uff0c\u8bf7\u68c0\u67e5splashAd\u53c2\u6570\u662f\u5426\u6b63\u786e"

    invoke-static {p1, p2}, Lcom/baidu/mobads/SplashAd;->a(Lcom/baidu/mobads/SplashAd;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 474
    iget-object v0, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/f/a;->a(Z)V

    :cond_0
    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 1

    .line 467
    iget-object v0, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/baidu/mobads/w;->a:Lcom/baidu/mobads/SplashAd;

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/production/f/a;->b(I)V

    :cond_0
    return-void
.end method
