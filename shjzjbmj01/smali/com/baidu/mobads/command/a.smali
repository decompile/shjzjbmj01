.class public Lcom/baidu/mobads/command/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:I

.field public d:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

.field public e:Ljava/lang/Object;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field public i:I

.field public j:Z

.field public k:Z

.field public l:Ljava/lang/String;

.field protected final m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 74
    iput-wide v0, p0, Lcom/baidu/mobads/command/a;->b:J

    const/4 v0, 0x0

    .line 76
    iput v0, p0, Lcom/baidu/mobads/command/a;->c:I

    .line 80
    sget-object v1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->NONE:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    iput-object v1, p0, Lcom/baidu/mobads/command/a;->d:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    const/4 v1, 0x0

    .line 82
    iput-object v1, p0, Lcom/baidu/mobads/command/a;->e:Ljava/lang/Object;

    const/4 v2, 0x1

    .line 92
    iput-boolean v2, p0, Lcom/baidu/mobads/command/a;->h:Z

    .line 106
    iput v0, p0, Lcom/baidu/mobads/command/a;->i:I

    .line 111
    iput-boolean v0, p0, Lcom/baidu/mobads/command/a;->j:Z

    .line 146
    iput-boolean v0, p0, Lcom/baidu/mobads/command/a;->k:Z

    .line 147
    iput-object v1, p0, Lcom/baidu/mobads/command/a;->l:Ljava/lang/String;

    .line 149
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/a;->m:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 153
    iput-object p1, p0, Lcom/baidu/mobads/command/a;->f:Ljava/lang/String;

    .line 154
    iput-object p2, p0, Lcom/baidu/mobads/command/a;->a:Ljava/lang/String;

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 2

    .line 219
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v0

    .line 220
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getCurrentProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
