.class public Lcom/baidu/mobads/command/b/a;
.super Lcom/baidu/mobads/openad/a/c;
.source "SourceFile"


# static fields
.field private static d:Lcom/baidu/mobads/command/b/a;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/baidu/mobads/command/b/b;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/baidu/mobads/openad/a/c;-><init>()V

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/command/b/a;->b:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/baidu/mobads/command/b/a;
    .locals 2

    .line 27
    sget-object v0, Lcom/baidu/mobads/command/b/a;->d:Lcom/baidu/mobads/command/b/a;

    if-nez v0, :cond_1

    .line 28
    const-class v0, Lcom/baidu/mobads/command/b/a;

    monitor-enter v0

    .line 29
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/command/b/a;->d:Lcom/baidu/mobads/command/b/a;

    if-nez v1, :cond_0

    .line 30
    new-instance v1, Lcom/baidu/mobads/command/b/a;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/command/b/a;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/baidu/mobads/command/b/a;->d:Lcom/baidu/mobads/command/b/a;

    .line 32
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 34
    :cond_1
    :goto_0
    sget-object p0, Lcom/baidu/mobads/command/b/a;->d:Lcom/baidu/mobads/command/b/a;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 39
    new-instance v0, Lcom/baidu/mobads/openad/a/b;

    const-string v1, "AdLpClosed"

    invoke-direct {v0, v1}, Lcom/baidu/mobads/openad/a/b;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/command/b/a;->dispatchEvent(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V

    return-void
.end method

.method public b()V
    .locals 3

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/command/b/a;->c:Lcom/baidu/mobads/command/b/b;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/baidu/mobads/command/b/b;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/command/b/b;-><init>(Lcom/baidu/mobads/command/b/a;)V

    iput-object v0, p0, Lcom/baidu/mobads/command/b/a;->c:Lcom/baidu/mobads/command/b/b;

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/command/b/a;->b:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 49
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "lp_close"

    .line 50
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, Lcom/baidu/mobads/command/b/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/command/b/a;->c:Lcom/baidu/mobads/command/b/b;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    return-void
.end method

.method public c()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/baidu/mobads/command/b/a;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/command/b/a;->c:Lcom/baidu/mobads/command/b/b;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/baidu/mobads/command/b/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/baidu/mobads/command/b/a;->c:Lcom/baidu/mobads/command/b/b;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    .line 62
    iput-object v0, p0, Lcom/baidu/mobads/command/b/a;->c:Lcom/baidu/mobads/command/b/b;

    :cond_0
    return-void
.end method
