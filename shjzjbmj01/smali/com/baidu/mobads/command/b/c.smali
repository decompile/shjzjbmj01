.class public Lcom/baidu/mobads/command/b/c;
.super Lcom/baidu/mobads/command/b;
.source "SourceFile"


# instance fields
.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;Ljava/lang/String;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/command/b;-><init>(Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;Lcom/baidu/mobads/interfaces/IXAdResource;)V

    const/4 p1, 0x0

    .line 29
    iput-object p1, p0, Lcom/baidu/mobads/command/b/c;->h:Ljava/lang/String;

    const-string p1, ""

    .line 30
    iput-object p1, p0, Lcom/baidu/mobads/command/b/c;->f:Ljava/lang/String;

    const-string p1, ""

    .line 31
    iput-object p1, p0, Lcom/baidu/mobads/command/b/c;->g:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/baidu/mobads/command/b/c;->h:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .line 43
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    .line 44
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getActivityUtils()Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;

    move-result-object v1

    .line 46
    new-instance v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;

    iget-object v3, p0, Lcom/baidu/mobads/command/b/c;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    .line 47
    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getProdInfo()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v3

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdProdInfo;->getProdType()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/baidu/mobads/command/b/c;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-direct {v2, v3, v4}, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;-><init>(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    const/16 v3, 0x3e7

    .line 48
    iput v3, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->mIntTesting4LM:I

    const-string v3, "this is the test string"

    .line 49
    iput-object v3, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->mStringTesting4LM:Ljava/lang/String;

    .line 50
    iget-object v3, p0, Lcom/baidu/mobads/command/b/c;->h:Ljava/lang/String;

    iput-object v3, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->url:Ljava/lang/String;

    const/4 v3, 0x1

    .line 51
    iput v3, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->e75:I

    const/4 v3, 0x0

    .line 52
    iput v3, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->from:I

    .line 53
    iget-object v3, p0, Lcom/baidu/mobads/command/b/c;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getAdId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->adid:Ljava/lang/String;

    .line 54
    iget-object v3, p0, Lcom/baidu/mobads/command/b/c;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v3}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getQueryKey()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->qk:Ljava/lang/String;

    .line 55
    iget-object v3, p0, Lcom/baidu/mobads/command/b/c;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->packageNameOfPubliser:Ljava/lang/String;

    .line 56
    iget-object v3, p0, Lcom/baidu/mobads/command/b/c;->a:Landroid/content/Context;

    invoke-virtual {v0, v3}, Lcom/baidu/mobads/utils/h;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->appsid:Ljava/lang/String;

    .line 57
    iget-object v3, p0, Lcom/baidu/mobads/command/b/c;->a:Landroid/content/Context;

    invoke-virtual {v0, v3}, Lcom/baidu/mobads/utils/h;->getAppSec(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->appsec:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/baidu/mobads/command/b/c;->c:Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->title:Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lcom/baidu/mobads/command/b/c;->f:Ljava/lang/String;

    iput-object v0, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->lpShoubaiStyle:Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/baidu/mobads/command/b/c;->g:Ljava/lang/String;

    iput-object v0, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->lpMurl:Ljava/lang/String;

    .line 61
    invoke-static {}, Lcom/baidu/mobads/AppActivity;->getActivityClass()Ljava/lang/Class;

    move-result-object v0

    .line 62
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/baidu/mobads/command/b/c;->a:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 63
    iget-object v0, p0, Lcom/baidu/mobads/command/b/c;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/baidu/mobads/command/b/c;->b:Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXNonLinearAdSlot;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdActivityUtils;->isFullScreen(Landroid/app/Activity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->isFullScreen:Z

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/command/b/c;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, v2, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->orientation:I

    .line 68
    invoke-static {}, Lcom/baidu/mobads/AppActivity;->isAnti()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    const-class v0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;

    invoke-static {v0, v2}, Lcom/baidu/mobads/AppActivityImp;->classToString(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EXTRA_DATA_STRING"

    .line 70
    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-class v0, Lcom/baidu/mobads/command/XAdCommandExtraInfo;

    invoke-static {v0, v2}, Lcom/baidu/mobads/AppActivityImp;->classToString(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EXTRA_DATA_STRING_COM"

    .line 74
    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-class v0, Lcom/baidu/mobads/vo/XAdInstanceInfo;

    .line 77
    invoke-virtual {v2}, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v1

    .line 76
    invoke-static {v0, v1}, Lcom/baidu/mobads/AppActivityImp;->classToString(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "EXTRA_DATA_STRING_AD"

    .line 78
    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_1
    const-string v0, "EXTRA_DATA"

    .line 80
    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 86
    :goto_0
    const-class v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    .line 87
    invoke-static {}, Lcom/baidu/mobads/AppActivity;->getActionBarColorTheme()Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/baidu/mobads/AppActivityImp;->classToString(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "theme"

    .line 88
    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "showWhenLocked"

    .line 90
    invoke-static {}, Lcom/baidu/mobads/AppActivity;->getLpShowWhenLocked()Z

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    .line 91
    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v0, 0x20000000

    .line 92
    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 93
    iget-object v0, p0, Lcom/baidu/mobads/command/b/c;->a:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 96
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    :goto_1
    return-void
.end method
