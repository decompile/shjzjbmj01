.class public Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;
.super Lcom/baidu/mobads/command/XAdCommandExtraInfo;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public e75:I

.field public from:I

.field public isFullScreen:Z

.field public lpMurl:Ljava/lang/String;

.field public lpShoubaiStyle:Ljava/lang/String;

.field public orientation:I

.field public os:I

.field public prodType:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 86
    new-instance v0, Lcom/baidu/mobads/command/c;

    invoke-direct {v0}, Lcom/baidu/mobads/command/c;-><init>()V

    sput-object v0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .line 37
    invoke-direct {p0, p1}, Lcom/baidu/mobads/command/XAdCommandExtraInfo;-><init>(Landroid/os/Parcel;)V

    const/4 v0, 0x0

    .line 18
    iput-boolean v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->isFullScreen:Z

    const/4 v1, 0x1

    .line 19
    iput v1, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->orientation:I

    .line 22
    iput v1, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->e75:I

    .line 23
    iput v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->from:I

    .line 25
    iput v1, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->os:I

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->url:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->isFullScreen:Z

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->orientation:I

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->e75:I

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->from:I

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->adid:Ljava/lang/String;

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->qk:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->packageNameOfPubliser:Ljava/lang/String;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->appsid:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->appsec:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->os:I

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->prodType:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->v:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->sn:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->title:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->lpShoubaiStyle:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->lpMurl:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/baidu/mobads/command/c;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/command/XAdCommandExtraInfo;-><init>(Ljava/lang/String;Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;)V

    const/4 p1, 0x0

    .line 18
    iput-boolean p1, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->isFullScreen:Z

    const/4 p2, 0x1

    .line 19
    iput p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->orientation:I

    .line 22
    iput p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->e75:I

    .line 23
    iput p1, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->from:I

    .line 25
    iput p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->os:I

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 60
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    .line 61
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v1

    .line 63
    invoke-super {p0, p1, p2}, Lcom/baidu/mobads/command/XAdCommandExtraInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 64
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->url:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    iget-boolean p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->isFullScreen:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 66
    iget p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->orientation:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 67
    iget p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->e75:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 68
    iget p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->from:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->adid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 72
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->qk:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->packageNameOfPubliser:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->appsid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->appsec:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/4 p2, 0x1

    .line 76
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->mProdType:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v0}, Lcom/baidu/mobads/utils/h;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-interface {v1, p2}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->getEncodedSN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 80
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->title:Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, ""

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->title:Ljava/lang/String;

    :goto_0
    iput-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->title:Ljava/lang/String;

    .line 81
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 82
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->lpShoubaiStyle:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object p2, p0, Lcom/baidu/mobads/command/XAdLandingPageExtraInfo;->lpMurl:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
