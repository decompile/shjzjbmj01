.class public final Lcom/baidu/mobads/CpuAdView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/baidu/mobads/production/cpu/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/CpuAdView;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V
    .locals 8

    .line 47
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v6, Lcom/baidu/mobads/component/XAdView;

    invoke-direct {v6, p1}, Lcom/baidu/mobads/component/XAdView;-><init>(Landroid/content/Context;)V

    .line 49
    new-instance v7, Lcom/baidu/mobads/production/cpu/c;

    move-object v0, v7

    move-object v1, p1

    move-object v2, v6

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/production/cpu/c;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;ILcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V

    iput-object v7, p0, Lcom/baidu/mobads/CpuAdView;->a:Lcom/baidu/mobads/production/cpu/c;

    .line 50
    iget-object p1, p0, Lcom/baidu/mobads/CpuAdView;->a:Lcom/baidu/mobads/production/cpu/c;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/cpu/c;->request()V

    .line 51
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 p2, -0x1

    invoke-direct {p1, p2, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v6, p1}, Lcom/baidu/mobads/CpuAdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/mobads/CpuInfoManager$UrlListener;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 27
    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/CpuAdView;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/mobads/CpuInfoManager$UrlListener;Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/mobads/CpuInfoManager$UrlListener;Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V
    .locals 7

    .line 33
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 34
    new-instance p4, Lcom/baidu/mobads/component/XAdView;

    invoke-direct {p4, p1}, Lcom/baidu/mobads/component/XAdView;-><init>(Landroid/content/Context;)V

    .line 35
    new-instance v6, Lcom/baidu/mobads/production/cpu/c;

    move-object v0, v6

    move-object v1, p1

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/production/cpu/c;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Ljava/lang/String;Lcom/baidu/mobads/production/cpu/CPUWebAdRequestParam;)V

    iput-object v6, p0, Lcom/baidu/mobads/CpuAdView;->a:Lcom/baidu/mobads/production/cpu/c;

    .line 36
    iget-object p1, p0, Lcom/baidu/mobads/CpuAdView;->a:Lcom/baidu/mobads/production/cpu/c;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/cpu/c;->request()V

    .line 37
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 p2, -0x1

    invoke-direct {p1, p2, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p4, p1}, Lcom/baidu/mobads/CpuAdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method protected canGoBack()Z
    .locals 1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/CpuAdView;->a:Lcom/baidu/mobads/production/cpu/c;

    iget-object v0, v0, Lcom/baidu/mobads/production/cpu/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method protected goBack()V
    .locals 1

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/CpuAdView;->a:Lcom/baidu/mobads/production/cpu/c;

    iget-object v0, v0, Lcom/baidu/mobads/production/cpu/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 79
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/CpuAdView;->a:Lcom/baidu/mobads/production/cpu/c;

    iget-object v0, v0, Lcom/baidu/mobads/production/cpu/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdView()Landroid/view/View;

    move-result-object v0

    .line 80
    instance-of v1, v0, Landroid/webkit/WebView;

    if-eqz v1, :cond_0

    .line 81
    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public onKeyBackDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p2, 0x4

    if-ne p1, p2, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/baidu/mobads/CpuAdView;->canGoBack()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/baidu/mobads/CpuAdView;->goBack()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onPause()V
    .locals 2

    .line 69
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/CpuAdView;->a:Lcom/baidu/mobads/production/cpu/c;

    iget-object v0, v0, Lcom/baidu/mobads/production/cpu/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdView()Landroid/view/View;

    move-result-object v0

    .line 70
    instance-of v1, v0, Landroid/webkit/WebView;

    if-eqz v1, :cond_0

    .line 71
    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .line 58
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/CpuAdView;->a:Lcom/baidu/mobads/production/cpu/c;

    iget-object v0, v0, Lcom/baidu/mobads/production/cpu/c;->h:Lcom/baidu/mobads/interfaces/IXAdContainer;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdContainer;->getAdView()Landroid/view/View;

    move-result-object v0

    .line 59
    instance-of v1, v0, Landroid/webkit/WebView;

    if-eqz v1, :cond_0

    .line 60
    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method
