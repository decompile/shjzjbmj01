.class Lcom/baidu/mobads/i/b$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/i/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobads/i/b;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/i/b;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/baidu/mobads/i/b$a;->a:Lcom/baidu/mobads/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 127
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object p1

    .line 128
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 p3, 0x0

    if-eqz p2, :cond_0

    return-object p3

    :cond_0
    const-string p2, "playCompletion"

    .line 131
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 132
    iget-object p1, p0, Lcom/baidu/mobads/i/b$a;->a:Lcom/baidu/mobads/i/b;

    invoke-static {p1}, Lcom/baidu/mobads/i/b;->a(Lcom/baidu/mobads/i/b;)Lcom/baidu/mobads/i/a;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 133
    iget-object p1, p0, Lcom/baidu/mobads/i/b$a;->a:Lcom/baidu/mobads/i/b;

    invoke-static {p1}, Lcom/baidu/mobads/i/b;->a(Lcom/baidu/mobads/i/b;)Lcom/baidu/mobads/i/a;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/i/a;->a()V

    goto :goto_0

    :cond_1
    const-string p2, "playFailure"

    .line 135
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 136
    iget-object p1, p0, Lcom/baidu/mobads/i/b$a;->a:Lcom/baidu/mobads/i/b;

    invoke-static {p1}, Lcom/baidu/mobads/i/b;->a(Lcom/baidu/mobads/i/b;)Lcom/baidu/mobads/i/a;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 137
    iget-object p1, p0, Lcom/baidu/mobads/i/b$a;->a:Lcom/baidu/mobads/i/b;

    invoke-static {p1}, Lcom/baidu/mobads/i/b;->a(Lcom/baidu/mobads/i/b;)Lcom/baidu/mobads/i/a;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/i/a;->b()V

    goto :goto_0

    :cond_2
    const-string p2, "onAdShow"

    .line 139
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 140
    iget-object p1, p0, Lcom/baidu/mobads/i/b$a;->a:Lcom/baidu/mobads/i/b;

    invoke-static {p1}, Lcom/baidu/mobads/i/b;->a(Lcom/baidu/mobads/i/b;)Lcom/baidu/mobads/i/a;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 141
    iget-object p1, p0, Lcom/baidu/mobads/i/b$a;->a:Lcom/baidu/mobads/i/b;

    invoke-static {p1}, Lcom/baidu/mobads/i/b;->a(Lcom/baidu/mobads/i/b;)Lcom/baidu/mobads/i/a;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/i/a;->c()V

    :cond_3
    :goto_0
    return-object p3
.end method
