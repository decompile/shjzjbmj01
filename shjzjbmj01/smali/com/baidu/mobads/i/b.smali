.class public Lcom/baidu/mobads/i/b;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/i/b$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/baidu/mobads/i/a;

.field private c:Landroid/view/View;

.field private d:Ljava/lang/String;

.field private e:Ldalvik/system/DexClassLoader;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const-string v0, "TvtfWYI1/aJhqbnwJ3f1NpdlWii72QtgZNj6vCzWTzeryGCytmYPBnjkQ3CBaxEa"

    .line 35
    iput-object v0, p0, Lcom/baidu/mobads/i/b;->d:Ljava/lang/String;

    .line 40
    invoke-direct {p0, p1}, Lcom/baidu/mobads/i/b;->a(Landroid/content/Context;)V

    const-string p1, "#000000"

    .line 41
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/i/b;->setBackgroundColor(I)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/i/b;)Lcom/baidu/mobads/i/a;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/baidu/mobads/i/b;->b:Lcom/baidu/mobads/i/a;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/i/b;->a:Landroid/content/Context;

    const/4 p1, 0x1

    .line 46
    new-array v0, p1, [Ljava/lang/Class;

    const-class v1, Landroid/content/Context;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 47
    new-array p1, p1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/baidu/mobads/i/b;->a:Landroid/content/Context;

    aput-object v1, p1, v2

    .line 48
    invoke-static {}, Lcom/baidu/mobads/g/b;->d()Ldalvik/system/DexClassLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    .line 49
    iget-object v1, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    if-nez v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/baidu/mobads/i/b;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/baidu/mobads/r;->a(Landroid/content/Context;)Ldalvik/system/DexClassLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/i/b;->d:Ljava/lang/String;

    .line 54
    invoke-static {v1}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    invoke-static {v1, v2, v0, p1}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/ClassLoader;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    .line 55
    iget-object p1, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    if-eqz p1, :cond_1

    .line 56
    iget-object p1, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/i/b;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a()J
    .locals 7

    .line 105
    iget-object v0, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/baidu/mobads/i/b;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    const-string v4, "getCurrentPosition"

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public a(Lcom/baidu/mobad/nativevideo/XAdVideoResponse;)V
    .locals 8

    if-nez p1, :cond_0

    return-void

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/baidu/mobads/i/b;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    const-string v4, "setAdData"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v6, Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public a(Lcom/baidu/mobads/i/a;)V
    .locals 10

    .line 78
    iput-object p1, p0, Lcom/baidu/mobads/i/b;->b:Lcom/baidu/mobads/i/a;

    :try_start_0
    const-string p1, "com.component.patchad.IPatchAdListener"

    .line 81
    iget-object v0, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    invoke-static {p1, v0}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    new-instance v4, Lcom/baidu/mobads/i/b$a;

    invoke-direct {v4, p0}, Lcom/baidu/mobads/i/b$a;-><init>(Lcom/baidu/mobads/i/b;)V

    invoke-static {v0, v2, v4}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    .line 84
    iget-object v2, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 85
    iget-object v2, p0, Lcom/baidu/mobads/i/b;->d:Ljava/lang/String;

    invoke-static {v2}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    iget-object v6, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    const-string v7, "setPatchAdListener"

    new-array v8, v1, [Ljava/lang/Class;

    aput-object p1, v8, v3

    new-array v9, v1, [Ljava/lang/Object;

    aput-object v0, v9, v3

    invoke-static/range {v4 .. v9}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 8

    .line 96
    iget-object v0, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/i/b;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    const-string v4, "setVideoVolume"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    .line 99
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v6, v7

    .line 97
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public b()J
    .locals 7

    .line 115
    iget-object v0, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/i/b;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/i/b;->c:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/i/b;->e:Ldalvik/system/DexClassLoader;

    const-string v4, "getDuration"

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method
