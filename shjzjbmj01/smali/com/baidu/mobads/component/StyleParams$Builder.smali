.class public Lcom/baidu/mobads/component/StyleParams$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/component/StyleParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public mBrandBottomDp:I

.field public mBrandFontColor:I

.field public mBrandFontSizeSp:I

.field public mBrandFontTypeFace:Landroid/graphics/Typeface;

.field public mBrandLeftDp:I

.field public mBrandRightDp:I

.field public mButtonBackgroundColor:I

.field public mButtonBottomDp:I

.field public mButtonFontColor:I

.field public mButtonFontSizeSp:I

.field public mButtonFontTypeFace:Landroid/graphics/Typeface;

.field public mButtonForegroundColor:I

.field public mButtonHeightDp:I

.field public mButtonLeftDp:I

.field public mButtonRightDp:I

.field public mButtonTopDp:I

.field public mButtonWidthDp:I

.field public mFirstPicBottomDp:I

.field public mFirstPicHeightDp:I

.field public mFirstPicLeftDp:I

.field public mFirstPicRightDp:I

.field public mFirstPicTopDp:I

.field public mFirstPicWidthDp:I

.field public mIconBottomDp:I

.field public mIconHeightDp:I

.field public mIconLeftDp:I

.field public mIconRightDp:I

.field public mIconTopDp:I

.field public mIconWidthDp:I

.field public mImageBackground:Landroid/graphics/drawable/Drawable;

.field public mImageBackgroundColor:I

.field public mIsShowDownloadInfo:Z

.field public mSmartDownloadAppNameBottomDp:I

.field public mSmartDownloadAppNameLeftDp:I

.field public mSmartDownloadAppNameRightDp:I

.field public mSmartDownloadAppNameTextColor:I

.field public mSmartDownloadAppNameTextSizeSp:I

.field public mSmartDownloadAppNameTopDp:I

.field public mSmartDownloadButtonBackgroundColor:I

.field public mSmartDownloadButtonBottomDp:I

.field public mSmartDownloadButtonFontColor:I

.field public mSmartDownloadButtonFontSizeSp:I

.field public mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

.field public mSmartDownloadButtonForegroundColor:I

.field public mSmartDownloadButtonHeightDp:I

.field public mSmartDownloadButtonLeftDp:I

.field public mSmartDownloadButtonRightDp:I

.field public mSmartDownloadButtonTopDp:I

.field public mSmartDownloadButtonWidthDp:I

.field public mSmartDownloadCompanyBottomDp:I

.field public mSmartDownloadCompanyLeftDp:I

.field public mSmartDownloadCompanyRightDp:I

.field public mSmartDownloadCompanyTextColor:I

.field public mSmartDownloadCompanyTextSizeSp:I

.field public mSmartDownloadCompanyTopDp:I

.field public mSmartDownloadPermissionBottomDp:I

.field public mSmartDownloadPermissionLeftDp:I

.field public mSmartDownloadPermissionRightDp:I

.field public mSmartDownloadPermissionTextColor:I

.field public mSmartDownloadPermissionTextSizeSp:I

.field public mSmartDownloadPermissionTopDp:I

.field public mSmartDownloadPrivacyBottomDp:I

.field public mSmartDownloadPrivacyLeftDp:I

.field public mSmartDownloadPrivacyRightDp:I

.field public mSmartDownloadPrivacyTextColor:I

.field public mSmartDownloadPrivacyTextSizeSp:I

.field public mSmartDownloadPrivacyTopDp:I

.field public mSmartDownloadVersionBottomDp:I

.field public mSmartDownloadVersionLeftDp:I

.field public mSmartDownloadVersionRightDp:I

.field public mSmartDownloadVersionTextColor:I

.field public mSmartDownloadVersionTextSizeSp:I

.field public mSmartDownloadVersionTopDp:I

.field public mSmartDownloadViewBackgroundColor:I

.field public mSmartDownloadViewBottomDp:I

.field public mSmartDownloadViewHeightDp:I

.field public mSmartDownloadViewLeftDp:I

.field public mSmartDownloadViewRightDp:I

.field public mSmartDownloadViewTopDp:I

.field public mSmartDownloadViewWidthDp:I

.field public mThreePicBottomDp:I

.field public mThreePicHeightDp:I

.field public mThreePicLeftDp:I

.field public mThreePicRightDp:I

.field public mThreePicTopDp:I

.field public mThreePicWidthDp:I

.field public mTitleBottomDp:I

.field public mTitleFontColor:I

.field public mTitleFontSizeSp:I

.field public mTitleFontTypeFace:Landroid/graphics/Typeface;

.field public mTitleLeftDp:I

.field public mTitleRightDp:I

.field public mTitleTopDp:I

.field public mTwoPicBottomDp:I

.field public mTwoPicHeightDp:I

.field public mTwoPicLeftDp:I

.field public mTwoPicRightDp:I

.field public mTwoPicTopDp:I

.field public mTwoPicWidthDp:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 687
    iput-boolean v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIsShowDownloadInfo:Z

    const-string v0, "#F5F5F5"

    .line 690
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewBackgroundColor:I

    const/4 v0, -0x1

    .line 692
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewWidthDp:I

    .line 694
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewHeightDp:I

    .line 696
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewLeftDp:I

    .line 698
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewRightDp:I

    .line 700
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewTopDp:I

    .line 702
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewBottomDp:I

    .line 705
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyTextSizeSp:I

    const-string v1, "#858585"

    .line 707
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyTextColor:I

    .line 709
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyTopDp:I

    .line 711
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyBottomDp:I

    .line 713
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyLeftDp:I

    .line 715
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyRightDp:I

    .line 718
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionTextSizeSp:I

    const-string v1, "#858585"

    .line 720
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionTextColor:I

    .line 722
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionTopDp:I

    .line 724
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionBottomDp:I

    .line 726
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionLeftDp:I

    .line 728
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionRightDp:I

    .line 731
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionTextSizeSp:I

    const-string v1, "#858585"

    .line 733
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionTextColor:I

    .line 735
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionTopDp:I

    .line 737
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionBottomDp:I

    .line 739
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionLeftDp:I

    .line 741
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionRightDp:I

    .line 744
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyTextSizeSp:I

    const-string v1, "#858585"

    .line 746
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyTextColor:I

    .line 748
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyTopDp:I

    .line 750
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyBottomDp:I

    .line 752
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyLeftDp:I

    .line 754
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyRightDp:I

    .line 757
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameTextSizeSp:I

    const-string v1, "#1F1F1F"

    .line 759
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameTextColor:I

    .line 761
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameTopDp:I

    .line 763
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameBottomDp:I

    .line 765
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameLeftDp:I

    .line 767
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameRightDp:I

    const/4 v1, -0x2

    .line 770
    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonWidthDp:I

    .line 772
    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonHeightDp:I

    .line 774
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonLeftDp:I

    .line 776
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonRightDp:I

    .line 778
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonTopDp:I

    .line 780
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonBottomDp:I

    const-string v2, "#3388FF"

    .line 782
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonForegroundColor:I

    const-string v2, "#D7E6FF"

    .line 784
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonBackgroundColor:I

    .line 786
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonFontSizeSp:I

    const-string v2, "#F5F5F5"

    .line 788
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonFontColor:I

    const/4 v2, 0x0

    .line 790
    iput-object v2, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

    .line 793
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconWidthDp:I

    .line 795
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconHeightDp:I

    .line 797
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconTopDp:I

    .line 799
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconBottomDp:I

    .line 801
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconLeftDp:I

    .line 803
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconRightDp:I

    .line 806
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleLeftDp:I

    .line 808
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleRightDp:I

    .line 810
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleTopDp:I

    .line 812
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleBottomDp:I

    .line 814
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleFontSizeSp:I

    const-string v3, "#000000"

    .line 816
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleFontColor:I

    .line 818
    iput-object v2, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleFontTypeFace:Landroid/graphics/Typeface;

    .line 822
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicWidthDp:I

    .line 824
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicHeightDp:I

    .line 826
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicTopDp:I

    .line 828
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicBottomDp:I

    .line 830
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicLeftDp:I

    .line 832
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicRightDp:I

    .line 836
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicWidthDp:I

    .line 838
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicHeightDp:I

    .line 840
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicTopDp:I

    .line 842
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicBottomDp:I

    .line 844
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicLeftDp:I

    .line 846
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicRightDp:I

    .line 850
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicWidthDp:I

    .line 852
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicHeightDp:I

    .line 854
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicTopDp:I

    .line 856
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicBottomDp:I

    .line 858
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicLeftDp:I

    .line 860
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicRightDp:I

    const-string v3, "#F4F5F6"

    .line 863
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mImageBackgroundColor:I

    .line 865
    iput-object v2, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mImageBackground:Landroid/graphics/drawable/Drawable;

    .line 868
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandLeftDp:I

    .line 872
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandBottomDp:I

    .line 874
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandFontSizeSp:I

    const-string v3, "#999999"

    .line 876
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandFontColor:I

    .line 878
    iput-object v2, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandFontTypeFace:Landroid/graphics/Typeface;

    .line 881
    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonWidthDp:I

    .line 883
    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonHeightDp:I

    .line 885
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonLeftDp:I

    .line 887
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonRightDp:I

    .line 889
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonTopDp:I

    .line 891
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonBottomDp:I

    const-string v1, "#3388FF"

    .line 893
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonForegroundColor:I

    const-string v1, "#D7E6FF"

    .line 895
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonBackgroundColor:I

    .line 897
    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonFontSizeSp:I

    const-string v0, "#FFFFFF"

    .line 899
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonFontColor:I

    .line 901
    iput-object v2, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonFontTypeFace:Landroid/graphics/Typeface;

    return-void
.end method


# virtual methods
.method public build()Lcom/baidu/mobads/component/StyleParams;
    .locals 1

    .line 1387
    new-instance v0, Lcom/baidu/mobads/component/StyleParams;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/component/StyleParams;-><init>(Lcom/baidu/mobads/component/StyleParams$Builder;)V

    return-object v0
.end method

.method public setBrandBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1079
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandBottomDp:I

    return-object p0
.end method

.method public setBrandFontColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1089
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandFontColor:I

    return-object p0
.end method

.method public setBrandFontSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1084
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandFontSizeSp:I

    return-object p0
.end method

.method public setBrandFontTypeFace(Landroid/graphics/Typeface;)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1094
    iput-object p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandFontTypeFace:Landroid/graphics/Typeface;

    return-object p0
.end method

.method public setBrandLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1069
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandLeftDp:I

    return-object p0
.end method

.method public setBrandRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1074
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandRightDp:I

    return-object p0
.end method

.method public setButtonBackgroundColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1134
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonBackgroundColor:I

    return-object p0
.end method

.method public setButtonBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1124
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonBottomDp:I

    return-object p0
.end method

.method public setButtonFontTypeface(Landroid/graphics/Typeface;)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1149
    iput-object p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonFontTypeFace:Landroid/graphics/Typeface;

    return-object p0
.end method

.method public setButtonForegroundColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1129
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonForegroundColor:I

    return-object p0
.end method

.method public setButtonHeightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1104
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonHeightDp:I

    return-object p0
.end method

.method public setButtonLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1109
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonLeftDp:I

    return-object p0
.end method

.method public setButtonRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1114
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonRightDp:I

    return-object p0
.end method

.method public setButtonTextColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1139
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonFontColor:I

    return-object p0
.end method

.method public setButtonTextSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1144
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonFontSizeSp:I

    return-object p0
.end method

.method public setButtonTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1119
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonTopDp:I

    return-object p0
.end method

.method public setButtonWidthDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1099
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonWidthDp:I

    return-object p0
.end method

.method public setDownloadViewBackgroundColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1174
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewBackgroundColor:I

    return-object p0
.end method

.method public setDownloadViewBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1169
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewBottomDp:I

    return-object p0
.end method

.method public setDownloadViewLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1154
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewLeftDp:I

    return-object p0
.end method

.method public setDownloadViewRightD(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1159
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewRightDp:I

    return-object p0
.end method

.method public setDownloadViewTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1164
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewTopDp:I

    return-object p0
.end method

.method public setFirstPicBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 984
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicBottomDp:I

    return-object p0
.end method

.method public setFirstPicHeightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 974
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicHeightDp:I

    return-object p0
.end method

.method public setFirstPicLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 989
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicLeftDp:I

    return-object p0
.end method

.method public setFirstPicRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 994
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicRightDp:I

    return-object p0
.end method

.method public setFirstPicTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 979
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicTopDp:I

    return-object p0
.end method

.method public setFirstPicWidthDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 969
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicWidthDp:I

    return-object p0
.end method

.method public setIconBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 919
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconBottomDp:I

    return-object p0
.end method

.method public setIconHeightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 909
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconHeightDp:I

    return-object p0
.end method

.method public setIconLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 924
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconLeftDp:I

    return-object p0
.end method

.method public setIconRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 929
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconRightDp:I

    return-object p0
.end method

.method public setIconTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 914
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconTopDp:I

    return-object p0
.end method

.method public setIconWidthDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 904
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconWidthDp:I

    return-object p0
.end method

.method public setImageBackground(Landroid/graphics/drawable/Drawable;)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1064
    iput-object p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mImageBackground:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public setImageBackgroundColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1059
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mImageBackgroundColor:I

    return-object p0
.end method

.method public setShowDownloadInfo(Z)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1383
    iput-boolean p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mIsShowDownloadInfo:Z

    return-object p0
.end method

.method public setSmartDownloadAppNameBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1314
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameBottomDp:I

    return-object p0
.end method

.method public setSmartDownloadAppNameLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1319
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameLeftDp:I

    return-object p0
.end method

.method public setSmartDownloadAppNameRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1324
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameRightDp:I

    return-object p0
.end method

.method public setSmartDownloadAppNameTextColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1304
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameTextColor:I

    return-object p0
.end method

.method public setSmartDownloadAppNameTextSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1299
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameTextSizeSp:I

    return-object p0
.end method

.method public setSmartDownloadAppNameTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1309
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameTopDp:I

    return-object p0
.end method

.method public setSmartDownloadButtonBackgroundColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1364
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonBackgroundColor:I

    return-object p0
.end method

.method public setSmartDownloadButtonBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1354
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonBottomDp:I

    return-object p0
.end method

.method public setSmartDownloadButtonFontColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1374
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonFontColor:I

    return-object p0
.end method

.method public setSmartDownloadButtonFontSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1369
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonFontSizeSp:I

    return-object p0
.end method

.method public setSmartDownloadButtonFontTypeFace(Landroid/graphics/Typeface;)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1379
    iput-object p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

    return-object p0
.end method

.method public setSmartDownloadButtonForegroundColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1359
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonForegroundColor:I

    return-object p0
.end method

.method public setSmartDownloadButtonHeightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1334
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonHeightDp:I

    return-object p0
.end method

.method public setSmartDownloadButtonLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1339
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonLeftDp:I

    return-object p0
.end method

.method public setSmartDownloadButtonRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1344
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonRightDp:I

    return-object p0
.end method

.method public setSmartDownloadButtonTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1349
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonTopDp:I

    return-object p0
.end method

.method public setSmartDownloadButtonWidthDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1329
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonWidthDp:I

    return-object p0
.end method

.method public setSmartDownloadCompanyBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1194
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyBottomDp:I

    return-object p0
.end method

.method public setSmartDownloadCompanyLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1199
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyLeftDp:I

    return-object p0
.end method

.method public setSmartDownloadCompanyRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1204
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyRightDp:I

    return-object p0
.end method

.method public setSmartDownloadCompanyTextColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1184
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyTextColor:I

    return-object p0
.end method

.method public setSmartDownloadCompanyTextSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1179
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyTextSizeSp:I

    return-object p0
.end method

.method public setSmartDownloadCompanyTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1189
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyTopDp:I

    return-object p0
.end method

.method public setSmartDownloadPermissionBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1284
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionBottomDp:I

    return-object p0
.end method

.method public setSmartDownloadPermissionLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1289
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionLeftDp:I

    return-object p0
.end method

.method public setSmartDownloadPermissionRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1294
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionRightDp:I

    return-object p0
.end method

.method public setSmartDownloadPermissionTextColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1274
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionTextColor:I

    return-object p0
.end method

.method public setSmartDownloadPermissionTextSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1269
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionTextSizeSp:I

    return-object p0
.end method

.method public setSmartDownloadPermissionTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1279
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionTopDp:I

    return-object p0
.end method

.method public setSmartDownloadPrivacyBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1254
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyBottomDp:I

    return-object p0
.end method

.method public setSmartDownloadPrivacyLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1259
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyLeftDp:I

    return-object p0
.end method

.method public setSmartDownloadPrivacyRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1264
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyRightDp:I

    return-object p0
.end method

.method public setSmartDownloadPrivacyTextColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1244
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyTextColor:I

    return-object p0
.end method

.method public setSmartDownloadPrivacyTextSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1239
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyTextSizeSp:I

    return-object p0
.end method

.method public setSmartDownloadPrivacyTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1249
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyTopDp:I

    return-object p0
.end method

.method public setSmartDownloadVersionBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1224
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionBottomDp:I

    return-object p0
.end method

.method public setSmartDownloadVersionLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1229
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionLeftDp:I

    return-object p0
.end method

.method public setSmartDownloadVersionRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1234
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionRightDp:I

    return-object p0
.end method

.method public setSmartDownloadVersionTextColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1214
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionTextColor:I

    return-object p0
.end method

.method public setSmartDownloadVersionTextSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1209
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionTextSizeSp:I

    return-object p0
.end method

.method public setSmartDownloadVersionTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1219
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionTopDp:I

    return-object p0
.end method

.method public setThreePicBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1044
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicBottomDp:I

    return-object p0
.end method

.method public setThreePicHeightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1034
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicHeightDp:I

    return-object p0
.end method

.method public setThreePicLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1049
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicLeftDp:I

    return-object p0
.end method

.method public setThreePicRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1054
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicRightDp:I

    return-object p0
.end method

.method public setThreePicTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1039
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicTopDp:I

    return-object p0
.end method

.method public setThreePicWidthDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1029
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicWidthDp:I

    return-object p0
.end method

.method public setTitleBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 949
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleBottomDp:I

    return-object p0
.end method

.method public setTitleFontColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 959
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleFontColor:I

    return-object p0
.end method

.method public setTitleFontSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 954
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleFontSizeSp:I

    return-object p0
.end method

.method public setTitleFontTypeFace(Landroid/graphics/Typeface;)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 964
    iput-object p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleFontTypeFace:Landroid/graphics/Typeface;

    return-object p0
.end method

.method public setTitleLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 934
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleLeftDp:I

    return-object p0
.end method

.method public setTitleRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 939
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleRightDp:I

    return-object p0
.end method

.method public setTitleTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 944
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleTopDp:I

    return-object p0
.end method

.method public setTwoPicBottomDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1014
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicBottomDp:I

    return-object p0
.end method

.method public setTwoPicHeightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1004
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicHeightDp:I

    return-object p0
.end method

.method public setTwoPicLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1019
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicLeftDp:I

    return-object p0
.end method

.method public setTwoPicRightDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1024
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicRightDp:I

    return-object p0
.end method

.method public setTwoPicTopDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 1009
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicTopDp:I

    return-object p0
.end method

.method public setTwoPicWidthDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;
    .locals 0

    .line 999
    iput p1, p0, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicWidthDp:I

    return-object p0
.end method
