.class Lcom/baidu/mobads/component/FeedPortraitVideoView$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/component/FeedPortraitVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobads/component/FeedPortraitVideoView;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V
    .locals 0

    .line 467
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 471
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object p1

    .line 472
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 p3, 0x0

    if-eqz p2, :cond_0

    return-object p3

    :cond_0
    const-string p2, "playCompletion"

    .line 475
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 476
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 477
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/component/IFeedPortraitListener;->playCompletion()V

    goto/16 :goto_0

    :cond_1
    const-string p2, "playError"

    .line 479
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 480
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 481
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/component/IFeedPortraitListener;->playError()V

    goto/16 :goto_0

    :cond_2
    const-string p2, "playRenderingStart"

    .line 483
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    .line 485
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$100(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    .line 486
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 487
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/component/IFeedPortraitListener;->playRenderingStart()V

    goto :goto_0

    :cond_3
    const-string p2, "playPause"

    .line 489
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    .line 490
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Landroid/widget/ImageView;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 491
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Landroid/widget/ImageView;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 493
    :cond_4
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 494
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/component/IFeedPortraitListener;->playPause()V

    goto :goto_0

    :cond_5
    const-string p2, "playResume"

    .line 496
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 497
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Landroid/widget/ImageView;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 498
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Landroid/widget/ImageView;

    move-result-object p1

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 500
    :cond_6
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 501
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;->a:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-static {p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/component/IFeedPortraitListener;->playResume()V

    :cond_7
    :goto_0
    return-object p3
.end method
