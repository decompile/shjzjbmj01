.class public Lcom/baidu/mobads/component/FeedNativeView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private mAdView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mLoader:Ldalvik/system/DexClassLoader;

.field private mRemoteClassName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 21
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const-string v0, "com.style.widget.RemoteNativeView"

    .line 17
    iput-object v0, p0, Lcom/baidu/mobads/component/FeedNativeView;->mRemoteClassName:Ljava/lang/String;

    .line 22
    invoke-direct {p0, p1}, Lcom/baidu/mobads/component/FeedNativeView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string p2, "com.style.widget.RemoteNativeView"

    .line 17
    iput-object p2, p0, Lcom/baidu/mobads/component/FeedNativeView;->mRemoteClassName:Ljava/lang/String;

    .line 27
    invoke-direct {p0, p1}, Lcom/baidu/mobads/component/FeedNativeView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p2, "com.style.widget.RemoteNativeView"

    .line 17
    iput-object p2, p0, Lcom/baidu/mobads/component/FeedNativeView;->mRemoteClassName:Ljava/lang/String;

    .line 32
    invoke-direct {p0, p1}, Lcom/baidu/mobads/component/FeedNativeView;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 3

    .line 62
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mContext:Landroid/content/Context;

    const/4 p1, 0x1

    .line 63
    new-array v0, p1, [Ljava/lang/Class;

    const-class v1, Landroid/content/Context;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 64
    new-array p1, p1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mContext:Landroid/content/Context;

    aput-object v1, p1, v2

    .line 65
    invoke-static {}, Lcom/baidu/mobads/g/b;->d()Ldalvik/system/DexClassLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mLoader:Ldalvik/system/DexClassLoader;

    .line 66
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mLoader:Ldalvik/system/DexClassLoader;

    if-nez v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/baidu/mobads/r;->a(Landroid/content/Context;)Ldalvik/system/DexClassLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mLoader:Ldalvik/system/DexClassLoader;

    .line 69
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mRemoteClassName:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedNativeView;->mLoader:Ldalvik/system/DexClassLoader;

    .line 70
    invoke-static {v1, v2, v0, p1}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/ClassLoader;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    .line 71
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    if-eqz p1, :cond_1

    .line 72
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/component/FeedNativeView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public changeViewLayoutParams(Ljava/lang/Object;)V
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 55
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mRemoteClassName:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedNativeView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "changeLayoutParams"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v6, Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public getAdContainerHeight()I
    .locals 8

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 98
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedNativeView;->mRemoteClassName:Ljava/lang/String;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    iget-object v4, p0, Lcom/baidu/mobads/component/FeedNativeView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v5, "getAdContainerHeight"

    new-array v6, v1, [Ljava/lang/Class;

    new-array v7, v1, [Ljava/lang/Object;

    invoke-static/range {v2 .. v7}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_0
    return v1
.end method

.method public getAdContainerWidth()I
    .locals 8

    .line 83
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 84
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedNativeView;->mRemoteClassName:Ljava/lang/String;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    iget-object v4, p0, Lcom/baidu/mobads/component/FeedNativeView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v5, "getAdContainerWidth"

    new-array v6, v1, [Ljava/lang/Class;

    new-array v7, v1, [Ljava/lang/Object;

    invoke-static/range {v2 .. v7}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_0
    return v1
.end method

.method public getContainerView()Landroid/widget/RelativeLayout;
    .locals 7

    .line 106
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 107
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mRemoteClassName:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedNativeView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "getAdView"

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public setAdData(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V
    .locals 8

    .line 41
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 42
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedNativeView;->mRemoteClassName:Ljava/lang/String;

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedNativeView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedNativeView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "setAdResponse"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v6, Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
