.class public Lcom/baidu/mobads/component/XNativeViewManager;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static sInstance:Lcom/baidu/mobads/component/XNativeViewManager;


# instance fields
.field private mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/baidu/mobads/component/XNativeView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/baidu/mobads/component/XNativeViewManager;
    .locals 3

    .line 19
    sget-object v0, Lcom/baidu/mobads/component/XNativeViewManager;->sInstance:Lcom/baidu/mobads/component/XNativeViewManager;

    if-nez v0, :cond_1

    .line 20
    const-class v0, Lcom/baidu/mobads/component/XNativeViewManager;

    monitor-enter v0

    .line 21
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/component/XNativeViewManager;->sInstance:Lcom/baidu/mobads/component/XNativeViewManager;

    if-nez v1, :cond_0

    .line 22
    new-instance v1, Lcom/baidu/mobads/component/XNativeViewManager;

    invoke-direct {v1}, Lcom/baidu/mobads/component/XNativeViewManager;-><init>()V

    sput-object v1, Lcom/baidu/mobads/component/XNativeViewManager;->sInstance:Lcom/baidu/mobads/component/XNativeViewManager;

    .line 23
    sget-object v1, Lcom/baidu/mobads/component/XNativeViewManager;->sInstance:Lcom/baidu/mobads/component/XNativeViewManager;

    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, v1, Lcom/baidu/mobads/component/XNativeViewManager;->mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 25
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 27
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobads/component/XNativeViewManager;->sInstance:Lcom/baidu/mobads/component/XNativeViewManager;

    return-object v0
.end method


# virtual methods
.method public addItem(Lcom/baidu/mobads/component/XNativeView;)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeViewManager;->mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeNativeView(Lcom/baidu/mobads/component/XNativeView;)V
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeViewManager;->mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeViewManager;->mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeViewManager;->mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/component/XNativeView;

    if-ne v1, p1, :cond_1

    .line 51
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeViewManager;->mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    :goto_1
    return-void
.end method

.method public resetAllPlayer(Lcom/baidu/mobads/component/XNativeView;)V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeViewManager;->mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeViewManager;->mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeViewManager;->mViewList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/baidu/mobads/component/XNativeView;

    if-eq v1, p1, :cond_1

    .line 40
    invoke-virtual {v1}, Lcom/baidu/mobads/component/XNativeView;->stop()V

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    :goto_1
    return-void
.end method
