.class public Lcom/baidu/mobads/component/FeedPortraitVideoView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/component/FeedPortraitVideoView$a;,
        Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;
    }
.end annotation


# static fields
.field private static final COVER_PIC_ID:I = 0x11

.field private static final LOADING_VIEW_ID:I = 0x12

.field private static final PAUSE_ID:I = 0x10

.field private static final PLAY_END:Ljava/lang/String; = "playCompletion"

.field private static final PLAY_ERROR:Ljava/lang/String; = "playError"

.field private static final PLAY_ICON_STRING:Ljava/lang/String; = "iVBORw0KGgoAAAANSUhEUgAAAJAAAACQCAYAAADnRuK4AAAABHNCSVQICAgIfAhkiAAADT5JREFU\neJztnXtsleUdxz897YFKL1AQKEUFKsjqAqgILotIzBYQdERilqAsMTAuIoMNGEtmt8G0cUuj/GFM\nDDFk/lGRqBXZisCMiGBAWjsuQS4ttFx6A6QX2kLpoefsj985tLQ97bm873ne9z3PJznhWM/lC8+3\nz+33e55fAvGHC0gHBgND/H8OBNxdHgO6PAfw+B/tXZ57gFtAE9Do//M64I3R38MSJKgWYDJuIBMY\nBWQghklDTGQGXsRETUADUAvUIWZzJE4zUCIwEsgCRgPDMc8soeIFrgLVQA1wGehQqshAnGAgNzAO\nGI/0NIlq5fRLB9IzlQPnsXnvZFcDJSA9zATEPElq5UTMbaASKEN6J59aOeFjNwOlAzmIcQYp1mI0\nrcBZ4BQyj7IFdjHQEOBRZJiyi+ZI8SFGOoKs7iyN1RvjXsQ441QLUUQFcBT4UbWQYFjVQMOBqcAD\nqoVYhItAKbKasxRWM9BAYDoyz9H05BRQjGxgWgKrGCgBeAh4AkhWrMXqtAGHkZWb8lWbFQw0DHgS\n2QDUhE4d8C1Qr1KESgO5kHnOI4p12BkfMskuRVEMTlXDpQC/QOJUmuipA75C9pJiiopt//uBucje\njsYYUpE5ZD0x3oSMpYFcwDRgBvYNPViZJGSHPhGJtcVkgh0rAyUDc5CdZI25ZCLZCBeQWJupxMJA\nqcCvkF1lTWxIBcYgJmo384vMNtBQxDxpJn+PpifJwINAFXDTrC8x00CZwLPojUGVuJFpQx3QYsYX\nmGWgMcAzdOYUa9SRiJioHkm1NfzDjWYMMAv1qaSaTlzIcHYNg01ktIEykZ5Hm8d6JCBpMbUYOJwZ\naaChyJxHD1vWxYWY6CIGTayNMlBgqa4nzNYnERiLJKtFvcQ3wkDJ6KW63XAjIaVzRLnZGK2BXMgO\ns94ktB/JSApNOVGEPaI10DR0eMLOpCKdQHWkHxCNge5HAqMae5MJXCHCKH6ky+0U4OkI36uxHk8j\nbRo2kRjIhSSD6RWXc0hG2jRsP0QyhD2O5J1onEUqstlYE86bwnXcMCSHWeNMHkE2hEMmHAMlIKcn\ndAK8cwm7jcMx0EPoozfxQCbS1iERqoEGIof+NPHBE0ib90uoBpqOXnXFE8lIm/dLKAYajj6rHo/k\nIG3fJ6EYaGr0WjQ2pd+2789A96KvWIlnHkC2boLSn4EeNU6Lxqb06YG+DDSE+L0ZTNNJNn0cQ+/L\nQLr30QQIGn0IZqB0dJ6PppMJBMk4DWagHHTIQtNJAkG2cnozUAIOi7a/+eabk8+ePbuqsLBwVmZm\n5gDVemzKBHrpVHrrZe5D7u9xDM3Nzf9ITU0dCtDa2tpQUFDw+cqVK7/r6HBMyYpYsZNu6a+99UCO\n6n0AAuYBSElJyVi+fPmi6urq19auXeu4v6vJ9AiydjdQoHCJ4xk5cuSYt99++48nT55cPmfOnH63\n7DWAeOOug6PdDWTnwiURkZOT81hRUdHGffv2vZCdnX2Paj0WJwk5lHiH7gaKy6W7y+VKmjlz5qwf\nfvghr6CgYOagQYP02f7g3DXsd/2HSkTqbcUtycnJqQsXLnzp0qVLf9u4ceNPVeuxKHfVZOtqoJFY\nv1hbTBg6dOioDRs2rD537tzvFyxYkKVaj8UIVIUE7jbQ6NhrsTbZ2dkPb9269a+HDx9+acqUKamq\n9ViIO79Urt5+qOkkISHBNX369JklJSV527dvn5WRkRFXi4wg9DCQmxCyz+IZt9t9z/PPP//C+fPn\n/75p06Z4T7IbgX85HzBQJpEfc44r0tPT712zZs2yqqqq9cuWLRujWo8iXPjLVARME9err0gYPXr0\n+M2bN//52LFji5566qkM1XoUMAo6DRSP/wBGkDB58uSf7d279/Xdu3fPy8rKiqdAbQZ0GkgXPomC\nxMTEAbNnz362rKws7/333/+52+2Oh1SYwSAGcqGvpzOElJSUwUuWLHm5qqrqtfXr14d8utOmpAMu\nV+CJYjGOYsSIEQ/k5+evO3369IrnnntuhGo9JuEC0lz4uyKN8UycOPGRHTt2bNy/f/+vJ06cOEi1\nHhMY4kLPf0zF5XIlzpgx45fHjh3L27p169MOC9QO1j1QjBg4cGDKiy++uKCqqmpDXl7eJNV6DGKw\nixBvYdAYQ0ZGRmZubu7vKisr/7Bw4UK7xx+TXejSBEoYO3ZsTkFBwV+Ki4t/M3XqVLuugt0uIJ42\nv6yGa9q0aTMOHTqUt2PHjmeGDRtmt0CtW/dAFsDtdifPmzdvfmVl5evvvPPO46r1hIE2kJVIS0sb\ntmrVqqU1NTV/evXVV+1wuMGdALyMwyfSPp9vs2oNkXD8+PHi+fPnb62oqDCt5mmUtOkeyMJMnjx5\n+p49e36rWkcfDHDSppZGAS7Ao1qEpndOnDhRPHv27C2qdfRBuzaQBamtra1YuXLlPydNmrTFwvMf\nAE8SBpQ91BhDc3Nz/QcffPDZ6tWrS1RrCRFPEroHUo7H42nbvXv37kWLFn157dq1qEpQxhhtIMV4\nS0tLDy5fvvzz0tLSZtViIkAbSBUXLlw4lZub+8mHH34YcblJC9CeBNxSrSKeaGhoqHvvvfcKc3Nz\nj6vWYgC3koAm1SrigVu3brVu3769aOnSpd+0tLQ45Wq0piSgUbUKJ+P1ejsOHjz49ZIlS3aeOXPm\nhmo9BtOkeyATKSsrO7Zu3bpPi4qKrqjWYhKNSUi5Zy/6ZIZhXL169dJbb731SX5+/hnVWkzECzQn\nBZ6gc6OjprW1tWnbtm2fr1ix4pDH4/Gp1mMy1wFvIAOuEW2giOno6Gjfu3fvl4sXL95TVVUVL6va\nRui8ULMBiNebJqLBd/z48ZJVq1Z9tn///gbVYmLMXQaqRZfzDovq6upzb7zxxsebN28+r1qLImqh\n00B16Il0SDQ3N/+4ZcuWz9asWVOqWotCvIhn7hjIA1xFl/UOisfjublz585dixcv/qqhocFOAU8z\nuII/BNb1GEk12kA98Pl83pKSkm9feeWVfx85csSOAU8zqAk8Ser2w8dir8W6VFRUnMzNzf1k27Zt\nNf2/Oq7o1UCXgQ70XdHU19fXvvvuu59u2LDhhGotFqQD8Qpwt4E6kJn1fbFWZBXa2tpaCgsL/7Ns\n2bL9N27c8KrWY1FqEa8APQurnCUODeT1em8fOHDg66VLl+4sLy+3cg6yFSjv+h/dDVQJPNnLzx3L\n6dOnj6xdu7Zw165dV1VrsQG3EY/cobtRPP4XOL4Q2+XLly/m5+d/vGnTpvL+X63xU4GY6A699TRl\nOMxALS0t9V1KXjb6S14e0iUvw6bHL1tvK64W4Cc46NqXtLS0q1lZWYP27dt3YO7cuf/66KOPLvh8\nTg+WG04rcLD7D4PdZ/wEMMVUORq7cRQo7v7DYLGvU4D+FdUE8CGe6EEwA11HlvQaDcjcp9cwTl/R\n9yPmaNHYkKPB/kdfBmpElm2a+KaCPk7u9Jf/E9R5mrihz5GoPwP9CFw0TovGZlwErvX1glAyEOM5\n8y7e+b6/F4SSunEDGISuqRpvnARO9/eiUHOgi4G2qORo7EQbENIlV6Ea6BZwOGI5GrvxHSHe2hLO\nKYwy/Jn4GkdTh7R1SIRjIB/wLTrE4WQCbRwy4eY/3/S/R5cJdyZHgHPhvCGSg4Sl6KHMidQB/wv3\nTZEYyAt8hV6VOYk2pE3DPkgQ6REeD1CPwzIX45gvkahD2ERzBuy6//2ZUXyGRj1HCZLrEwrRXqbw\nPXo+ZGfqCCFc0RfRnkL1AReQu4WSo/wsTWxpBL4gynvCjTjGfBsx0YPo2mN2oRUoQuKcUWHUOfh2\noAoYb+BnasyhHTHPdSM+zMjGvomMqePRF1VZlQ5gFxGuuHrD6N6iBVneP0jwI0MaNfiA/yL3QBmG\nGcNNE5LFNg7dE1mFDsQ8hmeXmjVfaUKuARln4ndoQqMdGbZMqQpkZuO2II4fi16dqSKw2jJsztMd\ns3uHm8ixkPvR+0SxphEDV1vBiMXw0o6kCIwEUmPwfRpZDX+BAfs8/RGr+clt5HisCx07M5ujwD5i\nVIkylhNcHzKRu4IMaXFzC1qMaEOi6jG9GEPFCilwccMI9JBmFHXATkycLAdD1RLbgwxpCciQpjcd\nI8OHpKF+g8w1Y44VGm4ocrGnnhuFRx2SAF+vUoQVDASi4yHkZjS93O+bNuTcVshHb8zEKgYKMBCY\nDuSoFmJRTiInRi1T1M5qBgowHJgKPKBaiEW4iGQOxnyS3B9WNVCAYcCjQLZqIYqoQCbJfV6xohKr\nGyjAEKSi4gTsozlSfMgK9Sh93AxmFezWGGnAw0jSWopiLUbTihjnFEEutLQidjNQgAQgC1m5jcO+\nu9q3kWGqHKnBZbt7B+xqoK64kZSRCciZfavnHwXKapUjdUlsXT7TCQbqSiIS9c/yP0agPivSi8T/\navyPQGE/R+A0A3XHjexwjwIygMFAOuaZyovE+hr9j1pkxzgmkXEVON1AveFCJuNDEEMNRna/3b08\nAgVn2hETdH+0Iem7TYhhmongggI7838jQjJwZ8OEVQAAAABJRU5ErkJggg==\n"

.field private static final PLAY_PAUSE:Ljava/lang/String; = "playPause"

.field private static final PLAY_RESUME:Ljava/lang/String; = "playResume"

.field private static final PLAY_START:Ljava/lang/String; = "playRenderingStart"

.field private static final TAG:Ljava/lang/String; = "FeedPortraitVideoView"


# instance fields
.field private mAdView:Landroid/view/View;

.field private mCanShowCoverPic:Z

.field private mCanVideoClick:Z

.field private mCoverPic:Landroid/widget/ImageView;

.field private mCurrentItem:Lcom/baidu/mobad/feeds/NativeResponse;

.field private mFeedVideoListener:Lcom/baidu/mobads/component/IFeedPortraitListener;

.field private mIsVideoFirstPlay:Z

.field private mLoader:Ldalvik/system/DexClassLoader;

.field private mLoadingView:Landroid/view/View;

.field private mPauseBtn:Landroid/widget/ImageView;

.field private mPortraitViewClickListener:Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;

.field private mRemoteClassName:Ljava/lang/String;

.field private mViewContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 130
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 121
    iput-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanVideoClick:Z

    const/4 v0, 0x1

    .line 122
    iput-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mIsVideoFirstPlay:Z

    .line 123
    iput-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanShowCoverPic:Z

    const-string v0, "KsFUVkW3hvh15S0DYx76RpROGGQW+8runZ3lnHDrYi9hfV1Uns7ZF3eKbMRKGO2s"

    .line 124
    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    .line 131
    invoke-direct {p0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 135
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 121
    iput-boolean p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanVideoClick:Z

    const/4 p2, 0x1

    .line 122
    iput-boolean p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mIsVideoFirstPlay:Z

    .line 123
    iput-boolean p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanShowCoverPic:Z

    const-string p2, "KsFUVkW3hvh15S0DYx76RpROGGQW+8runZ3lnHDrYi9hfV1Uns7ZF3eKbMRKGO2s"

    .line 124
    iput-object p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    .line 136
    invoke-direct {p0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 140
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 121
    iput-boolean p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanVideoClick:Z

    const/4 p2, 0x1

    .line 122
    iput-boolean p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mIsVideoFirstPlay:Z

    .line 123
    iput-boolean p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanShowCoverPic:Z

    const-string p2, "KsFUVkW3hvh15S0DYx76RpROGGQW+8runZ3lnHDrYi9hfV1Uns7ZF3eKbMRKGO2s"

    .line 124
    iput-object p2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    .line 141
    invoke-direct {p0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->init(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Lcom/baidu/mobads/component/IFeedPortraitListener;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFeedVideoListener:Lcom/baidu/mobads/component/IFeedPortraitListener;

    return-object p0
.end method

.method static synthetic access$100(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->hideCoverPic()V

    return-void
.end method

.method static synthetic access$200(Lcom/baidu/mobads/component/FeedPortraitVideoView;)Landroid/widget/ImageView;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    return-object p0
.end method

.method private handleCover()V
    .locals 2

    .line 427
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 431
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 433
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 434
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method private hideCoverPic()V
    .locals 2

    const/4 v0, 0x0

    .line 413
    iput-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mIsVideoFirstPlay:Z

    .line 414
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    const/4 v1, 0x4

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 421
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method private hidePauseBtn()V
    .locals 2

    .line 400
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 406
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 407
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 3

    .line 145
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mViewContext:Landroid/content/Context;

    const/4 p1, 0x1

    .line 146
    new-array v0, p1, [Ljava/lang/Class;

    const-class v1, Landroid/content/Context;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 147
    new-array p1, p1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mViewContext:Landroid/content/Context;

    aput-object v1, p1, v2

    .line 148
    invoke-static {}, Lcom/baidu/mobads/g/b;->d()Ldalvik/system/DexClassLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    .line 149
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    if-nez v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mViewContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/baidu/mobads/r;->a(Landroid/content/Context;)Ldalvik/system/DexClassLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    .line 152
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    .line 154
    invoke-static {v1}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    invoke-static {v1, v2, v0, p1}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/ClassLoader;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    .line 156
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz p1, :cond_1

    .line 157
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public getCurrentPosition()J
    .locals 7

    .line 281
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "getCurrentPosition"

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getDuration()J
    .locals 7

    .line 291
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "getDuration"

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public isPlaying()Z
    .locals 8

    .line 271
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v4, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v5, "isPlaying"

    new-array v6, v1, [Ljava/lang/Class;

    new-array v7, v1, [Ljava/lang/Object;

    invoke-static/range {v2 .. v7}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    return v1
.end method

.method public isShowEndFrame()Z
    .locals 8

    .line 311
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v4, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v5, "isShowEndFrame"

    new-array v6, v1, [Ljava/lang/Class;

    new-array v7, v1, [Ljava/lang/Object;

    invoke-static/range {v2 .. v7}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    return v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 440
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const/16 v0, 0x11

    if-ne p1, v0, :cond_4

    .line 441
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPortraitViewClickListener:Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;

    if-eqz p1, :cond_1

    .line 442
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPortraitViewClickListener:Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;

    invoke-interface {p1, p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;->onPortraitViewClick(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    .line 444
    :cond_1
    iget-boolean p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanVideoClick:Z

    if-nez p1, :cond_2

    .line 445
    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->play()V

    return-void

    .line 448
    :cond_2
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCurrentItem:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCurrentItem:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/NativeResponse;->isDownloadApp()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 450
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCurrentItem:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {p1, p0}, Lcom/baidu/mobad/feeds/NativeResponse;->handleClick(Landroid/view/View;)V

    .line 451
    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->play()V

    goto :goto_0

    .line 452
    :cond_3
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCurrentItem:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz p1, :cond_4

    .line 453
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCurrentItem:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {p1, p0}, Lcom/baidu/mobad/feeds/NativeResponse;->handleClick(Landroid/view/View;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public pause()V
    .locals 7

    .line 235
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "pause"

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public play()V
    .locals 7

    .line 224
    iget-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mIsVideoFirstPlay:Z

    if-eqz v0, :cond_0

    .line 225
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->hidePauseBtn()V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "play"

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public resume()V
    .locals 7

    .line 243
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "resume"

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setAdData(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V
    .locals 8

    if-nez p1, :cond_0

    return-void

    .line 189
    :cond_0
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCurrentItem:Lcom/baidu/mobad/feeds/NativeResponse;

    .line 190
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v4, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v5, "setAdData"

    new-array v6, v1, [Ljava/lang/Class;

    const-class v0, Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    new-array v0, v1, [Ljava/lang/Object;

    aput-object p1, v0, v7

    move-object v7, v0

    invoke-static/range {v2 .. v7}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    :cond_1
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->showNormalPic(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V

    .line 196
    iput-boolean v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mIsVideoFirstPlay:Z

    .line 197
    iget-boolean v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanShowCoverPic:Z

    if-eqz v0, :cond_2

    .line 198
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->showView(Lcom/baidu/mobad/feeds/NativeResponse;)V

    :cond_2
    return-void
.end method

.method public setCanClickVideo(Z)V
    .locals 8

    .line 301
    iput-boolean p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanVideoClick:Z

    .line 302
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "setCanClickVideo"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    .line 305
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v6, v7

    .line 303
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setCanShowCoverImage(Z)V
    .locals 0

    .line 208
    iput-boolean p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCanShowCoverPic:Z

    return-void
.end method

.method public setFeedPortraitListener(Lcom/baidu/mobads/component/IFeedPortraitListener;)V
    .locals 10

    .line 163
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mFeedVideoListener:Lcom/baidu/mobads/component/IFeedPortraitListener;

    :try_start_0
    const-string p1, "com.component.feed.IFeedPortraitListener"

    .line 166
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    .line 167
    invoke-static {p1, v0}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object p1

    .line 168
    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    new-instance v4, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;

    invoke-direct {v4, p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView$a;-><init>(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V

    invoke-static {v0, v2, v4}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    .line 170
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 171
    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v2}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v6, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v7, "setFeedPortraitListener"

    new-array v8, v1, [Ljava/lang/Class;

    aput-object p1, v8, v3

    new-array v9, v1, [Ljava/lang/Object;

    aput-object v0, v9, v3

    invoke-static/range {v4 .. v9}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public setOnPortraitViewClickListener(Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;)V
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPortraitViewClickListener:Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;

    return-void
.end method

.method public setProgressBackgroundColor(I)V
    .locals 8

    .line 327
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "setProgressBackgroundColor"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    .line 329
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v7

    .line 328
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setProgressBarColor(I)V
    .locals 8

    .line 334
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "setProgressBarColor"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    .line 336
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v7

    .line 335
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setProgressHeightInDp(I)V
    .locals 8

    .line 341
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "setProgressHeightDp"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    .line 343
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v7

    .line 342
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setShowProgress(Z)V
    .locals 8

    .line 320
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "setShowProgressBar"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    .line 322
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v6, v7

    .line 321
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setVideoMute(Z)V
    .locals 8

    .line 262
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "setVideoMute"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    .line 265
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v6, v7

    .line 263
    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method protected showNormalPic(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V
    .locals 8

    if-nez p1, :cond_0

    return-void

    .line 215
    :cond_0
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCurrentItem:Lcom/baidu/mobad/feeds/NativeResponse;

    .line 216
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "showNormalPic"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v6, Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    new-array v6, v0, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method protected showView(Lcom/baidu/mobad/feeds/NativeResponse;)V
    .locals 3

    .line 353
    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCurrentItem:Lcom/baidu/mobad/feeds/NativeResponse;

    .line 355
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 356
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    .line 358
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 359
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 360
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 367
    invoke-static {}, Lcom/baidu/mobads/c/a;->a()Lcom/baidu/mobads/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mCoverPic:Landroid/widget/ImageView;

    invoke-interface {p1}, Lcom/baidu/mobad/feeds/NativeResponse;->getImageUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 370
    :cond_1
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    const/16 v0, 0xd

    if-nez p1, :cond_2

    .line 371
    new-instance p1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    .line 372
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    .line 373
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getBitmapUtils()Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;

    move-result-object v1

    const-string v2, "iVBORw0KGgoAAAANSUhEUgAAAJAAAACQCAYAAADnRuK4AAAABHNCSVQICAgIfAhkiAAADT5JREFU\neJztnXtsleUdxz897YFKL1AQKEUFKsjqAqgILotIzBYQdERilqAsMTAuIoMNGEtmt8G0cUuj/GFM\nDDFk/lGRqBXZisCMiGBAWjsuQS4ttFx6A6QX2kLpoefsj985tLQ97bm873ne9z3PJznhWM/lC8+3\nz+33e55fAvGHC0gHBgND/H8OBNxdHgO6PAfw+B/tXZ57gFtAE9Do//M64I3R38MSJKgWYDJuIBMY\nBWQghklDTGQGXsRETUADUAvUIWZzJE4zUCIwEsgCRgPDMc8soeIFrgLVQA1wGehQqshAnGAgNzAO\nGI/0NIlq5fRLB9IzlQPnsXnvZFcDJSA9zATEPElq5UTMbaASKEN6J59aOeFjNwOlAzmIcQYp1mI0\nrcBZ4BQyj7IFdjHQEOBRZJiyi+ZI8SFGOoKs7iyN1RvjXsQ441QLUUQFcBT4UbWQYFjVQMOBqcAD\nqoVYhItAKbKasxRWM9BAYDoyz9H05BRQjGxgWgKrGCgBeAh4AkhWrMXqtAGHkZWb8lWbFQw0DHgS\n2QDUhE4d8C1Qr1KESgO5kHnOI4p12BkfMskuRVEMTlXDpQC/QOJUmuipA75C9pJiiopt//uBucje\njsYYUpE5ZD0x3oSMpYFcwDRgBvYNPViZJGSHPhGJtcVkgh0rAyUDc5CdZI25ZCLZCBeQWJupxMJA\nqcCvkF1lTWxIBcYgJmo384vMNtBQxDxpJn+PpifJwINAFXDTrC8x00CZwLPojUGVuJFpQx3QYsYX\nmGWgMcAzdOYUa9SRiJioHkm1NfzDjWYMMAv1qaSaTlzIcHYNg01ktIEykZ5Hm8d6JCBpMbUYOJwZ\naaChyJxHD1vWxYWY6CIGTayNMlBgqa4nzNYnERiLJKtFvcQ3wkDJ6KW63XAjIaVzRLnZGK2BXMgO\ns94ktB/JSApNOVGEPaI10DR0eMLOpCKdQHWkHxCNge5HAqMae5MJXCHCKH6ky+0U4OkI36uxHk8j\nbRo2kRjIhSSD6RWXc0hG2jRsP0QyhD2O5J1onEUqstlYE86bwnXcMCSHWeNMHkE2hEMmHAMlIKcn\ndAK8cwm7jcMx0EPoozfxQCbS1iERqoEGIof+NPHBE0ib90uoBpqOXnXFE8lIm/dLKAYajj6rHo/k\nIG3fJ6EYaGr0WjQ2pd+2789A96KvWIlnHkC2boLSn4EeNU6Lxqb06YG+DDSE+L0ZTNNJNn0cQ+/L\nQLr30QQIGn0IZqB0dJ6PppMJBMk4DWagHHTIQtNJAkG2cnozUAIOi7a/+eabk8+ePbuqsLBwVmZm\n5gDVemzKBHrpVHrrZe5D7u9xDM3Nzf9ITU0dCtDa2tpQUFDw+cqVK7/r6HBMyYpYsZNu6a+99UCO\n6n0AAuYBSElJyVi+fPmi6urq19auXeu4v6vJ9AiydjdQoHCJ4xk5cuSYt99++48nT55cPmfOnH63\n7DWAeOOug6PdDWTnwiURkZOT81hRUdHGffv2vZCdnX2Paj0WJwk5lHiH7gaKy6W7y+VKmjlz5qwf\nfvghr6CgYOagQYP02f7g3DXsd/2HSkTqbcUtycnJqQsXLnzp0qVLf9u4ceNPVeuxKHfVZOtqoJFY\nv1hbTBg6dOioDRs2rD537tzvFyxYkKVaj8UIVIUE7jbQ6NhrsTbZ2dkPb9269a+HDx9+acqUKamq\n9ViIO79Urt5+qOkkISHBNX369JklJSV527dvn5WRkRFXi4wg9DCQmxCyz+IZt9t9z/PPP//C+fPn\n/75p06Z4T7IbgX85HzBQJpEfc44r0tPT712zZs2yqqqq9cuWLRujWo8iXPjLVARME9err0gYPXr0\n+M2bN//52LFji5566qkM1XoUMAo6DRSP/wBGkDB58uSf7d279/Xdu3fPy8rKiqdAbQZ0GkgXPomC\nxMTEAbNnz362rKws7/333/+52+2Oh1SYwSAGcqGvpzOElJSUwUuWLHm5qqrqtfXr14d8utOmpAMu\nV+CJYjGOYsSIEQ/k5+evO3369IrnnntuhGo9JuEC0lz4uyKN8UycOPGRHTt2bNy/f/+vJ06cOEi1\nHhMY4kLPf0zF5XIlzpgx45fHjh3L27p169MOC9QO1j1QjBg4cGDKiy++uKCqqmpDXl7eJNV6DGKw\nixBvYdAYQ0ZGRmZubu7vKisr/7Bw4UK7xx+TXejSBEoYO3ZsTkFBwV+Ki4t/M3XqVLuugt0uIJ42\nv6yGa9q0aTMOHTqUt2PHjmeGDRtmt0CtW/dAFsDtdifPmzdvfmVl5evvvPPO46r1hIE2kJVIS0sb\ntmrVqqU1NTV/evXVV+1wuMGdALyMwyfSPp9vs2oNkXD8+PHi+fPnb62oqDCt5mmUtOkeyMJMnjx5\n+p49e36rWkcfDHDSppZGAS7Ao1qEpndOnDhRPHv27C2qdfRBuzaQBamtra1YuXLlPydNmrTFwvMf\nAE8SBpQ91BhDc3Nz/QcffPDZ6tWrS1RrCRFPEroHUo7H42nbvXv37kWLFn157dq1qEpQxhhtIMV4\nS0tLDy5fvvzz0tLSZtViIkAbSBUXLlw4lZub+8mHH34YcblJC9CeBNxSrSKeaGhoqHvvvfcKc3Nz\nj6vWYgC3koAm1SrigVu3brVu3769aOnSpd+0tLQ45Wq0piSgUbUKJ+P1ejsOHjz49ZIlS3aeOXPm\nhmo9BtOkeyATKSsrO7Zu3bpPi4qKrqjWYhKNSUi5Zy/6ZIZhXL169dJbb731SX5+/hnVWkzECzQn\nBZ6gc6OjprW1tWnbtm2fr1ix4pDH4/Gp1mMy1wFvIAOuEW2giOno6Gjfu3fvl4sXL95TVVUVL6va\nRui8ULMBiNebJqLBd/z48ZJVq1Z9tn///gbVYmLMXQaqRZfzDovq6upzb7zxxsebN28+r1qLImqh\n00B16Il0SDQ3N/+4ZcuWz9asWVOqWotCvIhn7hjIA1xFl/UOisfjublz585dixcv/qqhocFOAU8z\nuII/BNb1GEk12kA98Pl83pKSkm9feeWVfx85csSOAU8zqAk8Ser2w8dir8W6VFRUnMzNzf1k27Zt\nNf2/Oq7o1UCXgQ70XdHU19fXvvvuu59u2LDhhGotFqQD8Qpwt4E6kJn1fbFWZBXa2tpaCgsL/7Ns\n2bL9N27c8KrWY1FqEa8APQurnCUODeT1em8fOHDg66VLl+4sLy+3cg6yFSjv+h/dDVQJPNnLzx3L\n6dOnj6xdu7Zw165dV1VrsQG3EY/cobtRPP4XOL4Q2+XLly/m5+d/vGnTpvL+X63xU4GY6A699TRl\nOMxALS0t9V1KXjb6S14e0iUvw6bHL1tvK64W4Cc46NqXtLS0q1lZWYP27dt3YO7cuf/66KOPLvh8\nTg+WG04rcLD7D4PdZ/wEMMVUORq7cRQo7v7DYLGvU4D+FdUE8CGe6EEwA11HlvQaDcjcp9cwTl/R\n9yPmaNHYkKPB/kdfBmpElm2a+KaCPk7u9Jf/E9R5mrihz5GoPwP9CFw0TovGZlwErvX1glAyEOM5\n8y7e+b6/F4SSunEDGISuqRpvnARO9/eiUHOgi4G2qORo7EQbENIlV6Ea6BZwOGI5GrvxHSHe2hLO\nKYwy/Jn4GkdTh7R1SIRjIB/wLTrE4WQCbRwy4eY/3/S/R5cJdyZHgHPhvCGSg4Sl6KHMidQB/wv3\nTZEYyAt8hV6VOYk2pE3DPkgQ6REeD1CPwzIX45gvkahD2ERzBuy6//2ZUXyGRj1HCZLrEwrRXqbw\nPXo+ZGfqCCFc0RfRnkL1AReQu4WSo/wsTWxpBL4gynvCjTjGfBsx0YPo2mN2oRUoQuKcUWHUOfh2\noAoYb+BnasyhHTHPdSM+zMjGvomMqePRF1VZlQ5gFxGuuHrD6N6iBVneP0jwI0MaNfiA/yL3QBmG\nGcNNE5LFNg7dE1mFDsQ8hmeXmjVfaUKuARln4ndoQqMdGbZMqQpkZuO2II4fi16dqSKw2jJsztMd\ns3uHm8ixkPvR+0SxphEDV1vBiMXw0o6kCIwEUmPwfRpZDX+BAfs8/RGr+clt5HisCx07M5ujwD5i\nVIkylhNcHzKRu4IMaXFzC1qMaEOi6jG9GEPFCilwccMI9JBmFHXATkycLAdD1RLbgwxpCciQpjcd\nI8OHpKF+g8w1Y44VGm4ocrGnnhuFRx2SAF+vUoQVDASi4yHkZjS93O+bNuTcVshHb8zEKgYKMBCY\nDuSoFmJRTiInRi1T1M5qBgowHJgKPKBaiEW4iGQOxnyS3B9WNVCAYcCjQLZqIYqoQCbJfV6xohKr\nGyjAEKSi4gTsozlSfMgK9Sh93AxmFezWGGnAw0jSWopiLUbTihjnFEEutLQidjNQgAQgC1m5jcO+\nu9q3kWGqHKnBZbt7B+xqoK64kZSRCciZfavnHwXKapUjdUlsXT7TCQbqSiIS9c/yP0agPivSi8T/\navyPQGE/R+A0A3XHjexwjwIygMFAOuaZyovE+hr9j1pkxzgmkXEVON1AveFCJuNDEEMNRna/3b08\nAgVn2hETdH+0Iem7TYhhmongggI7838jQjJwZ8OEVQAAAABJRU5ErkJggg==\n"

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/utils/IXAdBitmapUtils;->string2bitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 372
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 374
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 375
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    const/16 v1, 0x10

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 377
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 379
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0x78

    invoke-direct {p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 380
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 381
    iget-object v1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mPauseBtn:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 384
    :cond_2
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    if-nez p1, :cond_3

    .line 385
    new-instance p1, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    .line 386
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 389
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 390
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 391
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 392
    iget-object p1, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoadingView:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->addView(Landroid/view/View;)V

    .line 395
    :cond_3
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->handleCover()V

    return-void
.end method

.method public stop()V
    .locals 7

    .line 252
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mRemoteClassName:Ljava/lang/String;

    invoke-static {v0}, Lcom/baidu/mobads/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mAdView:Landroid/view/View;

    iget-object v3, p0, Lcom/baidu/mobads/component/FeedPortraitVideoView;->mLoader:Ldalvik/system/DexClassLoader;

    const-string v4, "stop"

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static/range {v1 .. v6}, Lcom/baidu/mobads/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    :cond_0
    invoke-direct {p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->handleCover()V

    return-void
.end method
