.class Lcom/baidu/mobads/component/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/component/IFeedPortraitListener;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/component/XNativeView;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/component/XNativeView;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public playCompletion()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/component/INativeVideoListener;->onCompletion()V

    :cond_0
    return-void
.end method

.method public playError()V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/component/INativeVideoListener;->onError()V

    :cond_0
    return-void
.end method

.method public playPause()V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/component/INativeVideoListener;->onPause()V

    :cond_0
    return-void
.end method

.method public playRenderingStart()V
    .locals 2

    .line 120
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/XNativeViewManager;->resetAllPlayer(Lcom/baidu/mobads/component/XNativeView;)V

    .line 121
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/component/INativeVideoListener;->onRenderingStart()V

    :cond_0
    return-void
.end method

.method public playResume()V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/baidu/mobads/component/a;->a:Lcom/baidu/mobads/component/XNativeView;

    invoke-static {v0}, Lcom/baidu/mobads/component/XNativeView;->access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/component/INativeVideoListener;->onResume()V

    :cond_0
    return-void
.end method
