.class public Lcom/baidu/mobads/component/XNativeView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "XNativeView"


# instance fields
.field private mAdLogger:Lcom/baidu/mobads/utils/q;

.field private mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

.field private mFeedVideoListener:Lcom/baidu/mobads/component/INativeVideoListener;

.field private mNativeViewListener:Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;

.field private mProgressBg:I

.field private mProgressColor:I

.field private mProgressHeight:I

.field private mShowProgressBar:Z

.field private mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

.field private videoMute:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 38
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 27
    iput-boolean p1, p0, Lcom/baidu/mobads/component/XNativeView;->videoMute:Z

    const/4 v0, -0x1

    .line 29
    iput v0, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressColor:I

    const/high16 v0, -0x1000000

    .line 30
    iput v0, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressBg:I

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/baidu/mobads/component/XNativeView;->mShowProgressBar:Z

    .line 32
    iput p1, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressHeight:I

    .line 34
    new-instance p1, Lcom/baidu/mobads/utils/q;

    invoke-direct {p1}, Lcom/baidu/mobads/utils/q;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mAdLogger:Lcom/baidu/mobads/utils/q;

    const-string p1, "#000000"

    .line 39
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/XNativeView;->setBackgroundColor(I)V

    .line 40
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->addItem(Lcom/baidu/mobads/component/XNativeView;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    .line 27
    iput-boolean p1, p0, Lcom/baidu/mobads/component/XNativeView;->videoMute:Z

    const/4 p2, -0x1

    .line 29
    iput p2, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressColor:I

    const/high16 p2, -0x1000000

    .line 30
    iput p2, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressBg:I

    const/4 p2, 0x0

    .line 31
    iput-boolean p2, p0, Lcom/baidu/mobads/component/XNativeView;->mShowProgressBar:Z

    .line 32
    iput p1, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressHeight:I

    .line 34
    new-instance p1, Lcom/baidu/mobads/utils/q;

    invoke-direct {p1}, Lcom/baidu/mobads/utils/q;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mAdLogger:Lcom/baidu/mobads/utils/q;

    const-string p1, "#000000"

    .line 45
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/XNativeView;->setBackgroundColor(I)V

    .line 46
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->addItem(Lcom/baidu/mobads/component/XNativeView;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x1

    .line 27
    iput-boolean p1, p0, Lcom/baidu/mobads/component/XNativeView;->videoMute:Z

    const/4 p2, -0x1

    .line 29
    iput p2, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressColor:I

    const/high16 p2, -0x1000000

    .line 30
    iput p2, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressBg:I

    const/4 p2, 0x0

    .line 31
    iput-boolean p2, p0, Lcom/baidu/mobads/component/XNativeView;->mShowProgressBar:Z

    .line 32
    iput p1, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressHeight:I

    .line 34
    new-instance p1, Lcom/baidu/mobads/utils/q;

    invoke-direct {p1}, Lcom/baidu/mobads/utils/q;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mAdLogger:Lcom/baidu/mobads/utils/q;

    const-string p1, "#000000"

    .line 51
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/component/XNativeView;->setBackgroundColor(I)V

    .line 52
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->addItem(Lcom/baidu/mobads/component/XNativeView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/baidu/mobads/component/XNativeView;)Lcom/baidu/mobads/component/INativeVideoListener;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/baidu/mobads/component/XNativeView;->mFeedVideoListener:Lcom/baidu/mobads/component/INativeVideoListener;

    return-object p0
.end method

.method private initAdVideoView()V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-nez v0, :cond_0

    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setCanClickVideo(Z)V

    .line 234
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setAdData(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V

    .line 235
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-boolean v1, p0, Lcom/baidu/mobads/component/XNativeView;->videoMute:Z

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setVideoMute(Z)V

    return-void
.end method

.method private isVisible(Landroid/view/View;I)Z
    .locals 8

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 274
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    .line 275
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 278
    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 280
    invoke-virtual {p1, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_1

    return v0

    .line 285
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-long v2, v2

    .line 286
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-long v4, v1

    mul-long v2, v2, v4

    .line 287
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    int-to-long v6, p1

    mul-long v4, v4, v6

    const-wide/16 v6, 0x0

    cmp-long p1, v4, v6

    if-gtz p1, :cond_2

    return v0

    :cond_2
    const-wide/16 v6, 0x64

    mul-long v2, v2, v6

    int-to-long p1, p2

    mul-long p1, p1, v4

    cmp-long v1, v2, p1

    if-ltz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0

    :cond_4
    :goto_0
    return v0
.end method

.method private play()V
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 181
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->initAdVideoView()V

    .line 182
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->play()V

    :cond_0
    return-void
.end method

.method private renderView()V
    .locals 4

    .line 87
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    if-nez v0, :cond_0

    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-nez v1, :cond_1

    .line 92
    new-instance v1, Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    .line 93
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-boolean v2, p0, Lcom/baidu/mobads/component/XNativeView;->mShowProgressBar:Z

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setShowProgress(Z)V

    .line 94
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget v2, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressColor:I

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setProgressBarColor(I)V

    .line 95
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget v2, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressBg:I

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setProgressBackgroundColor(I)V

    .line 96
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget v2, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressHeight:I

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setProgressHeightInDp(I)V

    .line 97
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setCanClickVideo(Z)V

    .line 99
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setCanShowCoverImage(Z)V

    .line 100
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lcom/baidu/mobads/component/XNativeView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 102
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    new-instance v2, Lcom/baidu/mobads/component/a;

    invoke-direct {v2, p0}, Lcom/baidu/mobads/component/a;-><init>(Lcom/baidu/mobads/component/XNativeView;)V

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setFeedPortraitListener(Lcom/baidu/mobads/component/IFeedPortraitListener;)V

    .line 140
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mNativeViewListener:Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;

    if-eqz v1, :cond_1

    .line 141
    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v1, p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setOnPortraitViewClickListener(Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;)V

    .line 144
    :cond_1
    sget-object v1, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->NORMAL:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    if-ne v0, v1, :cond_2

    .line 145
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->showNormalPic(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V

    goto :goto_0

    .line 146
    :cond_2
    sget-object v1, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    if-ne v0, v1, :cond_3

    .line 148
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-object v1, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->showView(Lcom/baidu/mobad/feeds/NativeResponse;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private shouldAutoPlay()Z
    .locals 6

    .line 192
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->isAutoPlay()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 193
    :goto_0
    iget-object v3, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {v3}, Lcom/baidu/mobad/feeds/XAdNativeResponse;->isNonWifiAutoPlay()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v3, 0x1

    .line 195
    :goto_2
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v4

    invoke-virtual {v4}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getSystemUtils()Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;

    move-result-object v4

    .line 196
    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/baidu/mobads/interfaces/utils/IXAdSystemUtils;->isWifiConnected(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v4

    if-eqz v0, :cond_3

    .line 197
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :cond_5
    :goto_3
    return v1
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .line 247
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 248
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->addItem(Lcom/baidu/mobads/component/XNativeView;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 241
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 242
    invoke-static {}, Lcom/baidu/mobads/component/XNativeViewManager;->getInstance()Lcom/baidu/mobads/component/XNativeViewManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/component/XNativeViewManager;->removeNativeView(Lcom/baidu/mobads/component/XNativeView;)V

    return-void
.end method

.method public onPortraitViewClick(Lcom/baidu/mobads/component/FeedPortraitVideoView;)V
    .locals 0

    .line 298
    iget-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mNativeViewListener:Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;

    if-eqz p1, :cond_0

    .line 299
    iget-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mNativeViewListener:Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;

    invoke-interface {p1, p0}, Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;->onNativeViewClick(Lcom/baidu/mobads/component/XNativeView;)V

    :cond_0
    return-void
.end method

.method public onScroll()V
    .locals 1

    const/16 v0, 0x32

    .line 260
    invoke-direct {p0, p0, v0}, Lcom/baidu/mobads/component/XNativeView;->isVisible(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->pause()V

    :cond_0
    return-void
.end method

.method public onScrollStateChanged(I)V
    .locals 0

    if-nez p1, :cond_0

    .line 254
    invoke-virtual {p0}, Lcom/baidu/mobads/component/XNativeView;->render()V

    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->pause()V

    :cond_0
    return-void
.end method

.method public render()V
    .locals 1

    .line 166
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->shouldAutoPlay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->play()V

    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->resume()V

    :cond_0
    return-void
.end method

.method public setNativeItem(Lcom/baidu/mobad/feeds/NativeResponse;)V
    .locals 0

    .line 62
    check-cast p1, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    .line 63
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->renderView()V

    return-void
.end method

.method public setNativeItem(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mCurrentNativeItem:Lcom/baidu/mobad/feeds/XAdNativeResponse;

    .line 58
    invoke-direct {p0}, Lcom/baidu/mobads/component/XNativeView;->renderView()V

    return-void
.end method

.method public setNativeVideoListener(Lcom/baidu/mobads/component/INativeVideoListener;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mFeedVideoListener:Lcom/baidu/mobads/component/INativeVideoListener;

    return-void
.end method

.method public setNativeViewClickListener(Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mNativeViewListener:Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;

    .line 68
    iget-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz p1, :cond_0

    .line 69
    iget-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {p1, p0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setOnPortraitViewClickListener(Lcom/baidu/mobads/component/FeedPortraitVideoView$IPortraitVideoViewClickListener;)V

    :cond_0
    return-void
.end method

.method public setProgressBackgroundColor(I)V
    .locals 1

    .line 208
    iput p1, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressBg:I

    .line 209
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setProgressBackgroundColor(I)V

    :cond_0
    return-void
.end method

.method public setProgressBarColor(I)V
    .locals 1

    .line 215
    iput p1, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressColor:I

    .line 216
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setProgressBarColor(I)V

    :cond_0
    return-void
.end method

.method public setProgressHeightInDp(I)V
    .locals 1

    .line 222
    iput p1, p0, Lcom/baidu/mobads/component/XNativeView;->mProgressHeight:I

    .line 223
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setProgressHeightInDp(I)V

    :cond_0
    return-void
.end method

.method public setShowProgress(Z)V
    .locals 1

    .line 201
    iput-boolean p1, p0, Lcom/baidu/mobads/component/XNativeView;->mShowProgressBar:Z

    .line 202
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setShowProgress(Z)V

    :cond_0
    return-void
.end method

.method public setVideoMute(Z)V
    .locals 1

    .line 79
    iput-boolean p1, p0, Lcom/baidu/mobads/component/XNativeView;->videoMute:Z

    .line 80
    iget-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz p1, :cond_0

    .line 81
    iget-object p1, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    iget-boolean v0, p0, Lcom/baidu/mobads/component/XNativeView;->videoMute:Z

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->setVideoMute(Z)V

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/baidu/mobads/component/XNativeView;->mVideoView:Lcom/baidu/mobads/component/FeedPortraitVideoView;

    invoke-virtual {v0}, Lcom/baidu/mobads/component/FeedPortraitVideoView;->stop()V

    :cond_0
    return-void
.end method
