.class public Lcom/baidu/mobads/component/StyleParams;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/component/StyleParams$Builder;
    }
.end annotation


# instance fields
.field public mBrandBottomDp:I

.field public mBrandFontColor:I

.field public mBrandFontSizeSp:I

.field public mBrandFontTypeFace:Landroid/graphics/Typeface;

.field public mBrandLeftDp:I

.field public mBrandRightDp:I

.field public mButtonBackgroundColor:I

.field public mButtonBottomDp:I

.field public mButtonFontColor:I

.field public mButtonFontSizeSp:I

.field public mButtonFontTypeFace:Landroid/graphics/Typeface;

.field public mButtonForegroundColor:I

.field public mButtonHeightDp:I

.field public mButtonLeftDp:I

.field public mButtonRightDp:I

.field public mButtonTopDp:I

.field public mButtonWidthDp:I

.field public mFirstPicBottomDp:I

.field public mFirstPicHeightDp:I

.field public mFirstPicLeftDp:I

.field public mFirstPicRightDp:I

.field public mFirstPicTopDp:I

.field public mFirstPicWidthDp:I

.field public mIconBottomDp:I

.field public mIconHeightDp:I

.field public mIconLeftDp:I

.field public mIconRightDp:I

.field public mIconTopDp:I

.field public mIconWidthDp:I

.field public mImageBackground:Landroid/graphics/drawable/Drawable;

.field public mImageBackgroundColor:I

.field public mIsShowDownloadInfo:Z

.field public mSmartDownloadAppNameBottomDp:I

.field public mSmartDownloadAppNameLeftDp:I

.field public mSmartDownloadAppNameRightDp:I

.field public mSmartDownloadAppNameTextColor:I

.field public mSmartDownloadAppNameTextSizeSp:I

.field public mSmartDownloadAppNameTopDp:I

.field public mSmartDownloadButtonBackgroundColor:I

.field public mSmartDownloadButtonBottomDp:I

.field public mSmartDownloadButtonFontColor:I

.field public mSmartDownloadButtonFontSizeSp:I

.field public mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

.field public mSmartDownloadButtonForegroundColor:I

.field public mSmartDownloadButtonHeightDp:I

.field public mSmartDownloadButtonLeftDp:I

.field public mSmartDownloadButtonRightDp:I

.field public mSmartDownloadButtonTopDp:I

.field public mSmartDownloadButtonWidthDp:I

.field public mSmartDownloadCompanyBottomDp:I

.field public mSmartDownloadCompanyLeftDp:I

.field public mSmartDownloadCompanyRightDp:I

.field public mSmartDownloadCompanyTextColor:I

.field public mSmartDownloadCompanyTextSizeSp:I

.field public mSmartDownloadCompanyTopDp:I

.field public mSmartDownloadPermissionBottomDp:I

.field public mSmartDownloadPermissionLeftDp:I

.field public mSmartDownloadPermissionRightDp:I

.field public mSmartDownloadPermissionTextColor:I

.field public mSmartDownloadPermissionTextSizeSp:I

.field public mSmartDownloadPermissionTopDp:I

.field public mSmartDownloadPrivacyBottomDp:I

.field public mSmartDownloadPrivacyLeftDp:I

.field public mSmartDownloadPrivacyRightDp:I

.field public mSmartDownloadPrivacyTextColor:I

.field public mSmartDownloadPrivacyTextSizeSp:I

.field public mSmartDownloadPrivacyTopDp:I

.field public mSmartDownloadVersionBottomDp:I

.field public mSmartDownloadVersionLeftDp:I

.field public mSmartDownloadVersionRightDp:I

.field public mSmartDownloadVersionTextColor:I

.field public mSmartDownloadVersionTextSizeSp:I

.field public mSmartDownloadVersionTopDp:I

.field public mSmartDownloadViewBackgroundColor:I

.field public mSmartDownloadViewBottomDp:I

.field public mSmartDownloadViewLeftDp:I

.field public mSmartDownloadViewRightDp:I

.field public mSmartDownloadViewTopDp:I

.field public mThreePicBottomDp:I

.field public mThreePicHeightDp:I

.field public mThreePicLeftDp:I

.field public mThreePicRightDp:I

.field public mThreePicTopDp:I

.field public mThreePicWidthDp:I

.field public mTitleBottomDp:I

.field public mTitleFontColor:I

.field public mTitleFontSizeSp:I

.field public mTitleFontTypeFace:Landroid/graphics/Typeface;

.field public mTitleLeftDp:I

.field public mTitleRightDp:I

.field public mTitleTopDp:I

.field public mTwoPicBottomDp:I

.field public mTwoPicHeightDp:I

.field public mTwoPicLeftDp:I

.field public mTwoPicRightDp:I

.field public mTwoPicTopDp:I

.field public mTwoPicWidthDp:I


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/component/StyleParams$Builder;)V
    .locals 1

    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewTopDp:I

    .line 224
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewBottomDp:I

    .line 225
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewLeftDp:I

    .line 226
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewRightDp:I

    .line 228
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadViewBackgroundColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewBackgroundColor:I

    .line 231
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyTextSizeSp:I

    .line 232
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyTextColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyTextColor:I

    .line 233
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyTopDp:I

    .line 234
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyBottomDp:I

    .line 235
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyLeftDp:I

    .line 236
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadCompanyRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyRightDp:I

    .line 239
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionTextSizeSp:I

    .line 240
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionTextColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionTextColor:I

    .line 241
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionTopDp:I

    .line 242
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionBottomDp:I

    .line 243
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionLeftDp:I

    .line 244
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadVersionRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionRightDp:I

    .line 247
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionTextSizeSp:I

    .line 248
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionTextColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionTextColor:I

    .line 249
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionTopDp:I

    .line 250
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionBottomDp:I

    .line 251
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionLeftDp:I

    .line 252
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPermissionRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionRightDp:I

    .line 255
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyTextSizeSp:I

    .line 256
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyTextColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyTextColor:I

    .line 257
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyTopDp:I

    .line 258
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyBottomDp:I

    .line 259
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyLeftDp:I

    .line 260
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadPrivacyRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyRightDp:I

    .line 263
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameTextSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameTextSizeSp:I

    .line 264
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameTextColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameTextColor:I

    .line 265
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameTopDp:I

    .line 266
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameBottomDp:I

    .line 267
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameLeftDp:I

    .line 268
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadAppNameRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameRightDp:I

    .line 271
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonWidthDp:I

    .line 272
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonHeightDp:I

    .line 273
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonLeftDp:I

    .line 274
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonRightDp:I

    .line 275
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonTopDp:I

    .line 276
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonBottomDp:I

    .line 277
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonForegroundColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonForegroundColor:I

    .line 278
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonBackgroundColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonBackgroundColor:I

    .line 279
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonFontSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonFontSizeSp:I

    .line 280
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonFontColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonFontColor:I

    .line 281
    iget-object v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

    .line 283
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconWidthDp:I

    .line 284
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconHeightDp:I

    .line 285
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconTopDp:I

    .line 286
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconBottomDp:I

    .line 287
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconLeftDp:I

    .line 288
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mIconRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconRightDp:I

    .line 290
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleLeftDp:I

    .line 291
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleRightDp:I

    .line 292
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleTopDp:I

    .line 293
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleBottomDp:I

    .line 294
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleFontSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleFontSizeSp:I

    .line 295
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleFontColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleFontColor:I

    .line 296
    iget-object v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTitleFontTypeFace:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleFontTypeFace:Landroid/graphics/Typeface;

    .line 298
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicWidthDp:I

    .line 299
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicHeightDp:I

    .line 300
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicTopDp:I

    .line 301
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicBottomDp:I

    .line 302
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicLeftDp:I

    .line 303
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mFirstPicRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicRightDp:I

    .line 305
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicWidthDp:I

    .line 306
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicHeightDp:I

    .line 307
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicTopDp:I

    .line 308
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicBottomDp:I

    .line 309
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicLeftDp:I

    .line 310
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mTwoPicRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicRightDp:I

    .line 312
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicWidthDp:I

    .line 313
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicHeightDp:I

    .line 314
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicTopDp:I

    .line 315
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicBottomDp:I

    .line 316
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicLeftDp:I

    .line 317
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mThreePicRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicRightDp:I

    .line 319
    iget-object v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mImageBackground:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mImageBackground:Landroid/graphics/drawable/Drawable;

    .line 320
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mImageBackgroundColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mImageBackgroundColor:I

    .line 322
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandLeftDp:I

    .line 323
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandRightDp:I

    .line 324
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandBottomDp:I

    .line 325
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandFontSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandFontSizeSp:I

    .line 326
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandFontColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandFontColor:I

    .line 327
    iget-object v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mBrandFontTypeFace:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandFontTypeFace:Landroid/graphics/Typeface;

    .line 329
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonWidthDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonWidthDp:I

    .line 330
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonHeightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonHeightDp:I

    .line 331
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonLeftDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonLeftDp:I

    .line 332
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonRightDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonRightDp:I

    .line 333
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonTopDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonTopDp:I

    .line 334
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonBottomDp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonBottomDp:I

    .line 335
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonFontColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonFontColor:I

    .line 336
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonFontSizeSp:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonFontSizeSp:I

    .line 337
    iget-object v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonFontTypeFace:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonFontTypeFace:Landroid/graphics/Typeface;

    .line 338
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonForegroundColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonForegroundColor:I

    .line 339
    iget v0, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mButtonBackgroundColor:I

    iput v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonBackgroundColor:I

    .line 340
    iget-boolean p1, p1, Lcom/baidu/mobads/component/StyleParams$Builder;->mIsShowDownloadInfo:Z

    iput-boolean p1, p0, Lcom/baidu/mobads/component/StyleParams;->mIsShowDownloadInfo:Z

    return-void
.end method


# virtual methods
.method public getBrandBottomDp()I
    .locals 1

    .line 479
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandBottomDp:I

    return v0
.end method

.method public getBrandFontColor()I
    .locals 1

    .line 483
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandFontColor:I

    return v0
.end method

.method public getBrandFontSizeSp()I
    .locals 1

    .line 481
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandFontSizeSp:I

    return v0
.end method

.method public getBrandFontTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .line 485
    iget-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandFontTypeFace:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getBrandLeftDp()I
    .locals 1

    .line 475
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandLeftDp:I

    return v0
.end method

.method public getBrandRightDp()I
    .locals 1

    .line 477
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mBrandRightDp:I

    return v0
.end method

.method public getButtonBackgroundColor()I
    .locals 1

    .line 507
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonBackgroundColor:I

    return v0
.end method

.method public getButtonBottomDp()I
    .locals 1

    .line 497
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonBottomDp:I

    return v0
.end method

.method public getButtonFontTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonFontTypeFace:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getButtonForegroundColor()I
    .locals 1

    .line 505
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonForegroundColor:I

    return v0
.end method

.method public getButtonHeightDp()I
    .locals 1

    .line 489
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonHeightDp:I

    return v0
.end method

.method public getButtonLeftDp()I
    .locals 1

    .line 491
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonLeftDp:I

    return v0
.end method

.method public getButtonRightDp()I
    .locals 1

    .line 493
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonRightDp:I

    return v0
.end method

.method public getButtonTextColor()I
    .locals 1

    .line 501
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonFontColor:I

    return v0
.end method

.method public getButtonTextSizeSp()I
    .locals 1

    .line 499
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonFontSizeSp:I

    return v0
.end method

.method public getButtonTopDp()I
    .locals 1

    .line 495
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonTopDp:I

    return v0
.end method

.method public getButtonWidthDp()I
    .locals 1

    .line 487
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mButtonWidthDp:I

    return v0
.end method

.method public getFirstPicBottomDp()I
    .locals 1

    .line 408
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicBottomDp:I

    return v0
.end method

.method public getFirstPicHeightDp()I
    .locals 1

    .line 400
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicHeightDp:I

    return v0
.end method

.method public getFirstPicLeftDp()I
    .locals 1

    .line 412
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicLeftDp:I

    return v0
.end method

.method public getFirstPicRightDp()I
    .locals 1

    .line 416
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicRightDp:I

    return v0
.end method

.method public getFirstPicTopDp()I
    .locals 1

    .line 404
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicTopDp:I

    return v0
.end method

.method public getFirstPicWidthDp()I
    .locals 1

    .line 396
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mFirstPicWidthDp:I

    return v0
.end method

.method public getIconBottomDp()I
    .locals 1

    .line 356
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconBottomDp:I

    return v0
.end method

.method public getIconHeightDp()I
    .locals 1

    .line 348
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconHeightDp:I

    return v0
.end method

.method public getIconLeftDp()I
    .locals 1

    .line 360
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconLeftDp:I

    return v0
.end method

.method public getIconRightDp()I
    .locals 1

    .line 364
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconRightDp:I

    return v0
.end method

.method public getIconTopDp()I
    .locals 1

    .line 352
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconTopDp:I

    return v0
.end method

.method public getIconWidthDp()I
    .locals 1

    .line 344
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIconWidthDp:I

    return v0
.end method

.method public getImageBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 472
    iget-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mImageBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getImageBackgroundColor()I
    .locals 1

    .line 468
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mImageBackgroundColor:I

    return v0
.end method

.method public getSmartDownloadAppNameBottomDp()I
    .locals 1

    .line 628
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameBottomDp:I

    return v0
.end method

.method public getSmartDownloadAppNameLeftDp()I
    .locals 1

    .line 632
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameLeftDp:I

    return v0
.end method

.method public getSmartDownloadAppNameRightDp()I
    .locals 1

    .line 636
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameRightDp:I

    return v0
.end method

.method public getSmartDownloadAppNameTextColor()I
    .locals 1

    .line 620
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameTextColor:I

    return v0
.end method

.method public getSmartDownloadAppNameTextSizeSp()I
    .locals 1

    .line 616
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadAppNameTopDp()I
    .locals 1

    .line 624
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadAppNameTopDp:I

    return v0
.end method

.method public getSmartDownloadButtonBackgroundColor()I
    .locals 1

    .line 668
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonBackgroundColor:I

    return v0
.end method

.method public getSmartDownloadButtonBottomDp()I
    .locals 1

    .line 660
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonBottomDp:I

    return v0
.end method

.method public getSmartDownloadButtonFontColor()I
    .locals 1

    .line 676
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonFontColor:I

    return v0
.end method

.method public getSmartDownloadButtonFontSizeSp()I
    .locals 1

    .line 672
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonFontSizeSp:I

    return v0
.end method

.method public getSmartDownloadButtonFontTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .line 680
    iget-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonFontTypeFace:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getSmartDownloadButtonForegroundColor()I
    .locals 1

    .line 664
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonForegroundColor:I

    return v0
.end method

.method public getSmartDownloadButtonHeightDp()I
    .locals 1

    .line 644
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonHeightDp:I

    return v0
.end method

.method public getSmartDownloadButtonLeftDp()I
    .locals 1

    .line 648
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonLeftDp:I

    return v0
.end method

.method public getSmartDownloadButtonRightDp()I
    .locals 1

    .line 652
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonRightDp:I

    return v0
.end method

.method public getSmartDownloadButtonTopDp()I
    .locals 1

    .line 656
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonTopDp:I

    return v0
.end method

.method public getSmartDownloadButtonWidthDp()I
    .locals 1

    .line 640
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadButtonWidthDp:I

    return v0
.end method

.method public getSmartDownloadCompanyBottomDp()I
    .locals 1

    .line 534
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyBottomDp:I

    return v0
.end method

.method public getSmartDownloadCompanyLeftDp()I
    .locals 1

    .line 538
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyLeftDp:I

    return v0
.end method

.method public getSmartDownloadCompanyRightDp()I
    .locals 1

    .line 542
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyRightDp:I

    return v0
.end method

.method public getSmartDownloadCompanyTextColor()I
    .locals 1

    .line 526
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyTextColor:I

    return v0
.end method

.method public getSmartDownloadCompanyTextSizeSp()I
    .locals 1

    .line 522
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadCompanyTopDp()I
    .locals 1

    .line 530
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadCompanyTopDp:I

    return v0
.end method

.method public getSmartDownloadPermissionBottomDp()I
    .locals 1

    .line 580
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionBottomDp:I

    return v0
.end method

.method public getSmartDownloadPermissionLeftDp()I
    .locals 1

    .line 584
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionLeftDp:I

    return v0
.end method

.method public getSmartDownloadPermissionRightDp()I
    .locals 1

    .line 588
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionRightDp:I

    return v0
.end method

.method public getSmartDownloadPermissionTextColor()I
    .locals 1

    .line 572
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionTextColor:I

    return v0
.end method

.method public getSmartDownloadPermissionTextSizeSp()I
    .locals 1

    .line 569
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadPermissionTopDp()I
    .locals 1

    .line 576
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPermissionTopDp:I

    return v0
.end method

.method public getSmartDownloadPrivacyBottomDp()I
    .locals 1

    .line 604
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyBottomDp:I

    return v0
.end method

.method public getSmartDownloadPrivacyLeftDp()I
    .locals 1

    .line 608
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyLeftDp:I

    return v0
.end method

.method public getSmartDownloadPrivacyRightDp()I
    .locals 1

    .line 612
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyRightDp:I

    return v0
.end method

.method public getSmartDownloadPrivacyTextColor()I
    .locals 1

    .line 596
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyTextColor:I

    return v0
.end method

.method public getSmartDownloadPrivacyTextSizeSp()I
    .locals 1

    .line 592
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadPrivacyTopDp()I
    .locals 1

    .line 600
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadPrivacyTopDp:I

    return v0
.end method

.method public getSmartDownloadVersionBottomDp()I
    .locals 1

    .line 558
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionBottomDp:I

    return v0
.end method

.method public getSmartDownloadVersionLeftDp()I
    .locals 1

    .line 562
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionLeftDp:I

    return v0
.end method

.method public getSmartDownloadVersionRightDp()I
    .locals 1

    .line 566
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionRightDp:I

    return v0
.end method

.method public getSmartDownloadVersionTextColor()I
    .locals 1

    .line 550
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionTextColor:I

    return v0
.end method

.method public getSmartDownloadVersionTextSizeSp()I
    .locals 1

    .line 546
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionTextSizeSp:I

    return v0
.end method

.method public getSmartDownloadVersionTopDp()I
    .locals 1

    .line 554
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadVersionTopDp:I

    return v0
.end method

.method public getSmartDownloadViewBackgroundColor()I
    .locals 1

    .line 518
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewBackgroundColor:I

    return v0
.end method

.method public getSmartDownloadViewBottomDp()I
    .locals 1

    .line 515
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewBottomDp:I

    return v0
.end method

.method public getSmartDownloadViewLeftDp()I
    .locals 1

    .line 509
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewLeftDp:I

    return v0
.end method

.method public getSmartDownloadViewRightDp()I
    .locals 1

    .line 511
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewRightDp:I

    return v0
.end method

.method public getSmartDownloadViewTopDp()I
    .locals 1

    .line 513
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mSmartDownloadViewTopDp:I

    return v0
.end method

.method public getThreePicBottomDp()I
    .locals 1

    .line 456
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicBottomDp:I

    return v0
.end method

.method public getThreePicHeightDp()I
    .locals 1

    .line 448
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicHeightDp:I

    return v0
.end method

.method public getThreePicLeftDp()I
    .locals 1

    .line 460
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicLeftDp:I

    return v0
.end method

.method public getThreePicRightDp()I
    .locals 1

    .line 464
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicRightDp:I

    return v0
.end method

.method public getThreePicTopDp()I
    .locals 1

    .line 452
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicTopDp:I

    return v0
.end method

.method public getThreePicWidthDp()I
    .locals 1

    .line 444
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mThreePicWidthDp:I

    return v0
.end method

.method public getTitleBottomDp()I
    .locals 1

    .line 380
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleBottomDp:I

    return v0
.end method

.method public getTitleFontColor()I
    .locals 1

    .line 388
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleFontColor:I

    return v0
.end method

.method public getTitleFontSizeSp()I
    .locals 1

    .line 384
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleFontSizeSp:I

    return v0
.end method

.method public getTitleFontTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .line 392
    iget-object v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleFontTypeFace:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getTitleLeftDp()I
    .locals 1

    .line 368
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleLeftDp:I

    return v0
.end method

.method public getTitleRightDp()I
    .locals 1

    .line 372
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleRightDp:I

    return v0
.end method

.method public getTitleTopDp()I
    .locals 1

    .line 376
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTitleTopDp:I

    return v0
.end method

.method public getTwoPicBottomDp()I
    .locals 1

    .line 432
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicBottomDp:I

    return v0
.end method

.method public getTwoPicHeightDp()I
    .locals 1

    .line 424
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicHeightDp:I

    return v0
.end method

.method public getTwoPicLeftDp()I
    .locals 1

    .line 436
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicLeftDp:I

    return v0
.end method

.method public getTwoPicRightDp()I
    .locals 1

    .line 440
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicRightDp:I

    return v0
.end method

.method public getTwoPicTopDp()I
    .locals 1

    .line 428
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicTopDp:I

    return v0
.end method

.method public getTwoPicWidthDp()I
    .locals 1

    .line 420
    iget v0, p0, Lcom/baidu/mobads/component/StyleParams;->mTwoPicWidthDp:I

    return v0
.end method

.method public isShowDownloadInfo()Z
    .locals 1

    .line 683
    iget-boolean v0, p0, Lcom/baidu/mobads/component/StyleParams;->mIsShowDownloadInfo:Z

    return v0
.end method
