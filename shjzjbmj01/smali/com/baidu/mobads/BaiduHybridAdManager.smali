.class public Lcom/baidu/mobads/BaiduHybridAdManager;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

.field private b:Lcom/baidu/mobads/BaiduHybridAdViewListener;

.field private c:Lcom/baidu/mobads/production/c/a;

.field private d:Landroid/webkit/WebView;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/baidu/mobads/d;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/d;-><init>(Lcom/baidu/mobads/BaiduHybridAdManager;)V

    iput-object v0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->b:Lcom/baidu/mobads/BaiduHybridAdViewListener;

    const/4 v0, 0x0

    .line 39
    iput-boolean v0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->e:Z

    .line 41
    new-instance v0, Lcom/baidu/mobads/e;

    invoke-direct {v0, p0}, Lcom/baidu/mobads/e;-><init>(Lcom/baidu/mobads/BaiduHybridAdManager;)V

    iput-object v0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/BaiduHybridAdManager;)Lcom/baidu/mobads/production/c/a;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    return-object p0
.end method

.method static synthetic b(Lcom/baidu/mobads/BaiduHybridAdManager;)Lcom/baidu/mobads/BaiduHybridAdViewListener;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->b:Lcom/baidu/mobads/BaiduHybridAdViewListener;

    return-object p0
.end method


# virtual methods
.method public injectJavaScriptBridge(Landroid/webkit/WebView;)V
    .locals 2

    .line 72
    iget-boolean v0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->e:Z

    if-nez v0, :cond_0

    .line 73
    iput-object p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->d:Landroid/webkit/WebView;

    .line 74
    new-instance p1, Lcom/baidu/mobads/production/c/a;

    iget-object v0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->d:Landroid/webkit/WebView;

    invoke-direct {p1, v0}, Lcom/baidu/mobads/production/c/a;-><init>(Landroid/webkit/WebView;)V

    iput-object p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    .line 75
    iget-object p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    const-string v0, "AdLoaded"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/c/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 76
    iget-object p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    const-string v0, "AdError"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/c/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 77
    iget-object p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    const-string v0, "AdStarted"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/c/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 78
    iget-object p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    const-string v0, "AdUserClick"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/c/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 79
    iget-object p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    const-string v0, "AdUserClose"

    iget-object v1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/production/c/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 80
    iget-object p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/c/a;->request()V

    :cond_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    const/4 p1, 0x0

    .line 85
    iput-boolean p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->e:Z

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    const/4 p1, 0x1

    .line 104
    iput-boolean p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->e:Z

    return-void
.end method

.method public setBaiduHybridAdViewListener(Lcom/baidu/mobads/BaiduHybridAdViewListener;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->b:Lcom/baidu/mobads/BaiduHybridAdViewListener;

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    .line 89
    iget-boolean v0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/baidu/mobads/BaiduHybridAdManager;->c:Lcom/baidu/mobads/production/c/a;

    invoke-virtual {v0, p1, p2}, Lcom/baidu/mobads/production/c/a;->a(Landroid/webkit/WebView;Ljava/lang/String;)Z

    .line 93
    :cond_0
    :try_start_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string p2, "mobadssdk"

    .line 94
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    const-string p2, "mobads"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    .line 98
    sget-object p2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p2, p1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    :cond_2
    const/4 p1, 0x0

    return p1
.end method
