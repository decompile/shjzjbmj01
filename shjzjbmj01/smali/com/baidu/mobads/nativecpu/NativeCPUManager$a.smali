.class Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/nativecpu/NativeCPUManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_0

    .line 179
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 180
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 181
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_1

    const-string v1, "msg"

    .line 183
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 184
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 185
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0
.end method


# virtual methods
.method public run(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 5

    const-string v0, "AdLoaded"

    .line 123
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 126
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 127
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {v0}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->b(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/nativecpu/a/a;->b()Ljava/util/List;

    move-result-object v0

    .line 128
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 129
    new-instance v2, Lcom/baidu/mobads/nativecpu/a/b;

    iget-object v3, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {v3}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->c(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-direct {v2, v3, v1, v4}, Lcom/baidu/mobads/nativecpu/a/b;-><init>(Landroid/content/Context;Ljava/lang/Object;Lcom/baidu/mobads/nativecpu/NativeCPUManager;)V

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 131
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    new-instance v1, Lcom/baidu/mobads/nativecpu/b;

    invoke-direct {v1, p0, p1}, Lcom/baidu/mobads/nativecpu/b;-><init>(Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    :cond_1
    const-string v0, "AdError"

    .line 138
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {v0}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->b(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/nativecpu/a/a;->a()V

    .line 140
    invoke-direct {p0, p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)Ljava/lang/String;

    move-result-object v0

    .line 141
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getCode()I

    move-result p1

    .line 142
    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {v1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 143
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    new-instance v2, Lcom/baidu/mobads/nativecpu/c;

    invoke-direct {v2, p0, v0, p1}, Lcom/baidu/mobads/nativecpu/c;-><init>(Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/utils/h;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    :cond_2
    const-string v0, "AdEmptyList"

    .line 150
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v0

    const-string v1, "message"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 152
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getCode()I

    move-result p1

    .line 153
    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {v1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 154
    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {v1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;->onNoAd(Ljava/lang/String;I)V

    goto/16 :goto_1

    :cond_3
    const-string v0, "AdUserClick"

    .line 156
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 157
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 158
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;->onAdClick()V

    goto :goto_1

    :cond_4
    const-string v0, "AdStatusChange"

    .line 160
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 161
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {v0}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 162
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {v0}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object v0

    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;->onAdStatusChanged(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v0, "vdieoCacheSucc"

    .line 164
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 165
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 166
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;->onVideoDownloadSuccess()V

    goto :goto_1

    :cond_6
    const-string v0, "vdieoCacheFailed"

    .line 168
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 169
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 170
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;->a:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-static {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;->onVideoDownloadFailed()V

    :cond_7
    :goto_1
    return-void
.end method
