.class public Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/nativecpu/CPUAdRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;->a:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;)Ljava/util/HashMap;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;->a:Ljava/util/HashMap;

    return-object p0
.end method


# virtual methods
.method public addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;
    .locals 1

    .line 73
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object p0
.end method

.method public build()Lcom/baidu/mobads/nativecpu/CPUAdRequest;
    .locals 2

    .line 80
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUAdRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobads/nativecpu/CPUAdRequest;-><init>(Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;Lcom/baidu/mobads/nativecpu/a;)V

    return-object v0
.end method

.method public setAccessType(I)Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;->a:Ljava/util/HashMap;

    const-string v1, "accessType"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setCityIfLocalChannel(Ljava/lang/String;)Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;->a:Ljava/util/HashMap;

    const-string v1, "listScene"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;->a:Ljava/util/HashMap;

    const-string v1, "city"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setCustomUserId(Ljava/lang/String;)Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;->a:Ljava/util/HashMap;

    const-string v1, "outerUid"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setDownloadAppConfirmPolicy(I)Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;->a:Ljava/util/HashMap;

    const-string v1, "downloadAppConfirmPolicy"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setListScene(I)Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/CPUAdRequest$Builder;->a:Ljava/util/HashMap;

    const-string v1, "listScene"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
