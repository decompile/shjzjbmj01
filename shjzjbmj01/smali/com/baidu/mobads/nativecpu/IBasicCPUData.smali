.class public interface abstract Lcom/baidu/mobads/nativecpu/IBasicCPUData;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getActionType()I
.end method

.method public abstract getAdHeight()I
.end method

.method public abstract getAdLogoUrl()Ljava/lang/String;
.end method

.method public abstract getAdWidth()I
.end method

.method public abstract getAppPackageName()Ljava/lang/String;
.end method

.method public abstract getAppPermissionUrl()Ljava/lang/String;
.end method

.method public abstract getAppPrivacyUrl()Ljava/lang/String;
.end method

.method public abstract getAppPublisher()Ljava/lang/String;
.end method

.method public abstract getAppVersion()Ljava/lang/String;
.end method

.method public abstract getAuthor()Ljava/lang/String;
.end method

.method public abstract getBaiduLogoUrl()Ljava/lang/String;
.end method

.method public abstract getBrandName()Ljava/lang/String;
.end method

.method public abstract getChannelId()Ljava/lang/String;
.end method

.method public abstract getChannelName()Ljava/lang/String;
.end method

.method public abstract getCommentCounts()I
.end method

.method public abstract getContentClickUrl()Ljava/lang/String;
.end method

.method public abstract getDesc()Ljava/lang/String;
.end method

.method public abstract getDislikeReasons()Lorg/json/JSONArray;
.end method

.method public abstract getDownloadStatus()I
.end method

.method public abstract getDuration()I
.end method

.method public abstract getExtra()Lorg/json/JSONObject;
.end method

.method public abstract getIconUrl()Ljava/lang/String;
.end method

.method public abstract getImageUrls()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPlayCounts()I
.end method

.method public abstract getPresentationType()I
.end method

.method public abstract getSmallImageUrls()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getStyleType()I
.end method

.method public abstract getThumbHeight()I
.end method

.method public abstract getThumbUrl()Ljava/lang/String;
.end method

.method public abstract getThumbWidth()I
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public abstract getUpdateTime()Ljava/lang/String;
.end method

.method public abstract getVUrl()Ljava/lang/String;
.end method

.method public abstract handleClick(Landroid/view/View;)V
.end method

.method public abstract isDownloadApp()Z
.end method

.method public abstract isTop()Z
.end method

.method public abstract markDislike([Ljava/lang/String;)V
.end method

.method public abstract onImpression(Landroid/view/View;)V
.end method
