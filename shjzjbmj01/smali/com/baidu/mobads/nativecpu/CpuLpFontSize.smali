.class public final enum Lcom/baidu/mobads/nativecpu/CpuLpFontSize;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/nativecpu/CpuLpFontSize;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum EXTRA_LARGE:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

.field public static final enum LARGE:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

.field public static final enum REGULAR:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

.field public static final enum SMALL:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

.field public static final enum XX_LARGE:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

.field private static final synthetic b:[Lcom/baidu/mobads/nativecpu/CpuLpFontSize;


# instance fields
.field a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 4
    new-instance v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    const-string v1, "SMALL"

    const-string v2, "sml"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->SMALL:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    .line 5
    new-instance v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    const-string v1, "REGULAR"

    const-string v2, "reg"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->REGULAR:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    .line 6
    new-instance v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    const-string v1, "LARGE"

    const-string v2, "lrg"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->LARGE:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    .line 7
    new-instance v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    const-string v1, "EXTRA_LARGE"

    const-string v2, "xlg"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->EXTRA_LARGE:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    .line 8
    new-instance v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    const-string v1, "XX_LARGE"

    const-string v2, "xxl"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->XX_LARGE:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    const/4 v0, 0x5

    .line 3
    new-array v0, v0, [Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    sget-object v1, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->SMALL:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->REGULAR:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->LARGE:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->EXTRA_LARGE:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->XX_LARGE:Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    aput-object v1, v0, v7

    sput-object v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->b:[Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 13
    iput-object p3, p0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->a:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/nativecpu/CpuLpFontSize;
    .locals 1

    .line 3
    const-class v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/nativecpu/CpuLpFontSize;
    .locals 1

    .line 3
    sget-object v0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->b:[Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    invoke-virtual {v0}, [Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/nativecpu/CpuLpFontSize;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->a:Ljava/lang/String;

    return-object v0
.end method
