.class public Lcom/baidu/mobads/nativecpu/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/nativecpu/IBasicCPUData;


# instance fields
.field public a:Ljava/lang/Object;

.field public b:Lcom/baidu/mobads/nativecpu/a/c;

.field private c:Lcom/baidu/mobads/nativecpu/NativeCPUManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;Lcom/baidu/mobads/nativecpu/NativeCPUManager;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p2, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    .line 19
    iput-object p3, p0, Lcom/baidu/mobads/nativecpu/a/b;->c:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    const-string p2, "com.baidu.mobads.container.nativecpu.interfaces.ICPUAdInstance"

    .line 20
    invoke-static {p1, p2}, Lcom/baidu/mobads/nativecpu/a/c;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/baidu/mobads/nativecpu/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    return-void
.end method


# virtual methods
.method public getActionType()I
    .locals 4

    .line 161
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getActionType"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getAdHeight()I
    .locals 4

    .line 102
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getAdHeight"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getAdLogoUrl()Ljava/lang/String;
    .locals 4

    .line 107
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getAdLogoUrl"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAdWidth()I
    .locals 4

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getAdWidth"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getAppPackageName()Ljava/lang/String;
    .locals 4

    .line 166
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getPackageName"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAppPermissionUrl()Ljava/lang/String;
    .locals 4

    .line 232
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getAppPermissionUrl"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->c(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppPrivacyUrl()Ljava/lang/String;
    .locals 4

    .line 227
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getAppPrivacyUrl"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->c(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppPublisher()Ljava/lang/String;
    .locals 4

    .line 217
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getAppPublisher"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->c(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 4

    .line 222
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getAppVersion"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->c(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAuthor()Ljava/lang/String;
    .locals 4

    .line 30
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getAuthor"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getBaiduLogoUrl()Ljava/lang/String;
    .locals 4

    .line 112
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getBaiduLogoUrl"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getBrandName()Ljava/lang/String;
    .locals 4

    .line 171
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getBrandName"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 172
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 173
    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChannelId()Ljava/lang/String;
    .locals 4

    .line 60
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getCatId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getChannelName()Ljava/lang/String;
    .locals 4

    .line 65
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getCatName"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCommentCounts()I
    .locals 4

    .line 70
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getCommentCounts"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getContentClickUrl()Ljava/lang/String;
    .locals 4

    .line 180
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getContentClickUrl"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 181
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 182
    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDesc()Ljava/lang/String;
    .locals 4

    .line 50
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getContent"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDislikeReasons()Lorg/json/JSONArray;
    .locals 4

    .line 156
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getDislikeReasons"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONArray;

    return-object v0
.end method

.method public getDownloadStatus()I
    .locals 4

    .line 189
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getDownloadStatus"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getDuration()I
    .locals 4

    .line 146
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getDuration"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getExtra()Lorg/json/JSONObject;
    .locals 4

    .line 92
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getExtra"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getAvatar"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrls()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getImageList"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 87
    instance-of v1, v0, Ljava/util/List;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getPlayCounts()I
    .locals 5

    .line 132
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getPlayCounts"

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v4}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 133
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 134
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_0
    return v3
.end method

.method public getPresentationType()I
    .locals 4

    .line 151
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getPresentationType"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSmallImageUrls()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getSmallImageList"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 81
    instance-of v1, v0, Ljava/util/List;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getStyleType()I
    .locals 4

    .line 75
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getStyleType"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getThumbHeight()I
    .locals 4

    .line 127
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getThumbHeight"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 4

    .line 117
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getThumbUrl"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getThumbWidth()I
    .locals 4

    .line 122
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getThumbWidth"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 4

    .line 45
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getTitle"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 4

    .line 25
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getType"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateTime()Ljava/lang/String;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getUpdateTime"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getVUrl()Ljava/lang/String;
    .locals 4

    .line 141
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "getVUrl"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public handleClick(Landroid/view/View;)V
    .locals 7

    .line 204
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->c:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->c:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-virtual {v0}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->getLpStyleParams()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v3, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v4, "setLpStyleParams"

    new-array v5, v2, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/baidu/mobads/nativecpu/a/b;->c:Lcom/baidu/mobads/nativecpu/NativeCPUManager;

    invoke-virtual {v6}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->getLpStyleParams()Ljava/util/Map;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v0, v3, v4, v5}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v3, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v4, "handleClick"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-virtual {v0, v3, v4, v2}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public isDownloadApp()Z
    .locals 4

    .line 194
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "isDownloadApp"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isTop()Z
    .locals 4

    .line 55
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "isTop"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public markDislike([Ljava/lang/String;)V
    .locals 5

    .line 212
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "markDislike"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onImpression(Landroid/view/View;)V
    .locals 5

    .line 199
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/b;->b:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/b;->a:Ljava/lang/Object;

    const-string v2, "onImpression"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
