.class public Lcom/baidu/mobads/nativecpu/a/a;
.super Lcom/baidu/mobads/production/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/nativecpu/a/a$a;
    }
.end annotation


# instance fields
.field private A:Ljava/lang/Object;

.field private B:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private D:I

.field private E:I

.field private F:[I

.field private G:Z

.field private H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

.field private I:Ljava/lang/String;

.field private J:Lcom/baidu/mobads/utils/q;

.field private z:Lcom/baidu/mobads/nativecpu/a/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .line 51
    invoke-direct {p0, p1}, Lcom/baidu/mobads/production/a;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->B:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 48
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->J:Lcom/baidu/mobads/utils/q;

    .line 52
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/nativecpu/a/a;->setActivity(Landroid/content/Context;)V

    .line 53
    iput-object p2, p0, Lcom/baidu/mobads/nativecpu/a/a;->I:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/nativecpu/a/a;)Ljava/util/HashMap;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/baidu/mobads/nativecpu/a/a;->B:Ljava/util/HashMap;

    return-object p0
.end method

.method private s()V
    .locals 8

    .line 64
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "addEventListener"

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "AdLoaded"

    const/4 v6, 0x0

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    const/4 v7, 0x1

    aput-object v5, v4, v7

    invoke-virtual {v0, v1, v2, v4}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "addEventListener"

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "AdEmptyList"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    aput-object v5, v4, v7

    invoke-virtual {v0, v1, v2, v4}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "addEventListener"

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "AdError"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    aput-object v5, v4, v7

    invoke-virtual {v0, v1, v2, v4}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "addEventListener"

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "AdUserClick"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    aput-object v5, v4, v7

    invoke-virtual {v0, v1, v2, v4}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "addEventListener"

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "AdStatusChange"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    aput-object v5, v4, v7

    invoke-virtual {v0, v1, v2, v4}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "addEventListener"

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "vdieoCacheSucc"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    aput-object v5, v4, v7

    invoke-virtual {v0, v1, v2, v4}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "addEventListener"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "vdieoCacheFailed"

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 169
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/nativecpu/a/a;->removeAllListeners()V

    .line 170
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "removeAllListeners"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 174
    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->J:Lcom/baidu/mobads/utils/q;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method public a(I)V
    .locals 5

    .line 157
    :try_start_0
    iput p1, p0, Lcom/baidu/mobads/nativecpu/a/a;->m:I

    .line 159
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "setTimeoutMillis"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 163
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->J:Lcom/baidu/mobads/utils/q;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method public a(II[IZLjava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II[IZ",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 126
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->B:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    if-eqz p5, :cond_0

    .line 128
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->B:Ljava/util/HashMap;

    invoke-virtual {v0, p5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 131
    :cond_0
    iget-object p5, p0, Lcom/baidu/mobads/nativecpu/a/a;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    const/4 v0, 0x0

    if-eqz p5, :cond_1

    iget-object p5, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    if-eqz p5, :cond_1

    .line 132
    invoke-direct {p0}, Lcom/baidu/mobads/nativecpu/a/a;->s()V

    .line 133
    iget-object p5, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "loadAd"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    .line 134
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v0

    const/4 p1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v3, p1

    const/4 p1, 0x2

    aput-object p3, v3, p1

    const/4 p1, 0x3

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    aput-object p2, v3, p1

    const/4 p1, 0x4

    iget-object p2, p0, Lcom/baidu/mobads/nativecpu/a/a;->B:Ljava/util/HashMap;

    aput-object p2, v3, p1

    .line 133
    invoke-virtual {p5, v1, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 137
    :cond_1
    iput p1, p0, Lcom/baidu/mobads/nativecpu/a/a;->E:I

    .line 138
    iput p2, p0, Lcom/baidu/mobads/nativecpu/a/a;->D:I

    .line 139
    iput-object p3, p0, Lcom/baidu/mobads/nativecpu/a/a;->F:[I

    .line 140
    iput-boolean p4, p0, Lcom/baidu/mobads/nativecpu/a/a;->G:Z

    const-string p1, "AdError"

    .line 143
    iget-object p2, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p0, p1, p2}, Lcom/baidu/mobads/nativecpu/a/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 145
    invoke-virtual {p0}, Lcom/baidu/mobads/nativecpu/a/a;->m()V

    const/4 p1, 0x0

    .line 147
    invoke-virtual {p0, p1, p1, v0}, Lcom/baidu/mobads/nativecpu/a/a;->a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 150
    iget-object p2, p0, Lcom/baidu/mobads/nativecpu/a/a;->J:Lcom/baidu/mobads/utils/q;

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected a(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 8

    .line 91
    :try_start_0
    invoke-virtual {p0}, Lcom/baidu/mobads/nativecpu/a/a;->l()V

    .line 93
    invoke-virtual {p0}, Lcom/baidu/mobads/nativecpu/a/a;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 94
    invoke-virtual {p0}, Lcom/baidu/mobads/nativecpu/a/a;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 96
    new-instance v5, Lcom/baidu/mobads/production/l;

    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->f:Landroid/content/Context;

    invoke-direct {v5, v0, p0}, Lcom/baidu/mobads/production/l;-><init>(Landroid/content/Context;Lcom/baidu/mobads/production/a;)V

    .line 97
    new-instance v0, Lcom/baidu/mobads/nativecpu/a/a$a;

    sget-object v1, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_CPU:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {v0, p0, p1, v2, v1}, Lcom/baidu/mobads/nativecpu/a/a$a;-><init>(Lcom/baidu/mobads/nativecpu/a/a;Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->k:Lcom/baidu/mobads/vo/d;

    .line 99
    new-instance p1, Lcom/baidu/mobads/production/k;

    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->f:Landroid/content/Context;

    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->k:Lcom/baidu/mobads/vo/d;

    invoke-virtual {v0}, Lcom/baidu/mobads/vo/d;->d()Lcom/baidu/mobads/interfaces/IXAdProdInfo;

    move-result-object v3

    const/4 v4, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/baidu/mobads/nativecpu/a/a;->getAdResponseInfo()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    move-result-object v6

    const/4 v7, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v7}, Lcom/baidu/mobads/production/k;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/baidu/mobads/interfaces/IXAdProdInfo;Landroid/widget/RelativeLayout;Lcom/baidu/mobads/interfaces/IXAdContainerEventListener;Lcom/baidu/mobads/interfaces/IXAdResponseInfo;Landroid/view/View;)V

    .line 101
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->f:Landroid/content/Context;

    const-string v1, "com.baidu.mobads.container.nativecpu.interfaces.RCPUAdProd"

    invoke-static {v0, v1}, Lcom/baidu/mobads/nativecpu/a/c;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/baidu/mobads/nativecpu/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    .line 102
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v5, 0x1

    aput-object v3, v2, v5

    new-array v3, v1, [Ljava/lang/Object;

    aput-object p1, v3, v4

    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/a/a;->I:Ljava/lang/String;

    aput-object p1, v3, v5

    invoke-virtual {v0, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->a([Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    .line 105
    invoke-direct {p0}, Lcom/baidu/mobads/nativecpu/a/a;->s()V

    .line 106
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v2, "loadAd"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    iget v6, p0, Lcom/baidu/mobads/nativecpu/a/a;->E:I

    .line 107
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    iget v6, p0, Lcom/baidu/mobads/nativecpu/a/a;->D:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    iget-object v6, p0, Lcom/baidu/mobads/nativecpu/a/a;->F:[I

    aput-object v6, v3, v1

    const/4 v1, 0x3

    iget-boolean v6, p0, Lcom/baidu/mobads/nativecpu/a/a;->G:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v3, v1

    const/4 v1, 0x4

    iget-object v6, p0, Lcom/baidu/mobads/nativecpu/a/a;->B:Ljava/util/HashMap;

    aput-object v6, v3, v1

    .line 106
    invoke-virtual {p1, v0, v2, v3}, Lcom/baidu/mobads/nativecpu/a/c;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/a/a;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    :cond_0
    invoke-virtual {p1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 111
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->J:Lcom/baidu/mobads/utils/q;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method protected a(Lcom/baidu/mobads/openad/b/b;Lcom/baidu/mobads/production/p;I)V
    .locals 0

    .line 197
    :try_start_0
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/a/a;->f:Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/nativecpu/a/a;->a(Landroid/content/Context;)V

    .line 198
    new-instance p1, Lcom/baidu/mobads/vo/c;

    const-string p2, "{\'ad\':[{\'id\':99999999,\'url\':\'http://127.0.0.1\', type=\'CPUNative\'}],\'n\':1}"

    invoke-direct {p1, p2}, Lcom/baidu/mobads/vo/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/nativecpu/a/a;->setAdResponseInfo(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 200
    iget-object p2, p0, Lcom/baidu/mobads/nativecpu/a/a;->J:Lcom/baidu/mobads/utils/q;

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    :goto_0
    const-string p1, "XAdMouldeLoader native-cpu dummy requesting success"

    .line 203
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/nativecpu/a/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/baidu/mobads/nativecpu/a/a;->H:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    return-void
.end method

.method public b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/a/a;->z:Lcom/baidu/mobads/nativecpu/a/c;

    iget-object v2, p0, Lcom/baidu/mobads/nativecpu/a/a;->A:Ljava/lang/Object;

    const-string v3, "getAllAdList"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    .line 182
    invoke-virtual {v1, v2, v3, v4}, Lcom/baidu/mobads/nativecpu/a/c;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 183
    check-cast v1, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 185
    iget-object v2, p0, Lcom/baidu/mobads/nativecpu/a/a;->J:Lcom/baidu/mobads/utils/q;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/String;)I

    :goto_0
    return-object v0
.end method

.method protected b(Lcom/baidu/mobads/interfaces/IXAdContainer;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/baidu/mobads/interfaces/IXAdContainer;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public b(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method public getAdRequestInfo()Lcom/baidu/mobads/interfaces/IXAdRequestInfo;
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/a/a;->k:Lcom/baidu/mobads/vo/d;

    return-object v0
.end method

.method public request()V
    .locals 0

    return-void
.end method
