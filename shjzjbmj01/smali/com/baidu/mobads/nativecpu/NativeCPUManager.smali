.class public Lcom/baidu/mobads/nativecpu/NativeCPUManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;,
        Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "NativeCPUManager"


# instance fields
.field private b:Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

.field private c:Landroid/content/Context;

.field private d:I

.field private e:Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;

.field private f:Lcom/baidu/mobads/nativecpu/a/a;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;)V
    .locals 2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xa

    .line 24
    iput v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->d:I

    const/4 v0, 0x0

    .line 26
    iput-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->f:Lcom/baidu/mobads/nativecpu/a/a;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->g:Ljava/util/Map;

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->h:Ljava/util/Map;

    if-eqz p1, :cond_0

    .line 40
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    iput-object p3, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->b:Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    .line 42
    iput-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->c:Landroid/content/Context;

    .line 43
    new-instance p3, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;

    invoke-direct {p3, p0}, Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;-><init>(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)V

    iput-object p3, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->e:Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;

    .line 44
    new-instance p3, Lcom/baidu/mobads/nativecpu/a/a;

    invoke-direct {p3, p1, p2}, Lcom/baidu/mobads/nativecpu/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->f:Lcom/baidu/mobads/nativecpu/a/a;

    .line 45
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->f:Lcom/baidu/mobads/nativecpu/a/a;

    iget-object p2, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->e:Lcom/baidu/mobads/nativecpu/NativeCPUManager$a;

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/nativecpu/a/a;->a(Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    goto :goto_0

    .line 47
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p1

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    sget-object v1, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a:Ljava/lang/String;

    aput-object v1, p2, v0

    const/4 v0, 0x1

    const-string v1, "Init params error!"

    aput-object v1, p2, v0

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/utils/q;->e([Ljava/lang/Object;)I

    if-eqz p3, :cond_1

    const-string p1, "Input params error."

    .line 49
    sget-object p2, Lcom/baidu/mobads/interfaces/error/XAdErrorCode;->INTERFACE_USE_PROBLEM:Lcom/baidu/mobads/interfaces/error/XAdErrorCode;

    .line 50
    invoke-virtual {p2}, Lcom/baidu/mobads/interfaces/error/XAdErrorCode;->getCode()I

    move-result p2

    .line 49
    invoke-interface {p3, p1, p2}, Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;->onAdError(Ljava/lang/String;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->b:Lcom/baidu/mobads/nativecpu/NativeCPUManager$CPUAdListener;

    return-object p0
.end method

.method private a(I[IZ)V
    .locals 7

    if-lez p1, :cond_0

    if-eqz p2, :cond_0

    .line 64
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->f:Lcom/baidu/mobads/nativecpu/a/a;

    if-eqz v0, :cond_1

    .line 65
    iget-object v1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->f:Lcom/baidu/mobads/nativecpu/a/a;

    iget v3, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->d:I

    iget-object v6, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->g:Ljava/util/Map;

    move v2, p1

    move-object v4, p2

    move v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/baidu/mobads/nativecpu/a/a;->a(II[IZLjava/util/Map;)V

    goto :goto_0

    .line 68
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p1

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    sget-object v0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a:Ljava/lang/String;

    aput-object v0, p2, p3

    const/4 p3, 0x1

    const-string v0, "LoadAd with terrible params!"

    aput-object v0, p2, p3

    invoke-virtual {p1, p2}, Lcom/baidu/mobads/utils/q;->e([Ljava/lang/Object;)I

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Lcom/baidu/mobads/nativecpu/a/a;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->f:Lcom/baidu/mobads/nativecpu/a/a;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobads/nativecpu/NativeCPUManager;)Landroid/content/Context;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->c:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public getLpStyleParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->h:Ljava/util/Map;

    return-object v0
.end method

.method public loadAd(IIZ)V
    .locals 2

    const/4 v0, 0x1

    .line 56
    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p2, v0, v1

    invoke-direct {p0, p1, v0, p3}, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a(I[IZ)V

    return-void
.end method

.method public setLpDarkMode(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 84
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->h:Ljava/util/Map;

    const-string v0, "preferscolortheme"

    const-string v1, "dark"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 86
    :cond_0
    iget-object p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->h:Ljava/util/Map;

    const-string v0, "preferscolortheme"

    const-string v1, "light"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public setLpFontSize(Lcom/baidu/mobads/nativecpu/CpuLpFontSize;)V
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->h:Ljava/util/Map;

    const-string v1, "prefersfontsize"

    invoke-virtual {p1}, Lcom/baidu/mobads/nativecpu/CpuLpFontSize;->getValue()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setLpStyleParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 91
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->h:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public setPageSize(I)V
    .locals 3

    if-lez p1, :cond_0

    const/16 v0, 0x14

    if-gt p1, v0, :cond_0

    .line 109
    iput p1, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->d:I

    goto :goto_0

    .line 111
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    sget-object v2, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Input page size is wrong which should be in (0,20]!"

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/q;->e([Ljava/lang/Object;)I

    :goto_0
    return-void
.end method

.method public setRequestParameter(Lcom/baidu/mobads/nativecpu/CPUAdRequest;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 101
    invoke-virtual {p1}, Lcom/baidu/mobads/nativecpu/CPUAdRequest;->getExtras()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 103
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/baidu/mobads/nativecpu/CPUAdRequest;->getExtras()Ljava/util/Map;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public setRequestTimeoutMillis(I)V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->f:Lcom/baidu/mobads/nativecpu/a/a;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/NativeCPUManager;->f:Lcom/baidu/mobads/nativecpu/a/a;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/nativecpu/a/a;->a(I)V

    :cond_0
    return-void
.end method
