.class public final enum Lcom/baidu/mobads/nativecpu/CPUDataType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/nativecpu/CPUDataType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AD:Lcom/baidu/mobads/nativecpu/CPUDataType;

.field public static final enum HOTDOC:Lcom/baidu/mobads/nativecpu/CPUDataType;

.field public static final enum IMAGE:Lcom/baidu/mobads/nativecpu/CPUDataType;

.field public static final enum NEWS:Lcom/baidu/mobads/nativecpu/CPUDataType;

.field public static final enum POLICETASK:Lcom/baidu/mobads/nativecpu/CPUDataType;

.field public static final enum RECALLNEWS:Lcom/baidu/mobads/nativecpu/CPUDataType;

.field public static final enum SMALLVIDEO:Lcom/baidu/mobads/nativecpu/CPUDataType;

.field public static final enum TOPIC:Lcom/baidu/mobads/nativecpu/CPUDataType;

.field public static final enum VIDEO:Lcom/baidu/mobads/nativecpu/CPUDataType;

.field private static final synthetic c:[Lcom/baidu/mobads/nativecpu/CPUDataType;


# instance fields
.field a:Ljava/lang/String;

.field b:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 6
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    const-string v1, "NEWS"

    const-string v2, "news"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/baidu/mobads/nativecpu/CPUDataType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->NEWS:Lcom/baidu/mobads/nativecpu/CPUDataType;

    .line 7
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    const-string v1, "IMAGE"

    const-string v2, "image"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/baidu/mobads/nativecpu/CPUDataType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->IMAGE:Lcom/baidu/mobads/nativecpu/CPUDataType;

    .line 8
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    const-string v1, "VIDEO"

    const-string v2, "video"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2, v5}, Lcom/baidu/mobads/nativecpu/CPUDataType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->VIDEO:Lcom/baidu/mobads/nativecpu/CPUDataType;

    .line 9
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    const-string v1, "TOPIC"

    const-string v2, "topic"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2, v6}, Lcom/baidu/mobads/nativecpu/CPUDataType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->TOPIC:Lcom/baidu/mobads/nativecpu/CPUDataType;

    .line 10
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    const-string v1, "AD"

    const-string v2, "ad"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2, v7}, Lcom/baidu/mobads/nativecpu/CPUDataType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->AD:Lcom/baidu/mobads/nativecpu/CPUDataType;

    .line 11
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    const-string v1, "HOTDOC"

    const-string v2, "hotkey"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2, v8}, Lcom/baidu/mobads/nativecpu/CPUDataType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->HOTDOC:Lcom/baidu/mobads/nativecpu/CPUDataType;

    .line 12
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    const-string v1, "SMALLVIDEO"

    const-string v2, "smallvideo"

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2, v9}, Lcom/baidu/mobads/nativecpu/CPUDataType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->SMALLVIDEO:Lcom/baidu/mobads/nativecpu/CPUDataType;

    .line 13
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    const-string v1, "RECALLNEWS"

    const-string v2, "recallNews"

    const/4 v10, 0x7

    const/16 v11, 0x8

    invoke-direct {v0, v1, v10, v2, v11}, Lcom/baidu/mobads/nativecpu/CPUDataType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->RECALLNEWS:Lcom/baidu/mobads/nativecpu/CPUDataType;

    .line 14
    new-instance v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    const-string v1, "POLICETASK"

    const-string v2, "policetask"

    const/16 v12, 0x9

    invoke-direct {v0, v1, v11, v2, v12}, Lcom/baidu/mobads/nativecpu/CPUDataType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->POLICETASK:Lcom/baidu/mobads/nativecpu/CPUDataType;

    .line 5
    new-array v0, v12, [Lcom/baidu/mobads/nativecpu/CPUDataType;

    sget-object v1, Lcom/baidu/mobads/nativecpu/CPUDataType;->NEWS:Lcom/baidu/mobads/nativecpu/CPUDataType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/nativecpu/CPUDataType;->IMAGE:Lcom/baidu/mobads/nativecpu/CPUDataType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/nativecpu/CPUDataType;->VIDEO:Lcom/baidu/mobads/nativecpu/CPUDataType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/nativecpu/CPUDataType;->TOPIC:Lcom/baidu/mobads/nativecpu/CPUDataType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/nativecpu/CPUDataType;->AD:Lcom/baidu/mobads/nativecpu/CPUDataType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/baidu/mobads/nativecpu/CPUDataType;->HOTDOC:Lcom/baidu/mobads/nativecpu/CPUDataType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/baidu/mobads/nativecpu/CPUDataType;->SMALLVIDEO:Lcom/baidu/mobads/nativecpu/CPUDataType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/baidu/mobads/nativecpu/CPUDataType;->RECALLNEWS:Lcom/baidu/mobads/nativecpu/CPUDataType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/baidu/mobads/nativecpu/CPUDataType;->POLICETASK:Lcom/baidu/mobads/nativecpu/CPUDataType;

    aput-object v1, v0, v11

    sput-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->c:[Lcom/baidu/mobads/nativecpu/CPUDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput-object p3, p0, Lcom/baidu/mobads/nativecpu/CPUDataType;->a:Ljava/lang/String;

    .line 21
    iput p4, p0, Lcom/baidu/mobads/nativecpu/CPUDataType;->b:I

    return-void
.end method

.method public static parseType(Ljava/lang/String;)Lcom/baidu/mobads/nativecpu/CPUDataType;
    .locals 5

    .line 33
    invoke-static {}, Lcom/baidu/mobads/nativecpu/CPUDataType;->values()[Lcom/baidu/mobads/nativecpu/CPUDataType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    if-eqz v3, :cond_0

    .line 34
    iget-object v4, v3, Lcom/baidu/mobads/nativecpu/CPUDataType;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 35
    iget-object v4, v3, Lcom/baidu/mobads/nativecpu/CPUDataType;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/nativecpu/CPUDataType;
    .locals 1

    .line 5
    const-class v0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/nativecpu/CPUDataType;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/nativecpu/CPUDataType;
    .locals 1

    .line 5
    sget-object v0, Lcom/baidu/mobads/nativecpu/CPUDataType;->c:[Lcom/baidu/mobads/nativecpu/CPUDataType;

    invoke-virtual {v0}, [Lcom/baidu/mobads/nativecpu/CPUDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/nativecpu/CPUDataType;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/baidu/mobads/nativecpu/CPUDataType;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/baidu/mobads/nativecpu/CPUDataType;->b:I

    return v0
.end method
