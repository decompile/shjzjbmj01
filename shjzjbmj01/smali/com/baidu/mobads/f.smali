.class Lcom/baidu/mobads/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

.field final synthetic b:Lcom/baidu/mobads/e;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/e;Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/baidu/mobads/f;->b:Lcom/baidu/mobads/e;

    iput-object p2, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-string v0, "AdLoaded"

    .line 48
    iget-object v1, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/baidu/mobads/f;->b:Lcom/baidu/mobads/e;

    iget-object v0, v0, Lcom/baidu/mobads/e;->a:Lcom/baidu/mobads/BaiduHybridAdManager;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduHybridAdManager;->a(Lcom/baidu/mobads/BaiduHybridAdManager;)Lcom/baidu/mobads/production/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/production/c/a;->start()V

    goto :goto_0

    :cond_0
    const-string v0, "AdStarted"

    .line 50
    iget-object v1, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/baidu/mobads/f;->b:Lcom/baidu/mobads/e;

    iget-object v0, v0, Lcom/baidu/mobads/e;->a:Lcom/baidu/mobads/BaiduHybridAdManager;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduHybridAdManager;->b(Lcom/baidu/mobads/BaiduHybridAdManager;)Lcom/baidu/mobads/BaiduHybridAdViewListener;

    move-result-object v0

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/BaiduHybridAdViewListener;->onAdShow(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "AdError"

    .line 52
    iget-object v3, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v3}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const-string v0, "AdUserClick"

    .line 54
    iget-object v3, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v3}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55
    iget-object v0, p0, Lcom/baidu/mobads/f;->b:Lcom/baidu/mobads/e;

    iget-object v0, v0, Lcom/baidu/mobads/e;->a:Lcom/baidu/mobads/BaiduHybridAdManager;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduHybridAdManager;->b(Lcom/baidu/mobads/BaiduHybridAdManager;)Lcom/baidu/mobads/BaiduHybridAdViewListener;

    move-result-object v0

    invoke-interface {v0, v2, v1}, Lcom/baidu/mobads/BaiduHybridAdViewListener;->onAdClick(ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "AdUserClose"

    .line 56
    iget-object v1, p0, Lcom/baidu/mobads/f;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method
