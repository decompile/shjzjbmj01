.class public Lcom/baidu/mobads/MobRewardVideoImpl;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static activityName:Ljava/lang/String;

.field private static b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field public static mAdContainer:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

.field public static mContext:Lcom/baidu/mobads/interfaces/IXAdContainerContext;

.field public static mVideoPlaying:Z


# instance fields
.field private a:Landroid/app/Activity;

.field private c:Ljava/lang/Object;

.field private d:[Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 28
    iput-object v0, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->d:[Ljava/lang/reflect/Method;

    .line 38
    iput-object p1, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->a:Landroid/app/Activity;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/Class;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 114
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v0

    .line 115
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const/4 v2, 0x0

    .line 118
    :try_start_0
    iget-object v3, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->a:Landroid/app/Activity;

    invoke-static {v3}, Lcom/baidu/mobads/g/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 119
    iget-object v4, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->a:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 120
    new-instance v5, Ldalvik/system/DexClassLoader;

    invoke-direct {v5, v3, v4, v2, v1}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    const/4 v1, 0x1

    .line 121
    invoke-static {p1, v1, v5}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 123
    invoke-virtual {v0, p1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    move-object p1, v2

    .line 126
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "jar.path=, clz="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/q;->i(Ljava/lang/String;)I

    return-object p1
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 7

    .line 94
    invoke-static {}, Lcom/baidu/mobads/g/b;->d()Ldalvik/system/DexClassLoader;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    :try_start_0
    const-string v0, "com.baidu.mobads.container.rewardvideo.RemoteRewardActivity"

    .line 98
    invoke-direct {p0, v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->b:Ljava/lang/Class;

    goto :goto_0

    :cond_0
    const-string v3, "com.baidu.mobads.container.rewardvideo.RemoteRewardActivity"

    .line 100
    invoke-static {v3, v2, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->b:Ljava/lang/Class;

    .line 102
    :goto_0
    sget-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->d:[Ljava/lang/reflect/Method;

    .line 104
    sget-object v0, Lcom/baidu/mobads/MobRewardVideoImpl;->b:Ljava/lang/Class;

    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Landroid/app/Activity;

    aput-object v5, v4, v1

    const-class v5, Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    aput-object v5, v4, v2

    const-class v5, Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    const/4 v6, 0x2

    aput-object v5, v4, v6

    .line 105
    invoke-virtual {v0, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 106
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->a:Landroid/app/Activity;

    aput-object v4, v3, v1

    sget-object v4, Lcom/baidu/mobads/MobRewardVideoImpl;->mContext:Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    aput-object v4, v3, v2

    sget-object v4, Lcom/baidu/mobads/MobRewardVideoImpl;->mAdContainer:Lcom/baidu/mobads/interfaces/IXRewardVideoAdContainer;

    aput-object v4, v3, v6

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->c:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string v0, "onCreate"

    .line 110
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-direct {p0, v0, v2}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 132
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x1

    if-eqz p2, :cond_0

    array-length v4, p2

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x2

    aput-object p2, v1, v3

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/q;->d([Ljava/lang/Object;)I

    .line 133
    invoke-direct {p0, p1}, Lcom/baidu/mobads/MobRewardVideoImpl;->b(Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object p1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 135
    array-length v0, p2

    if-nez v0, :cond_1

    goto :goto_1

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->c:Ljava/lang/Object;

    invoke-virtual {p1, v0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 136
    :cond_2
    :goto_1
    iget-object p2, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->c:Ljava/lang/Object;

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 142
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    :cond_3
    :goto_2
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 6

    .line 147
    iget-object v0, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->d:[Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->d:[Ljava/lang/reflect/Method;

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v0, v3

    .line 153
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 p1, 0x1

    .line 154
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private varargs b(Ljava/lang/String;[Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    .line 163
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    const/4 v3, 0x1

    if-eqz p2, :cond_0

    array-length v4, p2

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/utils/q;->d([Ljava/lang/Object;)I

    .line 164
    invoke-direct {p0, p1}, Lcom/baidu/mobads/MobRewardVideoImpl;->b(Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object p1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 166
    array-length v1, p2

    if-nez v1, :cond_1

    goto :goto_1

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->c:Ljava/lang/Object;

    invoke-virtual {p1, v1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    .line 167
    :cond_2
    :goto_1
    iget-object p2, p0, Lcom/baidu/mobads/MobRewardVideoImpl;->c:Ljava/lang/Object;

    new-array v1, v0, [Ljava/lang/Object;

    invoke-virtual {p1, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 173
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/utils/q;->d(Ljava/lang/Throwable;)I

    :cond_3
    return v0
.end method

.method public static getActivityClass()Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 181
    const-class v0, Lcom/baidu/mobads/production/rewardvideo/MobRewardVideoActivity;

    .line 182
    sget-object v1, Lcom/baidu/mobads/MobRewardVideoImpl;->activityName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 184
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/MobRewardVideoImpl;->activityName:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 186
    invoke-static {}, Lcom/baidu/mobads/utils/q;->a()Lcom/baidu/mobads/utils/q;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/baidu/mobads/utils/q;->e(Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-object v0
.end method

.method public static setActivityName(Ljava/lang/String;)V
    .locals 0

    .line 193
    sput-object p0, Lcom/baidu/mobads/MobRewardVideoImpl;->activityName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    const-string v0, "finish"

    const/4 v1, 0x0

    .line 89
    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    const-string v0, "onAttachedToWindow"

    const/4 v1, 0x0

    .line 78
    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 2

    const-string v0, "onBackPressed"

    const/4 v1, 0x0

    .line 63
    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/MobRewardVideoImpl;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x1

    .line 42
    sput-boolean v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z

    .line 43
    sget-object v1, Lcom/baidu/mobads/MobRewardVideoImpl;->mContext:Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/baidu/mobads/MobRewardVideoImpl;->mContext:Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 44
    sget-object v1, Lcom/baidu/mobads/MobRewardVideoImpl;->mContext:Lcom/baidu/mobads/interfaces/IXAdContainerContext;

    invoke-interface {v1}, Lcom/baidu/mobads/interfaces/IXAdContainerContext;->getAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->setAdHasDisplayed(Z)V

    .line 46
    :cond_0
    invoke-direct {p0, p1}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v0, 0x0

    .line 58
    sput-boolean v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z

    const-string v1, "onDestroy"

    .line 59
    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    const/4 v0, 0x0

    .line 83
    sput-boolean v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z

    const-string v1, "onDetachedFromWindow"

    .line 84
    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v1, v0}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    const-string v0, "onPause"

    const/4 v1, 0x0

    .line 50
    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    const-string v0, "onResume"

    const/4 v1, 0x0

    .line 54
    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const-string v0, "onTouchEvent"

    const/4 v1, 0x1

    .line 72
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/MobRewardVideoImpl;->b(Ljava/lang/String;[Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    const-string v0, "onWindowFocusChanged"

    const/4 v1, 0x1

    .line 68
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/baidu/mobads/MobRewardVideoImpl;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
