.class public Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/AppActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActionBarColorTheme"
.end annotation


# static fields
.field public static final ACTION_BAR_BLACK_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

.field public static final ACTION_BAR_BLUE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

.field public static final ACTION_BAR_COFFEE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

.field public static final ACTION_BAR_GREEN_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

.field public static final ACTION_BAR_NAVYBLUE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

.field public static final ACTION_BAR_RED_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

.field public static final ACTION_BAR_WHITE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;


# instance fields
.field public backgroundColor:I

.field public closeColor:I

.field public progressColor:I

.field public titleColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 72
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    const v1, -0xa9932e

    const v2, -0x5b5b5c

    const v3, -0x686869

    const v4, -0x50506

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(IIII)V

    sput-object v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->ACTION_BAR_WHITE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    .line 75
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    const/16 v2, -0x30de

    const/4 v3, -0x1

    const v4, -0x13bfc4

    invoke-direct {v0, v3, v3, v2, v4}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(IIII)V

    sput-object v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->ACTION_BAR_RED_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    .line 78
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    const v4, -0xda3f5f

    invoke-direct {v0, v3, v3, v1, v4}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(IIII)V

    sput-object v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->ACTION_BAR_GREEN_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    .line 81
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    const v1, 0xffcf22

    const v4, -0xd8d4b2

    invoke-direct {v0, v3, v3, v1, v4}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(IIII)V

    sput-object v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->ACTION_BAR_NAVYBLUE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    .line 84
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    const v1, -0xd3a558

    invoke-direct {v0, v3, v3, v2, v1}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(IIII)V

    sput-object v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->ACTION_BAR_BLUE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    .line 87
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    const v1, -0xabbdbe

    invoke-direct {v0, v3, v3, v2, v1}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(IIII)V

    sput-object v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->ACTION_BAR_COFFEE_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    .line 90
    new-instance v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    const v1, -0xd1ccca

    invoke-direct {v0, v3, v3, v2, v1}, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;-><init>(IIII)V

    sput-object v0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->ACTION_BAR_BLACK_THEME:Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput p1, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->closeColor:I

    .line 104
    iput p2, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->titleColor:I

    .line 105
    iput p3, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->progressColor:I

    .line 106
    iput p4, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->backgroundColor:I

    return-void
.end method

.method public constructor <init>(Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;)V
    .locals 1

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iget v0, p1, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->closeColor:I

    iput v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->closeColor:I

    .line 111
    iget v0, p1, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->titleColor:I

    iput v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->titleColor:I

    .line 112
    iget v0, p1, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->progressColor:I

    iput v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->progressColor:I

    .line 113
    iget p1, p1, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->backgroundColor:I

    iput p1, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->backgroundColor:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 150
    check-cast p1, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;

    .line 151
    iget v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->backgroundColor:I

    iget v1, p1, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->backgroundColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->titleColor:I

    iget v1, p1, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->titleColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->closeColor:I

    iget v1, p1, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->closeColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->progressColor:I

    iget p1, p1, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->progressColor:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getBackgroundColor()I
    .locals 1

    .line 141
    iget v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->backgroundColor:I

    return v0
.end method

.method public getCloseColor()I
    .locals 1

    .line 117
    iget v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->closeColor:I

    return v0
.end method

.method public getProgressColor()I
    .locals 1

    .line 133
    iget v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->progressColor:I

    return v0
.end method

.method public getTitleColor()I
    .locals 1

    .line 125
    iget v0, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->titleColor:I

    return v0
.end method

.method public setBackgroundColor(I)V
    .locals 0

    .line 145
    iput p1, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->backgroundColor:I

    return-void
.end method

.method public setCloseColor(I)V
    .locals 0

    .line 121
    iput p1, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->closeColor:I

    return-void
.end method

.method public setProgressColor(I)V
    .locals 0

    .line 137
    iput p1, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->progressColor:I

    return-void
.end method

.method public setTitleColor(I)V
    .locals 0

    .line 129
    iput p1, p0, Lcom/baidu/mobads/AppActivity$ActionBarColorTheme;->titleColor:I

    return-void
.end method
