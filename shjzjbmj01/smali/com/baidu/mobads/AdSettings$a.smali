.class public final enum Lcom/baidu/mobads/AdSettings$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/AdSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/AdSettings$a;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final enum a:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum b:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum c:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum d:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum e:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum f:Lcom/baidu/mobads/AdSettings$a;

.field public static final enum g:Lcom/baidu/mobads/AdSettings$a;

.field private static final synthetic i:[Lcom/baidu/mobads/AdSettings$a;


# instance fields
.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 73
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const-string v1, "PRIMARY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->a:Lcom/baidu/mobads/AdSettings$a;

    .line 75
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const-string v1, "JUNIOR"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->b:Lcom/baidu/mobads/AdSettings$a;

    .line 77
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const-string v1, "SENIOR"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->c:Lcom/baidu/mobads/AdSettings$a;

    .line 79
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const-string v1, "SPECIALTY"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5, v5}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->d:Lcom/baidu/mobads/AdSettings$a;

    .line 81
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const-string v1, "BACHELOR"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6, v6}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->e:Lcom/baidu/mobads/AdSettings$a;

    .line 83
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const-string v1, "MASTER"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7, v7}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->f:Lcom/baidu/mobads/AdSettings$a;

    .line 85
    new-instance v0, Lcom/baidu/mobads/AdSettings$a;

    const-string v1, "DOCTOR"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8, v8}, Lcom/baidu/mobads/AdSettings$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->g:Lcom/baidu/mobads/AdSettings$a;

    const/4 v0, 0x7

    .line 68
    new-array v0, v0, [Lcom/baidu/mobads/AdSettings$a;

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->a:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->b:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->c:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->d:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->e:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->f:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/baidu/mobads/AdSettings$a;->g:Lcom/baidu/mobads/AdSettings$a;

    aput-object v1, v0, v8

    sput-object v0, Lcom/baidu/mobads/AdSettings$a;->i:[Lcom/baidu/mobads/AdSettings$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 90
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 91
    iput p3, p0, Lcom/baidu/mobads/AdSettings$a;->h:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 95
    iget v0, p0, Lcom/baidu/mobads/AdSettings$a;->h:I

    return v0
.end method
