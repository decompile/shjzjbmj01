.class public final Lcom/baidu/mobads/AdView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# static fields
.field protected static final P_VERSION:Ljava/lang/String; = "3.61"


# instance fields
.field a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

.field private b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private c:Lcom/baidu/mobads/production/a/a;

.field private d:Lcom/baidu/mobads/AdViewListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 75
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 36
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/baidu/mobads/AdView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 38
    new-instance p1, Lcom/baidu/mobads/a;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/a;-><init>(Lcom/baidu/mobads/AdView;)V

    iput-object p1, p0, Lcom/baidu/mobads/AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ZLcom/baidu/mobads/AdSize;Ljava/lang/String;)V
    .locals 0

    .line 109
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p4, 0x0

    invoke-direct {p2, p4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lcom/baidu/mobads/AdView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 38
    new-instance p2, Lcom/baidu/mobads/a;

    invoke-direct {p2, p0}, Lcom/baidu/mobads/a;-><init>(Lcom/baidu/mobads/AdView;)V

    iput-object p2, p0, Lcom/baidu/mobads/AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    .line 110
    new-instance p2, Lcom/baidu/mobads/component/XAdView;

    invoke-direct {p2, p1}, Lcom/baidu/mobads/component/XAdView;-><init>(Landroid/content/Context;)V

    .line 111
    new-instance p4, Lcom/baidu/mobads/production/a/a;

    invoke-direct {p4, p1, p2, p5, p3}, Lcom/baidu/mobads/production/a/a;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;Z)V

    iput-object p4, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    .line 112
    iget-object p1, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    const-string p3, "AdLoaded"

    iget-object p4, p0, Lcom/baidu/mobads/AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, p3, p4}, Lcom/baidu/mobads/production/a/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 113
    iget-object p1, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    const-string p3, "AdError"

    iget-object p4, p0, Lcom/baidu/mobads/AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, p3, p4}, Lcom/baidu/mobads/production/a/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 114
    iget-object p1, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    const-string p3, "AdStarted"

    iget-object p4, p0, Lcom/baidu/mobads/AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, p3, p4}, Lcom/baidu/mobads/production/a/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 115
    iget-object p1, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    const-string p3, "AdUserClick"

    iget-object p4, p0, Lcom/baidu/mobads/AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, p3, p4}, Lcom/baidu/mobads/production/a/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 116
    iget-object p1, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    const-string p3, "AdUserClose"

    iget-object p4, p0, Lcom/baidu/mobads/AdView;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {p1, p3, p4}, Lcom/baidu/mobads/production/a/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 117
    new-instance p1, Lcom/baidu/mobads/c;

    invoke-direct {p1, p0}, Lcom/baidu/mobads/c;-><init>(Lcom/baidu/mobads/AdView;)V

    invoke-virtual {p2, p1}, Lcom/baidu/mobads/component/XAdView;->setListener(Lcom/baidu/mobads/component/XAdView$Listener;)V

    .line 150
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 p3, -0x1

    invoke-direct {p1, p3, p3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p2, p1}, Lcom/baidu/mobads/AdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/AdSize;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 98
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/baidu/mobads/AdView;-><init>(Landroid/content/Context;ZLcom/baidu/mobads/AdSize;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .line 86
    sget-object v0, Lcom/baidu/mobads/AdSize;->Banner:Lcom/baidu/mobads/AdSize;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1, v0, p2}, Lcom/baidu/mobads/AdView;-><init>(Landroid/content/Context;ZLcom/baidu/mobads/AdSize;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;ZLcom/baidu/mobads/AdSize;Ljava/lang/String;)V
    .locals 6

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 105
    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/AdView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ZLcom/baidu/mobads/AdSize;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/AdView;)Lcom/baidu/mobads/AdViewListener;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/baidu/mobads/AdView;->d:Lcom/baidu/mobads/AdViewListener;

    return-object p0
.end method

.method private a()V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/baidu/mobads/AdView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/baidu/mobads/AdView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 159
    iget-object v0, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/a/a;->request()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/AdView;)Lcom/baidu/mobads/production/a/a;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobads/AdView;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/baidu/mobads/AdView;->a()V

    return-void
.end method

.method public static setAppSec(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public static setAppSid(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 243
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/h;->setAppId(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/a/a;->p()V

    return-void
.end method

.method public setAlpha(F)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 12

    .line 168
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "AdView.setLayoutParams="

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 169
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    invoke-virtual {p0}, Lcom/baidu/mobads/AdView;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v6, 0x3

    aput-object v2, v1, v6

    invoke-virtual {p0}, Lcom/baidu/mobads/AdView;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v7, 0x4

    aput-object v2, v1, v7

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d([Ljava/lang/Object;)I

    .line 171
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 172
    iget v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 174
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 175
    invoke-virtual {p0}, Lcom/baidu/mobads/AdView;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "window"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 177
    iget v8, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 178
    iget v9, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 179
    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 180
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v10

    invoke-virtual {v10}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v10

    new-array v7, v7, [Ljava/lang/Object;

    const-string v11, "AdView.setLayoutParams"

    aput-object v11, v7, v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v5

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v7, v6

    invoke-interface {v10, v7}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d([Ljava/lang/Object;)I

    if-gtz v0, :cond_0

    .line 183
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    int-to-float v7, v0

    const/high16 v10, 0x43480000    # 200.0f

    mul-float v10, v10, v2

    cmpg-float v7, v7, v10

    if-gez v7, :cond_1

    float-to-int v0, v10

    :cond_1
    :goto_0
    if-gtz v1, :cond_2

    .line 189
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3e19999a    # 0.15f

    mul-float v1, v1, v2

    float-to-int v1, v1

    goto :goto_1

    :cond_2
    if-lez v1, :cond_3

    int-to-float v7, v1

    const/high16 v8, 0x41f00000    # 30.0f

    mul-float v2, v2, v8

    cmpg-float v7, v7, v2

    if-gez v7, :cond_3

    float-to-int v1, v2

    .line 193
    :cond_3
    :goto_1
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 194
    iput v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 195
    iget-object v0, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/a/a;->a()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 196
    iget-object v0, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/a/a;->a()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    iget v1, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/vo/d;->d(I)V

    .line 197
    iget-object v0, p0, Lcom/baidu/mobads/AdView;->c:Lcom/baidu/mobads/production/a/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/a/a;->a()Lcom/baidu/mobads/vo/d;

    move-result-object v0

    iget v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/vo/d;->e(I)V

    .line 199
    :cond_4
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "AdView.setLayoutParams adapter"

    aput-object v2, v1, v3

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 200
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d([Ljava/lang/Object;)I

    .line 201
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setListener(Lcom/baidu/mobads/AdViewListener;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/baidu/mobads/AdView;->d:Lcom/baidu/mobads/AdViewListener;

    return-void
.end method
