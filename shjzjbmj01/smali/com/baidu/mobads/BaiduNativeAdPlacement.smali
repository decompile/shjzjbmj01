.class public Lcom/baidu/mobads/BaiduNativeAdPlacement;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/baidu/mobads/BaiduNativeH5AdView;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 17
    iput-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->e:Z

    const/4 v1, 0x0

    .line 18
    iput-object v1, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->f:Ljava/lang/ref/WeakReference;

    .line 20
    iput v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->g:I

    const/4 v0, 0x1

    .line 21
    iput v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->h:I

    .line 22
    iput v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->i:I

    return-void
.end method

.method public static setAppSid(Ljava/lang/String;)V
    .locals 1

    .line 56
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/utils/h;->setAppId(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getAdResponse()Lcom/baidu/mobads/interfaces/IXAdResponseInfo;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->b:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    return-object v0
.end method

.method public getAdView()Lcom/baidu/mobads/BaiduNativeH5AdView;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/baidu/mobads/BaiduNativeH5AdView;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getApId()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected getPosistionId()I
    .locals 1

    .line 131
    iget v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->h:I

    return v0
.end method

.method protected getRequestStarted()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->e:Z

    return v0
.end method

.method protected getSequenceId()I
    .locals 1

    .line 135
    iget v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->i:I

    return v0
.end method

.method protected getSessionId()I
    .locals 1

    .line 120
    iget v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->g:I

    return v0
.end method

.method public hasValidResponse()Z
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->b:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->isAdAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected isAdAvailable()Z
    .locals 7

    .line 79
    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->b:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->b:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->b:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdResponseInfo;->getPrimaryAdInstanceInfo()Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/interfaces/IXAdInstanceInfo;->getCreateTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x1b7740

    cmp-long v0, v3, v5

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 83
    iget-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->c:Z

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public isAdDataLoaded()Z
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->getAdView()Lcom/baidu/mobads/BaiduNativeH5AdView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->isAdDataLoaded()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected isWinSended()Z
    .locals 1

    .line 88
    iget-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->d:Z

    return v0
.end method

.method public setAdResponse(Lcom/baidu/mobads/interfaces/IXAdResponseInfo;)V
    .locals 1

    const/4 v0, 0x0

    .line 60
    iput-boolean v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->d:Z

    .line 61
    iput-object p1, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->b:Lcom/baidu/mobads/interfaces/IXAdResponseInfo;

    return-void
.end method

.method public setAdView(Lcom/baidu/mobads/BaiduNativeH5AdView;)V
    .locals 1

    .line 36
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->f:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public setApId(Ljava/lang/String;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->a:Ljava/lang/String;

    return-void
.end method

.method protected setClicked(Z)V
    .locals 0

    .line 93
    iput-boolean p1, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->c:Z

    return-void
.end method

.method public setPositionId(I)V
    .locals 1

    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    return-void

    .line 127
    :cond_0
    iput p1, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->h:I

    return-void
.end method

.method protected setRequestStarted(Z)V
    .locals 0

    .line 29
    iput-boolean p1, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->e:Z

    return-void
.end method

.method public setSessionId(I)V
    .locals 1

    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    return-void

    .line 115
    :cond_0
    iput p1, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->g:I

    .line 116
    invoke-static {}, Lcom/baidu/mobads/g;->a()Lcom/baidu/mobads/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/g;->a(I)I

    move-result p1

    iput p1, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->i:I

    return-void
.end method

.method protected setWinSended(Z)V
    .locals 0

    .line 97
    iput-boolean p1, p0, Lcom/baidu/mobads/BaiduNativeAdPlacement;->d:Z

    return-void
.end method
