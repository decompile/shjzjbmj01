.class public Lcom/baidu/mobads/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Landroid/content/Context;

.field private static b:J

.field private static volatile d:Lcom/baidu/mobads/j;


# instance fields
.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/j;->c:Ljava/util/HashMap;

    return-void
.end method

.method public static a()Lcom/baidu/mobads/j;
    .locals 2

    .line 23
    sget-object v0, Lcom/baidu/mobads/j;->d:Lcom/baidu/mobads/j;

    if-nez v0, :cond_1

    .line 24
    const-class v0, Lcom/baidu/mobads/j;

    monitor-enter v0

    .line 25
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/j;->d:Lcom/baidu/mobads/j;

    if-nez v1, :cond_0

    .line 26
    new-instance v1, Lcom/baidu/mobads/j;

    invoke-direct {v1}, Lcom/baidu/mobads/j;-><init>()V

    sput-object v1, Lcom/baidu/mobads/j;->d:Lcom/baidu/mobads/j;

    .line 28
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 30
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobads/j;->d:Lcom/baidu/mobads/j;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/j;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/j;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/baidu/mobads/j;->b:J

    .line 43
    iget-object v0, p0, Lcom/baidu/mobads/j;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    sget-wide v1, Lcom/baidu/mobads/j;->b:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 0

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    sput-object p1, Lcom/baidu/mobads/j;->a:Landroid/content/Context;

    .line 36
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/baidu/mobads/j;->c:Ljava/util/HashMap;

    return-void
.end method

.method public b()V
    .locals 5

    .line 53
    invoke-static {}, Lcom/baidu/mobads/b/a;->a()Lcom/baidu/mobads/b/a;

    move-result-object v0

    sget-object v1, Lcom/baidu/mobads/j;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/baidu/mobads/j;->c:Ljava/util/HashMap;

    const/16 v3, 0x321

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/baidu/mobads/b/a;->a(Landroid/content/Context;ILcom/baidu/mobads/interfaces/IXAdInstanceInfo;Ljava/util/HashMap;)V

    .line 54
    iget-object v0, p0, Lcom/baidu/mobads/j;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method
