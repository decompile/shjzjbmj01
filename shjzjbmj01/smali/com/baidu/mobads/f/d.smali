.class public Lcom/baidu/mobads/f/d;
.super Ljava/util/concurrent/AbstractExecutorService;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/f/d$a;,
        Lcom/baidu/mobads/f/d$b;
    }
.end annotation


# static fields
.field private static final n:Lcom/baidu/mobads/f/b;

.field private static final o:Ljava/lang/RuntimePermission;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final b:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lcom/baidu/mobads/f/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/concurrent/locks/Condition;

.field private f:I

.field private g:J

.field private volatile h:Ljava/util/concurrent/ThreadFactory;

.field private volatile i:Lcom/baidu/mobads/f/b;

.field private volatile j:J

.field private volatile k:Z

.field private volatile l:I

.field private volatile m:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 107
    new-instance v0, Lcom/baidu/mobads/f/d$a;

    invoke-direct {v0}, Lcom/baidu/mobads/f/d$a;-><init>()V

    sput-object v0, Lcom/baidu/mobads/f/d;->n:Lcom/baidu/mobads/f/b;

    .line 110
    new-instance v0, Ljava/lang/RuntimePermission;

    const-string v1, "modifyThread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimePermission;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/baidu/mobads/f/d;->o:Ljava/lang/RuntimePermission;

    return-void
.end method

.method public constructor <init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;",
            "Ljava/util/concurrent/ThreadFactory;",
            ")V"
        }
    .end annotation

    .line 522
    sget-object v8, Lcom/baidu/mobads/f/d;->n:Lcom/baidu/mobads/f/b;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-wide v3, p3

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/baidu/mobads/f/d;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Lcom/baidu/mobads/f/b;)V

    return-void
.end method

.method public constructor <init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Lcom/baidu/mobads/f/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;",
            "Ljava/util/concurrent/ThreadFactory;",
            "Lcom/baidu/mobads/f/b;",
            ")V"
        }
    .end annotation

    .line 542
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    .line 36
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/high16 v1, -0x20000000

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/baidu/mobads/f/d;->a(II)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 85
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 87
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    .line 89
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/f/d;->e:Ljava/util/concurrent/locks/Condition;

    if-ltz p1, :cond_1

    if-lez p2, :cond_1

    if-lt p2, p1, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v2, p3, v0

    if-ltz v2, :cond_1

    if-eqz p6, :cond_0

    if-eqz p7, :cond_0

    if-eqz p8, :cond_0

    .line 552
    iput p1, p0, Lcom/baidu/mobads/f/d;->l:I

    .line 553
    iput p2, p0, Lcom/baidu/mobads/f/d;->m:I

    .line 554
    iput-object p6, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    .line 555
    invoke-virtual {p5, p3, p4}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/baidu/mobads/f/d;->j:J

    .line 556
    iput-object p7, p0, Lcom/baidu/mobads/f/d;->h:Ljava/util/concurrent/ThreadFactory;

    .line 557
    iput-object p8, p0, Lcom/baidu/mobads/f/d;->i:Lcom/baidu/mobads/f/b;

    return-void

    .line 550
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1

    .line 547
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method private static a(I)I
    .locals 1

    const/high16 v0, -0x20000000

    and-int/2addr p0, v0

    return p0
.end method

.method private static a(II)I
    .locals 0

    or-int/2addr p0, p1

    return p0
.end method

.method private a(Lcom/baidu/mobads/f/d$b;Z)V
    .locals 6

    if-eqz p2, :cond_0

    .line 390
    invoke-direct {p0}, Lcom/baidu/mobads/f/d;->f()V

    .line 393
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 394
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 396
    :try_start_0
    iget-wide v1, p0, Lcom/baidu/mobads/f/d;->g:J

    iget-wide v3, p1, Lcom/baidu/mobads/f/d$b;->c:J

    const/4 v5, 0x0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/baidu/mobads/f/d;->g:J

    .line 397
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 402
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d;->a()V

    .line 404
    iget-object p1, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result p1

    const/high16 v0, 0x20000000

    .line 405
    invoke-static {p1, v0}, Lcom/baidu/mobads/f/d;->b(II)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    if-nez p2, :cond_3

    .line 407
    iget-boolean p2, p0, Lcom/baidu/mobads/f/d;->k:Z

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    goto :goto_0

    :cond_1
    iget p2, p0, Lcom/baidu/mobads/f/d;->l:I

    :goto_0
    if-nez p2, :cond_2

    .line 408
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 p2, 0x1

    .line 411
    :cond_2
    invoke-static {p1}, Lcom/baidu/mobads/f/d;->b(I)I

    move-result p1

    if-lt p1, p2, :cond_3

    return-void

    :cond_3
    const/4 p1, 0x0

    .line 415
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/f/d;->a(Ljava/lang/Runnable;Z)Z

    :cond_4
    return-void

    :catchall_0
    move-exception p1

    .line 399
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw p1
.end method

.method private a(Z)V
    .locals 5

    .line 249
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 250
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 252
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobads/f/d$b;

    .line 253
    iget-object v3, v2, Lcom/baidu/mobads/f/d$b;->a:Ljava/lang/Thread;

    .line 254
    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v2}, Lcom/baidu/mobads/f/d$b;->b()Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v4, :cond_1

    .line 256
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 259
    :catch_0
    :try_start_2
    invoke-virtual {v2}, Lcom/baidu/mobads/f/d$b;->c()V

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {v2}, Lcom/baidu/mobads/f/d$b;->c()V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    :goto_0
    if-eqz p1, :cond_0

    .line 267
    :cond_2
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_1
    move-exception p1

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw p1
.end method

.method private a(Ljava/lang/Runnable;Z)Z
    .locals 5

    .line 306
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 307
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->a(I)I

    move-result v1

    const/4 v2, 0x0

    if-ltz v1, :cond_1

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    .line 310
    iget-object v3, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    .line 313
    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    return v2

    .line 318
    :cond_1
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->b(I)I

    move-result v3

    const v4, 0x1fffffff

    if-ge v3, v4, :cond_b

    if-eqz p2, :cond_2

    .line 319
    iget v4, p0, Lcom/baidu/mobads/f/d;->l:I

    goto :goto_1

    :cond_2
    iget v4, p0, Lcom/baidu/mobads/f/d;->m:I

    :goto_1
    if-lt v3, v4, :cond_3

    goto/16 :goto_6

    .line 323
    :cond_3
    invoke-direct {p0, v0}, Lcom/baidu/mobads/f/d;->d(I)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 p2, 0x0

    .line 338
    :try_start_0
    new-instance v0, Lcom/baidu/mobads/f/d$b;

    invoke-direct {v0, p0, p1}, Lcom/baidu/mobads/f/d$b;-><init>(Lcom/baidu/mobads/f/d;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 339
    :try_start_1
    iget-object p2, v0, Lcom/baidu/mobads/f/d$b;->a:Ljava/lang/Thread;

    const/4 v1, 0x1

    if-eqz p2, :cond_8

    .line 341
    iget-object v3, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 342
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 344
    :try_start_2
    iget-object v4, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    invoke-static {v4}, Lcom/baidu/mobads/f/d;->a(I)I

    move-result v4

    if-ltz v4, :cond_5

    if-nez v4, :cond_4

    if-nez p1, :cond_4

    goto :goto_2

    :cond_4
    const/4 p1, 0x0

    goto :goto_3

    .line 348
    :cond_5
    :goto_2
    invoke-virtual {p2}, Ljava/lang/Thread;->isAlive()Z

    move-result p1

    if-nez p1, :cond_7

    .line 351
    iget-object p1, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 352
    iget-object p1, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result p1

    .line 353
    iget v4, p0, Lcom/baidu/mobads/f/d;->f:I

    if-le p1, v4, :cond_6

    .line 354
    iput p1, p0, Lcom/baidu/mobads/f/d;->f:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_6
    const/4 p1, 0x1

    .line 359
    :goto_3
    :try_start_3
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    if-eqz p1, :cond_8

    .line 362
    invoke-virtual {p2}, Ljava/lang/Thread;->start()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    .line 349
    :cond_7
    :try_start_4
    new-instance p1, Ljava/lang/IllegalThreadStateException;

    invoke-direct {p1}, Ljava/lang/IllegalThreadStateException;-><init>()V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    .line 359
    :try_start_5
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_8
    const/4 v1, 0x0

    :goto_4
    if-nez v1, :cond_9

    .line 368
    invoke-direct {p0, v0}, Lcom/baidu/mobads/f/d;->b(Lcom/baidu/mobads/f/d$b;)V

    :cond_9
    return v1

    :catchall_1
    move-exception p1

    goto :goto_5

    :catchall_2
    move-exception p1

    move-object v0, p2

    :goto_5
    invoke-direct {p0, v0}, Lcom/baidu/mobads/f/d;->b(Lcom/baidu/mobads/f/d$b;)V

    throw p1

    .line 326
    :cond_a
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 327
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->a(I)I

    move-result v3

    if-eq v3, v1, :cond_1

    goto/16 :goto_0

    :cond_b
    :goto_6
    return v2
.end method

.method private static b(I)I
    .locals 1

    const v0, 0x1fffffff

    and-int/2addr p0, v0

    return p0
.end method

.method private b(Lcom/baidu/mobads/f/d$b;)V
    .locals 2

    .line 375
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 376
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    if-eqz p1, :cond_0

    .line 379
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    .line 381
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/baidu/mobads/f/d;->f()V

    .line 382
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :goto_1
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw p1
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 3

    .line 596
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 597
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 598
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/f/d;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 599
    :cond_0
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, p1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 600
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->b(I)I

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x0

    .line 601
    invoke-direct {p0, p1, v2}, Lcom/baidu/mobads/f/d;->a(Ljava/lang/Runnable;Z)Z

    goto :goto_0

    .line 603
    :cond_1
    invoke-direct {p0, p1, v2}, Lcom/baidu/mobads/f/d;->a(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 604
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/f/d;->a(Ljava/lang/Runnable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private static b(II)Z
    .locals 0

    if-ge p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static c(I)Z
    .locals 0

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static c(II)Z
    .locals 0

    if-lt p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private d(I)Z
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result p1

    return p1
.end method

.method private e(I)Z
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result p1

    return p1
.end method

.method private f()V
    .locals 1

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/f/d;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method private f(I)V
    .locals 3

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 181
    invoke-static {v0, p1}, Lcom/baidu/mobads/f/d;->c(II)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 182
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->b(I)I

    move-result v2

    invoke-static {p1, v2}, Lcom/baidu/mobads/f/d;->a(II)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private g()V
    .locals 4

    .line 221
    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 223
    sget-object v1, Lcom/baidu/mobads/f/d;->o:Ljava/lang/RuntimePermission;

    invoke-virtual {v0, v1}, Ljava/lang/SecurityManager;->checkPermission(Ljava/security/Permission;)V

    .line 224
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 225
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 227
    :try_start_0
    iget-object v2, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/baidu/mobads/f/d$b;

    .line 228
    iget-object v3, v3, Lcom/baidu/mobads/f/d$b;->a:Ljava/lang/Thread;

    invoke-virtual {v0, v3}, Ljava/lang/SecurityManager;->checkAccess(Ljava/lang/Thread;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 231
    :cond_0
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_1
    :goto_1
    return-void
.end method

.method private h()V
    .locals 3

    .line 237
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 238
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 240
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/baidu/mobads/f/d$b;

    .line 241
    invoke-virtual {v2}, Lcom/baidu/mobads/f/d$b;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 244
    :cond_0
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method private i()V
    .locals 1

    const/4 v0, 0x0

    .line 272
    invoke-direct {p0, v0}, Lcom/baidu/mobads/f/d;->a(Z)V

    return-void
.end method

.method private j()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .line 290
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    .line 291
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 292
    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->drainTo(Ljava/util/Collection;)I

    .line 293
    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    .line 294
    new-array v3, v2, [Ljava/lang/Runnable;

    invoke-interface {v0, v3}, Ljava/util/concurrent/BlockingQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Runnable;

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 295
    invoke-interface {v0, v5}, Ljava/util/concurrent/BlockingQueue;->remove(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 296
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private k()Ljava/lang/Runnable;
    .locals 8

    const/4 v0, 0x0

    :catch_0
    const/4 v1, 0x0

    .line 423
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    .line 424
    invoke-static {v2}, Lcom/baidu/mobads/f/d;->a(I)I

    move-result v3

    const/4 v4, 0x0

    if-ltz v3, :cond_2

    const/high16 v5, 0x20000000

    if-ge v3, v5, :cond_1

    .line 427
    iget-object v3, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 428
    :cond_1
    invoke-direct {p0}, Lcom/baidu/mobads/f/d;->f()V

    return-object v4

    .line 432
    :cond_2
    invoke-static {v2}, Lcom/baidu/mobads/f/d;->b(I)I

    move-result v3

    .line 435
    iget-boolean v5, p0, Lcom/baidu/mobads/f/d;->k:Z

    const/4 v6, 0x1

    if-nez v5, :cond_4

    iget v5, p0, Lcom/baidu/mobads/f/d;->l:I

    if-le v3, v5, :cond_3

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v5, 0x1

    .line 437
    :goto_2
    iget v7, p0, Lcom/baidu/mobads/f/d;->m:I

    if-gt v3, v7, :cond_5

    if-eqz v5, :cond_6

    if-eqz v1, :cond_6

    :cond_5
    if-gt v3, v6, :cond_9

    iget-object v3, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    .line 438
    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_4

    :cond_6
    if-eqz v5, :cond_7

    .line 446
    :try_start_0
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    iget-wide v2, p0, Lcom/baidu/mobads/f/d;->j:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 447
    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    goto :goto_3

    :cond_7
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    .line 448
    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    if-eqz v1, :cond_8

    return-object v1

    :cond_8
    const/4 v1, 0x1

    goto :goto_0

    .line 439
    :cond_9
    :goto_4
    invoke-direct {p0, v2}, Lcom/baidu/mobads/f/d;->e(I)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v4
.end method


# virtual methods
.method final a()V
    .locals 5

    .line 190
    :goto_0
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 191
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->c(I)Z

    move-result v1

    if-nez v1, :cond_3

    const/high16 v1, 0x40000000    # 2.0f

    .line 192
    invoke-static {v0, v1}, Lcom/baidu/mobads/f/d;->c(II)Z

    move-result v2

    if-nez v2, :cond_3

    .line 193
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->a(I)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 196
    :cond_0
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->b(I)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 197
    invoke-direct {p0, v0}, Lcom/baidu/mobads/f/d;->a(Z)V

    return-void

    .line 201
    :cond_1
    iget-object v2, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 202
    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 204
    :try_start_0
    iget-object v3, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/baidu/mobads/f/d;->a(II)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_2

    const/high16 v0, 0x60000000

    .line 206
    :try_start_1
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    :try_start_2
    iget-object v1, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-static {v0, v4}, Lcom/baidu/mobads/f/d;->a(II)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 209
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 214
    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v1

    .line 208
    :try_start_3
    iget-object v3, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-static {v0, v4}, Lcom/baidu/mobads/f/d;->a(II)I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 209
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 214
    :cond_2
    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_3
    :goto_1
    return-void
.end method

.method public a(Lcom/baidu/mobads/f/b;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 688
    iput-object p1, p0, Lcom/baidu/mobads/f/d;->i:Lcom/baidu/mobads/f/b;

    return-void

    .line 686
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method final a(Lcom/baidu/mobads/f/d$b;)V
    .locals 7

    .line 460
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 461
    iget-object v1, p1, Lcom/baidu/mobads/f/d$b;->b:Ljava/lang/Runnable;

    const/4 v2, 0x0

    .line 462
    iput-object v2, p1, Lcom/baidu/mobads/f/d$b;->b:Ljava/lang/Runnable;

    .line 463
    invoke-virtual {p1}, Lcom/baidu/mobads/f/d$b;->c()V

    :goto_0
    if-nez v1, :cond_1

    .line 466
    :try_start_0
    invoke-direct {p0}, Lcom/baidu/mobads/f/d;->k()Ljava/lang/Runnable;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 503
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/f/d;->a(Lcom/baidu/mobads/f/d$b;Z)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_3

    .line 467
    :cond_1
    :goto_1
    :try_start_1
    invoke-virtual {p1}, Lcom/baidu/mobads/f/d$b;->a()V

    .line 472
    iget-object v3, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    const/high16 v4, 0x20000000

    invoke-static {v3, v4}, Lcom/baidu/mobads/f/d;->c(II)Z

    move-result v3

    if-nez v3, :cond_2

    .line 473
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 474
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-static {v3, v4}, Lcom/baidu/mobads/f/d;->c(II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 475
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_3

    .line 476
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    const-wide/16 v3, 0x1

    .line 479
    :try_start_2
    invoke-virtual {p0, v0, v1}, Lcom/baidu/mobads/f/d;->a(Ljava/lang/Thread;Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 482
    :try_start_3
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 493
    :try_start_4
    invoke-virtual {p0, v1, v2}, Lcom/baidu/mobads/f/d;->a(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 497
    :try_start_5
    iget-wide v5, p1, Lcom/baidu/mobads/f/d$b;->c:J

    const/4 v1, 0x0

    add-long/2addr v5, v3

    iput-wide v5, p1, Lcom/baidu/mobads/f/d$b;->c:J

    .line 498
    invoke-virtual {p1}, Lcom/baidu/mobads/f/d$b;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v1, v2

    goto :goto_0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v2

    .line 491
    :try_start_6
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, v2}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    move-exception v2

    .line 488
    throw v2

    :catch_2
    move-exception v2

    .line 485
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 493
    :goto_2
    :try_start_7
    invoke-virtual {p0, v1, v2}, Lcom/baidu/mobads/f/d;->a(Ljava/lang/Runnable;Ljava/lang/Throwable;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception v0

    .line 497
    :try_start_8
    iget-wide v1, p1, Lcom/baidu/mobads/f/d$b;->c:J

    const/4 v5, 0x0

    add-long/2addr v1, v3

    iput-wide v1, p1, Lcom/baidu/mobads/f/d$b;->c:J

    .line 498
    invoke-virtual {p1}, Lcom/baidu/mobads/f/d$b;->c()V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :goto_3
    const/4 v1, 0x1

    .line 503
    invoke-direct {p0, p1, v1}, Lcom/baidu/mobads/f/d;->a(Lcom/baidu/mobads/f/d$b;Z)V

    throw v0
.end method

.method final a(Ljava/lang/Runnable;)V
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->i:Lcom/baidu/mobads/f/b;

    invoke-interface {v0, p1, p0}, Lcom/baidu/mobads/f/b;->a(Ljava/lang/Runnable;Lcom/baidu/mobads/f/d;)V

    return-void
.end method

.method protected a(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method protected a(Ljava/lang/Thread;Ljava/lang/Runnable;)V
    .locals 0

    return-void
.end method

.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 3

    .line 653
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide p1

    .line 654
    iget-object p3, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 655
    invoke-virtual {p3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 657
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/high16 v1, 0x60000000

    invoke-static {v0, v1}, Lcom/baidu/mobads/f/d;->c(II)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    const/4 p1, 0x0

    .line 665
    :goto_1
    invoke-virtual {p3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return p1

    .line 661
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 665
    invoke-virtual {p3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw p1
.end method

.method b()V
    .locals 0

    return-void
.end method

.method public c()Ljava/util/concurrent/ThreadFactory;
    .locals 1

    .line 681
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->h:Ljava/util/concurrent/ThreadFactory;

    return-object v0
.end method

.method public d()I
    .locals 4

    .line 837
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 838
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    const/4 v1, 0x0

    .line 841
    :try_start_0
    iget-object v2, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/baidu/mobads/f/d$b;

    .line 842
    invoke-virtual {v3}, Lcom/baidu/mobads/f/d$b;->d()Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 848
    :cond_1
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 2

    if-eqz p1, :cond_3

    .line 570
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 571
    invoke-static {v0}, Lcom/baidu/mobads/f/d;->b(I)I

    move-result v0

    .line 572
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d;->d()I

    move-result v1

    sub-int v1, v0, v1

    if-lez v1, :cond_0

    .line 575
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/d;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 576
    :cond_0
    iget v1, p0, Lcom/baidu/mobads/f/d;->m:I

    if-ge v0, v1, :cond_1

    const/4 v0, 0x0

    .line 579
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/f/d;->a(Ljava/lang/Runnable;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 580
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/d;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 584
    :cond_1
    invoke-direct {p0, p1}, Lcom/baidu/mobads/f/d;->b(Ljava/lang/Runnable;)V

    :cond_2
    :goto_0
    return-void

    .line 567
    :cond_3
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public isShutdown()Z
    .locals 1

    .line 639
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-static {v0}, Lcom/baidu/mobads/f/d;->c(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isTerminated()Z
    .locals 2

    .line 648
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/high16 v1, 0x60000000

    invoke-static {v0, v1}, Lcom/baidu/mobads/f/d;->c(II)Z

    move-result v0

    return v0
.end method

.method public shutdown()V
    .locals 2

    .line 609
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 610
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 612
    :try_start_0
    invoke-direct {p0}, Lcom/baidu/mobads/f/d;->g()V

    const/4 v1, 0x0

    .line 613
    invoke-direct {p0, v1}, Lcom/baidu/mobads/f/d;->f(I)V

    .line 614
    invoke-direct {p0}, Lcom/baidu/mobads/f/d;->i()V

    .line 615
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 619
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d;->a()V

    return-void

    :catchall_0
    move-exception v1

    .line 617
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public shutdownNow()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .line 624
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 625
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 627
    :try_start_0
    invoke-direct {p0}, Lcom/baidu/mobads/f/d;->g()V

    const/high16 v1, 0x20000000

    .line 628
    invoke-direct {p0, v1}, Lcom/baidu/mobads/f/d;->f(I)V

    .line 629
    invoke-direct {p0}, Lcom/baidu/mobads/f/d;->h()V

    .line 630
    invoke-direct {p0}, Lcom/baidu/mobads/f/d;->j()Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 632
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 634
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d;->a()V

    return-object v1

    :catchall_0
    move-exception v1

    .line 632
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 11

    .line 897
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 898
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 900
    :try_start_0
    iget-wide v1, p0, Lcom/baidu/mobads/f/d;->g:J

    .line 902
    iget-object v3, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    .line 903
    iget-object v4, p0, Lcom/baidu/mobads/f/d;->d:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/baidu/mobads/f/d$b;

    .line 904
    iget-wide v8, v7, Lcom/baidu/mobads/f/d$b;->c:J

    const/4 v10, 0x0

    add-long/2addr v1, v8

    .line 905
    invoke-virtual {v7}, Lcom/baidu/mobads/f/d$b;->d()Z

    move-result v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v7, :cond_0

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 910
    :cond_1
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 912
    iget-object v0, p0, Lcom/baidu/mobads/f/d;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 914
    invoke-static {v0, v5}, Lcom/baidu/mobads/f/d;->b(II)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v0, "Running"

    goto :goto_1

    :cond_2
    const/high16 v4, 0x60000000

    .line 915
    invoke-static {v0, v4}, Lcom/baidu/mobads/f/d;->c(II)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Terminated"

    goto :goto_1

    :cond_3
    const-string v0, "Shutting down"

    .line 917
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", pool size = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", active threads = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", queued tasks = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/baidu/mobads/f/d;->b:Ljava/util/concurrent/BlockingQueue;

    .line 921
    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", completed tasks = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v1

    .line 910
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method
