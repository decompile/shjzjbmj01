.class public abstract Lcom/baidu/mobads/f/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/f/a$a;,
        Lcom/baidu/mobads/f/a$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field private static f:Lcom/baidu/mobads/f/a$b;


# instance fields
.field private a:Ljava/lang/String;

.field protected b:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future<",
            "TT;>;"
        }
    .end annotation
.end field

.field private c:J

.field private d:J

.field private e:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "default"

    .line 34
    iput-object v0, p0, Lcom/baidu/mobads/f/a;->a:Ljava/lang/String;

    return-void
.end method

.method private static d()Landroid/os/Handler;
    .locals 3

    .line 118
    const-class v0, Lcom/baidu/mobads/f/a;

    monitor-enter v0

    .line 119
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/f/a;->f:Lcom/baidu/mobads/f/a$b;

    if-nez v1, :cond_0

    .line 120
    new-instance v1, Lcom/baidu/mobads/f/a$b;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/baidu/mobads/f/a$b;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/baidu/mobads/f/a;->f:Lcom/baidu/mobads/f/a$b;

    .line 122
    :cond_0
    sget-object v1, Lcom/baidu/mobads/f/a;->f:Lcom/baidu/mobads/f/a$b;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 123
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public a(J)V
    .locals 0

    .line 55
    iput-wide p1, p0, Lcom/baidu/mobads/f/a;->c:J

    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    return-void
.end method

.method protected a(Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/util/concurrent/Future;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/baidu/mobads/f/a;->b:Ljava/util/concurrent/Future;

    return-void
.end method

.method public b()Lcom/baidu/mobads/f/a;
    .locals 4

    .line 104
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/f/a;->d:J

    .line 105
    invoke-virtual {p0}, Lcom/baidu/mobads/f/a;->a()Ljava/lang/Object;

    move-result-object v0

    .line 106
    invoke-static {}, Lcom/baidu/mobads/f/a;->d()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Lcom/baidu/mobads/f/a$a;

    invoke-direct {v3, p0, v0}, Lcom/baidu/mobads/f/a$a;-><init>(Lcom/baidu/mobads/f/a;Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 109
    :try_start_1
    invoke-static {}, Lcom/baidu/mobads/f/a;->d()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    new-instance v3, Lcom/baidu/mobads/f/a$a;

    invoke-direct {v3, p0, v0}, Lcom/baidu/mobads/f/a$a;-><init>(Lcom/baidu/mobads/f/a;Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/baidu/mobads/f/a;->e:J

    return-object p0

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/baidu/mobads/f/a;->e:J

    throw v0
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method public run()V
    .locals 0

    .line 43
    invoke-virtual {p0}, Lcom/baidu/mobads/f/a;->b()Lcom/baidu/mobads/f/a;

    return-void
.end method
