.class final Lcom/baidu/mobads/f/d$b;
.super Ljava/util/concurrent/locks/AbstractQueuedSynchronizer;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/f/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final a:Ljava/lang/Thread;

.field b:Ljava/lang/Runnable;

.field volatile c:J

.field final synthetic d:Lcom/baidu/mobads/f/d;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/f/d;Ljava/lang/Runnable;)V
    .locals 1

    .line 122
    iput-object p1, p0, Lcom/baidu/mobads/f/d$b;->d:Lcom/baidu/mobads/f/d;

    invoke-direct {p0}, Ljava/util/concurrent/locks/AbstractQueuedSynchronizer;-><init>()V

    const/4 v0, -0x1

    .line 123
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/f/d$b;->setState(I)V

    .line 124
    iput-object p2, p0, Lcom/baidu/mobads/f/d$b;->b:Ljava/lang/Runnable;

    .line 125
    invoke-virtual {p1}, Lcom/baidu/mobads/f/d;->c()Ljava/util/concurrent/ThreadFactory;

    move-result-object p1

    invoke-interface {p1, p0}, Ljava/util/concurrent/ThreadFactory;->newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object p1

    iput-object p1, p0, Lcom/baidu/mobads/f/d$b;->a:Ljava/lang/Thread;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    .line 151
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/f/d$b;->acquire(I)V

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x1

    .line 155
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/f/d$b;->tryAcquire(I)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    .line 159
    invoke-virtual {p0, v0}, Lcom/baidu/mobads/f/d$b;->release(I)Z

    return-void
.end method

.method public d()Z
    .locals 1

    .line 163
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d$b;->isHeldExclusively()Z

    move-result v0

    return v0
.end method

.method e()V
    .locals 2

    .line 168
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d$b;->getState()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/f/d$b;->a:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 170
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method protected isHeldExclusively()Z
    .locals 1

    .line 133
    invoke-virtual {p0}, Lcom/baidu/mobads/f/d$b;->getState()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public run()V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/baidu/mobads/f/d$b;->d:Lcom/baidu/mobads/f/d;

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/f/d;->a(Lcom/baidu/mobads/f/d$b;)V

    return-void
.end method

.method protected tryAcquire(I)Z
    .locals 2

    const/4 p1, 0x1

    const/4 v0, 0x0

    .line 137
    invoke-virtual {p0, v0, p1}, Lcom/baidu/mobads/f/d$b;->compareAndSetState(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/baidu/mobads/f/d$b;->setExclusiveOwnerThread(Ljava/lang/Thread;)V

    return p1

    :cond_0
    return v0
.end method

.method protected tryRelease(I)Z
    .locals 0

    const/4 p1, 0x0

    .line 145
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/f/d$b;->setExclusiveOwnerThread(Ljava/lang/Thread;)V

    const/4 p1, 0x0

    .line 146
    invoke-virtual {p0, p1}, Lcom/baidu/mobads/f/d$b;->setState(I)V

    const/4 p1, 0x1

    return p1
.end method
