.class public Lcom/baidu/mobads/f/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile c:Lcom/baidu/mobads/f/c;


# instance fields
.field private a:Lcom/baidu/mobads/f/d;

.field private b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-direct {p0}, Lcom/baidu/mobads/f/c;->b()V

    return-void
.end method

.method public static a()Lcom/baidu/mobads/f/c;
    .locals 2

    .line 30
    sget-object v0, Lcom/baidu/mobads/f/c;->c:Lcom/baidu/mobads/f/c;

    if-nez v0, :cond_1

    .line 31
    const-class v0, Lcom/baidu/mobads/f/c;

    monitor-enter v0

    .line 32
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/f/c;->c:Lcom/baidu/mobads/f/c;

    if-nez v1, :cond_0

    .line 33
    new-instance v1, Lcom/baidu/mobads/f/c;

    invoke-direct {v1}, Lcom/baidu/mobads/f/c;-><init>()V

    sput-object v1, Lcom/baidu/mobads/f/c;->c:Lcom/baidu/mobads/f/c;

    .line 35
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 37
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobads/f/c;->c:Lcom/baidu/mobads/f/c;

    return-object v0
.end method

.method private b()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x5

    .line 41
    invoke-static {v0, v1}, Lcom/baidu/mobads/f/e;->a(II)Lcom/baidu/mobads/f/d;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/f/c;->a:Lcom/baidu/mobads/f/d;

    const/4 v0, 0x2

    .line 42
    invoke-static {v0}, Lcom/baidu/mobads/f/e;->a(I)Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/f/c;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    return-void
.end method


# virtual methods
.method public a(Lcom/baidu/mobads/f/a;)V
    .locals 2

    if-eqz p1, :cond_1

    .line 61
    iget-object v0, p0, Lcom/baidu/mobads/f/c;->a:Lcom/baidu/mobads/f/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/f/c;->a:Lcom/baidu/mobads/f/d;

    .line 63
    invoke-virtual {v0}, Lcom/baidu/mobads/f/d;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/f/a;->a(J)V

    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lcom/baidu/mobads/f/c;->a:Lcom/baidu/mobads/f/d;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/baidu/mobads/f/c;->a:Lcom/baidu/mobads/f/d;

    invoke-virtual {v1}, Lcom/baidu/mobads/f/d;->isShutdown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/baidu/mobads/f/c;->a:Lcom/baidu/mobads/f/d;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/f/d;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    .line 70
    :cond_0
    invoke-virtual {p1, v0}, Lcom/baidu/mobads/f/a;->a(Ljava/util/concurrent/Future;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    return-void
.end method

.method public a(Lcom/baidu/mobads/f/a;JLjava/util/concurrent/TimeUnit;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 80
    iget-object v0, p0, Lcom/baidu/mobads/f/c;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/f/c;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 82
    invoke-virtual {v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/baidu/mobads/f/a;->a(J)V

    .line 86
    iget-object v0, p0, Lcom/baidu/mobads/f/c;->b:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p2

    .line 87
    invoke-virtual {p1, p2}, Lcom/baidu/mobads/f/a;->a(Ljava/util/concurrent/Future;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 47
    iget-object v0, p0, Lcom/baidu/mobads/f/c;->a:Lcom/baidu/mobads/f/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/f/c;->a:Lcom/baidu/mobads/f/d;

    .line 49
    invoke-virtual {v0}, Lcom/baidu/mobads/f/d;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/f/c;->a:Lcom/baidu/mobads/f/d;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/f/d;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method
