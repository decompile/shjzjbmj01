.class public Lcom/baidu/mobads/f/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/baidu/mobads/f/d;

.field private static b:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/concurrent/ThreadFactory;

.field private static final d:Lcom/baidu/mobads/f/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/baidu/mobads/f/f;

    invoke-direct {v0}, Lcom/baidu/mobads/f/f;-><init>()V

    sput-object v0, Lcom/baidu/mobads/f/e;->c:Ljava/util/concurrent/ThreadFactory;

    .line 40
    new-instance v0, Lcom/baidu/mobads/f/h;

    invoke-direct {v0}, Lcom/baidu/mobads/f/h;-><init>()V

    sput-object v0, Lcom/baidu/mobads/f/e;->d:Lcom/baidu/mobads/f/b;

    return-void
.end method

.method static synthetic a()Lcom/baidu/mobads/f/d;
    .locals 1

    .line 17
    sget-object v0, Lcom/baidu/mobads/f/e;->a:Lcom/baidu/mobads/f/d;

    return-object v0
.end method

.method public static a(II)Lcom/baidu/mobads/f/d;
    .locals 9

    .line 60
    new-instance v8, Lcom/baidu/mobads/f/d;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sget-object v7, Lcom/baidu/mobads/f/e;->c:Ljava/util/concurrent/ThreadFactory;

    const-wide/16 v3, 0x3c

    move-object v0, v8

    move v1, p0

    move v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/baidu/mobads/f/d;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 62
    sget-object p0, Lcom/baidu/mobads/f/e;->d:Lcom/baidu/mobads/f/b;

    invoke-virtual {v8, p0}, Lcom/baidu/mobads/f/d;->a(Lcom/baidu/mobads/f/b;)V

    return-object v8
.end method

.method static synthetic a(Lcom/baidu/mobads/f/d;)Lcom/baidu/mobads/f/d;
    .locals 0

    .line 17
    sput-object p0, Lcom/baidu/mobads/f/e;->a:Lcom/baidu/mobads/f/d;

    return-object p0
.end method

.method static synthetic a(Ljava/util/concurrent/LinkedBlockingQueue;)Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 0

    .line 17
    sput-object p0, Lcom/baidu/mobads/f/e;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object p0
.end method

.method public static a(I)Ljava/util/concurrent/ScheduledThreadPoolExecutor;
    .locals 2

    .line 68
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    sget-object v1, Lcom/baidu/mobads/f/e;->c:Ljava/util/concurrent/ThreadFactory;

    invoke-direct {v0, p0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(ILjava/util/concurrent/ThreadFactory;)V

    return-object v0
.end method

.method static synthetic b()Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1

    .line 17
    sget-object v0, Lcom/baidu/mobads/f/e;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method

.method static synthetic c()Ljava/util/concurrent/ThreadFactory;
    .locals 1

    .line 17
    sget-object v0, Lcom/baidu/mobads/f/e;->c:Ljava/util/concurrent/ThreadFactory;

    return-object v0
.end method
