.class public final enum Lcom/baidu/mobads/AdSettings$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/AdSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/AdSettings$b;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final enum a:Lcom/baidu/mobads/AdSettings$b;

.field public static final enum b:Lcom/baidu/mobads/AdSettings$b;

.field public static final enum c:Lcom/baidu/mobads/AdSettings$b;

.field private static final synthetic e:[Lcom/baidu/mobads/AdSettings$b;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 36
    new-instance v0, Lcom/baidu/mobads/AdSettings$b;

    const-string v1, "UNKNOWN_PROTOCOL_TYPE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/baidu/mobads/AdSettings$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$b;->a:Lcom/baidu/mobads/AdSettings$b;

    new-instance v0, Lcom/baidu/mobads/AdSettings$b;

    const-string v1, "HTTP_PROTOCOL_TYPE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/baidu/mobads/AdSettings$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$b;->b:Lcom/baidu/mobads/AdSettings$b;

    new-instance v0, Lcom/baidu/mobads/AdSettings$b;

    const-string v1, "HTTPS_PROTOCOL_TYPE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/baidu/mobads/AdSettings$b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSettings$b;->c:Lcom/baidu/mobads/AdSettings$b;

    const/4 v0, 0x3

    .line 34
    new-array v0, v0, [Lcom/baidu/mobads/AdSettings$b;

    sget-object v1, Lcom/baidu/mobads/AdSettings$b;->a:Lcom/baidu/mobads/AdSettings$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/AdSettings$b;->b:Lcom/baidu/mobads/AdSettings$b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/AdSettings$b;->c:Lcom/baidu/mobads/AdSettings$b;

    aput-object v1, v0, v4

    sput-object v0, Lcom/baidu/mobads/AdSettings$b;->e:[Lcom/baidu/mobads/AdSettings$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/baidu/mobads/AdSettings$b;->d:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/baidu/mobads/AdSettings$b;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
