.class public abstract Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "ScreenVideoIOAdEventListener"
.end annotation


# instance fields
.field final synthetic a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;)V
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "AdLoaded"

    .line 195
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    :cond_0
    const-string v0, "AdStarted"

    .line 198
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    if-eqz p1, :cond_9

    .line 201
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;->onAdShow()V

    goto/16 :goto_0

    :cond_1
    const-string v0, "AdUserClick"

    .line 203
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 205
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    if-eqz p1, :cond_9

    .line 206
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;->onAdClick()V

    goto/16 :goto_0

    :cond_2
    const-string v0, "AdStopped"

    .line 208
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 p1, 0x0

    .line 210
    sput-boolean p1, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z

    const-string p1, "0"

    if-eqz p2, :cond_3

    const-string p1, "play_scale"

    .line 213
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 215
    :cond_3
    iget-object p2, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p2, p2, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    if-eqz p2, :cond_9

    .line 216
    iget-object p2, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p2, p2, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    invoke-interface {p2, p1}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;->onAdClose(F)V

    goto :goto_0

    :cond_4
    const-string v0, "AdRvdieoCacheSucc"

    .line 218
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 220
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    if-eqz p1, :cond_9

    .line 221
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;->onVideoDownloadSuccess()V

    goto :goto_0

    :cond_5
    const-string v0, "AdRvdieoCacheFailed"

    .line 223
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 225
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    if-eqz p1, :cond_9

    .line 226
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;->onVideoDownloadFailed()V

    goto :goto_0

    :cond_6
    const-string v0, "AdError"

    .line 228
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 230
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    if-eqz p1, :cond_9

    .line 231
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    .line 232
    invoke-interface {v0, p2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->getMessage(Ljava/util/Map;)Ljava/lang/String;

    move-result-object p2

    .line 231
    invoke-interface {p1, p2}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;->onAdFailed(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const-string p2, "PlayCompletion"

    .line 234
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_8

    .line 236
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    if-eqz p1, :cond_9

    .line 237
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;

    iget-object p1, p1, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    invoke-interface {p1}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;->playCompletion()V

    goto :goto_0

    :cond_8
    const-string p2, "AdRvdieoPlayError"

    .line 239
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :cond_9
    :goto_0
    return-void
.end method


# virtual methods
.method protected abstract handleCustomEvent(Ljava/lang/String;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method public run(Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 1

    .line 188
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v0

    .line 189
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object p1

    check-cast p1, Ljava/util/HashMap;

    .line 190
    invoke-direct {p0, v0, p1}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->a(Ljava/lang/String;Ljava/util/HashMap;)V

    .line 191
    invoke-virtual {p0, v0, p1}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;->handleCustomEvent(Ljava/lang/String;Ljava/util/HashMap;)V

    return-void
.end method
