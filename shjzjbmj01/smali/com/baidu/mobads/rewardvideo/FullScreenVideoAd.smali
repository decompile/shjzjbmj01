.class public Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;
.super Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenIOEventListener;,
        Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenVideoAdListener;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenVideoAdListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenVideoAdListener;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenVideoAdListener;Z)V
    .locals 1

    .line 72
    invoke-direct {p0, p1, p3}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;-><init>(Landroid/content/Context;Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;)V

    .line 73
    new-instance p1, Lcom/baidu/mobads/production/rewardvideo/a;

    iget-object p3, p0, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;->SLOT_TYPE_FULLSCREEN_VIDEO:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;

    invoke-direct {p1, p3, p2, p4, v0}, Lcom/baidu/mobads/production/rewardvideo/a;-><init>(Landroid/content/Context;Ljava/lang/String;ZLcom/baidu/mobads/interfaces/IXAdConstants4PDK$SlotType;)V

    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    return-void
.end method


# virtual methods
.method protected registerIOAdEventListener()Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;
    .locals 3

    .line 80
    new-instance v0, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenIOEventListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenIOEventListener;-><init>(Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$1;)V

    .line 81
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdVideoSkip"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    return-object v0
.end method
