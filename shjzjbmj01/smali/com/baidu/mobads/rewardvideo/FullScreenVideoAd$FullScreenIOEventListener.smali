.class Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenIOEventListener;
.super Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FullScreenIOEventListener"
.end annotation


# instance fields
.field final synthetic b:Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;


# direct methods
.method private constructor <init>(Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenIOEventListener;->b:Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;

    invoke-direct {p0, p1}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;-><init>(Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$1;)V
    .locals 0

    .line 86
    invoke-direct {p0, p1}, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenIOEventListener;-><init>(Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;)V

    return-void
.end method


# virtual methods
.method protected handleCustomEvent(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenIOEventListener;->b:Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;

    iget-object v0, v0, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    instance-of v0, v0, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenVideoAdListener;

    if-eqz v0, :cond_1

    const-string v0, "AdVideoSkip"

    .line 92
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "0"

    if-eqz p2, :cond_0

    const-string p1, "play_scale"

    .line 95
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 97
    :cond_0
    iget-object p2, p0, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenIOEventListener;->b:Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;

    iget-object p2, p2, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    check-cast p2, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenVideoAdListener;

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p1

    invoke-interface {p2, p1}, Lcom/baidu/mobads/rewardvideo/FullScreenVideoAd$FullScreenVideoAdListener;->onAdSkip(F)V

    :cond_1
    return-void
.end method
