.class public Lcom/baidu/mobads/rewardvideo/RewardVideoAd;
.super Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoIOAdEventListener;,
        Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;Z)V
    .locals 0

    .line 66
    invoke-direct {p0, p1, p3}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;-><init>(Landroid/app/Activity;Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;)V

    .line 67
    new-instance p1, Lcom/baidu/mobads/production/rewardvideo/a;

    iget-object p3, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->mContext:Landroid/content/Context;

    invoke-direct {p1, p3, p2, p4}, Lcom/baidu/mobads/production/rewardvideo/a;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 76
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;Z)V
    .locals 0

    .line 86
    invoke-direct {p0, p1, p3}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;-><init>(Landroid/content/Context;Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;)V

    .line 87
    new-instance p1, Lcom/baidu/mobads/production/rewardvideo/a;

    iget-object p3, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->mContext:Landroid/content/Context;

    invoke-direct {p1, p3, p2, p4}, Lcom/baidu/mobads/production/rewardvideo/a;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    return-void
.end method


# virtual methods
.method protected registerIOAdEventListener()Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;
    .locals 2

    .line 92
    new-instance v0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoIOAdEventListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoIOAdEventListener;-><init>(Lcom/baidu/mobads/rewardvideo/RewardVideoAd;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$1;)V

    return-object v0
.end method
