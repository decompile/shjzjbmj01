.class public abstract Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;,
        Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;
    }
.end annotation


# instance fields
.field private a:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

.field protected mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

.field protected mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

.field protected final mContext:Landroid/content/Context;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;)V
    .locals 1

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->CREATE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    iput-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->a:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    .line 63
    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mContext:Landroid/content/Context;

    .line 64
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 65
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/baidu/mobads/g/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/g/q;->a()V

    .line 66
    iput-object p2, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;)V
    .locals 1

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->CREATE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    iput-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->a:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    .line 74
    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mContext:Landroid/content/Context;

    .line 75
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->initializeApplicationContext(Landroid/content/Context;)V

    .line 76
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/baidu/mobads/g/q;->a(Landroid/content/Context;)Lcom/baidu/mobads/g/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/baidu/mobads/g/q;->a()V

    .line 77
    iput-object p2, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdListener:Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoAdListener;

    return-void
.end method

.method private a(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;)V
    .locals 1

    .line 169
    iput-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->a:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    .line 171
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    if-eqz v0, :cond_1

    .line 172
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->PAUSE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    if-ne p1, v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->pause()V

    .line 175
    :cond_0
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->RESUME:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    if-ne p1, v0, :cond_1

    .line 176
    iget-object p1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {p1}, Lcom/baidu/mobads/production/rewardvideo/a;->resume()V

    :cond_1
    return-void
.end method

.method public static setAppSid(Ljava/lang/String;)V
    .locals 1

    .line 148
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/utils/h;->setAppId(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public isReady()Z
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    .line 136
    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized load()V
    .locals 2

    monitor-enter p0

    .line 83
    :try_start_0
    sget-boolean v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 85
    monitor-exit p0

    return-void

    .line 87
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/rewardvideo/a;->c(Z)V

    .line 90
    :cond_1
    invoke-virtual {p0}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->makeRequest()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 82
    monitor-exit p0

    throw v0
.end method

.method protected makeRequest()V
    .locals 3

    .line 154
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->removeAllListeners()V

    .line 155
    invoke-virtual {p0}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->registerIOAdEventListener()Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdUserClick"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 157
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdLoaded"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 158
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdStarted"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 159
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdStopped"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 160
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdError"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 161
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdRvdieoCacheSucc"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 162
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdRvdieoCacheFailed"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 163
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "PlayCompletion"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 164
    iget-object v1, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    const-string v2, "AdRvdieoPlayError"

    invoke-virtual {v1, v2, v0}, Lcom/baidu/mobads/production/rewardvideo/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 165
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->request()V

    return-void
.end method

.method public pause()V
    .locals 1

    .line 122
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->PAUSE:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    invoke-direct {p0, v0}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->a(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;)V

    return-void
.end method

.method protected abstract registerIOAdEventListener()Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd$ScreenVideoIOAdEventListener;
.end method

.method public resume()V
    .locals 1

    .line 126
    sget-object v0, Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;->RESUME:Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;

    invoke-direct {p0, v0}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->a(Lcom/baidu/mobads/interfaces/IXAdConstants4PDK$ActivityState;)V

    return-void
.end method

.method public declared-synchronized show()V
    .locals 2

    monitor-enter p0

    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    if-eqz v0, :cond_2

    .line 104
    sget-boolean v0, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 106
    monitor-exit p0

    return-void

    .line 108
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->getCurrentXAdContainer()Lcom/baidu/mobads/interfaces/IXAdContainer;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    .line 109
    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    .line 110
    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    .line 111
    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    sput-boolean v1, Lcom/baidu/mobads/MobRewardVideoImpl;->mVideoPlaying:Z

    .line 113
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/rewardvideo/a;->u()V

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->makeRequest()V

    .line 116
    iget-object v0, p0, Lcom/baidu/mobads/rewardvideo/AbstractScreenVideoAd;->mAdProd:Lcom/baidu/mobads/production/rewardvideo/a;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/rewardvideo/a;->c(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 102
    monitor-exit p0

    throw v0
.end method
