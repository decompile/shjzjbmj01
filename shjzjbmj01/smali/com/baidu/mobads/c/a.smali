.class public Lcom/baidu/mobads/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/baidu/mobads/c/a$a;
    }
.end annotation


# static fields
.field private static final b:Landroid/os/Handler;

.field private static c:Lcom/baidu/mobads/c/a;


# instance fields
.field private a:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/baidu/mobads/c/a;->b:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    long-to-int v0, v0

    .line 51
    div-int/lit8 v0, v0, 0x20

    .line 52
    new-instance v1, Lcom/baidu/mobads/c/b;

    invoke-direct {v1, p0, v0}, Lcom/baidu/mobads/c/b;-><init>(Lcom/baidu/mobads/c/a;I)V

    iput-object v1, p0, Lcom/baidu/mobads/c/a;->a:Landroid/util/LruCache;

    return-void
.end method

.method private static a(Landroid/graphics/BitmapFactory$Options;Landroid/widget/ImageView;)I
    .locals 3

    .line 211
    invoke-static {p1}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;)Lcom/baidu/mobads/c/a$a;

    move-result-object p1

    .line 212
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 213
    iget p0, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 215
    iget v1, p1, Lcom/baidu/mobads/c/a$a;->a:I

    if-gt v0, v1, :cond_1

    iget v1, p1, Lcom/baidu/mobads/c/a$a;->b:I

    if-le p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float v0, v0, v1

    .line 217
    iget v2, p1, Lcom/baidu/mobads/c/a$a;->a:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float p0, p0

    mul-float p0, p0, v1

    .line 218
    iget p1, p1, Lcom/baidu/mobads/c/a$a;->b:I

    int-to-float p1, p1

    div-float/2addr p0, p1

    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    move-result p0

    .line 219
    invoke-static {v0, p0}, Ljava/lang/Math;->max(II)I

    move-result p0

    :goto_1
    return p0
.end method

.method static synthetic a(Lcom/baidu/mobads/c/a;Ljava/lang/String;Landroid/widget/ImageView;Z)Landroid/graphics/Bitmap;
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Landroid/widget/ImageView;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;Landroid/widget/ImageView;Z)Landroid/graphics/Bitmap;
    .locals 2

    .line 183
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 184
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 185
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 186
    invoke-static {p2}, Lcom/baidu/mobads/c/a;->a(Landroid/widget/ImageView;)Lcom/baidu/mobads/c/a$a;

    .line 187
    invoke-static {v0, p2}, Lcom/baidu/mobads/c/a;->a(Landroid/graphics/BitmapFactory$Options;Landroid/widget/ImageView;)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v1, 0x0

    .line 188
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 189
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p3, :cond_0

    .line 191
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-object p1
.end method

.method private static a(Landroid/widget/ImageView;)Lcom/baidu/mobads/c/a$a;
    .locals 5

    .line 228
    new-instance v0, Lcom/baidu/mobads/c/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/baidu/mobads/c/a$a;-><init>(Lcom/baidu/mobads/c/b;)V

    .line 230
    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 231
    invoke-virtual {p0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 233
    invoke-virtual {p0}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    if-gtz v3, :cond_0

    .line 235
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    :cond_0
    if-gtz v3, :cond_1

    .line 238
    invoke-virtual {p0}, Landroid/widget/ImageView;->getMaxWidth()I

    move-result v3

    :cond_1
    if-gtz v3, :cond_2

    .line 241
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 244
    :cond_2
    invoke-virtual {p0}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    if-gtz v4, :cond_3

    .line 246
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_3
    if-gtz v4, :cond_4

    .line 249
    invoke-virtual {p0}, Landroid/widget/ImageView;->getMaxHeight()I

    move-result v4

    :cond_4
    if-gtz v4, :cond_5

    .line 252
    iget v4, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 255
    :cond_5
    iput v3, v0, Lcom/baidu/mobads/c/a$a;->a:I

    .line 256
    iput v4, v0, Lcom/baidu/mobads/c/a$a;->b:I

    return-object v0
.end method

.method public static a()Lcom/baidu/mobads/c/a;
    .locals 2

    .line 39
    sget-object v0, Lcom/baidu/mobads/c/a;->c:Lcom/baidu/mobads/c/a;

    if-nez v0, :cond_1

    .line 40
    const-class v0, Lcom/baidu/mobads/c/a;

    monitor-enter v0

    .line 41
    :try_start_0
    sget-object v1, Lcom/baidu/mobads/c/a;->c:Lcom/baidu/mobads/c/a;

    if-nez v1, :cond_0

    .line 42
    new-instance v1, Lcom/baidu/mobads/c/a;

    invoke-direct {v1}, Lcom/baidu/mobads/c/a;-><init>()V

    sput-object v1, Lcom/baidu/mobads/c/a;->c:Lcom/baidu/mobads/c/a;

    .line 44
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 46
    :cond_1
    :goto_0
    sget-object v0, Lcom/baidu/mobads/c/a;->c:Lcom/baidu/mobads/c/a;

    return-object v0
.end method

.method static synthetic a(Lcom/baidu/mobads/c/a;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/baidu/mobads/c/a;->a:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 151
    iget-object v0, p0, Lcom/baidu/mobads/c/a;->a:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method static synthetic b()Landroid/os/Handler;
    .locals 1

    .line 27
    sget-object v0, Lcom/baidu/mobads/c/a;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 268
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object v0

    .line 269
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->getStoreagePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 270
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/baidu/mobads/utils/h;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 271
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ".temp"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private b(Ljava/lang/String;Ljava/util/Observer;)V
    .locals 7

    .line 162
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 164
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 165
    new-instance v0, Lcom/baidu/mobads/openad/download/b;

    invoke-static {p1}, Lcom/baidu/mobads/c/a;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    invoke-static {p1}, Lcom/baidu/mobads/c/a;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".temp"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/baidu/mobads/openad/download/b;-><init>(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 167
    invoke-virtual {v0, p2}, Lcom/baidu/mobads/openad/download/b;->addObserver(Ljava/util/Observer;)V

    .line 168
    invoke-virtual {v0}, Lcom/baidu/mobads/openad/download/b;->start()V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private c(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    .line 202
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x0

    .line 203
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 204
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 275
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getIoUtils()Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;

    move-result-object p0

    .line 276
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdIOUtils;->getStoreagePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 281
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/utils/h;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 2

    .line 68
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_4

    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_1

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/c/a;->a:Landroid/util/LruCache;

    invoke-virtual {v0, p2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 78
    :cond_1
    invoke-static {p2}, Lcom/baidu/mobads/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Landroid/widget/ImageView;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 80
    invoke-direct {p0, p2, v0}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 82
    :cond_2
    new-instance v0, Lcom/baidu/mobads/c/c;

    invoke-direct {v0, p0, p2, p1}, Lcom/baidu/mobads/c/c;-><init>(Lcom/baidu/mobads/c/a;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-direct {p0, p2, v0}, Lcom/baidu/mobads/c/a;->b(Ljava/lang/String;Ljava/util/Observer;)V

    :goto_0
    return-void

    :cond_3
    :goto_1
    return-void

    .line 69
    :cond_4
    new-instance p1, Ljava/lang/IllegalThreadStateException;

    const-string p2, "please invoke in main thread!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Ljava/lang/String;Ljava/util/Observer;)V
    .locals 0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 112
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/baidu/mobads/c/a;->b(Ljava/lang/String;Ljava/util/Observer;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .line 125
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/baidu/mobads/c/a;->a:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 130
    invoke-static {p1}, Lcom/baidu/mobads/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/baidu/mobads/c/a;->c(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 136
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/baidu/mobads/c/a;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_1
    const/4 p1, 0x0

    return p1
.end method
