.class Lcom/baidu/mobads/c/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/widget/ImageView;

.field final synthetic c:Lcom/baidu/mobads/c/a;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/c/a;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/baidu/mobads/c/c;->c:Lcom/baidu/mobads/c/a;

    iput-object p2, p0, Lcom/baidu/mobads/c/c;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/baidu/mobads/c/c;->b:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 2

    .line 85
    check-cast p1, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;

    .line 86
    invoke-interface {p1}, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader;->getState()Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    move-result-object p1

    sget-object p2, Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;->COMPLETED:Lcom/baidu/mobads/openad/interfaces/download/IOAdDownloader$DownloadStatus;

    if-ne p1, p2, :cond_0

    .line 87
    iget-object p1, p0, Lcom/baidu/mobads/c/c;->c:Lcom/baidu/mobads/c/a;

    iget-object p2, p0, Lcom/baidu/mobads/c/c;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/baidu/mobads/c/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/baidu/mobads/c/c;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/baidu/mobads/c/a;->a(Lcom/baidu/mobads/c/a;Ljava/lang/String;Landroid/widget/ImageView;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 89
    invoke-static {}, Lcom/baidu/mobads/c/a;->b()Landroid/os/Handler;

    move-result-object p2

    new-instance v0, Lcom/baidu/mobads/c/d;

    invoke-direct {v0, p0, p1}, Lcom/baidu/mobads/c/d;-><init>(Lcom/baidu/mobads/c/c;Landroid/graphics/Bitmap;)V

    invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 95
    iget-object p2, p0, Lcom/baidu/mobads/c/c;->c:Lcom/baidu/mobads/c/a;

    iget-object v0, p0, Lcom/baidu/mobads/c/c;->a:Ljava/lang/String;

    invoke-static {p2, v0, p1}, Lcom/baidu/mobads/c/a;->a(Lcom/baidu/mobads/c/a;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
