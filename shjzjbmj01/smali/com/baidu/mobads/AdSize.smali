.class public final enum Lcom/baidu/mobads/AdSize;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/baidu/mobads/AdSize;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum Banner:Lcom/baidu/mobads/AdSize;

.field public static final enum FeedH5TemplateNative:Lcom/baidu/mobads/AdSize;

.field public static final enum FeedNative:Lcom/baidu/mobads/AdSize;

.field public static final enum InterstitialForVideoBeforePlay:Lcom/baidu/mobads/AdSize;

.field public static final enum InterstitialForVideoPausePlay:Lcom/baidu/mobads/AdSize;

.field public static final enum InterstitialGame:Lcom/baidu/mobads/AdSize;

.field public static final enum InterstitialOther:Lcom/baidu/mobads/AdSize;

.field public static final enum InterstitialReader:Lcom/baidu/mobads/AdSize;

.field public static final enum InterstitialRefresh:Lcom/baidu/mobads/AdSize;

.field public static final enum PrerollVideoNative:Lcom/baidu/mobads/AdSize;

.field public static final enum RewardVideo:Lcom/baidu/mobads/AdSize;

.field public static final enum Square:Lcom/baidu/mobads/AdSize;

.field private static final synthetic b:[Lcom/baidu/mobads/AdSize;


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "Banner"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->Banner:Lcom/baidu/mobads/AdSize;

    .line 15
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "Square"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->Square:Lcom/baidu/mobads/AdSize;

    .line 31
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "InterstitialGame"

    const/4 v4, 0x2

    const/4 v5, 0x6

    invoke-direct {v0, v1, v4, v5}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->InterstitialGame:Lcom/baidu/mobads/AdSize;

    .line 35
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "InterstitialReader"

    const/4 v6, 0x3

    const/4 v7, 0x7

    invoke-direct {v0, v1, v6, v7}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->InterstitialReader:Lcom/baidu/mobads/AdSize;

    .line 43
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "InterstitialRefresh"

    const/4 v8, 0x4

    const/16 v9, 0x9

    invoke-direct {v0, v1, v8, v9}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->InterstitialRefresh:Lcom/baidu/mobads/AdSize;

    .line 47
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "InterstitialOther"

    const/4 v10, 0x5

    const/16 v11, 0xa

    invoke-direct {v0, v1, v10, v11}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->InterstitialOther:Lcom/baidu/mobads/AdSize;

    .line 51
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "InterstitialForVideoBeforePlay"

    const/16 v12, 0xc

    invoke-direct {v0, v1, v5, v12}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->InterstitialForVideoBeforePlay:Lcom/baidu/mobads/AdSize;

    .line 55
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "InterstitialForVideoPausePlay"

    const/16 v13, 0xd

    invoke-direct {v0, v1, v7, v13}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->InterstitialForVideoPausePlay:Lcom/baidu/mobads/AdSize;

    .line 59
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "RewardVideo"

    const/16 v13, 0x8

    const/16 v14, 0xe

    invoke-direct {v0, v1, v13, v14}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->RewardVideo:Lcom/baidu/mobads/AdSize;

    .line 66
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "PrerollVideoNative"

    const/16 v14, 0xf

    invoke-direct {v0, v1, v9, v14}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->PrerollVideoNative:Lcom/baidu/mobads/AdSize;

    .line 70
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "FeedNative"

    const/16 v14, 0x10

    invoke-direct {v0, v1, v11, v14}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->FeedNative:Lcom/baidu/mobads/AdSize;

    .line 74
    new-instance v0, Lcom/baidu/mobads/AdSize;

    const-string v1, "FeedH5TemplateNative"

    const/16 v14, 0xb

    const/16 v15, 0x11

    invoke-direct {v0, v1, v14, v15}, Lcom/baidu/mobads/AdSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/baidu/mobads/AdSize;->FeedH5TemplateNative:Lcom/baidu/mobads/AdSize;

    .line 7
    new-array v0, v12, [Lcom/baidu/mobads/AdSize;

    sget-object v1, Lcom/baidu/mobads/AdSize;->Banner:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v2

    sget-object v1, Lcom/baidu/mobads/AdSize;->Square:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v3

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialGame:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v4

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialReader:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v6

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialRefresh:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v8

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialOther:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v10

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialForVideoBeforePlay:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v5

    sget-object v1, Lcom/baidu/mobads/AdSize;->InterstitialForVideoPausePlay:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v7

    sget-object v1, Lcom/baidu/mobads/AdSize;->RewardVideo:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v13

    sget-object v1, Lcom/baidu/mobads/AdSize;->PrerollVideoNative:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v9

    sget-object v1, Lcom/baidu/mobads/AdSize;->FeedNative:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v11

    sget-object v1, Lcom/baidu/mobads/AdSize;->FeedH5TemplateNative:Lcom/baidu/mobads/AdSize;

    aput-object v1, v0, v14

    sput-object v0, Lcom/baidu/mobads/AdSize;->b:[Lcom/baidu/mobads/AdSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 84
    iput p3, p0, Lcom/baidu/mobads/AdSize;->a:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/baidu/mobads/AdSize;
    .locals 1

    .line 7
    const-class v0, Lcom/baidu/mobads/AdSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/baidu/mobads/AdSize;

    return-object p0
.end method

.method public static values()[Lcom/baidu/mobads/AdSize;
    .locals 1

    .line 7
    sget-object v0, Lcom/baidu/mobads/AdSize;->b:[Lcom/baidu/mobads/AdSize;

    invoke-virtual {v0}, [Lcom/baidu/mobads/AdSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/baidu/mobads/AdSize;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 80
    iget v0, p0, Lcom/baidu/mobads/AdSize;->a:I

    return v0
.end method
