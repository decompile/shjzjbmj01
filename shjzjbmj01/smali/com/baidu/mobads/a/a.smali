.class public Lcom/baidu/mobads/a/a;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

.field private b:Lcom/baidu/mobads/command/a;


# direct methods
.method public constructor <init>(Lcom/baidu/mobads/command/a;)V
    .locals 1

    .line 30
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 28
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/baidu/mobads/a/a;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    .line 31
    iput-object p1, p0, Lcom/baidu/mobads/a/a;->b:Lcom/baidu/mobads/command/a;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x3
    .end annotation

    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "package:"

    const-string v1, ""

    .line 39
    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 41
    iget-object p2, p0, Lcom/baidu/mobads/a/a;->b:Lcom/baidu/mobads/command/a;

    iget-object p2, p2, Lcom/baidu/mobads/command/a;->f:Ljava/lang/String;

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 43
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getPackageUtils()Lcom/baidu/mobads/utils/r;

    move-result-object v2

    .line 44
    iget-object p2, p0, Lcom/baidu/mobads/a/a;->b:Lcom/baidu/mobads/command/a;

    iget-boolean p2, p2, Lcom/baidu/mobads/command/a;->k:Z

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    iget-object p2, p0, Lcom/baidu/mobads/a/a;->b:Lcom/baidu/mobads/command/a;

    iget-object p2, p2, Lcom/baidu/mobads/command/a;->l:Ljava/lang/String;

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/baidu/mobads/a/a;->b:Lcom/baidu/mobads/command/a;

    iget-object p2, p2, Lcom/baidu/mobads/command/a;->l:Ljava/lang/String;

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 45
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdConstants()Lcom/baidu/mobads/utils/l;

    move-result-object p2

    .line 46
    iget-object v0, p0, Lcom/baidu/mobads/a/a;->b:Lcom/baidu/mobads/command/a;

    iget-object v4, v0, Lcom/baidu/mobads/command/a;->l:Ljava/lang/String;

    const/16 v6, 0x17d

    invoke-interface {p2}, Lcom/baidu/mobads/interfaces/utils/IXAdConstants;->getActTypeDownload()I

    move-result v7

    const/4 v8, 0x0

    move-object v3, p1

    invoke-interface/range {v2 .. v8}, Lcom/baidu/mobads/interfaces/utils/IXAdPackageUtils;->sendAPOInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;III)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 47
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p2

    .line 48
    iget-object v0, p0, Lcom/baidu/mobads/a/a;->b:Lcom/baidu/mobads/command/a;

    iget-object v0, v0, Lcom/baidu/mobads/command/a;->l:Ljava/lang/String;

    invoke-virtual {p2, p1, v0}, Lcom/baidu/mobads/utils/h;->browserOutside(Landroid/content/Context;Ljava/lang/String;)V

    .line 50
    :cond_0
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    .line 51
    :cond_1
    iget-object p2, p0, Lcom/baidu/mobads/a/a;->b:Lcom/baidu/mobads/command/a;

    iget-boolean p2, p2, Lcom/baidu/mobads/command/a;->g:Z

    if-eqz p2, :cond_2

    const-wide/16 v0, 0x258

    .line 53
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p2

    .line 57
    invoke-virtual {p2, v5}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    const/high16 v0, 0x10000000

    .line 58
    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 59
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 60
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 64
    iget-object p2, p0, Lcom/baidu/mobads/a/a;->a:Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    const-string v0, "InstallReceiver"

    invoke-interface {p2, v0, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_0
    return-void
.end method
