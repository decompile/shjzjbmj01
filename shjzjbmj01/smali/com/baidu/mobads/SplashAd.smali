.class public Lcom/baidu/mobads/SplashAd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/baidu/mobads/production/f/a;

.field private b:I

.field private volatile c:Ljava/lang/String;

.field private d:Landroid/content/Context;

.field private e:Lcom/baidu/mobads/component/XAdView;

.field private f:Ljava/lang/String;

.field private g:Lcom/baidu/mobad/feeds/RequestParameters;

.field private h:Landroid/view/ViewGroup;

.field private i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/baidu/mobads/SplashAdListener;

.field private k:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

.field public mTimeout:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 174
    invoke-direct/range {v0 .. v5}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;I)V
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v7, p5

    .line 188
    invoke-direct/range {v0 .. v7}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;Z)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    .line 206
    invoke-direct/range {v0 .. v6}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZI)V
    .locals 8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v7, p6

    .line 193
    invoke-direct/range {v0 .. v7}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;)V
    .locals 8

    const/16 v7, 0x1068

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    .line 221
    invoke-direct/range {v0 .. v7}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;I)V
    .locals 10

    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    .line 238
    invoke-direct/range {v0 .. v9}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;IZZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;IZZ)V
    .locals 13

    move-object v9, p0

    move-object v0, p1

    move-object v10, p2

    move-object/from16 v1, p3

    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x4

    .line 41
    iput v2, v9, Lcom/baidu/mobads/SplashAd;->b:I

    const-string v2, "init"

    .line 62
    iput-object v2, v9, Lcom/baidu/mobads/SplashAd;->c:Ljava/lang/String;

    .line 70
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, v9, Lcom/baidu/mobads/SplashAd;->i:Ljava/util/HashMap;

    .line 78
    new-instance v2, Lcom/baidu/mobads/s;

    invoke-direct {v2, p0}, Lcom/baidu/mobads/s;-><init>(Lcom/baidu/mobads/SplashAd;)V

    iput-object v2, v9, Lcom/baidu/mobads/SplashAd;->j:Lcom/baidu/mobads/SplashAdListener;

    .line 112
    new-instance v2, Lcom/baidu/mobads/t;

    invoke-direct {v2, p0}, Lcom/baidu/mobads/t;-><init>(Lcom/baidu/mobads/SplashAd;)V

    iput-object v2, v9, Lcom/baidu/mobads/SplashAd;->k:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    .line 260
    :try_start_0
    invoke-static {}, Lcom/baidu/mobads/j;->a()Lcom/baidu/mobads/j;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/baidu/mobads/j;->a(Landroid/content/Context;)V

    .line 261
    invoke-static {}, Lcom/baidu/mobads/j;->a()Lcom/baidu/mobads/j;

    move-result-object v2

    const/16 v3, 0x3e9

    invoke-virtual {v2, v3}, Lcom/baidu/mobads/j;->a(I)V

    .line 262
    iput-object v0, v9, Lcom/baidu/mobads/SplashAd;->d:Landroid/content/Context;

    move-object/from16 v5, p4

    .line 263
    iput-object v5, v9, Lcom/baidu/mobads/SplashAd;->f:Ljava/lang/String;

    move-object/from16 v8, p6

    .line 264
    iput-object v8, v9, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    move/from16 v7, p7

    .line 265
    iput v7, v9, Lcom/baidu/mobads/SplashAd;->mTimeout:I

    .line 266
    iput-object v10, v9, Lcom/baidu/mobads/SplashAd;->h:Landroid/view/ViewGroup;

    .line 267
    iget-object v2, v9, Lcom/baidu/mobads/SplashAd;->i:Ljava/util/HashMap;

    const-string v3, "Display_Down_Info"

    invoke-static/range {p9 .. p9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/baidu/mobads/constants/a;->l:J

    const-wide/16 v2, 0x0

    .line 270
    sput-wide v2, Lcom/baidu/mobads/constants/a;->m:J

    .line 271
    sput-wide v2, Lcom/baidu/mobads/constants/a;->n:J

    .line 272
    sput-wide v2, Lcom/baidu/mobads/constants/a;->o:J

    .line 273
    sput-wide v2, Lcom/baidu/mobads/constants/a;->p:J

    .line 274
    sput-wide v2, Lcom/baidu/mobads/constants/a;->q:J

    .line 275
    sput-wide v2, Lcom/baidu/mobads/constants/a;->r:J

    if-eqz v1, :cond_0

    .line 277
    iput-object v1, v9, Lcom/baidu/mobads/SplashAd;->j:Lcom/baidu/mobads/SplashAdListener;

    .line 280
    :cond_0
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 281
    iget-object v0, v9, Lcom/baidu/mobads/SplashAd;->j:Lcom/baidu/mobads/SplashAdListener;

    const-string v1, "\u8bf7\u60a8\u8f93\u5165\u6b63\u786e\u7684\u5e7f\u544a\u4f4dID"

    invoke-interface {v0, v1}, Lcom/baidu/mobads/SplashAdListener;->onAdFailed(Ljava/lang/String;)V

    return-void

    :cond_1
    if-nez p8, :cond_2

    return-void

    .line 290
    :cond_2
    invoke-static {}, Lcom/baidu/mobads/AppActivity;->isAnti()Z

    move-result v1

    if-nez v1, :cond_3

    .line 291
    invoke-direct {p0, p2, p1}, Lcom/baidu/mobads/SplashAd;->a(Landroid/view/ViewGroup;Landroid/content/Context;)V

    .line 294
    :cond_3
    new-instance v11, Lcom/baidu/mobads/component/XAdView;

    invoke-direct {v11, p1}, Lcom/baidu/mobads/component/XAdView;-><init>(Landroid/content/Context;)V

    .line 295
    new-instance v12, Lcom/baidu/mobads/v;

    move-object v1, v12

    move-object v2, p0

    move-object v3, p1

    move-object v4, v11

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/baidu/mobads/v;-><init>(Lcom/baidu/mobads/SplashAd;Landroid/content/Context;Lcom/baidu/mobads/component/XAdView;Ljava/lang/String;ZILcom/baidu/mobad/feeds/RequestParameters;)V

    invoke-virtual {v11, v12}, Lcom/baidu/mobads/component/XAdView;->setListener(Lcom/baidu/mobads/component/XAdView$Listener;)V

    .line 383
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 385
    invoke-virtual {v11, v0}, Lcom/baidu/mobads/component/XAdView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 386
    invoke-virtual {p2, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 389
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/SplashAdListener;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->j:Lcom/baidu/mobads/SplashAdListener;

    return-object p0
.end method

.method static synthetic a(Lcom/baidu/mobads/SplashAd;Lcom/baidu/mobads/production/f/a;)Lcom/baidu/mobads/production/f/a;
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    return-object p1
.end method

.method private a(Landroid/view/ViewGroup;Landroid/content/Context;)V
    .locals 2

    .line 611
    :try_start_0
    new-instance v0, Landroid/view/SurfaceView;

    invoke-direct {v0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 612
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {p2, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 613
    invoke-virtual {p1, v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 615
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->d(Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/baidu/mobads/SplashAd;Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/baidu/mobads/SplashAd;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .line 522
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->j:Lcom/baidu/mobads/SplashAdListener;

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->j:Lcom/baidu/mobads/SplashAdListener;

    invoke-interface {v0, p1}, Lcom/baidu/mobads/SplashAdListener;->onAdFailed(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/baidu/mobads/SplashAd;)Landroid/content/Context;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->d:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic c(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->k:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    return-object p0
.end method

.method static synthetic d(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobads/production/f/a;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    return-object p0
.end method

.method static synthetic e(Lcom/baidu/mobads/SplashAd;)Lcom/baidu/mobad/feeds/RequestParameters;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    return-object p0
.end method

.method static synthetic f(Lcom/baidu/mobads/SplashAd;)I
    .locals 0

    .line 37
    iget p0, p0, Lcom/baidu/mobads/SplashAd;->b:I

    return p0
.end method

.method static synthetic g(Lcom/baidu/mobads/SplashAd;)Ljava/util/HashMap;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/baidu/mobads/SplashAd;->i:Ljava/util/HashMap;

    return-object p0
.end method

.method public static setAppSec(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public static setAppSid(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 534
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/baidu/mobads/utils/h;->setAppId(Ljava/lang/String;)V

    return-void
.end method

.method public static setMaxVideoCacheCapacityMb(I)V
    .locals 3

    const/16 v0, 0xf

    if-lt p0, v0, :cond_0

    const/16 v0, 0x64

    if-gt p0, v0, :cond_0

    .line 578
    invoke-static {p0}, Lcom/baidu/mobads/utils/m;->a(I)V

    goto :goto_0

    :cond_0
    const/16 p0, 0x1e

    .line 580
    invoke-static {p0}, Lcom/baidu/mobads/utils/m;->a(I)V

    .line 581
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object p0

    invoke-virtual {p0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object p0

    const-string v0, ""

    const-string v1, "\u5f00\u5c4f\u8bbe\u7f6e\u89c6\u9891\u6700\u5927\u7f13\u5b58\u503c\u6709\u6548\u8303\u56f4\u572815~100M,\u9ed8\u8ba430M"

    const-string v2, ""

    .line 582
    invoke-interface {p0, v0, v1, v2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->printErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 565
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->p()V

    :cond_0
    const/4 v0, 0x0

    .line 568
    iput-object v0, p0, Lcom/baidu/mobads/SplashAd;->j:Lcom/baidu/mobads/SplashAdListener;

    return-void
.end method

.method public getExtData()Ljava/util/HashMap;
    .locals 2

    .line 587
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 588
    iget-object v1, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    invoke-virtual {v1}, Lcom/baidu/mobads/production/f/a;->r()Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    return-object v1

    :cond_0
    return-object v0
.end method

.method public final load()V
    .locals 12

    .line 397
    new-instance v0, Lcom/baidu/mobads/component/XAdView;

    iget-object v1, p0, Lcom/baidu/mobads/SplashAd;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/baidu/mobads/component/XAdView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/component/XAdView;

    .line 399
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 401
    iget-object v1, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/component/XAdView;

    invoke-virtual {v1, v0}, Lcom/baidu/mobads/component/XAdView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 403
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->removeAllListeners()V

    const/4 v0, 0x0

    .line 405
    iput-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    .line 408
    :cond_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v0

    iget-object v1, p0, Lcom/baidu/mobads/SplashAd;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/utils/h;->getScreenDensity(Landroid/content/Context;)F

    move-result v0

    .line 409
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getCommonUtils()Lcom/baidu/mobads/utils/h;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/SplashAd;->d:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/baidu/mobads/utils/h;->getScreenRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v1

    .line 411
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 412
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 413
    iget-object v3, p0, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    if-eqz v3, :cond_2

    .line 414
    iget-object v3, p0, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {v3}, Lcom/baidu/mobad/feeds/RequestParameters;->isCustomSize()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 415
    iget-object v3, p0, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {v3}, Lcom/baidu/mobad/feeds/RequestParameters;->getWidth()I

    move-result v3

    if-lez v3, :cond_1

    .line 416
    iget-object v2, p0, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {v2}, Lcom/baidu/mobad/feeds/RequestParameters;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v0

    float-to-int v2, v2

    .line 418
    :cond_1
    iget-object v3, p0, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {v3}, Lcom/baidu/mobad/feeds/RequestParameters;->getHeight()I

    move-result v3

    if-lez v3, :cond_2

    .line 419
    iget-object v1, p0, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {v1}, Lcom/baidu/mobad/feeds/RequestParameters;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, v0

    float-to-int v1, v1

    :cond_2
    move v9, v1

    move v8, v2

    int-to-float v1, v8

    const/high16 v2, 0x43480000    # 200.0f

    mul-float v2, v2, v0

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_5

    int-to-float v1, v9

    const/high16 v2, 0x43160000    # 150.0f

    mul-float v0, v0, v2

    cmpg-float v0, v1, v0

    if-gez v0, :cond_3

    goto :goto_0

    .line 433
    :cond_3
    new-instance v0, Lcom/baidu/mobads/production/f/a;

    iget-object v4, p0, Lcom/baidu/mobads/SplashAd;->d:Landroid/content/Context;

    iget-object v5, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/component/XAdView;

    iget-object v6, p0, Lcom/baidu/mobads/SplashAd;->f:Ljava/lang/String;

    const/4 v7, 0x1

    iget v10, p0, Lcom/baidu/mobads/SplashAd;->b:I

    iget v11, p0, Lcom/baidu/mobads/SplashAd;->mTimeout:I

    move-object v3, v0

    invoke-direct/range {v3 .. v11}, Lcom/baidu/mobads/production/f/a;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Ljava/lang/String;ZIIII)V

    iput-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    .line 436
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    iget-object v1, p0, Lcom/baidu/mobads/SplashAd;->i:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/f/a;->a(Ljava/util/HashMap;)V

    .line 437
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/baidu/mobads/production/f/a;->A:Z

    .line 440
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    if-eqz v0, :cond_4

    .line 441
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    iget-object v1, p0, Lcom/baidu/mobads/SplashAd;->g:Lcom/baidu/mobad/feeds/RequestParameters;

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/production/f/a;->a(Lcom/baidu/mobad/feeds/RequestParameters;)V

    .line 443
    :cond_4
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    const-string v1, "AdUserClick"

    iget-object v2, p0, Lcom/baidu/mobads/SplashAd;->k:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 444
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    const-string v1, "AdLoaded"

    iget-object v2, p0, Lcom/baidu/mobads/SplashAd;->k:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 445
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    const-string v1, "AdStarted"

    iget-object v2, p0, Lcom/baidu/mobads/SplashAd;->k:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 446
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    const-string v1, "AdStopped"

    iget-object v2, p0, Lcom/baidu/mobads/SplashAd;->k:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 447
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    const-string v1, "AdError"

    iget-object v2, p0, Lcom/baidu/mobads/SplashAd;->k:Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;

    invoke-virtual {v0, v1, v2}, Lcom/baidu/mobads/production/f/a;->addEventListener(Ljava/lang/String;Lcom/baidu/mobads/openad/interfaces/event/IOAdEventListener;)V

    .line 448
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->request()V

    return-void

    .line 426
    :cond_5
    :goto_0
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v0

    .line 427
    sget-object v1, Lcom/baidu/mobads/interfaces/error/XAdErrorCode;->SHOW_STANDARD_UNFIT:Lcom/baidu/mobads/interfaces/error/XAdErrorCode;

    const-string v2, "\u5f00\u5c4f\u663e\u793a\u533a\u57df\u592a\u5c0f,\u5bbd\u5ea6\u81f3\u5c11200dp,\u9ad8\u5ea6\u81f3\u5c11150dp"

    invoke-interface {v0, v1, v2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->genCompleteErrorMessage(Lcom/baidu/mobads/interfaces/error/XAdErrorCode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 429
    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getAdLogger()Lcom/baidu/mobads/interfaces/utils/IXAdLogger;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/baidu/mobads/interfaces/utils/IXAdLogger;->e(Ljava/lang/String;)I

    .line 430
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->j:Lcom/baidu/mobads/SplashAdListener;

    invoke-interface {v0}, Lcom/baidu/mobads/SplashAdListener;->onAdDismissed()V

    return-void
.end method

.method public final show()V
    .locals 2

    .line 456
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->h:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/component/XAdView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    if-eqz v0, :cond_1

    .line 458
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/component/XAdView;

    invoke-virtual {v0}, Lcom/baidu/mobads/component/XAdView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->removeAllListeners()V

    const-string v0, "\u5c55\u73b0\u5931\u8d25\uff0c\u8bf7\u91cd\u65b0load"

    .line 460
    invoke-direct {p0, v0}, Lcom/baidu/mobads/SplashAd;->a(Ljava/lang/String;)V

    return-void

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/component/XAdView;

    new-instance v1, Lcom/baidu/mobads/w;

    invoke-direct {v1, p0}, Lcom/baidu/mobads/w;-><init>(Lcom/baidu/mobads/SplashAd;)V

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/component/XAdView;->setListener(Lcom/baidu/mobads/component/XAdView$Listener;)V

    .line 511
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->h:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/baidu/mobads/SplashAd;->e:Lcom/baidu/mobads/component/XAdView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 514
    :cond_1
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    if-eqz v0, :cond_2

    .line 515
    iget-object v0, p0, Lcom/baidu/mobads/SplashAd;->a:Lcom/baidu/mobads/production/f/a;

    invoke-virtual {v0}, Lcom/baidu/mobads/production/f/a;->removeAllListeners()V

    :cond_2
    const-string v0, "\u5c55\u73b0\u5931\u8d25\uff0c\u8bf7\u68c0\u67e5splashAd\u53c2\u6570\u662f\u5426\u6b63\u786e"

    .line 517
    invoke-direct {p0, v0}, Lcom/baidu/mobads/SplashAd;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
