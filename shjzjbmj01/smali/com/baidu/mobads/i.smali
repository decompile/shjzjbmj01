.class Lcom/baidu/mobads/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

.field final synthetic b:Lcom/baidu/mobads/h;


# direct methods
.method constructor <init>(Lcom/baidu/mobads/h;Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iput-object p2, p0, Lcom/baidu/mobads/i;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-string v0, "AdStarted"

    .line 37
    iget-object v1, p0, Lcom/baidu/mobads/i;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0, v2}, Lcom/baidu/mobads/BaiduNativeH5AdView;->a(Lcom/baidu/mobads/BaiduNativeH5AdView;Z)Z

    .line 39
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->a(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeAdPlacement;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->setRequestStarted(Z)V

    .line 40
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-virtual {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->recordImpression()V

    .line 41
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 42
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;->onAdShow()V

    goto/16 :goto_0

    :cond_0
    const-string v0, "AdError"

    .line 44
    iget-object v3, p0, Lcom/baidu/mobads/i;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v3}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->a(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeAdPlacement;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->setRequestStarted(Z)V

    .line 46
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 47
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    move-result-object v0

    invoke-static {}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getInstance()Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/baidu/mobads/utils/XAdSDKFoundationFacade;->getErrorCode()Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;

    move-result-object v1

    iget-object v2, p0, Lcom/baidu/mobads/i;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    .line 48
    invoke-interface {v2}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getData()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/baidu/mobads/interfaces/error/IXAdErrorCode;->getMessage(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-interface {v0, v1}, Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;->onAdFail(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "AdUserClick"

    .line 50
    iget-object v1, p0, Lcom/baidu/mobads/i;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->a(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeAdPlacement;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->setClicked(Z)V

    .line 52
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 53
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;->onAdClick()V

    goto :goto_0

    :cond_2
    const-string v0, "AdImpression"

    .line 55
    iget-object v1, p0, Lcom/baidu/mobads/i;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 56
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->a(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeAdPlacement;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/BaiduNativeAdPlacement;->setWinSended(Z)V

    goto :goto_0

    :cond_3
    const-string v0, "AdLoadData"

    .line 57
    iget-object v1, p0, Lcom/baidu/mobads/i;->a:Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;

    invoke-interface {v1}, Lcom/baidu/mobads/openad/interfaces/event/IOAdEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 58
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0, v2}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b(Lcom/baidu/mobads/BaiduNativeH5AdView;Z)Z

    .line 59
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 60
    iget-object v0, p0, Lcom/baidu/mobads/i;->b:Lcom/baidu/mobads/h;

    iget-object v0, v0, Lcom/baidu/mobads/h;->a:Lcom/baidu/mobads/BaiduNativeH5AdView;

    invoke-static {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView;->b(Lcom/baidu/mobads/BaiduNativeH5AdView;)Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;

    move-result-object v0

    invoke-interface {v0}, Lcom/baidu/mobads/BaiduNativeH5AdView$BaiduNativeH5EventListner;->onAdDataLoaded()V

    :cond_4
    :goto_0
    return-void
.end method
