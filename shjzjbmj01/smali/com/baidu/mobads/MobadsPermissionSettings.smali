.class public Lcom/baidu/mobads/MobadsPermissionSettings;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z = false

.field private static b:Z = false

.field private static c:Z = false

.field private static d:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hasPermissionGranted(Ljava/lang/String;)Z
    .locals 1

    .line 57
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "permission_location"

    .line 58
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    sget-boolean p0, Lcom/baidu/mobads/MobadsPermissionSettings;->b:Z

    return p0

    :cond_0
    const-string v0, "permission_storage"

    .line 60
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    sget-boolean p0, Lcom/baidu/mobads/MobadsPermissionSettings;->c:Z

    return p0

    :cond_1
    const-string v0, "permission_app_list"

    .line 62
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    sget-boolean p0, Lcom/baidu/mobads/MobadsPermissionSettings;->d:Z

    return p0

    :cond_2
    const-string v0, "permission_read_phone_state"

    .line 64
    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 65
    sget-boolean p0, Lcom/baidu/mobads/MobadsPermissionSettings;->a:Z

    return p0

    :cond_3
    const/4 p0, 0x0

    return p0
.end method

.method public static setPermissionAppList(Z)V
    .locals 0

    .line 48
    sput-boolean p0, Lcom/baidu/mobads/MobadsPermissionSettings;->d:Z

    return-void
.end method

.method public static setPermissionLocation(Z)V
    .locals 0

    .line 32
    sput-boolean p0, Lcom/baidu/mobads/MobadsPermissionSettings;->b:Z

    return-void
.end method

.method public static setPermissionReadDeviceID(Z)V
    .locals 0

    .line 24
    sput-boolean p0, Lcom/baidu/mobads/MobadsPermissionSettings;->a:Z

    return-void
.end method

.method public static setPermissionStorage(Z)V
    .locals 0

    .line 40
    sput-boolean p0, Lcom/baidu/mobads/MobadsPermissionSettings;->c:Z

    return-void
.end method
