.class public Lcom/baidu/mobads/constants/XAdSDKProxyVersion;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final BAIDU_AD_SDK_VERSION:Ljava/lang/String; = ""

.field public static final DEBUG:Ljava/lang/Boolean;

.field public static final MAJOR_VERSION_NUMBER:I = 0x0

.field public static final MINOR_VERSION_NUMBER:I = 0x0

.field public static final MODIFIED:Ljava/lang/Boolean;

.field public static final REAL_RELEASE_TAG:Ljava/lang/String; = ""

.field public static final RELEASE_TAG:Ljava/lang/String; = "8.8427"

.field public static final SVN_REVISION_FINAL:Ljava/lang/String; = ""

.field public static final SVN_TREE:Ljava/lang/String; = ""

.field public static final TIME_STAMP:Ljava/lang/String; = ""

.field public static final URL:Ljava/lang/String; = ""


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    .line 84
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->DEBUG:Ljava/lang/Boolean;

    .line 89
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->MODIFIED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMajorVersionNumber()I
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "8.8427"

    const-string v2, "\\."

    .line 36
    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :catch_0
    return v0
.end method

.method public static getVersion()D
    .locals 2

    :try_start_0
    const-string v0, "8.8427"

    .line 21
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method
