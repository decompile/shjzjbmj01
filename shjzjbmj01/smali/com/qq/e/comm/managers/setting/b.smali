.class public final Lcom/qq/e/comm/managers/setting/b;
.super Lcom/qq/e/comm/managers/setting/d;


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Lcom/qq/e/comm/managers/setting/d;-><init>()V

    const-string v0, "getad_report_sampling_rate"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "expad_report_sampling_rate"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "clkad_report_sampling_rate"

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "require_window_focus"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "show_logo"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "innerBrowserScheme"

    const-string v3, "weixin,tel,openapp.jdmobile"

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "flow_control"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "gdtSdkIdentity"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "getSdkChannel"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "getSdkEX1"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "getSdkEX2"

    const-string v3, ""

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "rf"

    const/16 v3, 0x7530

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "spl_ltime"

    const/16 v3, 0xbb8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "spl_exptime"

    const/16 v3, 0x1388

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "spl_conn"

    const/16 v3, 0x1a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "spl_maxrn"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "force_exp"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/qq/e/comm/managers/setting/b;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/qq/e/comm/managers/setting/d;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
