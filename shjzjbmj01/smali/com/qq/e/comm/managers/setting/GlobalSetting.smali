.class public final Lcom/qq/e/comm/managers/setting/GlobalSetting;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Lcom/qq/e/comm/pi/CustomLandingPageListener; = null

.field private static volatile b:Ljava/lang/Integer; = null

.field private static volatile c:Z = false

.field private static volatile d:Z = true

.field private static volatile e:Ljava/lang/Boolean;

.field private static volatile f:Ljava/lang/String;

.field private static volatile g:Ljava/lang/String;

.field private static volatile h:Ljava/lang/String;

.field private static volatile i:Ljava/lang/String;

.field private static volatile j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getChannel()Ljava/lang/Integer;
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public static getCustomADActivityClassName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->f:Ljava/lang/String;

    return-object v0
.end method

.method public static getCustomLandingPageListener()Lcom/qq/e/comm/pi/CustomLandingPageListener;
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->a:Lcom/qq/e/comm/pi/CustomLandingPageListener;

    return-object v0
.end method

.method public static getCustomLandscapeActivityClassName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->i:Ljava/lang/String;

    return-object v0
.end method

.method public static getCustomPortraitActivityClassName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static getCustomRewardvideoLandscapeActivityClassName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->j:Ljava/lang/String;

    return-object v0
.end method

.method public static getCustomRewardvideoPortraitActivityClassName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->h:Ljava/lang/String;

    return-object v0
.end method

.method public static isAgreePrivacyStrategy()Ljava/lang/Boolean;
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->e:Ljava/lang/Boolean;

    return-object v0
.end method

.method public static isAgreePrivacyStrategyNonNull()Z
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public static isEnableMediationTool()Z
    .locals 1

    sget-boolean v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->c:Z

    return v0
.end method

.method public static isEnableVideoDownloadingCache()Z
    .locals 1

    sget-boolean v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->d:Z

    return v0
.end method

.method public static setAgreePrivacyStrategy(Z)V
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    sput-object p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->e:Ljava/lang/Boolean;

    :cond_0
    return-void
.end method

.method public static setChannel(I)V
    .locals 1

    sget-object v0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->b:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    sput-object p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->b:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public static setCustomADActivityClassName(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->f:Ljava/lang/String;

    return-void
.end method

.method public static setCustomLandingPageListener(Lcom/qq/e/comm/pi/CustomLandingPageListener;)V
    .locals 0

    sput-object p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->a:Lcom/qq/e/comm/pi/CustomLandingPageListener;

    return-void
.end method

.method public static setCustomLandscapeActivityClassName(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->i:Ljava/lang/String;

    return-void
.end method

.method public static setCustomPortraitActivityClassName(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->g:Ljava/lang/String;

    return-void
.end method

.method public static setCustomRewardvideoLandscapeActivityClassName(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->j:Ljava/lang/String;

    return-void
.end method

.method public static setCustomRewardvideoPortraitActivityClassName(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->h:Ljava/lang/String;

    return-void
.end method

.method public static setEnableMediationTool(Z)V
    .locals 0

    sput-boolean p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->c:Z

    return-void
.end method

.method public static setEnableVideoDownloadingCache(Z)V
    .locals 0

    sput-boolean p0, Lcom/qq/e/comm/managers/setting/GlobalSetting;->d:Z

    return-void
.end method
