.class public final Lcom/qq/e/ads/nativ/MediaView;
.super Landroid/widget/FrameLayout;


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected final onAttachedToWindow()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const-string v0, "onAttachedToWindow"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->d(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/qq/e/ads/nativ/MediaView;->isHardwareAccelerated()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Hardware acceleration is off"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 4

    invoke-static {}, Lcom/qq/e/comm/managers/status/SDKStatus;->getSDKVersionCode()I

    move-result v0

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/qq/e/comm/managers/status/SDKStatus;->getSDKVersionCode()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_5

    :cond_0
    iget v0, p0, Lcom/qq/e/ads/nativ/MediaView;->a:I

    if-lez v0, :cond_5

    iget v0, p0, Lcom/qq/e/ads/nativ/MediaView;->b:I

    if-lez v0, :cond_5

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    iget v0, p0, Lcom/qq/e/ads/nativ/MediaView;->b:I

    mul-int v0, v0, p1

    iget v1, p0, Lcom/qq/e/ads/nativ/MediaView;->a:I

    mul-int v1, v1, p2

    const/high16 v2, 0x40000000    # 2.0f

    if-lt v0, v1, :cond_4

    if-nez p2, :cond_1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/qq/e/ads/nativ/MediaView;->b:I

    mul-int v0, v0, p1

    iget v1, p0, Lcom/qq/e/ads/nativ/MediaView;->a:I

    mul-int v1, v1, p2

    if-gt v0, v1, :cond_2

    if-nez p1, :cond_3

    :cond_2
    iget p1, p0, Lcom/qq/e/ads/nativ/MediaView;->a:I

    mul-int p1, p1, p2

    iget v0, p0, Lcom/qq/e/ads/nativ/MediaView;->b:I

    div-int/2addr p1, v0

    :cond_3
    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-static {p2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_1

    :cond_4
    :goto_0
    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    iget v0, p0, Lcom/qq/e/ads/nativ/MediaView;->b:I

    mul-int p1, p1, v0

    iget v0, p0, Lcom/qq/e/ads/nativ/MediaView;->a:I

    div-int/2addr p1, v0

    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    move v3, p2

    move p2, p1

    move p1, v3

    :cond_5
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method

.method public final setRatio(II)V
    .locals 0

    iput p1, p0, Lcom/qq/e/ads/nativ/MediaView;->a:I

    iput p2, p0, Lcom/qq/e/ads/nativ/MediaView;->b:I

    return-void
.end method
