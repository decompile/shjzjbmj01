.class public Lcom/bytedance/tea/crash/e/e;
.super Ljava/lang/Object;
.source "LogcatDump.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/tea/crash/e/e$b;,
        Lcom/bytedance/tea/crash/e/e$a;
    }
.end annotation


# direct methods
.method private static a(I)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x6

    .line 146
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "*:V"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "*:D"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "*:I"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "*:W"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "*:E"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "*:F"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    if-ltz p0, :cond_0

    .line 147
    array-length v1, v0

    if-ge p0, v1, :cond_0

    .line 148
    aget-object p0, v0, p0

    return-object p0

    :cond_0
    const-string p0, "*:V"

    return-object p0
.end method

.method public static a(II)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 64
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    const/4 v1, 0x4

    .line 65
    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "logcat"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "-t"

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    const/4 v2, 0x2

    aput-object p0, v1, v2

    invoke-static {p1}, Lcom/bytedance/tea/crash/e/e;->a(I)Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x3

    aput-object p0, v1, p1

    const/4 p0, 0x0

    .line 67
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    :try_start_1
    new-instance p0, Lcom/bytedance/tea/crash/e/e$a;

    invoke-virtual {p1}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/bytedance/tea/crash/e/e$a;-><init>(Ljava/io/InputStream;Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/bytedance/tea/crash/e/e$a;->start()V

    .line 69
    new-instance p0, Lcom/bytedance/tea/crash/e/e$a;

    invoke-virtual {p1}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/bytedance/tea/crash/e/e$a;-><init>(Ljava/io/InputStream;Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/bytedance/tea/crash/e/e$a;->start()V

    .line 70
    new-instance p0, Lcom/bytedance/tea/crash/e/e$b;

    const-wide/16 v1, 0xbb8

    invoke-direct {p0, p1, v1, v2}, Lcom/bytedance/tea/crash/e/e$b;-><init>(Ljava/lang/Process;J)V

    invoke-virtual {p0}, Lcom/bytedance/tea/crash/e/e$b;->start()V

    .line 73
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-lt p0, v3, :cond_0

    .line 74
    sget-object p0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1, v2, p0}, Ljava/lang/Process;->waitFor(JLjava/util/concurrent/TimeUnit;)Z

    goto :goto_0

    .line 76
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Process;->waitFor()I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_2

    :catch_0
    move-exception p0

    goto :goto_1

    :catchall_0
    move-exception p1

    move-object v4, p1

    move-object p1, p0

    move-object p0, v4

    goto :goto_3

    :catch_1
    move-exception p1

    move-object v4, p1

    move-object p1, p0

    move-object p0, v4

    .line 79
    :goto_1
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz p1, :cond_1

    .line 82
    :goto_2
    invoke-virtual {p1}, Ljava/lang/Process;->destroy()V

    :cond_1
    return-object v0

    :catchall_1
    move-exception p0

    :goto_3
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Process;->destroy()V

    :cond_2
    throw p0
.end method
