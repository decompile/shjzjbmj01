.class public final Lcom/bytedance/common/utility/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/common/utility/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final hours_ago:I

.field public static final just_now:I

.field public static final minutes_ago:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    sget v0, Lcom/bytedance/applog/R$string;->hours_ago:I

    sput v0, Lcom/bytedance/common/utility/R$string;->hours_ago:I

    .line 10
    sget v0, Lcom/bytedance/applog/R$string;->just_now:I

    sput v0, Lcom/bytedance/common/utility/R$string;->just_now:I

    .line 11
    sget v0, Lcom/bytedance/applog/R$string;->minutes_ago:I

    sput v0, Lcom/bytedance/common/utility/R$string;->minutes_ago:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
