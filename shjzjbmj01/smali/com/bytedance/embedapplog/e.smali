.class public Lcom/bytedance/embedapplog/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/bytedance/embedapplog/c;

.field private h:Z

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:I

.field private o:I

.field private p:I

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Z

.field private y:Ljava/lang/String;

.field private z:Lcom/bytedance/embedapplog/d;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 29
    iput v0, p0, Lcom/bytedance/embedapplog/e;->i:I

    const/4 v0, 0x1

    .line 45
    iput-boolean v0, p0, Lcom/bytedance/embedapplog/e;->w:Z

    .line 46
    iput-boolean v0, p0, Lcom/bytedance/embedapplog/e;->x:Z

    .line 56
    iput-object p1, p0, Lcom/bytedance/embedapplog/e;->a:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/bytedance/embedapplog/e;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/bytedance/embedapplog/e;
    .locals 0

    .line 239
    invoke-static {p1}, Lcom/bytedance/embedapplog/at;->a(I)V

    return-object p0
.end method

.method public a(Z)Lcom/bytedance/embedapplog/e;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    .line 197
    :goto_0
    iput p1, p0, Lcom/bytedance/embedapplog/e;->i:I

    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->j:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/bytedance/embedapplog/d;)V
    .locals 0

    .line 482
    iput-object p1, p0, Lcom/bytedance/embedapplog/e;->z:Lcom/bytedance/embedapplog/d;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 474
    iput-object p1, p0, Lcom/bytedance/embedapplog/e;->y:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 0

    .line 458
    iput-boolean p1, p0, Lcom/bytedance/embedapplog/e;->w:Z

    return-void
.end method

.method public b()Z
    .locals 1

    .line 89
    iget-boolean v0, p0, Lcom/bytedance/embedapplog/e;->h:Z

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c(Z)V
    .locals 0

    .line 466
    iput-boolean p1, p0, Lcom/bytedance/embedapplog/e;->x:Z

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->f:Ljava/lang/String;

    return-object v0
.end method

.method public i()I
    .locals 1

    .line 208
    iget v0, p0, Lcom/bytedance/embedapplog/e;->i:I

    return v0
.end method

.method public j()Lcom/bytedance/embedapplog/c;
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->g:Lcom/bytedance/embedapplog/c;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->k:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->l:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->m:Ljava/lang/String;

    return-object v0
.end method

.method public n()I
    .locals 1

    .line 298
    iget v0, p0, Lcom/bytedance/embedapplog/e;->n:I

    return v0
.end method

.method public o()I
    .locals 1

    .line 328
    iget v0, p0, Lcom/bytedance/embedapplog/e;->o:I

    return v0
.end method

.method public p()I
    .locals 1

    .line 343
    iget v0, p0, Lcom/bytedance/embedapplog/e;->p:I

    return v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .line 374
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->q:Ljava/lang/String;

    return-object v0
.end method

.method public r()Ljava/lang/String;
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->r:Ljava/lang/String;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->s:Ljava/lang/String;

    return-object v0
.end method

.method public t()Ljava/lang/String;
    .locals 1

    .line 423
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->t:Ljava/lang/String;

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    .line 437
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->u:Ljava/lang/String;

    return-object v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .line 450
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->v:Ljava/lang/String;

    return-object v0
.end method

.method public w()Z
    .locals 1

    .line 462
    iget-boolean v0, p0, Lcom/bytedance/embedapplog/e;->x:Z

    return v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    .line 470
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->y:Ljava/lang/String;

    return-object v0
.end method

.method public y()Lcom/bytedance/embedapplog/d;
    .locals 1

    .line 478
    iget-object v0, p0, Lcom/bytedance/embedapplog/e;->z:Lcom/bytedance/embedapplog/d;

    return-object v0
.end method
