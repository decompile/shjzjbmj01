.class Lcom/bytedance/embedapplog/an;
.super Lcom/bytedance/embedapplog/z;
.source "SourceFile"


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Lcom/bytedance/embedapplog/ae;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/ae;)V
    .locals 1

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, v0, v0}, Lcom/bytedance/embedapplog/z;-><init>(ZZ)V

    .line 25
    iput-object p1, p0, Lcom/bytedance/embedapplog/an;->e:Landroid/content/Context;

    .line 26
    iput-object p2, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    return-void
.end method


# virtual methods
.method protected a(Lorg/json/JSONObject;)Z
    .locals 5

    .line 31
    iget-object v0, p0, Lcom/bytedance/embedapplog/an;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->P()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, "package"

    .line 33
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 35
    :cond_0
    sget-boolean v1, Lcom/bytedance/embedapplog/bm;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "has zijie pkg"

    .line 36
    invoke-static {v1, v2}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    const-string v1, "package"

    .line 38
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/ae;->P()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p1, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "real_package_name"

    .line 39
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_0
    const/4 v1, 0x0

    .line 45
    :try_start_0
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-object v0, v2

    :goto_1
    if-eqz v0, :cond_2

    .line 48
    :try_start_1
    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    goto :goto_2

    :catch_1
    move-exception p1

    goto/16 :goto_9

    :cond_2
    const/4 v2, 0x0

    .line 50
    :goto_2
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/ae;->G()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "app_version"

    .line 51
    iget-object v4, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v4}, Lcom/bytedance/embedapplog/ae;->G()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_4

    :cond_3
    const-string v3, "app_version"

    if-eqz v0, :cond_4

    .line 53
    iget-object v4, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_3

    :cond_4
    const-string v4, ""

    :goto_3
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    :goto_4
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/ae;->L()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "app_version_minor"

    .line 57
    iget-object v4, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v4}, Lcom/bytedance/embedapplog/ae;->L()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_5

    :cond_5
    const-string v3, "app_version_minor"

    const-string v4, ""

    .line 59
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 62
    :goto_5
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/ae;->D()I

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "version_code"

    .line 63
    iget-object v4, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v4}, Lcom/bytedance/embedapplog/ae;->D()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_6

    :cond_6
    const-string v3, "version_code"

    .line 65
    invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 68
    :goto_6
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/ae;->E()I

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "update_version_code"

    .line 69
    iget-object v4, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v4}, Lcom/bytedance/embedapplog/ae;->E()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_7

    :cond_7
    const-string v3, "update_version_code"

    .line 71
    invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 74
    :goto_7
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/ae;->F()I

    move-result v3

    if-eqz v3, :cond_8

    const-string v2, "manifest_version_code"

    .line 75
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/ae;->F()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_8

    :cond_8
    const-string v3, "manifest_version_code"

    .line 77
    invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 80
    :goto_8
    iget-object v2, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/ae;->C()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "app_name"

    .line 81
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/ae;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 83
    :cond_9
    iget-object v2, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/ae;->H()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "tweaked_channel"

    .line 84
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/ae;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_a
    if-eqz v0, :cond_b

    .line 87
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_b

    .line 88
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->labelRes:I

    if-lez v0, :cond_b

    const-string v2, "display_name"

    .line 90
    iget-object v3, p0, Lcom/bytedance/embedapplog/an;->e:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :cond_b
    const/4 p1, 0x1

    return p1

    .line 95
    :goto_9
    invoke-static {p1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    return v1
.end method
