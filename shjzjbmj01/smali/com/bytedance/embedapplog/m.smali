.class Lcom/bytedance/embedapplog/m;
.super Lcom/bytedance/embedapplog/n;
.source "SourceFile"


# instance fields
.field private b:Z

.field private final c:Lcom/bytedance/embedapplog/af;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/af;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/n;-><init>(Landroid/content/Context;)V

    .line 25
    iput-object p2, p0, Lcom/bytedance/embedapplog/m;->c:Lcom/bytedance/embedapplog/af;

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method b()J
    .locals 2

    .line 35
    iget-boolean v0, p0, Lcom/bytedance/embedapplog/m;->b:Z

    if-eqz v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method c()[J
    .locals 1

    .line 40
    sget-object v0, Lcom/bytedance/embedapplog/u;->b:[J

    return-object v0
.end method

.method d()Z
    .locals 6

    .line 45
    iget-object v0, p0, Lcom/bytedance/embedapplog/m;->c:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/af;->o()I

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/bytedance/embedapplog/m;->c:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/af;->a()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v1, p0, Lcom/bytedance/embedapplog/m;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/embedapplog/m;->c:Lcom/bytedance/embedapplog/af;

    .line 49
    invoke-virtual {v2}, Lcom/bytedance/embedapplog/af;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {}, Lcom/bytedance/embedapplog/at;->a()Lcom/bytedance/embedapplog/util/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/util/a;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getIAppParam()Lcom/bytedance/embedapplog/a;

    move-result-object v5

    .line 48
    invoke-static {v1, v2, v3, v4, v5}, Lcom/bytedance/embedapplog/au;->a(Landroid/content/Context;Lorg/json/JSONObject;Ljava/lang/String;ZLcom/bytedance/embedapplog/a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/bytedance/embedapplog/at;->b(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/embedapplog/m;->b:Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 51
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 54
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/bytedance/embedapplog/m;->b:Z

    return v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    const-string v0, "ac"

    return-object v0
.end method
