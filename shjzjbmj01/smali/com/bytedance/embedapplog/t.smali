.class Lcom/bytedance/embedapplog/t;
.super Lcom/bytedance/embedapplog/n;
.source "SourceFile"


# static fields
.field private static final b:[J


# instance fields
.field private final c:Lcom/bytedance/embedapplog/aw;

.field private final d:Lcom/bytedance/embedapplog/af;

.field private e:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    .line 22
    new-array v0, v0, [J

    const/4 v1, 0x0

    const-wide/32 v2, 0xea60

    aput-wide v2, v0, v1

    sput-object v0, Lcom/bytedance/embedapplog/t;->b:[J

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/aw;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/n;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p3, p0, Lcom/bytedance/embedapplog/t;->c:Lcom/bytedance/embedapplog/aw;

    .line 33
    iput-object p2, p0, Lcom/bytedance/embedapplog/t;->d:Lcom/bytedance/embedapplog/af;

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method b()J
    .locals 4

    .line 43
    iget-wide v0, p0, Lcom/bytedance/embedapplog/t;->e:J

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    return-wide v0
.end method

.method c()[J
    .locals 1

    .line 48
    sget-object v0, Lcom/bytedance/embedapplog/t;->b:[J

    return-object v0
.end method

.method public d()Z
    .locals 5

    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 54
    invoke-static {}, Lcom/bytedance/embedapplog/p;->d()Lcom/bytedance/embedapplog/w;

    move-result-object v2

    if-eqz v2, :cond_0

    const-wide/32 v3, 0xc350

    .line 56
    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/bytedance/embedapplog/w;->a(JJ)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "play_session"

    .line 58
    invoke-static {v1, v0}, Lcom/bytedance/embedapplog/AppLog;->onEventV3(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 59
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->flush()V

    :cond_0
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lcom/bytedance/embedapplog/t;->d:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/af;->o()I

    move-result v1

    if-eqz v1, :cond_2

    .line 65
    iget-object v1, p0, Lcom/bytedance/embedapplog/t;->d:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/af;->b()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 67
    iget-object v0, p0, Lcom/bytedance/embedapplog/t;->c:Lcom/bytedance/embedapplog/aw;

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/aw;->a(Lorg/json/JSONObject;)Z

    move-result v0

    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/bytedance/embedapplog/t;->e:J

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 70
    invoke-static {v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    const-string v0, "p"

    return-object v0
.end method
