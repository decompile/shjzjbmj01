.class public Lcom/bytedance/embedapplog/ax;
.super Lcom/bytedance/embedapplog/av;
.source "SourceFile"


# instance fields
.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:J

.field private m:J


# direct methods
.method constructor <init>()V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/bytedance/embedapplog/av;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/bytedance/embedapplog/av;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/bytedance/embedapplog/ax;->h:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/bytedance/embedapplog/ax;->i:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/bytedance/embedapplog/ax;->j:Ljava/lang/String;

    .line 51
    iput-wide p4, p0, Lcom/bytedance/embedapplog/ax;->l:J

    .line 52
    iput-wide p6, p0, Lcom/bytedance/embedapplog/ax;->m:J

    .line 53
    iput-object p8, p0, Lcom/bytedance/embedapplog/ax;->k:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/bytedance/embedapplog/av;
    .locals 2
    .param p1    # Landroid/database/Cursor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    .line 81
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/ax;->a:J

    const/4 v0, 0x1

    .line 82
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/ax;->b:J

    const/4 v0, 0x2

    .line 83
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->c:Ljava/lang/String;

    const/4 v0, 0x3

    .line 84
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->d:Ljava/lang/String;

    const/4 v0, 0x4

    .line 85
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->h:Ljava/lang/String;

    const/4 v0, 0x5

    .line 86
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->i:Ljava/lang/String;

    const/4 v0, 0x6

    .line 87
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/ax;->l:J

    const/4 v0, 0x7

    .line 88
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/ax;->m:J

    const/16 v0, 0x8

    .line 89
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->k:Ljava/lang/String;

    const/16 v0, 0x9

    .line 90
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->j:Ljava/lang/String;

    const/16 v0, 0xa

    .line 91
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->e:Ljava/lang/String;

    const/16 v0, 0xb

    .line 92
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/ax;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 3
    .param p1    # Landroid/content/ContentValues;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    .line 98
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ax;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "tea_event_index"

    .line 99
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ax;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "session_id"

    .line 100
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "user_unique_id"

    .line 101
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "category"

    .line 102
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tag"

    .line 103
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "value"

    .line 104
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ax;->l:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "ext_value"

    .line 105
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ax;->m:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "params"

    .line 106
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "label"

    .line 107
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ab_version"

    .line 108
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ab_sdk_version"

    .line 109
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected a(Lorg/json/JSONObject;)V
    .locals 3
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    .line 114
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ax;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "tea_event_index"

    .line 115
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ax;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "session_id"

    .line 116
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "user_unique_id"

    .line 117
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "category"

    .line 118
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "tag"

    .line 119
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "value"

    .line 120
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ax;->l:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "ext_value"

    .line 121
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ax;->m:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "params"

    .line 122
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "label"

    .line 123
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ab_version"

    .line 124
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ab_sdk_version"

    .line 125
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method protected a()[Ljava/lang/String;
    .locals 3

    const/16 v0, 0x18

    .line 62
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "local_time_ms"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "tea_event_index"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "session_id"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "user_unique_id"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "category"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string v1, "tag"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string v1, "value"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string v1, "ext_value"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string v1, "params"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string v1, "label"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0x13

    aput-object v1, v0, v2

    const-string v1, "ab_version"

    const/16 v2, 0x14

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0x15

    aput-object v1, v0, v2

    const-string v1, "ab_sdk_version"

    const/16 v2, 0x16

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0x17

    aput-object v1, v0, v2

    return-object v0
.end method

.method protected b(Lorg/json/JSONObject;)Lcom/bytedance/embedapplog/av;
    .locals 6
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    const-wide/16 v1, 0x0

    .line 161
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/bytedance/embedapplog/ax;->a:J

    const-string v0, "tea_event_index"

    .line 162
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/bytedance/embedapplog/ax;->b:J

    const-string v0, "session_id"

    const/4 v3, 0x0

    .line 163
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->c:Ljava/lang/String;

    const-string v0, "user_unique_id"

    .line 164
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->d:Ljava/lang/String;

    const-string v0, "category"

    .line 165
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->h:Ljava/lang/String;

    const-string v0, "tag"

    .line 166
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->i:Ljava/lang/String;

    const-string v0, "value"

    .line 167
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/bytedance/embedapplog/ax;->l:J

    const-string v0, "ext_value"

    .line 168
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/ax;->m:J

    const-string v0, "params"

    .line 169
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->k:Ljava/lang/String;

    const-string v0, "label"

    .line 170
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->j:Ljava/lang/String;

    const-string v0, "ab_version"

    .line 171
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ax;->e:Ljava/lang/String;

    const-string v0, "ab_sdk_version"

    .line 172
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/ax;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected b()Lorg/json/JSONObject;
    .locals 4

    .line 131
    iget-object v0, p0, Lcom/bytedance/embedapplog/ax;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->k:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 135
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :cond_1
    const-string v1, "local_time_ms"

    .line 137
    iget-wide v2, p0, Lcom/bytedance/embedapplog/ax;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "tea_event_index"

    .line 138
    iget-wide v2, p0, Lcom/bytedance/embedapplog/ax;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "session_id"

    .line 139
    iget-object v2, p0, Lcom/bytedance/embedapplog/ax;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 140
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "user_unique_id"

    .line 141
    iget-object v2, p0, Lcom/bytedance/embedapplog/ax;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    const-string v1, "category"

    .line 143
    iget-object v2, p0, Lcom/bytedance/embedapplog/ax;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "tag"

    .line 144
    iget-object v2, p0, Lcom/bytedance/embedapplog/ax;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "value"

    .line 145
    iget-wide v2, p0, Lcom/bytedance/embedapplog/ax;->l:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "ext_value"

    .line 146
    iget-wide v2, p0, Lcom/bytedance/embedapplog/ax;->m:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "label"

    .line 147
    iget-object v2, p0, Lcom/bytedance/embedapplog/ax;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "datetime"

    .line 148
    iget-object v2, p0, Lcom/bytedance/embedapplog/ax;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 149
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "ab_version"

    .line 150
    iget-object v2, p0, Lcom/bytedance/embedapplog/ax;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 153
    :cond_3
    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "ab_sdk_version"

    .line 154
    iget-object v2, p0, Lcom/bytedance/embedapplog/ax;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_4
    return-object v0
.end method

.method d()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-string v0, "event"

    return-object v0
.end method

.method protected h()Ljava/lang/String;
    .locals 2

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/embedapplog/ax;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/bytedance/embedapplog/ax;->i:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/bytedance/embedapplog/ax;->j:Ljava/lang/String;

    return-object v0
.end method
