.class public Lcom/bytedance/embedapplog/az;
.super Lcom/bytedance/embedapplog/av;
.source "SourceFile"


# instance fields
.field protected h:Ljava/lang/String;

.field private i:Z

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/bytedance/embedapplog/av;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/bytedance/embedapplog/az;->j:Ljava/lang/String;

    .line 33
    iput-boolean p2, p0, Lcom/bytedance/embedapplog/az;->i:Z

    .line 34
    iput-object p3, p0, Lcom/bytedance/embedapplog/az;->h:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/bytedance/embedapplog/av;
    .locals 4
    .param p1    # Landroid/database/Cursor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    .line 55
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/bytedance/embedapplog/az;->a:J

    const/4 v1, 0x1

    .line 56
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/bytedance/embedapplog/az;->b:J

    const/4 v2, 0x2

    .line 57
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/embedapplog/az;->c:Ljava/lang/String;

    const/4 v2, 0x3

    .line 58
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/embedapplog/az;->d:Ljava/lang/String;

    const/4 v2, 0x4

    .line 59
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/embedapplog/az;->j:Ljava/lang/String;

    const/4 v2, 0x5

    .line 60
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/embedapplog/az;->h:Ljava/lang/String;

    const/4 v2, 0x6

    .line 61
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/bytedance/embedapplog/az;->i:Z

    const/4 v0, 0x7

    .line 62
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/az;->e:Ljava/lang/String;

    const/16 v0, 0x8

    .line 63
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/az;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 3
    .param p1    # Landroid/content/ContentValues;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    .line 69
    iget-wide v1, p0, Lcom/bytedance/embedapplog/az;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "tea_event_index"

    .line 70
    iget-wide v1, p0, Lcom/bytedance/embedapplog/az;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "session_id"

    .line 71
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "user_unique_id"

    .line 72
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event"

    .line 73
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-boolean v0, p0, Lcom/bytedance/embedapplog/az;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/embedapplog/az;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 76
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/az;->i()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 78
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    const-string v0, "params"

    .line 81
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "is_bav"

    .line 82
    iget-boolean v1, p0, Lcom/bytedance/embedapplog/az;->i:Z

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "ab_version"

    .line 83
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ab_sdk_version"

    .line 84
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected a(Lorg/json/JSONObject;)V
    .locals 3
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    .line 89
    iget-wide v1, p0, Lcom/bytedance/embedapplog/az;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "tea_event_index"

    .line 90
    iget-wide v1, p0, Lcom/bytedance/embedapplog/az;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "session_id"

    .line 91
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "user_unique_id"

    .line 92
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "event"

    .line 93
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 94
    iget-boolean v0, p0, Lcom/bytedance/embedapplog/az;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/embedapplog/az;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/az;->i()V

    :cond_0
    const-string v0, "params"

    .line 97
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "is_bav"

    .line 98
    iget-boolean v1, p0, Lcom/bytedance/embedapplog/az;->i:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "ab_version"

    .line 99
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ab_sdk_version"

    .line 100
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method protected a()[Ljava/lang/String;
    .locals 3

    const/16 v0, 0x12

    .line 39
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "local_time_ms"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "tea_event_index"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "session_id"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "user_unique_id"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "event"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string v1, "params"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string v1, "is_bav"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string v1, "ab_version"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string v1, "ab_sdk_version"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    return-object v0
.end method

.method protected b(Lorg/json/JSONObject;)Lcom/bytedance/embedapplog/av;
    .locals 5
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    const-wide/16 v1, 0x0

    .line 136
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/bytedance/embedapplog/az;->a:J

    const-string v0, "tea_event_index"

    .line 137
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/az;->b:J

    const-string v0, "session_id"

    const/4 v1, 0x0

    .line 138
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/az;->c:Ljava/lang/String;

    const-string v0, "user_unique_id"

    .line 139
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/az;->d:Ljava/lang/String;

    const-string v0, "event"

    .line 140
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/az;->j:Ljava/lang/String;

    const-string v0, "params"

    .line 141
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/az;->h:Ljava/lang/String;

    const-string v0, "is_bav"

    const/4 v2, 0x0

    .line 142
    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/embedapplog/az;->i:Z

    const-string v0, "ab_version"

    .line 143
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/az;->e:Ljava/lang/String;

    const-string v0, "ab_sdk_version"

    .line 144
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/az;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected b()Lorg/json/JSONObject;
    .locals 4

    .line 105
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "local_time_ms"

    .line 106
    iget-wide v2, p0, Lcom/bytedance/embedapplog/az;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "tea_event_index"

    .line 107
    iget-wide v2, p0, Lcom/bytedance/embedapplog/az;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "session_id"

    .line 108
    iget-object v2, p0, Lcom/bytedance/embedapplog/az;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 109
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "user_unique_id"

    .line 110
    iget-object v2, p0, Lcom/bytedance/embedapplog/az;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    const-string v1, "event"

    .line 112
    iget-object v2, p0, Lcom/bytedance/embedapplog/az;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 114
    iget-boolean v1, p0, Lcom/bytedance/embedapplog/az;->i:Z

    if-eqz v1, :cond_1

    const-string v1, "is_bav"

    const/4 v2, 0x1

    .line 115
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 118
    :cond_1
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "params"

    .line 119
    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/bytedance/embedapplog/az;->h:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    const-string v1, "datetime"

    .line 122
    iget-object v2, p0, Lcom/bytedance/embedapplog/az;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 124
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "ab_version"

    .line 125
    iget-object v2, p0, Lcom/bytedance/embedapplog/az;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/bytedance/embedapplog/az;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "ab_sdk_version"

    .line 129
    iget-object v2, p0, Lcom/bytedance/embedapplog/az;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_4
    return-object v0
.end method

.method d()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-string v0, "eventv3"

    return-object v0
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/bytedance/embedapplog/az;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected i()V
    .locals 0

    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/bytedance/embedapplog/az;->j:Ljava/lang/String;

    return-object v0
.end method
