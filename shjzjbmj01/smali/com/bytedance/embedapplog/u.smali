.class Lcom/bytedance/embedapplog/u;
.super Lcom/bytedance/embedapplog/n;
.source "SourceFile"


# static fields
.field static final b:[J

.field static final c:[J

.field private static final d:[J


# instance fields
.field private e:Lcom/bytedance/embedapplog/af;

.field private f:Lcom/bytedance/embedapplog/w;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    .line 24
    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/bytedance/embedapplog/u;->b:[J

    const/4 v0, 0x6

    .line 28
    new-array v0, v0, [J

    fill-array-data v0, :array_1

    sput-object v0, Lcom/bytedance/embedapplog/u;->c:[J

    const/16 v0, 0xa

    .line 32
    new-array v0, v0, [J

    fill-array-data v0, :array_2

    sput-object v0, Lcom/bytedance/embedapplog/u;->d:[J

    return-void

    nop

    :array_0
    .array-data 8
        0xea60
        0xea60
        0xea60
        0x1d4c0
        0x1d4c0
        0x1d4c0
        0x2bf20
        0x2bf20
    .end array-data

    :array_1
    .array-data 8
        0x2bf20
        0x2bf20
        0x57e40
        0x57e40
        0x83d60
        0x83d60
    .end array-data

    :array_2
    .array-data 8
        0x2710
        0x2710
        0x4e20
        0x4e20
        0xea60
        0x1770
        0x2bf20
        0x2bf20
        0x83d60
        0x83d60
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/w;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/n;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object p2, p0, Lcom/bytedance/embedapplog/u;->e:Lcom/bytedance/embedapplog/af;

    .line 44
    iput-object p3, p0, Lcom/bytedance/embedapplog/u;->f:Lcom/bytedance/embedapplog/w;

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method b()J
    .locals 4

    .line 54
    iget-object v0, p0, Lcom/bytedance/embedapplog/u;->e:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/af;->p()J

    move-result-wide v0

    iget-object v2, p0, Lcom/bytedance/embedapplog/u;->f:Lcom/bytedance/embedapplog/w;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/w;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x1499700

    goto :goto_0

    :cond_0
    const v2, 0x2932e00

    :goto_0
    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method c()[J
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/bytedance/embedapplog/u;->e:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/af;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    .line 71
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 72
    sget-object v0, Lcom/bytedance/embedapplog/u;->c:[J

    goto :goto_0

    .line 68
    :pswitch_0
    sget-object v0, Lcom/bytedance/embedapplog/u;->b:[J

    goto :goto_0

    .line 65
    :pswitch_1
    sget-object v0, Lcom/bytedance/embedapplog/u;->c:[J

    goto :goto_0

    .line 62
    :pswitch_2
    sget-object v0, Lcom/bytedance/embedapplog/u;->d:[J

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method d()Z
    .locals 6

    .line 79
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/bytedance/embedapplog/u;->e:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/af;->a()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "magic_tag"

    const-string v3, "ss_app_log"

    .line 82
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "header"

    .line 83
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "_gen_time"

    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 85
    iget-object v1, p0, Lcom/bytedance/embedapplog/u;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/embedapplog/u;->e:Lcom/bytedance/embedapplog/af;

    .line 86
    invoke-virtual {v2}, Lcom/bytedance/embedapplog/af;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {}, Lcom/bytedance/embedapplog/at;->a()Lcom/bytedance/embedapplog/util/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/util/a;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getIAppParam()Lcom/bytedance/embedapplog/a;

    move-result-object v5

    .line 85
    invoke-static {v1, v2, v3, v4, v5}, Lcom/bytedance/embedapplog/au;->a(Landroid/content/Context;Lorg/json/JSONObject;Ljava/lang/String;ZLcom/bytedance/embedapplog/a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/bytedance/embedapplog/at;->a(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "device_id"

    const-string v2, ""

    .line 88
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "install_id"

    const-string v3, ""

    .line 89
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ssid"

    const-string v4, ""

    .line 90
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 92
    iget-object v4, p0, Lcom/bytedance/embedapplog/u;->e:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 95
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    const-string v0, "r"

    return-object v0
.end method
