.class public Lcom/bytedance/embedapplog/au;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x18

    .line 25
    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "ab_version"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "device_brand"

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "language"

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const-string v2, "os_api"

    const/4 v6, 0x3

    aput-object v2, v1, v6

    const-string v2, "resolution"

    const/4 v7, 0x4

    aput-object v2, v1, v7

    const-string v2, "google_aid"

    const/4 v8, 0x5

    aput-object v2, v1, v8

    const-string v2, "build_serial"

    const/4 v9, 0x6

    aput-object v2, v1, v9

    const-string v2, "carrier"

    const/4 v10, 0x7

    aput-object v2, v1, v10

    const-string v2, "install_id"

    const/16 v11, 0x8

    aput-object v2, v1, v11

    const-string v2, "package"

    const/16 v12, 0x9

    aput-object v2, v1, v12

    const-string v2, "app_version"

    const/16 v13, 0xa

    aput-object v2, v1, v13

    const-string v2, "device_model"

    const/16 v14, 0xb

    aput-object v2, v1, v14

    const-string v2, "udid"

    const/16 v15, 0xc

    aput-object v2, v1, v15

    const-string v2, "density_dpi"

    const/16 v16, 0xd

    aput-object v2, v1, v16

    const-string v2, "aliyun_uuid"

    const/16 v17, 0xe

    aput-object v2, v1, v17

    const-string v2, "mcc_mnc"

    const/16 v17, 0xf

    aput-object v2, v1, v17

    const-string v2, "sim_region"

    const/16 v17, 0x10

    aput-object v2, v1, v17

    const-string v2, "ab_client"

    const/16 v17, 0x11

    aput-object v2, v1, v17

    const-string v2, "ab_group"

    const/16 v17, 0x12

    aput-object v2, v1, v17

    const-string v2, "ab_feature"

    const/16 v17, 0x13

    aput-object v2, v1, v17

    const-string v2, "device_id"

    const/16 v17, 0x14

    aput-object v2, v1, v17

    const-string v2, "openudid"

    const/16 v17, 0x15

    aput-object v2, v1, v17

    const-string v2, "clientudid"

    const/16 v17, 0x16

    aput-object v2, v1, v17

    const-string v2, "aid"

    const/16 v17, 0x17

    aput-object v2, v1, v17

    sput-object v1, Lcom/bytedance/embedapplog/au;->a:[Ljava/lang/String;

    .line 36
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ab_version"

    aput-object v1, v0, v3

    const-string v1, "device_brand"

    aput-object v1, v0, v4

    const-string v1, "language"

    aput-object v1, v0, v5

    const-string v1, "os_api"

    aput-object v1, v0, v6

    const-string v1, "resolution"

    aput-object v1, v0, v7

    const-string v1, "google_aid"

    aput-object v1, v0, v8

    const-string v1, "build_serial"

    aput-object v1, v0, v9

    const-string v1, "carrier"

    aput-object v1, v0, v10

    const-string v1, "iid"

    aput-object v1, v0, v11

    const-string v1, "app_name"

    aput-object v1, v0, v12

    const-string v1, "version_name"

    aput-object v1, v0, v13

    const-string v1, "device_type"

    aput-object v1, v0, v14

    const-string v1, "uuid"

    aput-object v1, v0, v15

    const-string v1, "dpi"

    aput-object v1, v0, v16

    const-string v1, "aliyun_uuid"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string v1, "mcc_mnc"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string v1, "sim_region"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string v1, "ab_client"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string v1, "ab_group"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    const-string v1, "ab_feature"

    const/16 v2, 0x13

    aput-object v1, v0, v2

    const-string v1, "device_id"

    const/16 v2, 0x14

    aput-object v1, v0, v2

    const-string v1, "openudid"

    const/16 v2, 0x15

    aput-object v1, v0, v2

    const-string v1, "clientudid"

    const/16 v2, 0x16

    aput-object v1, v0, v2

    const-string v1, "aid"

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sput-object v0, Lcom/bytedance/embedapplog/au;->b:[Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;Lorg/json/JSONObject;Ljava/lang/String;ZLcom/bytedance/embedapplog/a;)Ljava/lang/String;
    .locals 6

    .line 48
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p2

    .line 52
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/bytedance/embedapplog/au;->a:[Ljava/lang/String;

    array-length v1, v1

    const/16 v2, 0xa

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 55
    :goto_0
    sget-object v4, Lcom/bytedance/embedapplog/au;->a:[Ljava/lang/String;

    array-length v4, v4

    const/4 v5, 0x0

    if-ge v3, v4, :cond_2

    .line 56
    sget-object v4, Lcom/bytedance/embedapplog/au;->a:[Ljava/lang/String;

    aget-object v4, v4, v3

    invoke-virtual {p1, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 57
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 58
    sget-object v5, Lcom/bytedance/embedapplog/au;->b:[Ljava/lang/String;

    aget-object v5, v5, v3

    invoke-virtual {v0, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    if-eqz p4, :cond_3

    .line 66
    :try_start_0
    invoke-interface {p4, p0}, Lcom/bytedance/embedapplog/a;->a(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object p4

    goto :goto_1

    :catch_0
    move-exception p4

    goto :goto_2

    :cond_3
    move-object p4, v5

    .line 68
    :goto_1
    invoke-static {p0}, Lcom/bytedance/embedapplog/bn;->b(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz p4, :cond_4

    .line 69
    invoke-virtual {v0, p4}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 72
    :goto_2
    invoke-static {p4}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 76
    :cond_4
    :goto_3
    :try_start_1
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getExtraParams()Lcom/bytedance/embedapplog/b;

    move-result-object p4

    if-nez p4, :cond_5

    move-object p4, v5

    goto :goto_4

    :cond_5
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getExtraParams()Lcom/bytedance/embedapplog/b;

    move-result-object p4

    invoke-interface {p4}, Lcom/bytedance/embedapplog/b;->a()Ljava/util/HashMap;

    move-result-object p4

    :goto_4
    if-eqz p4, :cond_6

    .line 78
    invoke-virtual {v0, p4}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception p4

    .line 81
    invoke-static {p4}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 84
    :cond_6
    :goto_5
    sget-object p4, Lcom/bytedance/embedapplog/AppLog;->sCustomNetParams:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p4}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result p4

    if-lez p4, :cond_7

    .line 85
    sget-object p4, Lcom/bytedance/embedapplog/AppLog;->sCustomNetParams:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p4}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_7
    if-eqz p3, :cond_8

    const-string p3, "ssmix"

    const-string p4, "a"

    .line 89
    invoke-virtual {v0, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    :cond_8
    invoke-static {p0}, Lcom/bytedance/embedapplog/bj;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    .line 94
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_9

    const-string p3, "ac"

    .line 95
    invoke-virtual {v0, p3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    const-string p0, "tweaked_channel"

    const-string p3, ""

    .line 99
    invoke-static {p0, p3}, Lcom/bytedance/embedapplog/AppLog;->getHeaderValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 100
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_a

    const-string p0, "channel"

    const-string p3, ""

    .line 101
    invoke-static {p0, p3}, Lcom/bytedance/embedapplog/AppLog;->getHeaderValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 103
    :cond_a
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_b

    const-string p3, "channel"

    .line 104
    invoke-virtual {v0, p3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    const-string p0, "os_version"

    .line 107
    invoke-virtual {p1, p0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_c

    .line 108
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p3

    if-le p3, v2, :cond_c

    .line 109
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_c
    const-string p3, "os_version"

    .line 111
    invoke-virtual {v0, p3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p0, "_rticket"

    .line 112
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p3

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p0, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p0, "device_platform"

    const-string p3, "android"

    .line 113
    invoke-virtual {v0, p0, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p0, "version_code"

    const/4 p3, -0x1

    .line 116
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-static {p0, p4}, Lcom/bytedance/embedapplog/AppLog;->getHeaderValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-eq p0, p3, :cond_d

    const-string p4, "version_code"

    .line 118
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p4, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    const-string p0, "manifest_version_code"

    .line 121
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-static {p0, p4}, Lcom/bytedance/embedapplog/AppLog;->getHeaderValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-eq p0, p3, :cond_e

    const-string p4, "manifest_version_code"

    .line 123
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p4, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_e
    const-string p0, "update_version_code"

    .line 126
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-static {p0, p4}, Lcom/bytedance/embedapplog/AppLog;->getHeaderValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-eq p0, p3, :cond_f

    const-string p3, "update_version_code"

    .line 128
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_f
    const-string p0, "oaid"

    .line 131
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/bytedance/embedapplog/bt;->a(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object p0

    .line 132
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_10

    const-string p3, "oaid"

    .line 133
    invoke-virtual {v0, p3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_10
    const-string p0, "cdid"

    .line 136
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 137
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_11

    const-string p1, "cdid"

    .line 138
    invoke-virtual {v0, p1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_11
    const-string p0, "/"

    .line 140
    invoke-virtual {p2, p0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_12

    .line 141
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 143
    :cond_12
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x3f

    invoke-virtual {p2, p1}, Ljava/lang/String;->indexOf(I)I

    move-result p2

    if-gez p2, :cond_13

    goto :goto_6

    :cond_13
    const/16 p1, 0x26

    :goto_6
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "UTF-8"

    invoke-static {p0, v0, p1}, Lcom/bytedance/embedapplog/bn;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lorg/json/JSONObject;)[Ljava/lang/String;
    .locals 6

    .line 147
    invoke-static {}, Lcom/bytedance/embedapplog/at;->a()Lcom/bytedance/embedapplog/util/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/util/a;->e()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    .line 149
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getEncryptAndCompress()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "/service/2/app_log/?tt_data=a"

    goto :goto_0

    :cond_0
    const-string v1, "/service/2/app_log/?"

    :goto_0
    const/4 v2, 0x0

    .line 154
    :goto_1
    invoke-static {}, Lcom/bytedance/embedapplog/at;->a()Lcom/bytedance/embedapplog/util/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/util/a;->e()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 155
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/bytedance/embedapplog/at;->a()Lcom/bytedance/embedapplog/util/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/embedapplog/util/a;->e()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 156
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getIAppParam()Lcom/bytedance/embedapplog/a;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {p0, p1, v3, v5, v4}, Lcom/bytedance/embedapplog/au;->a(Landroid/content/Context;Lorg/json/JSONObject;Ljava/lang/String;ZLcom/bytedance/embedapplog/a;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 157
    aget-object v3, v0, v2

    sget-object v4, Lcom/bytedance/embedapplog/at;->b:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/bytedance/embedapplog/at;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-object v0
.end method
