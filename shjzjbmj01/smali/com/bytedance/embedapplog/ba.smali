.class public Lcom/bytedance/embedapplog/ba;
.super Lcom/bytedance/embedapplog/av;
.source "SourceFile"


# instance fields
.field public h:I

.field public i:Ljava/lang/String;

.field j:Z

.field volatile k:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/bytedance/embedapplog/av;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/bytedance/embedapplog/av;
    .locals 2
    .param p1    # Landroid/database/Cursor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    .line 52
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/ba;->a:J

    const/4 v0, 0x1

    .line 53
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/ba;->b:J

    const/4 v0, 0x2

    .line 54
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    const/4 v0, 0x3

    .line 55
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ba;->i:Ljava/lang/String;

    const/4 v0, 0x4

    .line 56
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/embedapplog/ba;->h:I

    const/4 v0, 0x5

    .line 57
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ba;->e:Ljava/lang/String;

    const/4 v0, 0x6

    .line 58
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/ba;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 3
    .param p1    # Landroid/content/ContentValues;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    .line 64
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ba;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "tea_event_index"

    .line 65
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ba;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "session_id"

    .line 66
    iget-object v1, p0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ver_name"

    .line 67
    iget-object v1, p0, Lcom/bytedance/embedapplog/ba;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ver_code"

    .line 68
    iget v1, p0, Lcom/bytedance/embedapplog/ba;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "ab_version"

    .line 69
    iget-object v1, p0, Lcom/bytedance/embedapplog/ba;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ab_sdk_version"

    .line 70
    iget-object v1, p0, Lcom/bytedance/embedapplog/ba;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected a(Lorg/json/JSONObject;)V
    .locals 3
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    .line 75
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ba;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "tea_event_index"

    .line 76
    iget-wide v1, p0, Lcom/bytedance/embedapplog/ba;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "session_id"

    .line 77
    iget-object v1, p0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ab_version"

    .line 78
    iget-object v1, p0, Lcom/bytedance/embedapplog/ba;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ab_sdk_version"

    .line 79
    iget-object v1, p0, Lcom/bytedance/embedapplog/ba;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method protected a()[Ljava/lang/String;
    .locals 3

    const/16 v0, 0xe

    .line 38
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "local_time_ms"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "tea_event_index"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "session_id"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "ver_name"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "ver_code"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string v1, "ab_version"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string v1, "ab_sdk_version"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    return-object v0
.end method

.method protected b(Lorg/json/JSONObject;)Lcom/bytedance/embedapplog/av;
    .locals 5
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    const-wide/16 v1, 0x0

    .line 104
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/bytedance/embedapplog/ba;->a:J

    const-string v0, "tea_event_index"

    .line 105
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/ba;->b:J

    const-string v0, "session_id"

    const/4 v1, 0x0

    .line 106
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    const-string v0, "ab_version"

    .line 107
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/ba;->e:Ljava/lang/String;

    const-string v0, "ab_sdk_version"

    .line 108
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/ba;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected b()Lorg/json/JSONObject;
    .locals 4

    .line 84
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "local_time_ms"

    .line 85
    iget-wide v2, p0, Lcom/bytedance/embedapplog/ba;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "tea_event_index"

    .line 86
    iget-wide v2, p0, Lcom/bytedance/embedapplog/ba;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "session_id"

    .line 87
    iget-object v2, p0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 88
    iget-boolean v1, p0, Lcom/bytedance/embedapplog/ba;->j:Z

    if-eqz v1, :cond_0

    const-string v1, "is_background"

    .line 89
    iget-boolean v2, p0, Lcom/bytedance/embedapplog/ba;->j:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    :cond_0
    const-string v1, "datetime"

    .line 91
    iget-object v2, p0, Lcom/bytedance/embedapplog/ba;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 92
    iget-object v1, p0, Lcom/bytedance/embedapplog/ba;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "ab_version"

    .line 93
    iget-object v2, p0, Lcom/bytedance/embedapplog/ba;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/bytedance/embedapplog/ba;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ab_sdk_version"

    .line 97
    iget-object v2, p0, Lcom/bytedance/embedapplog/ba;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    return-object v0
.end method

.method d()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-string v0, "launch"

    return-object v0
.end method
