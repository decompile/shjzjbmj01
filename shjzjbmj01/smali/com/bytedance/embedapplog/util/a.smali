.class public Lcom/bytedance/embedapplog/util/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:[Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    .line 24
    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "https://toblog.ctobsnssdk.com"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "https://tobapplog.ctobsnssdk.com"

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sput-object v1, Lcom/bytedance/embedapplog/util/a;->a:[Ljava/lang/String;

    .line 44
    new-array v1, v0, [Ljava/lang/String;

    const-string v2, "https://toblog.tobsnssdk.com"

    aput-object v2, v1, v3

    const-string v2, "https://tobapplog.tobsnssdk.com"

    aput-object v2, v1, v4

    sput-object v1, Lcom/bytedance/embedapplog/util/a;->b:[Ljava/lang/String;

    .line 64
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "https://toblog.itobsnssdk.com"

    aput-object v1, v0, v3

    const-string v1, "https://tobapplog.itobsnssdk.com"

    aput-object v1, v0, v4

    sput-object v0, Lcom/bytedance/embedapplog/util/a;->c:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-direct {p0}, Lcom/bytedance/embedapplog/util/a;->f()V

    return-void
.end method

.method public static a(I)Lcom/bytedance/embedapplog/util/a;
    .locals 1

    .line 91
    new-instance v0, Lcom/bytedance/embedapplog/util/a;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/util/a;-><init>()V

    packed-switch p0, :pswitch_data_0

    .line 103
    invoke-direct {v0}, Lcom/bytedance/embedapplog/util/a;->f()V

    goto :goto_0

    .line 100
    :pswitch_0
    invoke-direct {v0}, Lcom/bytedance/embedapplog/util/a;->h()V

    goto :goto_0

    .line 97
    :pswitch_1
    invoke-direct {v0}, Lcom/bytedance/embedapplog/util/a;->g()V

    goto :goto_0

    .line 94
    :pswitch_2
    invoke-direct {v0}, Lcom/bytedance/embedapplog/util/a;->f()V

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private f()V
    .locals 1

    const-string v0, "https://toblog.ctobsnssdk.com/service/2/device_register_only/"

    .line 30
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->d:Ljava/lang/String;

    const-string v0, "https://toblog.ctobsnssdk.com/service/2/app_alert_check/"

    .line 31
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->e:Ljava/lang/String;

    const-string v0, "https://toblog.ctobsnssdk.com/service/2/log_settings/"

    .line 32
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->f:Ljava/lang/String;

    const-string v0, "https://toblog.ctobsnssdk.com/service/2/abtest_config/"

    .line 33
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->g:Ljava/lang/String;

    .line 34
    sget-object v0, Lcom/bytedance/embedapplog/util/a;->a:[Ljava/lang/String;

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->h:[Ljava/lang/String;

    const-string v0, "https://success.ctobsnssdk.com"

    .line 35
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->i:Ljava/lang/String;

    return-void
.end method

.method private g()V
    .locals 1

    const-string v0, "https://toblog.tobsnssdk.com/service/2/device_register_only/"

    .line 50
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->d:Ljava/lang/String;

    const-string v0, "https://toblog.tobsnssdk.com/service/2/app_alert_check/"

    .line 51
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->e:Ljava/lang/String;

    const-string v0, "https://toblog.tobsnssdk.com/service/2/log_settings/"

    .line 52
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->f:Ljava/lang/String;

    const-string v0, "https://toblog.tobsnssdk.com/service/2/abtest_config/"

    .line 53
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->g:Ljava/lang/String;

    .line 54
    sget-object v0, Lcom/bytedance/embedapplog/util/a;->b:[Ljava/lang/String;

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->h:[Ljava/lang/String;

    const-string v0, "https://success.tobsnssdk.com"

    .line 55
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->i:Ljava/lang/String;

    return-void
.end method

.method private h()V
    .locals 1

    const-string v0, "https://toblog.itobsnssdk.com/service/2/device_register_only/"

    .line 70
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->d:Ljava/lang/String;

    const-string v0, "https://toblog.itobsnssdk.com/service/2/app_alert_check/"

    .line 71
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->e:Ljava/lang/String;

    const-string v0, "https://toblog.itobsnssdk.com/service/2/log_settings/"

    .line 72
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->f:Ljava/lang/String;

    const-string v0, "https://toblog.itobsnssdk.com/service/2/abtest_config/"

    .line 73
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->g:Ljava/lang/String;

    .line 74
    sget-object v0, Lcom/bytedance/embedapplog/util/a;->c:[Ljava/lang/String;

    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->h:[Ljava/lang/String;

    const-string v0, "https://success.itobsnssdk.com"

    .line 75
    iput-object v0, p0, Lcom/bytedance/embedapplog/util/a;->i:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public e()[Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/bytedance/embedapplog/util/a;->h:[Ljava/lang/String;

    return-object v0
.end method
