.class Lcom/bytedance/embedapplog/ad;
.super Lcom/bytedance/embedapplog/z;
.source "SourceFile"


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Lcom/bytedance/embedapplog/ae;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/ae;)V
    .locals 1

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, v0, v0}, Lcom/bytedance/embedapplog/z;-><init>(ZZ)V

    .line 23
    iput-object p1, p0, Lcom/bytedance/embedapplog/ad;->e:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    return-void
.end method


# virtual methods
.method protected a(Lorg/json/JSONObject;)Z
    .locals 3

    const-string v0, "sdk_version"

    const/16 v1, 0x150

    .line 29
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "sdk_version_name"

    const-string v1, "3.6.0-rc.8-embed"

    .line 30
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "channel"

    .line 31
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "aid"

    .line 32
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "release_build"

    .line 33
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->z()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "app_region"

    .line 34
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "app_language"

    .line 35
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "user_agent"

    .line 36
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->A()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ab_sdk_version"

    .line 37
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ab_version"

    .line 38
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->s()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "aliyun_uuid"

    .line 39
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->k()Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/bytedance/embedapplog/ad;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bi;->a(Landroid/content/Context;Lcom/bytedance/embedapplog/ae;)Ljava/lang/String;

    move-result-object v0

    .line 45
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "google_aid"

    .line 46
    invoke-static {p1, v1, v0}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->y()Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    :try_start_0
    const-string v1, "app_track"

    .line 52
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 54
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 58
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 59
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    const-string v1, "custom"

    .line 60
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_3
    const-string v0, "user_unique_id"

    .line 72
    iget-object v1, p0, Lcom/bytedance/embedapplog/ad;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method
