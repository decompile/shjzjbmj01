.class public Lcom/bytedance/embedapplog/bb;
.super Lcom/bytedance/embedapplog/av;
.source "SourceFile"


# instance fields
.field public h:[B

.field i:I

.field public j:I

.field private k:Lorg/json/JSONArray;

.field private l:Lorg/json/JSONArray;

.field private m:Lcom/bytedance/embedapplog/ba;

.field private n:Lorg/json/JSONArray;

.field private o:Lcom/bytedance/embedapplog/be;

.field private p:Lorg/json/JSONObject;

.field private q:Lorg/json/JSONArray;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/bytedance/embedapplog/av;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/bytedance/embedapplog/av;
    .locals 2
    .param p1    # Landroid/database/Cursor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    .line 110
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/bb;->a:J

    const/4 v0, 0x1

    .line 111
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/bb;->h:[B

    const/4 v0, 0x2

    .line 112
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result p1

    iput p1, p0, Lcom/bytedance/embedapplog/bb;->i:I

    const/4 p1, 0x0

    .line 113
    iput-object p1, p0, Lcom/bytedance/embedapplog/bb;->p:Lorg/json/JSONObject;

    .line 114
    iput-object p1, p0, Lcom/bytedance/embedapplog/bb;->m:Lcom/bytedance/embedapplog/ba;

    .line 115
    iput-object p1, p0, Lcom/bytedance/embedapplog/bb;->o:Lcom/bytedance/embedapplog/be;

    .line 116
    iput-object p1, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    .line 117
    iput-object p1, p0, Lcom/bytedance/embedapplog/bb;->k:Lorg/json/JSONArray;

    .line 118
    iput-object p1, p0, Lcom/bytedance/embedapplog/bb;->l:Lorg/json/JSONArray;

    .line 119
    iput-object p1, p0, Lcom/bytedance/embedapplog/bb;->q:Lorg/json/JSONArray;

    return-object p0
.end method

.method a(JLorg/json/JSONObject;Lcom/bytedance/embedapplog/ba;Lcom/bytedance/embedapplog/be;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;)V
    .locals 0

    .line 81
    iput-wide p1, p0, Lcom/bytedance/embedapplog/bb;->a:J

    .line 82
    iput-object p3, p0, Lcom/bytedance/embedapplog/bb;->p:Lorg/json/JSONObject;

    .line 83
    iput-object p4, p0, Lcom/bytedance/embedapplog/bb;->m:Lcom/bytedance/embedapplog/ba;

    .line 84
    iput-object p5, p0, Lcom/bytedance/embedapplog/bb;->o:Lcom/bytedance/embedapplog/be;

    const-string p1, "autoChina"

    const-string p2, "Base"

    .line 85
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "autoChina"

    const-string p2, "base"

    .line 86
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 90
    :cond_0
    iput-object p6, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 88
    iput-object p1, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    .line 92
    :goto_1
    iput-object p7, p0, Lcom/bytedance/embedapplog/bb;->k:Lorg/json/JSONArray;

    .line 93
    iput-object p8, p0, Lcom/bytedance/embedapplog/bb;->l:Lorg/json/JSONArray;

    .line 94
    iput-object p9, p0, Lcom/bytedance/embedapplog/bb;->q:Lorg/json/JSONArray;

    return-void
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 3
    .param p1    # Landroid/content/ContentValues;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    .line 125
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 127
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/bb;->f()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/embedapplog/AppLog;->toEncryptByte(Ljava/lang/String;)[B

    move-result-object v0

    const-string v1, "_data"

    .line 129
    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    return-void
.end method

.method protected a(Lorg/json/JSONObject;)V
    .locals 0
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 p1, 0x0

    .line 174
    invoke-static {p1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method protected a()[Ljava/lang/String;
    .locals 3

    const/16 v0, 0x8

    .line 99
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "local_time_ms"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "_data"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "blob"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "_fail"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "_full"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    return-object v0
.end method

.method protected b(Lorg/json/JSONObject;)Lcom/bytedance/embedapplog/av;
    .locals 0
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 p1, 0x0

    .line 254
    invoke-static {p1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    return-object p1
.end method

.method protected b()Lorg/json/JSONObject;
    .locals 10

    .line 179
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "magic_tag"

    const-string v2, "ss_app_log"

    .line 180
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "header"

    .line 181
    iget-object v2, p0, Lcom/bytedance/embedapplog/bb;->p:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "time_sync"

    .line 182
    sget-object v2, Lcom/bytedance/embedapplog/at;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 184
    iget-object v1, p0, Lcom/bytedance/embedapplog/bb;->m:Lcom/bytedance/embedapplog/ba;

    if-eqz v1, :cond_0

    .line 185
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 186
    iget-object v2, p0, Lcom/bytedance/embedapplog/bb;->m:Lcom/bytedance/embedapplog/ba;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/ba;->f()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    const-string v2, "launch"

    .line 187
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/bytedance/embedapplog/bb;->o:Lcom/bytedance/embedapplog/be;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    .line 191
    iget-object v1, p0, Lcom/bytedance/embedapplog/bb;->o:Lcom/bytedance/embedapplog/be;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/be;->f()Lorg/json/JSONObject;

    move-result-object v1

    .line 195
    iget-object v3, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v3

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    .line 196
    :goto_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v3, :cond_2

    .line 198
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 199
    new-instance v7, Lorg/json/JSONObject;

    new-instance v8, Lorg/json/JSONObject;

    iget-object v9, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    .line 200
    invoke-virtual {v9, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v9, "params"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v8, "page_key"

    const-string v9, ""

    .line 201
    invoke-virtual {v7, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v2, v8}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    const-string v8, "duration"

    .line 202
    invoke-virtual {v7, v8, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v7

    add-int/lit16 v7, v7, 0x3e7

    div-int/lit16 v7, v7, 0x3e8

    const/4 v8, 0x1

    invoke-virtual {v6, v8, v7}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;

    .line 203
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    if-lez v3, :cond_3

    const-string v3, "activites"

    .line 206
    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 210
    :cond_3
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 211
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    const-string v1, "terminate"

    .line 212
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 215
    :cond_4
    iget-object v1, p0, Lcom/bytedance/embedapplog/bb;->k:Lorg/json/JSONArray;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/bytedance/embedapplog/bb;->k:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    :goto_2
    if-lez v1, :cond_6

    const-string v3, "event"

    .line 217
    iget-object v4, p0, Lcom/bytedance/embedapplog/bb;->k:Lorg/json/JSONArray;

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 220
    :cond_6
    iget-object v3, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v3

    goto :goto_3

    :cond_7
    const/4 v3, 0x0

    .line 221
    :goto_3
    iget-object v4, p0, Lcom/bytedance/embedapplog/bb;->l:Lorg/json/JSONArray;

    if-nez v4, :cond_8

    .line 222
    iget-object v4, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    iput-object v4, p0, Lcom/bytedance/embedapplog/bb;->l:Lorg/json/JSONArray;

    goto :goto_5

    :cond_8
    if-lez v3, :cond_9

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v3, :cond_9

    .line 225
    iget-object v5, p0, Lcom/bytedance/embedapplog/bb;->l:Lorg/json/JSONArray;

    iget-object v6, p0, Lcom/bytedance/embedapplog/bb;->n:Lorg/json/JSONArray;

    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 229
    :cond_9
    :goto_5
    iget-object v4, p0, Lcom/bytedance/embedapplog/bb;->l:Lorg/json/JSONArray;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/bytedance/embedapplog/bb;->l:Lorg/json/JSONArray;

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v4

    goto :goto_6

    :cond_a
    const/4 v4, 0x0

    :goto_6
    if-lez v4, :cond_b

    const-string v5, "event_v3"

    .line 231
    iget-object v6, p0, Lcom/bytedance/embedapplog/bb;->l:Lorg/json/JSONArray;

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 235
    :cond_b
    iget-object v5, p0, Lcom/bytedance/embedapplog/bb;->q:Lorg/json/JSONArray;

    if-eqz v5, :cond_c

    iget-object v2, p0, Lcom/bytedance/embedapplog/bb;->q:Lorg/json/JSONArray;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2

    :cond_c
    if-lez v2, :cond_d

    const-string v5, "log_data"

    .line 237
    iget-object v6, p0, Lcom/bytedance/embedapplog/bb;->q:Lorg/json/JSONArray;

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 240
    :cond_d
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "wP {"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 241
    iget-object v6, p0, Lcom/bytedance/embedapplog/bb;->m:Lcom/bytedance/embedapplog/ba;

    if-eqz v6, :cond_e

    iget-object v6, p0, Lcom/bytedance/embedapplog/bb;->m:Lcom/bytedance/embedapplog/ba;

    goto :goto_7

    :cond_e
    const-string v6, "la"

    :goto_7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, ", "

    .line 242
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/bytedance/embedapplog/bb;->o:Lcom/bytedance/embedapplog/be;

    if-eqz v6, :cond_f

    iget-object v6, p0, Lcom/bytedance/embedapplog/bb;->o:Lcom/bytedance/embedapplog/be;

    goto :goto_8

    :cond_f
    const-string v6, "te"

    :goto_8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, ", p: "

    .line 243
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ", v1: "

    .line 244
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", v3: "

    .line 245
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", m: "

    .line 246
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/bytedance/embedapplog/bm;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method d()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-string v0, "pack"

    return-object v0
.end method
