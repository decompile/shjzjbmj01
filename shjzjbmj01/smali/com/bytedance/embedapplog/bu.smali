.class public final Lcom/bytedance/embedapplog/bu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:I

.field private static b:Lcom/bytedance/embed_dr/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    .line 11
    invoke-static {v0}, Lcom/bytedance/embedapplog/bu;->a(I)V

    .line 12
    new-instance v0, Lcom/bytedance/embed_dr/a$a;

    invoke-direct {v0}, Lcom/bytedance/embed_dr/a$a;-><init>()V

    invoke-static {v0}, Lcom/bytedance/embedapplog/bu;->a(Lcom/bytedance/embed_dr/a;)V

    return-void
.end method

.method public static a(I)V
    .locals 0

    .line 24
    sput p0, Lcom/bytedance/embedapplog/bu;->a:I

    return-void
.end method

.method public static a(Lcom/bytedance/embed_dr/a;)V
    .locals 0

    .line 16
    sput-object p0, Lcom/bytedance/embedapplog/bu;->b:Lcom/bytedance/embed_dr/a;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 32
    invoke-static {p0, p1, v0}, Lcom/bytedance/embedapplog/bu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 36
    sget-object v0, Lcom/bytedance/embedapplog/bu;->b:Lcom/bytedance/embed_dr/a;

    if-eqz v0, :cond_0

    sget v0, Lcom/bytedance/embedapplog/bu;->a:I

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    .line 37
    sget-object v0, Lcom/bytedance/embedapplog/bu;->b:Lcom/bytedance/embed_dr/a;

    invoke-interface {v0, p0, p1, p2}, Lcom/bytedance/embed_dr/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 42
    invoke-static {p0, p1, v0}, Lcom/bytedance/embedapplog/bu;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 46
    sget-object v0, Lcom/bytedance/embedapplog/bu;->b:Lcom/bytedance/embed_dr/a;

    if-eqz v0, :cond_0

    sget v0, Lcom/bytedance/embedapplog/bu;->a:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 47
    sget-object v0, Lcom/bytedance/embedapplog/bu;->b:Lcom/bytedance/embed_dr/a;

    invoke-interface {v0, p0, p1, p2}, Lcom/bytedance/embed_dr/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 62
    invoke-static {p0, p1, v0}, Lcom/bytedance/embedapplog/bu;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 66
    sget-object v0, Lcom/bytedance/embedapplog/bu;->b:Lcom/bytedance/embed_dr/a;

    if-eqz v0, :cond_0

    sget v0, Lcom/bytedance/embedapplog/bu;->a:I

    const/4 v1, 0x6

    if-gt v0, v1, :cond_0

    .line 67
    sget-object v0, Lcom/bytedance/embedapplog/bu;->b:Lcom/bytedance/embed_dr/a;

    invoke-interface {v0, p0, p1, p2}, Lcom/bytedance/embed_dr/a;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method
