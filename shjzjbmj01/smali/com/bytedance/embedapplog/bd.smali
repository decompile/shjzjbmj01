.class public Lcom/bytedance/embedapplog/bd;
.super Lcom/bytedance/embedapplog/av;
.source "SourceFile"


# static fields
.field static h:Ljava/lang/String; = "succEvent"


# instance fields
.field public i:I

.field private j:Ljava/lang/String;

.field private k:I

.field private l:Ljava/lang/String;

.field private m:I

.field private n:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/bytedance/embedapplog/av;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .line 48
    invoke-direct {p0}, Lcom/bytedance/embedapplog/av;-><init>()V

    const/4 v0, 0x1

    .line 50
    iput v0, p0, Lcom/bytedance/embedapplog/bd;->k:I

    .line 52
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getSuccRate()I

    move-result v0

    iput v0, p0, Lcom/bytedance/embedapplog/bd;->i:I

    .line 53
    iput-object p1, p0, Lcom/bytedance/embedapplog/bd;->j:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/bytedance/embedapplog/bd;->l:Ljava/lang/String;

    .line 55
    iput p3, p0, Lcom/bytedance/embedapplog/bd;->m:I

    .line 56
    invoke-static {}, Lcom/bytedance/embedapplog/bl;->a()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/embedapplog/bd;->n:J

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/bytedance/embedapplog/av;
    .locals 2
    .param p1    # Landroid/database/Cursor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    .line 81
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/bd;->a:J

    const/4 v0, 0x1

    .line 82
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/bd;->b:J

    const/4 v0, 0x2

    .line 83
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/bd;->c:Ljava/lang/String;

    const/4 v0, 0x3

    .line 84
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/bd;->d:Ljava/lang/String;

    const/4 v0, 0x4

    .line 85
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/bd;->j:Ljava/lang/String;

    const/4 v0, 0x5

    .line 86
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/embedapplog/bd;->k:I

    const/4 v0, 0x6

    .line 87
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/embedapplog/bd;->i:I

    const/4 v0, 0x7

    .line 88
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/bd;->l:Ljava/lang/String;

    const/16 v0, 0x8

    .line 89
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/embedapplog/bd;->m:I

    const/16 v0, 0x9

    .line 90
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/bd;->n:J

    return-object p0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 3
    .param p1    # Landroid/content/ContentValues;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    .line 96
    iget-wide v1, p0, Lcom/bytedance/embedapplog/bd;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "tea_event_index"

    .line 97
    iget-wide v1, p0, Lcom/bytedance/embedapplog/bd;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "session_id"

    .line 98
    iget-object v1, p0, Lcom/bytedance/embedapplog/bd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "user_unique_id"

    .line 99
    iget-object v1, p0, Lcom/bytedance/embedapplog/bd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event_name"

    .line 100
    iget-object v1, p0, Lcom/bytedance/embedapplog/bd;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "is_monitor"

    .line 101
    iget v1, p0, Lcom/bytedance/embedapplog/bd;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "bav_monitor_rate"

    .line 102
    iget v1, p0, Lcom/bytedance/embedapplog/bd;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "monitor_status"

    .line 103
    iget-object v1, p0, Lcom/bytedance/embedapplog/bd;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "monitor_num"

    .line 104
    iget v1, p0, Lcom/bytedance/embedapplog/bd;->m:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "date"

    .line 105
    iget-wide v1, p0, Lcom/bytedance/embedapplog/bd;->n:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method protected a(Lorg/json/JSONObject;)V
    .locals 3
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    .line 110
    iget-wide v1, p0, Lcom/bytedance/embedapplog/bd;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "tea_event_index"

    .line 111
    iget-wide v1, p0, Lcom/bytedance/embedapplog/bd;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "session_id"

    .line 112
    iget-object v1, p0, Lcom/bytedance/embedapplog/bd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "user_unique_id"

    .line 113
    iget-object v1, p0, Lcom/bytedance/embedapplog/bd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "event_name"

    .line 114
    iget-object v1, p0, Lcom/bytedance/embedapplog/bd;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "is_monitor"

    .line 115
    iget v1, p0, Lcom/bytedance/embedapplog/bd;->k:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "bav_monitor_rate"

    .line 116
    iget v1, p0, Lcom/bytedance/embedapplog/bd;->i:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "monitor_status"

    .line 117
    iget-object v1, p0, Lcom/bytedance/embedapplog/bd;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "monitor_num"

    .line 118
    iget v1, p0, Lcom/bytedance/embedapplog/bd;->m:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "date"

    .line 119
    iget-wide v1, p0, Lcom/bytedance/embedapplog/bd;->n:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    return-void
.end method

.method protected a()[Ljava/lang/String;
    .locals 3

    const/16 v0, 0x14

    .line 62
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "local_time_ms"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "tea_event_index"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "session_id"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "user_unique_id"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "event_name"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string v1, "is_monitor"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string v1, "bav_monitor_rate"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string v1, "monitor_status"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string v1, "varchar"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string v1, "monitor_num"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string v1, "date"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    const-string v1, "integer"

    const/16 v2, 0x13

    aput-object v1, v0, v2

    return-object v0
.end method

.method protected b(Lorg/json/JSONObject;)Lcom/bytedance/embedapplog/av;
    .locals 5
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "local_time_ms"

    const-wide/16 v1, 0x0

    .line 142
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/bytedance/embedapplog/bd;->a:J

    const-string v0, "tea_event_index"

    .line 143
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/bytedance/embedapplog/bd;->b:J

    const-string v0, "session_id"

    const/4 v3, 0x0

    .line 144
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/bd;->c:Ljava/lang/String;

    const-string v0, "user_unique_id"

    .line 145
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/bd;->d:Ljava/lang/String;

    const-string v0, "event_name"

    .line 146
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/bd;->j:Ljava/lang/String;

    const-string v0, "is_monitor"

    const/4 v4, 0x0

    .line 147
    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/embedapplog/bd;->k:I

    const-string v0, "bav_monitor_rate"

    .line 148
    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/embedapplog/bd;->i:I

    const-string v0, "monitor_status"

    .line 149
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/embedapplog/bd;->l:Ljava/lang/String;

    const-string v0, "monitor_num"

    .line 150
    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/embedapplog/bd;->m:I

    const-string v0, "date"

    .line 151
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/embedapplog/bd;->n:J

    return-object p0
.end method

.method protected b()Lorg/json/JSONObject;
    .locals 3

    .line 131
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "event_name"

    .line 132
    iget-object v2, p0, Lcom/bytedance/embedapplog/bd;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "is_monitor"

    .line 133
    iget v2, p0, Lcom/bytedance/embedapplog/bd;->k:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "bav_monitor_rate"

    .line 134
    iget v2, p0, Lcom/bytedance/embedapplog/bd;->i:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "monitor_status"

    .line 135
    iget-object v2, p0, Lcom/bytedance/embedapplog/bd;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "monitor_num"

    .line 136
    iget v2, p0, Lcom/bytedance/embedapplog/bd;->m:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    return-object v0
.end method

.method d()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 158
    sget-object v0, Lcom/bytedance/embedapplog/bd;->h:Ljava/lang/String;

    return-object v0
.end method
