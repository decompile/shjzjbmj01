.class final Lcom/bytedance/embedapplog/ce;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/Boolean;

.field final d:Ljava/lang/Long;

.field final e:Ljava/lang/Long;

.field final f:Ljava/lang/Integer;

.field final g:Ljava/lang/Long;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/bytedance/embedapplog/ce;->a:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lcom/bytedance/embedapplog/ce;->b:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Lcom/bytedance/embedapplog/ce;->c:Ljava/lang/Boolean;

    .line 55
    iput-object p4, p0, Lcom/bytedance/embedapplog/ce;->d:Ljava/lang/Long;

    .line 56
    iput-object p5, p0, Lcom/bytedance/embedapplog/ce;->e:Ljava/lang/Long;

    .line 57
    iput-object p6, p0, Lcom/bytedance/embedapplog/ce;->f:Ljava/lang/Integer;

    .line 58
    iput-object p7, p0, Lcom/bytedance/embedapplog/ce;->g:Ljava/lang/Long;

    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/bytedance/embedapplog/ce;
    .locals 11
    .param p0    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/AnyThread;
    .end annotation

    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 93
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 97
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "id"

    .line 99
    invoke-virtual {v0, p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string p0, "req_id"

    .line 100
    invoke-virtual {v0, p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string p0, "is_track_limited"

    .line 107
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    const-string p0, "is_track_limited"

    .line 108
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    move-object v5, p0

    goto :goto_0

    :cond_1
    move-object v5, v1

    :goto_0
    const-string p0, "take_ms"

    .line 110
    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    const-wide/16 v6, -0x1

    if-eqz p0, :cond_2

    const-string p0, "take_ms"

    .line 111
    invoke-virtual {v0, p0, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    goto :goto_1

    :cond_2
    move-object p0, v1

    :goto_1
    const-string v2, "time"

    .line 113
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "time"

    .line 114
    invoke-virtual {v0, v2, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v8, v2

    goto :goto_2

    :cond_3
    move-object v8, v1

    :goto_2
    const-string v2, "query_times"

    .line 116
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "query_times"

    const/4 v9, -0x1

    .line 117
    invoke-virtual {v0, v2, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v9, v2

    goto :goto_3

    :cond_4
    move-object v9, v1

    :goto_3
    const-string v2, "hw_id_version_code"

    .line 119
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "hw_id_version_code"

    .line 120
    invoke-virtual {v0, v2, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_4

    :cond_5
    move-object v0, v1

    .line 122
    :goto_4
    new-instance v10, Lcom/bytedance/embedapplog/ce;

    move-object v2, v10

    move-object v6, p0

    move-object v7, v8

    move-object v8, v9

    move-object v9, v0

    invoke-direct/range {v2 .. v9}, Lcom/bytedance/embedapplog/ce;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v10

    :catch_0
    move-exception p0

    .line 130
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    return-object v1
.end method


# virtual methods
.method a()Ljava/util/Map;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "id"

    .line 68
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v1, "req_id"

    .line 69
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v1, "is_track_limited"

    .line 70
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->c:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v1, "take_ms"

    .line 71
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->d:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v1, "time"

    .line 72
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->e:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v1, "query_times"

    .line 73
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->f:Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    const-string v1, "hw_id_version_code"

    .line 74
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->g:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method b()Lorg/json/JSONObject;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 79
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "id"

    .line 80
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "req_id"

    .line 81
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "is_track_limited"

    .line 82
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->c:Ljava/lang/Boolean;

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "take_ms"

    .line 83
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->d:Ljava/lang/Long;

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "time"

    .line 84
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->e:Ljava/lang/Long;

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "query_times"

    .line 85
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->f:Ljava/lang/Integer;

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "hw_id_version_code"

    .line 86
    iget-object v2, p0, Lcom/bytedance/embedapplog/ce;->g:Ljava/lang/Long;

    invoke-static {v0, v1, v2}, Lcom/bytedance/embedapplog/cb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 63
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/ce;->b()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
