.class Lcom/bytedance/embedapplog/ap;
.super Lcom/bytedance/embedapplog/z;
.source "SourceFile"


# instance fields
.field private final e:Lcom/bytedance/embedapplog/ae;


# direct methods
.method constructor <init>(Lcom/bytedance/embedapplog/ae;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 20
    invoke-direct {p0, v1, v0, v0}, Lcom/bytedance/embedapplog/z;-><init>(ZZZ)V

    .line 21
    iput-object p1, p0, Lcom/bytedance/embedapplog/ap;->e:Lcom/bytedance/embedapplog/ae;

    return-void
.end method


# virtual methods
.method protected a(Lorg/json/JSONObject;)Z
    .locals 8

    .line 26
    iget-object v0, p0, Lcom/bytedance/embedapplog/ap;->e:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "install_id"

    const/4 v2, 0x0

    .line 27
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "device_id"

    .line 28
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ssid"

    .line 29
    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "install_id"

    .line 31
    invoke-static {p1, v4, v1}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "device_id"

    .line 32
    invoke-static {p1, v4, v3}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ssid"

    .line 33
    invoke-static {p1, v4, v2}, Lcom/bytedance/embedapplog/af;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "register_time"

    const-wide/16 v4, 0x0

    .line 34
    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 35
    invoke-static {v1}, Lcom/bytedance/embedapplog/af;->e(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v3}, Lcom/bytedance/embedapplog/af;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    cmp-long v1, v6, v4

    if-eqz v1, :cond_1

    .line 38
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "register_time"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :cond_1
    move-wide v4, v6

    :goto_0
    const-string v0, "register_time"

    .line 41
    invoke-virtual {p1, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const/4 p1, 0x1

    return p1
.end method
