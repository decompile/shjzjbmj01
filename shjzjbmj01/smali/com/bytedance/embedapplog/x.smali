.class Lcom/bytedance/embedapplog/x;
.super Lcom/bytedance/embedapplog/z;
.source "SourceFile"


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Lcom/bytedance/embedapplog/ae;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/ae;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22
    invoke-direct {p0, v0, v1}, Lcom/bytedance/embedapplog/z;-><init>(ZZ)V

    .line 23
    iput-object p1, p0, Lcom/bytedance/embedapplog/x;->e:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    return-void
.end method


# virtual methods
.method protected a(Lorg/json/JSONObject;)Z
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ab_client"

    .line 30
    iget-object v1, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->I()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 33
    sget-boolean v0, Lcom/bytedance/embedapplog/bm;->b:Z

    if-eqz v0, :cond_1

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init config has abversion:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    const-string v0, "ab_version"

    .line 36
    iget-object v1, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38
    :cond_2
    iget-object v0, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->J()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ab_group"

    .line 39
    iget-object v1, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->J()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 41
    :cond_3
    iget-object v0, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->K()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "ab_feature"

    .line 42
    iget-object v1, p0, Lcom/bytedance/embedapplog/x;->f:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_4
    const/4 p1, 0x1

    return p1
.end method
