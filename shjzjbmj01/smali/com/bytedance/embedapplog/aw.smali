.class public Lcom/bytedance/embedapplog/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/embedapplog/aw$a;
    }
.end annotation


# static fields
.field static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/bytedance/embedapplog/av;",
            ">;"
        }
    .end annotation
.end field

.field private static b:I

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static e:Ljava/lang/String;

.field private static f:Ljava/lang/String;


# instance fields
.field private final g:Lcom/bytedance/embedapplog/ae;

.field private final h:Lcom/bytedance/embedapplog/af;

.field private final i:Lcom/bytedance/embedapplog/aw$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT * FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/bytedance/embedapplog/bd;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "event_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " =?  AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "monitor_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/bytedance/embedapplog/aw;->c:Ljava/lang/String;

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UPDATE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/bytedance/embedapplog/bd;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " SET "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "monitor_num"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " =? WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "event_name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " =? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "monitor_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " =?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/bytedance/embedapplog/aw;->d:Ljava/lang/String;

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT * FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/bytedance/embedapplog/bd;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "date"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "<? ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "local_time_ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " LIMIT ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/bytedance/embedapplog/aw;->e:Ljava/lang/String;

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DELETE FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/bytedance/embedapplog/bd;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "local_time_ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " <= ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/bytedance/embedapplog/aw;->f:Ljava/lang/String;

    .line 112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    .line 113
    new-instance v0, Lcom/bytedance/embedapplog/bc;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/bc;-><init>()V

    invoke-static {v0}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;)V

    .line 114
    new-instance v0, Lcom/bytedance/embedapplog/ax;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/ax;-><init>()V

    invoke-static {v0}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;)V

    .line 115
    new-instance v0, Lcom/bytedance/embedapplog/az;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v1}, Lcom/bytedance/embedapplog/az;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;)V

    .line 116
    new-instance v0, Lcom/bytedance/embedapplog/ba;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/ba;-><init>()V

    invoke-static {v0}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;)V

    .line 117
    new-instance v0, Lcom/bytedance/embedapplog/be;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/be;-><init>()V

    invoke-static {v0}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;)V

    .line 118
    new-instance v0, Lcom/bytedance/embedapplog/bb;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/bb;-><init>()V

    invoke-static {v0}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;)V

    .line 119
    new-instance v0, Lcom/bytedance/embedapplog/ay;

    const-string v1, ""

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/bytedance/embedapplog/ay;-><init>(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {v0}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;)V

    .line 120
    new-instance v0, Lcom/bytedance/embedapplog/bd;

    invoke-direct {v0}, Lcom/bytedance/embedapplog/bd;-><init>()V

    invoke-static {v0}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/ae;)V
    .locals 4

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Lcom/bytedance/embedapplog/aw$a;

    const-string v1, "bd_embed_tea_agent.db"

    const/4 v2, 0x0

    const/16 v3, 0x1d

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/bytedance/embedapplog/aw$a;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v0, p0, Lcom/bytedance/embedapplog/aw;->i:Lcom/bytedance/embedapplog/aw$a;

    .line 101
    iput-object p2, p0, Lcom/bytedance/embedapplog/aw;->h:Lcom/bytedance/embedapplog/af;

    .line 102
    iput-object p3, p0, Lcom/bytedance/embedapplog/aw;->g:Lcom/bytedance/embedapplog/ae;

    return-void
.end method

.method private a([Lcom/bytedance/embedapplog/av;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lorg/json/JSONArray;)I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    add-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    .line 279
    aput-object v2, p5, v0

    move v0, v1

    goto :goto_0

    :cond_0
    const/16 p2, 0xc8

    :cond_1
    :goto_1
    if-lez p2, :cond_2

    .line 283
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 284
    aget-object v1, p1, v0

    invoke-direct {p0, p3, v1, p4, p2}, Lcom/bytedance/embedapplog/aw;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/bytedance/embedapplog/av;Ljava/lang/String;I)Lorg/json/JSONArray;

    move-result-object v1

    aput-object v1, p5, v0

    .line 286
    aget-object v1, p5, v0

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1

    sub-int/2addr p2, v1

    if-lez p2, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return v0
.end method

.method private a(JI)Ljava/lang/String;
    .locals 2

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UPDATE pack SET _fail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " WHERE "

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "local_time_ms"

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "="

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/bytedance/embedapplog/av;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT * FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/av;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " WHERE "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "session_id"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "=\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' ORDER BY "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "local_time_ms"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " LIMIT "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/bytedance/embedapplog/av;Ljava/lang/String;J)Ljava/lang/String;
    .locals 2

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DELETE FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/av;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " WHERE "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "session_id"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "=\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' AND "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "local_time_ms"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "<="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lcom/bytedance/embedapplog/av;Ljava/lang/String;I)Lorg/json/JSONArray;
    .locals 9

    .line 295
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    .line 299
    :try_start_0
    invoke-direct {p0, p2, p3, p4}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p1, p4, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide v4, v1

    .line 300
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {p4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 301
    invoke-virtual {p2, p4}, Lcom/bytedance/embedapplog/av;->a(Landroid/database/Cursor;)Lcom/bytedance/embedapplog/av;

    .line 302
    sget-boolean v6, Lcom/bytedance/embedapplog/bm;->b:Z

    if-eqz v6, :cond_1

    .line 303
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "queryEvnetInner, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v3}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 305
    :cond_1
    invoke-virtual {p2}, Lcom/bytedance/embedapplog/av;->f()Lorg/json/JSONObject;

    move-result-object v6

    invoke-virtual {v0, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 307
    iget-wide v6, p2, Lcom/bytedance/embedapplog/av;->a:J

    cmp-long v8, v6, v4

    if-lez v8, :cond_0

    .line 308
    iget-wide v6, p2, Lcom/bytedance/embedapplog/av;->a:J

    move-wide v4, v6

    goto :goto_0

    :cond_2
    cmp-long v6, v4, v1

    if-lez v6, :cond_3

    .line 313
    invoke-direct {p0, p2, p3, v4, v5}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/av;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    if-eqz p4, :cond_4

    .line 319
    :goto_1
    invoke-interface {p4}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :catch_0
    move-exception p1

    goto :goto_2

    :catchall_0
    move-exception p1

    move-object p4, v3

    goto :goto_4

    :catch_1
    move-exception p1

    move-wide v4, v1

    move-object p4, v3

    .line 316
    :goto_2
    :try_start_2
    invoke-static {p1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz p4, :cond_4

    goto :goto_1

    .line 323
    :cond_4
    :goto_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "queryEvent, "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ", "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ", "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v3}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0

    :catchall_1
    move-exception p1

    :goto_4
    if-eqz p4, :cond_5

    .line 319
    invoke-interface {p4}, Landroid/database/Cursor;->close()V

    :cond_5
    throw p1
.end method

.method private a(Lcom/bytedance/embedapplog/ba;Lcom/bytedance/embedapplog/be;Lcom/bytedance/embedapplog/bc;Landroid/database/sqlite/SQLiteDatabase;)Lorg/json/JSONArray;
    .locals 10

    const/4 v0, 0x1

    .line 330
    new-array v1, v0, [Ljava/lang/String;

    iget-object v2, p1, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 331
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    :try_start_0
    const-string v7, "SELECT * FROM page WHERE session_id=? LIMIT 500"

    .line 335
    invoke-virtual {p4, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    :goto_0
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 338
    invoke-virtual {p3, v7}, Lcom/bytedance/embedapplog/bc;->a(Landroid/database/Cursor;)Lcom/bytedance/embedapplog/av;

    .line 339
    sget-boolean v3, Lcom/bytedance/embedapplog/bm;->b:Z

    if-eqz v3, :cond_0

    .line 340
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "queryPageInner, "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v8, ", "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 342
    :cond_0
    invoke-virtual {p3}, Lcom/bytedance/embedapplog/bc;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 344
    invoke-virtual {p3}, Lcom/bytedance/embedapplog/bc;->f()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 346
    :cond_1
    iget-wide v8, p3, Lcom/bytedance/embedapplog/bc;->h:J

    const/4 v3, 0x0

    add-long/2addr v5, v8

    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_3

    const-string p3, "DELETE FROM page WHERE session_id=?"

    .line 352
    invoke-virtual {p4, p3, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    if-eqz v7, :cond_4

    .line 358
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_4

    :catch_0
    move-exception p3

    goto :goto_3

    :catchall_0
    move-exception p1

    move-object v7, v4

    goto :goto_6

    :catch_1
    move-exception p3

    move-object v7, v4

    .line 355
    :goto_3
    :try_start_2
    invoke-static {p3}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v7, :cond_4

    goto :goto_2

    .line 362
    :cond_4
    :goto_4
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result p3

    if-lez p3, :cond_6

    const-wide/16 p3, 0x3e8

    cmp-long v0, v5, p3

    if-lez v0, :cond_5

    goto :goto_5

    :cond_5
    move-wide v5, p3

    .line 365
    :goto_5
    iput-wide v5, p2, Lcom/bytedance/embedapplog/be;->h:J

    .line 366
    iget-object v0, p1, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    iput-object v0, p2, Lcom/bytedance/embedapplog/be;->c:Ljava/lang/String;

    .line 367
    iget-wide v0, p1, Lcom/bytedance/embedapplog/ba;->a:J

    iput-wide v0, p2, Lcom/bytedance/embedapplog/be;->a:J

    .line 368
    iget-wide v0, p1, Lcom/bytedance/embedapplog/ba;->a:J

    add-long/2addr v0, v5

    div-long/2addr v0, p3

    iput-wide v0, p2, Lcom/bytedance/embedapplog/be;->i:J

    .line 369
    iget-object p3, p0, Lcom/bytedance/embedapplog/aw;->g:Lcom/bytedance/embedapplog/ae;

    invoke-static {p3}, Lcom/bytedance/embedapplog/w;->a(Lcom/bytedance/embedapplog/ae;)J

    move-result-wide p3

    iput-wide p3, p2, Lcom/bytedance/embedapplog/be;->b:J

    .line 370
    iget-object p3, p1, Lcom/bytedance/embedapplog/ba;->e:Ljava/lang/String;

    iput-object p3, p2, Lcom/bytedance/embedapplog/be;->e:Ljava/lang/String;

    .line 371
    iget-object p1, p1, Lcom/bytedance/embedapplog/ba;->f:Ljava/lang/String;

    iput-object p1, p2, Lcom/bytedance/embedapplog/be;->f:Ljava/lang/String;

    .line 373
    :cond_6
    sget-boolean p1, Lcom/bytedance/embedapplog/bm;->b:Z

    if-eqz p1, :cond_7

    .line 374
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "queryPage, "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ", "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v4}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_7
    return-object v2

    :catchall_1
    move-exception p1

    :goto_6
    if-eqz v7, :cond_8

    .line 358
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_8
    throw p1
.end method

.method private static a(Lcom/bytedance/embedapplog/av;)V
    .locals 2

    .line 108
    sget-object v0, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/bytedance/embedapplog/av;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(Lcom/bytedance/embedapplog/bb;Ljava/util/HashMap;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/embedapplog/bb;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    return-void
.end method

.method private a(Lcom/bytedance/embedapplog/ba;Z)Z
    .locals 0

    .line 176
    iget-boolean p1, p1, Lcom/bytedance/embedapplog/ba;->k:Z

    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/bytedance/embedapplog/bb;",
            ">;"
        }
    .end annotation

    .line 381
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 382
    sget-object v1, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v2, "pack"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/embedapplog/bb;

    const/4 v2, 0x0

    .line 386
    :try_start_0
    iget-object v3, p0, Lcom/bytedance/embedapplog/aw;->i:Lcom/bytedance/embedapplog/aw$a;

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/aw$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "SELECT * FROM pack ORDER BY local_time_ms DESC,_full DESC LIMIT 2"

    .line 387
    invoke-virtual {v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    :goto_0
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 389
    invoke-virtual {v1}, Lcom/bytedance/embedapplog/bb;->g()Lcom/bytedance/embedapplog/av;

    move-result-object v1

    check-cast v1, Lcom/bytedance/embedapplog/bb;

    .line 390
    invoke-virtual {v1, v3}, Lcom/bytedance/embedapplog/bb;->a(Landroid/database/Cursor;)Lcom/bytedance/embedapplog/av;

    .line 391
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :cond_0
    if-eqz v3, :cond_1

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v3, v2

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v3, v2

    .line 394
    :goto_1
    :try_start_2
    invoke-static {v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v3, :cond_1

    .line 397
    :goto_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 400
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "queryPack, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0

    :catchall_1
    move-exception v0

    :goto_3
    if-eqz v3, :cond_2

    .line 397
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 7
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/bytedance/embedapplog/av;",
            ">;)V"
        }
    .end annotation

    .line 531
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "save, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 534
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/aw;->i:Lcom/bytedance/embedapplog/aw$a;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/aw$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 537
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v3, v1

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/bytedance/embedapplog/av;

    .line 538
    invoke-virtual {v4}, Lcom/bytedance/embedapplog/av;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3}, Lcom/bytedance/embedapplog/av;->b(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v0, v5, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 540
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_1

    .line 547
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 542
    :goto_1
    :try_start_3
    sget v2, Lcom/bytedance/embedapplog/aw;->b:I

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    add-int/2addr v2, p1

    sput v2, Lcom/bytedance/embedapplog/aw;->b:I

    .line 543
    invoke-static {v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v0, :cond_1

    .line 547
    :try_start_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception p1

    .line 550
    invoke-static {p1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_1
    :goto_2
    return-void

    :catchall_1
    move-exception p1

    :goto_3
    if-eqz v0, :cond_2

    .line 547
    :try_start_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_4

    :catch_3
    move-exception v0

    .line 550
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 551
    :cond_2
    :goto_4
    throw p1
.end method

.method public a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/bytedance/embedapplog/bb;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/bytedance/embedapplog/bb;",
            ">;)V"
        }
    .end annotation

    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setResult, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 407
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 409
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 411
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 413
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 415
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz p1, :cond_1

    .line 416
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_1

    const/4 v8, 0x0

    .line 417
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v8, v9, :cond_1

    .line 418
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/bytedance/embedapplog/bb;

    iget v9, v9, Lcom/bytedance/embedapplog/bb;->i:I

    if-nez v9, :cond_0

    .line 420
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/bytedance/embedapplog/bb;

    invoke-direct {p0, v9, v0, v6}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/bb;Ljava/util/HashMap;Z)V

    goto :goto_1

    .line 423
    :cond_0
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/bytedance/embedapplog/bb;

    invoke-direct {p0, v9, v4, v7}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/bb;Ljava/util/HashMap;Z)V

    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 427
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 428
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 429
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/bytedance/embedapplog/bb;

    .line 430
    iget v8, v4, Lcom/bytedance/embedapplog/bb;->i:I

    if-nez v8, :cond_3

    .line 432
    iget v8, v4, Lcom/bytedance/embedapplog/bb;->j:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    if-nez v8, :cond_2

    .line 434
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 435
    iget v9, v4, Lcom/bytedance/embedapplog/bb;->j:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v9, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    :cond_2
    invoke-direct {p0, v4, v8, v7}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/bb;Ljava/util/HashMap;Z)V

    goto :goto_2

    .line 438
    :cond_3
    iget v8, v4, Lcom/bytedance/embedapplog/bb;->i:I

    add-int/2addr v8, v6

    int-to-long v8, v8

    const-wide/16 v10, 0x5

    cmp-long v12, v8, v10

    if-lez v12, :cond_4

    .line 440
    invoke-direct {p0, v4, v3, v7}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/bb;Ljava/util/HashMap;Z)V

    .line 441
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 445
    :cond_4
    invoke-direct {p0, v4, v5, v7}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/bb;Ljava/util/HashMap;Z)V

    goto :goto_2

    .line 450
    :cond_5
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/aw;->i:Lcom/bytedance/embedapplog/aw$a;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/aw$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 451
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 452
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/embedapplog/bb;

    const-string v2, "DELETE FROM pack WHERE local_time_ms=?"

    .line 453
    new-array v3, v6, [Ljava/lang/String;

    iget-wide v4, v1, Lcom/bytedance/embedapplog/bb;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 455
    :cond_6
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/bytedance/embedapplog/bb;

    .line 456
    iget-wide v1, p2, Lcom/bytedance/embedapplog/bb;->a:J

    iget v3, p2, Lcom/bytedance/embedapplog/bb;->i:I

    add-int/2addr v3, v6

    iput v3, p2, Lcom/bytedance/embedapplog/bb;->i:I

    invoke-direct {p0, v1, v2, v3}, Lcom/bytedance/embedapplog/aw;->a(JI)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_4

    .line 458
    :cond_7
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_8

    .line 464
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_6

    :catchall_0
    move-exception p1

    goto :goto_7

    :catch_0
    move-exception p1

    move-object v1, v0

    goto :goto_5

    :catchall_1
    move-exception p1

    move-object v0, v1

    goto :goto_7

    :catch_1
    move-exception p1

    .line 460
    :goto_5
    :try_start_3
    invoke-static {p1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_8

    .line 464
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_6

    :catch_2
    move-exception p1

    .line 467
    invoke-static {p1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_8
    :goto_6
    return-void

    :goto_7
    if-eqz v0, :cond_9

    .line 464
    :try_start_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_8

    :catch_3
    move-exception p2

    .line 467
    invoke-static {p2}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 468
    :cond_9
    :goto_8
    throw p1
.end method

.method public a(Lorg/json/JSONObject;)Z
    .locals 46

    move-object/from16 v7, p0

    .line 180
    sget-object v0, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v1, "launch"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/embedapplog/ba;

    .line 181
    sget-object v1, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v2, "terminate"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/bytedance/embedapplog/be;

    .line 182
    sget-object v1, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v2, "page"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/bytedance/embedapplog/bc;

    .line 183
    sget-object v1, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v2, "eventv3"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/bytedance/embedapplog/az;

    .line 184
    sget-object v1, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v2, "event"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/bytedance/embedapplog/ax;

    .line 185
    sget-object v1, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v2, "event_misc"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/bytedance/embedapplog/ay;

    .line 186
    sget-object v1, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v2, "pack"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/bytedance/embedapplog/bb;

    .line 187
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const/4 v12, 0x0

    const/16 v31, 0x0

    .line 193
    :try_start_0
    iget-object v2, v7, Lcom/bytedance/embedapplog/aw;->i:Lcom/bytedance/embedapplog/aw$a;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/aw$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_7

    .line 194
    :try_start_1
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const-string v2, "SELECT * FROM launch ORDER BY local_time_ms DESC LIMIT 5"

    .line 195
    invoke-virtual {v11, v2, v12}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    const-wide/high16 v32, -0x8000000000000000L

    const-wide v34, 0x7fffffffffffffffL

    move-object/from16 v5, p1

    move-object v10, v1

    move-wide/from16 v3, v32

    move-wide/from16 v1, v34

    .line 198
    :goto_0
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    const/4 v12, 0x1

    if-eqz v6, :cond_b

    .line 199
    invoke-virtual {v0, v9}, Lcom/bytedance/embedapplog/ba;->a(Landroid/database/Cursor;)Lcom/bytedance/embedapplog/av;

    .line 201
    iget-object v6, v0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    invoke-static {}, Lcom/bytedance/embedapplog/p;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v12, 0x0

    goto :goto_0

    .line 205
    :cond_0
    iget-object v6, v0, Lcom/bytedance/embedapplog/ba;->i:Ljava/lang/String;

    iget-object v8, v7, Lcom/bytedance/embedapplog/aw;->h:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v8}, Lcom/bytedance/embedapplog/af;->d()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    if-eqz v6, :cond_2

    :try_start_3
    iget v6, v0, Lcom/bytedance/embedapplog/ba;->h:I

    iget-object v8, v7, Lcom/bytedance/embedapplog/aw;->h:Lcom/bytedance/embedapplog/af;

    .line 206
    invoke-virtual {v8}, Lcom/bytedance/embedapplog/af;->c()I

    move-result v8
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eq v6, v8, :cond_1

    goto :goto_1

    :cond_1
    move-object/from16 v38, v5

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v0

    move-object/from16 v37, v9

    move-object v13, v11

    goto/16 :goto_11

    :catch_0
    move-exception v0

    move-object v12, v9

    move-object v13, v11

    goto/16 :goto_f

    .line 208
    :cond_2
    :goto_1
    :try_start_4
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 209
    invoke-static {v6, v5}, Lcom/bytedance/embedapplog/bn;->b(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    const-string v5, "app_version"

    .line 210
    iget-object v8, v0, Lcom/bytedance/embedapplog/ba;->i:Ljava/lang/String;

    invoke-virtual {v6, v5, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "version_code"

    .line 211
    iget v8, v0, Lcom/bytedance/embedapplog/ba;->h:I

    invoke-virtual {v6, v5, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-object/from16 v38, v6

    .line 214
    :goto_2
    iget-wide v5, v0, Lcom/bytedance/embedapplog/ba;->a:J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    cmp-long v8, v5, v1

    if-gez v8, :cond_3

    .line 215
    :try_start_5
    iget-wide v1, v0, Lcom/bytedance/embedapplog/ba;->a:J
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    move-wide/from16 v39, v1

    .line 217
    :try_start_6
    iget-wide v1, v0, Lcom/bytedance/embedapplog/ba;->a:J
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    cmp-long v5, v1, v3

    if-lez v5, :cond_4

    .line 218
    :try_start_7
    iget-wide v1, v0, Lcom/bytedance/embedapplog/ba;->a:J
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-wide/from16 v41, v1

    goto :goto_3

    :cond_4
    move-wide/from16 v41, v3

    .line 221
    :goto_3
    :try_start_8
    invoke-direct {v7, v0, v15, v14, v11}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/ba;Lcom/bytedance/embedapplog/be;Lcom/bytedance/embedapplog/bc;Landroid/database/sqlite/SQLiteDatabase;)Lorg/json/JSONArray;

    move-result-object v21

    const/4 v1, 0x3

    .line 223
    new-array v8, v1, [Lcom/bytedance/embedapplog/av;

    aput-object v19, v8, v31

    aput-object v18, v8, v12

    const/4 v2, 0x2

    aput-object v20, v8, v2

    .line 224
    new-array v6, v1, [Lorg/json/JSONArray;

    const/4 v3, 0x0

    .line 225
    iget-object v5, v0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    move-object/from16 v1, p0

    move-object v2, v8

    move-object v4, v11

    move-object/from16 v43, v6

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/embedapplog/aw;->a([Lcom/bytedance/embedapplog/av;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lorg/json/JSONArray;)I

    move-result v1

    .line 227
    invoke-virtual/range {v21 .. v21}, Lorg/json/JSONArray;->length()I

    move-result v2
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    if-lez v2, :cond_5

    .line 228
    :try_start_9
    iget-wide v2, v0, Lcom/bytedance/embedapplog/ba;->a:J

    const/4 v4, 0x0

    aget-object v5, v43, v31

    aget-object v16, v43, v12

    const/4 v6, 0x2

    aget-object v17, v43, v6
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    move-object v6, v8

    move-object v8, v13

    move-object/from16 v37, v9

    move-object/from16 v44, v10

    move-wide v9, v2

    move-object v3, v11

    move-object/from16 v11, v38

    const/4 v2, 0x1

    move-object v12, v4

    move-object v4, v13

    move-object v13, v15

    move-object/from16 v36, v14

    move-object/from16 v14, v21

    move-object/from16 v45, v15

    move-object v15, v5

    :try_start_a
    invoke-virtual/range {v8 .. v17}, Lcom/bytedance/embedapplog/bb;->a(JLorg/json/JSONObject;Lcom/bytedance/embedapplog/ba;Lcom/bytedance/embedapplog/be;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    const/4 v5, 0x2

    goto :goto_6

    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object/from16 v37, v9

    move-object v3, v11

    :goto_4
    move-object v1, v0

    move-object v13, v3

    goto/16 :goto_11

    :catch_1
    move-exception v0

    move-object/from16 v37, v9

    move-object v3, v11

    :goto_5
    move-object v13, v3

    goto/16 :goto_e

    :cond_5
    move-object v6, v8

    move-object/from16 v37, v9

    move-object/from16 v44, v10

    move-object v3, v11

    move-object v4, v13

    move-object/from16 v36, v14

    move-object/from16 v45, v15

    const/4 v2, 0x1

    .line 230
    :try_start_b
    iput-boolean v2, v0, Lcom/bytedance/embedapplog/ba;->j:Z

    .line 231
    iget-wide v9, v0, Lcom/bytedance/embedapplog/ba;->a:J

    const/4 v13, 0x0

    const/4 v14, 0x0

    aget-object v15, v43, v31

    aget-object v16, v43, v2

    const/4 v5, 0x2

    aget-object v17, v43, v5

    move-object v8, v4

    move-object/from16 v11, v38

    move-object v12, v0

    invoke-virtual/range {v8 .. v17}, Lcom/bytedance/embedapplog/bb;->a(JLorg/json/JSONObject;Lcom/bytedance/embedapplog/ba;Lcom/bytedance/embedapplog/be;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;)V

    .line 233
    :goto_6
    sget-boolean v8, Lcom/bytedance/embedapplog/bm;->b:Z
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    if-eqz v8, :cond_7

    .line 234
    :try_start_c
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "packer launch, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {v21 .. v21}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-gtz v9, :cond_6

    const/4 v9, 0x1

    goto :goto_7

    :cond_6
    const/4 v9, 0x0

    :goto_7
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v9, ", sid:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, v0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_8

    :cond_7
    const/4 v9, 0x0

    :goto_8
    :try_start_d
    const-string v8, "pack"

    move-object/from16 v10, v44

    .line 237
    invoke-virtual {v4, v10}, Lcom/bytedance/embedapplog/bb;->b(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v10

    invoke-virtual {v3, v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :goto_9
    move v8, v1

    .line 239
    array-length v1, v6

    if-ge v8, v1, :cond_a

    .line 240
    iget-object v11, v0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    move-object/from16 v1, p0

    const/4 v12, 0x1

    move-object v2, v6

    move-object v13, v3

    move v3, v8

    move-object v8, v4

    move-object v4, v13

    const/4 v14, 0x2

    move-object v5, v11

    move-object v11, v6

    move-object/from16 v6, v43

    :try_start_e
    invoke-direct/range {v1 .. v6}, Lcom/bytedance/embedapplog/aw;->a([Lcom/bytedance/embedapplog/av;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lorg/json/JSONArray;)I

    move-result v1

    .line 241
    aget-object v2, v43, v31

    if-nez v2, :cond_9

    aget-object v2, v43, v12

    if-nez v2, :cond_9

    aget-object v2, v43, v14

    if-eqz v2, :cond_8

    goto :goto_b

    :cond_8
    :goto_a
    move-object v4, v8

    move-object v6, v11

    move-object v3, v13

    const/4 v2, 0x1

    const/4 v5, 0x2

    goto :goto_9

    .line 242
    :cond_9
    :goto_b
    iget-wide v2, v0, Lcom/bytedance/embedapplog/ba;->a:J

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    aget-object v28, v43, v31

    aget-object v29, v43, v12

    aget-object v30, v43, v14

    move-object/from16 v21, v8

    move-wide/from16 v22, v2

    move-object/from16 v24, v38

    invoke-virtual/range {v21 .. v30}, Lcom/bytedance/embedapplog/bb;->a(JLorg/json/JSONObject;Lcom/bytedance/embedapplog/ba;Lcom/bytedance/embedapplog/be;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;)V

    const-string v2, "pack"

    .line 243
    invoke-virtual {v8, v10}, Lcom/bytedance/embedapplog/bb;->b(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v10

    invoke-virtual {v13, v2, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_a

    :cond_a
    move-object v11, v3

    move-object v13, v4

    move-object v12, v9

    move-object/from16 v14, v36

    move-object/from16 v9, v37

    move-object/from16 v5, v38

    move-wide/from16 v1, v39

    move-wide/from16 v3, v41

    move-object/from16 v15, v45

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    move-object v13, v3

    goto :goto_d

    :catch_2
    move-exception v0

    goto/16 :goto_5

    :cond_b
    move-object/from16 v37, v9

    move-object v13, v11

    const/4 v14, 0x2

    cmp-long v0, v1, v34

    if-eqz v0, :cond_c

    cmp-long v0, v3, v32

    if-eqz v0, :cond_c

    const-string v0, "DELETE FROM launch WHERE local_time_ms>=? AND local_time_ms<=?"

    .line 248
    new-array v5, v14, [Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v31

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v12

    invoke-virtual {v13, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 250
    :cond_c
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    if-eqz v37, :cond_d

    .line 256
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->close()V

    :cond_d
    if-eqz v13, :cond_e

    .line 260
    :try_start_f
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3

    goto :goto_c

    :catch_3
    move-exception v0

    move-object v1, v0

    .line 263
    invoke-static {v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_e
    :goto_c
    return v12

    :catchall_4
    move-exception v0

    goto :goto_d

    :catch_4
    move-exception v0

    goto :goto_e

    :catchall_5
    move-exception v0

    move-object/from16 v37, v9

    move-object v13, v11

    :goto_d
    move-object v1, v0

    goto :goto_11

    :catch_5
    move-exception v0

    move-object/from16 v37, v9

    move-object v13, v11

    :goto_e
    move-object/from16 v12, v37

    goto :goto_f

    :catchall_6
    move-exception v0

    move-object v13, v11

    move-object v9, v12

    move-object v1, v0

    move-object/from16 v37, v9

    goto :goto_11

    :catch_6
    move-exception v0

    move-object v13, v11

    move-object v9, v12

    goto :goto_f

    :catchall_7
    move-exception v0

    move-object v9, v12

    move-object v1, v0

    move-object v13, v9

    move-object/from16 v37, v13

    goto :goto_11

    :catch_7
    move-exception v0

    move-object v9, v12

    move-object v13, v12

    .line 252
    :goto_f
    :try_start_10
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_8

    if-eqz v12, :cond_f

    .line 256
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_f
    if-eqz v13, :cond_10

    .line 260
    :try_start_11
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_8

    goto :goto_10

    :catch_8
    move-exception v0

    move-object v1, v0

    .line 263
    invoke-static {v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_10
    :goto_10
    return v31

    :catchall_8
    move-exception v0

    move-object v1, v0

    move-object/from16 v37, v12

    :goto_11
    if-eqz v37, :cond_11

    .line 256
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->close()V

    :cond_11
    if-eqz v13, :cond_12

    .line 260
    :try_start_12
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_9

    goto :goto_12

    :catch_9
    move-exception v0

    move-object v2, v0

    .line 263
    invoke-static {v2}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 264
    :cond_12
    :goto_12
    throw v1
.end method

.method public a(Lorg/json/JSONObject;Lcom/bytedance/embedapplog/ba;Z)Z
    .locals 24

    move-object/from16 v7, p0

    move-object/from16 v0, p2

    move/from16 v8, p3

    .line 124
    sget-object v1, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v2, "eventv3"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/embedapplog/az;

    .line 125
    sget-object v2, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v3, "event"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/embedapplog/ax;

    .line 126
    sget-object v3, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v4, "event_misc"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/embedapplog/ay;

    .line 127
    sget-object v4, Lcom/bytedance/embedapplog/aw;->a:Ljava/util/HashMap;

    const-string v5, "pack"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v15, v4

    check-cast v15, Lcom/bytedance/embedapplog/bb;

    .line 128
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    const/4 v13, 0x0

    const/16 v19, 0x0

    .line 131
    :try_start_0
    iget-object v4, v7, Lcom/bytedance/embedapplog/aw;->i:Lcom/bytedance/embedapplog/aw$a;

    invoke-virtual {v4}, Lcom/bytedance/embedapplog/aw$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_8

    .line 132
    :try_start_1
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v4, 0x3

    .line 133
    new-array v10, v4, [Lcom/bytedance/embedapplog/av;

    aput-object v2, v10, v19

    const/4 v11, 0x1

    aput-object v1, v10, v11

    const/16 v20, 0x2

    aput-object v3, v10, v20

    .line 134
    new-array v9, v4, [Lorg/json/JSONArray;

    const/4 v3, 0x0

    .line 135
    iget-object v5, v0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    move-object/from16 v1, p0

    move-object v2, v10

    move-object v4, v12

    move-object v6, v9

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/embedapplog/aw;->a([Lcom/bytedance/embedapplog/av;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lorg/json/JSONArray;)I

    move-result v1

    .line 136
    aget-object v2, v9, v19
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_7

    if-eqz v2, :cond_0

    :try_start_2
    aget-object v2, v9, v19

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-gtz v2, :cond_3

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v0

    move-object v3, v12

    goto/16 :goto_b

    :catch_0
    move-exception v0

    move-object v13, v12

    goto/16 :goto_9

    :cond_0
    :goto_0
    :try_start_3
    aget-object v2, v9, v11
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_7

    if-eqz v2, :cond_1

    :try_start_4
    aget-object v2, v9, v11

    .line 137
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-gtz v2, :cond_3

    :cond_1
    :try_start_5
    aget-object v2, v9, v20
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_7

    if-eqz v2, :cond_2

    :try_start_6
    aget-object v2, v9, v20

    .line 138
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-gtz v2, :cond_3

    .line 139
    :cond_2
    :try_start_7
    invoke-direct {v7, v0, v8}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/ba;Z)Z

    move-result v2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    if-eqz v2, :cond_6

    .line 140
    :cond_3
    :try_start_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v7, v0, v8}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/ba;Z)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v4, v0

    goto :goto_1

    :cond_4
    move-object v4, v13

    :goto_1
    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v16, v9, v19

    aget-object v17, v9, v11

    aget-object v18, v9, v20
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-object/from16 v21, v9

    move-object v9, v15

    move-object/from16 v22, v10

    move-wide v10, v2

    move-object v3, v12

    move-object/from16 v12, p1

    move-object v2, v13

    move-object v13, v4

    move-object v4, v14

    move-object v14, v5

    move-object v5, v15

    move-object v15, v6

    :try_start_9
    invoke-virtual/range {v9 .. v18}, Lcom/bytedance/embedapplog/bb;->a(JLorg/json/JSONObject;Lcom/bytedance/embedapplog/ba;Lcom/bytedance/embedapplog/be;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;)V

    const-string v6, "pack"

    .line 142
    invoke-virtual {v5, v4}, Lcom/bytedance/embedapplog/bb;->b(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v14

    invoke-virtual {v3, v6, v2, v14}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 143
    sget-boolean v4, Lcom/bytedance/embedapplog/bm;->b:Z

    if-eqz v4, :cond_5

    invoke-direct {v7, v0, v8}, Lcom/bytedance/embedapplog/aw;->a(Lcom/bytedance/embedapplog/ba;Z)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 144
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "send launch, "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", hadUI:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    const/4 v15, 0x1

    .line 146
    iput-boolean v15, v0, Lcom/bytedance/embedapplog/ba;->k:Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move v4, v1

    move-object v13, v14

    move-object/from16 v14, v22

    goto :goto_3

    :catchall_1
    move-exception v0

    goto/16 :goto_7

    :catch_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v3, v12

    goto/16 :goto_7

    :catch_2
    move-exception v0

    move-object v3, v12

    :goto_2
    move-object v13, v3

    goto/16 :goto_9

    :cond_6
    move-object/from16 v21, v9

    move-object v3, v12

    move-object v2, v13

    move-object v4, v14

    move-object v5, v15

    const/4 v15, 0x1

    move-object v13, v4

    move-object v14, v10

    move v4, v1

    .line 149
    :goto_3
    :try_start_a
    array-length v1, v14

    if-ge v4, v1, :cond_a

    .line 150
    iget-object v6, v0, Lcom/bytedance/embedapplog/ba;->c:Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    move-object/from16 v1, p0

    move-object v12, v2

    move-object v2, v14

    move-object v11, v3

    move v3, v4

    move-object v4, v11

    move-object v9, v5

    move-object v5, v6

    move-object/from16 v6, v21

    :try_start_b
    invoke-direct/range {v1 .. v6}, Lcom/bytedance/embedapplog/aw;->a([Lcom/bytedance/embedapplog/av;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lorg/json/JSONArray;)I

    move-result v4

    .line 151
    aget-object v1, v21, v19
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    if-eqz v1, :cond_7

    :try_start_c
    aget-object v1, v21, v19

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    if-gtz v1, :cond_8

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v1, v0

    move-object v3, v11

    goto/16 :goto_b

    :catch_3
    move-exception v0

    move-object v13, v11

    goto/16 :goto_9

    :cond_7
    :goto_4
    :try_start_d
    aget-object v1, v21, v15

    if-eqz v1, :cond_9

    aget-object v1, v21, v15

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 152
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v16, v21, v19

    aget-object v17, v21, v15

    aget-object v18, v21, v20
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    move-object v8, v9

    move-object/from16 v23, v9

    move-wide v9, v1

    move-object v1, v11

    move-object/from16 v11, p1

    move-object v2, v12

    move-object v12, v3

    move-object v3, v13

    move-object v13, v5

    move-object v5, v14

    move-object v14, v6

    const/4 v6, 0x1

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    :try_start_e
    invoke-virtual/range {v8 .. v17}, Lcom/bytedance/embedapplog/bb;->a(JLorg/json/JSONObject;Lcom/bytedance/embedapplog/ba;Lcom/bytedance/embedapplog/be;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;)V

    const-string v8, "pack"

    move-object/from16 v9, v23

    .line 153
    invoke-virtual {v9, v3}, Lcom/bytedance/embedapplog/bb;->b(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v13

    invoke-virtual {v1, v8, v2, v13}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-object v3, v1

    move-object v14, v5

    move-object v5, v9

    const/4 v15, 0x1

    goto :goto_3

    :cond_9
    move-object v1, v11

    move-object v2, v12

    move-object v3, v13

    move-object v5, v14

    const/4 v6, 0x1

    move-object v13, v3

    move-object v14, v5

    move-object v5, v9

    const/4 v15, 0x1

    move-object v3, v1

    goto :goto_3

    :catchall_4
    move-exception v0

    move-object v1, v11

    goto :goto_6

    :catch_4
    move-exception v0

    move-object v1, v11

    goto :goto_8

    :cond_a
    move-object v1, v3

    const/4 v6, 0x1

    .line 156
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    if-eqz v1, :cond_b

    .line 163
    :try_start_f
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5

    goto :goto_5

    :catch_5
    move-exception v0

    move-object v1, v0

    .line 166
    invoke-static {v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_b
    :goto_5
    return v6

    :catchall_5
    move-exception v0

    goto :goto_6

    :catch_6
    move-exception v0

    goto :goto_8

    :catchall_6
    move-exception v0

    move-object v1, v3

    goto :goto_7

    :catch_7
    move-exception v0

    move-object v1, v3

    goto :goto_8

    :catchall_7
    move-exception v0

    move-object v1, v12

    :goto_6
    move-object v3, v1

    :goto_7
    move-object v1, v0

    goto :goto_b

    :catch_8
    move-exception v0

    move-object v1, v12

    :goto_8
    move-object v13, v1

    goto :goto_9

    :catchall_8
    move-exception v0

    move-object v2, v13

    move-object v1, v0

    move-object v3, v2

    goto :goto_b

    :catch_9
    move-exception v0

    move-object v2, v13

    .line 158
    :goto_9
    :try_start_10
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_9

    if-eqz v13, :cond_c

    .line 163
    :try_start_11
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_a

    goto :goto_a

    :catch_a
    move-exception v0

    move-object v1, v0

    .line 166
    invoke-static {v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_c
    :goto_a
    return v19

    :catchall_9
    move-exception v0

    move-object v1, v0

    move-object v3, v13

    :goto_b
    if-eqz v3, :cond_d

    .line 163
    :try_start_12
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_b

    goto :goto_c

    :catch_b
    move-exception v0

    move-object v2, v0

    .line 166
    invoke-static {v2}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 167
    :cond_d
    :goto_c
    throw v1
.end method
