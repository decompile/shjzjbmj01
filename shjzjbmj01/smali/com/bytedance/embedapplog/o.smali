.class Lcom/bytedance/embedapplog/o;
.super Lcom/bytedance/embedapplog/n;
.source "SourceFile"


# instance fields
.field private final b:Lcom/bytedance/embedapplog/ae;

.field private final c:Lcom/bytedance/embedapplog/af;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/ae;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/n;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object p2, p0, Lcom/bytedance/embedapplog/o;->c:Lcom/bytedance/embedapplog/af;

    .line 28
    iput-object p3, p0, Lcom/bytedance/embedapplog/o;->b:Lcom/bytedance/embedapplog/ae;

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method b()J
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/bytedance/embedapplog/o;->b:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->g()J

    move-result-wide v0

    const-wide/32 v2, 0x1499700

    add-long/2addr v0, v2

    return-wide v0
.end method

.method c()[J
    .locals 1

    .line 46
    sget-object v0, Lcom/bytedance/embedapplog/u;->c:[J

    return-object v0
.end method

.method public d()Z
    .locals 6

    .line 51
    iget-object v0, p0, Lcom/bytedance/embedapplog/o;->c:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/af;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/bytedance/embedapplog/o;->c:Lcom/bytedance/embedapplog/af;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/af;->o()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 54
    iget-object v1, p0, Lcom/bytedance/embedapplog/o;->b:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/ae;->g()J

    move-result-wide v1

    const-wide/32 v3, 0x1499700

    add-long/2addr v1, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v5, v1, v3

    if-gtz v5, :cond_0

    .line 55
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "magic_tag"

    const-string v3, "ss_app_log"

    .line 56
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "header"

    .line 57
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "_gen_time"

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 60
    iget-object v0, p0, Lcom/bytedance/embedapplog/o;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/embedapplog/o;->c:Lcom/bytedance/embedapplog/af;

    .line 61
    invoke-virtual {v2}, Lcom/bytedance/embedapplog/af;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {}, Lcom/bytedance/embedapplog/at;->a()Lcom/bytedance/embedapplog/util/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/embedapplog/util/a;->c()Ljava/lang/String;

    move-result-object v3

    .line 62
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getIAppParam()Lcom/bytedance/embedapplog/a;

    move-result-object v4

    const/4 v5, 0x1

    .line 61
    invoke-static {v0, v2, v3, v5, v4}, Lcom/bytedance/embedapplog/au;->a(Landroid/content/Context;Lorg/json/JSONObject;Ljava/lang/String;ZLcom/bytedance/embedapplog/a;)Ljava/lang/String;

    move-result-object v0

    .line 63
    sget-object v2, Lcom/bytedance/embedapplog/at;->c:[Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/bytedance/embedapplog/at;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/at;->c(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    .line 65
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getDataObserver()Lcom/bytedance/embedapplog/IDataObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/embedapplog/o;->b:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/ae;->f()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/embedapplog/bn;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)Z

    move-result v2

    xor-int/2addr v2, v5

    invoke-interface {v1, v2, v0}, Lcom/bytedance/embedapplog/IDataObserver;->onRemoteConfigGet(ZLorg/json/JSONObject;)V

    if-eqz v0, :cond_0

    .line 68
    iget-object v1, p0, Lcom/bytedance/embedapplog/o;->b:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v1, v0}, Lcom/bytedance/embedapplog/ae;->a(Lorg/json/JSONObject;)V

    return v5

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    const-string v0, "c"

    return-object v0
.end method
