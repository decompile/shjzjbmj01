.class public Lcom/bytedance/embedapplog/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/bytedance/embedapplog/e;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Landroid/content/SharedPreferences;

.field private volatile f:Lorg/json/JSONObject;

.field private volatile g:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/embedapplog/e;)V
    .locals 1

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/bytedance/embedapplog/ae;->a:Landroid/content/Context;

    .line 93
    iput-object p2, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    .line 94
    iget-object p1, p0, Lcom/bytedance/embedapplog/ae;->a:Landroid/content/Context;

    const-string p2, "embed_applog_stats"

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    .line 95
    iget-object p1, p0, Lcom/bytedance/embedapplog/ae;->a:Landroid/content/Context;

    const-string p2, "embed_header_custom"

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    .line 96
    iget-object p1, p0, Lcom/bytedance/embedapplog/ae;->a:Landroid/content/Context;

    const-string p2, "embed_last_sp_session"

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/embedapplog/ae;->d:Landroid/content/SharedPreferences;

    return-void
.end method


# virtual methods
.method A()Ljava/lang/String;
    .locals 3
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 341
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    const-string v1, "user_agent"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public B()J
    .locals 2

    const-wide/16 v0, 0x2710

    return-wide v0
.end method

.method public C()Ljava/lang/String;
    .locals 1

    .line 367
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()I
    .locals 1

    .line 371
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->n()I

    move-result v0

    return v0
.end method

.method public E()I
    .locals 1

    .line 375
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->o()I

    move-result v0

    return v0
.end method

.method public F()I
    .locals 1

    .line 379
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->p()I

    move-result v0

    return v0
.end method

.method public G()Ljava/lang/String;
    .locals 1

    .line 383
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    .line 387
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I()Ljava/lang/String;
    .locals 1

    .line 391
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public J()Ljava/lang/String;
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public K()Ljava/lang/String;
    .locals 1

    .line 399
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public L()Ljava/lang/String;
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->u()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public M()Ljava/lang/String;
    .locals 1

    .line 407
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->x()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->x()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public N()Z
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->w()Z

    move-result v0

    return v0
.end method

.method public O()Lcom/bytedance/embedapplog/e;
    .locals 1

    .line 419
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    return-object v0
.end method

.method public P()Ljava/lang/CharSequence;
    .locals 1

    .line 428
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->v()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a()Ljava/lang/String;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 0

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    .line 238
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ab_sdk_version"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "session_last_day"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "session_order"

    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public a(Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 13

    .line 134
    sget-boolean v0, Lcom/bytedance/embedapplog/bm;->b:Z

    if-eqz v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setConfig, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 138
    :cond_0
    iput-object p1, p0, Lcom/bytedance/embedapplog/ae;->g:Lorg/json/JSONObject;

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 142
    iget-object v2, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "session_interval"

    const/4 v4, 0x0

    .line 144
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    int-to-long v5, v3

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    const-wide/16 v9, 0x3e8

    const-wide/32 v11, 0x93a80

    if-lez v3, :cond_1

    cmp-long v3, v5, v11

    if-gtz v3, :cond_1

    const-string v3, "session_interval"

    mul-long v5, v5, v9

    .line 146
    invoke-interface {v2, v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_1
    const-string v3, "session_interval"

    .line 148
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_0
    const-string v3, "batch_event_interval"

    .line 151
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    int-to-long v5, v3

    cmp-long v3, v5, v7

    if-lez v3, :cond_2

    cmp-long v3, v5, v11

    if-gtz v3, :cond_2

    const-string v3, "batch_event_interval"

    mul-long v5, v5, v9

    .line 153
    invoke-interface {v2, v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :cond_2
    const-string v3, "batch_event_interval"

    .line 155
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_1
    const-string v3, "send_launch_timely"

    .line 158
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_3

    int-to-long v5, v3

    cmp-long v7, v5, v11

    if-gtz v7, :cond_3

    const-string v5, "send_launch_timely"

    .line 160
    invoke-interface {v2, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    :cond_3
    const-string v3, "send_launch_timely"

    .line 162
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_2
    const-string v3, "abtest_fetch_interval"

    .line 165
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    int-to-long v5, v3

    const-wide/16 v7, 0x14

    cmp-long v3, v5, v7

    if-lez v3, :cond_4

    cmp-long v3, v5, v11

    if-gtz v3, :cond_4

    const-string v3, "abtest_fetch_interval"

    mul-long v5, v5, v9

    .line 167
    invoke-interface {v2, v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_3

    :cond_4
    const-string v3, "abtest_fetch_interval"

    .line 169
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_3
    const-string v3, "bav_log_collect"

    const/4 v5, 0x1

    .line 172
    invoke-virtual {p1, v3, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v6, "bav_log_collect"

    .line 174
    invoke-interface {v2, v6, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_4

    :cond_5
    const-string v6, "bav_log_collect"

    .line 176
    invoke-interface {v2, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 178
    :goto_4
    sput-boolean v3, Lcom/bytedance/embedapplog/bm;->a:Z

    const-string v3, "bav_ab_config"

    .line 180
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "bav_ab_config"

    .line 182
    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_5

    :cond_6
    const-string v3, "bav_ab_config"

    .line 184
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_5
    const-string v3, "bav_monitor_rate"

    .line 187
    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    if-lez p1, :cond_7

    const/16 v3, 0x64

    if-gt p1, v3, :cond_7

    const-string v3, "bav_monitor_rate"

    .line 189
    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 190
    invoke-static {v5}, Lcom/bytedance/embedapplog/bl;->a(Z)V

    goto :goto_6

    :cond_7
    const-string p1, "bav_monitor_rate"

    .line 192
    invoke-interface {v2, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 193
    invoke-static {v4}, Lcom/bytedance/embedapplog/bl;->a(Z)V

    :goto_6
    const-string p1, "app_log_last_config_time"

    .line 196
    invoke-interface {v2, p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 198
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public a(Ljava/util/ArrayList;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/bytedance/embedapplog/av;",
            ">;)Z"
        }
    .end annotation

    const/4 p1, 0x1

    return p1
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .line 108
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->d:Landroid/content/SharedPreferences;

    const-string v1, "session_last_day"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b(Ljava/lang/String;)V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_unique_id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method b(Lorg/json/JSONObject;)V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "header_custom_info"

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public c()I
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->d:Landroid/content/SharedPreferences;

    const-string v1, "session_order"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 1

    .line 300
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/ae;->t()Lorg/json/JSONObject;

    move-result-object v0

    .line 301
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    return-object p1
.end method

.method c(Lorg/json/JSONObject;)V
    .locals 3

    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setAbConfig, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 268
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "ab_configure"

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 269
    iput-object v1, p0, Lcom/bytedance/embedapplog/ae;->f:Lorg/json/JSONObject;

    return-void
.end method

.method public d()Landroid/content/SharedPreferences;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method d(Ljava/lang/String;)V
    .locals 2

    .line 345
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_agent"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ab_version"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public e()Z
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->b()Z

    move-result v0

    return v0
.end method

.method public f()Lorg/json/JSONObject;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->g:Lorg/json/JSONObject;

    return-object v0
.end method

.method public g()J
    .locals 4

    .line 202
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    const-string v1, "app_log_last_config_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public h()I
    .locals 3

    .line 206
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    const-string v1, "bav_monitor_rate"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method j()Ljava/lang/String;
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method k()Ljava/lang/String;
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method l()Ljava/lang/String;
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method m()Ljava/lang/String;
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method n()Ljava/lang/String;
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    const-string v1, "header_custom_info"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method o()Ljava/lang/String;
    .locals 3

    .line 243
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    const-string v1, "ab_sdk_version"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method p()Ljava/lang/String;
    .locals 3

    .line 251
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    const-string v1, "user_unique_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public q()Z
    .locals 4

    .line 255
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->i()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    iget-object v2, p0, Lcom/bytedance/embedapplog/ae;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/bytedance/embedapplog/bn;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/2addr v2, v1

    invoke-virtual {v0, v2}, Lcom/bytedance/embedapplog/e;->a(Z)Lcom/bytedance/embedapplog/e;

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->i()I

    move-result v0

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public r()J
    .locals 4

    .line 262
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    const-string v1, "abtest_fetch_interval"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()Ljava/lang/String;
    .locals 3

    .line 273
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    const-string v1, "ab_version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public t()Lorg/json/JSONObject;
    .locals 5

    .line 280
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->f:Lorg/json/JSONObject;

    if-nez v0, :cond_2

    .line 282
    monitor-enter p0

    .line 284
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/ae;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/bytedance/embedapplog/ae;->c:Landroid/content/SharedPreferences;

    const-string v3, "ab_configure"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 291
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 293
    :cond_1
    iput-object v0, p0, Lcom/bytedance/embedapplog/ae;->f:Lorg/json/JSONObject;

    .line 294
    monitor-exit p0

    goto :goto_2

    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    :goto_2
    return-object v0
.end method

.method public u()Z
    .locals 3

    .line 305
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    const-string v1, "bav_ab_config"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public v()Z
    .locals 3

    .line 309
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    const-string v1, "bav_log_collect"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public w()J
    .locals 4

    .line 318
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    const-string v1, "session_interval"

    const-wide/16 v2, 0x7530

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public x()J
    .locals 4

    .line 322
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->e:Landroid/content/SharedPreferences;

    const-string v1, "batch_event_interval"

    const-wide/16 v2, 0x7530

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method y()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method z()Ljava/lang/String;
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/bytedance/embedapplog/ae;->b:Lcom/bytedance/embedapplog/e;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/e;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
