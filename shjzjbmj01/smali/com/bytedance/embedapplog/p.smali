.class public Lcom/bytedance/embedapplog/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Handler$Callback;",
        "Ljava/util/Comparator<",
        "Lcom/bytedance/embedapplog/av;",
        ">;"
    }
.end annotation


# static fields
.field private static b:Lcom/bytedance/embedapplog/p;


# instance fields
.field public a:Landroid/app/Application;

.field private c:Lcom/bytedance/embedapplog/l;

.field private d:Z

.field private e:Lcom/bytedance/embedapplog/ae;

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/bytedance/embedapplog/av;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/bytedance/embedapplog/aw;

.field private h:Lcom/bytedance/embedapplog/af;

.field private i:Landroid/os/Handler;

.field private j:Lcom/bytedance/embedapplog/w;

.field private k:Landroid/os/Handler;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    return-void
.end method

.method public static a()V
    .locals 2

    .line 70
    sget-object v0, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    if-eqz v0, :cond_0

    .line 71
    sget-object v0, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/bytedance/embedapplog/p;->b([Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(Lcom/bytedance/embedapplog/av;)V
    .locals 7

    .line 315
    sget-object v0, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "Init comes First!"

    .line 317
    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 318
    invoke-static {p0}, Lcom/bytedance/embedapplog/y;->a(Lcom/bytedance/embedapplog/av;)V

    return-void

    .line 321
    :cond_0
    iget-wide v2, p0, Lcom/bytedance/embedapplog/av;->a:J

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    .line 322
    invoke-static {v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    .line 325
    :cond_1
    instance-of v1, p0, Lcom/bytedance/embedapplog/bd;

    if-eqz v1, :cond_2

    .line 327
    move-object v1, p0

    check-cast v1, Lcom/bytedance/embedapplog/bd;

    iget-object v2, v0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/ae;->h()I

    move-result v2

    iput v2, v1, Lcom/bytedance/embedapplog/bd;->i:I

    .line 330
    :cond_2
    iget-object v1, v0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    monitor-enter v1

    .line 331
    :try_start_0
    iget-object v2, v0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 332
    iget-object v3, v0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    rem-int/lit8 p0, v2, 0xa

    if-nez p0, :cond_4

    iget-object p0, v0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    if-eqz p0, :cond_4

    .line 335
    iget-object p0, v0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 336
    iget-object p0, v0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    if-nez v2, :cond_3

    const-wide/16 v2, 0x1f4

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0xfa

    :goto_0
    invoke-virtual {p0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_4
    return-void

    :catchall_0
    move-exception p0

    .line 333
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method

.method public static a([Ljava/lang/String;)V
    .locals 3

    .line 351
    sget-object v0, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    if-nez v0, :cond_0

    .line 353
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "Init comes First!"

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    return-void

    .line 356
    :cond_0
    iget-object v1, v0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 357
    iget-object v1, v0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 358
    iget-object v0, v0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {p0}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    return-void
.end method

.method public static b()Lcom/bytedance/embedapplog/p;
    .locals 2

    .line 80
    sget-object v0, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    if-nez v0, :cond_1

    .line 81
    const-class v0, Lcom/bytedance/embedapplog/p;

    monitor-enter v0

    .line 82
    :try_start_0
    sget-object v1, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    if-nez v1, :cond_0

    .line 84
    new-instance v1, Lcom/bytedance/embedapplog/p;

    invoke-direct {v1}, Lcom/bytedance/embedapplog/p;-><init>()V

    .line 85
    sput-object v1, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    .line 87
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 89
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    return-object v0
.end method

.method private b([Ljava/lang/String;)V
    .locals 6

    .line 224
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    monitor-enter v0

    .line 225
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 226
    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 227
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 230
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    array-length v3, p1

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 231
    array-length v2, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, p1, v3

    .line 232
    invoke-static {v4}, Lcom/bytedance/embedapplog/av;->a(Ljava/lang/String;)Lcom/bytedance/embedapplog/av;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 236
    :cond_0
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {p1, v1}, Lcom/bytedance/embedapplog/ae;->a(Ljava/util/ArrayList;)Z

    move-result p1

    .line 238
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_b

    .line 239
    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/ae;->q()Z

    move-result v2

    if-eqz v2, :cond_8

    if-nez p1, :cond_2

    .line 240
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/16 v2, 0x64

    if-le p1, v2, :cond_1

    goto :goto_1

    .line 272
    :cond_1
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    monitor-enter p1

    .line 273
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 274
    monitor-exit p1

    goto/16 :goto_5

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 241
    :cond_2
    :goto_1
    invoke-static {v1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 242
    new-instance p1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 245
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/embedapplog/av;

    .line 246
    iget-object v4, p0, Lcom/bytedance/embedapplog/p;->j:Lcom/bytedance/embedapplog/w;

    invoke-virtual {v4, v3, p1}, Lcom/bytedance/embedapplog/w;->a(Lcom/bytedance/embedapplog/av;Ljava/util/ArrayList;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 248
    invoke-direct {p0}, Lcom/bytedance/embedapplog/p;->g()V

    .line 250
    :cond_4
    instance-of v4, v3, Lcom/bytedance/embedapplog/bc;

    if-eqz v4, :cond_3

    .line 252
    invoke-static {v3}, Lcom/bytedance/embedapplog/w;->a(Lcom/bytedance/embedapplog/av;)Z

    move-result v0

    const/4 v2, 0x1

    move v2, v0

    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_7

    const/4 v0, 0x7

    if-eqz v2, :cond_6

    .line 258
    iget-object v1, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_3

    .line 261
    :cond_6
    iget-object v1, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v2}, Lcom/bytedance/embedapplog/ae;->w()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 265
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->g:Lcom/bytedance/embedapplog/aw;

    invoke-virtual {v0, p1}, Lcom/bytedance/embedapplog/aw;->a(Ljava/util/ArrayList;)V

    .line 267
    iget-boolean p1, p0, Lcom/bytedance/embedapplog/p;->d:Z

    if-nez p1, :cond_b

    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->j:Lcom/bytedance/embedapplog/w;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/w;->b()Z

    move-result p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    if-eqz p1, :cond_b

    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getAutoActiveState()Z

    move-result p1

    if-eqz p1, :cond_b

    .line 268
    invoke-virtual {p0}, Lcom/bytedance/embedapplog/p;->e()Z

    goto :goto_5

    .line 277
    :cond_8
    new-instance p1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    const-class v3, Lcom/bytedance/embedapplog/collector/Collector;

    invoke-direct {p1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 278
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 279
    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    :goto_4
    if-ge v0, v2, :cond_9

    .line 282
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/bytedance/embedapplog/av;

    invoke-virtual {v5}, Lcom/bytedance/embedapplog/av;->e()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v0

    .line 283
    aget-object v5, v3, v0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    const v0, 0x4b000

    if-lt v4, v0, :cond_a

    const/4 v0, 0x0

    .line 287
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_a
    const-string v0, "EMBED_K_DATA"

    .line 289
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    :try_start_2
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    invoke-virtual {v0, p1}, Landroid/app/Application;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5

    :catch_0
    move-exception p1

    .line 293
    invoke-static {p1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    :cond_b
    :goto_5
    return-void

    :catchall_1
    move-exception p1

    .line 227
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    .line 93
    invoke-static {}, Lcom/bytedance/embedapplog/p;->d()Lcom/bytedance/embedapplog/w;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/embedapplog/w;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static d()Lcom/bytedance/embedapplog/w;
    .locals 1

    .line 97
    sget-object v0, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    if-eqz v0, :cond_0

    .line 98
    sget-object v0, Lcom/bytedance/embedapplog/p;->b:Lcom/bytedance/embedapplog/p;

    iget-object v0, v0, Lcom/bytedance/embedapplog/p;->j:Lcom/bytedance/embedapplog/w;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    .line 100
    invoke-static {v0}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private f()V
    .locals 4

    .line 201
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/ae;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->c:Lcom/bytedance/embedapplog/l;

    if-nez v0, :cond_1

    .line 203
    new-instance v0, Lcom/bytedance/embedapplog/l;

    iget-object v1, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    iget-object v3, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-direct {v0, v1, v2, v3}, Lcom/bytedance/embedapplog/l;-><init>(Landroid/app/Application;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/ae;)V

    iput-object v0, p0, Lcom/bytedance/embedapplog/p;->c:Lcom/bytedance/embedapplog/l;

    .line 204
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->c:Lcom/bytedance/embedapplog/l;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->c:Lcom/bytedance/embedapplog/l;

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->c:Lcom/bytedance/embedapplog/l;

    invoke-virtual {v0}, Lcom/bytedance/embedapplog/l;->f()V

    const/4 v0, 0x0

    .line 208
    iput-object v0, p0, Lcom/bytedance/embedapplog/p;->c:Lcom/bytedance/embedapplog/l;

    :cond_1
    :goto_0
    return-void
.end method

.method private g()V
    .locals 8

    .line 300
    sget-boolean v0, Lcom/bytedance/embedapplog/bm;->b:Z

    if-eqz v0, :cond_0

    .line 301
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packAndSend once, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/embedapplog/p;->j:Lcom/bytedance/embedapplog/w;

    iget-object v1, v1, Lcom/bytedance/embedapplog/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", hadUI:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/embedapplog/p;->j:Lcom/bytedance/embedapplog/w;

    invoke-virtual {v1}, Lcom/bytedance/embedapplog/w;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    new-instance v2, Lcom/bytedance/embedapplog/q;

    iget-object v3, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    iget-object v4, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    iget-object v5, p0, Lcom/bytedance/embedapplog/p;->g:Lcom/bytedance/embedapplog/aw;

    invoke-direct {v2, v3, v4, v5}, Lcom/bytedance/embedapplog/q;-><init>(Landroid/content/Context;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/aw;)V

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 305
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    new-instance v2, Lcom/bytedance/embedapplog/s;

    iget-object v4, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    iget-object v5, p0, Lcom/bytedance/embedapplog/p;->g:Lcom/bytedance/embedapplog/aw;

    iget-object v6, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    iget-object v7, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/bytedance/embedapplog/s;-><init>(Landroid/content/Context;Lcom/bytedance/embedapplog/aw;Lcom/bytedance/embedapplog/ae;Lcom/bytedance/embedapplog/af;)V

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/embedapplog/av;Lcom/bytedance/embedapplog/av;)I
    .locals 3

    .line 364
    iget-wide v0, p1, Lcom/bytedance/embedapplog/av;->a:J

    iget-wide p1, p2, Lcom/bytedance/embedapplog/av;->a:J

    sub-long/2addr v0, p1

    const-wide/16 p1, 0x0

    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    cmp-long v2, v0, p1

    if-lez v2, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public a(Landroid/app/Application;Lcom/bytedance/embedapplog/ae;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/k;)V
    .locals 1

    .line 107
    iput-object p1, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    .line 108
    new-instance v0, Lcom/bytedance/embedapplog/aw;

    invoke-direct {v0, p1, p3, p2}, Lcom/bytedance/embedapplog/aw;-><init>(Landroid/app/Application;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/ae;)V

    iput-object v0, p0, Lcom/bytedance/embedapplog/p;->g:Lcom/bytedance/embedapplog/aw;

    .line 109
    iput-object p2, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    .line 110
    iput-object p3, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    .line 111
    new-instance p1, Lcom/bytedance/embedapplog/w;

    iget-object p3, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-direct {p1, p3, v0}, Lcom/bytedance/embedapplog/w;-><init>(Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/ae;)V

    iput-object p1, p0, Lcom/bytedance/embedapplog/p;->j:Lcom/bytedance/embedapplog/w;

    .line 113
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    invoke-virtual {p1, p4}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 115
    new-instance p1, Landroid/os/HandlerThread;

    const-string p3, "bd_tracker_w"

    invoke-direct {p1, p3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    .line 117
    new-instance p3, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {p3, p1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object p3, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    .line 118
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    const/4 p3, 0x1

    invoke-virtual {p1, p3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 120
    invoke-virtual {p2}, Lcom/bytedance/embedapplog/ae;->h()I

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-static {p3}, Lcom/bytedance/embedapplog/bl;->a(Z)V

    return-void
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 33
    check-cast p1, Lcom/bytedance/embedapplog/av;

    check-cast p2, Lcom/bytedance/embedapplog/av;

    invoke-virtual {p0, p1, p2}, Lcom/bytedance/embedapplog/p;->a(Lcom/bytedance/embedapplog/av;Lcom/bytedance/embedapplog/av;)I

    move-result p1

    return p1
.end method

.method public e()Z
    .locals 4

    const/4 v0, 0x1

    .line 213
    iput-boolean v0, p0, Lcom/bytedance/embedapplog/p;->d:Z

    .line 214
    new-instance v1, Lcom/bytedance/embedapplog/m;

    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    iget-object v3, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    invoke-direct {v1, v2, v3}, Lcom/bytedance/embedapplog/m;-><init>(Landroid/content/Context;Lcom/bytedance/embedapplog/af;)V

    .line 215
    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 216
    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 9

    .line 125
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x6

    const-wide/32 v2, 0x337f9800

    const/4 v4, 0x1

    const/4 v5, 0x4

    const/4 v6, 0x0

    packed-switch v0, :pswitch_data_0

    .line 195
    :pswitch_0
    invoke-static {v6}, Lcom/bytedance/embedapplog/bm;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 188
    :pswitch_1
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    monitor-enter v0

    .line 189
    :try_start_0
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    invoke-static {}, Lcom/bytedance/embedapplog/w;->d()Lcom/bytedance/embedapplog/w$a;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    invoke-direct {p0, v6}, Lcom/bytedance/embedapplog/p;->b([Ljava/lang/String;)V

    goto/16 :goto_2

    :catchall_0
    move-exception p1

    .line 190
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 168
    :pswitch_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/bytedance/embedapplog/n;

    .line 169
    invoke-virtual {p1}, Lcom/bytedance/embedapplog/n;->g()Z

    move-result v0

    if-nez v0, :cond_6

    .line 170
    invoke-virtual {p1}, Lcom/bytedance/embedapplog/n;->h()J

    move-result-wide v5

    cmp-long v0, v5, v2

    if-gez v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    iget-object v2, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    invoke-virtual {v2, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v0, p1, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 175
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/embedapplog/p;->f()V

    goto/16 :goto_2

    .line 184
    :pswitch_3
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/bytedance/embedapplog/p;->b([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 180
    :pswitch_4
    invoke-direct {p0, v6}, Lcom/bytedance/embedapplog/p;->b([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 151
    :pswitch_5
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 152
    new-instance v0, Lcom/bytedance/embedapplog/u;

    iget-object v5, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    iget-object v6, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    iget-object v7, p0, Lcom/bytedance/embedapplog/p;->j:Lcom/bytedance/embedapplog/w;

    invoke-direct {v0, v5, v6, v7}, Lcom/bytedance/embedapplog/u;-><init>(Landroid/content/Context;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/w;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v0, Lcom/bytedance/embedapplog/o;

    iget-object v5, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    iget-object v6, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    iget-object v7, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-direct {v0, v5, v6, v7}, Lcom/bytedance/embedapplog/o;-><init>(Landroid/content/Context;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/ae;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    new-instance v0, Lcom/bytedance/embedapplog/t;

    iget-object v5, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    iget-object v6, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    iget-object v7, p0, Lcom/bytedance/embedapplog/p;->g:Lcom/bytedance/embedapplog/aw;

    invoke-direct {v0, v5, v6, v7}, Lcom/bytedance/embedapplog/t;-><init>(Landroid/content/Context;Lcom/bytedance/embedapplog/af;Lcom/bytedance/embedapplog/aw;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v0, Lcom/bytedance/embedapplog/v;

    iget-object v5, p0, Lcom/bytedance/embedapplog/p;->a:Landroid/app/Application;

    iget-object v6, p0, Lcom/bytedance/embedapplog/p;->g:Lcom/bytedance/embedapplog/aw;

    iget-object v7, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    iget-object v8, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    invoke-direct {v0, v5, v6, v7, v8}, Lcom/bytedance/embedapplog/v;-><init>(Landroid/content/Context;Lcom/bytedance/embedapplog/aw;Lcom/bytedance/embedapplog/ae;Lcom/bytedance/embedapplog/af;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/embedapplog/n;

    .line 158
    invoke-virtual {v0}, Lcom/bytedance/embedapplog/n;->h()J

    move-result-wide v5

    cmp-long v7, v5, v2

    if-gez v7, :cond_1

    .line 160
    iget-object v7, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    iget-object v8, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    invoke-virtual {v8, v1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 164
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/embedapplog/p;->f()V

    goto :goto_2

    .line 127
    :pswitch_6
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/ae;->v()Z

    move-result p1

    sput-boolean p1, Lcom/bytedance/embedapplog/bm;->a:Z

    .line 128
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->h:Lcom/bytedance/embedapplog/af;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/af;->e()Z

    move-result p1

    const-wide/16 v0, 0x3e8

    if-eqz p1, :cond_4

    .line 129
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->e:Lcom/bytedance/embedapplog/ae;

    invoke-virtual {p1}, Lcom/bytedance/embedapplog/ae;->q()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 130
    new-instance p1, Landroid/os/HandlerThread;

    const-string v2, "bd_tracker_n"

    invoke-direct {p1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    .line 132
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {v2, p1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v2, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    .line 134
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->i:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 136
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->f:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_3

    .line 137
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    invoke-virtual {p1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 138
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    invoke-virtual {p1, v5, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_3
    const-string p1, "net|worker start"

    .line 140
    invoke-static {p1, v6}, Lcom/bytedance/embedapplog/bm;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 143
    :cond_4
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    invoke-virtual {p1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 144
    iget-object p1, p0, Lcom/bytedance/embedapplog/p;->k:Landroid/os/Handler;

    invoke-virtual {p1, v4, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 147
    :cond_5
    :goto_1
    invoke-static {}, Lcom/bytedance/embedapplog/y;->a()V

    :cond_6
    :goto_2
    return v4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
