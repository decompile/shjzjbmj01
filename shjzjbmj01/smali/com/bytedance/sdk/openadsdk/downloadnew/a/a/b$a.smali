.class Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;
.super Lcom/bytedance/sdk/openadsdk/l/g;
.source "LibEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/ss/android/a/a/c/d;

.field private b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

.field private c:Lcom/bytedance/sdk/openadsdk/core/d/l;


# direct methods
.method private constructor <init>(Lcom/ss/android/a/a/c/d;)V
    .locals 1

    const-string v0, "LogTask"

    .line 76
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/l/g;-><init>(Ljava/lang/String;)V

    .line 77
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 79
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-virtual {p1}, Lcom/ss/android/a/a/c/d;->d()Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 80
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-virtual {p1}, Lcom/ss/android/a/a/c/d;->d()Lorg/json/JSONObject;

    move-result-object p1

    const-string v0, "ad_extra_data"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 82
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "open_ad_sdk_download_extra"

    .line 85
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 86
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    .line 87
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    .line 89
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    if-eqz p1, :cond_0

    .line 90
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->a:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private a()Landroid/content/Context;
    .locals 1

    .line 101
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/ss/android/a/a/c/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;
    .locals 1

    .line 72
    new-instance v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;-><init>(Lcom/ss/android/a/a/c/d;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .line 108
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "embeded_ad"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 109
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "draw_ad"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 110
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "draw_ad_landingpage"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 111
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "banner_ad"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 112
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "banner_call"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 113
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "banner_ad_landingpage"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 114
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "feed_call"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 115
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "embeded_ad_landingpage"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 116
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "interaction"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 117
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "interaction_call"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 118
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "interaction_landingpage"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 119
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "slide_banner_ad"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 120
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "splash_ad"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 121
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "fullscreen_interstitial_ad"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 122
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "splash_ad_landingpage"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 123
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "rewarded_video"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 124
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "rewarded_video_landingpage"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 125
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "openad_sdk_download_complete_tag"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 126
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "download_notification"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 127
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "landing_h5_download_ad_button"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 128
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "fullscreen_interstitial_ad_landingpage"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 129
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "feed_video_middle_page"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 130
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "stream"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 131
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    if-nez v0, :cond_0

    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LibEventLogger"

    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tag "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "LibEventLogger"

    .line 143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "label "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-virtual {v3}, Lcom/ss/android/a/a/c/d;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 145
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b:Ljava/lang/String;

    .line 149
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 152
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-virtual {v2}, Lcom/ss/android/a/a/c/d;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {v0, v2, v3, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 156
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 157
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    .line 158
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b;->c(Lcom/ss/android/a/a/c/d;)Lorg/json/JSONObject;

    move-result-object v0

    .line 163
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->b:Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/a;->b:Ljava/lang/String;

    .line 164
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-virtual {v2}, Lcom/ss/android/a/a/c/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 165
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-virtual {v2}, Lcom/ss/android/a/a/c/d;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "click"

    .line 167
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    return-void

    .line 170
    :cond_4
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/a/b$a;->a:Lcom/ss/android/a/a/c/d;

    invoke-virtual {v4}, Lcom/ss/android/a/a/c/d;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v1, v4, v0}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :cond_5
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "LibEventLogger"

    const-string v2, "upload event log error"

    .line 174
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_6
    :goto_1
    return-void
.end method
