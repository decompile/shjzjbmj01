.class public Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;
.super Ljava/lang/Object;
.source "DMLibManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;
.implements Lcom/bytedance/sdk/openadsdk/utils/am$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;,
        Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;
    }
.end annotation


# instance fields
.field private A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

.field private B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/ITTAppDownloadListener;",
            ">;"
        }
    .end annotation
.end field

.field private C:Z

.field private D:Z

.field protected a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field protected final b:Lcom/bytedance/sdk/openadsdk/core/d/b;

.field protected final c:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field protected d:Ljava/lang/String;

.field protected final e:Ljava/util/concurrent/atomic/AtomicInteger;

.field protected final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected g:Z

.field protected h:Z

.field protected i:Z

.field protected j:Lcom/bytedance/sdk/openadsdk/IListenerManager;

.field private k:I

.field private l:Lcom/ss/android/a/a/b/a;

.field private m:Lcom/ss/android/a/a/b/b;

.field private n:Lcom/ss/android/a/a/b/c;

.field private o:Z

.field private final p:Ljava/util/concurrent/atomic/AtomicLong;

.field private final q:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private r:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z

.field private t:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

.field private final v:Lcom/bytedance/sdk/openadsdk/utils/am;

.field private w:Z

.field private x:Ljava/lang/String;

.field private y:I

.field private final z:Lcom/ss/android/a/a/b/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)V
    .locals 4

    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 97
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->k:I

    .line 102
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 105
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 106
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g:Z

    .line 110
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->p:Ljava/util/concurrent/atomic/AtomicLong;

    .line 111
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 113
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->s:Z

    .line 117
    new-instance v0, Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3, p0}, Lcom/bytedance/sdk/openadsdk/utils/am;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/openadsdk/utils/am$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->v:Lcom/bytedance/sdk/openadsdk/utils/am;

    .line 118
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w:Z

    .line 120
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    .line 121
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->i:Z

    .line 127
    new-instance v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$1;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z:Lcom/ss/android/a/a/b/d;

    .line 255
    new-instance v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    .line 1144
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->B:Ljava/util/List;

    .line 1172
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->C:Z

    .line 1173
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Z

    .line 285
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    .line 286
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 287
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ah()Lcom/bytedance/sdk/openadsdk/core/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    .line 288
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    .line 289
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/ak;->c(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->k:I

    .line 290
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->hashCode()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aF()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 291
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x:Ljava/lang/String;

    .line 293
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "====tag==="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 295
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    if-nez p2, :cond_0

    const-string p1, "DMLibManager"

    const-string p2, "download create error: not a App type Ad!"

    .line 296
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 300
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p2

    if-nez p2, :cond_1

    .line 301
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/p;->a(Landroid/content/Context;)V

    .line 304
    :cond_1
    new-instance p1, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    invoke-direct {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    .line 305
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;Lorg/json/JSONObject;)Lcom/ss/android/b/a/a/c$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/b/a/a/c$a;->a()Lcom/ss/android/b/a/a/c;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n:Lcom/ss/android/a/a/b/c;

    .line 306
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Lcom/ss/android/b/a/a/a$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/b/a/a/a$a;->a()Lcom/ss/android/b/a/a/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l:Lcom/ss/android/a/a/b/a;

    .line 307
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/b/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/b/a/a/b$a;->a()Lcom/ss/android/b/a/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m:Lcom/ss/android/a/a/b/b;

    .line 308
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    return-object p0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;)V
    .locals 8

    if-nez p1, :cond_0

    return-void

    .line 973
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->Q()Ljava/lang/String;

    move-result-object v1

    .line 975
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/f;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v3

    const-string v0, ""

    .line 977
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->U()Lcom/bytedance/sdk/openadsdk/core/d/k;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 979
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/k;->a()Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v4, v0

    .line 981
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->T()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 982
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v6, "pop_up"

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->v()Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {v0, p1, v2, v6, v7}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 983
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$4;

    invoke-direct {v2, p0, p2, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$4;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    invoke-static/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/utils/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/utils/g$a;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Lcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 77
    invoke-direct/range {p0 .. p7}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 11

    move-object v9, p0

    .line 259
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    if-nez v0, :cond_0

    .line 260
    new-instance v10, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V

    iput-object v10, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    goto :goto_0

    .line 262
    :cond_0
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-object v1, p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->a(Ljava/lang/String;)V

    .line 263
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-wide v1, p2

    invoke-virtual {v0, p2, p3}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->a(J)V

    .line 264
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-wide v1, p4

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->b(J)V

    .line 265
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->b(Ljava/lang/String;)V

    .line 266
    iget-object v0, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    move-object/from16 v1, p7

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;->c(Ljava/lang/String;)V

    .line 268
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->c()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iget-object v1, v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->A:Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$a;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(I)Z
    .locals 3

    .line 826
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->e()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    .line 850
    :pswitch_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/i;->b(I)Z

    move-result p1

    if-nez p1, :cond_1

    const/high16 p1, 0x6400000

    .line 852
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/b;->g()I

    move-result v0

    if-lez v0, :cond_0

    .line 853
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->g()I

    move-result p1

    goto :goto_1

    :goto_0
    :pswitch_1
    const/4 v1, 0x1

    goto :goto_2

    :pswitch_2
    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    goto :goto_0

    .line 835
    :pswitch_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/i;->b(I)Z

    move-result p1

    xor-int/lit8 v1, p1, 0x1

    goto :goto_2

    .line 855
    :cond_0
    :goto_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->f()I

    move-result v0

    if-le p1, v0, :cond_1

    goto :goto_0

    :cond_1
    :goto_2
    :pswitch_4
    return v1

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Z)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 641
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aw()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p4, :cond_1

    .line 642
    invoke-static {p2, p3}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)Z

    .line 645
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    .line 646
    new-instance p3, Landroid/content/Intent;

    const-string p4, "android.intent.action.VIEW"

    invoke-direct {p3, p4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 647
    invoke-virtual {p3, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 p2, 0x10000000

    .line 648
    invoke-virtual {p3, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string p2, "open_url"

    .line 649
    invoke-virtual {p3, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 650
    invoke-virtual {p0, p3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :catch_0
    return v0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 9

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 915
    :cond_0
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->i:Z

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 918
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_2

    .line 921
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v4

    const-string v5, "tt_no_network"

    invoke-static {v4, v5}, Lcom/bytedance/sdk/openadsdk/utils/ad;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    invoke-static {v3, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 926
    :catch_0
    :cond_2
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(I)Z

    move-result v1

    .line 927
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->J()I

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    .line 928
    :goto_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->K()I

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    .line 929
    :goto_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->K()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    const/4 v5, 0x1

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    .line 930
    :goto_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->L()I

    move-result v7

    if-nez v7, :cond_6

    const/4 v7, 0x1

    goto :goto_3

    :cond_6
    const/4 v7, 0x0

    .line 932
    :goto_3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 933
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_7

    return v2

    :cond_7
    if-eqz v5, :cond_8

    return v2

    .line 939
    :cond_8
    iput v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->y:I

    if-eqz v4, :cond_9

    return v1

    :cond_9
    return v0

    .line 944
    :cond_a
    iget-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v4, :cond_c

    if-eqz v7, :cond_b

    return v2

    :cond_b
    const/4 p1, 0x3

    .line 948
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->y:I

    return v0

    .line 951
    :cond_c
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->y:I

    if-eqz v3, :cond_d

    return v1

    .line 954
    :cond_d
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->J()I

    move-result p1

    if-ne p1, v6, :cond_f

    .line 956
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->C:Z

    if-eqz p1, :cond_f

    .line 957
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Z

    if-eqz p1, :cond_e

    return v0

    :cond_e
    return v1

    :cond_f
    return v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/ss/android/a/a/b/c;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n:Lcom/ss/android/a/a/b/c;

    return-object p0
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
    .locals 3

    const-string v0, "DMLibManager"

    .line 1147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "registerMultiProcessListener, mMetaMd5:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 1151
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->c()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$5;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic b(Ljava/lang/String;)V
    .locals 0

    .line 77
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/ss/android/a/a/b/b;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m:Lcom/ss/android/a/a/b/b;

    return-object p0
.end method

.method private static c(Ljava/lang/String;)V
    .locals 1

    const-string v0, "DMLibManager"

    .line 281
    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lcom/ss/android/a/a/b/a;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l:Lcom/ss/android/a/a/b/a;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Lorg/json/JSONObject;
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->v()Lorg/json/JSONObject;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Ljava/lang/String;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)Ljava/util/List;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->B:Ljava/util/List;

    return-object p0
.end method

.method private h(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 1267
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v2, "quickapp_success"

    invoke-static {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1269
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v2, "quickapp_fail"

    invoke-static {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private o()V
    .locals 0

    return-void
.end method

.method private p()V
    .locals 2

    const-string v0, "tryReleaseResource=="

    .line 389
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 391
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const-string v0, "tryReleaseResource==  mContext is null"

    .line 392
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    return-void

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 396
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    const-string v0, "tryReleaseResource==  activity is null"

    .line 399
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    return-void

    .line 402
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/i;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "tryReleaseResource==  isActivityAlive is true"

    .line 403
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    return-void

    .line 406
    :cond_3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->y()V

    return-void
.end method

.method private declared-synchronized q()V
    .locals 3

    monitor-enter p0

    .line 419
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unbindDownload=="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 420
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 421
    monitor-exit p0

    return-void

    .line 423
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 425
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n:Lcom/ss/android/a/a/b/c;

    invoke-interface {v1}, Lcom/ss/android/a/a/b/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;I)V

    .line 427
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->p()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 428
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 418
    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized r()V
    .locals 5

    monitor-enter p0

    .line 431
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bindDownload=="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 433
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 434
    monitor-exit p0

    return-void

    .line 437
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 442
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 443
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z:Lcom/ss/android/a/a/b/d;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n:Lcom/ss/android/a/a/b/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ss/android/downloadlib/g;->a(Landroid/content/Context;ILcom/ss/android/a/a/b/d;Lcom/ss/android/a/a/b/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 430
    monitor-exit p0

    throw v0
.end method

.method private s()V
    .locals 10

    .line 467
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 472
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i;->e()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 473
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUseWriteExternal()Z

    move-result v0

    if-nez v0, :cond_1

    .line 475
    :try_start_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a:Ljava/lang/String;

    .line 476
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_1

    .line 477
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    return-void

    .line 485
    :catch_0
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->E()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "changeDownloadStatus, not support pause/continue function"

    .line 486
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 489
    :try_start_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    const-string v1, "\u5e94\u7528\u6b63\u5728\u4e0b\u8f7d..."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void

    .line 496
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 497
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 498
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$2;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$b;)V

    goto :goto_0

    .line 505
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/b;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n:Lcom/ss/android/a/a/b/c;

    invoke-interface {v0}, Lcom/ss/android/a/a/b/c;->d()J

    move-result-wide v4

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m:Lcom/ss/android/a/a/b/b;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l:Lcom/ss/android/a/a/b/a;

    invoke-virtual/range {v2 .. v8}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;JILcom/ss/android/a/a/b/b;Lcom/ss/android/a/a/b/a;)V

    :goto_0
    return-void

    .line 509
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "changeDownloadStatus, the current status is1: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    .line 510
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/b;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n:Lcom/ss/android/a/a/b/c;

    invoke-interface {v0}, Lcom/ss/android/a/a/b/c;->d()J

    move-result-wide v3

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m:Lcom/ss/android/a/a/b/b;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->l:Lcom/ss/android/a/a/b/a;

    const/4 v8, 0x0

    new-instance v9, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$3;

    invoke-direct {v9, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$3;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    invoke-virtual/range {v1 .. v9}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;JILcom/ss/android/a/a/b/b;Lcom/ss/android/a/a/b/a;Lcom/ss/android/a/a/a/s;Lcom/ss/android/a/a/a/n;)V

    .line 525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "changeDownloadStatus, the current status is2: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c(Ljava/lang/String;)V

    return-void

    :cond_5
    :goto_1
    return-void
.end method

.method private t()Z
    .locals 4

    .line 531
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->T()I

    move-result v0

    .line 535
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->au()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 536
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private u()Z
    .locals 6

    .line 611
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 615
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/b;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-static {v0, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 617
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0x9

    .line 618
    iput v2, v1, Landroid/os/Message;->what:I

    .line 619
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->v:Lcom/bytedance/sdk/openadsdk/utils/am;

    const-wide/16 v3, 0xbb8

    invoke-virtual {v2, v1, v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/am;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 621
    :cond_1
    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h(Z)V

    :goto_0
    return v0

    :cond_2
    :goto_1
    return v1
.end method

.method private v()Lorg/json/JSONObject;
    .locals 3

    .line 1006
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "download_type"

    .line 1008
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->y:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 1010
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private w()V
    .locals 2

    .line 1020
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x()V

    .line 1021
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method private x()V
    .locals 0

    .line 1038
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e()V

    return-void
.end method

.method private y()V
    .locals 3

    const-string v0, "DMLibManager"

    .line 1183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "unregisterMultiProcessListener, mMetaMd5:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1187
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->c()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$6;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d$6;-><init>(Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private z()V
    .locals 1

    .line 1274
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/b;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1275
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->i()Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1280
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 1283
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    if-nez p1, :cond_1

    const-string p1, ""

    return-object p1

    .line 1287
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p1, ""

    return-object p1

    .line 1290
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 0

    .line 313
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    .line 315
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->o()V

    return-void
.end method

.method public a(ILcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;)V
    .locals 2

    .line 1220
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 1221
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/HashSet;

    .line 1224
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1226
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(ILcom/bytedance/sdk/openadsdk/downloadnew/core/a$a;)V

    return-void
.end method

.method public a(J)V
    .locals 1

    .line 453
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->p:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "DMLibManager"

    .line 372
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setActivity==activity:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 377
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    .line 378
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 1

    .line 1231
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0x9

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1233
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/i;->a()Z

    move-result p1

    if-nez p1, :cond_3

    const/4 p1, 0x0

    .line 1234
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h(Z)V

    .line 1235
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w:Z

    if-eqz p1, :cond_2

    .line 1237
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w:Z

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Z)Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 1240
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z()V

    :cond_2
    return-void

    :cond_3
    const/4 p1, 0x1

    .line 1244
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h(Z)V

    :goto_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 384
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
    .locals 1

    const/4 v0, 0x1

    .line 1124
    invoke-virtual {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;Z)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;Z)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 1132
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    if-eqz v0, :cond_1

    .line 1133
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    :cond_1
    if-eqz p2, :cond_2

    .line 1136
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    .line 1141
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9

    const/4 v0, 0x0

    if-eqz p1, :cond_9

    .line 1069
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    :cond_0
    const-string v1, "DMLibManager"

    .line 1072
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v1, :cond_1

    const-string v1, "DMLibManager"

    .line 1074
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77\uff0c \u5f00\u59cb\u4e0a\u62a5 lp_open_dpl packageName "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v4, "lp_open_dpl"

    invoke-static {v1, v2, v3, v4, p2}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    :cond_1
    :try_start_0
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/ak;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    .line 1080
    :try_start_1
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/ak;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    if-nez p2, :cond_2

    return v0

    .line 1085
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n()V

    const-string v2, "START_ONLY_FOR_ANDROID"

    .line 1086
    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1087
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1088
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_3

    const-string p1, "DMLibManager"

    const-string p2, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77\uff0c\u5f00\u59cb\u8c03\u8d77\uff0c\u4e0a\u62a5 lp_openurl "

    .line 1089
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v3, "lp_openurl"

    invoke-static {p1, p2, v2, v3}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    :cond_3
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_4

    .line 1093
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/k;->a()Lcom/bytedance/sdk/openadsdk/c/k;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-virtual {p1, p2, v2, v1}, Lcom/bytedance/sdk/openadsdk/c/k;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :cond_4
    return v1

    .line 1097
    :catch_0
    :try_start_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->W()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 1098
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->W()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Ljava/lang/String;)I

    move-result v5

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;ILjava/lang/String;ZLjava/util/Map;)Z

    .line 1100
    :cond_5
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_6

    const-string p1, "DMLibManager"

    const-string p2, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77\uff0c\u5f00\u59cb\u8c03\u8d77\uff0c\u8c03\u8d77\u5f02\u5e38\uff0c\u4e0a\u62a5 lp_openurl_failed "

    .line 1101
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1102
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v3, "lp_openurl_failed"

    invoke-static {p1, p2, v2, v3}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    return v1

    .line 1107
    :cond_7
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz p1, :cond_8

    const-string p1, "DMLibManager"

    const-string p2, "\u4f7f\u7528\u5305\u540d\u8c03\u8d77\uff0c\u8be5app\u672a\u5b89\u88c5 \uff0c\u4e0a\u62a5 lp_openurl_failed "

    .line 1108
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v2, "lp_openurl_failed"

    invoke-static {p1, p2, v1, v2}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    :cond_8
    return v0

    :cond_9
    :goto_0
    return v0
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 1

    const/4 v0, 0x1

    .line 1119
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p2, p3, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public a(Z)Z
    .locals 0

    .line 772
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w:Z

    .line 773
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u()Z

    move-result p1

    return p1
.end method

.method public b()V
    .locals 1

    .line 333
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 334
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/p;->a(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x1

    .line 336
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->o:Z

    .line 338
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    return-void
.end method

.method public b(J)V
    .locals 1

    .line 547
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    if-nez p1, :cond_0

    return-void

    .line 550
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 551
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->d()Lcom/ss/android/downloadlib/g;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n:Lcom/ss/android/a/a/b/c;

    invoke-interface {p2}, Lcom/ss/android/a/a/b/c;->a()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Lcom/ss/android/downloadlib/g;->a(Ljava/lang/String;Z)V

    .line 552
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->r()V

    return-void
.end method

.method public b(Z)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    .line 344
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->o:Z

    return-void
.end method

.method public c(Z)V
    .locals 0

    .line 415
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->s:Z

    return-void
.end method

.method public d()V
    .locals 2

    .line 350
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u:Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/c;->a()V

    .line 353
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->q()V

    .line 355
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/HashSet;

    if-eqz v0, :cond_1

    .line 356
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->t:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 357
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 358
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 359
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(I)V

    .line 360
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 365
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    const/4 v0, 0x0

    .line 366
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    :cond_2
    return-void
.end method

.method public d(Z)V
    .locals 0

    .line 1175
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->C:Z

    return-void
.end method

.method public e()V
    .locals 0

    .line 463
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->s()V

    return-void
.end method

.method public e(Z)V
    .locals 0

    .line 1179
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->D:Z

    return-void
.end method

.method public f()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 542
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(J)V

    return-void
.end method

.method public f(Z)V
    .locals 0

    .line 1250
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    return-void
.end method

.method public g()V
    .locals 2

    .line 557
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "feed_video_middle_page"

    .line 562
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 563
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    .line 567
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->k()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 568
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void

    .line 571
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    return-void

    .line 576
    :cond_3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 577
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void

    .line 582
    :cond_4
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w:Z

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 583
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void

    .line 587
    :cond_5
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->z()V

    return-void

    :cond_6
    :goto_0
    return-void
.end method

.method public g(Z)V
    .locals 0

    .line 1262
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->i:Z

    return-void
.end method

.method public h()Z
    .locals 2

    .line 597
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->O()Lcom/bytedance/sdk/openadsdk/core/d/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->O()Lcom/bytedance/sdk/openadsdk/core/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->b()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/b;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public i()Z
    .locals 5

    .line 779
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_2

    .line 781
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 784
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v3

    const-string v4, "tt_no_network"

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/ad;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 793
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 794
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n()V

    .line 796
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->w()V

    goto :goto_2

    .line 801
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 802
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n()V

    .line 805
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e()V

    .line 807
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    goto :goto_0

    .line 809
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v3, 0x6

    if-ne v0, v3, :cond_6

    .line 810
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_1

    .line 808
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_6
    :goto_1
    const/4 v1, 0x0

    :catch_0
    :goto_2
    return v1
.end method

.method public j()Z
    .locals 6

    .line 1043
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1046
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/b;->d()Ljava/lang/String;

    move-result-object v0

    .line 1047
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1049
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1052
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v2, "click_open"

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0, v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1053
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->h(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v0

    .line 1054
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v0, v5}, Lcom/bytedance/sdk/openadsdk/c/d;->j(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    return v1

    :cond_2
    return v1
.end method

.method public k()Z
    .locals 9

    .line 694
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ai()Lcom/bytedance/sdk/openadsdk/core/d/g;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_a

    .line 695
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ai()Lcom/bytedance/sdk/openadsdk/core/d/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/g;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "DMLibManager"

    .line 696
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u542b\u6709deeplink\u94fe\u63a5 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "deepLink"

    .line 697
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DMLibManager \u542b\u6709deeplink\u94fe\u63a5\u5c1d\u8bd5deeplink\u8c03\u8d77 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_8

    .line 699
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 700
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 701
    invoke-virtual {v5, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 703
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v2, :cond_0

    const-string v2, "DMLibManager"

    .line 704
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\u542b\u6709deeplink\u94fe\u63a5\uff0c\u5f00\u59cb\u4e0a\u62a5 lp_open_dpl schema "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v8, "lp_open_dpl"

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v6, v7, v8, v0}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "DMLibManager"

    const-string v2, "\u542b\u6709deeplink\u94fe\u63a5\uff0c \u8be5app\u5df2\u5b89\u88c5 "

    .line 709
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    const/high16 v0, 0x10000000

    .line 711
    invoke-virtual {v5, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 715
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->n()V

    .line 716
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v2, "open_url_app"

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0, v0, v2, v6}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 717
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v7, "open_url_app"

    invoke-static {v0, v2, v6, v7, v3}, Lcom/bytedance/sdk/openadsdk/c/d;->i(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 719
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 721
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/c/k;->a()Lcom/bytedance/sdk/openadsdk/c/k;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    invoke-virtual {v0, v2, v3, v5}, Lcom/bytedance/sdk/openadsdk/c/k;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Z)V

    .line 722
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v0, :cond_3

    const-string v0, "DMLibManager"

    const-string v2, "\u542b\u6709deeplink\u94fe\u63a5\uff0c \u8be5app\u5df2\u5b89\u88c5\uff0c\u8fdb\u884c\u5f00\u59cb\u8c03\u8d77\u4e0a\u62a5 lp_openurl "

    .line 723
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v5, "lp_openurl"

    invoke-static {v0, v2, v3, v5}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v5, "lp_deeplink_success_realtime"

    invoke-static {v0, v2, v3, v5}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 727
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v5, "deeplink_success_realtime"

    invoke-static {v0, v2, v3, v5}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v4

    .line 731
    :catch_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->W()Ljava/lang/String;

    move-result-object v0

    .line 732
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 733
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->W()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Ljava/lang/String;)I

    move-result v5

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/core/z;->a(Landroid/content/Context;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;ILjava/lang/String;ZLjava/util/Map;)Z

    .line 735
    :cond_4
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v0, :cond_5

    const-string v0, "DMLibManager"

    const-string v2, "\u542b\u6709deeplink\u94fe\u63a5\uff0c \u8be5app\u5df2\u5b89\u88c5\uff0c\u8c03\u8d77\u5931\u8d25 \u4e0a\u62a5lp_openurl_failed "

    .line 736
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v4, "lp_openurl_failed"

    invoke-static {v0, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v4, "lp_deeplink_fail_realtime"

    invoke-static {v0, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 740
    :cond_5
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v4, "deeplink_fail_realtime"

    invoke-static {v0, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return v1

    .line 746
    :cond_6
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-eqz v0, :cond_7

    const-string v0, "DMLibManager"

    const-string v2, "\u542b\u6709deeplink\u94fe\u63a5\uff0c \u8be5app\u672a\u5b89\u88c5\uff0c\u4e0a\u62a5lp_openurl_failed "

    .line 747
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v6, "lp_openurl_failed"

    invoke-static {v0, v2, v5, v6}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v6, "lp_deeplink_fail_realtime"

    invoke-static {v0, v2, v5, v6}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 751
    :cond_7
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v6, "deeplink_fail_realtime"

    invoke-static {v0, v2, v5, v6}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_a

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_a

    .line 757
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 758
    :cond_9
    iput-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->g:Z

    .line 760
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v2, "open_fallback_url"

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0, v0, v2, v4}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 761
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->m()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    const-string v5, "open_fallback_url"

    invoke-static {v0, v2, v4, v5, v3}, Lcom/bytedance/sdk/openadsdk/c/d;->i(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_a
    return v1
.end method

.method protected l()Lcom/bytedance/sdk/openadsdk/IListenerManager;
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->j:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    if-nez v0, :cond_0

    .line 273
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;

    move-result-object v0

    const/4 v1, 0x3

    .line 274
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a;->a(I)Landroid/os/IBinder;

    move-result-object v0

    .line 275
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/multipro/aidl/a/b;->asInterface(Landroid/os/IBinder;)Lcom/bytedance/sdk/openadsdk/IListenerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->j:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->j:Lcom/bytedance/sdk/openadsdk/IListenerManager;

    return-object v0
.end method

.method protected m()Landroid/content/Context;
    .locals 1

    .line 688
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method protected n()V
    .locals 2

    .line 1027
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-nez v0, :cond_0

    return-void

    .line 1030
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aw()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1031
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 1032
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1033
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/downloadnew/a/d;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)Z

    :cond_1
    return-void
.end method
