.class Lcom/bytedance/sdk/openadsdk/l/a;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "ADThreadPoolExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/l/a$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;",
            "Ljava/util/concurrent/ThreadFactory;",
            "Ljava/util/concurrent/RejectedExecutionHandler;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move v1, p2

    move v2, p3

    move-wide v3, p4

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    .line 47
    invoke-direct/range {v0 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    move-object v1, p1

    .line 48
    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    .locals 7

    .line 111
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V

    .line 112
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 113
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 116
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, 0x1a344

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eq v1, v2, :cond_1

    const v2, 0x2daeb0

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "aidl"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "log"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :cond_2
    :goto_0
    const/4 p2, 0x5

    const/4 v1, 0x3

    const/4 v2, 0x6

    const/4 v5, 0x2

    const/4 v6, 0x4

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 143
    :pswitch_0
    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result p1

    if-lt p1, v6, :cond_3

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getCorePoolSize()I

    move-result p1

    if-eq p1, v5, :cond_3

    .line 144
    invoke-virtual {p0, v5}, Lcom/bytedance/sdk/openadsdk/l/a;->setCorePoolSize(I)V

    .line 145
    invoke-virtual {p0, v6}, Lcom/bytedance/sdk/openadsdk/l/a;->setMaximumPoolSize(I)V

    const-string p1, "ADThreadPoolExecutor"

    .line 146
    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "afterExecute: reduce "

    aput-object v2, v0, v4

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    aput-object v2, v0, v3

    const-string v2, " coreSize="

    aput-object v2, v0, v5

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getCorePoolSize()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "  maxSize="

    aput-object v1, v0, v6

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getMaximumPoolSize()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, p2

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 131
    :pswitch_1
    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result p1

    if-ge p1, v6, :cond_3

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getCorePoolSize()I

    move-result p1

    if-eqz p1, :cond_3

    .line 133
    :try_start_0
    invoke-virtual {p0, v4}, Lcom/bytedance/sdk/openadsdk/l/a;->setCorePoolSize(I)V

    .line 134
    invoke-virtual {p0, v6}, Lcom/bytedance/sdk/openadsdk/l/a;->setMaximumPoolSize(I)V

    const-string p1, "ADThreadPoolExecutor"

    .line 135
    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "afterExecute: reduce "

    aput-object v2, v0, v4

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    aput-object v2, v0, v3

    const-string v2, " coreSize="

    aput-object v2, v0, v5

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getCorePoolSize()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "  maxSize="

    aput-object v1, v0, v6

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getMaximumPoolSize()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, p2

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string p2, "ADThreadPoolExecutor"

    .line 137
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 8

    .line 53
    instance-of v0, p1, Lcom/bytedance/sdk/openadsdk/l/g;

    if-eqz v0, :cond_0

    .line 54
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/b;

    check-cast p1, Lcom/bytedance/sdk/openadsdk/l/g;

    invoke-direct {v0, p1, p0}, Lcom/bytedance/sdk/openadsdk/l/b;-><init>(Lcom/bytedance/sdk/openadsdk/l/g;Lcom/bytedance/sdk/openadsdk/l/a;)V

    invoke-super {p0, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 56
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/l/a$1;

    const-string v1, "unknown"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/l/a$1;-><init>(Lcom/bytedance/sdk/openadsdk/l/a;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 62
    new-instance p1, Lcom/bytedance/sdk/openadsdk/l/b;

    invoke-direct {p1, v0, p0}, Lcom/bytedance/sdk/openadsdk/l/b;-><init>(Lcom/bytedance/sdk/openadsdk/l/g;Lcom/bytedance/sdk/openadsdk/l/a;)V

    invoke-super {p0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 65
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/l/e;->e()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 66
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 69
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0x1a344

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq v2, v3, :cond_2

    const v3, 0x2daeb0

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    const-string v2, "aidl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const-string v2, "log"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    :goto_1
    const/4 v0, 0x5

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v6, 0x6

    const/4 v7, 0x4

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_2

    .line 96
    :pswitch_0
    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result p1

    if-lt p1, v7, :cond_4

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getCorePoolSize()I

    move-result p1

    if-eq p1, v7, :cond_4

    .line 97
    sget p1, Lcom/bytedance/sdk/openadsdk/l/e;->a:I

    add-int/2addr p1, v7

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/a;->setMaximumPoolSize(I)V

    .line 98
    invoke-virtual {p0, v7}, Lcom/bytedance/sdk/openadsdk/l/a;->setCorePoolSize(I)V

    const-string p1, "ADThreadPoolExecutor"

    .line 99
    new-array v1, v6, [Ljava/lang/Object;

    const-string v6, "execute: increase poolType =  "

    aput-object v6, v1, v4

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    aput-object v4, v1, v5

    const-string v4, " coreSize="

    aput-object v4, v1, v3

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getCorePoolSize()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "  maxSize="

    aput-object v2, v1, v7

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getMaximumPoolSize()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 84
    :pswitch_1
    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result p1

    if-lt p1, v7, :cond_4

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getCorePoolSize()I

    move-result p1

    if-eq p1, v7, :cond_4

    .line 86
    :try_start_0
    sget p1, Lcom/bytedance/sdk/openadsdk/l/e;->a:I

    add-int/2addr p1, v7

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/a;->setMaximumPoolSize(I)V

    .line 87
    invoke-virtual {p0, v7}, Lcom/bytedance/sdk/openadsdk/l/a;->setCorePoolSize(I)V

    const-string p1, "ADThreadPoolExecutor"

    .line 88
    new-array v1, v6, [Ljava/lang/Object;

    const-string v6, "execute: increase poolType =  "

    aput-object v6, v1, v4

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    aput-object v4, v1, v5

    const-string v4, " coreSize="

    aput-object v4, v1, v3

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getCorePoolSize()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "  maxSize="

    aput-object v2, v1, v7

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/l/a;->getMaximumPoolSize()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    const-string v0, "ADThreadPoolExecutor"

    .line 90
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public shutdown()V
    .locals 2

    const-string v0, "io"

    .line 167
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "aidl"

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 170
    :cond_0
    invoke-super {p0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method public shutdownNow()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    const-string v0, "io"

    .line 159
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "aidl"

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/l/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 162
    :cond_0
    invoke-super {p0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 160
    :cond_1
    :goto_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
