.class Lcom/bytedance/sdk/openadsdk/l/b;
.super Ljava/lang/Object;
.source "DelegateRunnable.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lcom/bytedance/sdk/openadsdk/l/g;

.field private b:Lcom/bytedance/sdk/openadsdk/l/a;

.field private c:J

.field private d:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/l/g;Lcom/bytedance/sdk/openadsdk/l/a;)V
    .locals 3

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 16
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    .line 17
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->b:Lcom/bytedance/sdk/openadsdk/l/a;

    const-wide/16 v1, 0x0

    .line 18
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/l/b;->c:J

    .line 19
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->d:Ljava/lang/Thread;

    .line 23
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    .line 24
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/l/b;->b:Lcom/bytedance/sdk/openadsdk/l/a;

    .line 25
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/l/b;->c:J

    return-void
.end method


# virtual methods
.method public a()Lcom/bytedance/sdk/openadsdk/l/g;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    return-object v0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 1

    .line 58
    instance-of v0, p1, Lcom/bytedance/sdk/openadsdk/l/b;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    check-cast p1, Lcom/bytedance/sdk/openadsdk/l/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/b;->a()Lcom/bytedance/sdk/openadsdk/l/g;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/l/g;->a(Lcom/bytedance/sdk/openadsdk/l/g;)I

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 71
    instance-of v0, p1, Lcom/bytedance/sdk/openadsdk/l/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    check-cast p1, Lcom/bytedance/sdk/openadsdk/l/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/l/b;->a()Lcom/bytedance/sdk/openadsdk/l/g;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public run()V
    .locals 8

    .line 30
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 31
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/l/b;->c:J

    sub-long v2, v0, v2

    .line 32
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/l/b;->d:Ljava/lang/Thread;

    .line 34
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    if-eqz v4, :cond_0

    .line 35
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/l/g;->run()V

    .line 37
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    .line 38
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->b:Lcom/bytedance/sdk/openadsdk/l/a;

    if-eqz v0, :cond_1

    .line 39
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/b;->b:Lcom/bytedance/sdk/openadsdk/l/a;

    invoke-static {v0, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/l/d;->a(Lcom/bytedance/sdk/openadsdk/l/a;JJ)V

    :cond_1
    const-string v0, "DelegateRunnable"

    const/16 v1, 0x8

    .line 41
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "run: pool  = "

    aput-object v7, v1, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/l/b;->b:Lcom/bytedance/sdk/openadsdk/l/a;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/l/b;->b:Lcom/bytedance/sdk/openadsdk/l/a;

    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/l/a;->a()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_2
    const-string v7, "null"

    :goto_0
    aput-object v7, v1, v6

    const/4 v6, 0x2

    const-string v7, " waitTime ="

    aput-object v7, v1, v6

    const/4 v6, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x4

    const-string v3, " taskCost = "

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, " name="

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/l/b;->a:Lcom/bytedance/sdk/openadsdk/l/g;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/l/g;->h()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    const-string v3, "null"

    :goto_1
    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
