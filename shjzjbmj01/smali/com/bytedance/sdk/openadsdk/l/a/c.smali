.class public Lcom/bytedance/sdk/openadsdk/l/a/c;
.super Ljava/lang/Object;
.source "ThreadPoolLogModel.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIJJJJI)V
    .locals 3

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 21
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->b:I

    .line 22
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->c:I

    const-wide/16 v1, 0x0

    .line 26
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->e:J

    .line 27
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->f:J

    .line 29
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->g:J

    .line 30
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->h:J

    .line 32
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->i:I

    .line 64
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->a:Ljava/lang/String;

    .line 65
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->b:I

    .line 66
    iput p3, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->c:I

    .line 67
    iput-wide p4, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->e:J

    .line 68
    iput-wide p6, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->f:J

    .line 69
    iput-wide p8, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->g:J

    .line 70
    iput-wide p10, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->h:J

    .line 71
    iput p12, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->i:I

    return-void
.end method


# virtual methods
.method public a()Lorg/json/JSONObject;
    .locals 7

    .line 36
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const/4 v1, 0x0

    .line 39
    :try_start_0
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->i:I

    if-gtz v2, :cond_0

    return-object v1

    :cond_0
    const-string v2, "poolType"

    .line 42
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "corePoolSize"

    .line 43
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->b:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "maximumPoolSize"

    .line 44
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->c:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "largestPoolSize"

    .line 45
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->d:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "waitLargestTime"

    .line 47
    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->e:J

    invoke-virtual {v0, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v2, "waitAvgTime"

    .line 48
    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->f:J

    long-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float v3, v3, v4

    iget v5, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->i:I

    int-to-float v5, v5

    div-float/2addr v3, v5

    float-to-long v5, v3

    invoke-virtual {v0, v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v2, "taskCostLargestTime"

    .line 49
    iget-wide v5, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->g:J

    invoke-virtual {v0, v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v2, "taskCostAvgTime"

    .line 50
    iget-wide v5, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->h:J

    long-to-float v3, v5

    mul-float v3, v3, v4

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->i:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v2, "logCount"

    .line 52
    iget v3, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->i:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 57
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    return-object v1
.end method

.method public a(I)V
    .locals 1

    .line 110
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->i:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->i:I

    return-void
.end method

.method public a(J)V
    .locals 0

    .line 84
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->e:J

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .line 118
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->d:I

    return-void
.end method

.method public b(J)V
    .locals 2

    .line 89
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->f:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->f:J

    return-void
.end method

.method public c()J
    .locals 2

    .line 80
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->e:J

    return-wide v0
.end method

.method public c(J)V
    .locals 0

    .line 97
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->g:J

    return-void
.end method

.method public d()J
    .locals 2

    .line 93
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->g:J

    return-wide v0
.end method

.method public d(J)V
    .locals 2

    .line 102
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->h:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->h:J

    return-void
.end method

.method public e()I
    .locals 1

    .line 106
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/l/a/c;->i:I

    return v0
.end method
