.class public final Lcom/bytedance/sdk/openadsdk/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final cancel_tv:I

.field public static final confirm_tv:I

.field public static final dash_line:I

.field public static final iv_app_icon:I

.field public static final iv_detail_back:I

.field public static final iv_privacy_back:I

.field public static final line:I

.field public static final ll_download:I

.field public static final message_tv:I

.field public static final permission_list:I

.field public static final privacy_webview:I

.field public static final tag_ignore:I

.field public static final tag_view_name:I

.field public static final title_bar:I

.field public static final tt_ad_container:I

.field public static final tt_ad_content_layout:I

.field public static final tt_ad_logo:I

.field public static final tt_app_detail_back_tv:I

.field public static final tt_app_developer_tv:I

.field public static final tt_app_name_tv:I

.field public static final tt_app_privacy_back_tv:I

.field public static final tt_app_privacy_title:I

.field public static final tt_app_privacy_tv:I

.field public static final tt_app_privacy_url_tv:I

.field public static final tt_app_version_tv:I

.field public static final tt_appdownloader_action:I

.field public static final tt_appdownloader_desc:I

.field public static final tt_appdownloader_download_progress:I

.field public static final tt_appdownloader_download_progress_new:I

.field public static final tt_appdownloader_download_size:I

.field public static final tt_appdownloader_download_status:I

.field public static final tt_appdownloader_download_success:I

.field public static final tt_appdownloader_download_success_size:I

.field public static final tt_appdownloader_download_success_status:I

.field public static final tt_appdownloader_download_text:I

.field public static final tt_appdownloader_icon:I

.field public static final tt_appdownloader_root:I

.field public static final tt_backup_draw_bg:I

.field public static final tt_battery_time_layout:I

.field public static final tt_browser_download_btn:I

.field public static final tt_browser_download_btn_stub:I

.field public static final tt_browser_progress:I

.field public static final tt_browser_titlebar_dark_view_stub:I

.field public static final tt_browser_titlebar_view_stub:I

.field public static final tt_browser_webview:I

.field public static final tt_browser_webview_loading:I

.field public static final tt_bu_close:I

.field public static final tt_bu_desc:I

.field public static final tt_bu_dislike:I

.field public static final tt_bu_download:I

.field public static final tt_bu_icon:I

.field public static final tt_bu_img:I

.field public static final tt_bu_img_1:I

.field public static final tt_bu_img_2:I

.field public static final tt_bu_img_3:I

.field public static final tt_bu_img_container:I

.field public static final tt_bu_img_content:I

.field public static final tt_bu_name:I

.field public static final tt_bu_score:I

.field public static final tt_bu_score_bar:I

.field public static final tt_bu_title:I

.field public static final tt_bu_video_container:I

.field public static final tt_bu_video_container_inner:I

.field public static final tt_bu_video_icon:I

.field public static final tt_bu_video_name1:I

.field public static final tt_bu_video_name2:I

.field public static final tt_bu_video_score:I

.field public static final tt_bu_video_score_bar:I

.field public static final tt_column_line:I

.field public static final tt_comment_close:I

.field public static final tt_comment_commit:I

.field public static final tt_comment_content:I

.field public static final tt_comment_number:I

.field public static final tt_dislike_dialog_linearlayout:I

.field public static final tt_dislike_header_back:I

.field public static final tt_dislike_header_tv:I

.field public static final tt_dislike_line1:I

.field public static final tt_dislike_title_content:I

.field public static final tt_download_app_btn:I

.field public static final tt_download_app_detail:I

.field public static final tt_download_app_developer:I

.field public static final tt_download_app_privacy:I

.field public static final tt_download_app_version:I

.field public static final tt_download_btn:I

.field public static final tt_download_cancel:I

.field public static final tt_download_icon:I

.field public static final tt_download_layout:I

.field public static final tt_download_title:I

.field public static final tt_edit_suggestion:I

.field public static final tt_filer_words_lv:I

.field public static final tt_filer_words_lv_second:I

.field public static final tt_image:I

.field public static final tt_insert_ad_img:I

.field public static final tt_insert_ad_logo:I

.field public static final tt_insert_ad_text:I

.field public static final tt_insert_dislike_icon_img:I

.field public static final tt_insert_express_ad_fl:I

.field public static final tt_install_btn_no:I

.field public static final tt_install_btn_yes:I

.field public static final tt_install_content:I

.field public static final tt_install_title:I

.field public static final tt_item_desc_tv:I

.field public static final tt_item_select_img:I

.field public static final tt_item_title_tv:I

.field public static final tt_item_tv:I

.field public static final tt_item_tv_son:I

.field public static final tt_lite_web_back:I

.field public static final tt_lite_web_title:I

.field public static final tt_lite_web_view:I

.field public static final tt_message:I

.field public static final tt_middle_page_layout:I

.field public static final tt_native_video_container:I

.field public static final tt_native_video_frame:I

.field public static final tt_native_video_img_cover:I

.field public static final tt_native_video_img_cover_viewStub:I

.field public static final tt_native_video_img_id:I

.field public static final tt_native_video_layout:I

.field public static final tt_native_video_play:I

.field public static final tt_native_video_titlebar:I

.field public static final tt_negtive:I

.field public static final tt_open_app_detail_layout:I

.field public static final tt_personalization_layout:I

.field public static final tt_personalization_name:I

.field public static final tt_playable_ad_close:I

.field public static final tt_playable_ad_close_layout:I

.field public static final tt_playable_ad_dislike:I

.field public static final tt_playable_loading:I

.field public static final tt_positive:I

.field public static final tt_privacy_layout:I

.field public static final tt_privacy_list:I

.field public static final tt_privacy_webview:I

.field public static final tt_rl_download:I

.field public static final tt_root_view:I

.field public static final tt_scroll_view:I

.field public static final tt_splash_ad_gif:I

.field public static final tt_splash_express_container:I

.field public static final tt_splash_skip_btn:I

.field public static final tt_splash_video_ad_mute:I

.field public static final tt_splash_video_container:I

.field public static final tt_title:I

.field public static final tt_titlebar_app_detail:I

.field public static final tt_titlebar_app_name:I

.field public static final tt_titlebar_app_privacy:I

.field public static final tt_titlebar_back:I

.field public static final tt_titlebar_close:I

.field public static final tt_titlebar_detail_layout:I

.field public static final tt_titlebar_developer:I

.field public static final tt_titlebar_dislike:I

.field public static final tt_titlebar_title:I

.field public static final tt_video_ad_bottom_layout:I

.field public static final tt_video_ad_button:I

.field public static final tt_video_ad_button_draw:I

.field public static final tt_video_ad_cover:I

.field public static final tt_video_ad_cover_center_layout:I

.field public static final tt_video_ad_cover_center_layout_draw:I

.field public static final tt_video_ad_covers:I

.field public static final tt_video_ad_finish_cover_image:I

.field public static final tt_video_ad_full_screen:I

.field public static final tt_video_ad_logo_image:I

.field public static final tt_video_ad_name:I

.field public static final tt_video_ad_replay:I

.field public static final tt_video_app_detail:I

.field public static final tt_video_app_detail_layout:I

.field public static final tt_video_app_name:I

.field public static final tt_video_app_privacy:I

.field public static final tt_video_back:I

.field public static final tt_video_btn_ad_image_tv:I

.field public static final tt_video_close:I

.field public static final tt_video_current_time:I

.field public static final tt_video_developer:I

.field public static final tt_video_draw_layout_viewStub:I

.field public static final tt_video_fullscreen_back:I

.field public static final tt_video_loading_cover_image:I

.field public static final tt_video_loading_progress:I

.field public static final tt_video_loading_retry:I

.field public static final tt_video_loading_retry_layout:I

.field public static final tt_video_play:I

.field public static final tt_video_progress:I

.field public static final tt_video_retry:I

.field public static final tt_video_retry_des:I

.field public static final tt_video_seekbar:I

.field public static final tt_video_time_left_time:I

.field public static final tt_video_time_play:I

.field public static final tt_video_title:I

.field public static final tt_video_top_layout:I

.field public static final tt_video_top_title:I

.field public static final tt_video_traffic_continue_play_btn:I

.field public static final tt_video_traffic_continue_play_tv:I

.field public static final tt_video_traffic_tip_layout:I

.field public static final tt_video_traffic_tip_layout_viewStub:I

.field public static final tt_video_traffic_tip_tv:I

.field public static final tv_app_detail:I

.field public static final tv_app_developer:I

.field public static final tv_app_name:I

.field public static final tv_app_privacy:I

.field public static final tv_app_version:I

.field public static final tv_empty:I

.field public static final tv_give_up:I

.field public static final tv_permission_description:I

.field public static final tv_permission_title:I

.field public static final web_frame:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 217
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->cancel_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->cancel_tv:I

    .line 218
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->confirm_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->confirm_tv:I

    .line 219
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->dash_line:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->dash_line:I

    .line 220
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->iv_app_icon:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->iv_app_icon:I

    .line 221
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->iv_detail_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->iv_detail_back:I

    .line 222
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->iv_privacy_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->iv_privacy_back:I

    .line 223
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->line:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->line:I

    .line 224
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->ll_download:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->ll_download:I

    .line 225
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->message_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->message_tv:I

    .line 226
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->permission_list:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->permission_list:I

    .line 227
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->privacy_webview:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->privacy_webview:I

    .line 228
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tag_ignore:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tag_ignore:I

    .line 229
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tag_view_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tag_view_name:I

    .line 230
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->title_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->title_bar:I

    .line 231
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_ad_container:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_ad_container:I

    .line 232
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_ad_content_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_ad_content_layout:I

    .line 233
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_ad_logo:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_ad_logo:I

    .line 234
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_app_detail_back_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_app_detail_back_tv:I

    .line 235
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_app_developer_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_app_developer_tv:I

    .line 236
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_app_name_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_app_name_tv:I

    .line 237
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_app_privacy_back_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_app_privacy_back_tv:I

    .line 238
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_app_privacy_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_app_privacy_title:I

    .line 239
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_app_privacy_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_app_privacy_tv:I

    .line 240
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_app_privacy_url_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_app_privacy_url_tv:I

    .line 241
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_app_version_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_app_version_tv:I

    .line 242
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_action:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_action:I

    .line 243
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_desc:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_desc:I

    .line 244
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_progress:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_download_progress:I

    .line 245
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_progress_new:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_download_progress_new:I

    .line 246
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_size:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_download_size:I

    .line 247
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_status:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_download_status:I

    .line 248
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_success:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_download_success:I

    .line 249
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_success_size:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_download_success_size:I

    .line 250
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_success_status:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_download_success_status:I

    .line 251
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_text:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_download_text:I

    .line 252
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_icon:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_icon:I

    .line 253
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_root:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_appdownloader_root:I

    .line 254
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_backup_draw_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_backup_draw_bg:I

    .line 255
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_battery_time_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_battery_time_layout:I

    .line 256
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_browser_download_btn:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_browser_download_btn:I

    .line 257
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_browser_download_btn_stub:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_browser_download_btn_stub:I

    .line 258
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_browser_progress:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_browser_progress:I

    .line 259
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_browser_titlebar_dark_view_stub:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_browser_titlebar_dark_view_stub:I

    .line 260
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_browser_titlebar_view_stub:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_browser_titlebar_view_stub:I

    .line 261
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_browser_webview:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_browser_webview:I

    .line 262
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_browser_webview_loading:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_browser_webview_loading:I

    .line 263
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_close:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_close:I

    .line 264
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_desc:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_desc:I

    .line 265
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_dislike:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_dislike:I

    .line 266
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_download:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_download:I

    .line 267
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_icon:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_icon:I

    .line 268
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_img:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_img:I

    .line 269
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_img_1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_img_1:I

    .line 270
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_img_2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_img_2:I

    .line 271
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_img_3:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_img_3:I

    .line 272
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_img_container:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_img_container:I

    .line 273
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_img_content:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_img_content:I

    .line 274
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_name:I

    .line 275
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_score:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_score:I

    .line 276
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_score_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_score_bar:I

    .line 277
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_title:I

    .line 278
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_video_container:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_video_container:I

    .line 279
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_video_container_inner:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_video_container_inner:I

    .line 280
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_video_icon:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_video_icon:I

    .line 281
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_video_name1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_video_name1:I

    .line 282
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_video_name2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_video_name2:I

    .line 283
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_video_score:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_video_score:I

    .line 284
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_bu_video_score_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_bu_video_score_bar:I

    .line 285
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_column_line:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_column_line:I

    .line 286
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_comment_close:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_comment_close:I

    .line 287
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_comment_commit:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_comment_commit:I

    .line 288
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_comment_content:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_comment_content:I

    .line 289
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_comment_number:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_comment_number:I

    .line 290
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_dislike_dialog_linearlayout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_dislike_dialog_linearlayout:I

    .line 291
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_dislike_header_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_dislike_header_back:I

    .line 292
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_dislike_header_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_dislike_header_tv:I

    .line 293
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_dislike_line1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_dislike_line1:I

    .line 294
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_dislike_title_content:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_dislike_title_content:I

    .line 295
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_app_btn:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_app_btn:I

    .line 296
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_app_detail:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_app_detail:I

    .line 297
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_app_developer:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_app_developer:I

    .line 298
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_app_privacy:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_app_privacy:I

    .line 299
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_app_version:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_app_version:I

    .line 300
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_btn:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_btn:I

    .line 301
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_cancel:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_cancel:I

    .line 302
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_icon:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_icon:I

    .line 303
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_layout:I

    .line 304
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_download_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_download_title:I

    .line 305
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_edit_suggestion:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_edit_suggestion:I

    .line 306
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_filer_words_lv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_filer_words_lv:I

    .line 307
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_filer_words_lv_second:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_filer_words_lv_second:I

    .line 308
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_image:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_image:I

    .line 309
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_insert_ad_img:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_insert_ad_img:I

    .line 310
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_insert_ad_logo:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_insert_ad_logo:I

    .line 311
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_insert_ad_text:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_insert_ad_text:I

    .line 312
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_insert_dislike_icon_img:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_insert_dislike_icon_img:I

    .line 313
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_insert_express_ad_fl:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_insert_express_ad_fl:I

    .line 314
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_install_btn_no:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_install_btn_no:I

    .line 315
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_install_btn_yes:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_install_btn_yes:I

    .line 316
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_install_content:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_install_content:I

    .line 317
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_install_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_install_title:I

    .line 318
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_item_desc_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_item_desc_tv:I

    .line 319
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_item_select_img:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_item_select_img:I

    .line 320
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_item_title_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_item_title_tv:I

    .line 321
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_item_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_item_tv:I

    .line 322
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_item_tv_son:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_item_tv_son:I

    .line 323
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_lite_web_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_lite_web_back:I

    .line 324
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_lite_web_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_lite_web_title:I

    .line 325
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_lite_web_view:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_lite_web_view:I

    .line 326
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_message:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_message:I

    .line 327
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_middle_page_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_middle_page_layout:I

    .line 328
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_native_video_container:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_native_video_container:I

    .line 329
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_native_video_frame:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_native_video_frame:I

    .line 330
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_native_video_img_cover:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_native_video_img_cover:I

    .line 331
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_native_video_img_cover_viewStub:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_native_video_img_cover_viewStub:I

    .line 332
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_native_video_img_id:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_native_video_img_id:I

    .line 333
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_native_video_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_native_video_layout:I

    .line 334
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_native_video_play:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_native_video_play:I

    .line 335
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_native_video_titlebar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_native_video_titlebar:I

    .line 336
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_negtive:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_negtive:I

    .line 337
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_open_app_detail_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_open_app_detail_layout:I

    .line 338
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_personalization_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_personalization_layout:I

    .line 339
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_personalization_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_personalization_name:I

    .line 340
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_playable_ad_close:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_playable_ad_close:I

    .line 341
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_playable_ad_close_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_playable_ad_close_layout:I

    .line 342
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_playable_ad_dislike:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_playable_ad_dislike:I

    .line 343
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_playable_loading:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_playable_loading:I

    .line 344
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_positive:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_positive:I

    .line 345
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_privacy_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_privacy_layout:I

    .line 346
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_privacy_list:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_privacy_list:I

    .line 347
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_privacy_webview:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_privacy_webview:I

    .line 348
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_rl_download:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_rl_download:I

    .line 349
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_root_view:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_root_view:I

    .line 350
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_scroll_view:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_scroll_view:I

    .line 351
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_splash_ad_gif:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_splash_ad_gif:I

    .line 352
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_splash_express_container:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_splash_express_container:I

    .line 353
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_splash_skip_btn:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_splash_skip_btn:I

    .line 354
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_splash_video_ad_mute:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_splash_video_ad_mute:I

    .line 355
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_splash_video_container:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_splash_video_container:I

    .line 356
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_title:I

    .line 357
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_titlebar_app_detail:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_titlebar_app_detail:I

    .line 358
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_titlebar_app_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_titlebar_app_name:I

    .line 359
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_titlebar_app_privacy:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_titlebar_app_privacy:I

    .line 360
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_titlebar_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_titlebar_back:I

    .line 361
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_titlebar_close:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_titlebar_close:I

    .line 362
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_titlebar_detail_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_titlebar_detail_layout:I

    .line 363
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_titlebar_developer:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_titlebar_developer:I

    .line 364
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_titlebar_dislike:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_titlebar_dislike:I

    .line 365
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_titlebar_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_titlebar_title:I

    .line 366
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_bottom_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_bottom_layout:I

    .line 367
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_button:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_button:I

    .line 368
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_button_draw:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_button_draw:I

    .line 369
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_cover:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_cover:I

    .line 370
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_cover_center_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_cover_center_layout:I

    .line 371
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_cover_center_layout_draw:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_cover_center_layout_draw:I

    .line 372
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_covers:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_covers:I

    .line 373
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_finish_cover_image:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_finish_cover_image:I

    .line 374
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_full_screen:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_full_screen:I

    .line 375
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_logo_image:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_logo_image:I

    .line 376
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_name:I

    .line 377
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_ad_replay:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_ad_replay:I

    .line 378
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_app_detail:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_app_detail:I

    .line 379
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_app_detail_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_app_detail_layout:I

    .line 380
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_app_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_app_name:I

    .line 381
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_app_privacy:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_app_privacy:I

    .line 382
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_back:I

    .line 383
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_btn_ad_image_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_btn_ad_image_tv:I

    .line 384
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_close:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_close:I

    .line 385
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_current_time:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_current_time:I

    .line 386
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_developer:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_developer:I

    .line 387
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_draw_layout_viewStub:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_draw_layout_viewStub:I

    .line 388
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_fullscreen_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_fullscreen_back:I

    .line 389
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_loading_cover_image:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_loading_cover_image:I

    .line 390
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_loading_progress:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_loading_progress:I

    .line 391
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_loading_retry:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_loading_retry:I

    .line 392
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_loading_retry_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_loading_retry_layout:I

    .line 393
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_play:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_play:I

    .line 394
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_progress:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_progress:I

    .line 395
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_retry:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_retry:I

    .line 396
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_retry_des:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_retry_des:I

    .line 397
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_seekbar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_seekbar:I

    .line 398
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_time_left_time:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_time_left_time:I

    .line 399
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_time_play:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_time_play:I

    .line 400
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_title:I

    .line 401
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_top_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_top_layout:I

    .line 402
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_top_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_top_title:I

    .line 403
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_traffic_continue_play_btn:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_traffic_continue_play_btn:I

    .line 404
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_traffic_continue_play_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_traffic_continue_play_tv:I

    .line 405
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_traffic_tip_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_traffic_tip_layout:I

    .line 406
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_traffic_tip_layout_viewStub:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_traffic_tip_layout_viewStub:I

    .line 407
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_video_traffic_tip_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tt_video_traffic_tip_tv:I

    .line 408
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_detail:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tv_app_detail:I

    .line 409
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_developer:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tv_app_developer:I

    .line 410
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tv_app_name:I

    .line 411
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_privacy:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tv_app_privacy:I

    .line 412
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_version:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tv_app_version:I

    .line 413
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_empty:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tv_empty:I

    .line 414
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_give_up:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tv_give_up:I

    .line 415
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_permission_description:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tv_permission_description:I

    .line 416
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_permission_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->tv_permission_title:I

    .line 417
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->web_frame:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$id;->web_frame:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
