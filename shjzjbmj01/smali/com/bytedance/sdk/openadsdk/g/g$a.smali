.class Lcom/bytedance/sdk/openadsdk/g/g$a;
.super Landroid/content/BroadcastReceiver;
.source "VolumeChangeObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/g/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/g/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/g/g;)V
    .locals 1

    .line 167
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 168
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/g/g$a;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    const-string p1, "android.media.VOLUME_CHANGED_ACTION"

    .line 175
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    const/4 v0, -0x1

    .line 176
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    const/4 p2, 0x3

    if-ne p1, p2, :cond_0

    const-string p1, "VolumeChangeObserver"

    const-string p2, "\u5a92\u4f53\u97f3\u91cf\u6539\u53d8\u901a......."

    .line 177
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/g/g$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/g/g;

    if-eqz p1, :cond_0

    .line 180
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/g/g;->h()Lcom/bytedance/sdk/openadsdk/g/f;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 182
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/g/g;->g()I

    move-result v0

    .line 183
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/g/g;->a()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 184
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/g/g;->a(I)V

    if-ltz v0, :cond_0

    .line 186
    invoke-interface {p2, v0}, Lcom/bytedance/sdk/openadsdk/g/f;->c(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "VolumeChangeObserver"

    const-string v0, "onVolumeChangedError: "

    .line 194
    invoke-static {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method
