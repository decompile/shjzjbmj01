.class public final Lcom/bytedance/sdk/openadsdk/component/reward/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final DialogFullscreen:I

.field public static final EditTextStyle:I

.field public static final Theme_Dialog_TTDownload:I

.field public static final Theme_Dialog_TTDownloadOld:I

.field public static final tt_Widget_ProgressBar_Horizontal:I

.field public static final tt_appdownloader_style_detail_download_progress_bar:I

.field public static final tt_appdownloader_style_notification_text:I

.field public static final tt_appdownloader_style_notification_title:I

.field public static final tt_appdownloader_style_progress_bar:I

.field public static final tt_appdownloader_style_progress_bar_new:I

.field public static final tt_back_view:I

.field public static final tt_custom_dialog:I

.field public static final tt_dislikeDialog:I

.field public static final tt_dislikeDialogAnimation:I

.field public static final tt_dislikeDialog_new:I

.field public static final tt_full_screen:I

.field public static final tt_ss_popup_toast_anim:I

.field public static final tt_wg_insert_dialog:I

.field public static final tt_widget_gifView:I

.field public static final ttdownloader_translucent_dialog:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 623
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->DialogFullscreen:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->DialogFullscreen:I

    .line 624
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->EditTextStyle:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->EditTextStyle:I

    .line 625
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->Theme_Dialog_TTDownload:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->Theme_Dialog_TTDownload:I

    .line 626
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->Theme_Dialog_TTDownloadOld:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->Theme_Dialog_TTDownloadOld:I

    .line 627
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_Widget_ProgressBar_Horizontal:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_Widget_ProgressBar_Horizontal:I

    .line 628
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_detail_download_progress_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_appdownloader_style_detail_download_progress_bar:I

    .line 629
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_notification_text:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_appdownloader_style_notification_text:I

    .line 630
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_notification_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_appdownloader_style_notification_title:I

    .line 631
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_progress_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_appdownloader_style_progress_bar:I

    .line 632
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_progress_bar_new:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_appdownloader_style_progress_bar_new:I

    .line 633
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_back_view:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_back_view:I

    .line 634
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_custom_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_custom_dialog:I

    .line 635
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_dislikeDialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_dislikeDialog:I

    .line 636
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_dislikeDialogAnimation:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_dislikeDialogAnimation:I

    .line 637
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_dislikeDialog_new:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_dislikeDialog_new:I

    .line 638
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_full_screen:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_full_screen:I

    .line 639
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_ss_popup_toast_anim:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_ss_popup_toast_anim:I

    .line 640
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_wg_insert_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_wg_insert_dialog:I

    .line 641
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_widget_gifView:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->tt_widget_gifView:I

    .line 642
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->ttdownloader_translucent_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$style;->ttdownloader_translucent_dialog:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 622
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
