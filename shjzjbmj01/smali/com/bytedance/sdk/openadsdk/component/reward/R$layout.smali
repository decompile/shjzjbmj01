.class public final Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final tt_activity_full_video:I

.field public static final tt_activity_full_video_new_bar_3_style:I

.field public static final tt_activity_full_video_newstyle:I

.field public static final tt_activity_lite_web_layout:I

.field public static final tt_activity_middle_page:I

.field public static final tt_activity_reward_and_full_video_bar:I

.field public static final tt_activity_reward_and_full_video_new_bar:I

.field public static final tt_activity_reward_video_newstyle:I

.field public static final tt_activity_rewardvideo:I

.field public static final tt_activity_rewardvideo_new_bar_3_style:I

.field public static final tt_activity_ttlandingpage:I

.field public static final tt_activity_ttlandingpage_playable:I

.field public static final tt_activity_video_scroll_landingpage:I

.field public static final tt_activity_videolandingpage:I

.field public static final tt_app_detail_dialog:I

.field public static final tt_app_detail_full_dialog:I

.field public static final tt_app_detail_full_dialog_list_head:I

.field public static final tt_app_detail_listview_item:I

.field public static final tt_app_privacy_dialog:I

.field public static final tt_appdownloader_notification_layout:I

.field public static final tt_backup_ad:I

.field public static final tt_backup_ad1:I

.field public static final tt_backup_ad2:I

.field public static final tt_backup_banner_layout1:I

.field public static final tt_backup_banner_layout2:I

.field public static final tt_backup_banner_layout3:I

.field public static final tt_backup_draw:I

.field public static final tt_backup_feed_horizontal:I

.field public static final tt_backup_feed_img_group:I

.field public static final tt_backup_feed_img_small:I

.field public static final tt_backup_feed_vertical:I

.field public static final tt_backup_feed_video:I

.field public static final tt_backup_full_reward:I

.field public static final tt_backup_insert_layout1:I

.field public static final tt_backup_insert_layout2:I

.field public static final tt_backup_insert_layout3:I

.field public static final tt_browser_download_layout:I

.field public static final tt_browser_titlebar:I

.field public static final tt_browser_titlebar_for_dark:I

.field public static final tt_common_download_dialog:I

.field public static final tt_custom_dailog_layout:I

.field public static final tt_dialog_listview_item:I

.field public static final tt_dislike_comment_layout:I

.field public static final tt_dislike_dialog_layout:I

.field public static final tt_dislike_dialog_layout1:I

.field public static final tt_dislike_dialog_layout2:I

.field public static final tt_dislike_flowlayout_tv:I

.field public static final tt_insert_ad_layout:I

.field public static final tt_install_dialog_layout:I

.field public static final tt_native_video_ad_view:I

.field public static final tt_native_video_img_cover_layout:I

.field public static final tt_playable_loading_layout:I

.field public static final tt_playable_view_layout:I

.field public static final tt_splash_view:I

.field public static final tt_top_full_1:I

.field public static final tt_top_reward_1:I

.field public static final tt_top_reward_dislike_2:I

.field public static final tt_video_ad_cover_layout:I

.field public static final tt_video_detail_layout:I

.field public static final tt_video_draw_btn_layout:I

.field public static final tt_video_play_layout_for_live:I

.field public static final tt_video_traffic_tip:I

.field public static final tt_video_traffic_tips_layout:I

.field public static final ttdownloader_activity_app_detail_info:I

.field public static final ttdownloader_activity_app_privacy_policy:I

.field public static final ttdownloader_dialog_appinfo:I

.field public static final ttdownloader_dialog_select_operation:I

.field public static final ttdownloader_item_permission:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 452
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_full_video:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_full_video:I

    .line 453
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_full_video_new_bar_3_style:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_full_video_new_bar_3_style:I

    .line 454
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_full_video_newstyle:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_full_video_newstyle:I

    .line 455
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_lite_web_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_lite_web_layout:I

    .line 456
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_middle_page:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_middle_page:I

    .line 457
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_reward_and_full_video_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_reward_and_full_video_bar:I

    .line 458
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_reward_and_full_video_new_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_reward_and_full_video_new_bar:I

    .line 459
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_reward_video_newstyle:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_reward_video_newstyle:I

    .line 460
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_rewardvideo:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_rewardvideo:I

    .line 461
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_rewardvideo_new_bar_3_style:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_rewardvideo_new_bar_3_style:I

    .line 462
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_ttlandingpage:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_ttlandingpage:I

    .line 463
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_ttlandingpage_playable:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_ttlandingpage_playable:I

    .line 464
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_video_scroll_landingpage:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_video_scroll_landingpage:I

    .line 465
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_activity_videolandingpage:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_activity_videolandingpage:I

    .line 466
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_app_detail_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_app_detail_dialog:I

    .line 467
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_app_detail_full_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_app_detail_full_dialog:I

    .line 468
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_app_detail_full_dialog_list_head:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_app_detail_full_dialog_list_head:I

    .line 469
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_app_detail_listview_item:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_app_detail_listview_item:I

    .line 470
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_app_privacy_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_app_privacy_dialog:I

    .line 471
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_appdownloader_notification_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_appdownloader_notification_layout:I

    .line 472
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_ad:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_ad:I

    .line 473
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_ad1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_ad1:I

    .line 474
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_ad2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_ad2:I

    .line 475
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_banner_layout1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_banner_layout1:I

    .line 476
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_banner_layout2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_banner_layout2:I

    .line 477
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_banner_layout3:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_banner_layout3:I

    .line 478
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_draw:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_draw:I

    .line 479
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_feed_horizontal:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_feed_horizontal:I

    .line 480
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_feed_img_group:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_feed_img_group:I

    .line 481
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_feed_img_small:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_feed_img_small:I

    .line 482
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_feed_vertical:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_feed_vertical:I

    .line 483
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_feed_video:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_feed_video:I

    .line 484
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_full_reward:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_full_reward:I

    .line 485
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_insert_layout1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_insert_layout1:I

    .line 486
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_insert_layout2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_insert_layout2:I

    .line 487
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_backup_insert_layout3:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_backup_insert_layout3:I

    .line 488
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_browser_download_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_browser_download_layout:I

    .line 489
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_browser_titlebar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_browser_titlebar:I

    .line 490
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_browser_titlebar_for_dark:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_browser_titlebar_for_dark:I

    .line 491
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_common_download_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_common_download_dialog:I

    .line 492
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_custom_dailog_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_custom_dailog_layout:I

    .line 493
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_dialog_listview_item:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_dialog_listview_item:I

    .line 494
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_dislike_comment_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_dislike_comment_layout:I

    .line 495
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_dislike_dialog_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_dislike_dialog_layout:I

    .line 496
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_dislike_dialog_layout1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_dislike_dialog_layout1:I

    .line 497
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_dislike_dialog_layout2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_dislike_dialog_layout2:I

    .line 498
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_dislike_flowlayout_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_dislike_flowlayout_tv:I

    .line 499
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_insert_ad_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_insert_ad_layout:I

    .line 500
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_install_dialog_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_install_dialog_layout:I

    .line 501
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_native_video_ad_view:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_native_video_ad_view:I

    .line 502
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_native_video_img_cover_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_native_video_img_cover_layout:I

    .line 503
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_playable_loading_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_playable_loading_layout:I

    .line 504
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_playable_view_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_playable_view_layout:I

    .line 505
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_splash_view:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_splash_view:I

    .line 506
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_top_full_1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_top_full_1:I

    .line 507
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_top_reward_1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_top_reward_1:I

    .line 508
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_top_reward_dislike_2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_top_reward_dislike_2:I

    .line 509
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_video_ad_cover_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_video_ad_cover_layout:I

    .line 510
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_video_detail_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_video_detail_layout:I

    .line 511
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_video_draw_btn_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_video_draw_btn_layout:I

    .line 512
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_video_play_layout_for_live:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_video_play_layout_for_live:I

    .line 513
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_video_traffic_tip:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_video_traffic_tip:I

    .line 514
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->tt_video_traffic_tips_layout:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->tt_video_traffic_tips_layout:I

    .line 515
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->ttdownloader_activity_app_detail_info:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->ttdownloader_activity_app_detail_info:I

    .line 516
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->ttdownloader_activity_app_privacy_policy:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->ttdownloader_activity_app_privacy_policy:I

    .line 517
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->ttdownloader_dialog_appinfo:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->ttdownloader_dialog_appinfo:I

    .line 518
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->ttdownloader_dialog_select_operation:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->ttdownloader_dialog_select_operation:I

    .line 519
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$layout;->ttdownloader_item_permission:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/reward/R$layout;->ttdownloader_item_permission:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
