.class Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;
.super Ljava/lang/Object;
.source "RewardVideoLoadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/g/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/reward/h$2;->a(Lcom/bytedance/sdk/openadsdk/core/d/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/component/reward/h$2;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/reward/h$2;Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->b:Lcom/bytedance/sdk/openadsdk/component/reward/h$2;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->a:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 4

    .line 232
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->b:Lcom/bytedance/sdk/openadsdk/component/reward/h$2;

    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/h$2;->a:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->b:Lcom/bytedance/sdk/openadsdk/component/reward/h$2;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/h$2;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->a:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->a:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 233
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->D()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 234
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->b:Lcom/bytedance/sdk/openadsdk/component/reward/h$2;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/h$2;->e:Lcom/bytedance/sdk/openadsdk/component/reward/h;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/reward/h;->a(Lcom/bytedance/sdk/openadsdk/component/reward/h;)Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->a:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->b:Lcom/bytedance/sdk/openadsdk/component/reward/h$2;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/component/reward/h$2;->c:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getDurationSlotType()I

    move-result v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->b(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->b:Lcom/bytedance/sdk/openadsdk/component/reward/h$2;

    iget-wide v2, v2, Lcom/bytedance/sdk/openadsdk/component/reward/h$2;->d:J

    invoke-static {p1, v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;J)V

    .line 235
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/reward/h$2$1;->b:Lcom/bytedance/sdk/openadsdk/component/reward/h$2;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/reward/h$2;->b:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;->onRewardVideoCached()V

    :cond_0
    return-void
.end method
