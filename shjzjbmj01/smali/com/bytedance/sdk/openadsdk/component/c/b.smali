.class Lcom/bytedance/sdk/openadsdk/component/c/b;
.super Ljava/lang/Object;
.source "TTInteractionAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTInteractionAd;


# static fields
.field private static k:Z


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field private d:Landroid/app/Dialog;

.field private e:Lcom/bytedance/sdk/openadsdk/dislike/b;

.field private f:Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;

.field private g:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

.field private h:Lcom/bytedance/sdk/openadsdk/core/l;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "interaction"

    .line 43
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->a:Ljava/lang/String;

    .line 59
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->b:Landroid/content/Context;

    .line 60
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/c/b;)Landroid/app/Dialog;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->d:Landroid/app/Dialog;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/c/b;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->j:Landroid/widget/ImageView;

    return-object p1
.end method

.method private a()V
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->d:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/n;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/n;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->d:Landroid/app/Dialog;

    .line 84
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->d:Landroid/app/Dialog;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/c/b$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/c/b$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/c/b;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 96
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->d:Landroid/app/Dialog;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/c/b$2;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/c/b$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/c/b;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 106
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->d:Landroid/app/Dialog;

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/n;

    const/4 v1, 0x0

    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/c/b$3;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/component/c/b$3;-><init>(Lcom/bytedance/sdk/openadsdk/component/c/b;)V

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/n;->a(ZLcom/bytedance/sdk/openadsdk/core/n$a;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/c/b;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->i:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/c/b;)Lcom/bytedance/sdk/openadsdk/core/d/l;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    return-object p0
.end method

.method private b()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 145
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/a/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-string v3, "interaction"

    const/4 v4, 0x3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/a/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;I)V

    .line 146
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/a/a;->a(Landroid/view/View;)V

    .line 147
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/a/a;->b(Landroid/view/View;)V

    .line 148
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->g:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/a/a;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V

    .line 149
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/c/b$4;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/c/b$4;-><init>(Lcom/bytedance/sdk/openadsdk/component/c/b;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/a/b$a;)V

    .line 166
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/c/b;)Landroid/content/Context;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->b:Landroid/content/Context;

    return-object p0
.end method

.method private c()V
    .locals 4

    .line 177
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->Z()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/d/k;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/k;->b()I

    move-result v0

    .line 178
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->Z()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/d/k;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/k;->a()Ljava/lang/String;

    move-result-object v1

    .line 180
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/e;->c()Lcom/bytedance/sdk/openadsdk/i/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/i/e;->h()Lcom/bytedance/sdk/adnet/b/d;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/component/c/b$5;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/component/c/b$5;-><init>(Lcom/bytedance/sdk/openadsdk/component/c/b;)V

    invoke-virtual {v2, v1, v3, v0, v0}, Lcom/bytedance/sdk/adnet/b/d;->a(Ljava/lang/String;Lcom/bytedance/sdk/adnet/b/d$e;II)V

    return-void
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/component/c/b;)Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->f:Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;

    return-object p0
.end method

.method private d()V
    .locals 1

    const/4 v0, 0x0

    .line 286
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/component/c/b;->k:Z

    .line 287
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/component/c/b;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->g:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/component/c/b;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/c/b;->b()V

    return-void
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/component/c/b;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/c/b;->c()V

    return-void
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/component/c/b;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/c/b;->d()V

    return-void
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/component/c/b;)Lcom/bytedance/sdk/openadsdk/dislike/b;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->e:Lcom/bytedance/sdk/openadsdk/dislike/b;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/component/c/b;)Landroid/widget/ImageView;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->j:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/component/c/b;)Lcom/bytedance/sdk/openadsdk/core/l;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->h:Lcom/bytedance/sdk/openadsdk/core/l;

    return-object p0
.end method


# virtual methods
.method a(Lcom/bytedance/sdk/openadsdk/core/l;)V
    .locals 3
    .param p1    # Lcom/bytedance/sdk/openadsdk/core/l;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 67
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->h:Lcom/bytedance/sdk/openadsdk/core/l;

    .line 68
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    .line 69
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/c/b;->getInteractionType()I

    move-result p1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 70
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-string v1, "interaction"

    invoke-static {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->g:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    .line 72
    :cond_0
    new-instance p1, Lcom/bytedance/sdk/openadsdk/dislike/b;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-string v2, "interaction"

    invoke-direct {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/dislike/b;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->e:Lcom/bytedance/sdk/openadsdk/dislike/b;

    .line 73
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/c/b;->a()V

    return-void
.end method

.method public getInteractionType()I
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->T()I

    move-result v0

    return v0
.end method

.method public getMediaExtraInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 261
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->as()Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public setAdInteractionListener(Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;)V
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->f:Lcom/bytedance/sdk/openadsdk/TTInteractionAd$AdInteractionListener;

    return-void
.end method

.method public setDownloadListener(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->g:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->g:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    :cond_0
    return-void
.end method

.method public setShowDislikeIcon(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->e:Lcom/bytedance/sdk/openadsdk/dislike/b;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/b;->setDislikeInteractionCallback(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    return-void
.end method

.method public showInteractionAd(Landroid/app/Activity;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 270
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 273
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 276
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/component/c/b;->k:Z

    if-nez p1, :cond_1

    const/4 p1, 0x1

    .line 277
    sput-boolean p1, Lcom/bytedance/sdk/openadsdk/component/c/b;->k:Z

    .line 278
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/c/b;->d:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    :cond_1
    return-void

    .line 274
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "\u4e0d\u80fd\u5728\u5b50\u7ebf\u7a0b\u8c03\u7528 TTInteractionAd.showInteractionAd"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
