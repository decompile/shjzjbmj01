.class public Lcom/bytedance/sdk/openadsdk/component/a/e;
.super Ljava/lang/Object;
.source "TTBannerAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTBannerAd;
.implements Lcom/bytedance/sdk/openadsdk/utils/am$a;


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/component/a/d;

.field private final b:Lcom/bytedance/sdk/openadsdk/component/a/a;

.field private final c:Landroid/content/Context;

.field private d:Lcom/bytedance/sdk/openadsdk/utils/am;

.field private e:I

.field private f:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field private g:Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;

.field private h:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

.field private i:Lcom/bytedance/sdk/openadsdk/dislike/b;

.field private final j:Lcom/bytedance/sdk/openadsdk/component/a/b;

.field private k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

.field private l:Ljava/lang/String;

.field private m:Lcom/bytedance/sdk/openadsdk/AdSlot;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/component/a/a;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 1

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "banner_ad"

    .line 53
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->l:Ljava/lang/String;

    .line 58
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->c:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->b:Lcom/bytedance/sdk/openadsdk/component/a/a;

    .line 60
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->m:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 61
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/component/a/a;->b()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object p3

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->f:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 62
    new-instance p3, Lcom/bytedance/sdk/openadsdk/component/a/d;

    invoke-direct {p3, p1}, Lcom/bytedance/sdk/openadsdk/component/a/d;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    .line 63
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/a/b;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->j:Lcom/bytedance/sdk/openadsdk/component/a/b;

    .line 64
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/a/d;->b()Lcom/bytedance/sdk/openadsdk/component/a/c;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Lcom/bytedance/sdk/openadsdk/component/a/c;Lcom/bytedance/sdk/openadsdk/component/a/a;)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;)Lcom/bytedance/sdk/openadsdk/core/EmptyView;
    .locals 3

    const/4 v0, 0x0

    .line 364
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 365
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 366
    instance-of v2, v1, Lcom/bytedance/sdk/openadsdk/core/EmptyView;

    if-eqz v2, :cond_0

    .line 367
    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/EmptyView;

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;
    .locals 2

    .line 256
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->T()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 257
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->l:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private a()V
    .locals 3

    .line 218
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->j:Lcom/bytedance/sdk/openadsdk/component/a/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->m:Lcom/bytedance/sdk/openadsdk/AdSlot;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/component/a/e$1;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/component/a/e$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/a/e;)V

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/a/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/component/a/b$a;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V
    .locals 1

    .line 193
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    .line 194
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->i:Lcom/bytedance/sdk/openadsdk/dislike/b;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->a(Lcom/bytedance/sdk/openadsdk/dislike/b;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/component/a/a;)V
    .locals 1
    .param p1    # Lcom/bytedance/sdk/openadsdk/component/a/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 250
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->c()Lcom/bytedance/sdk/openadsdk/component/a/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->c()Lcom/bytedance/sdk/openadsdk/component/a/c;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Lcom/bytedance/sdk/openadsdk/component/a/c;Lcom/bytedance/sdk/openadsdk/component/a/a;)V

    :cond_0
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/component/a/c;Lcom/bytedance/sdk/openadsdk/component/a/a;)V
    .locals 5
    .param p1    # Lcom/bytedance/sdk/openadsdk/component/a/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/component/a/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 267
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/component/a/a;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/a/c;->a(Landroid/graphics/Bitmap;)V

    .line 268
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/component/a/a;->b()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object p2

    .line 269
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->f:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 270
    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->f:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->l:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/dislike/b;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->i:Lcom/bytedance/sdk/openadsdk/dislike/b;

    .line 271
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/component/a/c;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    .line 272
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    .line 273
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->b()V

    .line 275
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/a/c;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/a/c;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/a/c;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a(Landroid/app/Activity;)V

    .line 279
    :cond_0
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    .line 280
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Landroid/view/ViewGroup;)Lcom/bytedance/sdk/openadsdk/core/EmptyView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 282
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/EmptyView;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 283
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/a/c;->addView(Landroid/view/View;)V

    .line 286
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz v1, :cond_2

    .line 287
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-interface {v1, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a(Landroid/view/View;)V

    .line 290
    :cond_2
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/a/e$2;

    invoke-direct {v1, p0, p2}, Lcom/bytedance/sdk/openadsdk/component/a/e$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/a/e;Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;->setCallback(Lcom/bytedance/sdk/openadsdk/core/EmptyView$a;)V

    .line 341
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/a/a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->l:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-direct {v1, v2, p2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/a/a;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;I)V

    .line 342
    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/a/a;->a(Landroid/view/View;)V

    .line 343
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/component/a/d;->d()Landroid/view/View;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/bytedance/sdk/openadsdk/core/a/a;->b(Landroid/view/View;)V

    .line 344
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    invoke-virtual {v1, p2}, Lcom/bytedance/sdk/openadsdk/core/a/a;->a(Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;)V

    .line 345
    new-instance p2, Lcom/bytedance/sdk/openadsdk/component/a/e$3;

    invoke-direct {p2, p0}, Lcom/bytedance/sdk/openadsdk/component/a/e$3;-><init>(Lcom/bytedance/sdk/openadsdk/component/a/e;)V

    invoke-virtual {v1, p2}, Lcom/bytedance/sdk/openadsdk/core/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/a/b$a;)V

    .line 354
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/a/c;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 355
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/a/c;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 356
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz p1, :cond_3

    .line 357
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->h:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    :cond_3
    const/4 p1, 0x1

    .line 359
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/EmptyView;->setNeedCheckingShow(Z)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/a/e;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/a/e;Lcom/bytedance/sdk/openadsdk/component/a/a;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Lcom/bytedance/sdk/openadsdk/component/a/a;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    return-object p0
.end method

.method private b()V
    .locals 4

    .line 234
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 236
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    const/4 v1, 0x1

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->e:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/am;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V
    .locals 4

    .line 201
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->i:Lcom/bytedance/sdk/openadsdk/dislike/b;

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Lcom/bytedance/sdk/openadsdk/dislike/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->f:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->l:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/dislike/b;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->i:Lcom/bytedance/sdk/openadsdk/dislike/b;

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->i:Lcom/bytedance/sdk/openadsdk/dislike/b;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/dislike/b;->setDislikeInteractionCallback(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    return-void
.end method

.method private c()V
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/a/e;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->c()V

    return-void
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/component/a/d;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/component/a/e;)Landroid/content/Context;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->c:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/component/a/e;)Ljava/lang/String;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->l:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->g:Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 1

    .line 209
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a()V

    :cond_0
    return-void
.end method

.method public getBannerView()Landroid/view/View;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    return-object v0
.end method

.method public getDislikeDialog(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)Lcom/bytedance/sdk/openadsdk/TTAdDislike;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 148
    :cond_0
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    .line 149
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->i:Lcom/bytedance/sdk/openadsdk/dislike/b;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->a(Lcom/bytedance/sdk/openadsdk/dislike/b;)V

    .line 150
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->i:Lcom/bytedance/sdk/openadsdk/dislike/b;

    return-object p1
.end method

.method public getInteractionType()I
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->f:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->f:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->T()I

    move-result v0

    return v0
.end method

.method public getMediaExtraInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 182
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->f:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->f:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->as()Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public setBannerInteractionListener(Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->g:Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;

    return-void
.end method

.method public setDownloadListener(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V
    .locals 1

    .line 128
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->h:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

    .line 130
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    if-eqz p1, :cond_0

    .line 131
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->k:Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->h:Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;

    invoke-interface {p1, v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    :cond_0
    return-void
.end method

.method public setShowDislikeIcon(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    .line 158
    :cond_0
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    return-void
.end method

.method public setSlideIntervalTime(I)V
    .locals 2

    if-gtz p1, :cond_0

    return-void

    :cond_0
    const-string v0, "slide_banner_ad"

    .line 166
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->l:Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->b()Lcom/bytedance/sdk/openadsdk/component/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->b:Lcom/bytedance/sdk/openadsdk/component/a/a;

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Lcom/bytedance/sdk/openadsdk/component/a/c;Lcom/bytedance/sdk/openadsdk/component/a/a;)V

    .line 169
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/a/d;->a()V

    .line 170
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->a:Lcom/bytedance/sdk/openadsdk/component/a/d;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/a/d;->a(I)V

    const v0, 0x1d4c0

    const/16 v1, 0x7530

    if-ge p1, v1, :cond_1

    const/16 p1, 0x7530

    goto :goto_0

    :cond_1
    if-le p1, v0, :cond_2

    const p1, 0x1d4c0

    .line 176
    :cond_2
    :goto_0
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->e:I

    .line 177
    new-instance p1, Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Lcom/bytedance/sdk/openadsdk/utils/am;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/openadsdk/utils/am$a;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    return-void
.end method
