.class Lcom/bytedance/sdk/openadsdk/component/a/e$2;
.super Ljava/lang/Object;
.source "TTBannerAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/EmptyView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Lcom/bytedance/sdk/openadsdk/component/a/c;Lcom/bytedance/sdk/openadsdk/component/a/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/component/a/e;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/a/e;Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->a:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 315
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->a()V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 4

    .line 330
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Lcom/bytedance/sdk/openadsdk/component/a/e;)V

    const-string v0, "TTBannerAd"

    const-string v1, "BANNER SHOW"

    .line 331
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "AdEvent"

    .line 332
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pangolin ad show "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->a:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {v2, p1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->e(Lcom/bytedance/sdk/openadsdk/component/a/e;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->a:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/a/e;->f(Lcom/bytedance/sdk/openadsdk/component/a/e;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/util/Map;)V

    .line 334
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->g(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->g(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->a:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->T()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/TTBannerAd$AdInteractionListener;->onAdShow(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 295
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->b()V

    goto :goto_0

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->c()V

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    .line 305
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/a/e;->a(Lcom/bytedance/sdk/openadsdk/component/a/e;)V

    const-string p1, "TTBannerAd"

    const-string v0, "\u83b7\u5f97\u7126\u70b9\uff0c\u5f00\u59cb\u8ba1\u65f6"

    .line 306
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string p1, "TTBannerAd"

    const-string v0, "\u5931\u53bb\u7126\u70b9\uff0c\u505c\u6b62\u8ba1\u65f6"

    .line 308
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/a/e;->c(Lcom/bytedance/sdk/openadsdk/component/a/e;)V

    :goto_1
    return-void
.end method

.method public b()V
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->c(Lcom/bytedance/sdk/openadsdk/component/a/e;)V

    .line 323
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/a/e$2;->b:Lcom/bytedance/sdk/openadsdk/component/a/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/a/e;->b(Lcom/bytedance/sdk/openadsdk/component/a/e;)Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/core/a;->d()V

    :cond_0
    return-void
.end method
