.class Lcom/bytedance/sdk/openadsdk/component/b/c$3;
.super Ljava/lang/Object;
.source "TTFeedAdImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/b/c;->getCustomVideo()Lcom/bytedance/sdk/openadsdk/TTFeedAd$CustomizeVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/b/c;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/b/c;)V
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getVideoUrl()Ljava/lang/String;
    .locals 2

    .line 206
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->c(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    .line 207
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->d(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    .line 208
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->e(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->f(Lcom/bytedance/sdk/openadsdk/component/b/c;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->a(Lcom/bytedance/sdk/openadsdk/component/b/c;Z)Z

    .line 214
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->g(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public reportVideoAutoStart()V
    .locals 9

    .line 253
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->w(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->x(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->y(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "feed_auto_play"

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    return-void
.end method

.method public reportVideoBreak(J)V
    .locals 9

    .line 246
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->t(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->u(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->v(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "feed_break"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    .line 247
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide v5

    double-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long v5, v5, v7

    invoke-static {p1, p2, v5, v6}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v7

    const/4 v8, 0x0

    move-wide v5, p1

    .line 246
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    return-void
.end method

.method public reportVideoContinue(J)V
    .locals 9

    .line 232
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->n(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->o(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->p(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "feed_continue"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    .line 233
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide v5

    double-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long v5, v5, v7

    invoke-static {p1, p2, v5, v6}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v7

    const/4 v8, 0x0

    move-wide v5, p1

    .line 232
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    return-void
.end method

.method public reportVideoError(JII)V
    .locals 5

    .line 273
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "creative_id"

    .line 274
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/b/c;->E(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "error_code"

    .line 275
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p3, "extra_error_code"

    .line 276
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-interface {v0, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/component/b/c;->F(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object p3

    if-eqz p3, :cond_0

    const-string p4, "video_size"

    .line 279
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/d/u;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p4, "video_resolution"

    .line 280
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/d/u;->f()Ljava/lang/String;

    move-result-object p3

    invoke-interface {v0, p4, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string p3, "duration"

    .line 282
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    invoke-interface {v0, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p3, "percent"

    .line 283
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide v1

    double-to-long v1, v1

    const-wide/16 v3, 0x3e8

    mul-long v1, v1, v3

    invoke-static {p1, p2, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->G(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/b/c;->H(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object p2

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/component/b/c;->I(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object p3

    invoke-static {p3}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object p3

    const-string p4, "play_error"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/bytedance/sdk/openadsdk/c/d;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public reportVideoFinish()V
    .locals 9

    .line 239
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->q(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->r(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->s(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "feed_over"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    .line 240
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide v5

    double-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long v5, v5, v7

    const/16 v7, 0x64

    const/4 v8, 0x0

    .line 239
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    return-void
.end method

.method public reportVideoPause(J)V
    .locals 9

    .line 226
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->k(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->l(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->m(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "feed_pause"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    .line 227
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->getVideoDuration()D

    move-result-wide v5

    double-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long v5, v5, v7

    invoke-static {p1, p2, v5, v6}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v7

    const/4 v8, 0x0

    move-wide v5, p1

    .line 226
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    return-void
.end method

.method public reportVideoStart()V
    .locals 9

    .line 219
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->h(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->i(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/b/c;->j(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "feed_play"

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    return-void
.end method

.method public reportVideoStartError(II)V
    .locals 3

    .line 259
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "creative_id"

    .line 260
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/b/c;->z(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "error_code"

    .line 261
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "extra_error_code"

    .line 262
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->A(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string p2, "video_size"

    .line 265
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/u;->d()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "video_resolution"

    .line 266
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/u;->f()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->B(Lcom/bytedance/sdk/openadsdk/component/b/c;)Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/b/c;->C(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object p2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/b/c$3;->a:Lcom/bytedance/sdk/openadsdk/component/b/c;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/b/c;->D(Lcom/bytedance/sdk/openadsdk/component/b/c;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "play_start_error"

    invoke-static {p1, p2, v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/c/d;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method
