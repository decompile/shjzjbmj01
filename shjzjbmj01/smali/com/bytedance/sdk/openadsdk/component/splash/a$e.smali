.class Lcom/bytedance/sdk/openadsdk/component/splash/a$e;
.super Lcom/bytedance/sdk/openadsdk/l/g;
.source "SplashAdCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/component/splash/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/component/splash/a;Ljava/lang/String;)V
    .locals 0

    .line 552
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    const-string p1, "ReadCacheTask"

    .line 553
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/g;-><init>(Ljava/lang/String;)V

    .line 554
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/a$e;Ljava/lang/String;)V
    .locals 0

    .line 549
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .line 558
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/io/File;)[B
    .locals 5

    .line 612
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x400

    .line 614
    new-array v1, v1, [B

    const/4 v2, 0x0

    .line 617
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 618
    :goto_0
    :try_start_1
    array-length p1, v1

    const/4 v2, 0x0

    invoke-virtual {v3, v1, v2, p1}, Ljava/io/FileInputStream;->read([BII)I

    move-result p1

    const/4 v4, -0x1

    if-eq p1, v4, :cond_0

    .line 619
    invoke-virtual {v0, v1, v2, p1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 621
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 627
    :try_start_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 630
    :goto_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_3

    :catchall_0
    move-exception p1

    goto :goto_2

    :catchall_1
    move-exception p1

    move-object v3, v2

    .line 627
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    if-eqz v3, :cond_1

    .line 630
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 634
    :catch_0
    :cond_1
    throw p1

    :catch_1
    move-object v3, v2

    .line 627
    :catch_2
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 635
    :catch_3
    :cond_2
    :goto_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public run()V
    .locals 12

    .line 563
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/openadsdk/utils/am;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    .line 564
    iput v1, v0, Landroid/os/Message;->what:I

    .line 566
    :try_start_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/d/a;

    move-result-object v2

    .line 567
    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/d/p;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4, v4}, Lcom/bytedance/sdk/openadsdk/core/d/p;-><init>(Lcom/bytedance/sdk/openadsdk/core/d/a;Lcom/bytedance/sdk/openadsdk/core/d/l;[B)V

    if-eqz v2, :cond_2

    .line 568
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 569
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 570
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v5, :cond_2

    .line 571
    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aB()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 574
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/a/a;->a()Lcom/bytedance/sdk/openadsdk/i/a/a;

    move-result-object v6

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v7, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;Lcom/bytedance/sdk/openadsdk/core/d/a;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v8, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Lcom/bytedance/sdk/openadsdk/component/splash/a;Lcom/bytedance/sdk/openadsdk/core/d/a;)I

    move-result v8

    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    .line 575
    invoke-static {v9, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Lcom/bytedance/sdk/openadsdk/component/splash/a;Lcom/bytedance/sdk/openadsdk/core/d/a;)I

    move-result v2

    sget-object v9, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    .line 574
    invoke-virtual {v6, v7, v8, v2, v9}, Lcom/bytedance/sdk/openadsdk/i/a/a;->a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;)Ljava/lang/String;

    move-result-object v2

    .line 576
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/a/a;->a()Lcom/bytedance/sdk/openadsdk/i/a/a;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/a/a;->b()Ljava/lang/String;

    move-result-object v6

    const-string v7, "splashLoadAd"

    .line 577
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " readSplashAdFromCache \u5f00\u59cb\u83b7\u53d6\u7f13\u5b58\u6587\u4ef6 filePath "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "splashLoadAd"

    .line 579
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " readSplashAdFromCache path "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v7}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-gtz v6, :cond_0

    goto :goto_0

    .line 586
    :cond_0
    sput v1, Lcom/bytedance/sdk/openadsdk/i/a/b;->a:I

    const-string v1, "splashLoadAd"

    .line 587
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " readSplashAdFromCache \u83b7\u53d6\u6587\u4ef6\u6210\u529f cacheKey "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    invoke-direct {p0, v7}, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a(Ljava/io/File;)[B

    move-result-object v1

    if-eqz v1, :cond_2

    .line 589
    array-length v2, v1

    if-eqz v2, :cond_2

    .line 590
    invoke-virtual {v3, v5}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    .line 591
    invoke-virtual {v3, v1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a([B)V

    goto :goto_1

    :cond_1
    :goto_0
    const-string v1, "splashLoadAd"

    .line 582
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " readSplashAdFromCache \u83b7\u53d6\u6587\u4ef6\u5931\u8d25 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 600
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/openadsdk/utils/am;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->sendMessage(Landroid/os/Message;)Z

    return-void

    .line 596
    :cond_2
    :goto_1
    :try_start_1
    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    .line 600
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/openadsdk/utils/am;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->sendMessage(Landroid/os/Message;)Z

    .line 601
    throw v1

    .line 600
    :catch_0
    :goto_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/component/splash/a;)Lcom/bytedance/sdk/openadsdk/utils/am;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->sendMessage(Landroid/os/Message;)Z

    :try_start_2
    const-string v0, "splashLoadAd"

    .line 605
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u83b7\u53d6\u7f13\u5b58\u5e7f\u544a\u4e4b\u540e\u5c06\u5176\u6e05\u7a7a clearCache "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->a:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/a$e;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    return-void
.end method
