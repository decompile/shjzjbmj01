.class Lcom/bytedance/sdk/openadsdk/component/splash/b$2;
.super Ljava/lang/Object;
.source "SplashAdLoadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/component/splash/a$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/component/splash/b;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;I)V
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->a:Ljava/lang/String;

    iput p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 381
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/p;)V
    .locals 13
    .param p1    # Lcom/bytedance/sdk/openadsdk/core/d/p;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 341
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/l;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 342
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    .line 343
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 344
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/component/splash/a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string p1, "splashLoadAd"

    const-string v0, "tryDisplaySplashAdFromCache onSuccess videoPath == null tryDisplaySplashImageFromCache"

    .line 346
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v1, "splashLoadAd"

    .line 350
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tryDisplaySplashAdFromCache onSuccess videoPath "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v2, 0x4

    invoke-static {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v1

    .line 353
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2, p1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/p;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/component/splash/e;

    move-result-object v10

    .line 354
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->b:I

    const/16 v3, 0x3a98

    const/4 v4, 0x1

    if-ne v2, v3, :cond_1

    .line 355
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result v5

    xor-int/2addr v5, v4

    invoke-virtual {v2, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 357
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result v5

    xor-int/2addr v5, v4

    invoke-virtual {v2, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 359
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->b:I

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 360
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2, p1, v10, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    goto :goto_0

    .line 362
    :cond_2
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-string v2, "splashLoadAd"

    .line 363
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u4e0d\u68c0\u6d4b\u76f4\u63a5\u8fd4\u56de\u7f13\u5b58....splashAd="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "splashLoadAd"

    const-string v3, "tryDisplaySplashAdFromCache onSuccess \u52a0\u8f7d\u89c6\u9891\u7f13\u5b58"

    .line 364
    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget v11, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->b:I

    new-instance v12, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, v12

    move-object v8, v10

    move-object v9, v1

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    move v3, v11

    move-object v4, v12

    move-object v5, p1

    move-object v6, v10

    move-object v7, v0

    invoke-static/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 368
    :goto_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 369
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget v7, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->b:I

    move-object v3, p1

    move-object v4, v10

    move-object v5, v0

    move-object v6, v1

    invoke-static/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;I)V

    goto :goto_1

    :cond_3
    const-string p1, "splashLoadAd"

    const-string v0, "tryDisplaySplashAdFromCache videoPath == null tryDisplaySplashImageFromCache"

    .line 373
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->c:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;)V

    :cond_4
    :goto_1
    return-void
.end method
