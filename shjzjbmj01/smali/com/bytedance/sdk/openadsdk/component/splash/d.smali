.class public Lcom/bytedance/sdk/openadsdk/component/splash/d;
.super Lcom/bytedance/sdk/openadsdk/core/video/c/a;
.source "SplashVideoController.java"


# instance fields
.field private j:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    const/4 p1, 0x0

    .line 48
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->j:Z

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected a(II)V
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/16 v0, 0x134

    if-ne p1, v0, :cond_1

    return-void

    .line 38
    :cond_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->h:Z

    if-eqz v0, :cond_2

    const-string v0, "play_error"

    goto :goto_0

    :cond_2
    const-string v0, "play_start_error"

    .line 39
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v2

    invoke-static {v1, p1, p2, v2}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;IILcom/bytedance/sdk/openadsdk/core/video/d/d;)Ljava/util/Map;

    move-result-object p1

    .line 40
    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->h:Z

    if-eqz p2, :cond_3

    const-string p2, "duration"

    .line 41
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->p()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "percent"

    .line 42
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->r()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "buffers_time"

    .line 43
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->o()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    :cond_3
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-string v2, "splash_ad"

    invoke-static {p2, v1, v2, v0, p1}, Lcom/bytedance/sdk/openadsdk/c/d;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method protected b()V
    .locals 9

    .line 52
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 53
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->j:Z

    .line 54
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-string v3, "splash_ad"

    const-string v4, "feed_over"

    iget-wide v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->c:J

    const/16 v7, 0x64

    .line 55
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->C()Ljava/util/Map;

    move-result-object v8

    .line 54
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    :cond_0
    return-void
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected e()V
    .locals 5

    .line 73
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-string v2, "splash_ad"

    const-string v3, "feed_play"

    .line 74
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->B()Ljava/util/Map;

    move-result-object v4

    .line 73
    invoke-static {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/c/d;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method protected f()V
    .locals 5

    .line 79
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/d;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-string v2, "splash_ad"

    const-string v3, "feed_play"

    .line 80
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/d;->B()Ljava/util/Map;

    move-result-object v4

    .line 79
    invoke-static {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/c/d;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method
