.class public final Lcom/bytedance/sdk/openadsdk/component/splash/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/component/splash/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final tt_app_detail_bg:I

.field public static final tt_app_detail_line_bg:I

.field public static final tt_app_detail_privacy_text_bg:I

.field public static final tt_app_detail_stroke_bg:I

.field public static final tt_appdownloader_notification_material_background_color:I

.field public static final tt_appdownloader_notification_title_color:I

.field public static final tt_appdownloader_s1:I

.field public static final tt_appdownloader_s13:I

.field public static final tt_appdownloader_s18:I

.field public static final tt_appdownloader_s4:I

.field public static final tt_appdownloader_s8:I

.field public static final tt_cancle_bg:I

.field public static final tt_common_download_bg:I

.field public static final tt_common_download_btn_bg:I

.field public static final tt_dislike_dialog_background:I

.field public static final tt_dislike_transparent:I

.field public static final tt_divider:I

.field public static final tt_download_app_name:I

.field public static final tt_download_bar_background:I

.field public static final tt_download_bar_background_new:I

.field public static final tt_download_text_background:I

.field public static final tt_draw_btn_back:I

.field public static final tt_full_screen_skip_bg:I

.field public static final tt_header_font:I

.field public static final tt_heise3:I

.field public static final tt_listview:I

.field public static final tt_listview_press:I

.field public static final tt_rating_comment:I

.field public static final tt_rating_comment_vertical:I

.field public static final tt_rating_star:I

.field public static final tt_skip_red:I

.field public static final tt_ssxinbaise4:I

.field public static final tt_ssxinbaise4_press:I

.field public static final tt_ssxinheihui3:I

.field public static final tt_ssxinhongse1:I

.field public static final tt_ssxinmian1:I

.field public static final tt_ssxinmian11:I

.field public static final tt_ssxinmian15:I

.field public static final tt_ssxinmian6:I

.field public static final tt_ssxinmian7:I

.field public static final tt_ssxinmian8:I

.field public static final tt_ssxinxian11:I

.field public static final tt_ssxinxian11_selected:I

.field public static final tt_ssxinxian3:I

.field public static final tt_ssxinxian3_press:I

.field public static final tt_ssxinzi12:I

.field public static final tt_ssxinzi15:I

.field public static final tt_ssxinzi4:I

.field public static final tt_ssxinzi9:I

.field public static final tt_text_font:I

.field public static final tt_titlebar_background_dark:I

.field public static final tt_titlebar_background_ffffff:I

.field public static final tt_titlebar_background_light:I

.field public static final tt_trans_black:I

.field public static final tt_trans_half_black:I

.field public static final tt_transparent:I

.field public static final tt_video_player_text:I

.field public static final tt_video_player_text_withoutnight:I

.field public static final tt_video_playerbg_color:I

.field public static final tt_video_shadow_color:I

.field public static final tt_video_shaoow_color_fullscreen:I

.field public static final tt_video_time_color:I

.field public static final tt_video_traffic_tip_background_color:I

.field public static final tt_video_transparent:I

.field public static final tt_white:I

.field public static final ttdownloader_transparent:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_app_detail_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_app_detail_bg:I

    .line 10
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_app_detail_line_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_app_detail_line_bg:I

    .line 11
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_app_detail_privacy_text_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_app_detail_privacy_text_bg:I

    .line 12
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_app_detail_stroke_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_app_detail_stroke_bg:I

    .line 13
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_appdownloader_notification_material_background_color:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_appdownloader_notification_material_background_color:I

    .line 14
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_appdownloader_notification_title_color:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_appdownloader_notification_title_color:I

    .line 15
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_appdownloader_s1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_appdownloader_s1:I

    .line 16
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_appdownloader_s13:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_appdownloader_s13:I

    .line 17
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_appdownloader_s18:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_appdownloader_s18:I

    .line 18
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_appdownloader_s4:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_appdownloader_s4:I

    .line 19
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_appdownloader_s8:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_appdownloader_s8:I

    .line 20
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_cancle_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_cancle_bg:I

    .line 21
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_common_download_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_common_download_bg:I

    .line 22
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_common_download_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_common_download_btn_bg:I

    .line 23
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_dislike_dialog_background:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_dislike_dialog_background:I

    .line 24
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_dislike_transparent:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_dislike_transparent:I

    .line 25
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_divider:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_divider:I

    .line 26
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_download_app_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_download_app_name:I

    .line 27
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_download_bar_background:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_download_bar_background:I

    .line 28
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_download_bar_background_new:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_download_bar_background_new:I

    .line 29
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_download_text_background:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_download_text_background:I

    .line 30
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_draw_btn_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_draw_btn_back:I

    .line 31
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_full_screen_skip_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_full_screen_skip_bg:I

    .line 32
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_header_font:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_header_font:I

    .line 33
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_heise3:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_heise3:I

    .line 34
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_listview:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_listview:I

    .line 35
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_listview_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_listview_press:I

    .line 36
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_rating_comment:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_rating_comment:I

    .line 37
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_rating_comment_vertical:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_rating_comment_vertical:I

    .line 38
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_rating_star:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_rating_star:I

    .line 39
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_skip_red:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_skip_red:I

    .line 40
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinbaise4:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinbaise4:I

    .line 41
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinbaise4_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinbaise4_press:I

    .line 42
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinheihui3:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinheihui3:I

    .line 43
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinhongse1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinhongse1:I

    .line 44
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinmian1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinmian1:I

    .line 45
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinmian11:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinmian11:I

    .line 46
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinmian15:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinmian15:I

    .line 47
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinmian6:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinmian6:I

    .line 48
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinmian7:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinmian7:I

    .line 49
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinmian8:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinmian8:I

    .line 50
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinxian11:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinxian11:I

    .line 51
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinxian11_selected:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinxian11_selected:I

    .line 52
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinxian3:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinxian3:I

    .line 53
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinxian3_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinxian3_press:I

    .line 54
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinzi12:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinzi12:I

    .line 55
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinzi15:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinzi15:I

    .line 56
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinzi4:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinzi4:I

    .line 57
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_ssxinzi9:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_ssxinzi9:I

    .line 58
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_text_font:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_text_font:I

    .line 59
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_titlebar_background_dark:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_titlebar_background_dark:I

    .line 60
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_titlebar_background_ffffff:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_titlebar_background_ffffff:I

    .line 61
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_titlebar_background_light:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_titlebar_background_light:I

    .line 62
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_trans_black:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_trans_black:I

    .line 63
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_trans_half_black:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_trans_half_black:I

    .line 64
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_transparent:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_transparent:I

    .line 65
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_video_player_text:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_video_player_text:I

    .line 66
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_video_player_text_withoutnight:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_video_player_text_withoutnight:I

    .line 67
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_video_playerbg_color:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_video_playerbg_color:I

    .line 68
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_video_shadow_color:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_video_shadow_color:I

    .line 69
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_video_shaoow_color_fullscreen:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_video_shaoow_color_fullscreen:I

    .line 70
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_video_time_color:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_video_time_color:I

    .line 71
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_video_traffic_tip_background_color:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_video_traffic_tip_background_color:I

    .line 72
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_video_transparent:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_video_transparent:I

    .line 73
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->tt_white:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->tt_white:I

    .line 74
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$color;->ttdownloader_transparent:I

    sput v0, Lcom/bytedance/sdk/openadsdk/component/splash/R$color;->ttdownloader_transparent:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
