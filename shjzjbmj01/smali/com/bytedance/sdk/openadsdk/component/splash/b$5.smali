.class Lcom/bytedance/sdk/openadsdk/component/splash/b$5;
.super Ljava/lang/Object;
.source "SplashAdLoadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/utils/o$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/h/a/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/d/a;

.field final synthetic b:Z

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/h/a/c;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lcom/bytedance/sdk/openadsdk/component/splash/b;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/a;ZLcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/h/a/c;Ljava/lang/String;)V
    .locals 0

    .line 616
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->a:Lcom/bytedance/sdk/openadsdk/core/d/a;

    iput-boolean p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->b:Z

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->d:Lcom/bytedance/sdk/openadsdk/h/a/c;

    iput-object p6, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 11
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 686
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->a:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/d/a;)V

    .line 687
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;I)I

    .line 689
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->d:Lcom/bytedance/sdk/openadsdk/h/a/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/h/a/c;Ljava/lang/String;)V

    .line 692
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->d:Lcom/bytedance/sdk/openadsdk/h/a/c;

    const/4 v1, -0x7

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(I)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    .line 693
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    const-string v0, "splashLoadAd"

    .line 694
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "splashLoad----5-LoadImageBytes-onFailed-code="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ",msg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->d:Lcom/bytedance/sdk/openadsdk/h/a/c;

    const/4 v6, 0x2

    const/4 v9, 0x0

    const/4 v7, -0x7

    move-object v4, v0

    invoke-direct/range {v4 .. v10}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    const/16 v4, 0x3a98

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v5, v0

    invoke-static/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 698
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->b:Z

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J

    move-result-wide v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-wide/16 v6, -0x7

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(JZZLcom/bytedance/sdk/openadsdk/core/d/l;JLcom/bytedance/sdk/adnet/core/m;)V

    :cond_0
    const-string v0, "SplashAdLoadManager"

    const-string v1, "\u56fe\u7247\u52a0\u8f7d\u5931\u8d25"

    .line 702
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/i/a/d;)V
    .locals 10
    .param p1    # Lcom/bytedance/sdk/openadsdk/i/a/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 622
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/i/a/d;)V

    const-string v0, "splashLoadAd"

    const-string v1, "\u56fe\u7247\u52a0\u8f7d\u6210\u529f"

    .line 624
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->a:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/d/a;)V

    .line 626
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/core/d/q;

    move-result-object v0

    sget v1, Lcom/bytedance/sdk/openadsdk/i/a/b;->a:I

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->d(I)V

    .line 628
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->b:Z

    if-nez v0, :cond_0

    .line 629
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J

    .line 630
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5$1;

    const-string v1, "preLoadImage"

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b$5$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b$5;Ljava/lang/String;)V

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Lcom/bytedance/sdk/openadsdk/l/g;I)V

    .line 639
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/i/a/d;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 641
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->d(Z)V

    .line 642
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/e;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v4

    const-string v5, "splash_ad"

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/component/splash/e;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/AdSlot;Ljava/lang/String;)V

    .line 643
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/e;

    .line 644
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(Lcom/bytedance/sdk/openadsdk/i/a/d;)V

    .line 647
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object p1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->d()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 649
    new-instance p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->d:Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-object v2, p1

    move-object v7, v0

    invoke-direct/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    .line 651
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/16 v3, 0x3a98

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v4, p1

    move-object v6, v0

    invoke-static/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 653
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 654
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->d:Lcom/bytedance/sdk/openadsdk/h/a/c;

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    .line 658
    :cond_2
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->b:Z

    if-eqz p1, :cond_5

    .line 659
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(JZZLcom/bytedance/sdk/openadsdk/core/d/l;JLcom/bytedance/sdk/adnet/core/m;)V

    goto :goto_0

    .line 663
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;I)I

    .line 665
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->d:Lcom/bytedance/sdk/openadsdk/h/a/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->e:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/h/a/c;Ljava/lang/String;)V

    const/4 p1, -0x7

    .line 667
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->d:Lcom/bytedance/sdk/openadsdk/h/a/c;

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(I)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v1

    .line 668
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 669
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/4 v5, 0x2

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->d:Lcom/bytedance/sdk/openadsdk/h/a/c;

    const/4 v6, -0x7

    move-object v3, v1

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    const-string v2, "splashLoadAd"

    const-string v3, "preLoadImage  data == null REQUEST_TYPE_REAL_NETWORK"

    .line 670
    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v2, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;I)I

    .line 672
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    const/16 v4, 0x3a98

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v5, v1

    invoke-static/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 674
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->b:Z

    if-eqz v0, :cond_4

    .line 675
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J

    move-result-wide v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->c:Lcom/bytedance/sdk/openadsdk/core/d/l;

    int-to-long v6, p1

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(JZZLcom/bytedance/sdk/openadsdk/core/d/l;JLcom/bytedance/sdk/adnet/core/m;)V

    :cond_4
    const-string p1, "SplashAdLoadManager"

    const-string v0, "\u56fe\u7247\u52a0\u8f7d\u5931\u8d25"

    .line 678
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public b()V
    .locals 1

    .line 707
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;->f:Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o(Lcom/bytedance/sdk/openadsdk/component/splash/b;)V

    return-void
.end method
