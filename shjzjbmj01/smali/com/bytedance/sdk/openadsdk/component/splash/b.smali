.class public Lcom/bytedance/sdk/openadsdk/component/splash/b;
.super Ljava/lang/Object;
.source "SplashAdLoadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/utils/am$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/component/splash/b$a;
    }
.end annotation


# static fields
.field private static j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/bytedance/sdk/openadsdk/component/splash/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field private B:I

.field private C:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private D:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private E:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

.field private a:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field private b:Lcom/bytedance/sdk/openadsdk/core/d/m;

.field private c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

.field private final d:Lcom/bytedance/sdk/openadsdk/core/q;

.field private e:Landroid/content/Context;

.field private final f:Lcom/bytedance/sdk/openadsdk/utils/am;

.field private final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

.field private i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

.field private k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

.field private l:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private m:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private n:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private o:Z

.field private p:J

.field private q:J

.field private r:J

.field private final s:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final t:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private u:Lcom/bytedance/sdk/openadsdk/core/d/a;

.field private v:Lcom/bytedance/sdk/openadsdk/core/d/a;

.field private w:Lcom/bytedance/sdk/openadsdk/h/a/c;

.field private x:I

.field private y:Lcom/bytedance/sdk/openadsdk/core/d/q;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 99
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 87
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    .line 93
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 106
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 107
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 108
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-wide/16 v2, 0x0

    .line 116
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->r:J

    .line 119
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 120
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 137
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->x:I

    .line 142
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->z:Z

    const/4 v0, 0x4

    .line 147
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->B:I

    .line 986
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 988
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz p1, :cond_0

    .line 154
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    .line 156
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->f()Lcom/bytedance/sdk/openadsdk/core/q;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d:Lcom/bytedance/sdk/openadsdk/core/q;

    .line 157
    new-instance p1, Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Lcom/bytedance/sdk/openadsdk/utils/am;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/openadsdk/utils/am$a;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/openadsdk/utils/am;

    .line 158
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p1

    :goto_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/a;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    .line 159
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->t()Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Z

    .line 160
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 161
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;I)I
    .locals 0

    .line 69
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->B:I

    return p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;J)J
    .locals 0

    .line 69
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->p:J

    return-wide p1
.end method

.method public static a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/b;
    .locals 1

    .line 149
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/b;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/component/splash/e;)Lcom/bytedance/sdk/openadsdk/component/splash/e;
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/p;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/component/splash/e;
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/p;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/component/splash/e;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/d/p;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/component/splash/e;
    .locals 8

    .line 1323
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/d/p;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    if-eqz p2, :cond_1

    .line 1326
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->d(Z)V

    .line 1329
    :cond_1
    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/e;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v4

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    const-string v7, "splash_ad"

    move-object v2, v0

    move-object v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/e;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/AdSlot;Ljava/lang/String;)V

    .line 1330
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/a;)Lcom/bytedance/sdk/openadsdk/core/d/a;
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->v:Lcom/bytedance/sdk/openadsdk/core/d/a;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/l;)Lcom/bytedance/sdk/openadsdk/core/d/l;
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/d/l;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/core/d/m;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    return-object p0
.end method

.method private a(ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 404
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 405
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v1

    .line 406
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object p2

    .line 407
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 409
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "req_id"

    .line 410
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    goto :goto_0

    :cond_0
    move-object p2, v0

    move-object v1, p2

    :catch_0
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 417
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/d/m;->a:Ljava/lang/String;

    .line 420
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v2

    .line 421
    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    .line 422
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(I)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 423
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    if-eqz v1, :cond_3

    .line 426
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    :cond_3
    if-eqz p2, :cond_4

    .line 430
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    :cond_4
    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p0

    return-object p0
.end method

.method private a()V
    .locals 3

    .line 231
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getBidAdm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/d/m;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private a(I)V
    .locals 1

    .line 1169
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/16 v0, 0x3a98

    if-ne p1, v0, :cond_1

    .line 1173
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1175
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    const-string v0, "real_time_ad"

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1178
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    const-string v0, "cache_ad"

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private declared-synchronized a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V
    .locals 8

    monitor-enter p0

    const/16 v0, 0x3a9c

    const/16 v1, 0x3a9b

    const/16 v2, 0x3a99

    const/4 v3, 0x1

    const/16 v4, 0x3a98

    if-ne p1, v4, :cond_0

    :try_start_0
    const-string v5, "splashLoadAd"

    .line 1004
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "splashAdTryCallback start....\u5b9e\u65f6\u8bf7\u6c42\u6765\u4e86\uff01="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :catchall_0
    move-exception p1

    goto/16 :goto_4

    :cond_0
    if-ne p1, v2, :cond_1

    const-string v5, "splashLoadAd"

    .line 1007
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "splashAdTryCallback start....\u7f13\u5b58\u8bf7\u6c42\u6765\u4e86\uff01="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :cond_1
    if-ne p1, v0, :cond_2

    const-string v5, "splashLoadAd"

    .line 1010
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "splashAdTryCallback start....\u8d85\u65f6\u8bf7\u6c42\u6765\u4e86\uff01="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x2

    .line 1011
    iput v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->B:I

    goto :goto_0

    :cond_2
    if-ne p1, v1, :cond_3

    const-string v5, "splashLoadAd"

    .line 1014
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "splashAdTryCallback start....\u6e32\u67d3\u8d85\u65f6\u8bf7\u6c42\u6765\u4e86\uff01="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    :cond_3
    :goto_0
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback mSplashAdHasLoad==true \u5df2\u6210\u529f\u56de\u8c03\uff0c\u4e0d\u518d\u6267\u884c\u56de\u8c03\u64cd\u4f5c\uff01\uff01\uff01"

    .line 1018
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1019
    monitor-exit p0

    return-void

    :cond_4
    if-nez p4, :cond_11

    .line 1024
    :try_start_1
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_6

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_5

    goto :goto_1

    :cond_5
    const-string p3, "splashLoadAd"

    const-string p4, "\u666e\u901a\u7c7b\u578b\u8d70\u8fd9\uff0c\u76f4\u63a5\u5931\u8d25\u56de\u8c03"

    .line 1084
    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    :goto_1
    if-ne p1, v4, :cond_a

    .line 1028
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_7

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback \u5b9e\u65f6\u8bf7\u6c42\u5931\u8d25\uff08\u5b9e\u65f6\u5148\u56de\uff0c\u7f13\u5b58\u8fd8\u6ca1\u56de\uff09...\u7b49\u5f85\u7f13\u5b58"

    .line 1029
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1030
    monitor-exit p0

    return-void

    :cond_7
    :try_start_2
    const-string p3, "splashLoadAd"

    const-string p4, "splashAdTryCallback \u5b9e\u65f6\u8bf7\u6c42\u5931\u8d25\uff08\u7f13\u5b58\u5148\u56de\uff0c\u5b9e\u65f6\u540e\u56de\uff09...."

    .line 1032
    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_9

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_8

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_9

    :cond_8
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->E:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    if-eqz p3, :cond_9

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback \u5b9e\u65f6\u8bf7\u6c42\u5931\u8d25\uff08\u7f13\u5b58\u5148\u56de\uff0c\u5b9e\u65f6\u540e\u56de\uff09....\u5c1d\u8bd5\u56de\u8c03\u7f13\u5b58\u6210\u529f\uff01"

    .line 1035
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    const-string p2, "cache_ad"

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(Ljava/lang/String;)V

    .line 1038
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->E:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1039
    monitor-exit p0

    return-void

    .line 1041
    :cond_9
    :try_start_3
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_10

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_10

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback \u5f53\u4e3a\u6a21\u7248\u5e7f\u544a\uff0c\u5b58\u5728\u7f13\u5b58\u7684\u60c5\u51b5\u4e0b\uff0c\u6a21\u7248\u6e32\u67d3\u8fd8\u6ca1\u6709\u56de\u6765\u65f6\u7b49\u5f85\u6a21\u7248\u5e7f\u544a\u6e32\u67d3\u56de\u6765\uff01"

    .line 1043
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1044
    monitor-exit p0

    return-void

    :cond_a
    if-ne p1, v2, :cond_c

    .line 1052
    :try_start_4
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_b

    const-string p1, "splashLoadAd"

    const-string p2, "splashAdTryCallback \u7f13\u5b58\u8bf7\u6c42\u5931\u8d25(\u7f13\u5b58\u5148\u56de\u6765)\uff0c\u5b9e\u65f6\u8fd8\u6ca1\u6709\u56de\u8c03....\u7b49\u5f85...\uff01"

    .line 1053
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1054
    monitor-exit p0

    return-void

    :cond_b
    :try_start_5
    const-string p3, "splashLoadAd"

    const-string p4, "splashAdTryCallback \u7f13\u5b58\u8bf7\u6c42\u5931\u8d25(\u7f13\u5b58\u5148\u56de\u6765)\uff0c\u5b9e\u65f6\u4e5f\u5931\u8d25....\u76f4\u63a5\u56de\u8c03\u51fa\u53bb\uff01"

    .line 1056
    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1060
    :cond_c
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_10

    if-ne p1, v0, :cond_10

    const-string p3, "splashLoadAd"

    const-string p4, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0cREQUEST_TYPE_TIMEOUT--->>>>"

    .line 1061
    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_f

    .line 1064
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_10

    const-string p3, "splashLoadAd"

    .line 1065
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0c\u5b9e\u65f6\u662f\u5426\u6e32\u67d3\u6210\u529f "

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, " \u7f13\u5b58\u6e32\u67d3\u662f\u5426\u6210\u529f "

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_e

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_d

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_e

    :cond_d
    const-string p1, "splashLoadAd"

    const-string p2, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0c\u7f13\u5b58\u6210\u529f\u76f4\u63a5\u56de\u8c03"

    .line 1067
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    const-string p2, "cache_ad"

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(Ljava/lang/String;)V

    .line 1069
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->E:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1070
    monitor-exit p0

    return-void

    :cond_e
    :try_start_6
    const-string p3, "splashLoadAd"

    const-string p4, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0c\u76f4\u63a5\u5931\u8d25\u56de\u8c03"

    .line 1074
    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_f
    const-string p3, "splashLoadAd"

    const-string p4, "\u5982\u679c\u5b9e\u65f6\u5df2\u8d85\u65f6\uff0c\u7f13\u5b58\u4e5f\u5931\u8d25\u76f4\u63a5\u56de\u8c03\uff0c\u76f4\u63a5\u5931\u8d25\u56de\u8c03"

    .line 1080
    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    :cond_10
    :goto_2
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(I)V

    .line 1087
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1088
    monitor-exit p0

    return-void

    :cond_11
    if-nez p5, :cond_12

    if-eqz p3, :cond_12

    if-eqz p4, :cond_12

    .line 1096
    :try_start_7
    new-instance p5, Lcom/bytedance/sdk/openadsdk/i/a/d;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/d/p;->b()[B

    move-result-object p3

    invoke-direct {p5, p3}, Lcom/bytedance/sdk/openadsdk/i/a/d;-><init>([B)V

    .line 1099
    invoke-virtual {p4, p5}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a(Lcom/bytedance/sdk/openadsdk/i/a/d;)V

    .line 1103
    :cond_12
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_14

    if-ne p1, v2, :cond_14

    .line 1104
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->D:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_13

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_13

    const-string p3, "splashLoadAd"

    .line 1106
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "\u5982\u679c\u7f13\u5b58\u5148\u56de\u6765,\u5b9e\u65f6\u8fd8\u6ca1\u6709\u56de\u6765\uff0c\u5b9e\u65f6\u662f\u5426\u6e32\u67d3\u6210\u529f "

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, " \u7f13\u5b58\u6e32\u67d3\u662f\u5426\u6210\u529f "

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_13

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_13

    const-string p1, "splashLoadAd"

    const-string p3, "\u7f13\u5b58\u8d4b\u503c\u7ed9resultTemp"

    .line 1108
    invoke-static {p1, p3}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->E:Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const-string p1, "splashLoadAd"

    const-string p2, "\u5982\u679c\u7f13\u5b58\u5148\u56de\u6765,\u5b9e\u65f6\u8fd8\u6ca1\u6709\u56de\u6765\uff0c\u7b49\u5f85\u5b9e\u65f6\u56de\u6765"

    .line 1110
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1111
    monitor-exit p0

    return-void

    .line 1116
    :cond_13
    :try_start_8
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_14

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-eqz p3, :cond_14

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a()Z

    move-result p3

    if-eqz p3, :cond_14

    const-string p1, "splashLoadAd"

    const-string p2, "\u6b64\u65f6\u5b9e\u65f6\u53ef\u80fd\u5728\u6e32\u67d3\uff0c\u907f\u514d\u7f13\u5b58\u7684\u6e32\u67d3\u6210\u529f\u540e\u76f4\u63a5\u88ab\u56de\u8c03"

    .line 1117
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1118
    monitor-exit p0

    return-void

    :cond_14
    if-ne p1, v4, :cond_15

    :try_start_9
    const-string p3, "splashLoadAd"

    const-string p4, "splashAdTryCallback \u5b9e\u65f6\u6216\u6e32\u67d3\u6210\u529f\u56de\u8c03......\uff01"

    .line 1123
    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1126
    :cond_15
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_16

    if-ne p1, v4, :cond_16

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_16

    const-string p1, "splashLoadAd"

    const-string p2, "\u4f18\u5148\u5b9e\u65f6\uff0c\u4e14\u5b9e\u65f6\u7c7b\u578b\u5e7f\u544a\u56fe\u7247\u52a0\u8f7d\u6210\u529f\uff1b\u5982\u679c\u662f\u6a21\u677f\u5e7f\u544a\u4e14\u6e32\u67d3\u672a\u6210\u529f\uff0c\u5219\u7ee7\u7eed\u7b49\u5f85"

    .line 1127
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1128
    monitor-exit p0

    return-void

    :cond_16
    :try_start_a
    const-string p3, "splashLoadAd"

    .line 1131
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "splashAdTryCallback..\uff08 \u662f\u5426\u7f13\u5b58\u5df2check\u6210\u529f\uff1a"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, " || \u662f\u5426\u6e32\u67d3\u8d85\u65f6\uff1a"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p5, 0x0

    if-ne p1, v1, :cond_17

    const/4 v0, 0x1

    goto :goto_3

    :cond_17
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, " || \u662f\u5426\u4e3a\u5b9e\u65f6\u8bf7\u6c42"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ne p1, v4, :cond_18

    const/4 p5, 0x1

    :cond_18
    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, "\uff09 && \uff08\u662f\u5426\u5b9e\u65f6\u6a21\u7248\u6e32\u67d3\u5b8c\u6210\uff1a"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1133
    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, " || \u662f\u5426\u7f13\u5b58\u6a21\u7248\u6e32\u67d3\u5b8c\u6210\uff1a"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p5, "\uff09"

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 1131
    invoke-static {p3, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1135
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_19

    if-eq p1, v4, :cond_19

    if-ne p1, v1, :cond_1b

    :cond_19
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-nez p3, :cond_1a

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p3

    if-eqz p3, :cond_1b

    .line 1136
    :cond_1a
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(I)V

    .line 1137
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1140
    :cond_1b
    monitor-exit p0

    return-void

    .line 1002
    :goto_4
    monitor-exit p0

    throw p1
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    const-string v0, "splashLoadAd"

    .line 335
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tryDisplaySplashAdFromCache rit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    invoke-virtual {v0, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b$2;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;I)V

    invoke-virtual {v0, p2, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/component/splash/a$c;)V

    goto :goto_0

    .line 388
    :cond_0
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 5

    const-string v0, "splashLoadAd"

    const-string v1, "try LoadSplashAdFromNetwork......"

    .line 496
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 498
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v1

    .line 502
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v2

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/h/h;->i(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 503
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getExpressViewAcceptedWidth()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 504
    :cond_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    const/4 v3, 0x2

    iput v3, v2, Lcom/bytedance/sdk/openadsdk/core/d/m;->e:I

    .line 506
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d:Lcom/bytedance/sdk/openadsdk/core/q;

    instance-of v2, v2, Lcom/bytedance/sdk/openadsdk/core/r;

    if-eqz v2, :cond_2

    .line 507
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d:Lcom/bytedance/sdk/openadsdk/core/q;

    check-cast v2, Lcom/bytedance/sdk/openadsdk/core/r;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/d/q;)V

    .line 509
    :cond_2
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d:Lcom/bytedance/sdk/openadsdk/core/q;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    new-instance v4, Lcom/bytedance/sdk/openadsdk/component/splash/b$4;

    invoke-direct {v4, p0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b$4;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    invoke-interface {v2, p1, v3, v0, v4}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/d/m;ILcom/bytedance/sdk/openadsdk/core/q$b;)V

    return-void
.end method

.method private declared-synchronized a(Lcom/bytedance/sdk/openadsdk/component/splash/b$a;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    const-string v0, "splashLoadAd"

    const-string v1, "onCallback ......"

    .line 1382
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1383
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i()V

    if-nez p1, :cond_0

    const-string p1, "splashAdListener is null, then return"

    .line 1385
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;)V

    .line 1386
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1387
    monitor-exit p0

    return-void

    .line 1390
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    const/4 v1, 0x3

    if-nez v0, :cond_2

    const-string v0, "splashAdListener is null, then return"

    .line 1391
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;)V

    .line 1392
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->a:I

    if-ne v0, v1, :cond_1

    .line 1393
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->e:Lcom/bytedance/sdk/openadsdk/h/a/c;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    .line 1395
    :cond_1
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1396
    monitor-exit p0

    return-void

    .line 1399
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_4

    .line 1400
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1410
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->a:I

    packed-switch v0, :pswitch_data_0

    const/4 p1, -0x2

    .line 1429
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    goto :goto_0

    :pswitch_0
    const-string v0, "splashLoadAd"

    const-string v3, "onCallback CALLBACK_RESULT_TIMEOUT"

    .line 1423
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->e:Lcom/bytedance/sdk/openadsdk/h/a/c;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    .line 1425
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;->onTimeout()V

    goto :goto_1

    :pswitch_1
    const-string v0, "splashLoadAd"

    const-string v3, "onCallback CALLBACK_RESULT_FAILED"

    .line 1418
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->e:Lcom/bytedance/sdk/openadsdk/h/a/c;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    .line 1420
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    iget v3, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->b:I

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->c:Ljava/lang/String;

    invoke-interface {v0, v3, p1}, Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;->onError(ILjava/lang/String;)V

    goto :goto_1

    .line 1413
    :pswitch_2
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->e:Lcom/bytedance/sdk/openadsdk/h/a/c;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    .line 1414
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->d:Lcom/bytedance/sdk/openadsdk/TTSplashAd;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;->onSplashAdLoad(Lcom/bytedance/sdk/openadsdk/TTSplashAd;)V

    .line 1415
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d(Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    goto :goto_1

    .line 1429
    :goto_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, p1, v3}, Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;->onError(ILjava/lang/String;)V

    .line 1433
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/openadsdk/utils/am;

    if-eqz p1, :cond_3

    .line 1435
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/openadsdk/utils/am;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeMessages(I)V

    .line 1436
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeMessages(I)V

    .line 1439
    :cond_3
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    .line 1440
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1441
    monitor-exit p0

    return-void

    .line 1402
    :cond_4
    :try_start_3
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    .line 1403
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->a:I

    if-ne v0, v1, :cond_5

    .line 1404
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;->e:Lcom/bytedance/sdk/openadsdk/h/a/c;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    .line 1406
    :cond_5
    sget-object p1, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1407
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 1381
    monitor-exit p0

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V
    .locals 0

    .line 69
    invoke-direct/range {p0 .. p5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILjava/lang/String;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;I)V
    .locals 0

    .line 69
    invoke-direct/range {p0 .. p5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;I)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/i/a/d;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/i/a/d;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/d/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 11

    const-string v0, "splashLoadAd"

    const-string v1, " SplashUtils preLoadImage"

    .line 590
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->b(Lcom/bytedance/sdk/openadsdk/core/d/a;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez p2, :cond_0

    goto/16 :goto_1

    .line 596
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 598
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/d/l;->V()Lcom/bytedance/sdk/openadsdk/core/d/k;

    move-result-object v0

    if-nez v0, :cond_1

    .line 600
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/d/l;->Z()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/d/k;

    .line 602
    :cond_1
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/k;->a()Ljava/lang/String;

    move-result-object v9

    const-string v2, "splashLoadAd"

    .line 603
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u5f00\u5c4f\u52a0\u8f7d\u7684\u56fe\u7247\u94fe\u63a5 url "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/k;->b()I

    move-result v0

    .line 607
    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v2

    const/4 v10, 0x1

    if-eqz v2, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    .line 608
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->q:J

    .line 609
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->r:J

    if-eqz v5, :cond_3

    const/4 v1, 0x2

    .line 612
    :cond_3
    invoke-static {v6, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;I)V

    .line 614
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/e;->c()Lcom/bytedance/sdk/openadsdk/i/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/i/e;->g()Lcom/bytedance/sdk/openadsdk/i/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/i/a/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/q;)V

    .line 616
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;

    move-object v2, v1

    move-object v3, p0

    move-object v4, p1

    move-object v7, p3

    move-object v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b$5;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/a;ZLcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/h/a/c;Ljava/lang/String;)V

    invoke-static {p2, v9, v0, v1, v10}, Lcom/bytedance/sdk/openadsdk/utils/o;->a(Landroid/content/Context;Ljava/lang/String;ILcom/bytedance/sdk/openadsdk/utils/o$a;Z)V

    return-void

    :cond_4
    :goto_1
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V
    .locals 10

    const-string v0, "splashLoadAd"

    const-string v1, "\u6267\u884c checkAdFromServer \u68c0\u6d4b\u7f13\u5b58...."

    .line 937
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/d/p;)Z

    move-result v0

    const/4 v1, 0x4

    if-eqz v0, :cond_0

    .line 942
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/p;->a()Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    .line 943
    invoke-direct {p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v5

    .line 945
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->f()Lcom/bytedance/sdk/openadsdk/core/q;

    move-result-object v1

    .line 946
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v8

    .line 947
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object v0

    new-instance v9, Lcom/bytedance/sdk/openadsdk/component/splash/b$9;

    move-object v2, v9

    move-object v3, p0

    move-object v4, p2

    move-object v6, p1

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/b$9;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/component/splash/e;Lcom/bytedance/sdk/openadsdk/h/a/c;Lcom/bytedance/sdk/openadsdk/core/d/p;Ljava/lang/String;)V

    .line 946
    invoke-interface {v1, v8, v0, v9}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/q$a;)V

    goto :goto_0

    :cond_0
    const-string p2, "splashLoadAd"

    const-string p3, "checkAdFromServer check fail !!!!"

    .line 971
    invoke-static {p2, p3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e()Z

    move-result p2

    if-eqz p2, :cond_1

    const-string p1, "splashLoadAd"

    const-string p2, "checkAdFromServer check fail !!!! ---> tryLoadSplashAdFromNetwork !!!"

    .line 973
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 977
    invoke-direct {p0, v1, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v8

    const-string p2, "splashLoadAd"

    const-string p3, "checkAdFromServer check fail !!!! ---> return callback !!!"

    .line 978
    invoke-static {p2, p3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x3a99

    .line 980
    new-instance p2, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private declared-synchronized a(Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 7

    monitor-enter p0

    if-nez p2, :cond_0

    .line 901
    monitor-exit p0

    return-void

    .line 903
    :cond_0
    :try_start_0
    new-instance v6, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/component/splash/b$8;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Lcom/bytedance/sdk/openadsdk/h/a/c;Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->renderExpressAd(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 932
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 899
    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized a(Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;I)V
    .locals 8

    monitor-enter p0

    if-nez p2, :cond_0

    .line 856
    monitor-exit p0

    return-void

    .line 858
    :cond_0
    :try_start_0
    new-instance v7, Lcom/bytedance/sdk/openadsdk/component/splash/b$7;

    move-object v0, v7

    move-object v1, p0

    move v2, p5

    move-object v3, p2

    move-object v4, p4

    move-object v5, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b$7;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;ILcom/bytedance/sdk/openadsdk/component/splash/e;Lcom/bytedance/sdk/openadsdk/h/a/c;Lcom/bytedance/sdk/openadsdk/core/d/p;Ljava/lang/String;)V

    invoke-virtual {p2, v7}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->renderExpressAd(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 895
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 854
    monitor-exit p0

    throw p1
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 2

    const-string v0, "splashLoadAd"

    const-string v1, "onLogTimeoutEvent"

    .line 1444
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1445
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    .line 1446
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a;->a()Lcom/bytedance/sdk/openadsdk/h/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->g(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/i/a/d;)V
    .locals 6

    if-eqz p1, :cond_6

    .line 1205
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 1208
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/i/a/d;->b()[B

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1209
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->i()D

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpl-double v5, v1, v3

    if-nez v5, :cond_1

    .line 1210
    array-length v0, v0

    int-to-float v0, v0

    const/high16 v1, 0x44800000    # 1024.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 1211
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(D)V

    .line 1213
    :cond_1
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/i/a/d;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1214
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1215
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "X"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1216
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->b(Ljava/lang/String;)V

    .line 1219
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/i/a/d;->e()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 1220
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->k()Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_5

    .line 1221
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1222
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1223
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    .line 1227
    :cond_3
    :try_start_0
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "SplashAdLoadManager"

    .line 1229
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1232
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(Lorg/json/JSONObject;)V

    :cond_5
    return-void

    :cond_6
    :goto_1
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .line 438
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/b$3;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b$3;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;)V

    invoke-virtual {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/component/splash/a$c;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/AdSlot;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    return-object p0
.end method

.method private b()V
    .locals 4

    .line 241
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    if-nez v0, :cond_0

    return-void

    .line 244
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q()I

    move-result v0

    .line 245
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(I)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 276
    :pswitch_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 277
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-string v0, "splashLoadAd"

    const-string v1, "splash_type_concurrent_first_come_first_use=====\u5e76\u53d1\u8bf7\u6c42\u5e7f\u544a\u548ccheck\u7f13\u5b58\uff0c\u8c01\u5148\u5230\u7528\u8c01"

    .line 278
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 280
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c()Z

    goto :goto_1

    :pswitch_1
    const-string v0, "splashLoadAd"

    const-string v3, "splash_type_concurrent_priority_real_time=====\u5e76\u53d1\u8bf7\u6c42\u5e7f\u544a\u548ccheck\u7f13\u5b58\uff0c\u4f18\u5148\u4f7f\u7528\u5b9e\u65f6"

    .line 268
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 270
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 271
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    .line 272
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c()Z

    return-void

    .line 255
    :pswitch_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 256
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-string v0, "splashLoadAd"

    const-string v1, "splash_type_priorrity_cache_defualt=====\u4f18\u5148\u7f13\u5b58"

    .line 257
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "splashLoadAd"

    const-string v1, "splash_type_priorrity_cache_defualt=====\u4f18\u5148\u7f13\u5b58--->\u6267\u884c\u7f13\u5b58\u5931\u8d25\uff0c\u8fdb\u884c\u7f51\u7edc\u8bf7\u6c42"

    .line 260
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    goto :goto_0

    :cond_1
    const-string v0, "splashLoadAd"

    const-string v1, "splash_type_priorrity_cache_defualt=====\u4f18\u5148\u7f13\u5b58--->\u6267\u884c\u7f13\u5b58\u6210\u529f\uff01\uff01"

    .line 264
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    .line 248
    :pswitch_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 249
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-string v0, "splashLoadAd"

    const-string v1, "splash_type_real_time=====\u53ea\u8d70\u5b9e\u65f6"

    .line 251
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void

    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/core/d/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b(Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/core/d/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 12

    const-string v0, "splashLoadAd"

    const-string v1, " SplashUtils preLoadVideo"

    .line 713
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->b(Lcom/bytedance/sdk/openadsdk/core/d/a;)Z

    move-result v0

    if-eqz v0, :cond_8

    if-nez p2, :cond_0

    goto/16 :goto_2

    .line 718
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 719
    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->d(Ljava/lang/String;)I

    move-result v8

    .line 720
    invoke-virtual {v7}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_2

    const/4 v1, 0x2

    .line 726
    :cond_2
    invoke-static {v7, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;I)V

    if-eqz v0, :cond_7

    .line 728
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    .line 729
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->i()Ljava/lang/String;

    move-result-object v1

    const-string v3, "splashLoadAd"

    .line 730
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SplashUtils preLoadVideo videoUrl "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v1, :cond_3

    .line 733
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    return-void

    .line 737
    :cond_3
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Lcom/bytedance/sdk/openadsdk/core/d/a;

    .line 738
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->w:Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 740
    invoke-static {v7, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;I)V

    .line 742
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->l()Ljava/lang/String;

    move-result-object p2

    .line 743
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :cond_4
    move-object v5, p2

    .line 744
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/a;

    move-result-object p2

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v2

    invoke-virtual {p2, v0, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p2

    .line 745
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2, v5}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 748
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object p2

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->e(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_6

    .line 749
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/x;->d(Landroid/content/Context;)Z

    move-result p2

    if-nez p2, :cond_6

    if-eqz v5, :cond_5

    .line 752
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result p2

    if-eqz p2, :cond_5

    const-string p2, "splashLoadAd"

    const-string p3, "\u975ewifi\u73af\u5883\uff0c\u5df2\u7f13\u5b58\u76f8\u540curl\u7684\u89c6\u9891\u6587\u4ef6\u4e5f\u662f\u53ef\u4ee5\u64ad\u653e\u7684"

    .line 753
    invoke-static {p2, p3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/component/splash/a;

    move-result-object p2

    new-instance p3, Lcom/bytedance/sdk/openadsdk/core/d/p;

    const/4 v0, 0x0

    invoke-direct {p3, p1, v7, v0}, Lcom/bytedance/sdk/openadsdk/core/d/p;-><init>(Lcom/bytedance/sdk/openadsdk/core/d/a;Lcom/bytedance/sdk/openadsdk/core/d/l;[B)V

    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Lcom/bytedance/sdk/openadsdk/core/d/p;)V

    const/16 p1, 0x3a98

    .line 756
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ""

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILjava/lang/String;)V

    return-void

    :cond_5
    const-string p1, "splashLoadAd"

    const-string p2, "\u975ewifi\u73af\u5883"

    .line 760
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 767
    :cond_6
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/e;->c()Lcom/bytedance/sdk/openadsdk/i/e;

    move-result-object p2

    new-instance v0, Lcom/bytedance/sdk/openadsdk/component/splash/b$6;

    move-object v2, v0

    move-object v3, p0

    move-object v6, p1

    move-object v11, p3

    invoke-direct/range {v2 .. v11}, Lcom/bytedance/sdk/openadsdk/component/splash/b$6;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/io/File;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/a;Lcom/bytedance/sdk/openadsdk/core/d/l;IJLcom/bytedance/sdk/openadsdk/h/a/c;)V

    invoke-virtual {p2, v1, v0}, Lcom/bytedance/sdk/openadsdk/i/e;->a(Ljava/lang/String;Lcom/bytedance/sdk/adnet/b/b$a;)V

    goto :goto_1

    :cond_7
    const-string v0, "splashLoadAd"

    const-string v1, "\u672a\u4e0b\u53d1\u89c6\u9891\u6587\u4ef6\uff0c\u5219\u52a0\u8f7d\u56fe\u7247\u5b8c\u6210\u540e\u7ed9\u4e88\u56de\u8c03"

    .line 845
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/a;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    :goto_1
    return-void

    :cond_8
    :goto_2
    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 1

    if-eqz p1, :cond_2

    .line 1156
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    if-nez v0, :cond_0

    goto :goto_1

    .line 1159
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 1161
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->c(I)V

    goto :goto_0

    .line 1163
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->c(I)V

    :goto_0
    return-void

    :cond_2
    :goto_1
    return-void
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 5

    const-string v0, "splashLoadAd"

    const-string v1, "reportMarkAtRespFail outer_call_no_rsp"

    .line 1572
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 1577
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1579
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    .line 1582
    :cond_1
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->x:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 1584
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/c;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    :cond_2
    :goto_0
    const-string v2, "splashLoadAd"

    .line 1587
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportMarkAtRespFail cacheStatus "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v2, "if_have_cache"

    .line 1589
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "if_have_rt_ads"

    .line 1590
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->B:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1593
    :catch_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a;->a()Lcom/bytedance/sdk/openadsdk/h/a;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->d(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/core/d/l;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/d/l;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/component/splash/b;Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c(Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    return-void
.end method

.method private c(Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 1186
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object p1

    if-eqz p1, :cond_1

    return-void

    .line 1190
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q()I

    move-result p1

    if-eqz p1, :cond_3

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 p1, 0x1

    .line 1192
    :goto_1
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->z:Z

    return-void
.end method

.method private c(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    const-string v0, "splashLoadAd"

    const-string v1, "reportMarkAtRespSucc outer_call_send"

    .line 1597
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 1602
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "splashLoadAd"

    .line 1603
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportMarkAtRespSucc sSplashLoadImageSource "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Lcom/bytedance/sdk/openadsdk/i/a/b;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v1, "image_CacheType"

    .line 1605
    sget v2, Lcom/bytedance/sdk/openadsdk/i/a/b;->a:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1608
    :catch_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a;->a()Lcom/bytedance/sdk/openadsdk/h/a;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->c(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    return-void
.end method

.method private c()Z
    .locals 2

    .line 291
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3a99

    .line 292
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILjava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/component/splash/a;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    return-object p0
.end method

.method private d(Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 13

    .line 1262
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 1265
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->v()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 1269
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->K()Z

    move-result v0

    .line 1270
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->w()Z

    move-result v1

    .line 1271
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    if-eqz v1, :cond_2

    .line 1274
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->t()J

    move-result-wide v4

    sub-long v4, v2, v4

    .line 1275
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/d/q;->c(J)V

    goto :goto_0

    .line 1278
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->t()J

    move-result-wide v4

    sub-long v4, v2, v4

    .line 1279
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/core/d/q;->y()J

    move-result-wide v6

    sub-long v6, v2, v6

    invoke-virtual {v1, v6, v7}, Lcom/bytedance/sdk/openadsdk/core/d/q;->k(J)V

    .line 1280
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/d/q;->o(J)V

    .line 1283
    :goto_0
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    if-eqz v0, :cond_3

    :try_start_0
    const-string v1, "pre_connect_status"

    .line 1286
    sget v4, Lcom/bytedance/sdk/openadsdk/core/m;->d:I

    invoke-virtual {v12, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_1

    :catch_0
    move-exception v0

    goto/16 :goto_2

    :cond_3
    :goto_1
    const-string v1, "if_pre_connect"

    .line 1288
    invoke-virtual {v12, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "splash_load_type"

    .line 1289
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a()I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "splash_final_type"

    .line 1290
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "active_type"

    .line 1291
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->c()I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "splash_creative_type"

    .line 1292
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->d()I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "splash_load_type"

    .line 1293
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a()I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1296
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->z:Z

    if-eqz v0, :cond_4

    const-string v0, "load_duration"

    .line 1297
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->e()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_image_duration"

    .line 1298
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->x()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "cache_image_duration"

    .line 1299
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->g()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "image_cachetype"

    .line 1300
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->h()I

    move-result v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "image_size"

    .line 1301
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->i()D

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v0, "image_resolution"

    .line 1302
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "image_response_header"

    .line 1303
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->k()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "client_start_time"

    .line 1304
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->l()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "network_time"

    .line 1305
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->m()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "sever_time"

    .line 1306
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->n()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "client_end_time"

    .line 1307
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->o()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_client_start_time"

    .line 1308
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->p()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_net_time"

    .line 1309
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->q()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_write_time"

    .line 1310
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->r()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_client_end_time"

    .line 1311
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->s()J

    move-result-wide v4

    invoke-virtual {v12, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1314
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1316
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->u()J

    move-result-wide v0

    sub-long v10, v2, v0

    .line 1317
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    const-string v8, "splash_ad"

    const-string v9, "splash_ad_loadtime"

    move-object v7, p1

    invoke-static/range {v6 .. v12}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JLorg/json/JSONObject;)V

    return-void

    :cond_5
    :goto_4
    return-void
.end method

.method private d()Z
    .locals 11

    const-string v0, "splashLoadAd"

    const-string v1, "try checkSpashAdCacheIsValidAndTryShowAd......"

    .line 299
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x3a99

    const/4 v1, 0x0

    .line 300
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v8

    .line 302
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->b(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    .line 303
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "splashLoadAd"

    const-string v2, "\u6ca1\u6709\u7f13\u5b58\u6570\u636e.........."

    .line 304
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, -0xc

    const/16 v9, 0x3a99

    .line 307
    new-instance v10, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v4, 0x2

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v5, -0xc

    move-object v2, v10

    move-object v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move v3, v9

    move-object v4, v10

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 309
    :cond_1
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->x:I

    return v1

    .line 315
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "splashLoadAd"

    const-string v2, "======== \u7f13\u5b58\u8fc7\u671f ========"

    .line 316
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->s:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/16 v0, -0xb

    const/16 v9, 0x3a99

    .line 319
    new-instance v10, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v4, 0x2

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v5, -0xb

    move-object v2, v10

    move-object v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move v3, v9

    move-object v4, v10

    invoke-direct/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    :cond_4
    const/4 v0, 0x2

    .line 321
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->x:I

    return v1

    :cond_5
    return v3
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private e()Z
    .locals 2

    .line 329
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q()I

    move-result v0

    const/4 v1, 0x2

    if-eq v1, v0, :cond_0

    const/4 v1, 0x3

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private f()V
    .locals 3

    .line 1144
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    .line 1145
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/q;->m(J)V

    .line 1146
    sget-object v0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->isColdStartSdk:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1147
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->b(I)V

    .line 1148
    sget-object v0, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->isColdStartSdk:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 1150
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->b(I)V

    :goto_0
    return-void
.end method

.method private g()V
    .locals 4

    .line 1197
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    if-nez v0, :cond_0

    return-void

    .line 1200
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/q;->u()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 1201
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v2, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(J)V

    return-void
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Z
    .locals 0

    .line 69
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->o:Z

    return p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private h()V
    .locals 8

    .line 1238
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    if-nez v0, :cond_0

    goto :goto_1

    .line 1241
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->v()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 1245
    :cond_1
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "image_size"

    .line 1247
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->i()D

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v0, "image_resolution"

    .line 1248
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "image_response_header"

    .line 1249
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->k()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "download_client_start_time"

    .line 1250
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->p()J

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_net_time"

    .line 1251
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->q()J

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "download_write_time"

    .line 1252
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->r()J

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1254
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1256
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->f()J

    move-result-wide v5

    .line 1257
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->A:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const-string v3, "splash_ad"

    const-string v4, "download_image_duration"

    invoke-static/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JLorg/json/JSONObject;)V

    return-void

    :cond_2
    :goto_1
    return-void
.end method

.method private i()V
    .locals 3

    .line 1371
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    if-eqz v0, :cond_0

    .line 1372
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k:Lcom/bytedance/sdk/openadsdk/component/splash/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/a;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/d/m;)V

    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Z
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e()Z

    move-result p0

    return p0
.end method

.method private j()V
    .locals 12

    const-string v0, "splashLoadAd"

    const-string v1, "loadSplashOnLineVideo"

    .line 1539
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Lcom/bytedance/sdk/openadsdk/core/d/a;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->w:Lcom/bytedance/sdk/openadsdk/h/a/c;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 1543
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_0

    .line 1546
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-nez v0, :cond_2

    return-void

    .line 1550
    :cond_2
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->x()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    return-void

    .line 1555
    :cond_3
    new-instance v7, Lcom/bytedance/sdk/openadsdk/core/d/p;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->u:Lcom/bytedance/sdk/openadsdk/core/d/a;

    const/4 v2, 0x0

    invoke-direct {v7, v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/core/d/p;-><init>(Lcom/bytedance/sdk/openadsdk/core/d/a;Lcom/bytedance/sdk/openadsdk/core/d/l;[B)V

    .line 1556
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v1

    if-nez v1, :cond_4

    return-void

    .line 1559
    :cond_4
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->i()Ljava/lang/String;

    move-result-object v8

    const-string v0, "splashLoadAd"

    .line 1560
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadSplashOnLineVideo videoPath "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1561
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    return-void

    :cond_5
    const-string v0, "splashLoadAd"

    const-string v1, "loadSplashOnLineVideo splashAdTryCallback"

    .line 1564
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1565
    invoke-direct {p0, v7, v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(Lcom/bytedance/sdk/openadsdk/core/d/p;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/component/splash/e;

    move-result-object v9

    const/16 v10, 0x3a9b

    .line 1566
    new-instance v11, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->w:Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-object v0, v11

    move-object v1, p0

    move-object v5, v9

    invoke-direct/range {v0 .. v6}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    move-object v0, p0

    move v1, v10

    move-object v2, v11

    move-object v3, v7

    move-object v4, v9

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    return-void

    :cond_6
    :goto_0
    return-void

    :cond_7
    :goto_1
    return-void
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/component/splash/b;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g()V

    return-void
.end method

.method private k()Lcom/bytedance/sdk/openadsdk/core/d/m;
    .locals 3

    .line 1612
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1613
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/d/m;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/core/d/m;-><init>()V

    .line 1614
    iput-wide v0, v2, Lcom/bytedance/sdk/openadsdk/core/d/m;->f:J

    return-object v2
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Lcom/bytedance/sdk/openadsdk/core/d/q;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->y:Lcom/bytedance/sdk/openadsdk/core/d/q;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Landroid/content/Context;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->e:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic m(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J
    .locals 2

    .line 69
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->q:J

    return-wide v0
.end method

.method static synthetic n(Lcom/bytedance/sdk/openadsdk/component/splash/b;)J
    .locals 2

    .line 69
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->r:J

    return-wide v0
.end method

.method static synthetic o(Lcom/bytedance/sdk/openadsdk/component/splash/b;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h()V

    return-void
.end method

.method static synthetic p(Lcom/bytedance/sdk/openadsdk/component/splash/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 11

    .line 1452
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 1453
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1455
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1456
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b()V

    const-string v0, "SplashAdLoadManager"

    const-string v3, "\u5c1d\u8bd5\u4ece\u7f13\u5b58\u4e2d\u53d6"

    .line 1457
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1460
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i()V

    const-string v0, "SplashAdLoadManager"

    const-string v3, "\u5f00\u59cb\u9884\u52a0\u8f7d"

    .line 1461
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1463
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1466
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    .line 1467
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1468
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    const-string v0, "splashLoadAd"

    const-string v3, "MSG_USER_TIME_OUT----7-"

    .line 1471
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1472
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v9

    const/16 v0, 0x3a9c

    .line 1473
    new-instance v10, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v5, 0x3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v10

    move-object v4, p0

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    const/4 v6, 0x0

    move-object v3, p0

    move v4, v0

    move-object v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    .line 1476
    :cond_3
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x3

    if-ne p1, v0, :cond_b

    .line 1477
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeMessages(I)V

    .line 1478
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_4

    return-void

    .line 1482
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1483
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1487
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q()I

    move-result p1

    const/4 v1, 0x4

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_3

    .line 1514
    :pswitch_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_5

    const-string p1, "splashLoadAd"

    const-string v0, "\u8c01\u56de\u6765\u7528\u8c01//--mCacheTTSplashAd"

    .line 1515
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1516
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    const/4 v0, 0x4

    goto :goto_1

    :cond_5
    move-object p1, v2

    .line 1519
    :goto_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-eqz v1, :cond_9

    const-string p1, "splashLoadAd"

    const-string v1, "\u8c01\u56de\u6765\u7528\u8c01//--mRealNetWorkTTSplashAd"

    .line 1520
    invoke-static {p1, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1521
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    goto :goto_4

    .line 1503
    :pswitch_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-eqz p1, :cond_6

    .line 1504
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    const-string v1, "splashLoadAd"

    const-string v3, "\u8d85\u65f6\u4f7f\u7528\u5b9e\u65f6--mRealNetWorkTTSplashAd"

    .line 1505
    invoke-static {v1, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1506
    :cond_6
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_8

    const-string p1, "splashLoadAd"

    const-string v0, "\u8d85\u65f6\u4f7f\u7528\u5b9e\u65f6//--mCacheTTSplashAd"

    .line 1507
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1508
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    goto :goto_2

    .line 1494
    :pswitch_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 1495
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    :goto_2
    const/4 v0, 0x4

    goto :goto_4

    .line 1497
    :cond_7
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    if-eqz p1, :cond_8

    .line 1498
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    goto :goto_4

    .line 1490
    :pswitch_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    goto :goto_4

    :cond_8
    :goto_3
    move-object p1, v2

    :cond_9
    :goto_4
    if-eqz p1, :cond_a

    .line 1526
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/component/splash/e;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "splashLoadAd"

    const-string v3, "\u8d85\u65f6\u4e86 temp != null "

    .line 1527
    invoke-static {v1, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1528
    invoke-direct {p0, v0, v2}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/core/d/p;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v9

    const/16 v0, 0x3a9b

    .line 1529
    new-instance v1, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, v1

    move-object v4, p0

    move-object v8, p1

    invoke-direct/range {v3 .. v9}, Lcom/bytedance/sdk/openadsdk/component/splash/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;IILjava/lang/String;Lcom/bytedance/sdk/openadsdk/TTSplashAd;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    move v4, v0

    move-object v5, v1

    move-object v7, p1

    invoke-direct/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a(ILcom/bytedance/sdk/openadsdk/component/splash/b$a;Lcom/bytedance/sdk/openadsdk/core/d/p;Lcom/bytedance/sdk/openadsdk/component/splash/e;Ljava/lang/String;)V

    goto :goto_5

    :cond_a
    const-string p1, "splashLoadAd"

    const-string v0, "\u8d85\u65f6\u4e86 \u5f00\u59cb\u52a0\u8f7d\u5728\u7ebf\u89c6\u9891"

    .line 1532
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->j()V

    :cond_b
    :goto_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;I)V
    .locals 7
    .param p2    # Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 173
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->k()Lcom/bytedance/sdk/openadsdk/core/d/m;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    .line 174
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a:Lcom/bytedance/sdk/openadsdk/AdSlot;

    .line 175
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$SplashAdListener;

    .line 176
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 p2, 0x0

    .line 177
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->h:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    .line 178
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->i:Lcom/bytedance/sdk/openadsdk/component/splash/e;

    const-string p2, "splashLoadAd"

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u5f00\u53d1\u8005\u4f20\u5165\u7684\u8d85\u65f6\u65f6\u957f timeOut "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    int-to-long v0, p3

    .line 181
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object p2

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->c(Ljava/lang/String;)I

    move-result p1

    const-string p2, "splashLoadAd"

    .line 182
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u4e91\u63a7\u7684\u8d85\u65f6\u65f6\u957f cloudTimeOut "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p2, 0x1f4

    if-lez p1, :cond_0

    const-string p2, "splashLoadAd"

    const-string v2, "\u4e91\u63a7\u63a7\u5236\u7684\u8d85\u65f6\u65f6\u957f\u5927\u4e8e0\u6bd4\u8f83 \u8f83\u670d\u52a1\u7aef\u4e0b\u53d1\u7684\u8d85\u65f6\u65f6\u957f\u548c\u5f00\u53d1\u8005\u914d\u7f6e\u7684\u8d85\u65f6\u65f6\u957f "

    .line 185
    invoke-static {p2, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-lt p1, p3, :cond_1

    move p2, p1

    goto :goto_0

    :cond_0
    if-ge p3, p2, :cond_1

    goto :goto_0

    :cond_1
    move p2, p3

    :goto_0
    const-string p3, "splashLoadAd"

    .line 194
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSplashAd \u5b9e\u9645 timeOut "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p3, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/openadsdk/utils/am;

    const/4 v2, 0x2

    int-to-long v3, p2

    invoke-virtual {p3, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/am;->sendEmptyMessageDelayed(IJ)Z

    add-int/lit16 p3, p2, -0x12c

    if-lez p3, :cond_2

    move p2, p3

    .line 198
    :cond_2
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->f:Lcom/bytedance/sdk/openadsdk/utils/am;

    const/4 v2, 0x3

    int-to-long v5, p2

    invoke-virtual {p3, v2, v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/am;->sendEmptyMessageDelayed(IJ)Z

    .line 200
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    iput-wide v0, p2, Lcom/bytedance/sdk/openadsdk/core/d/m;->g:J

    .line 201
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    int-to-long v5, p1

    iput-wide v5, p2, Lcom/bytedance/sdk/openadsdk/core/d/m;->h:J

    .line 202
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b:Lcom/bytedance/sdk/openadsdk/core/d/m;

    iput-wide v3, p1, Lcom/bytedance/sdk/openadsdk/core/d/m;->i:J

    .line 204
    new-instance p1, Lcom/bytedance/sdk/openadsdk/component/splash/b$1;

    const-string p2, "getSplashAd"

    invoke-direct {p1, p0, p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/component/splash/b$1;-><init>(Lcom/bytedance/sdk/openadsdk/component/splash/b;Ljava/lang/String;J)V

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Lcom/bytedance/sdk/openadsdk/l/g;)V

    .line 222
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->b()V

    .line 223
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/component/splash/b;->a()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 394
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->d()I

    move-result p1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    return v0
.end method
