.class Lcom/bytedance/sdk/openadsdk/c/p$11;
.super Lcom/bytedance/sdk/openadsdk/l/g;
.source "WebviewTimeTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/c/p;->b(Lorg/json/JSONObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lorg/json/JSONObject;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/c/p;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/c/p;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->b:Lcom/bytedance/sdk/openadsdk/c/p;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->a:Lorg/json/JSONObject;

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/l/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 357
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->b:Lcom/bytedance/sdk/openadsdk/c/p;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/c/p;->a(Lcom/bytedance/sdk/openadsdk/c/p;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 358
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->b:Lcom/bytedance/sdk/openadsdk/c/p;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/c/p;->b(Lcom/bytedance/sdk/openadsdk/c/p;)Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->a:Lorg/json/JSONObject;

    if-nez v1, :cond_0

    goto :goto_1

    .line 361
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->a:Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 362
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 363
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 364
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->b:Lcom/bytedance/sdk/openadsdk/c/p;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->b:Lcom/bytedance/sdk/openadsdk/c/p;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/c/p;->b(Lcom/bytedance/sdk/openadsdk/c/p;)Lorg/json/JSONObject;

    move-result-object v4

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->a:Lorg/json/JSONObject;

    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v3, v4, v2, v5}, Lcom/bytedance/sdk/openadsdk/c/p;->a(Lcom/bytedance/sdk/openadsdk/c/p;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 366
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->b:Lcom/bytedance/sdk/openadsdk/c/p;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/c/p;->a(Lcom/bytedance/sdk/openadsdk/c/p;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 367
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/c/p$11;->b:Lcom/bytedance/sdk/openadsdk/c/p;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/c/p;->m()V

    .line 368
    monitor-exit v0

    return-void

    .line 359
    :cond_2
    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    .line 368
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
