.class public Lcom/bytedance/sdk/openadsdk/AppLogHelper;
.super Ljava/lang/Object;
.source "AppLogHelper.java"


# static fields
.field private static volatile a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private volatile d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    return-void
.end method

.method private a()V
    .locals 2

    .line 68
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getDid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "sdk_app_log_did"

    .line 70
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .line 94
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getDid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "app_log_user_unique_id"

    .line 96
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/bytedance/sdk/openadsdk/AppLogHelper;
    .locals 2

    .line 38
    sget-object v0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    if-nez v0, :cond_1

    .line 39
    const-class v0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    monitor-enter v0

    .line 40
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    if-nez v1, :cond_0

    .line 41
    new-instance v1, Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    .line 43
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 45
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a:Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    return-object v0
.end method


# virtual methods
.method public getAppLogDid()Ljava/lang/String;
    .locals 3

    .line 53
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "sdk_app_log_did"

    const-wide v1, 0x9a7ec800L

    .line 55
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/i;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    .line 57
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    if-nez v0, :cond_0

    .line 59
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->initAppLog(Landroid/content/Context;)V

    .line 61
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a()V

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getAppLogUserUniqueID()Ljava/lang/String;
    .locals 3

    .line 79
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "app_log_user_unique_id"

    const-wide v1, 0x9a7ec800L

    .line 81
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/i;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    .line 83
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    if-nez v0, :cond_0

    .line 85
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->initAppLog(Landroid/content/Context;)V

    .line 87
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b()V

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 2

    .line 179
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    const-string v0, "sdk_version_name"

    const-string v1, ""

    .line 184
    invoke-static {v0, v1}, Lcom/bytedance/embedapplog/AppLog;->getHeaderValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized initAppLog(Landroid/content/Context;)V
    .locals 3

    monitor-enter p0

    .line 129
    :try_start_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    if-nez v0, :cond_2

    .line 130
    new-instance v0, Lcom/bytedance/embedapplog/e;

    const v1, 0x2820a

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "unionser_slardar_applog"

    invoke-direct {v0, v1, v2}, Lcom/bytedance/embedapplog/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/m;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    if-eqz v1, :cond_1

    .line 132
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/m;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUsePhoneState()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/e;->c(Z)V

    .line 133
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/m;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUsePhoneState()Z

    move-result v1

    if-nez v1, :cond_0

    .line 135
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/m;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->getDevImei()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/e;->a(Ljava/lang/String;)V

    .line 137
    :cond_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/m;->b:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUseWifiState()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/e;->b(Z)V

    .line 139
    :cond_1
    new-instance v1, Lcom/bytedance/sdk/openadsdk/AppLogHelper$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper$1;-><init>(Lcom/bytedance/sdk/openadsdk/AppLogHelper;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/e;->a(Lcom/bytedance/embedapplog/d;)V

    const/4 v1, 0x0

    .line 159
    invoke-virtual {v0, v1}, Lcom/bytedance/embedapplog/e;->a(I)Lcom/bytedance/embedapplog/e;

    .line 161
    invoke-static {p1, v0}, Lcom/bytedance/embedapplog/AppLog;->init(Landroid/content/Context;Lcom/bytedance/embedapplog/e;)V

    .line 162
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/y;->a(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 163
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    .line 164
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->a()V

    .line 165
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 128
    monitor-exit p0

    throw p1
.end method

.method public setHeaderInfo(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 172
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->d:Z

    if-nez v0, :cond_0

    .line 173
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->initAppLog(Landroid/content/Context;)V

    .line 175
    :cond_0
    invoke-static {p1}, Lcom/bytedance/embedapplog/AppLog;->setHeaderInfo(Ljava/util/HashMap;)V

    return-void
.end method
