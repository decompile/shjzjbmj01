.class Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;
.super Lcom/bytedance/sdk/openadsdk/l/g;
.source "VideoCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/video/a/a/b;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/video/a/a/b;Ljava/lang/String;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/l/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 29

    move-object/from16 v1, p0

    const/4 v2, 0x0

    .line 79
    :try_start_0
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;J)J

    .line 80
    new-instance v0, Lcom/bytedance/sdk/a/b/v;

    invoke-direct {v0}, Lcom/bytedance/sdk/a/b/v;-><init>()V

    const-string v3, "VideoCacheImpl"

    const/4 v4, 0x4

    .line 81
    new-array v5, v4, [Ljava/lang/Object;

    const-string v6, "RANGE, bytes="

    const/4 v7, 0x0

    aput-object v6, v5, v7

    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v6}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v8, 0x1

    aput-object v6, v5, v8

    const-string v6, " file hash="

    const/4 v9, 0x2

    aput-object v6, v5, v9

    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v6}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->b(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/bytedance/sdk/openadsdk/video/b/a;->b()Ljava/lang/String;

    move-result-object v6

    const/4 v10, 0x3

    aput-object v6, v5, v10

    invoke-static {v3, v5}, Lcom/bytedance/sdk/openadsdk/video/d/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    new-instance v3, Lcom/bytedance/sdk/a/b/y$a;

    invoke-direct {v3}, Lcom/bytedance/sdk/a/b/y$a;-><init>()V

    const-string v5, "RANGE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "bytes="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    .line 83
    invoke-static {v11}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v11

    invoke-virtual {v6, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v11, "-"

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/bytedance/sdk/a/b/y$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/a/b/y$a;

    move-result-object v3

    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    .line 84
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->b(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/video/b/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/bytedance/sdk/a/b/y$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/a/b/y$a;

    move-result-object v3

    .line 85
    invoke-virtual {v3}, Lcom/bytedance/sdk/a/b/y$a;->a()Lcom/bytedance/sdk/a/b/y$a;

    move-result-object v3

    .line 86
    invoke-virtual {v3}, Lcom/bytedance/sdk/a/b/y$a;->d()Lcom/bytedance/sdk/a/b/y;

    move-result-object v3

    .line 87
    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/a/b/v;->a(Lcom/bytedance/sdk/a/b/y;)Lcom/bytedance/sdk/a/b/e;

    move-result-object v0

    .line 88
    invoke-interface {v0}, Lcom/bytedance/sdk/a/b/e;->a()Lcom/bytedance/sdk/a/b/aa;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 89
    :try_start_1
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-virtual {v3}, Lcom/bytedance/sdk/a/b/aa;->d()Z

    move-result v5

    invoke-static {v0, v5}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;Z)Z

    .line 90
    invoke-virtual {v3}, Lcom/bytedance/sdk/a/b/aa;->h()Lcom/bytedance/sdk/a/b/ab;

    move-result-object v5
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 91
    :try_start_2
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->c(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v5, :cond_0

    .line 92
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-virtual {v5}, Lcom/bytedance/sdk/a/b/ab;->b()J

    move-result-wide v11

    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v6}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v13

    const/4 v6, 0x0

    add-long/2addr v11, v13

    invoke-static {v0, v11, v12}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->b(Lcom/bytedance/sdk/openadsdk/video/a/a/b;J)J

    .line 93
    invoke-virtual {v5}, Lcom/bytedance/sdk/a/b/ab;->c()Ljava/io/InputStream;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v2, v0

    :cond_0
    if-nez v2, :cond_5

    if-eqz v2, :cond_1

    .line 123
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz v5, :cond_2

    .line 126
    invoke-virtual {v5}, Lcom/bytedance/sdk/a/b/ab;->close()V

    :cond_2
    if-eqz v3, :cond_3

    .line 130
    invoke-virtual {v3}, Lcom/bytedance/sdk/a/b/aa;->close()V

    .line 132
    :cond_3
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->c(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->d(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 133
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->g(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 137
    :goto_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_4
    :goto_2
    return-void

    :cond_5
    const/16 v0, 0x4000

    .line 99
    :try_start_4
    new-array v6, v0, [B

    .line 102
    iget-object v11, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v11}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v11

    const-wide/16 v13, 0x0

    move-wide/from16 v17, v11

    move-wide v15, v13

    const/4 v11, 0x0

    :goto_3
    rsub-int v12, v11, 0x4000

    .line 103
    invoke-virtual {v2, v6, v11, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v12
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/4 v0, -0x1

    const/16 v19, 0x9

    const/16 v20, 0x8

    const/16 v21, 0x7

    const/16 v22, 0x6

    const/16 v23, 0x5

    if-eq v12, v0, :cond_9

    add-int/2addr v11, v12

    move-object/from16 v25, v5

    int-to-long v4, v12

    add-long/2addr v15, v4

    const-wide/16 v4, 0x4000

    .line 106
    :try_start_5
    rem-long v4, v15, v4

    cmp-long v0, v4, v13

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->d(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v4

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v26

    const/4 v0, 0x0

    sub-long v4, v4, v26

    cmp-long v0, v15, v4

    if-nez v0, :cond_6

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    :cond_7
    :goto_4
    const/4 v0, 0x1

    :goto_5
    const-string v4, "VideoCacheImpl"

    const/16 v5, 0xe

    .line 107
    new-array v5, v5, [Ljava/lang/Object;

    const-string v12, "Write segment,execAppend ="

    aput-object v12, v5, v7

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v5, v8

    const-string v12, " offset="

    aput-object v12, v5, v9

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v5, v10

    const-string v12, " totalLength = "

    const/16 v24, 0x4

    aput-object v12, v5, v24

    iget-object v12, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v12}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->d(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v5, v23

    const-string v12, " saveSize ="

    aput-object v12, v5, v22

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v5, v21

    const-string v12, " startSaved="

    aput-object v12, v5, v20

    iget-object v12, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v12}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v5, v19

    const-string v12, " fileHash="

    const/16 v19, 0xa

    aput-object v12, v5, v19

    const/16 v12, 0xb

    iget-object v13, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v13}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->b(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v13

    invoke-virtual {v13}, Lcom/bytedance/sdk/openadsdk/video/b/a;->b()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v5, v12

    const/16 v12, 0xc

    const-string v13, " url="

    aput-object v13, v5, v12

    const/16 v12, 0xd

    iget-object v13, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v13}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->b(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v13

    invoke-virtual {v13}, Lcom/bytedance/sdk/openadsdk/video/b/a;->a()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v5, v12

    invoke-static {v4, v5}, Lcom/bytedance/sdk/openadsdk/video/d/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v0, :cond_8

    .line 109
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->e(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 110
    :try_start_6
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->f(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Ljava/io/RandomAccessFile;

    move-result-object v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->intValue()I

    move-result v5

    iget-object v12, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v12}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->b(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v12

    invoke-virtual {v12}, Lcom/bytedance/sdk/openadsdk/video/b/a;->b()Ljava/lang/String;

    move-result-object v12

    invoke-static {v0, v6, v5, v11, v12}, Lcom/bytedance/sdk/openadsdk/video/d/c;->a(Ljava/io/RandomAccessFile;[BIILjava/lang/String;)V

    .line 111
    monitor-exit v4

    int-to-long v4, v11

    add-long v17, v17, v4

    const/4 v11, 0x0

    goto :goto_6

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0

    :cond_8
    :goto_6
    move-object/from16 v5, v25

    const/16 v0, 0x4000

    const/4 v4, 0x4

    const-wide/16 v13, 0x0

    goto/16 :goto_3

    :catch_1
    move-exception v0

    goto/16 :goto_8

    :cond_9
    move-object/from16 v25, v5

    const-string v0, "VideoCacheImpl"

    const/16 v4, 0xa

    .line 116
    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "Write segment,Write over, startIndex ="

    aput-object v5, v4, v7

    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    const-string v5, " totalLength = "

    aput-object v5, v4, v9

    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->d(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v10

    const-string v5, " saveSize = "

    const/4 v6, 0x4

    aput-object v5, v4, v6

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v23

    const-string v5, " writeEndSegment ="

    aput-object v5, v4, v22

    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->d(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v5

    iget-object v9, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v9}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v9

    const/4 v11, 0x0

    sub-long/2addr v5, v9

    cmp-long v9, v15, v5

    if-nez v9, :cond_a

    const/4 v7, 0x1

    :cond_a
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v21

    const-string v5, " url="

    aput-object v5, v4, v20

    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->b(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/video/b/a;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v19

    invoke-static {v0, v4}, Lcom/bytedance/sdk/openadsdk/video/d/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    if-eqz v2, :cond_b

    .line 123
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_b
    if-eqz v25, :cond_c

    .line 126
    invoke-virtual/range {v25 .. v25}, Lcom/bytedance/sdk/a/b/ab;->close()V

    :cond_c
    if-eqz v3, :cond_d

    .line 130
    invoke-virtual {v3}, Lcom/bytedance/sdk/a/b/aa;->close()V

    .line 132
    :cond_d
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->c(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->d(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_11

    .line 133
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->g(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_b

    :catchall_1
    move-exception v0

    move-object/from16 v25, v5

    goto :goto_c

    :catch_2
    move-exception v0

    move-object/from16 v25, v5

    goto :goto_8

    :catchall_2
    move-exception v0

    move-object/from16 v25, v2

    goto :goto_7

    :catch_3
    move-exception v0

    move-object/from16 v25, v2

    goto :goto_8

    :catchall_3
    move-exception v0

    move-object v3, v2

    move-object/from16 v25, v3

    :goto_7
    move-object v2, v0

    move-object/from16 v0, v25

    goto :goto_d

    :catch_4
    move-exception v0

    move-object v3, v2

    move-object/from16 v25, v3

    .line 119
    :goto_8
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    if-eqz v2, :cond_e

    .line 123
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_9

    :catch_5
    move-exception v0

    goto :goto_a

    :cond_e
    :goto_9
    if-eqz v25, :cond_f

    .line 126
    invoke-virtual/range {v25 .. v25}, Lcom/bytedance/sdk/a/b/ab;->close()V

    :cond_f
    if-eqz v3, :cond_10

    .line 130
    invoke-virtual {v3}, Lcom/bytedance/sdk/a/b/aa;->close()V

    .line 132
    :cond_10
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->c(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->d(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_11

    .line 133
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->g(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    goto :goto_b

    .line 137
    :goto_a
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_11
    :goto_b
    return-void

    :catchall_4
    move-exception v0

    :goto_c
    move-object/from16 v28, v2

    move-object v2, v0

    move-object/from16 v0, v28

    :goto_d
    if-eqz v0, :cond_12

    .line 123
    :try_start_b
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    goto :goto_e

    :catch_6
    move-exception v0

    goto :goto_f

    :cond_12
    :goto_e
    if-eqz v25, :cond_13

    .line 126
    invoke-virtual/range {v25 .. v25}, Lcom/bytedance/sdk/a/b/ab;->close()V

    :cond_13
    if-eqz v3, :cond_14

    .line 130
    invoke-virtual {v3}, Lcom/bytedance/sdk/a/b/aa;->close()V

    .line 132
    :cond_14
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->c(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->d(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-nez v0, :cond_15

    .line 133
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/video/a/a/b$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/video/a/a/b;->g(Lcom/bytedance/sdk/openadsdk/video/a/a/b;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    goto :goto_10

    .line 137
    :goto_f
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 139
    :cond_15
    :goto_10
    throw v2
.end method
