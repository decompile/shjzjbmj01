.class Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;
.super Lcom/bytedance/sdk/openadsdk/l/g;
.source "VideoPreload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/video/a/c/a;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/video/a/c/a;Ljava/lang/String;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/l/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    move-object/from16 v1, p0

    .line 66
    new-instance v0, Lcom/bytedance/sdk/a/b/v;

    invoke-direct {v0}, Lcom/bytedance/sdk/a/b/v;-><init>()V

    .line 67
    new-instance v2, Lcom/bytedance/sdk/a/b/y$a;

    invoke-direct {v2}, Lcom/bytedance/sdk/a/b/y$a;-><init>()V

    const-string v3, "RANGE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bytes=0-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    .line 68
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->b(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/a/b/y$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/a/b/y$a;

    move-result-object v2

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    .line 69
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->a(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/video/b/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/a/b/y$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/a/b/y$a;

    move-result-object v2

    .line 70
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/y$a;->a()Lcom/bytedance/sdk/a/b/y$a;

    move-result-object v2

    .line 71
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/y$a;->d()Lcom/bytedance/sdk/a/b/y;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 77
    :try_start_0
    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/a/b/v;->a(Lcom/bytedance/sdk/a/b/y;)Lcom/bytedance/sdk/a/b/e;

    move-result-object v0

    .line 78
    invoke-interface {v0}, Lcom/bytedance/sdk/a/b/e;->a()Lcom/bytedance/sdk/a/b/aa;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 79
    :try_start_1
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/aa;->d()Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v0, :cond_1

    if-eqz v2, :cond_0

    .line 123
    :try_start_2
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/aa;->close()V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_0
    :goto_0
    const-string v0, "VideoPreload"

    .line 125
    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "Pre finally "

    aput-object v3, v2, v8

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->a(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/video/b/a;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const-string v3, " Preload size="

    aput-object v3, v2, v5

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->b(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/video/d/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 127
    :goto_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :goto_2
    return-void

    .line 82
    :cond_1
    :try_start_3
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/aa;->h()Lcom/bytedance/sdk/a/b/ab;

    move-result-object v9
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 84
    :try_start_4
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/aa;->d()Z

    move-result v0

    .line 85
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/aa;->h()Lcom/bytedance/sdk/a/b/ab;

    move-result-object v10
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-wide/16 v11, 0x0

    if-eqz v0, :cond_2

    if-eqz v10, :cond_2

    .line 88
    :try_start_5
    invoke-virtual {v10}, Lcom/bytedance/sdk/a/b/ab;->b()J

    move-result-wide v13

    .line 89
    invoke-virtual {v10}, Lcom/bytedance/sdk/a/b/ab;->c()Ljava/io/InputStream;

    move-result-object v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-object v3, v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto/16 :goto_b

    :cond_2
    move-wide v13, v11

    :goto_3
    if-nez v3, :cond_6

    if-eqz v3, :cond_3

    .line 117
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_5

    :cond_3
    :goto_4
    if-eqz v10, :cond_4

    .line 120
    invoke-virtual {v10}, Lcom/bytedance/sdk/a/b/ab;->close()V

    :cond_4
    if-eqz v2, :cond_5

    .line 123
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/aa;->close()V

    :cond_5
    const-string v0, "VideoPreload"

    .line 125
    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "Pre finally "

    aput-object v3, v2, v8

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->a(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/video/b/a;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const-string v3, " Preload size="

    aput-object v3, v2, v5

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->b(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/video/d/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_6

    .line 127
    :goto_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :goto_6
    return-void

    .line 94
    :cond_6
    :try_start_7
    new-instance v0, Ljava/io/RandomAccessFile;

    iget-object v9, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v9}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->c(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)Ljava/io/File;

    move-result-object v9

    const-string v15, "rw"

    invoke-direct {v0, v9, v15}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/16 v9, 0x4000

    .line 96
    new-array v15, v9, [B

    move-wide/from16 v16, v11

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_7
    rsub-int v6, v4, 0x4000

    .line 99
    invoke-virtual {v3, v15, v4, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    const/4 v9, -0x1

    if-eq v6, v9, :cond_a

    .line 100
    iget-object v9, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v9}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->d(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)Z

    move-result v9

    if-eqz v9, :cond_7

    goto :goto_a

    :cond_7
    add-int/2addr v4, v6

    int-to-long v8, v6

    add-long v16, v16, v8

    const-wide/16 v8, 0x4000

    .line 105
    rem-long v8, v16, v8

    cmp-long v6, v8, v11

    if-eqz v6, :cond_9

    cmp-long v6, v16, v13

    if-nez v6, :cond_8

    goto :goto_9

    :cond_8
    :goto_8
    const/4 v8, 0x0

    const/16 v9, 0x4000

    goto :goto_7

    :cond_9
    :goto_9
    int-to-long v8, v5

    .line 106
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->intValue()I

    move-result v6

    iget-object v8, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v8}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->a(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v8

    invoke-virtual {v8}, Lcom/bytedance/sdk/openadsdk/video/b/a;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v15, v6, v4, v8}, Lcom/bytedance/sdk/openadsdk/video/d/c;->a(Ljava/io/RandomAccessFile;[BIILjava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    add-int/2addr v5, v4

    const/4 v4, 0x0

    goto :goto_8

    :cond_a
    :goto_a
    if-eqz v3, :cond_b

    .line 117
    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_b
    if-eqz v10, :cond_c

    .line 120
    invoke-virtual {v10}, Lcom/bytedance/sdk/a/b/ab;->close()V

    :cond_c
    if-eqz v2, :cond_d

    .line 123
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/aa;->close()V

    :cond_d
    const-string v0, "VideoPreload"

    .line 125
    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "Pre finally "

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->a(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/video/b/a;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-string v3, " Preload size="

    const/4 v4, 0x2

    aput-object v3, v2, v4

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->b(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/video/d/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_e

    :catchall_0
    move-exception v0

    move-object v10, v9

    goto :goto_f

    :catch_3
    move-exception v0

    move-object v10, v9

    goto :goto_b

    :catchall_1
    move-exception v0

    move-object v10, v3

    goto :goto_f

    :catch_4
    move-exception v0

    move-object v10, v3

    goto :goto_b

    :catchall_2
    move-exception v0

    move-object v2, v0

    move-object v0, v3

    move-object v10, v0

    goto :goto_10

    :catch_5
    move-exception v0

    move-object v2, v3

    move-object v10, v2

    .line 113
    :goto_b
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    if-eqz v3, :cond_e

    .line 117
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_c

    :catch_6
    move-exception v0

    goto :goto_d

    :cond_e
    :goto_c
    if-eqz v10, :cond_f

    .line 120
    invoke-virtual {v10}, Lcom/bytedance/sdk/a/b/ab;->close()V

    :cond_f
    if-eqz v2, :cond_10

    .line 123
    invoke-virtual {v2}, Lcom/bytedance/sdk/a/b/aa;->close()V

    :cond_10
    const-string v0, "VideoPreload"

    .line 125
    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "Pre finally "

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->a(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/video/b/a;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-string v3, " Preload size="

    const/4 v4, 0x2

    aput-object v3, v2, v4

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->b(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/video/d/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto :goto_e

    .line 127
    :goto_d
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :goto_e
    return-void

    :catchall_3
    move-exception v0

    :goto_f
    move-object/from16 v18, v2

    move-object v2, v0

    move-object/from16 v0, v18

    :goto_10
    if-eqz v3, :cond_11

    .line 117
    :try_start_b
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_11

    :catch_7
    move-exception v0

    goto :goto_12

    :cond_11
    :goto_11
    if-eqz v10, :cond_12

    .line 120
    invoke-virtual {v10}, Lcom/bytedance/sdk/a/b/ab;->close()V

    :cond_12
    if-eqz v0, :cond_13

    .line 123
    invoke-virtual {v0}, Lcom/bytedance/sdk/a/b/aa;->close()V

    :cond_13
    const-string v0, "VideoPreload"

    .line 125
    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "Pre finally "

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->a(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)Lcom/bytedance/sdk/openadsdk/video/b/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/video/b/a;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    const-string v4, " Preload size="

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/video/a/c/a$1;->a:Lcom/bytedance/sdk/openadsdk/video/a/c/a;

    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/video/a/c/a;->b(Lcom/bytedance/sdk/openadsdk/video/a/c/a;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x3

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/video/d/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    goto :goto_13

    .line 127
    :goto_12
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 130
    :goto_13
    throw v2
.end method
