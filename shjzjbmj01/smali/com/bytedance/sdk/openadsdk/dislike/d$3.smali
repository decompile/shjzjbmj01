.class Lcom/bytedance/sdk/openadsdk/dislike/d$3;
.super Ljava/lang/Object;
.source "TTDislikeDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/dislike/d;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/dislike/d;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/dislike/d;)V
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 161
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 162
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/dislike/d;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/bytedance/sdk/openadsdk/dislike/TTDislikeWebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v0, "title"

    .line 163
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/dislike/d;->c(Lcom/bytedance/sdk/openadsdk/dislike/d;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->am()Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "url"

    .line 164
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/dislike/d;->c(Lcom/bytedance/sdk/openadsdk/dislike/d;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->am()Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "tag"

    .line 165
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/dislike/d;->d(Lcom/bytedance/sdk/openadsdk/dislike/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "meta"

    .line 166
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/dislike/d;->c(Lcom/bytedance/sdk/openadsdk/dislike/d;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aF()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/dislike/d;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/utils/b;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/bytedance/sdk/openadsdk/utils/b$a;)Z

    .line 169
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/dislike/d;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/dislike/d;->c(Lcom/bytedance/sdk/openadsdk/dislike/d;)Lcom/bytedance/sdk/openadsdk/core/d/l;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/dislike/d$3;->a:Lcom/bytedance/sdk/openadsdk/dislike/d;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/dislike/d;->d(Lcom/bytedance/sdk/openadsdk/dislike/d;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ad_explation_click"

    invoke-static {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/c/d;->b(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
