.class public Lcom/bytedance/sdk/openadsdk/i/e;
.super Ljava/lang/Object;
.source "TTNetClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/i/e$a;
    }
.end annotation


# static fields
.field private static volatile a:Lcom/bytedance/sdk/openadsdk/i/e;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field

.field private static c:Lcom/bytedance/sdk/adnet/face/IHttpStack;


# instance fields
.field private b:Landroid/content/Context;

.field private volatile d:Lcom/bytedance/sdk/adnet/core/l;

.field private e:Lcom/bytedance/sdk/adnet/b/b;

.field private volatile f:Lcom/bytedance/sdk/adnet/core/l;

.field private volatile g:Lcom/bytedance/sdk/adnet/core/l;

.field private h:Lcom/bytedance/sdk/adnet/b/d;

.field private i:Lcom/bytedance/sdk/openadsdk/i/a/b;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    .line 69
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->b:Landroid/content/Context;

    return-void
.end method

.method public static a()Lcom/bytedance/sdk/adnet/face/IHttpStack;
    .locals 1

    .line 41
    sget-object v0, Lcom/bytedance/sdk/openadsdk/i/e;->c:Lcom/bytedance/sdk/adnet/face/IHttpStack;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/widget/ImageView;II)Lcom/bytedance/sdk/openadsdk/i/e$a;
    .locals 1

    .line 146
    new-instance v0, Lcom/bytedance/sdk/openadsdk/i/e$a;

    invoke-direct {v0, p1, p0, p2, p3}, Lcom/bytedance/sdk/openadsdk/i/e$a;-><init>(Landroid/widget/ImageView;Ljava/lang/String;II)V

    return-object v0
.end method

.method public static a(Lcom/bytedance/sdk/adnet/face/IHttpStack;)V
    .locals 0

    .line 45
    sput-object p0, Lcom/bytedance/sdk/openadsdk/i/e;->c:Lcom/bytedance/sdk/adnet/face/IHttpStack;

    return-void
.end method

.method public static b()Lcom/bytedance/sdk/adnet/core/e;
    .locals 1

    .line 54
    new-instance v0, Lcom/bytedance/sdk/adnet/core/e;

    invoke-direct {v0}, Lcom/bytedance/sdk/adnet/core/e;-><init>()V

    return-object v0
.end method

.method public static c()Lcom/bytedance/sdk/openadsdk/i/e;
    .locals 3

    .line 58
    sget-object v0, Lcom/bytedance/sdk/openadsdk/i/e;->a:Lcom/bytedance/sdk/openadsdk/i/e;

    if-nez v0, :cond_1

    .line 59
    const-class v0, Lcom/bytedance/sdk/openadsdk/i/e;

    monitor-enter v0

    .line 60
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/i/e;->a:Lcom/bytedance/sdk/openadsdk/i/e;

    if-nez v1, :cond_0

    .line 61
    new-instance v1, Lcom/bytedance/sdk/openadsdk/i/e;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/bytedance/sdk/openadsdk/i/e;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/i/e;->a:Lcom/bytedance/sdk/openadsdk/i/e;

    .line 63
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 65
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/i/e;->a:Lcom/bytedance/sdk/openadsdk/i/e;

    return-object v0
.end method

.method private i()V
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->i:Lcom/bytedance/sdk/openadsdk/i/a/b;

    if-nez v0, :cond_0

    .line 227
    new-instance v0, Lcom/bytedance/sdk/openadsdk/i/a/b;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/i/e;->d()Lcom/bytedance/sdk/adnet/core/l;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/bytedance/sdk/openadsdk/i/a/b;-><init>(Lcom/bytedance/sdk/adnet/core/l;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->i:Lcom/bytedance/sdk/openadsdk/i/a/b;

    :cond_0
    return-void
.end method

.method private j()V
    .locals 3

    .line 232
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->h:Lcom/bytedance/sdk/adnet/b/d;

    if-nez v0, :cond_0

    .line 233
    new-instance v0, Lcom/bytedance/sdk/adnet/b/d;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/i/e;->d()Lcom/bytedance/sdk/adnet/core/l;

    move-result-object v1

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/a;->a()Lcom/bytedance/sdk/openadsdk/i/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/adnet/b/d;-><init>(Lcom/bytedance/sdk/adnet/core/l;Lcom/bytedance/sdk/adnet/b/d$b;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->h:Lcom/bytedance/sdk/adnet/b/d;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 1

    const/4 v0, 0x0

    .line 123
    invoke-static {p1, p2, v0, v0}, Lcom/bytedance/sdk/openadsdk/i/e;->a(Ljava/lang/String;Landroid/widget/ImageView;II)Lcom/bytedance/sdk/openadsdk/i/e$a;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/i/e;->a(Ljava/lang/String;Landroid/widget/ImageView;Lcom/bytedance/sdk/adnet/b/d$e;)V

    return-void
.end method

.method public a(Ljava/lang/String;Landroid/widget/ImageView;Lcom/bytedance/sdk/adnet/b/d$e;)V
    .locals 0

    .line 127
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/i/e;->j()V

    .line 128
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/i/e;->h:Lcom/bytedance/sdk/adnet/b/d;

    invoke-virtual {p2, p1, p3}, Lcom/bytedance/sdk/adnet/b/d;->a(Ljava/lang/String;Lcom/bytedance/sdk/adnet/b/d$e;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/bytedance/sdk/adnet/b/b$a;)V
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->e:Lcom/bytedance/sdk/adnet/b/b;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/bytedance/sdk/adnet/b/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/i/e;->d()Lcom/bytedance/sdk/adnet/core/l;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/bytedance/sdk/adnet/b/b;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/adnet/core/l;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->e:Lcom/bytedance/sdk/adnet/b/b;

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->e:Lcom/bytedance/sdk/adnet/b/b;

    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/adnet/b/b;->a(Ljava/lang/String;Lcom/bytedance/sdk/adnet/b/b$a;)V

    return-void
.end method

.method public d()Lcom/bytedance/sdk/adnet/core/l;
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->d:Lcom/bytedance/sdk/adnet/core/l;

    if-nez v0, :cond_1

    .line 81
    const-class v0, Lcom/bytedance/sdk/openadsdk/i/e;

    monitor-enter v0

    .line 82
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->d:Lcom/bytedance/sdk/adnet/core/l;

    if-nez v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/adnet/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/adnet/core/l;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->d:Lcom/bytedance/sdk/adnet/core/l;

    .line 85
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 87
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->d:Lcom/bytedance/sdk/adnet/core/l;

    return-object v0
.end method

.method public e()Lcom/bytedance/sdk/adnet/core/l;
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->g:Lcom/bytedance/sdk/adnet/core/l;

    if-nez v0, :cond_1

    .line 92
    const-class v0, Lcom/bytedance/sdk/openadsdk/i/e;

    monitor-enter v0

    .line 93
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->g:Lcom/bytedance/sdk/adnet/core/l;

    if-nez v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/adnet/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/adnet/core/l;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->g:Lcom/bytedance/sdk/adnet/core/l;

    .line 96
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 98
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->g:Lcom/bytedance/sdk/adnet/core/l;

    return-object v0
.end method

.method public f()Lcom/bytedance/sdk/adnet/core/l;
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->f:Lcom/bytedance/sdk/adnet/core/l;

    if-nez v0, :cond_1

    .line 103
    const-class v0, Lcom/bytedance/sdk/openadsdk/i/e;

    monitor-enter v0

    .line 104
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->f:Lcom/bytedance/sdk/adnet/core/l;

    if-nez v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/bytedance/sdk/adnet/a;->a(Landroid/content/Context;)Lcom/bytedance/sdk/adnet/core/l;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/e;->f:Lcom/bytedance/sdk/adnet/core/l;

    .line 107
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 109
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->f:Lcom/bytedance/sdk/adnet/core/l;

    return-object v0
.end method

.method public g()Lcom/bytedance/sdk/openadsdk/i/a/b;
    .locals 1

    .line 113
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/i/e;->i()V

    .line 114
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->i:Lcom/bytedance/sdk/openadsdk/i/a/b;

    return-object v0
.end method

.method public h()Lcom/bytedance/sdk/adnet/b/d;
    .locals 1

    .line 118
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/i/e;->j()V

    .line 119
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/e;->h:Lcom/bytedance/sdk/adnet/b/d;

    return-object v0
.end method
