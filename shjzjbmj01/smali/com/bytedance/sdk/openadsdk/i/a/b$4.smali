.class Lcom/bytedance/sdk/openadsdk/i/a/b$4;
.super Ljava/lang/Object;
.source "GifLoader.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/i/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/i/a/b;->a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/i/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/bytedance/sdk/openadsdk/i/a/b;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/i/a/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->c:Lcom/bytedance/sdk/openadsdk/i/a/b;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/adnet/core/m;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/adnet/core/m<",
            "Lcom/bytedance/sdk/openadsdk/i/a/d;",
            ">;)V"
        }
    .end annotation

    .line 185
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->c:Lcom/bytedance/sdk/openadsdk/i/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/i/a/b;->a(Lcom/bytedance/sdk/openadsdk/i/a/b;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/i/a/b$d;

    if-eqz v0, :cond_0

    .line 187
    iput-object p1, v0, Lcom/bytedance/sdk/openadsdk/i/a/b$d;->b:Lcom/bytedance/sdk/adnet/core/m;

    .line 188
    iget-object p1, p1, Lcom/bytedance/sdk/adnet/core/m;->a:Ljava/lang/Object;

    check-cast p1, Lcom/bytedance/sdk/openadsdk/i/a/d;

    iput-object p1, v0, Lcom/bytedance/sdk/openadsdk/i/a/b$d;->e:Lcom/bytedance/sdk/openadsdk/i/a/d;

    .line 189
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->c:Lcom/bytedance/sdk/openadsdk/i/a/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->b:Ljava/lang/String;

    invoke-static {p1, v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/i/a/b;->a(Lcom/bytedance/sdk/openadsdk/i/a/b;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/i/a/b$d;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/i/a/d;)V
    .locals 3

    .line 167
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->c:Lcom/bytedance/sdk/openadsdk/i/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/i/a/b;->a(Lcom/bytedance/sdk/openadsdk/i/a/b;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/i/a/b$d;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/i/a/b$d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/i/a/b$b;

    if-eqz v1, :cond_0

    const/4 v2, 0x2

    .line 171
    sput v2, Lcom/bytedance/sdk/openadsdk/i/a/b;->a:I

    .line 172
    invoke-interface {v1, p1, p2}, Lcom/bytedance/sdk/openadsdk/i/a/b$b;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/i/a/d;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public b(Lcom/bytedance/sdk/adnet/core/m;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/adnet/core/m<",
            "Lcom/bytedance/sdk/openadsdk/i/a/d;",
            ">;)V"
        }
    .end annotation

    .line 195
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->c:Lcom/bytedance/sdk/openadsdk/i/a/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/i/a/b;->a(Lcom/bytedance/sdk/openadsdk/i/a/b;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/i/a/b$d;

    if-eqz v0, :cond_0

    .line 197
    iput-object p1, v0, Lcom/bytedance/sdk/openadsdk/i/a/b$d;->b:Lcom/bytedance/sdk/adnet/core/m;

    .line 198
    iget-object p1, p1, Lcom/bytedance/sdk/adnet/core/m;->c:Lcom/bytedance/sdk/adnet/err/VAdError;

    iput-object p1, v0, Lcom/bytedance/sdk/openadsdk/i/a/b$d;->d:Lcom/bytedance/sdk/adnet/err/VAdError;

    .line 199
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->c:Lcom/bytedance/sdk/openadsdk/i/a/b;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/i/a/b$4;->b:Ljava/lang/String;

    invoke-static {p1, v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/i/a/b;->a(Lcom/bytedance/sdk/openadsdk/i/a/b;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/i/a/b$d;)V

    :cond_0
    return-void
.end method
