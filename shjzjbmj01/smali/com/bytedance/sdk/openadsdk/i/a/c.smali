.class public Lcom/bytedance/sdk/openadsdk/i/a/c;
.super Lcom/bytedance/sdk/adnet/core/Request;
.source "GifRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/i/a/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bytedance/sdk/adnet/core/Request<",
        "Lcom/bytedance/sdk/openadsdk/i/a/d;",
        ">;"
    }
.end annotation


# static fields
.field private static final k:Ljava/lang/Object;


# instance fields
.field private final c:Ljava/lang/Object;

.field private d:Lcom/bytedance/sdk/openadsdk/i/a/c$a;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation

    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final e:Landroid/graphics/Bitmap$Config;

.field private final f:I

.field private final g:I

.field private final h:Landroid/widget/ImageView$ScaleType;

.field private i:Lcom/bytedance/sdk/openadsdk/core/d/q;

.field private j:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/i/a/c;->k:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/i/a/c$a;IILandroid/widget/ImageView$ScaleType;Landroid/graphics/Bitmap$Config;)V
    .locals 4

    const/4 v0, 0x0

    .line 93
    invoke-direct {p0, v0, p1, p2}, Lcom/bytedance/sdk/adnet/core/Request;-><init>(ILjava/lang/String;Lcom/bytedance/sdk/adnet/core/m$a;)V

    .line 49
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->c:Ljava/lang/Object;

    .line 94
    new-instance p1, Lcom/bytedance/sdk/adnet/core/e;

    const/16 v1, 0x3e8

    const/4 v2, 0x2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {p1, v1, v2, v3}, Lcom/bytedance/sdk/adnet/core/e;-><init>(IIF)V

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/i/a/c;->setRetryPolicy(Lcom/bytedance/sdk/adnet/face/d;)Lcom/bytedance/sdk/adnet/core/Request;

    .line 99
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->d:Lcom/bytedance/sdk/openadsdk/i/a/c$a;

    .line 100
    iput-object p6, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->e:Landroid/graphics/Bitmap$Config;

    .line 101
    iput p3, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->f:I

    .line 102
    iput p4, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->g:I

    .line 103
    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->h:Landroid/widget/ImageView$ScaleType;

    .line 105
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/i/a/c;->setShouldCache(Z)Lcom/bytedance/sdk/adnet/core/Request;

    return-void
.end method

.method static a(IIII)I
    .locals 4
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    int-to-double v0, p0

    int-to-double v2, p2

    .line 401
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v0, v2

    int-to-double p0, p1

    int-to-double p2, p3

    .line 402
    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr p0, p2

    .line 403
    invoke-static {v0, v1, p0, p1}, Ljava/lang/Math;->min(DD)D

    move-result-wide p0

    const/high16 p2, 0x3f800000    # 1.0f

    :goto_0
    const/high16 p3, 0x40000000    # 2.0f

    mul-float p3, p3, p2

    float-to-double v0, p3

    cmpg-double v2, v0, p0

    if-gtz v2, :cond_0

    move p2, p3

    goto :goto_0

    :cond_0
    float-to-int p0, p2

    return p0
.end method

.method private static a(IIIILandroid/widget/ImageView$ScaleType;)I
    .locals 4

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    return p2

    .line 157
    :cond_0
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    if-ne p4, v0, :cond_2

    if-nez p0, :cond_1

    return p2

    :cond_1
    return p0

    :cond_2
    if-nez p0, :cond_3

    int-to-double p0, p1

    int-to-double p3, p3

    .line 166
    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    invoke-static {p3, p4}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr p0, p3

    int-to-double p2, p2

    .line 167
    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    mul-double p2, p2, p0

    double-to-int p0, p2

    return p0

    :cond_3
    if-nez p1, :cond_4

    return p0

    :cond_4
    int-to-double v0, p3

    int-to-double p2, p2

    .line 174
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v0, p2

    .line 178
    sget-object p2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    if-ne p4, p2, :cond_6

    int-to-double p2, p0

    .line 179
    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    mul-double p2, p2, v0

    int-to-double v2, p1

    cmpg-double p1, p2, v2

    if-gez p1, :cond_5

    .line 180
    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v2, v0

    double-to-int p0, v2

    :cond_5
    return p0

    :cond_6
    int-to-double p2, p0

    .line 185
    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    mul-double p2, p2, v0

    int-to-double v2, p1

    cmpl-double p1, p2, v2

    if-lez p1, :cond_7

    .line 186
    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v2, v0

    double-to-int p0, v2

    :cond_7
    return p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/i/a/c;)Lcom/bytedance/sdk/openadsdk/i/a/c$a;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->d:Lcom/bytedance/sdk/openadsdk/i/a/c$a;

    return-object p0
.end method

.method private a(J[BLandroid/graphics/Bitmap;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J[B",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 353
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    if-nez v0, :cond_0

    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->z()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    if-eqz p3, :cond_2

    .line 359
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->i()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-nez v4, :cond_2

    .line 360
    array-length p3, p3

    int-to-float p3, p3

    const/high16 v0, 0x44800000    # 1024.0f

    div-float/2addr p3, v0

    float-to-double v0, p3

    .line 361
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {p3, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(D)V

    :cond_2
    if-eqz p4, :cond_3

    .line 364
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/d/q;->j()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 365
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "X"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 366
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {p4, p3}, Lcom/bytedance/sdk/openadsdk/core/d/q;->b(Ljava/lang/String;)V

    :cond_3
    if-eqz p5, :cond_6

    .line 369
    invoke-interface {p5}, Ljava/util/Map;->size()I

    move-result p3

    if-lez p3, :cond_6

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/d/q;->k()Lorg/json/JSONObject;

    move-result-object p3

    if-nez p3, :cond_6

    .line 370
    new-instance p3, Lorg/json/JSONObject;

    invoke-direct {p3}, Lorg/json/JSONObject;-><init>()V

    .line 371
    invoke-interface {p5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p4

    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 372
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_0

    .line 376
    :cond_4
    :try_start_0
    invoke-interface {p5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GifRequest"

    .line 378
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 381
    :cond_5
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {p4, p3}, Lcom/bytedance/sdk/openadsdk/core/d/q;->a(Lorg/json/JSONObject;)V

    .line 384
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p3

    .line 385
    iget-object p5, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    sub-long/2addr p3, p1

    invoke-virtual {p5, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/d/q;->j(J)V

    .line 386
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/core/d/q;->t()J

    move-result-wide p4

    sub-long/2addr p2, p4

    invoke-virtual {p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/d/q;->b(J)V

    return-void
.end method

.method private b(Lcom/bytedance/sdk/adnet/core/i;)Lcom/bytedance/sdk/adnet/core/m;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/adnet/core/i;",
            ")",
            "Lcom/bytedance/sdk/adnet/core/m<",
            "Lcom/bytedance/sdk/openadsdk/i/a/d;",
            ">;"
        }
    .end annotation

    .line 208
    iget-object v0, p1, Lcom/bytedance/sdk/adnet/core/i;->b:[B

    .line 209
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/i/a/c;->f()V

    .line 210
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/a/a;->a()Lcom/bytedance/sdk/openadsdk/i/a/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/i/a/c;->getUrl()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->f:I

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->g:I

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->h:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/i/a/a;->a(Ljava/lang/String;IILandroid/widget/ImageView$ScaleType;)Ljava/lang/String;

    move-result-object v1

    .line 213
    array-length v2, v0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x3

    if-lt v2, v5, :cond_1

    aget-byte v2, v0, v4

    const/16 v5, 0x47

    if-ne v2, v5, :cond_1

    aget-byte v2, v0, v3

    const/16 v5, 0x49

    if-ne v2, v5, :cond_1

    const/4 v2, 0x2

    aget-byte v2, v0, v2

    const/16 v5, 0x46

    if-ne v2, v5, :cond_1

    .line 216
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/a/a;->a()Lcom/bytedance/sdk/openadsdk/i/a/a;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/bytedance/sdk/openadsdk/i/a/a;->a(Ljava/lang/String;[B)V

    .line 217
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->d:Lcom/bytedance/sdk/openadsdk/i/a/c$a;

    if-eqz v2, :cond_0

    .line 218
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->b:Landroid/os/Handler;

    new-instance v5, Lcom/bytedance/sdk/openadsdk/i/a/c$1;

    invoke-direct {v5, p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/i/a/c$1;-><init>(Lcom/bytedance/sdk/openadsdk/i/a/c;[BLcom/bytedance/sdk/adnet/core/i;)V

    invoke-virtual {v2, v5}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 231
    :cond_0
    new-instance v2, Lcom/bytedance/sdk/openadsdk/i/a/d;

    invoke-direct {v2, v0}, Lcom/bytedance/sdk/openadsdk/i/a/d;-><init>([B)V

    .line 232
    iget-object v5, p1, Lcom/bytedance/sdk/adnet/core/i;->d:Ljava/util/List;

    invoke-virtual {v2, v5}, Lcom/bytedance/sdk/openadsdk/i/a/d;->a(Ljava/util/List;)V

    .line 233
    iget-object v5, p1, Lcom/bytedance/sdk/adnet/core/i;->c:Ljava/util/Map;

    invoke-virtual {v2, v5}, Lcom/bytedance/sdk/openadsdk/i/a/d;->a(Ljava/util/Map;)V

    .line 234
    invoke-static {p1}, Lcom/bytedance/sdk/adnet/d/c;->a(Lcom/bytedance/sdk/adnet/core/i;)Lcom/bytedance/sdk/adnet/face/a$a;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/bytedance/sdk/adnet/core/m;->a(Ljava/lang/Object;Lcom/bytedance/sdk/adnet/face/a$a;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 239
    :catch_0
    :cond_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 241
    iget v5, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->f:I

    if-nez v5, :cond_3

    iget v5, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->g:I

    if-nez v5, :cond_3

    .line 242
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->e:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 243
    array-length v3, v0

    invoke-static {v0, v4, v3, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_2
    move-object v7, v0

    goto :goto_0

    .line 246
    :cond_3
    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 247
    array-length v5, v0

    invoke-static {v0, v4, v5, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 248
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 249
    iget v6, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 252
    iget v7, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->f:I

    iget v8, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->g:I

    iget-object v9, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->h:Landroid/widget/ImageView$ScaleType;

    .line 253
    invoke-static {v7, v8, v5, v6, v9}, Lcom/bytedance/sdk/openadsdk/i/a/c;->a(IIIILandroid/widget/ImageView$ScaleType;)I

    move-result v7

    .line 255
    iget v8, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->g:I

    iget v9, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->f:I

    iget-object v10, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->h:Landroid/widget/ImageView$ScaleType;

    .line 256
    invoke-static {v8, v9, v6, v5, v10}, Lcom/bytedance/sdk/openadsdk/i/a/c;->a(IIIILandroid/widget/ImageView$ScaleType;)I

    move-result v8

    .line 260
    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 264
    invoke-static {v5, v6, v7, v8}, Lcom/bytedance/sdk/openadsdk/i/a/c;->a(IIII)I

    move-result v5

    iput v5, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 265
    array-length v5, v0

    invoke-static {v0, v4, v5, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 269
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-gt v2, v7, :cond_4

    .line 270
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v2, v8, :cond_2

    .line 271
    :cond_4
    invoke-static {v0, v7, v8, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 272
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move-object v7, v2

    :goto_0
    if-nez v7, :cond_5

    .line 279
    new-instance v0, Lcom/bytedance/sdk/adnet/err/e;

    invoke-direct {v0, p1}, Lcom/bytedance/sdk/adnet/err/e;-><init>(Lcom/bytedance/sdk/adnet/core/i;)V

    invoke-static {v0}, Lcom/bytedance/sdk/adnet/core/m;->a(Lcom/bytedance/sdk/adnet/err/VAdError;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object p1

    return-object p1

    .line 282
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->d:Lcom/bytedance/sdk/openadsdk/i/a/c$a;

    if-eqz v0, :cond_6

    .line 285
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->b:Landroid/os/Handler;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/i/a/c$2;

    invoke-direct {v2, p0, v7, p1}, Lcom/bytedance/sdk/openadsdk/i/a/c$2;-><init>(Lcom/bytedance/sdk/openadsdk/i/a/c;Landroid/graphics/Bitmap;Lcom/bytedance/sdk/adnet/core/i;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 299
    :cond_6
    invoke-static {v7}, Lcom/bytedance/sdk/openadsdk/utils/f;->b(Landroid/graphics/Bitmap;)[B

    move-result-object v0

    .line 300
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/a/a;->a()Lcom/bytedance/sdk/openadsdk/i/a/a;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/bytedance/sdk/openadsdk/i/a/a;->a(Ljava/lang/String;[B)V

    .line 301
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->d:Lcom/bytedance/sdk/openadsdk/i/a/c$a;

    if-eqz v1, :cond_7

    .line 302
    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->j:J

    iget-object v8, p1, Lcom/bytedance/sdk/adnet/core/i;->c:Ljava/util/Map;

    move-object v3, p0

    move-object v6, v0

    invoke-direct/range {v3 .. v8}, Lcom/bytedance/sdk/openadsdk/i/a/c;->a(J[BLandroid/graphics/Bitmap;Ljava/util/Map;)V

    .line 303
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->d:Lcom/bytedance/sdk/openadsdk/i/a/c$a;

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/i/a/c$a;->a()V

    .line 306
    :cond_7
    new-instance v1, Lcom/bytedance/sdk/openadsdk/i/a/d;

    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/i/a/d;-><init>([B)V

    .line 307
    iget-object v0, p1, Lcom/bytedance/sdk/adnet/core/i;->d:Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/i/a/d;->a(Ljava/util/List;)V

    .line 308
    iget-object v0, p1, Lcom/bytedance/sdk/adnet/core/i;->c:Ljava/util/Map;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/i/a/d;->a(Ljava/util/Map;)V

    .line 309
    invoke-static {p1}, Lcom/bytedance/sdk/adnet/d/c;->a(Lcom/bytedance/sdk/adnet/core/i;)Lcom/bytedance/sdk/adnet/face/a$a;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/sdk/adnet/core/m;->a(Ljava/lang/Object;Lcom/bytedance/sdk/adnet/face/a$a;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object p1

    return-object p1
.end method

.method private f()V
    .locals 5

    .line 341
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    if-nez v0, :cond_0

    return-void

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/q;->z()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 347
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->j:J

    .line 348
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->j:J

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/q;->p(J)V

    .line 349
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->j:J

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/d/q;->v()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/q;->i(J)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/bytedance/sdk/adnet/core/i;)Lcom/bytedance/sdk/adnet/core/m;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/adnet/core/i;",
            ")",
            "Lcom/bytedance/sdk/adnet/core/m<",
            "Lcom/bytedance/sdk/openadsdk/i/a/d;",
            ">;"
        }
    .end annotation

    .line 194
    sget-object v0, Lcom/bytedance/sdk/openadsdk/i/a/c;->k:Ljava/lang/Object;

    monitor-enter v0

    .line 196
    :try_start_0
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/i/a/c;->b(Lcom/bytedance/sdk/adnet/core/i;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v1, "GifRequest"

    const-string v2, "Caught OOM for byte image"

    .line 198
    invoke-static {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 199
    new-instance v1, Lcom/bytedance/sdk/adnet/err/e;

    const/16 v2, 0x264

    invoke-direct {v1, p1, v2}, Lcom/bytedance/sdk/adnet/err/e;-><init>(Ljava/lang/Throwable;I)V

    invoke-static {v1}, Lcom/bytedance/sdk/adnet/core/m;->a(Lcom/bytedance/sdk/adnet/err/VAdError;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object p1

    monitor-exit v0

    return-object p1

    .line 201
    :goto_0
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method protected a(Lcom/bytedance/sdk/adnet/core/m;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/adnet/core/m<",
            "Lcom/bytedance/sdk/openadsdk/i/a/d;",
            ">;)V"
        }
    .end annotation

    .line 324
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->c:Ljava/lang/Object;

    monitor-enter v0

    .line 325
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->d:Lcom/bytedance/sdk/openadsdk/i/a/c$a;

    .line 326
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 328
    invoke-interface {v1, p1}, Lcom/bytedance/sdk/adnet/core/m$a;->a(Lcom/bytedance/sdk/adnet/core/m;)V

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    .line 326
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/q;)V
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->i:Lcom/bytedance/sdk/openadsdk/core/d/q;

    return-void
.end method

.method public cancel()V
    .locals 2

    .line 315
    invoke-super {p0}, Lcom/bytedance/sdk/adnet/core/Request;->cancel()V

    .line 316
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->c:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 317
    :try_start_0
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/i/a/c;->d:Lcom/bytedance/sdk/openadsdk/i/a/c$a;

    .line 318
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getPriority()Lcom/bytedance/sdk/adnet/core/Request$b;
    .locals 1

    .line 130
    sget-object v0, Lcom/bytedance/sdk/adnet/core/Request$b;->a:Lcom/bytedance/sdk/adnet/core/Request$b;

    return-object v0
.end method
