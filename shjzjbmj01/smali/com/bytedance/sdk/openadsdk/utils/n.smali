.class public Lcom/bytedance/sdk/openadsdk/utils/n;
.super Landroid/view/TouchDelegate;
.source "HackTouchDelegate.java"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/graphics/Rect;

.field private c:Landroid/graphics/Rect;

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>(Landroid/graphics/Rect;Landroid/view/View;)V
    .locals 2

    .line 80
    invoke-direct {p0, p1, p2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 81
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->b:Landroid/graphics/Rect;

    .line 83
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->e:I

    .line 84
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->c:Landroid/graphics/Rect;

    .line 85
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->c:Landroid/graphics/Rect;

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->e:I

    neg-int v0, v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->e:I

    neg-int v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->inset(II)V

    .line 86
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->a:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 97
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 98
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 103
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x0

    goto :goto_1

    .line 128
    :pswitch_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->d:Z

    .line 129
    iput-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->d:Z

    move v2, v0

    goto :goto_1

    .line 119
    :pswitch_1
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->d:Z

    if-eqz v2, :cond_1

    .line 121
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->c:Landroid/graphics/Rect;

    .line 122
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    goto :goto_1

    .line 105
    :pswitch_2
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->b:Landroid/graphics/Rect;

    .line 107
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->d:Z

    const/4 v2, 0x1

    goto :goto_1

    .line 113
    :cond_0
    iput-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->d:Z

    goto :goto_0

    :cond_1
    :goto_1
    if-eqz v2, :cond_3

    .line 133
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->a:Landroid/view/View;

    if-eqz v3, :cond_2

    .line 137
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_2

    .line 141
    :cond_2
    iget v1, p0, Lcom/bytedance/sdk/openadsdk/utils/n;->e:I

    mul-int/lit8 v1, v1, 0x2

    neg-int v1, v1

    int-to-float v1, v1

    .line 142
    invoke-virtual {p1, v1, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 144
    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 145
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    :cond_3
    return v4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
