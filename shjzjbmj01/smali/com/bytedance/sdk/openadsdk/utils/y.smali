.class public Lcom/bytedance/sdk/openadsdk/utils/y;
.super Ljava/lang/Object;
.source "OAIDHelper.java"


# static fields
.field private static volatile a:Ljava/lang/String; = ""

.field private static volatile b:Z = false


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 28
    :try_start_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/core/d;

    move-result-object v0

    const-string v1, "oaid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 3

    .line 55
    sget-object v0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "sdk_app_log_oaid"

    const-wide/32 v1, 0x5265c00

    .line 56
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/i;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    .line 58
    :cond_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/utils/y;->b:Z

    if-nez v0, :cond_1

    .line 60
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i;->e()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 61
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->getDevOaid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 62
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->getDevOaid()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    .line 63
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/y;->c()V

    .line 66
    :cond_1
    sget-object v0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 20
    sput-object p0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    return-object p0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    .line 37
    :try_start_0
    new-instance p0, Lcom/bytedance/sdk/openadsdk/utils/y$1;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/utils/y$1;-><init>()V

    invoke-static {p0}, Lcom/bytedance/embedapplog/AppLog;->setOaidObserver(Lcom/bytedance/embedapplog/IOaidObserver;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method static synthetic a(Z)Z
    .locals 0

    .line 20
    sput-boolean p0, Lcom/bytedance/sdk/openadsdk/utils/y;->b:Z

    return p0
.end method

.method static synthetic b()V
    .locals 0

    .line 20
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/y;->c()V

    return-void
.end method

.method private static c()V
    .locals 2

    .line 70
    sget-object v0, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "sdk_app_log_oaid"

    .line 72
    sget-object v1, Lcom/bytedance/sdk/openadsdk/utils/y;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
