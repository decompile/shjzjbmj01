.class Lcom/bytedance/sdk/openadsdk/f/a/g;
.super Ljava/lang/Object;
.source "CallHandler.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/f/a/v$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/f/a/g$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/bytedance/sdk/openadsdk/f/a/h;

.field private final b:Lcom/bytedance/sdk/openadsdk/f/a/t;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/f/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/f/a/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/f/a/p;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/bytedance/sdk/openadsdk/f/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/bytedance/sdk/openadsdk/f/a/m;

.field private final h:Z

.field private final i:Z

.field private final j:Lcom/bytedance/sdk/openadsdk/f/a/a;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/f/a/j;Lcom/bytedance/sdk/openadsdk/f/a/a;Lcom/bytedance/sdk/openadsdk/f/a/u;)V
    .locals 2
    .param p1    # Lcom/bytedance/sdk/openadsdk/f/a/j;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/bytedance/sdk/openadsdk/f/a/u;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->c:Ljava/util/Map;

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->d:Ljava/util/Map;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->e:Ljava/util/List;

    .line 28
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->f:Ljava/util/Set;

    .line 37
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->j:Lcom/bytedance/sdk/openadsdk/f/a/a;

    .line 38
    iget-object p2, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->d:Lcom/bytedance/sdk/openadsdk/f/a/h;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->a:Lcom/bytedance/sdk/openadsdk/f/a/h;

    .line 39
    new-instance p2, Lcom/bytedance/sdk/openadsdk/f/a/t;

    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->l:Ljava/util/Set;

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->m:Ljava/util/Set;

    invoke-direct {p2, p3, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/t;-><init>(Lcom/bytedance/sdk/openadsdk/f/a/u;Ljava/util/Set;Ljava/util/Set;)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->b:Lcom/bytedance/sdk/openadsdk/f/a/t;

    .line 40
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->b:Lcom/bytedance/sdk/openadsdk/f/a/t;

    invoke-virtual {p2, p0}, Lcom/bytedance/sdk/openadsdk/f/a/t;->a(Lcom/bytedance/sdk/openadsdk/f/a/v$a;)V

    .line 41
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->b:Lcom/bytedance/sdk/openadsdk/f/a/t;

    iget-object p3, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->p:Lcom/bytedance/sdk/openadsdk/f/a/k$b;

    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/f/a/t;->a(Lcom/bytedance/sdk/openadsdk/f/a/k$b;)V

    .line 42
    iget-object p2, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->i:Lcom/bytedance/sdk/openadsdk/f/a/m;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->g:Lcom/bytedance/sdk/openadsdk/f/a/m;

    .line 43
    iget-boolean p2, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->h:Z

    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->h:Z

    .line 44
    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->o:Z

    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->i:Z

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/f/a/g;)Lcom/bytedance/sdk/openadsdk/f/a/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->j:Lcom/bytedance/sdk/openadsdk/f/a/a;

    return-object p0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/f/a/p;Lcom/bytedance/sdk/openadsdk/f/a/c;Lcom/bytedance/sdk/openadsdk/f/a/w;)Lcom/bytedance/sdk/openadsdk/f/a/g$a;
    .locals 3
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 177
    new-instance v0, Lcom/bytedance/sdk/openadsdk/f/a/s;

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/f/a/p;->d:Ljava/lang/String;

    new-instance v2, Lcom/bytedance/sdk/openadsdk/f/a/g$2;

    invoke-direct {v2, p0, p1}, Lcom/bytedance/sdk/openadsdk/f/a/g$2;-><init>(Lcom/bytedance/sdk/openadsdk/f/a/g;Lcom/bytedance/sdk/openadsdk/f/a/p;)V

    invoke-direct {v0, v1, p3, v2}, Lcom/bytedance/sdk/openadsdk/f/a/s;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/w;Lcom/bytedance/sdk/openadsdk/f/a/s$a;)V

    invoke-virtual {p2, p1, v0}, Lcom/bytedance/sdk/openadsdk/f/a/c;->a(Lcom/bytedance/sdk/openadsdk/f/a/p;Lcom/bytedance/sdk/openadsdk/f/a/s;)V

    .line 185
    new-instance p1, Lcom/bytedance/sdk/openadsdk/f/a/g$a;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/f/a/x;->a()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    const/4 v0, 0x0

    invoke-direct {p1, p3, p2, v0}, Lcom/bytedance/sdk/openadsdk/f/a/g$a;-><init>(ZLjava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/g$1;)V

    return-object p1
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/f/a/p;Lcom/bytedance/sdk/openadsdk/f/a/d;Lcom/bytedance/sdk/openadsdk/f/a/f;)Lcom/bytedance/sdk/openadsdk/f/a/g$a;
    .locals 2
    .param p1    # Lcom/bytedance/sdk/openadsdk/f/a/p;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/bytedance/sdk/openadsdk/f/a/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->f:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/f/a/p;->e:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/bytedance/sdk/openadsdk/f/a/g;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/b;)Ljava/lang/Object;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/a/g$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/f/a/g$1;-><init>(Lcom/bytedance/sdk/openadsdk/f/a/g;Lcom/bytedance/sdk/openadsdk/f/a/p;Lcom/bytedance/sdk/openadsdk/f/a/d;)V

    invoke-virtual {p2, v0, p3, v1}, Lcom/bytedance/sdk/openadsdk/f/a/d;->a(Ljava/lang/Object;Lcom/bytedance/sdk/openadsdk/f/a/f;Lcom/bytedance/sdk/openadsdk/f/a/d$a;)V

    .line 171
    new-instance p1, Lcom/bytedance/sdk/openadsdk/f/a/g$a;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/f/a/x;->a()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    const/4 v0, 0x0

    invoke-direct {p1, p3, p2, v0}, Lcom/bytedance/sdk/openadsdk/f/a/g$a;-><init>(ZLjava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/g$1;)V

    return-object p1
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/f/a/p;Lcom/bytedance/sdk/openadsdk/f/a/e;Lcom/bytedance/sdk/openadsdk/f/a/f;)Lcom/bytedance/sdk/openadsdk/f/a/g$a;
    .locals 1
    .param p1    # Lcom/bytedance/sdk/openadsdk/f/a/p;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/bytedance/sdk/openadsdk/f/a/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 143
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/f/a/p;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/f/a/g;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/b;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p2, p1, p3}, Lcom/bytedance/sdk/openadsdk/f/a/e;->a(Ljava/lang/Object;Lcom/bytedance/sdk/openadsdk/f/a/f;)Ljava/lang/Object;

    move-result-object p1

    .line 144
    new-instance p2, Lcom/bytedance/sdk/openadsdk/f/a/g$a;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->a:Lcom/bytedance/sdk/openadsdk/f/a/h;

    invoke-virtual {p3, p1}, Lcom/bytedance/sdk/openadsdk/f/a/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/f/a/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 p3, 0x1

    const/4 v0, 0x0

    invoke-direct {p2, p3, p1, v0}, Lcom/bytedance/sdk/openadsdk/f/a/g$a;-><init>(ZLjava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/g$1;)V

    return-object p2
.end method

.method private a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/b;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 199
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->a:Lcom/bytedance/sdk/openadsdk/f/a/h;

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/f/a/g;->a(Ljava/lang/Object;)[Ljava/lang/reflect/Type;

    move-result-object p2

    const/4 v1, 0x0

    aget-object p2, p2, v1

    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/f/a/h;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method private static a(Ljava/lang/Object;)[Ljava/lang/reflect/Type;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 211
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 215
    check-cast p0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object p0

    return-object p0

    .line 213
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Method is not parameterized?!"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/f/a/g;)Lcom/bytedance/sdk/openadsdk/f/a/h;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->a:Lcom/bytedance/sdk/openadsdk/f/a/h;

    return-object p0
.end method

.method private b(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/b;)Lcom/bytedance/sdk/openadsdk/f/a/w;
    .locals 2

    .line 203
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->i:Z

    if-eqz v0, :cond_0

    .line 204
    sget-object p1, Lcom/bytedance/sdk/openadsdk/f/a/w;->c:Lcom/bytedance/sdk/openadsdk/f/a/w;

    return-object p1

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->b:Lcom/bytedance/sdk/openadsdk/f/a/t;

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->h:Z

    invoke-virtual {v0, v1, p1, p2}, Lcom/bytedance/sdk/openadsdk/f/a/t;->a(ZLjava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/b;)Lcom/bytedance/sdk/openadsdk/f/a/w;

    move-result-object p1

    return-object p1
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/f/a/g;)Ljava/util/Set;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->f:Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method a(Lcom/bytedance/sdk/openadsdk/f/a/p;Lcom/bytedance/sdk/openadsdk/f/a/f;)Lcom/bytedance/sdk/openadsdk/f/a/g$a;
    .locals 5
    .param p1    # Lcom/bytedance/sdk/openadsdk/f/a/p;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->c:Ljava/util/Map;

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/f/a/p;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/f/a/b;

    const/4 v1, -0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    .line 53
    :try_start_0
    iget-object v3, p2, Lcom/bytedance/sdk/openadsdk/f/a/f;->b:Ljava/lang/String;

    invoke-direct {p0, v3, v0}, Lcom/bytedance/sdk/openadsdk/f/a/g;->b(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/b;)Lcom/bytedance/sdk/openadsdk/f/a/w;

    move-result-object v3

    .line 54
    iput-object v3, p2, Lcom/bytedance/sdk/openadsdk/f/a/f;->d:Lcom/bytedance/sdk/openadsdk/f/a/w;

    if-nez v3, :cond_1

    .line 56
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->g:Lcom/bytedance/sdk/openadsdk/f/a/m;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->g:Lcom/bytedance/sdk/openadsdk/f/a/m;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/f/a/f;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/bytedance/sdk/openadsdk/f/a/p;->d:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v0, p2, v3, v4}, Lcom/bytedance/sdk/openadsdk/f/a/m;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 59
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Permission denied, call: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Ljava/lang/String;)V

    .line 60
    new-instance p2, Lcom/bytedance/sdk/openadsdk/f/a/r;

    invoke-direct {p2, v1}, Lcom/bytedance/sdk/openadsdk/f/a/r;-><init>(I)V

    throw p2

    .line 62
    :cond_1
    instance-of v4, v0, Lcom/bytedance/sdk/openadsdk/f/a/e;

    if-eqz v4, :cond_2

    .line 63
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Processing stateless call: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Ljava/lang/String;)V

    .line 64
    check-cast v0, Lcom/bytedance/sdk/openadsdk/f/a/e;

    invoke-direct {p0, p1, v0, p2}, Lcom/bytedance/sdk/openadsdk/f/a/g;->a(Lcom/bytedance/sdk/openadsdk/f/a/p;Lcom/bytedance/sdk/openadsdk/f/a/e;Lcom/bytedance/sdk/openadsdk/f/a/f;)Lcom/bytedance/sdk/openadsdk/f/a/g$a;

    move-result-object p2

    return-object p2

    .line 65
    :cond_2
    instance-of v4, v0, Lcom/bytedance/sdk/openadsdk/f/a/c;

    if-eqz v4, :cond_3

    .line 66
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Processing raw call: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Ljava/lang/String;)V

    .line 67
    check-cast v0, Lcom/bytedance/sdk/openadsdk/f/a/c;

    invoke-direct {p0, p1, v0, v3}, Lcom/bytedance/sdk/openadsdk/f/a/g;->a(Lcom/bytedance/sdk/openadsdk/f/a/p;Lcom/bytedance/sdk/openadsdk/f/a/c;Lcom/bytedance/sdk/openadsdk/f/a/w;)Lcom/bytedance/sdk/openadsdk/f/a/g$a;

    move-result-object p2

    return-object p2

    :catch_0
    move-exception p2

    goto/16 :goto_0

    .line 70
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->d:Ljava/util/Map;

    iget-object v3, p1, Lcom/bytedance/sdk/openadsdk/f/a/p;->d:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/f/a/d$b;

    if-eqz v0, :cond_5

    .line 72
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/f/a/d$b;->a()Lcom/bytedance/sdk/openadsdk/f/a/d;

    move-result-object v0

    .line 73
    iget-object v3, p1, Lcom/bytedance/sdk/openadsdk/f/a/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/f/a/d;->a(Ljava/lang/String;)V

    .line 74
    iget-object v3, p2, Lcom/bytedance/sdk/openadsdk/f/a/f;->b:Ljava/lang/String;

    invoke-direct {p0, v3, v0}, Lcom/bytedance/sdk/openadsdk/f/a/g;->b(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/b;)Lcom/bytedance/sdk/openadsdk/f/a/w;

    move-result-object v3

    .line 75
    iput-object v3, p2, Lcom/bytedance/sdk/openadsdk/f/a/f;->d:Lcom/bytedance/sdk/openadsdk/f/a/w;

    if-eqz v3, :cond_4

    .line 81
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Processing stateful call: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Ljava/lang/String;)V

    .line 82
    invoke-direct {p0, p1, v0, p2}, Lcom/bytedance/sdk/openadsdk/f/a/g;->a(Lcom/bytedance/sdk/openadsdk/f/a/p;Lcom/bytedance/sdk/openadsdk/f/a/d;Lcom/bytedance/sdk/openadsdk/f/a/f;)Lcom/bytedance/sdk/openadsdk/f/a/g$a;

    move-result-object p2

    return-object p2

    .line 77
    :cond_4
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Permission denied, call: "

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/f/a/d;->d()V

    .line 79
    new-instance p2, Lcom/bytedance/sdk/openadsdk/f/a/r;

    invoke-direct {p2, v1}, Lcom/bytedance/sdk/openadsdk/f/a/r;-><init>(I)V

    throw p2
    :try_end_0
    .catch Lcom/bytedance/sdk/openadsdk/f/a/u$a; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :cond_5
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->g:Lcom/bytedance/sdk/openadsdk/f/a/m;

    if-eqz v0, :cond_6

    .line 91
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->g:Lcom/bytedance/sdk/openadsdk/f/a/m;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/f/a/f;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/f/a/p;->d:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-interface {v0, p2, v1, v3}, Lcom/bytedance/sdk/openadsdk/f/a/m;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 93
    :cond_6
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Received call: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", but not registered."

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/f/a/i;->b(Ljava/lang/String;)V

    return-object v2

    .line 86
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No remote permission config fetched, call pending: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 87
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->e:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    new-instance p1, Lcom/bytedance/sdk/openadsdk/f/a/g$a;

    const/4 p2, 0x0

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/f/a/x;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p2, v0, v2}, Lcom/bytedance/sdk/openadsdk/f/a/g$a;-><init>(ZLjava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/g$1;)V

    return-object p1
.end method

.method a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/d$b;)V
    .locals 1
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/d$b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 108
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "JsBridge stateful method registered: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Ljava/lang/String;)V

    return-void
.end method

.method a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)V
    .locals 1
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/f/a/e<",
            "**>;)V"
        }
    .end annotation

    .line 102
    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/f/a/e;->a(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/g;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "JsBridge stateless method registered: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Ljava/lang/String;)V

    return-void
.end method
