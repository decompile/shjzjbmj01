.class public Lcom/bytedance/sdk/openadsdk/f/a/q;
.super Ljava/lang/Object;
.source "JsBridge2.java"


# static fields
.field static a:Lcom/bytedance/sdk/openadsdk/f/a/v;


# instance fields
.field private final b:Lcom/bytedance/sdk/openadsdk/f/a/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final c:Landroid/webkit/WebView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final d:Lcom/bytedance/sdk/openadsdk/f/a/j;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/f/a/n;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/bytedance/sdk/openadsdk/f/a/o;

.field private volatile g:Z


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/f/a/j;)V
    .locals 2

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->e:Ljava/util/List;

    const/4 v0, 0x0

    .line 30
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->g:Z

    .line 204
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->d:Lcom/bytedance/sdk/openadsdk/f/a/j;

    .line 206
    iget-boolean v0, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->h:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/bytedance/sdk/openadsdk/f/a/q;->a:Lcom/bytedance/sdk/openadsdk/f/a/v;

    if-eqz v0, :cond_0

    .line 207
    sget-object v0, Lcom/bytedance/sdk/openadsdk/f/a/q;->a:Lcom/bytedance/sdk/openadsdk/f/a/v;

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/v;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/f/a/u;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 209
    :goto_0
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->a:Landroid/webkit/WebView;

    if-eqz v1, :cond_1

    .line 210
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/a/y;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/f/a/y;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->b:Lcom/bytedance/sdk/openadsdk/f/a/a;

    .line 211
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->b:Lcom/bytedance/sdk/openadsdk/f/a/a;

    invoke-virtual {v1, p1, v0}, Lcom/bytedance/sdk/openadsdk/f/a/a;->a(Lcom/bytedance/sdk/openadsdk/f/a/j;Lcom/bytedance/sdk/openadsdk/f/a/u;)V

    goto :goto_1

    .line 213
    :cond_1
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->b:Lcom/bytedance/sdk/openadsdk/f/a/a;

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->b:Lcom/bytedance/sdk/openadsdk/f/a/a;

    .line 214
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->b:Lcom/bytedance/sdk/openadsdk/f/a/a;

    invoke-virtual {v1, p1, v0}, Lcom/bytedance/sdk/openadsdk/f/a/a;->a(Lcom/bytedance/sdk/openadsdk/f/a/j;Lcom/bytedance/sdk/openadsdk/f/a/u;)V

    .line 216
    :goto_1
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->a:Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->c:Landroid/webkit/WebView;

    .line 217
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->j:Lcom/bytedance/sdk/openadsdk/f/a/n;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    iget-boolean v0, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->f:Z

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Z)V

    .line 219
    iget-boolean p1, p1, Lcom/bytedance/sdk/openadsdk/f/a/j;->g:Z

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/f/a/x;->a(Z)V

    return-void
.end method

.method public static a(Landroid/webkit/WebView;)Lcom/bytedance/sdk/openadsdk/f/a/j;
    .locals 1
    .param p0    # Landroid/webkit/WebView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 60
    new-instance v0, Lcom/bytedance/sdk/openadsdk/f/a/j;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/f/a/j;-><init>(Landroid/webkit/WebView;)V

    return-object v0
.end method

.method private a()V
    .locals 2

    .line 228
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->g:Z

    if-eqz v0, :cond_0

    .line 229
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsBridge2 is already released!!!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/f/a/i;->a(Ljava/lang/RuntimeException;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/d$b;)Lcom/bytedance/sdk/openadsdk/f/a/q;
    .locals 1
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/d$b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 114
    invoke-virtual {p0, p1, v0, p2}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/d$b;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;
    .locals 1
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/f/a/e<",
            "**>;)",
            "Lcom/bytedance/sdk/openadsdk/f/a/q;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 91
    invoke-virtual {p0, p1, v0, p2}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/d$b;)Lcom/bytedance/sdk/openadsdk/f/a/q;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/bytedance/sdk/openadsdk/f/a/d$b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 129
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a()V

    .line 130
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->b:Lcom/bytedance/sdk/openadsdk/f/a/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/f/a/a;->g:Lcom/bytedance/sdk/openadsdk/f/a/g;

    invoke-virtual {p2, p1, p3}, Lcom/bytedance/sdk/openadsdk/f/a/g;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/d$b;)V

    .line 131
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->f:Lcom/bytedance/sdk/openadsdk/f/a/o;

    if-eqz p2, :cond_0

    .line 132
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->f:Lcom/bytedance/sdk/openadsdk/f/a/o;

    invoke-interface {p2, p1}, Lcom/bytedance/sdk/openadsdk/f/a/o;->a(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/bytedance/sdk/openadsdk/f/a/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/f/a/e<",
            "**>;)",
            "Lcom/bytedance/sdk/openadsdk/f/a/q;"
        }
    .end annotation

    .line 105
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a()V

    .line 106
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->b:Lcom/bytedance/sdk/openadsdk/f/a/a;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/f/a/a;->g:Lcom/bytedance/sdk/openadsdk/f/a/g;

    invoke-virtual {p2, p1, p3}, Lcom/bytedance/sdk/openadsdk/f/a/g;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)V

    .line 107
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->f:Lcom/bytedance/sdk/openadsdk/f/a/o;

    if-eqz p2, :cond_0

    .line 108
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->f:Lcom/bytedance/sdk/openadsdk/f/a/o;

    invoke-interface {p2, p1}, Lcom/bytedance/sdk/openadsdk/f/a/o;->a(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/AnyThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .line 86
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a()V

    .line 87
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/a/q;->b:Lcom/bytedance/sdk/openadsdk/f/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/f/a/a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
