.class Lcom/bytedance/sdk/openadsdk/f/a/i;
.super Ljava/lang/Object;
.source "DebugUtil.java"


# static fields
.field private static a:Z


# direct methods
.method static a(Ljava/lang/RuntimeException;)V
    .locals 1

    .line 58
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/f/a/i;->a:Z

    if-nez v0, :cond_0

    return-void

    .line 59
    :cond_0
    throw p0
.end method

.method static a(Ljava/lang/String;)V
    .locals 1

    .line 26
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/f/a/i;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "JsBridge2"

    .line 27
    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 38
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/f/a/i;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "JsBridge2"

    .line 39
    invoke-static {v0, p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string p0, "JsBridge2"

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stacktrace: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method static a(Z)V
    .locals 0

    .line 18
    sput-boolean p0, Lcom/bytedance/sdk/openadsdk/f/a/i;->a:Z

    return-void
.end method

.method static b(Ljava/lang/String;)V
    .locals 1

    .line 32
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/f/a/i;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "JsBridge2"

    .line 33
    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method static b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .line 51
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/f/a/i;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "JsBridge2"

    .line 52
    invoke-static {v0, p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string p0, "JsBridge2"

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stacktrace: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
