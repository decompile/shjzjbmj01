.class public Lcom/bytedance/sdk/openadsdk/f/b/i;
.super Lcom/bytedance/sdk/openadsdk/f/a/d;
.source "ShowAppDetailOrPrivacyDialogMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bytedance/sdk/openadsdk/f/a/d<",
        "Lorg/json/JSONObject;",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/core/x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/core/x;)V
    .locals 1

    .line 37
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/f/a/d;-><init>()V

    .line 38
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/b/i;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/f/a/q;Lcom/bytedance/sdk/openadsdk/core/x;)V
    .locals 2

    const-string v0, "showAppDetailOrPrivacyDialog"

    .line 29
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/i$1;

    invoke-direct {v1, p1}, Lcom/bytedance/sdk/openadsdk/f/b/i$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/d$b;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Ljava/lang/Object;Lcom/bytedance/sdk/openadsdk/f/a/f;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 16
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/f/b/i;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/f/a/f;)V

    return-void
.end method

.method protected a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/f/a/f;)V
    .locals 2
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 49
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/i;->w()Z

    move-result p2

    if-eqz p2, :cond_1

    const-string p2, "ShowAppDetailOrPrivacyDialogMethod"

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[JSB-REQ] version: 3 data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_1
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/b/i;->a:Ljava/lang/ref/WeakReference;

    if-nez p2, :cond_2

    return-void

    .line 56
    :cond_2
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/b/i;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/bytedance/sdk/openadsdk/core/x;

    if-nez p2, :cond_3

    .line 58
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/f/b/i;->c()V

    return-void

    :cond_3
    if-nez p1, :cond_4

    return-void

    :cond_4
    const-string v0, "show_dialog_style"

    .line 64
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_5

    .line 66
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/x;->j()V

    const-string p1, "ShowAppDetailOrPrivacyDialogMethod"

    const-string p2, "ShowAppDetailOrPrivacyDialogMethod showAppDetailDialog "

    .line 67
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const/4 v0, 0x2

    if-ne p1, v0, :cond_6

    .line 69
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/x;->i()V

    const-string p1, "ShowAppDetailOrPrivacyDialogMethod"

    const-string p2, "ShowAppDetailOrPrivacyDialogMethod showAppPrivacyDialog "

    .line 70
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_1
    return-void
.end method
