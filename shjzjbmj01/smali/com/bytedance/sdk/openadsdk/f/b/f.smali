.class public Lcom/bytedance/sdk/openadsdk/f/b/f;
.super Lcom/bytedance/sdk/openadsdk/f/a/e;
.source "OldBridgeSyncMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bytedance/sdk/openadsdk/f/a/e<",
        "Lorg/json/JSONObject;",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/bytedance/sdk/openadsdk/core/x;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/f/a/e;-><init>()V

    .line 53
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/f/b/f;->b:Lcom/bytedance/sdk/openadsdk/core/x;

    .line 54
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/f/b/f;->a:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/f/a/q;Lcom/bytedance/sdk/openadsdk/core/x;)V
    .locals 3

    const-string v0, "appInfo"

    .line 58
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "appInfo"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "adInfo"

    .line 59
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "adInfo"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "playable_style"

    .line 60
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "playable_style"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "getTemplateInfo"

    .line 61
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "getTemplateInfo"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "getTeMaiAds"

    .line 62
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "getTeMaiAds"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "isViewable"

    .line 63
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "isViewable"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "getScreenSize"

    .line 64
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "getScreenSize"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "getCloseButtonInfo"

    .line 65
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "getCloseButtonInfo"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "getVolume"

    .line 66
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "getVolume"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "removeLoading"

    .line 67
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "removeLoading"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "sendReward"

    .line 68
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "sendReward"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "subscribe_app_ad"

    .line 69
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "subscribe_app_ad"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "download_app_ad"

    .line 70
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "download_app_ad"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "cancel_download_app_ad"

    .line 71
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "cancel_download_app_ad"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "unsubscribe_app_ad"

    .line 72
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "unsubscribe_app_ad"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "landscape_click"

    .line 73
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "landscape_click"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "clickEvent"

    .line 74
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "clickEvent"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "renderDidFinish"

    .line 75
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "renderDidFinish"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "dynamicTrack"

    .line 76
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "dynamicTrack"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "skipVideo"

    .line 77
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "skipVideo"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "muteVideo"

    .line 78
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "muteVideo"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "changeVideoState"

    .line 79
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "changeVideoState"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "getCurrentVideoState"

    .line 80
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "getCurrentVideoState"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "send_temai_product_ids"

    .line 81
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "send_temai_product_ids"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "getMaterialMeta"

    .line 82
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "getMaterialMeta"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "endcard_load"

    .line 83
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "endcard_load"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "pauseWebView"

    .line 84
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "pauseWebView"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "pauseWebViewTimers"

    .line 85
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "pauseWebViewTimers"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    const-string v0, "webview_time_track"

    .line 86
    new-instance v1, Lcom/bytedance/sdk/openadsdk/f/b/f;

    const-string v2, "webview_time_track"

    invoke-direct {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/f/b/f;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/core/x;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/f/a/e;)Lcom/bytedance/sdk/openadsdk/f/a/q;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Lcom/bytedance/sdk/openadsdk/f/a/f;)Ljava/lang/Object;
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 15
    check-cast p1, Lorg/json/JSONObject;

    invoke-virtual {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/f/b/f;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/f/a/f;)Lorg/json/JSONObject;

    move-result-object p1

    return-object p1
.end method

.method public a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/f/a/f;)Lorg/json/JSONObject;
    .locals 2
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/f/a/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 98
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/x$a;

    invoke-direct {p2}, Lcom/bytedance/sdk/openadsdk/core/x$a;-><init>()V

    const-string v0, "call"

    .line 99
    iput-object v0, p2, Lcom/bytedance/sdk/openadsdk/core/x$a;->a:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/f/b/f;->a:Ljava/lang/String;

    iput-object v0, p2, Lcom/bytedance/sdk/openadsdk/core/x$a;->c:Ljava/lang/String;

    .line 101
    iput-object p1, p2, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    .line 102
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/f/b/f;->b:Lcom/bytedance/sdk/openadsdk/core/x;

    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/x$a;I)Lorg/json/JSONObject;

    move-result-object p1

    .line 103
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object p2

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/i;->w()Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, "OldBridgeSyncMethod"

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[JSB-RSP] version: 3 data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object p1
.end method
