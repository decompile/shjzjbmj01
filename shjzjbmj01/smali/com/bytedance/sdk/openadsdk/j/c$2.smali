.class Lcom/bytedance/sdk/openadsdk/j/c$2;
.super Ljava/lang/Object;
.source "PlayableJsBridge.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/j/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/j/c;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/j/c;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/j/c;)V
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/c$2;->a:Lcom/bytedance/sdk/openadsdk/j/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 204
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const/4 v1, 0x2

    if-eqz p1, :cond_0

    :try_start_0
    const-string v2, "interval_android"

    .line 208
    invoke-virtual {p1, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    :catch_0
    move-exception p1

    goto :goto_1

    .line 210
    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/c$2;->a:Lcom/bytedance/sdk/openadsdk/j/c;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/j/c;->c(Lcom/bytedance/sdk/openadsdk/j/c;)Landroid/content/Context;

    move-result-object p1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/j/c$2;->a:Lcom/bytedance/sdk/openadsdk/j/c;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/j/c;->d(Lcom/bytedance/sdk/openadsdk/j/c;)Landroid/hardware/SensorEventListener;

    move-result-object v2

    invoke-static {p1, v2, v1}, Lcom/bytedance/sdk/openadsdk/j/g;->a(Landroid/content/Context;Landroid/hardware/SensorEventListener;I)V

    const-string p1, "code"

    const/4 v1, 0x0

    .line 211
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :goto_1
    const-string v1, "PlayableJsBridge"

    const-string v2, "invoke start_accelerometer_observer error"

    .line 214
    invoke-static {v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "code"

    const/4 v2, -0x2

    .line 215
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "codeMsg"

    .line 216
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0
.end method
