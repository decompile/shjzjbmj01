.class public final enum Lcom/bytedance/sdk/openadsdk/j/b;
.super Ljava/lang/Enum;
.source "NetType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/bytedance/sdk/openadsdk/j/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/bytedance/sdk/openadsdk/j/b;

.field public static final enum b:Lcom/bytedance/sdk/openadsdk/j/b;

.field public static final enum c:Lcom/bytedance/sdk/openadsdk/j/b;

.field public static final enum d:Lcom/bytedance/sdk/openadsdk/j/b;

.field public static final enum e:Lcom/bytedance/sdk/openadsdk/j/b;

.field public static final enum f:Lcom/bytedance/sdk/openadsdk/j/b;

.field private static final synthetic h:[Lcom/bytedance/sdk/openadsdk/j/b;


# instance fields
.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 4
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/b;

    const-string v1, "TYPE_2G"

    const-string v2, "2g"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/j/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/j/b;->a:Lcom/bytedance/sdk/openadsdk/j/b;

    .line 5
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/b;

    const-string v1, "TYPE_3G"

    const-string v2, "3g"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/bytedance/sdk/openadsdk/j/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/j/b;->b:Lcom/bytedance/sdk/openadsdk/j/b;

    .line 6
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/b;

    const-string v1, "TYPE_4G"

    const-string v2, "4g"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/bytedance/sdk/openadsdk/j/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/j/b;->c:Lcom/bytedance/sdk/openadsdk/j/b;

    .line 7
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/b;

    const-string v1, "TYPE_5G"

    const-string v2, "5g"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/bytedance/sdk/openadsdk/j/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/j/b;->d:Lcom/bytedance/sdk/openadsdk/j/b;

    .line 8
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/b;

    const-string v1, "TYPE_WIFI"

    const-string v2, "wifi"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/bytedance/sdk/openadsdk/j/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/j/b;->e:Lcom/bytedance/sdk/openadsdk/j/b;

    .line 9
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/b;

    const-string v1, "TYPE_UNKNOWN"

    const-string v2, "mobile"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lcom/bytedance/sdk/openadsdk/j/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/j/b;->f:Lcom/bytedance/sdk/openadsdk/j/b;

    const/4 v0, 0x6

    .line 3
    new-array v0, v0, [Lcom/bytedance/sdk/openadsdk/j/b;

    sget-object v1, Lcom/bytedance/sdk/openadsdk/j/b;->a:Lcom/bytedance/sdk/openadsdk/j/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/bytedance/sdk/openadsdk/j/b;->b:Lcom/bytedance/sdk/openadsdk/j/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/bytedance/sdk/openadsdk/j/b;->c:Lcom/bytedance/sdk/openadsdk/j/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/bytedance/sdk/openadsdk/j/b;->d:Lcom/bytedance/sdk/openadsdk/j/b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/bytedance/sdk/openadsdk/j/b;->e:Lcom/bytedance/sdk/openadsdk/j/b;

    aput-object v1, v0, v7

    sget-object v1, Lcom/bytedance/sdk/openadsdk/j/b;->f:Lcom/bytedance/sdk/openadsdk/j/b;

    aput-object v1, v0, v8

    sput-object v0, Lcom/bytedance/sdk/openadsdk/j/b;->h:[Lcom/bytedance/sdk/openadsdk/j/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/j/b;->g:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/j/b;
    .locals 1

    .line 3
    const-class v0, Lcom/bytedance/sdk/openadsdk/j/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/bytedance/sdk/openadsdk/j/b;

    return-object p0
.end method

.method public static values()[Lcom/bytedance/sdk/openadsdk/j/b;
    .locals 1

    .line 3
    sget-object v0, Lcom/bytedance/sdk/openadsdk/j/b;->h:[Lcom/bytedance/sdk/openadsdk/j/b;

    invoke-virtual {v0}, [Lcom/bytedance/sdk/openadsdk/j/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/bytedance/sdk/openadsdk/j/b;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/b;->g:Ljava/lang/String;

    return-object v0
.end method
