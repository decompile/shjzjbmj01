.class public Lcom/bytedance/sdk/openadsdk/j/e;
.super Ljava/lang/Object;
.source "PlayablePlugin.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/webkit/WebView;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/bytedance/sdk/openadsdk/j/c;

.field private d:Lcom/bytedance/sdk/openadsdk/j/a;

.field private e:I

.field private f:I

.field private g:Lorg/json/JSONObject;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lorg/json/JSONObject;

.field private m:Lorg/json/JSONObject;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Z

.field private q:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/j/a;)V
    .locals 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 31
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->e:I

    .line 32
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->f:I

    .line 33
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->g:Lorg/json/JSONObject;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->k:Ljava/util/Map;

    .line 48
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->a:Landroid/content/Context;

    .line 49
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->b:Ljava/lang/ref/WeakReference;

    .line 50
    new-instance p1, Lcom/bytedance/sdk/openadsdk/j/c;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/j/c;-><init>(Lcom/bytedance/sdk/openadsdk/j/e;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->c:Lcom/bytedance/sdk/openadsdk/j/c;

    .line 51
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/j/e;->d:Lcom/bytedance/sdk/openadsdk/j/a;

    .line 52
    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/j/f;->a(Landroid/webkit/WebView;)V

    .line 53
    invoke-virtual {p2}, Landroid/webkit/WebView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    new-instance p3, Lcom/bytedance/sdk/openadsdk/j/e$1;

    invoke-direct {p3, p0, p2}, Lcom/bytedance/sdk/openadsdk/j/e$1;-><init>(Lcom/bytedance/sdk/openadsdk/j/e;Landroid/webkit/WebView;)V

    invoke-virtual {p1, p3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/j/e;)I
    .locals 0

    .line 14
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->e:I

    return p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/j/e;I)I
    .locals 0

    .line 14
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->e:I

    return p1
.end method

.method public static a(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/j/a;)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 1

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 272
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/e;

    invoke-direct {v0, p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/j/e;-><init>(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/j/a;)V

    return-object v0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/j/e;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 0

    .line 14
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->g:Lorg/json/JSONObject;

    return-object p1
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/j/e;)I
    .locals 0

    .line 14
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->f:I

    return p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/j/e;I)I
    .locals 0

    .line 14
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->f:I

    return p1
.end method


# virtual methods
.method public a()Landroid/content/Context;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->h:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->k:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public a(Z)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 2

    .line 141
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->o:Z

    .line 143
    :try_start_0
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "endcard_mute"

    .line 144
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->o:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "volumeChange"

    .line 145
    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/j/e;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "PlayablePlugin"

    const-string v1, "setIsMute error"

    .line 147
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object p0
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3

    .line 191
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/j/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PlayablePlugin"

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CALL JS ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->d:Lcom/bytedance/sdk/openadsdk/j/a;

    invoke-interface {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/j/a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->i:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 2

    .line 157
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->p:Z

    .line 159
    :try_start_0
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "viewStatus"

    .line 160
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->p:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "viewableChange"

    .line 161
    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/j/e;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "PlayablePlugin"

    const-string v1, "setViewable error"

    .line 163
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object p0
.end method

.method public b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->k:Ljava/util/Map;

    return-object v0
.end method

.method public b(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 6

    .line 251
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 254
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/j/d;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "PlayablePlugin"

    .line 255
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PlayablePlugin JSB-REQ ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    const-string v4, ""

    :goto_0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :cond_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/j/e;->c:Lcom/bytedance/sdk/openadsdk/j/c;

    invoke-virtual {v2, p1, p2}, Lcom/bytedance/sdk/openadsdk/j/c;->a(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p2

    .line 262
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/j/d;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "PlayablePlugin"

    .line 263
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PlayablePlugin JSB-RSP ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "] time:"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    const-string p1, ""

    :goto_1
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-object p2
.end method

.method public c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 2

    .line 105
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "playable_style"

    .line 106
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 107
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->m:Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "PlayablePlugin"

    const-string v1, "setPlayableStyle error"

    .line 109
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object p0
.end method

.method public c(Z)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 2

    .line 173
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->q:Z

    .line 175
    :try_start_0
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "send_click"

    .line 176
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->q:Z

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "change_playable_click"

    .line 177
    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/j/e;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "PlayablePlugin"

    const-string v1, "setPlayableClick error"

    .line 179
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object p0
.end method

.method public c()Lorg/json/JSONObject;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->m:Lorg/json/JSONObject;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->j:Ljava/lang/String;

    return-object p0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->i:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/e;->n:Ljava/lang/String;

    return-object p0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->h:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->j:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->n:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .line 153
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->o:Z

    return v0
.end method

.method public i()Z
    .locals 1

    .line 169
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->p:Z

    return v0
.end method

.method public j()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 185
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->c:Lcom/bytedance/sdk/openadsdk/j/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/j/c;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/bytedance/sdk/openadsdk/j/b;
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->d:Lcom/bytedance/sdk/openadsdk/j/a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/j/a;->a()Lcom/bytedance/sdk/openadsdk/j/b;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/bytedance/sdk/openadsdk/j/a;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->d:Lcom/bytedance/sdk/openadsdk/j/a;

    return-object v0
.end method

.method public m()Lorg/json/JSONObject;
    .locals 1

    .line 208
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->g:Lorg/json/JSONObject;

    return-object v0
.end method

.method public n()Lorg/json/JSONObject;
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->l:Lorg/json/JSONObject;

    return-object v0
.end method

.method public o()V
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->d:Lcom/bytedance/sdk/openadsdk/j/a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/j/a;->b()V

    return-void
.end method

.method public p()V
    .locals 1

    const/4 v0, 0x0

    .line 225
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/j/e;->b(Z)Lcom/bytedance/sdk/openadsdk/j/e;

    return-void
.end method

.method public q()V
    .locals 1

    const/4 v0, 0x1

    .line 229
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/j/e;->b(Z)Lcom/bytedance/sdk/openadsdk/j/e;

    return-void
.end method

.method public r()V
    .locals 1

    .line 234
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e;->c:Lcom/bytedance/sdk/openadsdk/j/c;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/j/c;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method
