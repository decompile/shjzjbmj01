.class public Lcom/bytedance/sdk/openadsdk/j/c;
.super Ljava/lang/Object;
.source "PlayableJsBridge.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/j/c$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/j/e;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/j/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/hardware/SensorEventListener;

.field private e:Landroid/hardware/SensorEventListener;


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/j/e;)V
    .locals 1

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    .line 29
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/c$1;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/j/c$1;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->d:Landroid/hardware/SensorEventListener;

    .line 57
    new-instance v0, Lcom/bytedance/sdk/openadsdk/j/c$10;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/j/c$10;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->e:Landroid/hardware/SensorEventListener;

    .line 87
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/j/e;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->a:Landroid/content/Context;

    .line 88
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->b:Ljava/lang/ref/WeakReference;

    .line 89
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/j/c;->c()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/j/c;)Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/j/c;->d()Lcom/bytedance/sdk/openadsdk/j/e;

    move-result-object p0

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/j/c;)Lcom/bytedance/sdk/openadsdk/j/a;
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/j/c;->e()Lcom/bytedance/sdk/openadsdk/j/a;

    move-result-object p0

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/j/c;)Landroid/content/Context;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->a:Landroid/content/Context;

    return-object p0
.end method

.method private c()V
    .locals 3

    .line 97
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "adInfo"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$11;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$11;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "appInfo"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$12;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$12;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "subscribe_app_ad"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$13;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$13;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "download_app_ad"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$14;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$14;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "isViewable"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$15;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$15;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "getVolume"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$16;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$16;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "getScreenSize"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$17;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$17;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "start_accelerometer_observer"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$2;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$2;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "close_accelerometer_observer"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$3;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$3;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "start_gyro_observer"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$4;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$4;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "close_gyro_observer"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$5;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$5;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "device_shake"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$6;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$6;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "playable_style"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$7;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$7;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "sendReward"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$8;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$8;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    const-string v1, "webview_time_track"

    new-instance v2, Lcom/bytedance/sdk/openadsdk/j/c$9;

    invoke-direct {v2, p0}, Lcom/bytedance/sdk/openadsdk/j/c$9;-><init>(Lcom/bytedance/sdk/openadsdk/j/c;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/j/c;)Landroid/hardware/SensorEventListener;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->d:Landroid/hardware/SensorEventListener;

    return-object p0
.end method

.method private d()Lcom/bytedance/sdk/openadsdk/j/e;
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/j/e;

    return-object v0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/j/c;)Landroid/hardware/SensorEventListener;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->e:Landroid/hardware/SensorEventListener;

    return-object p0
.end method

.method private e()Lcom/bytedance/sdk/openadsdk/j/a;
    .locals 1

    .line 340
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/j/c;->d()Lcom/bytedance/sdk/openadsdk/j/e;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 344
    :cond_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/j/e;->l()Lcom/bytedance/sdk/openadsdk/j/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 2

    const/4 v0, 0x0

    .line 349
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/c;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/j/c$a;

    if-nez p1, :cond_0

    return-object v0

    .line 353
    :cond_0
    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/j/c$a;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    const-string p2, "PlayableJsBridge"

    const-string v1, "invoke error"

    .line 355
    invoke-static {p2, v1, p1}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public b()V
    .locals 2

    .line 361
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/c;->d:Landroid/hardware/SensorEventListener;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/j/g;->a(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V

    .line 362
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/c;->e:Landroid/hardware/SensorEventListener;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/j/g;->a(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V

    return-void
.end method
