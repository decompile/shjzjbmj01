.class Lcom/bytedance/sdk/openadsdk/j/e$1;
.super Ljava/lang/Object;
.source "PlayablePlugin.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/j/e;-><init>(Landroid/content/Context;Landroid/webkit/WebView;Lcom/bytedance/sdk/openadsdk/j/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/webkit/WebView;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/j/e;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/j/e;Landroid/webkit/WebView;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->b:Lcom/bytedance/sdk/openadsdk/j/e;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->a:Landroid/webkit/WebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .line 58
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->b:Lcom/bytedance/sdk/openadsdk/j/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/j/e;->a(Lcom/bytedance/sdk/openadsdk/j/e;)I

    move-result v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->a:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->b:Lcom/bytedance/sdk/openadsdk/j/e;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/j/e;->b(Lcom/bytedance/sdk/openadsdk/j/e;)I

    move-result v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->a:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->b:Lcom/bytedance/sdk/openadsdk/j/e;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->a:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/j/e;->a(Lcom/bytedance/sdk/openadsdk/j/e;I)I

    .line 62
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->b:Lcom/bytedance/sdk/openadsdk/j/e;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->a:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/j/e;->b(Lcom/bytedance/sdk/openadsdk/j/e;I)I

    .line 63
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "width"

    .line 64
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->b:Lcom/bytedance/sdk/openadsdk/j/e;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/j/e;->a(Lcom/bytedance/sdk/openadsdk/j/e;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "height"

    .line 65
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->b:Lcom/bytedance/sdk/openadsdk/j/e;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/j/e;->b(Lcom/bytedance/sdk/openadsdk/j/e;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 66
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->b:Lcom/bytedance/sdk/openadsdk/j/e;

    const-string v2, "resize"

    invoke-virtual {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/j/e;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 67
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/j/e$1;->b:Lcom/bytedance/sdk/openadsdk/j/e;

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/j/e;->a(Lcom/bytedance/sdk/openadsdk/j/e;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PlayablePlugin"

    const-string v2, "onSizeChanged error"

    .line 69
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/j/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
