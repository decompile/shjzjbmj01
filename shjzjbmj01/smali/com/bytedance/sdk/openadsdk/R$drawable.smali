.class public final Lcom/bytedance/sdk/openadsdk/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final tt_ad_backup_bk:I

.field public static final tt_ad_backup_bk2:I

.field public static final tt_ad_cover_btn_begin_bg:I

.field public static final tt_ad_cover_btn_draw_begin_bg:I

.field public static final tt_ad_download_progress_bar_horizontal:I

.field public static final tt_ad_logo:I

.field public static final tt_ad_logo_background:I

.field public static final tt_ad_logo_reward_full:I

.field public static final tt_ad_logo_small:I

.field public static final tt_ad_logo_small_rectangle:I

.field public static final tt_ad_skip_btn_bg:I

.field public static final tt_app_detail_back_btn:I

.field public static final tt_app_detail_bg:I

.field public static final tt_app_detail_black:I

.field public static final tt_app_detail_info:I

.field public static final tt_appdownloader_action_bg:I

.field public static final tt_appdownloader_action_new_bg:I

.field public static final tt_appdownloader_ad_detail_download_progress:I

.field public static final tt_appdownloader_detail_download_success_bg:I

.field public static final tt_appdownloader_download_progress_bar_horizontal:I

.field public static final tt_appdownloader_download_progress_bar_horizontal_new:I

.field public static final tt_appdownloader_download_progress_bar_horizontal_night:I

.field public static final tt_back_video:I

.field public static final tt_backup_btn_1:I

.field public static final tt_backup_btn_2:I

.field public static final tt_browser_download_selector:I

.field public static final tt_browser_progress_style:I

.field public static final tt_circle_solid_mian:I

.field public static final tt_close_move_detail:I

.field public static final tt_close_move_details_normal:I

.field public static final tt_close_move_details_pressed:I

.field public static final tt_comment_tv:I

.field public static final tt_common_download_bg:I

.field public static final tt_common_download_btn_bg:I

.field public static final tt_custom_dialog_bg:I

.field public static final tt_detail_video_btn_bg:I

.field public static final tt_dislike_bottom_seletor:I

.field public static final tt_dislike_cancle_bg_selector:I

.field public static final tt_dislike_dialog_bg:I

.field public static final tt_dislike_flowlayout_tv_bg:I

.field public static final tt_dislike_icon:I

.field public static final tt_dislike_icon2:I

.field public static final tt_dislike_middle_seletor:I

.field public static final tt_dislike_son_tag:I

.field public static final tt_dislike_top_bg:I

.field public static final tt_dislike_top_seletor:I

.field public static final tt_download_btn_bg:I

.field public static final tt_download_corner_bg:I

.field public static final tt_download_dialog_btn_bg:I

.field public static final tt_draw_back_bg:I

.field public static final tt_enlarge_video:I

.field public static final tt_forward_video:I

.field public static final tt_install_bk:I

.field public static final tt_install_btn_bk:I

.field public static final tt_leftbackbutton_titlebar_photo_preview:I

.field public static final tt_leftbackicon_selector:I

.field public static final tt_leftbackicon_selector_for_dark:I

.field public static final tt_lefterbackicon_titlebar:I

.field public static final tt_lefterbackicon_titlebar_for_dark:I

.field public static final tt_lefterbackicon_titlebar_press:I

.field public static final tt_lefterbackicon_titlebar_press_for_dark:I

.field public static final tt_mute:I

.field public static final tt_mute_btn_bg:I

.field public static final tt_new_pause_video:I

.field public static final tt_new_pause_video_press:I

.field public static final tt_new_play_video:I

.field public static final tt_normalscreen_loading:I

.field public static final tt_open_app_detail_download_btn_bg:I

.field public static final tt_open_app_detail_list_item:I

.field public static final tt_play_movebar_textpage:I

.field public static final tt_playable_btn_bk:I

.field public static final tt_playable_l_logo:I

.field public static final tt_playable_progress_style:I

.field public static final tt_refreshing_video_textpage:I

.field public static final tt_refreshing_video_textpage_normal:I

.field public static final tt_refreshing_video_textpage_pressed:I

.field public static final tt_reward_countdown_bg:I

.field public static final tt_reward_dislike_icon:I

.field public static final tt_reward_full_new_bar_bg:I

.field public static final tt_reward_full_new_bar_btn_bg:I

.field public static final tt_reward_full_video_backup_btn_bg:I

.field public static final tt_reward_video_download_btn_bg:I

.field public static final tt_right_arrow:I

.field public static final tt_seek_progress:I

.field public static final tt_seek_thumb:I

.field public static final tt_seek_thumb_fullscreen:I

.field public static final tt_seek_thumb_fullscreen_press:I

.field public static final tt_seek_thumb_fullscreen_selector:I

.field public static final tt_seek_thumb_normal:I

.field public static final tt_seek_thumb_press:I

.field public static final tt_shadow_btn_back:I

.field public static final tt_shadow_btn_back_withoutnight:I

.field public static final tt_shadow_fullscreen_top:I

.field public static final tt_shadow_lefterback_titlebar:I

.field public static final tt_shadow_lefterback_titlebar_press:I

.field public static final tt_shadow_lefterback_titlebar_press_withoutnight:I

.field public static final tt_shadow_lefterback_titlebar_withoutnight:I

.field public static final tt_shrink_fullscreen:I

.field public static final tt_shrink_video:I

.field public static final tt_skip_text_bg:I

.field public static final tt_splash_ad_logo:I

.field public static final tt_splash_mute:I

.field public static final tt_splash_unmute:I

.field public static final tt_star_empty_bg:I

.field public static final tt_star_full_bg:I

.field public static final tt_stop_movebar_textpage:I

.field public static final tt_suggestion_logo:I

.field public static final tt_titlebar_close_drawable:I

.field public static final tt_titlebar_close_for_dark:I

.field public static final tt_titlebar_close_press:I

.field public static final tt_titlebar_close_press_for_dark:I

.field public static final tt_titlebar_close_seletor:I

.field public static final tt_titlebar_close_seletor_for_dark:I

.field public static final tt_unmute:I

.field public static final tt_video_black_desc_gradient:I

.field public static final tt_video_close_drawable:I

.field public static final tt_video_loading_progress_bar:I

.field public static final tt_video_progress_drawable:I

.field public static final tt_video_traffic_continue_play_bg:I

.field public static final tt_white_lefterbackicon_titlebar:I

.field public static final tt_white_lefterbackicon_titlebar_press:I

.field public static final ttdownloader_bg_appinfo_btn:I

.field public static final ttdownloader_bg_appinfo_dialog:I

.field public static final ttdownloader_bg_button_blue_corner:I

.field public static final ttdownloader_bg_kllk_btn1:I

.field public static final ttdownloader_bg_kllk_btn2:I

.field public static final ttdownloader_bg_transparent:I

.field public static final ttdownloader_bg_white_corner:I

.field public static final ttdownloader_dash_line:I

.field public static final ttdownloader_icon_back_arrow:I

.field public static final ttdownloader_icon_download:I

.field public static final ttdownloader_icon_yes:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 83
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_backup_bk:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_backup_bk:I

    .line 84
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_backup_bk2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_backup_bk2:I

    .line 85
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_cover_btn_begin_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_cover_btn_begin_bg:I

    .line 86
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_cover_btn_draw_begin_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_cover_btn_draw_begin_bg:I

    .line 87
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_download_progress_bar_horizontal:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_download_progress_bar_horizontal:I

    .line 88
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_logo:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_logo:I

    .line 89
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_logo_background:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_logo_background:I

    .line 90
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_logo_reward_full:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_logo_reward_full:I

    .line 91
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_logo_small:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_logo_small:I

    .line 92
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_logo_small_rectangle:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_logo_small_rectangle:I

    .line 93
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_ad_skip_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_ad_skip_btn_bg:I

    .line 94
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_app_detail_back_btn:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_app_detail_back_btn:I

    .line 95
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_app_detail_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_app_detail_bg:I

    .line 96
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_app_detail_black:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_app_detail_black:I

    .line 97
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_app_detail_info:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_app_detail_info:I

    .line 98
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_appdownloader_action_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_appdownloader_action_bg:I

    .line 99
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_appdownloader_action_new_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_appdownloader_action_new_bg:I

    .line 100
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_appdownloader_ad_detail_download_progress:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_appdownloader_ad_detail_download_progress:I

    .line 101
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_appdownloader_detail_download_success_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_appdownloader_detail_download_success_bg:I

    .line 102
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_appdownloader_download_progress_bar_horizontal:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_appdownloader_download_progress_bar_horizontal:I

    .line 103
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_appdownloader_download_progress_bar_horizontal_new:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_appdownloader_download_progress_bar_horizontal_new:I

    .line 104
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_appdownloader_download_progress_bar_horizontal_night:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_appdownloader_download_progress_bar_horizontal_night:I

    .line 105
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_back_video:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_back_video:I

    .line 106
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_backup_btn_1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_backup_btn_1:I

    .line 107
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_backup_btn_2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_backup_btn_2:I

    .line 108
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_browser_download_selector:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_browser_download_selector:I

    .line 109
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_browser_progress_style:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_browser_progress_style:I

    .line 110
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_circle_solid_mian:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_circle_solid_mian:I

    .line 111
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_close_move_detail:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_close_move_detail:I

    .line 112
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_close_move_details_normal:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_close_move_details_normal:I

    .line 113
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_close_move_details_pressed:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_close_move_details_pressed:I

    .line 114
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_comment_tv:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_comment_tv:I

    .line 115
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_common_download_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_common_download_bg:I

    .line 116
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_common_download_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_common_download_btn_bg:I

    .line 117
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_custom_dialog_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_custom_dialog_bg:I

    .line 118
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_detail_video_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_detail_video_btn_bg:I

    .line 119
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_bottom_seletor:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_bottom_seletor:I

    .line 120
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_cancle_bg_selector:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_cancle_bg_selector:I

    .line 121
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_dialog_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_dialog_bg:I

    .line 122
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_flowlayout_tv_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_flowlayout_tv_bg:I

    .line 123
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_icon:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_icon:I

    .line 124
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_icon2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_icon2:I

    .line 125
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_middle_seletor:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_middle_seletor:I

    .line 126
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_son_tag:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_son_tag:I

    .line 127
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_top_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_top_bg:I

    .line 128
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_dislike_top_seletor:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_dislike_top_seletor:I

    .line 129
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_download_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_download_btn_bg:I

    .line 130
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_download_corner_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_download_corner_bg:I

    .line 131
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_download_dialog_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_download_dialog_btn_bg:I

    .line 132
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_draw_back_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_draw_back_bg:I

    .line 133
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_enlarge_video:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_enlarge_video:I

    .line 134
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_forward_video:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_forward_video:I

    .line 135
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_install_bk:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_install_bk:I

    .line 136
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_install_btn_bk:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_install_btn_bk:I

    .line 137
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_leftbackbutton_titlebar_photo_preview:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_leftbackbutton_titlebar_photo_preview:I

    .line 138
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_leftbackicon_selector:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_leftbackicon_selector:I

    .line 139
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_leftbackicon_selector_for_dark:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_leftbackicon_selector_for_dark:I

    .line 140
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_lefterbackicon_titlebar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_lefterbackicon_titlebar:I

    .line 141
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_lefterbackicon_titlebar_for_dark:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_lefterbackicon_titlebar_for_dark:I

    .line 142
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_lefterbackicon_titlebar_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_lefterbackicon_titlebar_press:I

    .line 143
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_lefterbackicon_titlebar_press_for_dark:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_lefterbackicon_titlebar_press_for_dark:I

    .line 144
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_mute:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_mute:I

    .line 145
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_mute_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_mute_btn_bg:I

    .line 146
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_new_pause_video:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_new_pause_video:I

    .line 147
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_new_pause_video_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_new_pause_video_press:I

    .line 148
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_new_play_video:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_new_play_video:I

    .line 149
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_normalscreen_loading:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_normalscreen_loading:I

    .line 150
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_open_app_detail_download_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_open_app_detail_download_btn_bg:I

    .line 151
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_open_app_detail_list_item:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_open_app_detail_list_item:I

    .line 152
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_play_movebar_textpage:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_play_movebar_textpage:I

    .line 153
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_playable_btn_bk:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_playable_btn_bk:I

    .line 154
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_playable_l_logo:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_playable_l_logo:I

    .line 155
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_playable_progress_style:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_playable_progress_style:I

    .line 156
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_refreshing_video_textpage:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_refreshing_video_textpage:I

    .line 157
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_refreshing_video_textpage_normal:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_refreshing_video_textpage_normal:I

    .line 158
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_refreshing_video_textpage_pressed:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_refreshing_video_textpage_pressed:I

    .line 159
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_reward_countdown_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_reward_countdown_bg:I

    .line 160
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_reward_dislike_icon:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_reward_dislike_icon:I

    .line 161
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_reward_full_new_bar_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_reward_full_new_bar_bg:I

    .line 162
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_reward_full_new_bar_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_reward_full_new_bar_btn_bg:I

    .line 163
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_reward_full_video_backup_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_reward_full_video_backup_btn_bg:I

    .line 164
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_reward_video_download_btn_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_reward_video_download_btn_bg:I

    .line 165
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_right_arrow:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_right_arrow:I

    .line 166
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_seek_progress:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_seek_progress:I

    .line 167
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_seek_thumb:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_seek_thumb:I

    .line 168
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_seek_thumb_fullscreen:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_seek_thumb_fullscreen:I

    .line 169
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_seek_thumb_fullscreen_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_seek_thumb_fullscreen_press:I

    .line 170
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_seek_thumb_fullscreen_selector:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_seek_thumb_fullscreen_selector:I

    .line 171
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_seek_thumb_normal:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_seek_thumb_normal:I

    .line 172
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_seek_thumb_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_seek_thumb_press:I

    .line 173
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_shadow_btn_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_shadow_btn_back:I

    .line 174
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_shadow_btn_back_withoutnight:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_shadow_btn_back_withoutnight:I

    .line 175
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_shadow_fullscreen_top:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_shadow_fullscreen_top:I

    .line 176
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_shadow_lefterback_titlebar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_shadow_lefterback_titlebar:I

    .line 177
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_shadow_lefterback_titlebar_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_shadow_lefterback_titlebar_press:I

    .line 178
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_shadow_lefterback_titlebar_press_withoutnight:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_shadow_lefterback_titlebar_press_withoutnight:I

    .line 179
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_shadow_lefterback_titlebar_withoutnight:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_shadow_lefterback_titlebar_withoutnight:I

    .line 180
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_shrink_fullscreen:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_shrink_fullscreen:I

    .line 181
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_shrink_video:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_shrink_video:I

    .line 182
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_skip_text_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_skip_text_bg:I

    .line 183
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_splash_ad_logo:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_splash_ad_logo:I

    .line 184
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_splash_mute:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_splash_mute:I

    .line 185
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_splash_unmute:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_splash_unmute:I

    .line 186
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_star_empty_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_star_empty_bg:I

    .line 187
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_star_full_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_star_full_bg:I

    .line 188
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_stop_movebar_textpage:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_stop_movebar_textpage:I

    .line 189
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_suggestion_logo:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_suggestion_logo:I

    .line 190
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_titlebar_close_drawable:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_titlebar_close_drawable:I

    .line 191
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_titlebar_close_for_dark:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_titlebar_close_for_dark:I

    .line 192
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_titlebar_close_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_titlebar_close_press:I

    .line 193
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_titlebar_close_press_for_dark:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_titlebar_close_press_for_dark:I

    .line 194
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_titlebar_close_seletor:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_titlebar_close_seletor:I

    .line 195
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_titlebar_close_seletor_for_dark:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_titlebar_close_seletor_for_dark:I

    .line 196
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_unmute:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_unmute:I

    .line 197
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_video_black_desc_gradient:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_video_black_desc_gradient:I

    .line 198
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_video_close_drawable:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_video_close_drawable:I

    .line 199
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_video_loading_progress_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_video_loading_progress_bar:I

    .line 200
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_video_progress_drawable:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_video_progress_drawable:I

    .line 201
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_video_traffic_continue_play_bg:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_video_traffic_continue_play_bg:I

    .line 202
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_white_lefterbackicon_titlebar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_white_lefterbackicon_titlebar:I

    .line 203
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->tt_white_lefterbackicon_titlebar_press:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->tt_white_lefterbackicon_titlebar_press:I

    .line 204
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_bg_appinfo_btn:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_bg_appinfo_btn:I

    .line 205
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_bg_appinfo_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_bg_appinfo_dialog:I

    .line 206
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_bg_button_blue_corner:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_bg_button_blue_corner:I

    .line 207
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_bg_kllk_btn1:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_bg_kllk_btn1:I

    .line 208
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_bg_kllk_btn2:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_bg_kllk_btn2:I

    .line 209
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_bg_transparent:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_bg_transparent:I

    .line 210
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_bg_white_corner:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_bg_white_corner:I

    .line 211
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_dash_line:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_dash_line:I

    .line 212
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_icon_back_arrow:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_icon_back_arrow:I

    .line 213
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_icon_download:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_icon_download:I

    .line 214
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;->ttdownloader_icon_yes:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$drawable;->ttdownloader_icon_yes:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
