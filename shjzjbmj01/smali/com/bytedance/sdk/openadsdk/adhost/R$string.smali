.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f08002d

.field public static final status_bar_notification_info_overflow:I = 0x7f08001c

.field public static final tt_00_00:I = 0x7f080060

.field public static final tt_ad:I = 0x7f080061

.field public static final tt_ad_logo_txt:I = 0x7f080062

.field public static final tt_app_name:I = 0x7f080063

.field public static final tt_app_privacy_dialog_title:I = 0x7f080064

.field public static final tt_appdownloader_button_cancel_download:I = 0x7f080065

.field public static final tt_appdownloader_button_queue_for_wifi:I = 0x7f080066

.field public static final tt_appdownloader_button_start_now:I = 0x7f080067

.field public static final tt_appdownloader_download_percent:I = 0x7f080068

.field public static final tt_appdownloader_download_remaining:I = 0x7f080069

.field public static final tt_appdownloader_download_unknown_title:I = 0x7f08006a

.field public static final tt_appdownloader_duration_hours:I = 0x7f08006b

.field public static final tt_appdownloader_duration_minutes:I = 0x7f08006c

.field public static final tt_appdownloader_duration_seconds:I = 0x7f08006d

.field public static final tt_appdownloader_jump_unknown_source:I = 0x7f08006e

.field public static final tt_appdownloader_label_cancel:I = 0x7f08006f

.field public static final tt_appdownloader_label_cancel_directly:I = 0x7f080070

.field public static final tt_appdownloader_label_ok:I = 0x7f080071

.field public static final tt_appdownloader_label_reserve_wifi:I = 0x7f080072

.field public static final tt_appdownloader_notification_download:I = 0x7f080073

.field public static final tt_appdownloader_notification_download_complete_open:I = 0x7f080074

.field public static final tt_appdownloader_notification_download_complete_with_install:I = 0x7f080075

.field public static final tt_appdownloader_notification_download_complete_without_install:I = 0x7f080076

.field public static final tt_appdownloader_notification_download_continue:I = 0x7f080077

.field public static final tt_appdownloader_notification_download_delete:I = 0x7f080078

.field public static final tt_appdownloader_notification_download_failed:I = 0x7f080079

.field public static final tt_appdownloader_notification_download_install:I = 0x7f08007a

.field public static final tt_appdownloader_notification_download_open:I = 0x7f08007b

.field public static final tt_appdownloader_notification_download_pause:I = 0x7f08007c

.field public static final tt_appdownloader_notification_download_restart:I = 0x7f08007d

.field public static final tt_appdownloader_notification_download_resume:I = 0x7f08007e

.field public static final tt_appdownloader_notification_download_space_failed:I = 0x7f08007f

.field public static final tt_appdownloader_notification_download_waiting_net:I = 0x7f080080

.field public static final tt_appdownloader_notification_download_waiting_wifi:I = 0x7f080081

.field public static final tt_appdownloader_notification_downloading:I = 0x7f080082

.field public static final tt_appdownloader_notification_install_finished_open:I = 0x7f080083

.field public static final tt_appdownloader_notification_insufficient_space_error:I = 0x7f080084

.field public static final tt_appdownloader_notification_need_wifi_for_size:I = 0x7f080085

.field public static final tt_appdownloader_notification_no_internet_error:I = 0x7f080086

.field public static final tt_appdownloader_notification_no_wifi_and_in_net:I = 0x7f080087

.field public static final tt_appdownloader_notification_paused_in_background:I = 0x7f080088

.field public static final tt_appdownloader_notification_pausing:I = 0x7f080089

.field public static final tt_appdownloader_notification_prepare:I = 0x7f08008a

.field public static final tt_appdownloader_notification_request_btn_no:I = 0x7f08008b

.field public static final tt_appdownloader_notification_request_btn_yes:I = 0x7f08008c

.field public static final tt_appdownloader_notification_request_message:I = 0x7f08008d

.field public static final tt_appdownloader_notification_request_title:I = 0x7f08008e

.field public static final tt_appdownloader_notification_waiting_download_complete_handler:I = 0x7f08008f

.field public static final tt_appdownloader_resume_in_wifi:I = 0x7f080090

.field public static final tt_appdownloader_tip:I = 0x7f080091

.field public static final tt_appdownloader_wifi_recommended_body:I = 0x7f080092

.field public static final tt_appdownloader_wifi_recommended_title:I = 0x7f080093

.field public static final tt_appdownloader_wifi_required_body:I = 0x7f080094

.field public static final tt_appdownloader_wifi_required_title:I = 0x7f080095

.field public static final tt_auto_play_cancel_text:I = 0x7f080096

.field public static final tt_cancel:I = 0x7f080097

.field public static final tt_comment_num:I = 0x7f080098

.field public static final tt_comment_num_backup:I = 0x7f080099

.field public static final tt_comment_score:I = 0x7f08009a

.field public static final tt_common_download_app_detail:I = 0x7f08009b

.field public static final tt_common_download_app_privacy:I = 0x7f08009c

.field public static final tt_common_download_cancel:I = 0x7f08009d

.field public static final tt_confirm_download:I = 0x7f08009e

.field public static final tt_confirm_download_have_app_name:I = 0x7f08009f

.field public static final tt_dislike_header_tv_back:I = 0x7f0800a0

.field public static final tt_dislike_header_tv_title:I = 0x7f0800a1

.field public static final tt_full_screen_skip_tx:I = 0x7f0800a2

.field public static final tt_label_cancel:I = 0x7f0800a3

.field public static final tt_label_ok:I = 0x7f0800a4

.field public static final tt_no_network:I = 0x7f0800b1

.field public static final tt_open_app_detail_developer:I = 0x7f0800b2

.field public static final tt_open_app_detail_privacy:I = 0x7f0800b3

.field public static final tt_open_app_detail_privacy_list:I = 0x7f0800b4

.field public static final tt_open_app_name:I = 0x7f0800b5

.field public static final tt_open_app_version:I = 0x7f0800b6

.field public static final tt_open_landing_page_app_name:I = 0x7f0800b7

.field public static final tt_permission_denied:I = 0x7f0800b8

.field public static final tt_playable_btn_play:I = 0x7f0800b9

.field public static final tt_request_permission_descript_external_storage:I = 0x7f0800ba

.field public static final tt_request_permission_descript_location:I = 0x7f0800bb

.field public static final tt_request_permission_descript_read_phone_state:I = 0x7f0800bc

.field public static final tt_reward_feedback:I = 0x7f0800bd

.field public static final tt_reward_screen_skip_tx:I = 0x7f0800be

.field public static final tt_splash_skip_tv_text:I = 0x7f0800bf

.field public static final tt_tip:I = 0x7f0800c0

.field public static final tt_unlike:I = 0x7f0800c1

.field public static final tt_video_bytesize:I = 0x7f0800c2

.field public static final tt_video_bytesize_M:I = 0x7f0800c3

.field public static final tt_video_bytesize_MB:I = 0x7f0800c4

.field public static final tt_video_continue_play:I = 0x7f0800c5

.field public static final tt_video_dial_phone:I = 0x7f0800c6

.field public static final tt_video_dial_replay:I = 0x7f0800c7

.field public static final tt_video_download_apk:I = 0x7f0800c8

.field public static final tt_video_mobile_go_detail:I = 0x7f0800c9

.field public static final tt_video_retry_des_txt:I = 0x7f0800ca

.field public static final tt_video_without_wifi_tips:I = 0x7f0800cb

.field public static final tt_web_title_default:I = 0x7f0800cc

.field public static final tt_will_play:I = 0x7f0800cd


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
