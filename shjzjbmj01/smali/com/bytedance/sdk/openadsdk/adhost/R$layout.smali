.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final notification_action:I = 0x7f040061

.field public static final notification_action_tombstone:I = 0x7f040062

.field public static final notification_template_custom_big:I = 0x7f040063

.field public static final notification_template_icon_group:I = 0x7f040064

.field public static final notification_template_part_chronometer:I = 0x7f040065

.field public static final notification_template_part_time:I = 0x7f040066

.field public static final tt_activity_full_video:I = 0x7f04006b

.field public static final tt_activity_full_video_new_bar_3_style:I = 0x7f04006c

.field public static final tt_activity_full_video_newstyle:I = 0x7f04006d

.field public static final tt_activity_lite_web_layout:I = 0x7f04006e

.field public static final tt_activity_middle_page:I = 0x7f04006f

.field public static final tt_activity_reward_and_full_video_bar:I = 0x7f040070

.field public static final tt_activity_reward_and_full_video_new_bar:I = 0x7f040071

.field public static final tt_activity_reward_video_newstyle:I = 0x7f040072

.field public static final tt_activity_rewardvideo:I = 0x7f040073

.field public static final tt_activity_rewardvideo_new_bar_3_style:I = 0x7f040074

.field public static final tt_activity_ttlandingpage:I = 0x7f040075

.field public static final tt_activity_ttlandingpage_playable:I = 0x7f040076

.field public static final tt_activity_video_scroll_landingpage:I = 0x7f040077

.field public static final tt_activity_videolandingpage:I = 0x7f040078

.field public static final tt_app_detail_dialog:I = 0x7f040079

.field public static final tt_app_detail_full_dialog:I = 0x7f04007a

.field public static final tt_app_detail_full_dialog_list_head:I = 0x7f04007b

.field public static final tt_app_detail_listview_item:I = 0x7f04007c

.field public static final tt_app_privacy_dialog:I = 0x7f04007d

.field public static final tt_appdownloader_notification_layout:I = 0x7f04007e

.field public static final tt_backup_ad:I = 0x7f04007f

.field public static final tt_backup_ad1:I = 0x7f040080

.field public static final tt_backup_ad2:I = 0x7f040081

.field public static final tt_backup_banner_layout1:I = 0x7f040082

.field public static final tt_backup_banner_layout2:I = 0x7f040083

.field public static final tt_backup_banner_layout3:I = 0x7f040084

.field public static final tt_backup_draw:I = 0x7f040085

.field public static final tt_backup_feed_horizontal:I = 0x7f040086

.field public static final tt_backup_feed_img_group:I = 0x7f040087

.field public static final tt_backup_feed_img_small:I = 0x7f040088

.field public static final tt_backup_feed_vertical:I = 0x7f040089

.field public static final tt_backup_feed_video:I = 0x7f04008a

.field public static final tt_backup_full_reward:I = 0x7f04008b

.field public static final tt_backup_insert_layout1:I = 0x7f04008c

.field public static final tt_backup_insert_layout2:I = 0x7f04008d

.field public static final tt_backup_insert_layout3:I = 0x7f04008e

.field public static final tt_browser_download_layout:I = 0x7f04008f

.field public static final tt_browser_titlebar:I = 0x7f040090

.field public static final tt_browser_titlebar_for_dark:I = 0x7f040091

.field public static final tt_common_download_dialog:I = 0x7f040092

.field public static final tt_custom_dailog_layout:I = 0x7f040093

.field public static final tt_dialog_listview_item:I = 0x7f040094

.field public static final tt_dislike_comment_layout:I = 0x7f040095

.field public static final tt_dislike_dialog_layout:I = 0x7f040096

.field public static final tt_dislike_dialog_layout1:I = 0x7f040097

.field public static final tt_dislike_dialog_layout2:I = 0x7f040098

.field public static final tt_dislike_flowlayout_tv:I = 0x7f040099

.field public static final tt_insert_ad_layout:I = 0x7f04009a

.field public static final tt_install_dialog_layout:I = 0x7f04009b

.field public static final tt_native_video_ad_view:I = 0x7f04009c

.field public static final tt_native_video_img_cover_layout:I = 0x7f04009d

.field public static final tt_playable_loading_layout:I = 0x7f04009e

.field public static final tt_playable_view_layout:I = 0x7f04009f

.field public static final tt_splash_view:I = 0x7f0400a0

.field public static final tt_top_full_1:I = 0x7f0400a1

.field public static final tt_top_reward_1:I = 0x7f0400a2

.field public static final tt_top_reward_dislike_2:I = 0x7f0400a3

.field public static final tt_video_ad_cover_layout:I = 0x7f0400a4

.field public static final tt_video_detail_layout:I = 0x7f0400a5

.field public static final tt_video_draw_btn_layout:I = 0x7f0400a6

.field public static final tt_video_play_layout_for_live:I = 0x7f0400a7

.field public static final tt_video_traffic_tip:I = 0x7f0400a8

.field public static final tt_video_traffic_tips_layout:I = 0x7f0400a9

.field public static final ttdownloader_activity_app_detail_info:I = 0x7f0400aa

.field public static final ttdownloader_activity_app_privacy_policy:I = 0x7f0400ab

.field public static final ttdownloader_dialog_appinfo:I = 0x7f0400ac

.field public static final ttdownloader_dialog_select_operation:I = 0x7f0400ad

.field public static final ttdownloader_item_permission:I = 0x7f0400ae


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
