.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_container:I = 0x7f0d0188

.field public static final action_divider:I = 0x7f0d0193

.field public static final action_image:I = 0x7f0d0189

.field public static final action_text:I = 0x7f0d018a

.field public static final actions:I = 0x7f0d0194

.field public static final async:I = 0x7f0d0061

.field public static final blocking:I = 0x7f0d0062

.field public static final bottom:I = 0x7f0d0052

.field public static final cancel_tv:I = 0x7f0d0275

.field public static final chronometer:I = 0x7f0d0190

.field public static final confirm_tv:I = 0x7f0d0276

.field public static final dash_line:I = 0x7f0d0279

.field public static final end:I = 0x7f0d0058

.field public static final forever:I = 0x7f0d0063

.field public static final icon:I = 0x7f0d007a

.field public static final icon_group:I = 0x7f0d0195

.field public static final info:I = 0x7f0d0191

.field public static final italic:I = 0x7f0d0064

.field public static final iv_app_icon:I = 0x7f0d026d

.field public static final iv_detail_back:I = 0x7f0d0266

.field public static final iv_privacy_back:I = 0x7f0d026b

.field public static final left:I = 0x7f0d005c

.field public static final line:I = 0x7f0d0267

.field public static final line1:I = 0x7f0d0027

.field public static final line3:I = 0x7f0d0028

.field public static final ll_download:I = 0x7f0d0269

.field public static final message_tv:I = 0x7f0d0274

.field public static final none:I = 0x7f0d0045

.field public static final normal:I = 0x7f0d0041

.field public static final notification_background:I = 0x7f0d018b

.field public static final notification_main_column:I = 0x7f0d018d

.field public static final notification_main_column_container:I = 0x7f0d018c

.field public static final permission_list:I = 0x7f0d026a

.field public static final privacy_webview:I = 0x7f0d026c

.field public static final right:I = 0x7f0d005d

.field public static final right_icon:I = 0x7f0d0192

.field public static final right_side:I = 0x7f0d018e

.field public static final start:I = 0x7f0d005e

.field public static final tag_ignore:I = 0x7f0d0030

.field public static final tag_transition_group:I = 0x7f0d0032

.field public static final tag_unhandled_key_event_manager:I = 0x7f0d0033

.field public static final tag_unhandled_key_listeners:I = 0x7f0d0034

.field public static final tag_view_name:I = 0x7f0d0035

.field public static final text:I = 0x7f0d0036

.field public static final text2:I = 0x7f0d0037

.field public static final time:I = 0x7f0d018f

.field public static final title:I = 0x7f0d0038

.field public static final title_bar:I = 0x7f0d0265

.field public static final top:I = 0x7f0d005f

.field public static final tt_ad_container:I = 0x7f0d0202

.field public static final tt_ad_content_layout:I = 0x7f0d01f2

.field public static final tt_ad_logo:I = 0x7f0d01ac

.field public static final tt_app_detail_back_tv:I = 0x7f0d01cf

.field public static final tt_app_developer_tv:I = 0x7f0d01d1

.field public static final tt_app_name_tv:I = 0x7f0d01d7

.field public static final tt_app_privacy_back_tv:I = 0x7f0d01dc

.field public static final tt_app_privacy_title:I = 0x7f0d01d8

.field public static final tt_app_privacy_tv:I = 0x7f0d01d3

.field public static final tt_app_privacy_url_tv:I = 0x7f0d01d4

.field public static final tt_app_version_tv:I = 0x7f0d01d2

.field public static final tt_appdownloader_action:I = 0x7f0d01e8

.field public static final tt_appdownloader_desc:I = 0x7f0d01df

.field public static final tt_appdownloader_download_progress:I = 0x7f0d01e6

.field public static final tt_appdownloader_download_progress_new:I = 0x7f0d01e7

.field public static final tt_appdownloader_download_size:I = 0x7f0d01e4

.field public static final tt_appdownloader_download_status:I = 0x7f0d01e5

.field public static final tt_appdownloader_download_success:I = 0x7f0d01e0

.field public static final tt_appdownloader_download_success_size:I = 0x7f0d01e1

.field public static final tt_appdownloader_download_success_status:I = 0x7f0d01e2

.field public static final tt_appdownloader_download_text:I = 0x7f0d01e3

.field public static final tt_appdownloader_icon:I = 0x7f0d01de

.field public static final tt_appdownloader_root:I = 0x7f0d01dd

.field public static final tt_backup_draw_bg:I = 0x7f0d01f5

.field public static final tt_battery_time_layout:I = 0x7f0d0255

.field public static final tt_browser_download_btn:I = 0x7f0d0204

.field public static final tt_browser_download_btn_stub:I = 0x7f0d01b5

.field public static final tt_browser_progress:I = 0x7f0d01b6

.field public static final tt_browser_titlebar_dark_view_stub:I = 0x7f0d01b3

.field public static final tt_browser_titlebar_view_stub:I = 0x7f0d01b2

.field public static final tt_browser_webview:I = 0x7f0d01b4

.field public static final tt_browser_webview_loading:I = 0x7f0d01a3

.field public static final tt_bu_close:I = 0x7f0d01f0

.field public static final tt_bu_desc:I = 0x7f0d01f1

.field public static final tt_bu_dislike:I = 0x7f0d0203

.field public static final tt_bu_download:I = 0x7f0d01ef

.field public static final tt_bu_icon:I = 0x7f0d01eb

.field public static final tt_bu_img:I = 0x7f0d01f3

.field public static final tt_bu_img_1:I = 0x7f0d01f8

.field public static final tt_bu_img_2:I = 0x7f0d01f9

.field public static final tt_bu_img_3:I = 0x7f0d01fa

.field public static final tt_bu_img_container:I = 0x7f0d01f7

.field public static final tt_bu_img_content:I = 0x7f0d01fb

.field public static final tt_bu_name:I = 0x7f0d01f4

.field public static final tt_bu_score:I = 0x7f0d01ed

.field public static final tt_bu_score_bar:I = 0x7f0d01ee

.field public static final tt_bu_title:I = 0x7f0d01ec

.field public static final tt_bu_video_container:I = 0x7f0d01f6

.field public static final tt_bu_video_container_inner:I = 0x7f0d01fc

.field public static final tt_bu_video_icon:I = 0x7f0d01fd

.field public static final tt_bu_video_name1:I = 0x7f0d01fe

.field public static final tt_bu_video_name2:I = 0x7f0d01ff

.field public static final tt_bu_video_score:I = 0x7f0d0200

.field public static final tt_bu_video_score_bar:I = 0x7f0d0201

.field public static final tt_click_lower_non_content_layout:I = 0x7f0d01a0

.field public static final tt_click_upper_non_content_layout:I = 0x7f0d019a

.field public static final tt_column_line:I = 0x7f0d0217

.field public static final tt_comment_backup:I = 0x7f0d01a8

.field public static final tt_comment_close:I = 0x7f0d021b

.field public static final tt_comment_commit:I = 0x7f0d021c

.field public static final tt_comment_content:I = 0x7f0d021d

.field public static final tt_comment_number:I = 0x7f0d021e

.field public static final tt_comment_vertical:I = 0x7f0d019f

.field public static final tt_dislike_dialog_linearlayout:I = 0x7f0d0225

.field public static final tt_dislike_header_back:I = 0x7f0d0220

.field public static final tt_dislike_header_tv:I = 0x7f0d0221

.field public static final tt_dislike_line1:I = 0x7f0d0222

.field public static final tt_dislike_title_content:I = 0x7f0d021f

.field public static final tt_download_app_btn:I = 0x7f0d01d6

.field public static final tt_download_app_detail:I = 0x7f0d020f

.field public static final tt_download_app_developer:I = 0x7f0d020e

.field public static final tt_download_app_privacy:I = 0x7f0d0210

.field public static final tt_download_app_version:I = 0x7f0d020d

.field public static final tt_download_btn:I = 0x7f0d0211

.field public static final tt_download_cancel:I = 0x7f0d0212

.field public static final tt_download_icon:I = 0x7f0d020b

.field public static final tt_download_layout:I = 0x7f0d020a

.field public static final tt_download_title:I = 0x7f0d020c

.field public static final tt_edit_suggestion:I = 0x7f0d0228

.field public static final tt_filer_words_lv:I = 0x7f0d0223

.field public static final tt_filer_words_lv_second:I = 0x7f0d0224

.field public static final tt_image:I = 0x7f0d0214

.field public static final tt_insert_ad_img:I = 0x7f0d022a

.field public static final tt_insert_ad_logo:I = 0x7f0d01e9

.field public static final tt_insert_ad_text:I = 0x7f0d01ea

.field public static final tt_insert_dislike_icon_img:I = 0x7f0d022b

.field public static final tt_insert_express_ad_fl:I = 0x7f0d0229

.field public static final tt_install_btn_no:I = 0x7f0d022e

.field public static final tt_install_btn_yes:I = 0x7f0d022f

.field public static final tt_install_content:I = 0x7f0d022d

.field public static final tt_install_title:I = 0x7f0d022c

.field public static final tt_item_desc_tv:I = 0x7f0d01db

.field public static final tt_item_select_img:I = 0x7f0d01da

.field public static final tt_item_title_tv:I = 0x7f0d01d9

.field public static final tt_item_tv:I = 0x7f0d0219

.field public static final tt_item_tv_son:I = 0x7f0d021a

.field public static final tt_lite_web_back:I = 0x7f0d01ad

.field public static final tt_lite_web_title:I = 0x7f0d01ae

.field public static final tt_lite_web_view:I = 0x7f0d01af

.field public static final tt_message:I = 0x7f0d0215

.field public static final tt_middle_page_layout:I = 0x7f0d01b0

.field public static final tt_native_video_container:I = 0x7f0d01c1

.field public static final tt_native_video_frame:I = 0x7f0d0231

.field public static final tt_native_video_img_cover:I = 0x7f0d0233

.field public static final tt_native_video_img_cover_viewStub:I = 0x7f0d0232

.field public static final tt_native_video_img_id:I = 0x7f0d0234

.field public static final tt_native_video_layout:I = 0x7f0d0230

.field public static final tt_native_video_play:I = 0x7f0d0235

.field public static final tt_native_video_titlebar:I = 0x7f0d01bb

.field public static final tt_negtive:I = 0x7f0d0216

.field public static final tt_open_app_detail_layout:I = 0x7f0d01d0

.field public static final tt_personalization_layout:I = 0x7f0d0226

.field public static final tt_personalization_name:I = 0x7f0d0227

.field public static final tt_playable_ad_close:I = 0x7f0d01b9

.field public static final tt_playable_ad_close_layout:I = 0x7f0d01b8

.field public static final tt_playable_ad_dislike:I = 0x7f0d01b7

.field public static final tt_playable_loading:I = 0x7f0d01ba

.field public static final tt_playable_pb_view:I = 0x7f0d0236

.field public static final tt_playable_play:I = 0x7f0d0238

.field public static final tt_playable_progress_tip:I = 0x7f0d0237

.field public static final tt_positive:I = 0x7f0d0218

.field public static final tt_privacy_layout:I = 0x7f0d01cd

.field public static final tt_privacy_list:I = 0x7f0d01d5

.field public static final tt_privacy_webview:I = 0x7f0d01ce

.field public static final tt_rb_score:I = 0x7f0d019e

.field public static final tt_rb_score_backup:I = 0x7f0d01a7

.field public static final tt_reward_ad_appname:I = 0x7f0d019d

.field public static final tt_reward_ad_appname_backup:I = 0x7f0d01a6

.field public static final tt_reward_ad_download:I = 0x7f0d01a1

.field public static final tt_reward_ad_download_backup:I = 0x7f0d01a9

.field public static final tt_reward_ad_download_layout:I = 0x7f0d01b1

.field public static final tt_reward_ad_icon:I = 0x7f0d019c

.field public static final tt_reward_ad_icon_backup:I = 0x7f0d01a5

.field public static final tt_reward_browser_webview:I = 0x7f0d01a2

.field public static final tt_reward_full_endcard_backup:I = 0x7f0d01a4

.field public static final tt_reward_playable_loading:I = 0x7f0d0198

.field public static final tt_reward_root:I = 0x7f0d0196

.field public static final tt_rl_download:I = 0x7f0d01c3

.field public static final tt_root_view:I = 0x7f0d0248

.field public static final tt_scroll_view:I = 0x7f0d01c0

.field public static final tt_splash_ad_gif:I = 0x7f0d023b

.field public static final tt_splash_express_container:I = 0x7f0d0239

.field public static final tt_splash_skip_btn:I = 0x7f0d023d

.field public static final tt_splash_video_ad_mute:I = 0x7f0d023c

.field public static final tt_splash_video_container:I = 0x7f0d023a

.field public static final tt_title:I = 0x7f0d0213

.field public static final tt_titlebar_app_detail:I = 0x7f0d0208

.field public static final tt_titlebar_app_name:I = 0x7f0d0207

.field public static final tt_titlebar_app_privacy:I = 0x7f0d0209

.field public static final tt_titlebar_back:I = 0x7f0d01bc

.field public static final tt_titlebar_close:I = 0x7f0d01bd

.field public static final tt_titlebar_detail_layout:I = 0x7f0d0205

.field public static final tt_titlebar_developer:I = 0x7f0d0206

.field public static final tt_titlebar_dislike:I = 0x7f0d01bf

.field public static final tt_titlebar_title:I = 0x7f0d01be

.field public static final tt_top_countdown:I = 0x7f0d023e

.field public static final tt_top_dislike:I = 0x7f0d0241

.field public static final tt_top_layout_proxy:I = 0x7f0d0199

.field public static final tt_top_mute:I = 0x7f0d023f

.field public static final tt_top_skip:I = 0x7f0d0240

.field public static final tt_video_ad_bottom_layout:I = 0x7f0d0258

.field public static final tt_video_ad_button:I = 0x7f0d01c7

.field public static final tt_video_ad_button_draw:I = 0x7f0d0246

.field public static final tt_video_ad_close:I = 0x7f0d01ab

.field public static final tt_video_ad_close_layout:I = 0x7f0d01aa

.field public static final tt_video_ad_cover:I = 0x7f0d025d

.field public static final tt_video_ad_cover_center_layout:I = 0x7f0d0244

.field public static final tt_video_ad_cover_center_layout_draw:I = 0x7f0d0245

.field public static final tt_video_ad_covers:I = 0x7f0d0242

.field public static final tt_video_ad_finish_cover_image:I = 0x7f0d0243

.field public static final tt_video_ad_full_screen:I = 0x7f0d025c

.field public static final tt_video_ad_logo_image:I = 0x7f0d01c4

.field public static final tt_video_ad_name:I = 0x7f0d01c6

.field public static final tt_video_ad_replay:I = 0x7f0d0247

.field public static final tt_video_app_detail:I = 0x7f0d01cb

.field public static final tt_video_app_detail_layout:I = 0x7f0d01c8

.field public static final tt_video_app_name:I = 0x7f0d01ca

.field public static final tt_video_app_privacy:I = 0x7f0d01cc

.field public static final tt_video_back:I = 0x7f0d025e

.field public static final tt_video_btn_ad_image_tv:I = 0x7f0d01c5

.field public static final tt_video_close:I = 0x7f0d0250

.field public static final tt_video_current_time:I = 0x7f0d0256

.field public static final tt_video_developer:I = 0x7f0d01c9

.field public static final tt_video_draw_layout_viewStub:I = 0x7f0d025f

.field public static final tt_video_fullscreen_back:I = 0x7f0d0253

.field public static final tt_video_loading_cover_image:I = 0x7f0d024a

.field public static final tt_video_loading_progress:I = 0x7f0d024b

.field public static final tt_video_loading_retry:I = 0x7f0d024c

.field public static final tt_video_loading_retry_layout:I = 0x7f0d0249

.field public static final tt_video_play:I = 0x7f0d024f

.field public static final tt_video_progress:I = 0x7f0d0257

.field public static final tt_video_retry:I = 0x7f0d024d

.field public static final tt_video_retry_des:I = 0x7f0d024e

.field public static final tt_video_reward_bar:I = 0x7f0d019b

.field public static final tt_video_reward_container:I = 0x7f0d0197

.field public static final tt_video_seekbar:I = 0x7f0d025a

.field public static final tt_video_time_left_time:I = 0x7f0d025b

.field public static final tt_video_time_play:I = 0x7f0d0259

.field public static final tt_video_title:I = 0x7f0d0251

.field public static final tt_video_top_layout:I = 0x7f0d0252

.field public static final tt_video_top_title:I = 0x7f0d0254

.field public static final tt_video_traffic_continue_play_btn:I = 0x7f0d0263

.field public static final tt_video_traffic_continue_play_tv:I = 0x7f0d0264

.field public static final tt_video_traffic_tip_layout:I = 0x7f0d0261

.field public static final tt_video_traffic_tip_layout_viewStub:I = 0x7f0d0260

.field public static final tt_video_traffic_tip_tv:I = 0x7f0d0262

.field public static final tv_app_detail:I = 0x7f0d0271

.field public static final tv_app_developer:I = 0x7f0d0270

.field public static final tv_app_name:I = 0x7f0d026e

.field public static final tv_app_privacy:I = 0x7f0d0272

.field public static final tv_app_version:I = 0x7f0d026f

.field public static final tv_empty:I = 0x7f0d0268

.field public static final tv_give_up:I = 0x7f0d0273

.field public static final tv_permission_description:I = 0x7f0d0278

.field public static final tv_permission_title:I = 0x7f0d0277

.field public static final web_frame:I = 0x7f0d01c2


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
