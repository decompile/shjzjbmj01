.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final notification_action_background:I = 0x7f0200d1

.field public static final notification_bg:I = 0x7f0200d2

.field public static final notification_bg_low:I = 0x7f0200d3

.field public static final notification_bg_low_normal:I = 0x7f0200d4

.field public static final notification_bg_low_pressed:I = 0x7f0200d5

.field public static final notification_bg_normal:I = 0x7f0200d6

.field public static final notification_bg_normal_pressed:I = 0x7f0200d7

.field public static final notification_icon_background:I = 0x7f0200d8

.field public static final notification_template_icon_bg:I = 0x7f020161

.field public static final notification_template_icon_low_bg:I = 0x7f020162

.field public static final notification_tile_bg:I = 0x7f0200d9

.field public static final notify_panel_notification_icon_bg:I = 0x7f0200da

.field public static final tt_ad_backup_bk:I = 0x7f0200dd

.field public static final tt_ad_backup_bk2:I = 0x7f0200de

.field public static final tt_ad_cover_btn_begin_bg:I = 0x7f0200df

.field public static final tt_ad_cover_btn_draw_begin_bg:I = 0x7f0200e0

.field public static final tt_ad_download_progress_bar_horizontal:I = 0x7f0200e1

.field public static final tt_ad_logo:I = 0x7f0200e2

.field public static final tt_ad_logo_background:I = 0x7f0200e3

.field public static final tt_ad_logo_reward_full:I = 0x7f0200e4

.field public static final tt_ad_logo_small:I = 0x7f0200e5

.field public static final tt_ad_logo_small_rectangle:I = 0x7f0200e6

.field public static final tt_ad_skip_btn_bg:I = 0x7f0200e7

.field public static final tt_app_detail_back_btn:I = 0x7f0200e8

.field public static final tt_app_detail_bg:I = 0x7f0200e9

.field public static final tt_app_detail_black:I = 0x7f0200ea

.field public static final tt_app_detail_info:I = 0x7f0200eb

.field public static final tt_appdownloader_action_bg:I = 0x7f0200ec

.field public static final tt_appdownloader_action_new_bg:I = 0x7f0200ed

.field public static final tt_appdownloader_ad_detail_download_progress:I = 0x7f0200ee

.field public static final tt_appdownloader_detail_download_success_bg:I = 0x7f0200ef

.field public static final tt_appdownloader_download_progress_bar_horizontal:I = 0x7f0200f0

.field public static final tt_appdownloader_download_progress_bar_horizontal_new:I = 0x7f0200f1

.field public static final tt_appdownloader_download_progress_bar_horizontal_night:I = 0x7f0200f2

.field public static final tt_back_video:I = 0x7f0200f3

.field public static final tt_backup_btn_1:I = 0x7f0200f4

.field public static final tt_backup_btn_2:I = 0x7f0200f5

.field public static final tt_browser_download_selector:I = 0x7f0200f6

.field public static final tt_browser_progress_style:I = 0x7f0200f7

.field public static final tt_circle_solid_mian:I = 0x7f0200f8

.field public static final tt_close_move_detail:I = 0x7f0200f9

.field public static final tt_close_move_details_normal:I = 0x7f0200fa

.field public static final tt_close_move_details_pressed:I = 0x7f0200fb

.field public static final tt_comment_tv:I = 0x7f0200fc

.field public static final tt_common_download_bg:I = 0x7f0200fd

.field public static final tt_common_download_btn_bg:I = 0x7f0200fe

.field public static final tt_custom_dialog_bg:I = 0x7f0200ff

.field public static final tt_detail_video_btn_bg:I = 0x7f020100

.field public static final tt_dislike_bottom_seletor:I = 0x7f020101

.field public static final tt_dislike_cancle_bg_selector:I = 0x7f020102

.field public static final tt_dislike_dialog_bg:I = 0x7f020103

.field public static final tt_dislike_flowlayout_tv_bg:I = 0x7f020104

.field public static final tt_dislike_icon:I = 0x7f020105

.field public static final tt_dislike_icon2:I = 0x7f020106

.field public static final tt_dislike_middle_seletor:I = 0x7f020107

.field public static final tt_dislike_son_tag:I = 0x7f020108

.field public static final tt_dislike_top_bg:I = 0x7f020109

.field public static final tt_dislike_top_seletor:I = 0x7f02010a

.field public static final tt_download_btn_bg:I = 0x7f02010b

.field public static final tt_download_corner_bg:I = 0x7f02010c

.field public static final tt_download_dialog_btn_bg:I = 0x7f02010d

.field public static final tt_draw_back_bg:I = 0x7f02010e

.field public static final tt_enlarge_video:I = 0x7f02010f

.field public static final tt_forward_video:I = 0x7f020110

.field public static final tt_install_bk:I = 0x7f020111

.field public static final tt_install_btn_bk:I = 0x7f020112

.field public static final tt_leftbackbutton_titlebar_photo_preview:I = 0x7f020113

.field public static final tt_leftbackicon_selector:I = 0x7f020114

.field public static final tt_leftbackicon_selector_for_dark:I = 0x7f020115

.field public static final tt_lefterbackicon_titlebar:I = 0x7f020116

.field public static final tt_lefterbackicon_titlebar_for_dark:I = 0x7f020117

.field public static final tt_lefterbackicon_titlebar_press:I = 0x7f020118

.field public static final tt_lefterbackicon_titlebar_press_for_dark:I = 0x7f020119

.field public static final tt_mute:I = 0x7f02011a

.field public static final tt_mute_btn_bg:I = 0x7f02011b

.field public static final tt_new_pause_video:I = 0x7f02011c

.field public static final tt_new_pause_video_press:I = 0x7f02011d

.field public static final tt_new_play_video:I = 0x7f02011e

.field public static final tt_normalscreen_loading:I = 0x7f02011f

.field public static final tt_open_app_detail_download_btn_bg:I = 0x7f020120

.field public static final tt_open_app_detail_list_item:I = 0x7f020121

.field public static final tt_play_movebar_textpage:I = 0x7f020122

.field public static final tt_playable_btn_bk:I = 0x7f020123

.field public static final tt_playable_l_logo:I = 0x7f020124

.field public static final tt_playable_progress_style:I = 0x7f020125

.field public static final tt_refreshing_video_textpage:I = 0x7f020126

.field public static final tt_refreshing_video_textpage_normal:I = 0x7f020127

.field public static final tt_refreshing_video_textpage_pressed:I = 0x7f020128

.field public static final tt_reward_countdown_bg:I = 0x7f020129

.field public static final tt_reward_dislike_icon:I = 0x7f02012a

.field public static final tt_reward_full_new_bar_bg:I = 0x7f02012b

.field public static final tt_reward_full_new_bar_btn_bg:I = 0x7f02012c

.field public static final tt_reward_full_video_backup_btn_bg:I = 0x7f02012d

.field public static final tt_reward_video_download_btn_bg:I = 0x7f02012e

.field public static final tt_right_arrow:I = 0x7f02012f

.field public static final tt_seek_progress:I = 0x7f020130

.field public static final tt_seek_thumb:I = 0x7f020131

.field public static final tt_seek_thumb_fullscreen:I = 0x7f020132

.field public static final tt_seek_thumb_fullscreen_press:I = 0x7f020133

.field public static final tt_seek_thumb_fullscreen_selector:I = 0x7f020134

.field public static final tt_seek_thumb_normal:I = 0x7f020135

.field public static final tt_seek_thumb_press:I = 0x7f020136

.field public static final tt_shadow_btn_back:I = 0x7f020137

.field public static final tt_shadow_btn_back_withoutnight:I = 0x7f020138

.field public static final tt_shadow_fullscreen_top:I = 0x7f020139

.field public static final tt_shadow_lefterback_titlebar:I = 0x7f02013a

.field public static final tt_shadow_lefterback_titlebar_press:I = 0x7f02013b

.field public static final tt_shadow_lefterback_titlebar_press_withoutnight:I = 0x7f02013c

.field public static final tt_shadow_lefterback_titlebar_withoutnight:I = 0x7f02013d

.field public static final tt_shrink_fullscreen:I = 0x7f02013e

.field public static final tt_shrink_video:I = 0x7f02013f

.field public static final tt_skip_text_bg:I = 0x7f020140

.field public static final tt_splash_ad_logo:I = 0x7f020141

.field public static final tt_splash_mute:I = 0x7f020142

.field public static final tt_splash_unmute:I = 0x7f020143

.field public static final tt_star_empty_bg:I = 0x7f020144

.field public static final tt_star_full_bg:I = 0x7f020145

.field public static final tt_stop_movebar_textpage:I = 0x7f020146

.field public static final tt_suggestion_logo:I = 0x7f020147

.field public static final tt_titlebar_close_drawable:I = 0x7f020148

.field public static final tt_titlebar_close_for_dark:I = 0x7f020149

.field public static final tt_titlebar_close_press:I = 0x7f02014a

.field public static final tt_titlebar_close_press_for_dark:I = 0x7f02014b

.field public static final tt_titlebar_close_seletor:I = 0x7f02014c

.field public static final tt_titlebar_close_seletor_for_dark:I = 0x7f02014d

.field public static final tt_unmute:I = 0x7f02014e

.field public static final tt_video_black_desc_gradient:I = 0x7f02014f

.field public static final tt_video_close_drawable:I = 0x7f020150

.field public static final tt_video_loading_progress_bar:I = 0x7f020151

.field public static final tt_video_progress_drawable:I = 0x7f020152

.field public static final tt_video_traffic_continue_play_bg:I = 0x7f020153

.field public static final tt_white_lefterbackicon_titlebar:I = 0x7f020154

.field public static final tt_white_lefterbackicon_titlebar_press:I = 0x7f020155

.field public static final ttdownloader_bg_appinfo_btn:I = 0x7f020156

.field public static final ttdownloader_bg_appinfo_dialog:I = 0x7f020157

.field public static final ttdownloader_bg_button_blue_corner:I = 0x7f020158

.field public static final ttdownloader_bg_kllk_btn1:I = 0x7f020159

.field public static final ttdownloader_bg_kllk_btn2:I = 0x7f02015a

.field public static final ttdownloader_bg_transparent:I = 0x7f02015b

.field public static final ttdownloader_bg_white_corner:I = 0x7f02015c

.field public static final ttdownloader_dash_line:I = 0x7f02015d

.field public static final ttdownloader_icon_back_arrow:I = 0x7f02015e

.field public static final ttdownloader_icon_download:I = 0x7f02015f

.field public static final ttdownloader_icon_yes:I = 0x7f020160


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
