.class public final Lcom/bytedance/sdk/openadsdk/adhost/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/adhost/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final notification_action_color_filter:I = 0x7f0c0000

.field public static final notification_icon_bg_color:I = 0x7f0c0058

.field public static final primary_text_default_material_dark:I = 0x7f0c005d

.field public static final ripple_material_light:I = 0x7f0c0062

.field public static final secondary_text_default_material_dark:I = 0x7f0c0063

.field public static final secondary_text_default_material_light:I = 0x7f0c0064

.field public static final tt_app_detail_bg:I = 0x7f0c006e

.field public static final tt_app_detail_line_bg:I = 0x7f0c006f

.field public static final tt_app_detail_privacy_text_bg:I = 0x7f0c0070

.field public static final tt_app_detail_stroke_bg:I = 0x7f0c0071

.field public static final tt_appdownloader_notification_material_background_color:I = 0x7f0c0072

.field public static final tt_appdownloader_notification_title_color:I = 0x7f0c0073

.field public static final tt_appdownloader_s1:I = 0x7f0c0074

.field public static final tt_appdownloader_s13:I = 0x7f0c0075

.field public static final tt_appdownloader_s18:I = 0x7f0c0076

.field public static final tt_appdownloader_s4:I = 0x7f0c0077

.field public static final tt_appdownloader_s8:I = 0x7f0c0078

.field public static final tt_cancle_bg:I = 0x7f0c0079

.field public static final tt_common_download_bg:I = 0x7f0c007a

.field public static final tt_common_download_btn_bg:I = 0x7f0c007b

.field public static final tt_dislike_dialog_background:I = 0x7f0c007c

.field public static final tt_dislike_transparent:I = 0x7f0c007d

.field public static final tt_divider:I = 0x7f0c007e

.field public static final tt_download_app_name:I = 0x7f0c007f

.field public static final tt_download_bar_background:I = 0x7f0c0080

.field public static final tt_download_bar_background_new:I = 0x7f0c0081

.field public static final tt_download_text_background:I = 0x7f0c0082

.field public static final tt_draw_btn_back:I = 0x7f0c0083

.field public static final tt_full_screen_skip_bg:I = 0x7f0c0084

.field public static final tt_header_font:I = 0x7f0c0085

.field public static final tt_heise3:I = 0x7f0c0086

.field public static final tt_listview:I = 0x7f0c0087

.field public static final tt_listview_press:I = 0x7f0c0088

.field public static final tt_rating_comment:I = 0x7f0c008a

.field public static final tt_rating_comment_vertical:I = 0x7f0c008b

.field public static final tt_rating_star:I = 0x7f0c008c

.field public static final tt_skip_red:I = 0x7f0c008d

.field public static final tt_ssxinbaise4:I = 0x7f0c008e

.field public static final tt_ssxinbaise4_press:I = 0x7f0c008f

.field public static final tt_ssxinheihui3:I = 0x7f0c0090

.field public static final tt_ssxinhongse1:I = 0x7f0c0091

.field public static final tt_ssxinmian1:I = 0x7f0c0092

.field public static final tt_ssxinmian11:I = 0x7f0c0093

.field public static final tt_ssxinmian15:I = 0x7f0c0094

.field public static final tt_ssxinmian6:I = 0x7f0c0095

.field public static final tt_ssxinmian7:I = 0x7f0c0096

.field public static final tt_ssxinmian8:I = 0x7f0c0097

.field public static final tt_ssxinxian11:I = 0x7f0c0098

.field public static final tt_ssxinxian11_selected:I = 0x7f0c0099

.field public static final tt_ssxinxian3:I = 0x7f0c009a

.field public static final tt_ssxinxian3_press:I = 0x7f0c009b

.field public static final tt_ssxinzi12:I = 0x7f0c009c

.field public static final tt_ssxinzi15:I = 0x7f0c009d

.field public static final tt_ssxinzi4:I = 0x7f0c009e

.field public static final tt_ssxinzi9:I = 0x7f0c009f

.field public static final tt_text_font:I = 0x7f0c00a0

.field public static final tt_titlebar_background_dark:I = 0x7f0c00a1

.field public static final tt_titlebar_background_ffffff:I = 0x7f0c00a2

.field public static final tt_titlebar_background_light:I = 0x7f0c00a3

.field public static final tt_trans_black:I = 0x7f0c00a4

.field public static final tt_trans_half_black:I = 0x7f0c00a5

.field public static final tt_transparent:I = 0x7f0c00a6

.field public static final tt_video_player_text:I = 0x7f0c00a7

.field public static final tt_video_player_text_withoutnight:I = 0x7f0c00a8

.field public static final tt_video_playerbg_color:I = 0x7f0c00a9

.field public static final tt_video_shadow_color:I = 0x7f0c00aa

.field public static final tt_video_shaoow_color_fullscreen:I = 0x7f0c00ab

.field public static final tt_video_time_color:I = 0x7f0c00ac

.field public static final tt_video_traffic_tip_background_color:I = 0x7f0c00ad

.field public static final tt_video_transparent:I = 0x7f0c00ae

.field public static final tt_white:I = 0x7f0c00af

.field public static final ttdownloader_transparent:I = 0x7f0c00b0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
