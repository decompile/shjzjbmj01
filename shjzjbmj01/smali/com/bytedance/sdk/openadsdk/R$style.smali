.class public final Lcom/bytedance/sdk/openadsdk/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final DialogFullscreen:I

.field public static final EditTextStyle:I

.field public static final Theme_Dialog_TTDownload:I

.field public static final Theme_Dialog_TTDownloadOld:I

.field public static final tt_Widget_ProgressBar_Horizontal:I

.field public static final tt_appdownloader_style_detail_download_progress_bar:I

.field public static final tt_appdownloader_style_notification_text:I

.field public static final tt_appdownloader_style_notification_title:I

.field public static final tt_appdownloader_style_progress_bar:I

.field public static final tt_appdownloader_style_progress_bar_new:I

.field public static final tt_back_view:I

.field public static final tt_custom_dialog:I

.field public static final tt_dislikeDialog:I

.field public static final tt_dislikeDialogAnimation:I

.field public static final tt_dislikeDialog_new:I

.field public static final tt_full_screen:I

.field public static final tt_ss_popup_toast_anim:I

.field public static final tt_wg_insert_dialog:I

.field public static final tt_widget_gifView:I

.field public static final ttdownloader_translucent_dialog:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 582
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->DialogFullscreen:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->DialogFullscreen:I

    .line 583
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->EditTextStyle:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->EditTextStyle:I

    .line 584
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->Theme_Dialog_TTDownload:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->Theme_Dialog_TTDownload:I

    .line 585
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->Theme_Dialog_TTDownloadOld:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->Theme_Dialog_TTDownloadOld:I

    .line 586
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_Widget_ProgressBar_Horizontal:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_Widget_ProgressBar_Horizontal:I

    .line 587
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_detail_download_progress_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_appdownloader_style_detail_download_progress_bar:I

    .line 588
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_notification_text:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_appdownloader_style_notification_text:I

    .line 589
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_notification_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_appdownloader_style_notification_title:I

    .line 590
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_progress_bar:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_appdownloader_style_progress_bar:I

    .line 591
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_appdownloader_style_progress_bar_new:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_appdownloader_style_progress_bar_new:I

    .line 592
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_back_view:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_back_view:I

    .line 593
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_custom_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_custom_dialog:I

    .line 594
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_dislikeDialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_dislikeDialog:I

    .line 595
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_dislikeDialogAnimation:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_dislikeDialogAnimation:I

    .line 596
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_dislikeDialog_new:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_dislikeDialog_new:I

    .line 597
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_full_screen:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_full_screen:I

    .line 598
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_ss_popup_toast_anim:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_ss_popup_toast_anim:I

    .line 599
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_wg_insert_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_wg_insert_dialog:I

    .line 600
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->tt_widget_gifView:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->tt_widget_gifView:I

    .line 601
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$style;->ttdownloader_translucent_dialog:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$style;->ttdownloader_translucent_dialog:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
