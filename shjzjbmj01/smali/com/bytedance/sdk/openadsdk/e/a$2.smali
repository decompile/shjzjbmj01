.class Lcom/bytedance/sdk/openadsdk/e/a$2;
.super Ljava/lang/Object;
.source "JsAppAdDownloadManager.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/e/a;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Lorg/json/JSONObject;Ljava/lang/String;Z)Lcom/bytedance/sdk/openadsdk/downloadnew/core/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lorg/json/JSONObject;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/e/a;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/e/a;Lorg/json/JSONObject;)V
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/a$2;->b:Lcom/bytedance/sdk/openadsdk/e/a;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/e/a$2;->a:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private varargs a([Ljava/lang/String;)V
    .locals 4

    if-eqz p1, :cond_2

    .line 315
    array-length v0, p1

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    goto :goto_2

    .line 319
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "message"

    const-string v2, "success"

    .line 320
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "appad"

    .line 321
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/e/a$2;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const/4 v1, 0x0

    .line 322
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 323
    aget-object v2, p1, v1

    add-int/lit8 v3, v1, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 325
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/e/a$2;->b:Lcom/bytedance/sdk/openadsdk/e/a;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/e/a;->a(Lcom/bytedance/sdk/openadsdk/e/a;)Lcom/bytedance/sdk/openadsdk/e/b;

    move-result-object p1

    const-string v1, "app_ad_event"

    invoke-interface {p1, v1, v0}, Lcom/bytedance/sdk/openadsdk/e/b;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    const-string p1, "JsAppAdDownloadManager"

    const-string v0, "JSONException"

    .line 327
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_2
    :goto_2
    return-void
.end method


# virtual methods
.method public onDownloadActive(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 p5, 0x6

    .line 279
    new-array p5, p5, [Ljava/lang/String;

    const-string p6, "status"

    const/4 v0, 0x0

    aput-object p6, p5, v0

    const-string p6, "download_active"

    const/4 v0, 0x1

    aput-object p6, p5, v0

    const-string p6, "total_bytes"

    const/4 v0, 0x2

    aput-object p6, p5, v0

    .line 281
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, p5, p2

    const-string p1, "current_bytes"

    const/4 p2, 0x4

    aput-object p1, p5, p2

    .line 282
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x5

    aput-object p1, p5, p2

    .line 279
    invoke-direct {p0, p5}, Lcom/bytedance/sdk/openadsdk/e/a$2;->a([Ljava/lang/String;)V

    return-void
.end method

.method public onDownloadFailed(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 p5, 0x6

    .line 295
    new-array p5, p5, [Ljava/lang/String;

    const-string p6, "status"

    const/4 v0, 0x0

    aput-object p6, p5, v0

    const-string p6, "download_failed"

    const/4 v0, 0x1

    aput-object p6, p5, v0

    const-string p6, "total_bytes"

    const/4 v0, 0x2

    aput-object p6, p5, v0

    .line 297
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, p5, p2

    const-string p1, "current_bytes"

    const/4 p2, 0x4

    aput-object p1, p5, p2

    .line 298
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x5

    aput-object p1, p5, p2

    .line 295
    invoke-direct {p0, p5}, Lcom/bytedance/sdk/openadsdk/e/a$2;->a([Ljava/lang/String;)V

    return-void
.end method

.method public onDownloadFinished(JLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 p3, 0x6

    .line 303
    new-array p3, p3, [Ljava/lang/String;

    const-string p4, "status"

    const/4 v0, 0x0

    aput-object p4, p3, v0

    const-string p4, "download_finished"

    const/4 v0, 0x1

    aput-object p4, p3, v0

    const-string p4, "total_bytes"

    const/4 v0, 0x2

    aput-object p4, p3, v0

    .line 305
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p4

    const/4 v0, 0x3

    aput-object p4, p3, v0

    const-string p4, "current_bytes"

    const/4 v0, 0x4

    aput-object p4, p3, v0

    .line 306
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x5

    aput-object p1, p3, p2

    .line 303
    invoke-direct {p0, p3}, Lcom/bytedance/sdk/openadsdk/e/a$2;->a([Ljava/lang/String;)V

    return-void
.end method

.method public onDownloadPaused(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 p5, 0x6

    .line 287
    new-array p5, p5, [Ljava/lang/String;

    const-string p6, "status"

    const/4 v0, 0x0

    aput-object p6, p5, v0

    const-string p6, "download_paused"

    const/4 v0, 0x1

    aput-object p6, p5, v0

    const-string p6, "total_bytes"

    const/4 v0, 0x2

    aput-object p6, p5, v0

    .line 289
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, p5, p2

    const-string p1, "current_bytes"

    const/4 p2, 0x4

    aput-object p1, p5, p2

    .line 290
    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x5

    aput-object p1, p5, p2

    .line 287
    invoke-direct {p0, p5}, Lcom/bytedance/sdk/openadsdk/e/a$2;->a([Ljava/lang/String;)V

    return-void
.end method

.method public onIdle()V
    .locals 3

    const/4 v0, 0x2

    .line 274
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "status"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "idle"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/a$2;->a([Ljava/lang/String;)V

    return-void
.end method

.method public onInstalled(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 p1, 0x2

    .line 311
    new-array p1, p1, [Ljava/lang/String;

    const-string p2, "status"

    const/4 v0, 0x0

    aput-object p2, p1, v0

    const-string p2, "installed"

    const/4 v0, 0x1

    aput-object p2, p1, v0

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/e/a$2;->a([Ljava/lang/String;)V

    return-void
.end method
