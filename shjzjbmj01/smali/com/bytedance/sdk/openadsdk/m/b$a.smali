.class Lcom/bytedance/sdk/openadsdk/m/b$a;
.super Lcom/bytedance/sdk/openadsdk/l/g;
.source "TrackAdUrlImpl.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StaticFieldLeak"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/m/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/m/b;

.field private final b:Lcom/bytedance/sdk/openadsdk/m/e;

.field private final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/openadsdk/m/b;Lcom/bytedance/sdk/openadsdk/m/e;Ljava/lang/String;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->a:Lcom/bytedance/sdk/openadsdk/m/b;

    const-string p1, "AdsStats"

    .line 111
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/g;-><init>(Ljava/lang/String;)V

    .line 112
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    .line 113
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->c:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/openadsdk/m/b;Lcom/bytedance/sdk/openadsdk/m/e;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/m/b$1;)V
    .locals 0

    .line 104
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/m/b$a;-><init>(Lcom/bytedance/sdk/openadsdk/m/b;Lcom/bytedance/sdk/openadsdk/m/e;Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 119
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "{TS}"

    .line 120
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "__TS__"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "{TS}"

    .line 122
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "__TS__"

    .line 123
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    const-string v0, "{UID}"

    .line 125
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "__UID__"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "{UID}"

    .line 126
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "__UID__"

    .line 127
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 130
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/y;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "{OAID}"

    .line 131
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "__OAID__"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "{OAID}"

    .line 132
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "__OAID__"

    .line 133
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :cond_5
    return-object p1
.end method


# virtual methods
.method a(Ljava/lang/String;)Z
    .locals 1

    .line 140
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 145
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    :try_start_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const-string v1, "[ss_random]"

    .line 148
    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string p1, "[ss_timestamp]"

    .line 149
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    move-object v4, v0

    move-object v0, p1

    move-object p1, v4

    goto :goto_0

    :catch_1
    move-exception v0

    .line 151
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_1
    return-object p1
.end method

.method public run()V
    .locals 4

    .line 160
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/m/b$a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/e;->d()I

    move-result v0

    if-nez v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->a:Lcom/bytedance/sdk/openadsdk/m/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/m/b;->a(Lcom/bytedance/sdk/openadsdk/m/b;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->c(Lcom/bytedance/sdk/openadsdk/m/e;)V

    return-void

    .line 171
    :catch_0
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/e;->d()I

    move-result v0

    if-lez v0, :cond_9

    .line 173
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/e;->d()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 174
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->a:Lcom/bytedance/sdk/openadsdk/m/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/m/b;->a(Lcom/bytedance/sdk/openadsdk/m/b;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->a(Lcom/bytedance/sdk/openadsdk/m/e;)V

    .line 176
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->a:Lcom/bytedance/sdk/openadsdk/m/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/b;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/x;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_1

    .line 180
    :cond_4
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/m/b$a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/m/e;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 182
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/m/b$a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    :cond_5
    invoke-static {}, Lcom/bytedance/sdk/adnet/b/i;->a()Lcom/bytedance/sdk/adnet/b/i;

    move-result-object v1

    .line 186
    new-instance v2, Lcom/bytedance/sdk/adnet/b/j;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0, v1}, Lcom/bytedance/sdk/adnet/b/j;-><init>(ILjava/lang/String;Lcom/bytedance/sdk/adnet/core/m$a;)V

    .line 187
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/e;->b()Lcom/bytedance/sdk/adnet/core/e;

    move-result-object v0

    const/16 v3, 0x2710

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/adnet/core/e;->a(I)Lcom/bytedance/sdk/adnet/core/e;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/adnet/b/j;->setRetryPolicy(Lcom/bytedance/sdk/adnet/face/d;)Lcom/bytedance/sdk/adnet/core/Request;

    move-result-object v0

    .line 188
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/e;->c()Lcom/bytedance/sdk/openadsdk/i/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/i/e;->e()Lcom/bytedance/sdk/adnet/core/l;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/adnet/core/Request;->build(Lcom/bytedance/sdk/adnet/core/l;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    .line 192
    :try_start_1
    invoke-virtual {v1}, Lcom/bytedance/sdk/adnet/b/i;->b()Lcom/bytedance/sdk/adnet/core/m;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    :catch_1
    if-eqz v0, :cond_6

    .line 197
    :try_start_2
    invoke-virtual {v0}, Lcom/bytedance/sdk/adnet/core/m;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 199
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->a:Lcom/bytedance/sdk/openadsdk/m/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/m/b;->a(Lcom/bytedance/sdk/openadsdk/m/b;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->c(Lcom/bytedance/sdk/openadsdk/m/e;)V

    .line 200
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/u;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "trackurl"

    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "track success : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/m/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 205
    :cond_6
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/u;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "trackurl"

    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "track fail : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/m/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/m/e;->d()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/e;->a(I)V

    .line 210
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/m/e;->d()I

    move-result v0

    if-nez v0, :cond_8

    .line 211
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->a:Lcom/bytedance/sdk/openadsdk/m/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/m/b;->a(Lcom/bytedance/sdk/openadsdk/m/b;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->c(Lcom/bytedance/sdk/openadsdk/m/e;)V

    .line 212
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/u;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "trackurl"

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "track fail and delete : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/m/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 217
    :cond_8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->a:Lcom/bytedance/sdk/openadsdk/m/b;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/m/b;->a(Lcom/bytedance/sdk/openadsdk/m/b;)Lcom/bytedance/sdk/openadsdk/m/f;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/m/b$a;->b:Lcom/bytedance/sdk/openadsdk/m/e;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/m/f;->b(Lcom/bytedance/sdk/openadsdk/m/e;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :cond_9
    :goto_1
    return-void
.end method
