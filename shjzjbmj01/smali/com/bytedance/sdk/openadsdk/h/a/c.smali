.class public Lcom/bytedance/sdk/openadsdk/h/a/c;
.super Ljava/lang/Object;
.source "LogStatsBase.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/h/a/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/bytedance/sdk/openadsdk/h/a/c;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/bytedance/sdk/openadsdk/h/a/a;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:I

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "3.4.5.3"

    .line 21
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->d:Ljava/lang/String;

    .line 23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->f:J

    const/4 v0, 0x0

    .line 24
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->g:I

    .line 26
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->i:I

    return-void
.end method

.method public static b()Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/bytedance/sdk/openadsdk/h/a/c<",
            "Lcom/bytedance/sdk/openadsdk/h/a/c;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/bytedance/sdk/openadsdk/h/a/c;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;-><init>()V

    return-object v0
.end method

.method private p()Lorg/json/JSONObject;
    .locals 3

    .line 101
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "os"

    const/4 v2, 0x1

    .line 103
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "imei"

    .line 104
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/core/k;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "oaid"

    .line 105
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/y;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object v0
.end method

.method private q()Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method public a(I)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 186
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->g:I

    .line 187
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 122
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->a:Ljava/lang/String;

    .line 123
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public a()Lorg/json/JSONObject;
    .locals 6

    .line 37
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 39
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "type"

    .line 40
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "rit"

    .line 44
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "creative_id"

    .line 48
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 51
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "ad_sdk_version"

    .line 52
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 55
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "app_version"

    .line 56
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    :cond_4
    const-string v1, "app_version"

    .line 58
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/ak;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 60
    :goto_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->i()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_5

    const-string v1, "timestamp"

    .line 61
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->i()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 64
    :cond_5
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->j()I

    move-result v1

    if-lez v1, :cond_6

    const-string v1, "adtype"

    .line 65
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->j()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 68
    :cond_6
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "req_id"

    .line 69
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_7
    const-string v1, "error_code"

    .line 72
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 73
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "error_msg"

    .line 74
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 77
    :cond_8
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "extra"

    .line 78
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 81
    :cond_9
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "image_url"

    .line 82
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 84
    :cond_a
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "event_extra"

    .line 85
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_b
    const-string v1, "conn_type"

    .line 87
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/utils/x;->b(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 89
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    :try_start_1
    const-string v1, "device_info"

    .line 93
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->p()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-object v0
.end method

.method public b(I)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 204
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->i:I

    .line 205
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 131
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->m:Ljava/lang/String;

    .line 132
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation

    .line 177
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->f:J

    .line 178
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 140
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->b:Ljava/lang/String;

    .line 141
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 149
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->c:Ljava/lang/String;

    .line 150
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->m:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 158
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->d:Ljava/lang/String;

    .line 159
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 195
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->h:Ljava/lang/String;

    .line 196
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 213
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->j:Ljava/lang/String;

    .line 214
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->d:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 222
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->k:Ljava/lang/String;

    .line 223
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method public i()J
    .locals 2

    .line 172
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->f:J

    return-wide v0
.end method

.method public i(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 231
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->l:Ljava/lang/String;

    .line 232
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->q()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    return-object p1
.end method

.method public j()I
    .locals 1

    .line 182
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->g:I

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->h:Ljava/lang/String;

    return-object v0
.end method

.method public l()I
    .locals 1

    .line 200
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->i:I

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->j:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->k:Ljava/lang/String;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/h/a/c;->l:Ljava/lang/String;

    return-object v0
.end method
