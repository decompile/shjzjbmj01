.class public Lcom/bytedance/sdk/openadsdk/h/a;
.super Ljava/lang/Object;
.source "StatsLogManager.java"


# static fields
.field private static volatile a:Lcom/bytedance/sdk/openadsdk/h/a;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/bytedance/sdk/openadsdk/h/a;
    .locals 2

    .line 47
    sget-object v0, Lcom/bytedance/sdk/openadsdk/h/a;->a:Lcom/bytedance/sdk/openadsdk/h/a;

    if-nez v0, :cond_1

    .line 48
    const-class v0, Lcom/bytedance/sdk/openadsdk/h/a;

    monitor-enter v0

    .line 49
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/h/a;->a:Lcom/bytedance/sdk/openadsdk/h/a;

    if-nez v1, :cond_0

    .line 50
    new-instance v1, Lcom/bytedance/sdk/openadsdk/h/a;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/h/a;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/h/a;->a:Lcom/bytedance/sdk/openadsdk/h/a;

    .line 52
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 54
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/h/a;->a:Lcom/bytedance/sdk/openadsdk/h/a;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/h/a;Ljava/util/List;Lcom/bytedance/sdk/openadsdk/core/d/l;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/h/a;->b(Ljava/util/List;Lcom/bytedance/sdk/openadsdk/core/d/l;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/h/a;Lcom/bytedance/sdk/openadsdk/h/a/c;)Z
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/h/a;Ljava/lang/String;I)Z
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/h/a;->a(Ljava/lang/String;I)Z

    move-result p0

    return p0
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 5

    .line 241
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/core/d;

    move-result-object v0

    const/4 v1, 0x0

    .line 244
    invoke-virtual {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/core/d;->b(Ljava/lang/String;I)I

    move-result v2

    and-int/lit8 v3, v2, 0x2

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    and-int/2addr v2, v4

    if-eq v2, p2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_2

    add-int/lit8 p2, p2, 0x2

    .line 248
    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/d;->a(Ljava/lang/String;I)V

    :cond_2
    return v1
.end method

.method private b(Ljava/util/List;Lcom/bytedance/sdk/openadsdk/core/d/l;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/core/d/l;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/d/d;",
            ">;",
            "Lcom/bytedance/sdk/openadsdk/core/d/l;",
            "Lorg/json/JSONObject;",
            ")",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    if-eqz p1, :cond_5

    .line 374
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_5

    if-nez p2, :cond_0

    goto/16 :goto_1

    .line 377
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 378
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v2, "log_extra"

    .line 380
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "network_type"

    .line 381
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "timestamp"

    .line 382
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v2, "cid"

    .line 383
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p2, "platform"

    const-string v2, "Android"

    .line 384
    invoke-virtual {v1, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p2, "app"

    .line 385
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/i;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p2, "device_id"

    .line 386
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/core/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 387
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/utils/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/utils/c;

    move-result-object p2

    .line 388
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    if-eqz p2, :cond_1

    const-string v3, "longitude"

    .line 390
    iget v4, p2, Lcom/bytedance/sdk/openadsdk/utils/c;->b:F

    float-to-double v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v3, "latitude"

    .line 391
    iget p2, p2, Lcom/bytedance/sdk/openadsdk/utils/c;->a:F

    float-to-double v4, p2

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    :cond_1
    const-string p2, "location"

    .line 393
    invoke-virtual {v1, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 394
    new-instance p2, Lorg/json/JSONArray;

    invoke-direct {p2}, Lorg/json/JSONArray;-><init>()V

    .line 395
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/openadsdk/core/d/d;

    if-eqz v2, :cond_2

    .line 397
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/d;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :cond_3
    const-string p1, "pages"

    .line 400
    invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz p3, :cond_4

    const-string p1, "extra_info"

    .line 402
    invoke-virtual {v1, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "extra_info"

    .line 403
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "back extra info:"

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_4
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->i(Ljava/lang/String;)[B

    move-result-object p1

    .line 406
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/b;->c()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/a;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "content"

    .line 407
    invoke-virtual {v0, p2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p2, "StatsLogManager"

    .line 408
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "html content:"

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object v0

    :cond_5
    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 4

    .line 140
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "page_name"

    .line 141
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 142
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/al;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 143
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    .line 144
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    const-string v1, "delegate_on_create"

    .line 145
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    .line 146
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    const-string v1, "StatsLogManager"

    .line 147
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delegate_on_create: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object p1

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    .line 63
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 66
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 67
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .line 71
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "playable_url"

    .line 73
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :catch_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    const-string v1, "click_playable_test_tool"

    .line 78
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 79
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 81
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    .line 99
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "playable_url"

    .line 101
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "error_code"

    .line 102
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "error_message"

    .line 103
    invoke-virtual {v0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :catch_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    const-string p2, "use_playable_test_tool_error"

    .line 108
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 109
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 111
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object p2

    const/4 p3, 0x0

    invoke-interface {p2, p1, p3}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1

    .line 417
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 420
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    .line 421
    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 422
    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 423
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object p2

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Ljava/util/List;Lcom/bytedance/sdk/openadsdk/core/d/l;Lorg/json/JSONObject;)V
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/sdk/openadsdk/core/d/l;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/d/d;",
            ">;",
            "Lcom/bytedance/sdk/openadsdk/core/d/l;",
            "Lorg/json/JSONObject;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 355
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 358
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/h/a$3;

    const-string v3, "upLoadHtmlInfo"

    move-object v1, v0

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/sdk/openadsdk/h/a$3;-><init>(Lcom/bytedance/sdk/openadsdk/h/a;Ljava/lang/String;Ljava/util/List;Lcom/bytedance/sdk/openadsdk/core/d/l;Lorg/json/JSONObject;)V

    const/4 p1, 0x5

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Lcom/bytedance/sdk/openadsdk/l/g;I)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    .line 347
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    const-string v1, "app_env"

    .line 348
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    .line 349
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    .line 350
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 351
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;)V

    return-void
.end method

.method public a(Z[Ljava/lang/String;)V
    .locals 5

    .line 319
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "if_sd"

    .line 322
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    if-eqz p2, :cond_2

    .line 323
    array-length p1, p2

    if-lez p1, :cond_2

    .line 324
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 325
    array-length v1, p2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, p2, v2

    .line 326
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 327
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ","

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string p2, "permission"

    .line 330
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :catch_0
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    const-string p2, "download_permission"

    .line 336
    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 337
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    invoke-virtual {p1, v1, v2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 338
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 340
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;)V

    return-void
.end method

.method public b()V
    .locals 7

    .line 115
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i;->e()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->alist()Z

    move-result v0

    .line 116
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/i;->e()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUseLocation()Z

    move-result v1

    .line 117
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/i;->e()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUseWriteExternal()Z

    move-result v2

    .line 118
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/i;->e()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUseWifiState()Z

    move-result v3

    .line 119
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/i;->e()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/TTCustomController;->isCanUsePhoneState()Z

    move-result v4

    .line 121
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v6, "access_fine_location"

    .line 123
    invoke-virtual {v5, v6, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "applist"

    .line 124
    invoke-virtual {v5, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "external_storage"

    .line 125
    invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "wifi_state"

    .line 126
    invoke-virtual {v5, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "phone_state"

    .line 127
    invoke-virtual {v5, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :catch_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    const-string v1, "sdk_permission"

    .line 132
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    .line 133
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    .line 135
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    .line 154
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "outer_call"

    .line 158
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 159
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 162
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .line 85
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "playable_url"

    .line 87
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :catch_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b()Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    const-string v1, "close_playable_test_tool"

    .line 92
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 93
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object p1

    .line 95
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method

.method public c(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    .line 166
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "outer_call_send"

    .line 170
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 174
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method

.method public d(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    .line 178
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "outer_call_no_rsp"

    .line 182
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 183
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 186
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method

.method public e(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    .line 190
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "load_ad_duration_no_ad"

    .line 193
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 194
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 196
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;)V

    return-void
.end method

.method public f(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    .line 200
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "load_creative_error"

    .line 204
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 205
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 208
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method

.method public g(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    .line 212
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "load_timeout"

    .line 216
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 217
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 220
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method

.method public h(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    .line 224
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "express_ad_render"

    .line 227
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 228
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 229
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;)V

    return-void
.end method

.method public i(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 2
    .param p1    # Lcom/bytedance/sdk/openadsdk/h/a/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 255
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    new-instance v0, Lcom/bytedance/sdk/openadsdk/h/a$1;

    const-string v1, "markAtCreativeRegister"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/h/a$1;-><init>(Lcom/bytedance/sdk/openadsdk/h/a;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    const/4 p1, 0x5

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Lcom/bytedance/sdk/openadsdk/l/g;I)V

    :cond_0
    return-void
.end method

.method public j(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 2
    .param p1    # Lcom/bytedance/sdk/openadsdk/h/a/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 270
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    new-instance v0, Lcom/bytedance/sdk/openadsdk/h/a$2;

    const-string v1, "markAtCreativeNotRegister"

    invoke-direct {v0, p0, v1, p1}, Lcom/bytedance/sdk/openadsdk/h/a$2;-><init>(Lcom/bytedance/sdk/openadsdk/h/a;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    const/4 p1, 0x5

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Lcom/bytedance/sdk/openadsdk/l/g;I)V

    :cond_0
    return-void
.end method

.method public k(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 1
    .param p1    # Lcom/bytedance/sdk/openadsdk/h/a/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 285
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "load_icon_error"

    .line 286
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 287
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;)V

    :cond_0
    return-void
.end method

.method public l(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4

    .line 292
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "show_backup_endcard"

    .line 295
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 296
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 297
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;)V

    return-void
.end method

.method public m(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 1
    .param p1    # Lcom/bytedance/sdk/openadsdk/h/a/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 301
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;)V

    :cond_0
    return-void
.end method

.method public n(Lcom/bytedance/sdk/openadsdk/h/a/c;)V
    .locals 4
    .param p1    # Lcom/bytedance/sdk/openadsdk/h/a/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 307
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/h/a;->o(Lcom/bytedance/sdk/openadsdk/h/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "splash_creative_check"

    .line 311
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/h/a/c;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 312
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/c;->c(J)Lcom/bytedance/sdk/openadsdk/h/a/c;

    .line 315
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->i()Lcom/bytedance/sdk/openadsdk/h/c/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/h/c/a;->a(Lcom/bytedance/sdk/openadsdk/h/a/a;Z)V

    return-void
.end method
