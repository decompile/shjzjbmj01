.class Lcom/bytedance/sdk/openadsdk/k/g;
.super Lcom/bytedance/sdk/openadsdk/k/a;
.source "ProxyTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/k/g$a;,
        Lcom/bytedance/sdk/openadsdk/k/g$b;,
        Lcom/bytedance/sdk/openadsdk/k/g$c;
    }
.end annotation


# instance fields
.field private final m:Ljava/net/Socket;

.field private final n:Lcom/bytedance/sdk/openadsdk/k/g$c;

.field private final o:Lcom/bytedance/sdk/openadsdk/k/d;

.field private volatile p:Lcom/bytedance/sdk/openadsdk/k/b;

.field private volatile q:Z


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/k/g$a;)V
    .locals 2

    .line 42
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/k/g$a;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/k/g$a;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/k/a;-><init>(Lcom/bytedance/sdk/openadsdk/k/a/a;Lcom/bytedance/sdk/openadsdk/k/b/c;)V

    const/4 v0, 0x1

    .line 142
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->q:Z

    .line 44
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/k/g$a;->c:Ljava/net/Socket;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->m:Ljava/net/Socket;

    .line 45
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/k/g$a;->d:Lcom/bytedance/sdk/openadsdk/k/g$c;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->n:Lcom/bytedance/sdk/openadsdk/k/g$c;

    .line 46
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/d;->c()Lcom/bytedance/sdk/openadsdk/k/d;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->o:Lcom/bytedance/sdk/openadsdk/k/d;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/k/g;)Lcom/bytedance/sdk/openadsdk/k/d;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->o:Lcom/bytedance/sdk/openadsdk/k/d;

    return-object p0
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/k/b/a;Ljava/io/File;Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/k/c/d;,
            Lcom/bytedance/sdk/openadsdk/k/h$a;,
            Lcom/bytedance/sdk/openadsdk/k/c/a;,
            Lcom/bytedance/sdk/openadsdk/k/c/b;,
            Lcom/bytedance/sdk/adnet/err/VAdError;
        }
    .end annotation

    .line 292
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/k/g$b;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 293
    invoke-direct {p0, p1, p3, p4}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Lcom/bytedance/sdk/openadsdk/k/b/a;Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)[B

    move-result-object v0

    .line 294
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->e()V

    if-nez v0, :cond_0

    return-void

    .line 297
    :cond_0
    array-length v2, v0

    invoke-virtual {p3, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/k/g$b;->a([BII)V

    :cond_1
    const/4 v0, 0x0

    if-nez p1, :cond_4

    .line 301
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v3, v3, Lcom/bytedance/sdk/openadsdk/k/i$a;->a:I

    invoke-virtual {p1, v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/k/b/a;

    move-result-object p1

    if-nez p1, :cond_4

    .line 303
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz p1, :cond_2

    const-string p1, "TAG_PROXY_ProxyTask"

    const-string v2, "failed to get video header info from db"

    .line 304
    invoke-static {p1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :cond_2
    invoke-direct {p0, v0, p3, p4}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Lcom/bytedance/sdk/openadsdk/k/b/a;Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)[B

    .line 308
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v3, v3, Lcom/bytedance/sdk/openadsdk/k/i$a;->a:I

    invoke-virtual {p1, v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/k/b/a;

    move-result-object p1

    if-eqz p1, :cond_3

    goto :goto_0

    .line 311
    :cond_3
    new-instance p1, Lcom/bytedance/sdk/openadsdk/k/c/c;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "failed to get header, rawKey: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ", url: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/bytedance/sdk/openadsdk/k/c/c;-><init>(Ljava/lang/String;)V

    throw p1

    .line 317
    :cond_4
    :goto_0
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    iget v4, p1, Lcom/bytedance/sdk/openadsdk/k/b/a;->c:I

    int-to-long v4, v4

    cmp-long v6, v2, v4

    if-gez v6, :cond_6

    .line 319
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->p:Lcom/bytedance/sdk/openadsdk/k/b;

    if-eqz v2, :cond_5

    .line 320
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/k/b;->b()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/k/b;->d()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 321
    :cond_5
    new-instance v2, Lcom/bytedance/sdk/openadsdk/k/b$a;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/k/b$a;-><init>()V

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    .line 323
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b$a;->a(Lcom/bytedance/sdk/openadsdk/k/a/a;)Lcom/bytedance/sdk/openadsdk/k/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b$a;->a(Lcom/bytedance/sdk/openadsdk/k/b/c;)Lcom/bytedance/sdk/openadsdk/k/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b$a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b$a;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/b$a;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/k/l;

    iget-object v4, p4, Lcom/bytedance/sdk/openadsdk/k/l$a;->a:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/bytedance/sdk/openadsdk/k/l;-><init>(Ljava/lang/String;)V

    .line 324
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b$a;->a(Lcom/bytedance/sdk/openadsdk/k/l;)Lcom/bytedance/sdk/openadsdk/k/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->f:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b$a;->a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/k/b$a;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b$a;->a(Lcom/bytedance/sdk/openadsdk/k/i;)Lcom/bytedance/sdk/openadsdk/k/b$a;

    move-result-object v2

    new-instance v3, Lcom/bytedance/sdk/openadsdk/k/g$1;

    invoke-direct {v3, p0}, Lcom/bytedance/sdk/openadsdk/k/g$1;-><init>(Lcom/bytedance/sdk/openadsdk/k/g;)V

    .line 325
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b$a;->a(Lcom/bytedance/sdk/openadsdk/k/b$b;)Lcom/bytedance/sdk/openadsdk/k/b$a;

    move-result-object v2

    .line 340
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/k/b$a;->a()Lcom/bytedance/sdk/openadsdk/k/b;

    move-result-object v2

    .line 341
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->p:Lcom/bytedance/sdk/openadsdk/k/b;

    .line 343
    new-instance v3, Lcom/bytedance/sdk/openadsdk/l/f;

    const/16 v4, 0xa

    const/4 v5, 0x1

    invoke-direct {v3, v2, v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/l/f;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;II)V

    .line 344
    new-instance v2, Lcom/bytedance/sdk/openadsdk/k/g$2;

    const-string v4, "processCacheNetWorkConcurrent"

    invoke-direct {v2, p0, v4, v3}, Lcom/bytedance/sdk/openadsdk/k/g$2;-><init>(Lcom/bytedance/sdk/openadsdk/k/g;Ljava/lang/String;Lcom/bytedance/sdk/openadsdk/l/f;)V

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Lcom/bytedance/sdk/openadsdk/l/g;)V

    .line 351
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v2, :cond_7

    const-string v2, "TAG_PROXY_ProxyTask"

    const-string v4, "fire download in process cache task"

    .line 352
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    move-object v3, v0

    :cond_7
    :goto_1
    const/16 v2, 0x2000

    .line 357
    new-array v2, v2, [B

    .line 363
    :try_start_0
    new-instance v4, Lcom/bytedance/sdk/openadsdk/k/h;

    const-string v5, "r"

    invoke-direct {v4, p2, v5}, Lcom/bytedance/sdk/openadsdk/k/h;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 364
    :try_start_1
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result p2

    int-to-long v5, p2

    invoke-virtual {v4, v5, v6}, Lcom/bytedance/sdk/openadsdk/k/h;->a(J)V

    .line 365
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget p2, p2, Lcom/bytedance/sdk/openadsdk/k/i$a;->e:I

    if-lez p2, :cond_8

    iget p1, p1, Lcom/bytedance/sdk/openadsdk/k/b/a;->c:I

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget p2, p2, Lcom/bytedance/sdk/openadsdk/k/i$a;->e:I

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_2

    :cond_8
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/k/b/a;->c:I

    .line 367
    :goto_2
    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result p2

    if-ge p2, p1, :cond_10

    .line 368
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->e()V

    .line 370
    invoke-virtual {v4, v2}, Lcom/bytedance/sdk/openadsdk/k/h;->a([B)I

    move-result p2

    if-gtz p2, :cond_f

    .line 373
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->p:Lcom/bytedance/sdk/openadsdk/k/b;

    if-eqz p2, :cond_b

    .line 375
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/k/b;->i()Lcom/bytedance/sdk/openadsdk/k/c/b;

    move-result-object v0

    if-nez v0, :cond_a

    .line 380
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/k/b;->h()Lcom/bytedance/sdk/openadsdk/k/h$a;

    move-result-object v0

    if-nez v0, :cond_9

    goto :goto_3

    .line 383
    :cond_9
    throw v0

    .line 377
    :cond_a
    throw v0

    :cond_b
    :goto_3
    if-eqz p2, :cond_d

    .line 387
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/k/b;->b()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/k/b;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    goto :goto_6

    .line 394
    :cond_c
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->e()V

    .line 396
    iget-object v0, p2, Lcom/bytedance/sdk/openadsdk/k/b;->m:Ljava/lang/Object;

    monitor-enter v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 398
    :try_start_2
    iget-object p2, p2, Lcom/bytedance/sdk/openadsdk/k/b;->m:Ljava/lang/Object;

    const-wide/16 v5, 0x3e8

    invoke-virtual {p2, v5, v6}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception p1

    goto :goto_5

    :catch_0
    move-exception p2

    .line 400
    :try_start_3
    invoke-virtual {p2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 402
    :goto_4
    monitor-exit v0

    goto :goto_7

    :goto_5
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1

    .line 388
    :cond_d
    :goto_6
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz p1, :cond_e

    const-string p1, "TAG_PROXY_ProxyTask"

    const-string p2, "download task has finished!!!"

    .line 389
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_e
    new-instance p1, Lcom/bytedance/sdk/openadsdk/k/c/c;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "illegal state download task has finished, rawKey: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ", url: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/bytedance/sdk/openadsdk/k/c/c;-><init>(Ljava/lang/String;)V

    throw p1

    .line 404
    :cond_f
    invoke-virtual {p3, v2, v1, p2}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b([BII)V

    .line 407
    :goto_7
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->e()V

    goto :goto_2

    .line 410
    :cond_10
    sget-boolean p2, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz p2, :cond_11

    const-string p2, "TAG_PROXY_ProxyTask"

    .line 411
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "read cache file complete: "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result p3

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ", "

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :cond_11
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->c()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 417
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/k/h;->a()V

    if-eqz v3, :cond_12

    .line 422
    :try_start_5
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/l/f;->get()Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_8

    :catch_1
    move-exception p1

    .line 424
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_12
    :goto_8
    return-void

    :catchall_1
    move-exception p1

    goto :goto_9

    :catchall_2
    move-exception p1

    move-object v4, v0

    :goto_9
    if-eqz v4, :cond_13

    .line 417
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/k/h;->a()V

    :cond_13
    if-eqz v3, :cond_14

    .line 422
    :try_start_6
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/l/f;->get()Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_a

    :catch_2
    move-exception p2

    .line 424
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 427
    :cond_14
    :goto_a
    throw p1
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/openadsdk/k/c/d;,
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/k/h$a;,
            Lcom/bytedance/sdk/openadsdk/k/c/a;,
            Lcom/bytedance/sdk/openadsdk/k/c/b;,
            Lcom/bytedance/sdk/adnet/err/VAdError;
        }
    .end annotation

    const-string v0, "HEAD"

    .line 215
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/k/i;->a:Lcom/bytedance/sdk/openadsdk/k/i$c;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/k/i$c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/k/g;->b(Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V

    goto :goto_0

    .line 218
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/k/g;->c(Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V

    :goto_0
    return-void
.end method

.method private a(ZIIII)V
    .locals 0

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/k/g$b;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/openadsdk/k/c/a;,
            Lcom/bytedance/sdk/adnet/err/VAdError;
        }
    .end annotation

    .line 146
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->j:Lcom/bytedance/sdk/openadsdk/k/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/k/l;->a()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    .line 147
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->e()V

    .line 149
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->j:Lcom/bytedance/sdk/openadsdk/k/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/k/l;->b()Lcom/bytedance/sdk/openadsdk/k/l$a;

    move-result-object v0

    const/4 v2, 0x1

    .line 151
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V
    :try_end_0
    .catch Lcom/bytedance/sdk/openadsdk/k/c/c; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/bytedance/sdk/openadsdk/k/c/d; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/bytedance/sdk/openadsdk/k/h$a; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/bytedance/sdk/openadsdk/k/c/b; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/bytedance/sdk/adnet/err/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :catch_0
    move-exception v0

    .line 196
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v1, :cond_0

    const-string v1, "TAG_PROXY_ProxyTask"

    .line 197
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    .line 192
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v1, :cond_0

    const-string v1, "TAG_PROXY_ProxyTask"

    .line 193
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception p1

    .line 185
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v0, :cond_1

    const-string v0, "TAG_PROXY_ProxyTask"

    .line 186
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v1

    :catch_3
    move-exception v0

    .line 179
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v2, :cond_2

    const-string v2, "TAG_PROXY_ProxyTask"

    .line 180
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_2
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->q:Z

    .line 183
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->g()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_4
    move-exception p1

    .line 173
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v0, :cond_3

    const-string v0, "TAG_PROXY_ProxyTask"

    .line 174
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return v2

    :catch_5
    move-exception v1

    .line 157
    instance-of v2, v1, Ljava/net/SocketTimeoutException;

    if-eqz v2, :cond_4

    .line 158
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/k/l$a;->b()V

    .line 161
    :cond_4
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 162
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "Canceled"

    .line 163
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "TAG_PROXY_ProxyTask"

    const-string v1, "okhttp call canceled"

    .line 164
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const-string v0, "TAG_PROXY_ProxyTask"

    .line 166
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 170
    :cond_6
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->g()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_6
    move-exception v1

    .line 154
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/k/l$a;->a()V

    .line 155
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->g()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_7
    return v1
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/k/b/a;Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/adnet/err/VAdError;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 266
    sget-boolean p3, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz p3, :cond_0

    const-string p3, "TAG_PROXY_ProxyTask"

    const-string v0, "get header from db"

    .line 267
    invoke-static {p3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_0
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result p2

    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Lcom/bytedance/sdk/openadsdk/k/b/a;I)Ljava/lang/String;

    move-result-object p1

    sget-object p2, Lcom/bytedance/sdk/openadsdk/k/g/d;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, -0x1

    const-string v0, "HEAD"

    const/4 v1, 0x0

    .line 271
    invoke-virtual {p0, p3, v1, p1, v0}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Lcom/bytedance/sdk/openadsdk/k/l$a;IILjava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/e/a;

    move-result-object p1

    if-nez p1, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 274
    :cond_2
    :try_start_0
    invoke-static {p1, v1, v1}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Lcom/bytedance/sdk/openadsdk/k/e/a;ZZ)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 278
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/k/i$a;->a:I

    invoke-static {p1, p3, v0, v1}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Lcom/bytedance/sdk/openadsdk/k/e/a;Lcom/bytedance/sdk/openadsdk/k/b/c;Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/k/b/a;

    move-result-object p3

    .line 279
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v0, :cond_3

    const-string v0, "TAG_PROXY_ProxyTask"

    const-string v1, "get header from network"

    .line 280
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :cond_3
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result p2

    invoke-static {p3, p2}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Lcom/bytedance/sdk/openadsdk/k/b/a;I)Ljava/lang/String;

    move-result-object p2

    sget-object p3, Lcom/bytedance/sdk/openadsdk/k/g/d;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p2, p3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/e/a;->d()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Ljava/io/Closeable;)V

    return-object p2

    .line 276
    :cond_4
    :try_start_1
    new-instance p2, Lcom/bytedance/sdk/openadsdk/k/c/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", rawKey: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", url: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/bytedance/sdk/openadsdk/k/c/c;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p2

    .line 284
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/e/a;->d()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Ljava/io/Closeable;)V

    .line 285
    throw p2
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/k/c/d;,
            Lcom/bytedance/sdk/adnet/err/VAdError;
        }
    .end annotation

    .line 224
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/k/i$a;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/k/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/k/b/a;

    move-result-object v0

    .line 225
    invoke-direct {p0, v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Lcom/bytedance/sdk/openadsdk/k/b/a;Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)[B

    move-result-object p2

    if-nez p2, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 227
    array-length v1, p2

    invoke-virtual {p1, p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->a([BII)V

    return-void
.end method

.method private c(Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/openadsdk/k/h$a;,
            Lcom/bytedance/sdk/openadsdk/k/c/d;,
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/k/c/a;,
            Lcom/bytedance/sdk/openadsdk/k/c/b;,
            Lcom/bytedance/sdk/adnet/err/VAdError;
        }
    .end annotation

    .line 232
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->q:Z

    if-eqz v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/k/a/a;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 235
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 236
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v4, v4, Lcom/bytedance/sdk/openadsdk/k/i$a;->a:I

    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/k/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/k/b/a;

    move-result-object v7

    .line 237
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result v5

    int-to-long v2, v5

    sub-long v2, v0, v2

    long-to-int v4, v2

    if-nez v7, :cond_0

    const/4 v8, -0x1

    goto :goto_0

    .line 239
    :cond_0
    iget v8, v7, Lcom/bytedance/sdk/openadsdk/k/b/a;->c:I

    .line 240
    :goto_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result v9

    int-to-long v9, v9

    cmp-long v11, v0, v9

    if-lez v11, :cond_2

    .line 242
    sget-boolean v9, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v9, :cond_1

    const-string v9, "TAG_PROXY_ProxyTask"

    .line 243
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "cache hit, remainSize: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v2, 0x1

    long-to-int v9, v0

    move-object v0, p0

    move v1, v2

    move v2, v4

    move v3, v8

    move v4, v9

    .line 246
    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/k/g;->a(ZIIII)V

    .line 247
    invoke-direct {p0, v7, v6, p1, p2}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Lcom/bytedance/sdk/openadsdk/k/b/a;Ljava/io/File;Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V

    return-void

    :cond_2
    const/4 v2, 0x0

    long-to-int v6, v0

    move-object v0, p0

    move v1, v2

    move v2, v4

    move v3, v8

    move v4, v6

    .line 251
    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/k/g;->a(ZIIII)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 253
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/sdk/openadsdk/k/g;->a(ZIIII)V

    .line 256
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/k/g;->d(Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V

    return-void
.end method

.method private d(Lcom/bytedance/sdk/openadsdk/k/g$b;Lcom/bytedance/sdk/openadsdk/k/l$a;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/openadsdk/k/c/d;,
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/openadsdk/k/c/a;,
            Lcom/bytedance/sdk/openadsdk/k/c/b;,
            Lcom/bytedance/sdk/adnet/err/VAdError;
        }
    .end annotation

    .line 431
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->i()V

    .line 432
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 433
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result v2

    .line 434
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v3, v3, Lcom/bytedance/sdk/openadsdk/k/i$a;->e:I

    const-string v4, "GET"

    invoke-virtual {p0, p2, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Lcom/bytedance/sdk/openadsdk/k/l$a;IILjava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/e/a;

    move-result-object v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 440
    :try_start_0
    invoke-static {v3, v6, v4}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Lcom/bytedance/sdk/openadsdk/k/e/a;ZZ)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_e

    .line 445
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->f()I

    move-result v8

    invoke-virtual {v4, v7, v8}, Lcom/bytedance/sdk/openadsdk/k/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/k/b/a;

    move-result-object v4

    .line 446
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Lcom/bytedance/sdk/openadsdk/k/e/a;)I

    move-result v7

    if-eqz v4, :cond_2

    .line 447
    iget v8, v4, Lcom/bytedance/sdk/openadsdk/k/b/a;->c:I

    if-eq v8, v7, :cond_2

    .line 448
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz p1, :cond_1

    const-string p1, "TAG_PROXY_ProxyTask"

    .line 449
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Content-Length not match, old: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v8, v4, Lcom/bytedance/sdk/openadsdk/k/b/a;->c:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, ", "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, ", key: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_1
    new-instance p1, Lcom/bytedance/sdk/openadsdk/k/c/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Content-Length not match, old length: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v8, v4, Lcom/bytedance/sdk/openadsdk/k/b/a;->c:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, ", new length: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ", rawKey: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ", currentUrl: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ", previousInfo: "

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, v4, Lcom/bytedance/sdk/openadsdk/k/b/a;->e:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/bytedance/sdk/openadsdk/k/c/b;-><init>(Ljava/lang/String;)V

    throw p1

    .line 456
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->a()Z

    move-result p2

    if-nez p2, :cond_3

    .line 457
    invoke-static {v3, v2}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Lcom/bytedance/sdk/openadsdk/k/e/a;I)Ljava/lang/String;

    move-result-object p2

    .line 458
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->e()V

    .line 459
    sget-object v2, Lcom/bytedance/sdk/openadsdk/k/g/d;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p2, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p2

    .line 460
    array-length v2, p2

    invoke-virtual {p1, p2, v6, v2}, Lcom/bytedance/sdk/openadsdk/k/g$b;->a([BII)V

    .line 463
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->e()V

    .line 465
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {p2, v2}, Lcom/bytedance/sdk/openadsdk/k/a/a;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object p2

    .line 466
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->q:Z

    if-eqz v2, :cond_4

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result v2

    int-to-long v9, v2

    cmp-long v2, v7, v9

    if-ltz v2, :cond_4

    .line 467
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v7, v7, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v7, v7, Lcom/bytedance/sdk/openadsdk/k/i$a;->a:I

    invoke-static {v3, v2, v4, v7}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Lcom/bytedance/sdk/openadsdk/k/e/a;Lcom/bytedance/sdk/openadsdk/k/b/c;Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/k/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 471
    :try_start_1
    new-instance v2, Lcom/bytedance/sdk/openadsdk/k/h;

    const-string v4, "rwd"

    invoke-direct {v2, p2, v4}, Lcom/bytedance/sdk/openadsdk/k/h;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/bytedance/sdk/openadsdk/k/h$a; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 472
    :try_start_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result v4

    int-to-long v7, v4

    invoke-virtual {v2, v7, v8}, Lcom/bytedance/sdk/openadsdk/k/h;->a(J)V
    :try_end_2
    .catch Lcom/bytedance/sdk/openadsdk/k/h$a; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    :catch_0
    move-exception v4

    goto :goto_0

    :catch_1
    move-exception v4

    move-object v2, v5

    .line 474
    :goto_0
    :try_start_3
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/k/h$a;->printStackTrace()V

    move-object v2, v5

    .line 478
    :goto_1
    sget-boolean v4, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v4, :cond_6

    const-string v4, "TAG_PROXY_ProxyTask"

    .line 479
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "can write to cache file in network task, cache file size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, ", from: "

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result p2

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v4, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    .line 481
    :cond_4
    :try_start_4
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v2, :cond_5

    const-string v2, "TAG_PROXY_ProxyTask"

    .line 482
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "can\'t write to cache file in network task, cache file size: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, ", from: "

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result p2

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v2, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :cond_5
    move-object v2, v5

    .line 485
    :cond_6
    :goto_2
    :try_start_5
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->f()I

    move-result v7

    invoke-virtual {p2, v4, v7}, Lcom/bytedance/sdk/openadsdk/k/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/k/b/a;

    move-result-object p2

    if-nez p2, :cond_7

    const/4 p2, 0x0

    goto :goto_3

    .line 486
    :cond_7
    iget p2, p2, Lcom/bytedance/sdk/openadsdk/k/b/a;->c:I

    :goto_3
    const/16 v4, 0x2000

    .line 488
    new-array v4, v4, [B

    .line 490
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/k/e/a;->d()Ljava/io/InputStream;

    move-result-object v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object v8, v2

    const/4 v2, 0x0

    .line 492
    :goto_4
    :try_start_6
    invoke-virtual {v7, v4}, Ljava/io/InputStream;->read([B)I

    move-result v9

    if-ltz v9, :cond_b

    .line 493
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->e()V

    if-lez v9, :cond_a

    .line 495
    invoke-virtual {p1, v4, v6, v9}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b([BII)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    add-int/2addr v2, v9

    if-eqz v8, :cond_9

    .line 499
    :try_start_7
    invoke-virtual {v8, v4, v6, v9}, Lcom/bytedance/sdk/openadsdk/k/h;->a([BII)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_5

    :catch_2
    move-exception v9

    .line 501
    :try_start_8
    invoke-virtual {v8}, Lcom/bytedance/sdk/openadsdk/k/h;->a()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 504
    :try_start_9
    sget-boolean v8, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v8, :cond_8

    const-string v8, "TAG_PROXY_ProxyTask"

    .line 505
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "append to cache file error in network task!!! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_8
    move-object v8, v5

    goto :goto_5

    :catchall_0
    move-exception p1

    move v6, v2

    goto :goto_6

    .line 511
    :cond_9
    :goto_5
    :try_start_a
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/k/g$b;->b()I

    move-result v9

    invoke-virtual {p0, p2, v9}, Lcom/bytedance/sdk/openadsdk/k/g;->a(II)V

    .line 513
    :cond_a
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->e()V

    goto :goto_4

    .line 516
    :cond_b
    sget-boolean p1, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz p1, :cond_c

    const-string p1, "TAG_PROXY_ProxyTask"

    const-string p2, "read from net complete!"

    .line 517
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    :cond_c
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->c()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 522
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/k/e/a;->d()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Ljava/io/Closeable;)V

    if-eqz v8, :cond_d

    .line 525
    invoke-virtual {v8}, Lcom/bytedance/sdk/openadsdk/k/h;->a()V

    .line 528
    :cond_d
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 529
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    return-void

    :catchall_1
    move-exception p1

    move v6, v2

    move-object v5, v8

    goto :goto_6

    :catchall_2
    move-exception p1

    move-object v5, v2

    goto :goto_6

    .line 442
    :cond_e
    :try_start_b
    new-instance p1, Lcom/bytedance/sdk/openadsdk/k/c/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", rawKey: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", url: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/bytedance/sdk/openadsdk/k/c/c;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :catchall_3
    move-exception p1

    .line 522
    :goto_6
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/k/e/a;->d()Ljava/io/InputStream;

    move-result-object p2

    invoke-static {p2}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Ljava/io/Closeable;)V

    if-eqz v5, :cond_f

    .line 525
    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/k/h;->a()V

    .line 528
    :cond_f
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p2, v6}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 529
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p2, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 530
    throw p1
.end method

.method private h()Lcom/bytedance/sdk/openadsdk/k/g$b;
    .locals 5

    const/4 v0, 0x0

    .line 52
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->m:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/k/i;->a(Ljava/io/InputStream;)Lcom/bytedance/sdk/openadsdk/k/i;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    .line 53
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->m:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 55
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/k/i$a;->a:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/bytedance/sdk/openadsdk/k/e;->a:Lcom/bytedance/sdk/openadsdk/k/a/b;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/bytedance/sdk/openadsdk/k/e;->b:Lcom/bytedance/sdk/openadsdk/k/a/c;

    :goto_0
    if-nez v2, :cond_2

    .line 57
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v1, :cond_1

    const-string v1, "TAG_PROXY_ProxyTask"

    const-string v2, "cache is null"

    .line 58
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v0

    .line 62
    :cond_2
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    .line 64
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/k/i$a;->b:Ljava/lang/String;

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    .line 65
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/k/i$a;->c:Ljava/lang/String;

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    .line 66
    new-instance v2, Lcom/bytedance/sdk/openadsdk/k/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/k/i$a;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/l;-><init>(Ljava/util/List;)V

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->j:Lcom/bytedance/sdk/openadsdk/k/l;

    .line 67
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v2, v2, Lcom/bytedance/sdk/openadsdk/k/i;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->f:Ljava/util/List;

    .line 69
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v2, :cond_3

    const-string v2, "TAG_PROXY_ProxyTask"

    .line 70
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "request from MediaPlayer:    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/k/i;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_3
    new-instance v2, Lcom/bytedance/sdk/openadsdk/k/g$b;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v3, v3, Lcom/bytedance/sdk/openadsdk/k/i$a;->d:I

    invoke-direct {v2, v1, v3}, Lcom/bytedance/sdk/openadsdk/k/g$b;-><init>(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/bytedance/sdk/openadsdk/k/i$d; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception v1

    .line 83
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->m:Ljava/net/Socket;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Ljava/net/Socket;)V

    .line 84
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v2, :cond_4

    const-string v2, "TAG_PROXY_ProxyTask"

    .line 85
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_4
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    if-nez v2, :cond_5

    move-object v2, v0

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->g()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 88
    :goto_1
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v2, v3, v1}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_1
    move-exception v1

    .line 75
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->m:Ljava/net/Socket;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Ljava/net/Socket;)V

    .line 76
    sget-boolean v2, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v2, :cond_6

    const-string v2, "TAG_PROXY_ProxyTask"

    .line 77
    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_6
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    if-nez v2, :cond_7

    move-object v2, v0

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->g()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 81
    :goto_2
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->g:Ljava/lang/String;

    invoke-virtual {p0, v2, v3, v1}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_3
    return-object v0
.end method

.method private i()V
    .locals 2

    .line 540
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->p:Lcom/bytedance/sdk/openadsdk/k/b;

    const/4 v1, 0x0

    .line 541
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->p:Lcom/bytedance/sdk/openadsdk/k/b;

    if-eqz v0, :cond_0

    .line 543
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/k/b;->a()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .line 535
    invoke-super {p0}, Lcom/bytedance/sdk/openadsdk/k/a;->a()V

    .line 536
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->i()V

    return-void
.end method

.method public run()V
    .locals 6

    .line 95
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->h()Lcom/bytedance/sdk/openadsdk/k/g$b;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->n:Lcom/bytedance/sdk/openadsdk/k/g$c;

    if-eqz v1, :cond_1

    .line 101
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->n:Lcom/bytedance/sdk/openadsdk/k/g$c;

    invoke-interface {v1, p0}, Lcom/bytedance/sdk/openadsdk/k/g$c;->a(Lcom/bytedance/sdk/openadsdk/k/g;)V

    .line 104
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/k/a/a;->a(Ljava/lang/String;)V

    .line 106
    sget v1, Lcom/bytedance/sdk/openadsdk/k/e;->h:I

    if-eqz v1, :cond_3

    .line 108
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->b:Lcom/bytedance/sdk/openadsdk/k/b/c;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->i:Lcom/bytedance/sdk/openadsdk/k/i;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/k/i;->c:Lcom/bytedance/sdk/openadsdk/k/i$a;

    iget v3, v3, Lcom/bytedance/sdk/openadsdk/k/i$a;->a:I

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/k/b/c;->a(Ljava/lang/String;I)Lcom/bytedance/sdk/openadsdk/k/b/a;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 110
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/g;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/k/a/a;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/k/b/a;->c:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 111
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->o:Lcom/bytedance/sdk/openadsdk/k/d;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->g()Z

    move-result v2

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/k/d;->a(ZLjava/lang/String;)V

    .line 116
    :cond_3
    :try_start_0
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/k/g;->a(Lcom/bytedance/sdk/openadsdk/k/g$b;)Z
    :try_end_0
    .catch Lcom/bytedance/sdk/openadsdk/k/c/a; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/bytedance/sdk/adnet/err/VAdError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 125
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v1, :cond_4

    const-string v1, "TAG_PROXY_ProxyTask"

    .line 126
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    .line 123
    invoke-virtual {v0}, Lcom/bytedance/sdk/adnet/err/VAdError;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    .line 119
    sget-boolean v1, Lcom/bytedance/sdk/openadsdk/k/e;->c:Z

    if-eqz v1, :cond_4

    const-string v1, "TAG_PROXY_ProxyTask"

    .line 120
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->a:Lcom/bytedance/sdk/openadsdk/k/a/a;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/k/g;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/k/a/a;->b(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->o:Lcom/bytedance/sdk/openadsdk/k/d;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->g()Z

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/k/d;->a(ZLjava/lang/String;)V

    .line 133
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/k/g;->a()V

    .line 135
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->m:Ljava/net/Socket;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Ljava/net/Socket;)V

    .line 137
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->n:Lcom/bytedance/sdk/openadsdk/k/g$c;

    if-eqz v0, :cond_5

    .line 138
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/g;->n:Lcom/bytedance/sdk/openadsdk/k/g$c;

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/k/g$c;->b(Lcom/bytedance/sdk/openadsdk/k/g;)V

    :cond_5
    return-void
.end method
