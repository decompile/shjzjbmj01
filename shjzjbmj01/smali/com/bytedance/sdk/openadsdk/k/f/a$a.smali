.class Lcom/bytedance/sdk/openadsdk/k/f/a$a;
.super Ljava/lang/Thread;
.source "VideoCachePreloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/k/f/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/k/f/a;

.field private b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private final d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/bytedance/sdk/openadsdk/k/f/a;)V
    .locals 1

    .line 170
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->a:Lcom/bytedance/sdk/openadsdk/k/f/a;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 167
    new-instance p1, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v0, 0xa

    invoke-direct {p1, v0}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->d:Ljava/util/Queue;

    .line 171
    new-instance p1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {p1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->b:Ljava/util/Queue;

    const/4 p1, 0x1

    .line 172
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->c:Z

    .line 173
    new-instance p1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {p1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->e:Ljava/util/Queue;

    return-void
.end method

.method private a(ILcom/bytedance/sdk/openadsdk/k/f/b;)Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;
    .locals 3

    .line 189
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->b()V

    const-string v0, "VideoCachePreloader"

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pool: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->d:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;

    if-nez v0, :cond_0

    .line 193
    new-instance v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;-><init>(Lcom/bytedance/sdk/openadsdk/k/f/a$a;)V

    .line 195
    :cond_0
    iput p1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->a:I

    .line 196
    iput-object p2, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->f:Lcom/bytedance/sdk/openadsdk/k/f/b;

    return-object v0
.end method

.method private a()V
    .locals 0

    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;)V
    .locals 2

    .line 201
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->a()V

    const/4 v0, 0x0

    .line 202
    iput-object v0, p1, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->c:[Ljava/lang/String;

    .line 203
    iput-object v0, p1, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->b:Ljava/lang/String;

    const/4 v1, -0x1

    .line 204
    iput v1, p1, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->a:I

    .line 205
    iput-object v0, p1, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->f:Lcom/bytedance/sdk/openadsdk/k/f/b;

    .line 206
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->d:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method private b()V
    .locals 0

    return-void
.end method

.method private declared-synchronized b(Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;)V
    .locals 1

    monitor-enter p0

    .line 221
    :try_start_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->b()V

    .line 222
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->e:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 223
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    .line 220
    monitor-exit p0

    throw p1
.end method

.method private c()V
    .locals 4

    .line 318
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->a()V

    .line 320
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;

    if-eqz v0, :cond_1

    .line 321
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->f:Lcom/bytedance/sdk/openadsdk/k/f/b;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/k/f/b;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->b:Ljava/lang/String;

    const/4 v1, 0x1

    .line 322
    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->f:Lcom/bytedance/sdk/openadsdk/k/f/b;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/k/f/b;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->c:[Ljava/lang/String;

    .line 323
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->f:Lcom/bytedance/sdk/openadsdk/k/f/b;

    iget v1, v1, Lcom/bytedance/sdk/openadsdk/k/f/b;->b:I

    iput v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->d:I

    .line 324
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->f:Lcom/bytedance/sdk/openadsdk/k/f/b;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/k/f/b;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->e:Ljava/lang/String;

    .line 325
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->f:Lcom/bytedance/sdk/openadsdk/k/f/b;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/k/f/b;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 326
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->f:Lcom/bytedance/sdk/openadsdk/k/f/b;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/k/f/b;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->b:Ljava/lang/String;

    :cond_0
    const/4 v1, 0x0

    .line 328
    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->f:Lcom/bytedance/sdk/openadsdk/k/f/b;

    .line 329
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->c(Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private c(Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;)V
    .locals 1

    .line 255
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->a()V

    if-nez p1, :cond_0

    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 258
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/openadsdk/k/f/b;)V
    .locals 1

    const/4 v0, 0x0

    .line 334
    invoke-direct {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->a(ILcom/bytedance/sdk/openadsdk/k/f/b;)Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->b(Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;)V

    return-void
.end method

.method public run()V
    .locals 8

    .line 263
    :goto_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->c:Z

    if-eqz v0, :cond_7

    .line 264
    monitor-enter p0

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->c()V

    .line 268
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 269
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;

    if-nez v0, :cond_1

    goto :goto_1

    .line 271
    :cond_1
    iget v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->a:I

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_3

    .line 302
    :pswitch_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/d;->c()Lcom/bytedance/sdk/openadsdk/k/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/k/d;->d()V

    .line 303
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->c:Z

    goto/16 :goto_3

    .line 293
    :pswitch_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/d;->c()Lcom/bytedance/sdk/openadsdk/k/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/k/d;->d()V

    .line 294
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/e;->c()Lcom/bytedance/sdk/openadsdk/k/a/b;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 295
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/e;->c()Lcom/bytedance/sdk/openadsdk/k/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/k/a/b;->a()V

    .line 297
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/e;->b()Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 298
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/e;->b()Lcom/bytedance/sdk/openadsdk/k/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/k/a/c;->a()V

    goto :goto_3

    .line 290
    :pswitch_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/d;->c()Lcom/bytedance/sdk/openadsdk/k/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/k/d;->d()V

    goto :goto_3

    .line 287
    :pswitch_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/d;->c()Lcom/bytedance/sdk/openadsdk/k/d;

    move-result-object v1

    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/k/d;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 274
    :pswitch_4
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->c:[Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->c:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 275
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 276
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->c:[Ljava/lang/String;

    array-length v4, v3

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 277
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/k/g/d;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 278
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 281
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, [Ljava/lang/String;

    .line 282
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v4, v1, 0x1

    .line 283
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/d;->c()Lcom/bytedance/sdk/openadsdk/k/d;

    move-result-object v2

    const/4 v3, 0x0

    iget v5, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->d:I

    iget-object v6, v0, Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;->b:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Lcom/bytedance/sdk/openadsdk/k/d;->a(ZZILjava/lang/String;[Ljava/lang/String;)V

    .line 306
    :cond_5
    :goto_3
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/k/f/a$a;->a(Lcom/bytedance/sdk/openadsdk/k/f/a$a$a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 309
    :cond_6
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :catch_0
    move-exception v0

    .line 311
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 313
    :goto_4
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_7
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
