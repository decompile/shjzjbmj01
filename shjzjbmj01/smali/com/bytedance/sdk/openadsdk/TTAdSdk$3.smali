.class final Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;
.super Lcom/bytedance/sdk/openadsdk/l/g;
.source "TTAdSdk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/TTAdSdk;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/TTAdConfig;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)V
    .locals 0

    .line 204
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->b:Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/l/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 208
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->J()Z

    move-result v1

    if-nez v1, :cond_1

    .line 210
    monitor-enter v0

    .line 211
    :try_start_0
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->J()Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->a()V

    .line 214
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 217
    :cond_1
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/e;->c()Lcom/bytedance/sdk/openadsdk/i/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/i/e;->f()Lcom/bytedance/sdk/adnet/core/l;

    .line 219
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->a(Landroid/content/Context;)V

    .line 221
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->getInstance()Lcom/bytedance/sdk/openadsdk/AppLogHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/AppLogHelper;->initAppLog(Landroid/content/Context;)V

    .line 222
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i;->h()V

    .line 224
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->b:Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isPaid()Z

    move-result v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->updatePaid(Z)V

    .line 228
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/e;->c()Lcom/bytedance/sdk/openadsdk/i/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/i/e;->e()Lcom/bytedance/sdk/adnet/core/l;

    .line 229
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/i/e;->c()Lcom/bytedance/sdk/openadsdk/i/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/i/e;->d()Lcom/bytedance/sdk/adnet/core/l;

    .line 230
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/m;->a(Landroid/content/Context;)V

    .line 231
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->b:Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/u;->b()V

    :cond_2
    const/4 v0, 0x1

    .line 235
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Z)V

    .line 236
    new-instance v0, Lcom/bytedance/sdk/openadsdk/h/b/a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/h/b/a;-><init>()V

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/l/e;->a(Lcom/bytedance/sdk/openadsdk/l/c;)V

    .line 238
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->b:Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->a()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->openDebugMode()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    .line 241
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/d;->a(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/utils/c;

    .line 242
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/i;->a(Landroid/content/Context;)V

    .line 244
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/TTAdSdk$3;->a:Landroid/content/Context;

    const/16 v1, 0x14

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/video/a/b/a;->a(Landroid/content/Context;I)V

    return-void
.end method
