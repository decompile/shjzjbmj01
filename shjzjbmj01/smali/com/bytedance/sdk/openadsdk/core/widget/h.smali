.class public Lcom/bytedance/sdk/openadsdk/core/widget/h;
.super Ljava/lang/Object;
.source "VideoTrafficTipLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/widget/h$a;,
        Lcom/bytedance/sdk/openadsdk/core/widget/h$b;
    }
.end annotation


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/content/Context;

.field private d:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;

.field private e:Lcom/bytedance/sdk/openadsdk/core/widget/h$b;

.field private f:Z

.field private g:Lcom/bytedance/sdk/openadsdk/core/d/u;

.field private h:Landroid/view/ViewStub;

.field private i:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 47
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->f:Z

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/view/View;Z)V
    .locals 1

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 64
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->h:Landroid/view/ViewStub;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    goto :goto_1

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    const-string v0, "tt_video_traffic_tip_layout"

    .line 68
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    const-string v0, "tt_video_traffic_tip_tv"

    .line 69
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->b:Landroid/widget/TextView;

    const-string v0, "tt_video_traffic_continue_play_btn"

    .line 70
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p3, :cond_1

    const/4 p2, 0x1

    .line 72
    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    .line 73
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/widget/h$1;

    invoke-direct {p2, p0}, Lcom/bytedance/sdk/openadsdk/core/widget/h$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/widget/h;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 85
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p2, 0x0

    .line 86
    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    :goto_0
    return-void

    :cond_2
    :goto_1
    return-void
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/d/u;Z)V
    .locals 4

    if-eqz p1, :cond_5

    .line 172
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->e:Lcom/bytedance/sdk/openadsdk/core/widget/h$b;

    if-eqz v0, :cond_2

    .line 179
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->e:Lcom/bytedance/sdk/openadsdk/core/widget/h$b;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/widget/h$b;->j()V

    .line 181
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/u;->d()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v0, v0, v2

    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    div-double/2addr v0, v2

    .line 182
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int p1, v0

    if-eqz p2, :cond_3

    .line 185
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    const-string v1, "tt_video_without_wifi_tips"

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/ad;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    const-string v0, "tt_video_bytesize_MB"

    .line 186
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    const-string v0, "tt_video_bytesize"

    .line 187
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 189
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    const-string v0, "tt_video_without_wifi_tips"

    invoke-static {p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    const-string v0, "tt_video_bytesize"

    .line 190
    invoke-static {p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 193
    :goto_0
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/al;->a(Landroid/view/View;I)V

    .line 194
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->b:Landroid/widget/TextView;

    invoke-static {p2, p1}, Lcom/bytedance/sdk/openadsdk/utils/al;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 196
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/al;->d(Landroid/view/View;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    if-eqz p1, :cond_4

    .line 197
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    :cond_4
    return-void

    :cond_5
    :goto_1
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/widget/h;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c()V

    return-void
.end method

.method private a(I)Z
    .locals 3

    .line 129
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a()Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    return v0

    .line 132
    :cond_0
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->f:Z

    if-nez p1, :cond_3

    .line 133
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->d:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->e:Lcom/bytedance/sdk/openadsdk/core/widget/h$b;

    if-eqz p1, :cond_2

    .line 134
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->e:Lcom/bytedance/sdk/openadsdk/core/widget/h$b;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/h$b;->h()Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    .line 135
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->d:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;

    invoke-interface {p1, v1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;->e(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V

    .line 137
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->d:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;

    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/widget/h$a;->a:Lcom/bytedance/sdk/openadsdk/core/widget/h$a;

    invoke-interface {p1, v2, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;->a(Lcom/bytedance/sdk/openadsdk/core/widget/h$a;Ljava/lang/String;)V

    .line 139
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->g:Lcom/bytedance/sdk/openadsdk/core/d/u;

    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a(Lcom/bytedance/sdk/openadsdk/core/d/u;Z)V

    const/4 p1, 0x0

    return p1

    :cond_3
    return v0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/widget/h;)Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->d:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;

    return-object p0
.end method

.method private b()V
    .locals 1

    const/4 v0, 0x0

    .line 96
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->g:Lcom/bytedance/sdk/openadsdk/core/d/u;

    return-void
.end method

.method private c()V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    .line 149
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/widget/h;->d()V

    return-void
.end method

.method private d()V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/view/View;)V
    .locals 3

    if-eqz p1, :cond_0

    .line 55
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 56
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->i:Landroid/view/View;

    .line 57
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    .line 58
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const-string v1, "tt_video_traffic_tip"

    invoke-static {p1, v1}, Lcom/bytedance/sdk/openadsdk/utils/ad;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    check-cast p2, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const-string v0, "tt_video_traffic_tip_layout_viewStub"

    .line 59
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewStub;

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->h:Landroid/view/ViewStub;

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;Lcom/bytedance/sdk/openadsdk/core/widget/h$b;)V
    .locals 0

    .line 91
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->e:Lcom/bytedance/sdk/openadsdk/core/widget/h$b;

    .line 92
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->d:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;

    return-void
.end method

.method public a(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/widget/h;->b()V

    .line 157
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/widget/h;->d()V

    return-void
.end method

.method public a()Z
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public a(ILcom/bytedance/sdk/openadsdk/core/d/u;Z)Z
    .locals 3

    .line 107
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->i:Landroid/view/View;

    invoke-direct {p0, v0, v2, p3}, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a(Landroid/content/Context;Landroid/view/View;Z)V

    .line 111
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/widget/h;->g:Lcom/bytedance/sdk/openadsdk/core/d/u;

    packed-switch p1, :pswitch_data_0

    return v1

    .line 116
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/widget/h;->a(I)Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
