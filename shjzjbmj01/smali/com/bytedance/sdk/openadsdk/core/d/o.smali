.class public Lcom/bytedance/sdk/openadsdk/core/d/o;
.super Ljava/lang/Object;
.source "RenderInfo.java"


# instance fields
.field private a:I

.field private b:Z

.field private c:D

.field private d:D

.field private e:D

.field private f:D

.field private g:D

.field private h:D

.field private i:Ljava/lang/String;

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->a:I

    return v0
.end method

.method public a(D)V
    .locals 0

    .line 45
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->c:D

    return-void
.end method

.method public a(I)V
    .locals 0

    .line 29
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->a:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->i:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 37
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->b:Z

    return-void
.end method

.method public b(D)V
    .locals 0

    .line 53
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->d:D

    return-void
.end method

.method public b(I)V
    .locals 0

    .line 101
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->j:I

    return-void
.end method

.method public b()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->b:Z

    return v0
.end method

.method public c()D
    .locals 2

    .line 41
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->c:D

    return-wide v0
.end method

.method public c(D)V
    .locals 0

    .line 61
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->e:D

    return-void
.end method

.method public d()D
    .locals 2

    .line 49
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->d:D

    return-wide v0
.end method

.method public d(D)V
    .locals 0

    .line 69
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->f:D

    return-void
.end method

.method public e()D
    .locals 2

    .line 57
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->e:D

    return-wide v0
.end method

.method public e(D)V
    .locals 0

    .line 77
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->g:D

    return-void
.end method

.method public f()D
    .locals 2

    .line 65
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->f:D

    return-wide v0
.end method

.method public f(D)V
    .locals 0

    .line 85
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->h:D

    return-void
.end method

.method public g()D
    .locals 2

    .line 73
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->g:D

    return-wide v0
.end method

.method public h()D
    .locals 2

    .line 81
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->h:D

    return-wide v0
.end method

.method public i()I
    .locals 1

    .line 97
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/o;->j:I

    return v0
.end method
