.class public Lcom/bytedance/sdk/openadsdk/core/d/j$a;
.super Ljava/lang/Object;
.source "DynamicClickInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/core/d/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:J

.field private b:J

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->f:I

    return p0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->e:I

    return p0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->d:I

    return p0
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->c:I

    return p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)J
    .locals 2

    .line 34
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->b:J

    return-wide v0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)J
    .locals 2

    .line 34
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->a:J

    return-wide v0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->g:I

    return p0
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->h:I

    return p0
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->i:I

    return p0
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I
    .locals 0

    .line 34
    iget p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->j:I

    return p0
.end method


# virtual methods
.method public a(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 57
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->c:I

    return-object p0
.end method

.method public a(J)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 47
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->a:J

    return-object p0
.end method

.method public a()Lcom/bytedance/sdk/openadsdk/core/d/j;
    .locals 2

    .line 98
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/d/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/j;-><init>(Lcom/bytedance/sdk/openadsdk/core/d/j$a;Lcom/bytedance/sdk/openadsdk/core/d/j$1;)V

    return-object v0
.end method

.method public b(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 62
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->d:I

    return-object p0
.end method

.method public b(J)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 52
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->b:J

    return-object p0
.end method

.method public c(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 67
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->e:I

    return-object p0
.end method

.method public d(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 72
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->f:I

    return-object p0
.end method

.method public e(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 77
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->g:I

    return-object p0
.end method

.method public f(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 82
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->h:I

    return-object p0
.end method

.method public g(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 87
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->i:I

    return-object p0
.end method

.method public h(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    .locals 0

    .line 92
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->j:I

    return-object p0
.end method
