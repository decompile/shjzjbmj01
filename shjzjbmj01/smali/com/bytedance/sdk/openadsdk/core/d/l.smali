.class public Lcom/bytedance/sdk/openadsdk/core/d/l;
.super Ljava/lang/Object;
.source "MaterialMeta.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/d/l$a;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Z

.field private C:Lcom/bytedance/sdk/openadsdk/core/d/h;

.field private D:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private E:Lcom/bytedance/sdk/openadsdk/core/d/l$a;

.field private F:Lcom/bytedance/sdk/openadsdk/core/d/l$a;

.field private G:Z

.field private H:I

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:I

.field private M:I

.field private N:Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

.field private O:I

.field private P:I

.field private Q:Lcom/bytedance/sdk/openadsdk/core/d/e;

.field private R:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field private S:I

.field private T:I

.field private U:Ljava/lang/String;

.field private V:I

.field private W:I

.field private X:I

.field private Y:Lorg/json/JSONObject;

.field private Z:I

.field public a:Ljava/lang/String;

.field private aa:I

.field private ab:I

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:Lcom/bytedance/sdk/openadsdk/core/d/n;

.field private ag:Lcom/bytedance/sdk/openadsdk/core/d/c;

.field private ah:Ljava/lang/String;

.field private b:I

.field private c:Lcom/bytedance/sdk/openadsdk/core/d/k;

.field private d:Lcom/bytedance/sdk/openadsdk/core/d/k;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/d/k;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/String;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Lcom/bytedance/sdk/openadsdk/core/d/b;

.field private q:Lcom/bytedance/sdk/openadsdk/core/d/g;

.field private r:I

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/FilterWord;",
            ">;"
        }
    .end annotation
.end field

.field private v:I

.field private w:J

.field private x:I

.field private y:Lcom/bytedance/sdk/openadsdk/core/d/u;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 42
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->f:I

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->h:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->j:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->k:Ljava/util/List;

    const-string v0, "0"

    .line 51
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->o:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->u:Ljava/util/List;

    const/4 v0, 0x0

    .line 59
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->v:I

    const-string v1, ""

    .line 94
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->J:Ljava/lang/String;

    const-string v1, ""

    .line 96
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->K:Ljava/lang/String;

    .line 97
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->L:I

    const/4 v1, 0x2

    .line 99
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->M:I

    const/16 v1, -0xc8

    .line 103
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->O:I

    .line 106
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->P:I

    .line 107
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/d/e;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/core/d/e;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->Q:Lcom/bytedance/sdk/openadsdk/core/d/e;

    const/4 v1, 0x1

    .line 113
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->T:I

    .line 117
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->V:I

    .line 119
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->W:I

    .line 121
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->X:I

    .line 125
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->Z:I

    .line 130
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ab:I

    .line 132
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ac:I

    .line 140
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ad:I

    .line 142
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ae:I

    return-void
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 1

    if-eqz p0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->a()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static a(Lcom/bytedance/sdk/openadsdk/core/d/l;ZZ)Z
    .locals 1

    if-eqz p0, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->a()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    const-string p0, "MaterialMeta"

    const-string p1, "can show end card follow js"

    .line 1175
    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return p2

    :cond_0
    const-string p0, "MaterialMeta"

    const-string p2, "can show end card follow js WebViewClient"

    .line 1178
    invoke-static {p0, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return p1
.end method

.method private b(Lcom/bytedance/sdk/openadsdk/FilterWord;)Lorg/json/JSONObject;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 1100
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/FilterWord;->isValid()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1101
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "id"

    .line 1102
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/FilterWord;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "name"

    .line 1103
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/FilterWord;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "is_selected"

    .line 1104
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/FilterWord;->getIsSelected()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1106
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/FilterWord;->hasSecondOptions()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1107
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 1108
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/FilterWord;->getOptions()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/FilterWord;

    .line 1109
    invoke-direct {p0, v3}, Lcom/bytedance/sdk/openadsdk/core/d/l;->b(Lcom/bytedance/sdk/openadsdk/FilterWord;)Lorg/json/JSONObject;

    move-result-object v3

    .line 1110
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 1113
    :cond_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result p1

    if-lez p1, :cond_2

    const-string p1, "options"

    .line 1114
    invoke-virtual {v1, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-object v1

    :catch_0
    :cond_3
    return-object v0
.end method

.method public static b(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 0

    if-eqz p0, :cond_0

    .line 359
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->C()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static c(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 1130
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ak()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    .line 1131
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ak()I

    move-result p0

    const/16 v1, 0xf

    if-ne p0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public static d(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 1

    if-eqz p0, :cond_0

    .line 1135
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->i()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static e(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 1

    if-eqz p0, :cond_0

    .line 1142
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->i()I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static f(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 4

    const/4 v0, 0x0

    .line 1150
    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->at()Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "parent_type"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    move-exception p0

    const-string v1, "MaterialMeta"

    .line 1152
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isLuBanLandingPage error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public static g(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    .line 1188
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    if-nez v1, :cond_0

    goto :goto_0

    .line 1191
    :cond_0
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->a()I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    :goto_0
    return v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .line 342
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->n()I

    move-result v0

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public B()Z
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->o()I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public C()Z
    .locals 1

    .line 355
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->G:Z

    return v0
.end method

.method public D()Z
    .locals 2

    .line 371
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->C()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->i()I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public E()Z
    .locals 2

    .line 384
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->F()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public F()I
    .locals 1

    .line 388
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->a()I

    move-result v0

    :goto_0
    return v0
.end method

.method public G()I
    .locals 1

    .line 392
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->c()I

    move-result v0

    :goto_0
    return v0
.end method

.method public H()I
    .locals 1

    .line 396
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->d()I

    move-result v0

    :goto_0
    return v0
.end method

.method public I()I
    .locals 1

    .line 401
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->k()I

    move-result v0

    :goto_0
    return v0
.end method

.method public J()I
    .locals 1

    .line 407
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->f()I

    move-result v0

    :goto_0
    return v0
.end method

.method public K()I
    .locals 1

    .line 411
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->g()I

    move-result v0

    :goto_0
    return v0
.end method

.method public L()I
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->h()I

    move-result v0

    :goto_0
    return v0
.end method

.method public M()Lcom/bytedance/sdk/openadsdk/core/d/l$a;
    .locals 1

    .line 419
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->E:Lcom/bytedance/sdk/openadsdk/core/d/l$a;

    return-object v0
.end method

.method public N()Lcom/bytedance/sdk/openadsdk/core/d/l$a;
    .locals 1

    .line 430
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->F:Lcom/bytedance/sdk/openadsdk/core/d/l$a;

    return-object v0
.end method

.method public O()Lcom/bytedance/sdk/openadsdk/core/d/h;
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    return-object v0
.end method

.method public P()Lcom/bytedance/sdk/openadsdk/core/d/c;
    .locals 1

    .line 449
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag:Lcom/bytedance/sdk/openadsdk/core/d/c;

    return-object v0
.end method

.method public Q()Ljava/lang/String;
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ah:Ljava/lang/String;

    return-object v0
.end method

.method public R()Lcom/bytedance/sdk/openadsdk/core/d/u;
    .locals 1

    .line 465
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    return-object v0
.end method

.method public S()Ljava/lang/String;
    .locals 1

    .line 477
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->s:Ljava/lang/String;

    return-object v0
.end method

.method public T()I
    .locals 1

    .line 481
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->b:I

    return v0
.end method

.method public U()Lcom/bytedance/sdk/openadsdk/core/d/k;
    .locals 1

    .line 489
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->c:Lcom/bytedance/sdk/openadsdk/core/d/k;

    return-object v0
.end method

.method public V()Lcom/bytedance/sdk/openadsdk/core/d/k;
    .locals 1

    .line 497
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->d:Lcom/bytedance/sdk/openadsdk/core/d/k;

    return-object v0
.end method

.method public W()Ljava/lang/String;
    .locals 1

    .line 505
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->e:Ljava/lang/String;

    return-object v0
.end method

.method public X()I
    .locals 1

    .line 513
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->f:I

    return v0
.end method

.method public Y()Ljava/lang/String;
    .locals 1

    .line 521
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->g:Ljava/lang/String;

    return-object v0
.end method

.method public Z()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/d/k;",
            ">;"
        }
    .end annotation

    .line 529
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->h:Ljava/util/List;

    return-object v0
.end method

.method public a()I
    .locals 1

    .line 153
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ad:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .line 157
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ad:I

    return-void
.end method

.method public a(J)V
    .locals 0

    .line 653
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->w:J

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->R:Lcom/bytedance/sdk/openadsdk/AdSlot;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/FilterWord;)V
    .locals 1

    .line 629
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;)V
    .locals 0

    .line 633
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->N:Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/b;)V
    .locals 0

    .line 597
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->p:Lcom/bytedance/sdk/openadsdk/core/d/b;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/c;)V
    .locals 0

    .line 453
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag:Lcom/bytedance/sdk/openadsdk/core/d/c;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/e;)V
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->Q:Lcom/bytedance/sdk/openadsdk/core/d/e;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/g;)V
    .locals 0

    .line 605
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->q:Lcom/bytedance/sdk/openadsdk/core/d/g;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/h;)V
    .locals 0

    .line 445
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/k;)V
    .locals 0

    .line 493
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->c:Lcom/bytedance/sdk/openadsdk/core/d/k;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/l$a;)V
    .locals 0

    .line 423
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->E:Lcom/bytedance/sdk/openadsdk/core/d/l$a;

    .line 425
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;->a()Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;->b()V

    .line 426
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/a;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/u;)V
    .locals 0

    .line 469
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->y:Lcom/bytedance/sdk/openadsdk/core/d/u;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->K:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 693
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->D:Ljava/util/Map;

    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    .line 212
    :cond_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/n;->a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/d/n;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->af:Lcom/bytedance/sdk/openadsdk/core/d/n;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 375
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->G:Z

    return-void
.end method

.method public aA()I
    .locals 1

    .line 746
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ae:I

    return v0
.end method

.method public aB()Z
    .locals 3

    .line 772
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 775
    :cond_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->r:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    return v1

    .line 778
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/openadsdk/core/d/k;

    .line 779
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/k;->e()Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method public aC()I
    .locals 1

    .line 787
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->b()I

    move-result v0

    :goto_0
    return v0
.end method

.method public aD()Z
    .locals 2

    .line 797
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aE()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public aE()I
    .locals 1

    .line 801
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->v:I

    return v0
.end method

.method public aF()Lorg/json/JSONObject;
    .locals 7

    .line 829
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "interaction_type"

    .line 831
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->T()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "target_url"

    .line 832
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->W()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "use_media_video_player"

    .line 833
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "landing_scroll_percentage"

    .line 834
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->X()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "gecko_id"

    .line 835
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->Y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "ad_id"

    .line 836
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "source"

    .line 837
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "package_name"

    .line 838
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->az()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "screenshot"

    .line 839
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ap()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "dislike_control"

    .line 840
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aE()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "play_bar_show_time"

    .line 841
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->w()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "is_playable"

    .line 842
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->C()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "playable"

    .line 843
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->g()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "playable_type"

    .line 844
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->i()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "playable_style"

    .line 845
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "play_bar_style"

    .line 846
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->n()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "if_block_lp"

    .line 847
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "cache_sort"

    .line 848
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->x()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "if_sp_cache"

    .line 849
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->z()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "render_control"

    .line 850
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 852
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "reward_name"

    .line 853
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "reward_amount"

    .line 854
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->c()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "reward_data"

    .line 855
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 857
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->U()Lcom/bytedance/sdk/openadsdk/core/d/k;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 858
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 859
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "url"

    .line 860
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/k;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "height"

    .line 861
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/k;->c()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v3, "width"

    .line 862
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/k;->b()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "icon"

    .line 864
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 868
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->V()Lcom/bytedance/sdk/openadsdk/core/d/k;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 869
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/k;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 870
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "url"

    .line 871
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/k;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "height"

    .line 872
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/k;->c()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v3, "width"

    .line 873
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/k;->b()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "cover_image"

    .line 875
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 878
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->at()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "session_params"

    .line 880
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 884
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->o()Lcom/bytedance/sdk/openadsdk/core/d/e;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 886
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "click_upper_content_area"

    .line 887
    iget-boolean v4, v1, Lcom/bytedance/sdk/openadsdk/core/d/e;->a:Z

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v3, "click_upper_non_content_area"

    .line 888
    iget-boolean v4, v1, Lcom/bytedance/sdk/openadsdk/core/d/e;->b:Z

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v3, "click_lower_content_area"

    .line 889
    iget-boolean v4, v1, Lcom/bytedance/sdk/openadsdk/core/d/e;->c:Z

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v3, "click_lower_non_content_area"

    .line 890
    iget-boolean v4, v1, Lcom/bytedance/sdk/openadsdk/core/d/e;->d:Z

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v3, "click_button_area"

    .line 891
    iget-boolean v4, v1, Lcom/bytedance/sdk/openadsdk/core/d/e;->e:Z

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v3, "click_video_area"

    .line 892
    iget-boolean v1, v1, Lcom/bytedance/sdk/openadsdk/core/d/e;->f:Z

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "click_area"

    .line 893
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 897
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->p()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 899
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->toJsonObj()Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "adslot"

    .line 900
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 905
    :cond_4
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->Z()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 907
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 908
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/core/d/k;

    .line 909
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "url"

    .line 910
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/d/k;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "height"

    .line 911
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/d/k;->c()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v5, "width"

    .line 912
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/d/k;->b()I

    move-result v3

    invoke-virtual {v4, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 913
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :cond_5
    const-string v1, "image"

    .line 916
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 920
    :cond_6
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ab()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 922
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 923
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 924
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    :cond_7
    const-string v1, "show_url"

    .line 927
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 931
    :cond_8
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ac()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 933
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 934
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 935
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_2

    :cond_9
    const-string v1, "click_url"

    .line 938
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_a
    const-string v1, "phone_num"

    .line 941
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "title"

    .line 942
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ad()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "description"

    .line 943
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ae()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "ext"

    .line 944
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "image_mode"

    .line 945
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ak()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "is_playable"

    .line 946
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->C()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "intercept_flag"

    .line 947
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->v()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "button_text"

    .line 948
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->af()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "ad_logo"

    .line 949
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->m()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "video_adaptation"

    .line 950
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "feed_video_opentype"

    .line 951
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->k()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 953
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ah()Lcom/bytedance/sdk/openadsdk/core/d/b;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 955
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "app_name"

    .line 956
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "package_name"

    .line 957
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "download_url"

    .line 958
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "score"

    .line 959
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->e()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v3, "comment_num"

    .line 960
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->f()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v3, "quick_app_url"

    .line 961
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "app_size"

    .line 962
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->g()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "app"

    .line 964
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 968
    :cond_b
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ai()Lcom/bytedance/sdk/openadsdk/core/d/g;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 970
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "deeplink_url"

    .line 971
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "fallback_url"

    .line 972
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/g;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "fallback_type"

    .line 973
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/g;->c()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "deep_link"

    .line 975
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 979
    :cond_c
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->al()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 981
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 982
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_d
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/FilterWord;

    .line 983
    invoke-direct {p0, v3}, Lcom/bytedance/sdk/openadsdk/core/d/l;->b(Lcom/bytedance/sdk/openadsdk/FilterWord;)Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 985
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_3

    :cond_e
    const-string v1, "filter_words"

    .line 989
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 993
    :cond_f
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->am()Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

    move-result-object v1

    if-eqz v1, :cond_10

    const-string v2, "personalization_prompts"

    .line 995
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;->toJson()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_10
    const-string v1, "count_down"

    .line 998
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ao()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "expiration_time"

    .line 999
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->an()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1002
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v1

    if-eqz v1, :cond_11

    const-string v2, "video"

    .line 1004
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/u;->m()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1008
    :cond_11
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "auto_open"

    .line 1009
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->G()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "download_mode"

    .line 1010
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->H()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "download_type"

    .line 1011
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aC()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "auto_control"

    .line 1012
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->r()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "auto_control_choose"

    .line 1013
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->t()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "auto_control_time"

    .line 1014
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->u()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "if_suspend_download"

    .line 1015
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->F()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "if_send_click"

    .line 1017
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->I()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "dl_popup"

    .line 1020
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->J()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "market_popup"

    .line 1021
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->K()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "if_pop_lp"

    .line 1022
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->L()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "download_conf"

    .line 1023
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "if_both_open"

    .line 1024
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->av()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "if_double_deeplink"

    .line 1025
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ay()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1027
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->as()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 1028
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1029
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->as()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 1031
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_12

    .line 1032
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1033
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    .line 1034
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1035
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_4

    :cond_12
    const-string v2, "media_ext"

    .line 1038
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_13
    const-string v1, "landing_page_type"

    .line 1041
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aA()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1043
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->M()Lcom/bytedance/sdk/openadsdk/core/d/l$a;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 1045
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "id"

    .line 1046
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "md5"

    .line 1047
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "url"

    .line 1048
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "data"

    .line 1049
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "diff_data"

    .line 1050
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "version"

    .line 1051
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "dynamic_creative"

    .line 1052
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "tpl_info"

    .line 1054
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1057
    :cond_14
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->N()Lcom/bytedance/sdk/openadsdk/core/d/l$a;

    move-result-object v1

    if-eqz v1, :cond_15

    .line 1059
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "middle_id"

    .line 1060
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "middle_md5"

    .line 1061
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "middle_url"

    .line 1062
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "middle_data"

    .line 1063
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "middle_diff_data"

    .line 1064
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "middle_version"

    .line 1065
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "middle_dynamic_creative"

    .line 1066
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l$a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "middle_tpl_info"

    .line 1067
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_15
    const-string v1, "market_url"

    .line 1071
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->au()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1074
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->P()Lcom/bytedance/sdk/openadsdk/core/d/c;

    move-result-object v1

    if-eqz v1, :cond_16

    .line 1076
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "developer_name"

    .line 1077
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "app_version"

    .line 1078
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/c;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "permissions"

    .line 1079
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/c;->e()Lorg/json/JSONArray;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "privacy_policy_url"

    .line 1080
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/c;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "package_name"

    .line 1081
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/c;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "app_name"

    .line 1082
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/c;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "app_manage"

    .line 1083
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_16
    return-object v0
.end method

.method public aa()Ljava/lang/String;
    .locals 1

    .line 537
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->i:Ljava/lang/String;

    return-object v0
.end method

.method public ab()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 545
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->j:Ljava/util/List;

    return-object v0
.end method

.method public ac()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 553
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->k:Ljava/util/List;

    return-object v0
.end method

.method public ad()Ljava/lang/String;
    .locals 1

    .line 561
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->l:Ljava/lang/String;

    return-object v0
.end method

.method public ae()Ljava/lang/String;
    .locals 1

    .line 569
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->m:Ljava/lang/String;

    return-object v0
.end method

.method public af()Ljava/lang/String;
    .locals 1

    .line 577
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->n:Ljava/lang/String;

    return-object v0
.end method

.method public ag()Ljava/lang/String;
    .locals 1

    .line 585
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->o:Ljava/lang/String;

    return-object v0
.end method

.method public ah()Lcom/bytedance/sdk/openadsdk/core/d/b;
    .locals 1

    .line 593
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->p:Lcom/bytedance/sdk/openadsdk/core/d/b;

    return-object v0
.end method

.method public ai()Lcom/bytedance/sdk/openadsdk/core/d/g;
    .locals 1

    .line 601
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->q:Lcom/bytedance/sdk/openadsdk/core/d/g;

    return-object v0
.end method

.method public aj()Ljava/lang/String;
    .locals 1

    .line 609
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->t:Ljava/lang/String;

    return-object v0
.end method

.method public ak()I
    .locals 1

    .line 617
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->r:I

    return v0
.end method

.method public al()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/FilterWord;",
            ">;"
        }
    .end annotation

    .line 625
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->u:Ljava/util/List;

    return-object v0
.end method

.method public am()Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;
    .locals 1

    .line 637
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->N:Lcom/bytedance/sdk/openadsdk/PersonalizationPrompt;

    return-object v0
.end method

.method public an()J
    .locals 2

    .line 649
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->w:J

    return-wide v0
.end method

.method public ao()I
    .locals 1

    .line 657
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->x:I

    return v0
.end method

.method public ap()Z
    .locals 1

    .line 665
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->z:Z

    return v0
.end method

.method public aq()Z
    .locals 1

    .line 673
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->A:Z

    return v0
.end method

.method public ar()Z
    .locals 1

    .line 681
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->B:Z

    return v0
.end method

.method public as()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 689
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->D:Ljava/util/Map;

    return-object v0
.end method

.method public at()Lorg/json/JSONObject;
    .locals 1

    .line 697
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->Y:Lorg/json/JSONObject;

    return-object v0
.end method

.method public au()Ljava/lang/String;
    .locals 1

    .line 705
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->U:Ljava/lang/String;

    return-object v0
.end method

.method public av()I
    .locals 1

    .line 717
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ab:I

    return v0
.end method

.method public aw()Z
    .locals 2

    .line 721
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->av()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public ax()Z
    .locals 2

    .line 725
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ay()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public ay()I
    .locals 1

    .line 733
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ac:I

    return v0
.end method

.method public az()Ljava/lang/String;
    .locals 1

    .line 738
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->K:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .line 164
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->L:I

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/d/k;)V
    .locals 0

    .line 501
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->d:Lcom/bytedance/sdk/openadsdk/core/d/k;

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/d/l$a;)V
    .locals 0

    .line 434
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->F:Lcom/bytedance/sdk/openadsdk/core/d/l$a;

    .line 436
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;->a()Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/d;->b()V

    .line 437
    invoke-static {p0}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/a;->b(Lcom/bytedance/sdk/openadsdk/core/d/l;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->I:Ljava/lang/String;

    return-void
.end method

.method public b(Lorg/json/JSONObject;)V
    .locals 0

    .line 701
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->Y:Lorg/json/JSONObject;

    return-void
.end method

.method public b(Z)V
    .locals 0

    .line 669
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->z:Z

    return-void
.end method

.method public c()I
    .locals 1

    .line 166
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->L:I

    return v0
.end method

.method public c(I)V
    .locals 0

    .line 168
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->M:I

    return-void
.end method

.method public c(Lcom/bytedance/sdk/openadsdk/core/d/k;)V
    .locals 1

    .line 533
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .line 323
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->J:Ljava/lang/String;

    return-void
.end method

.method public c(Lorg/json/JSONObject;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x4

    .line 817
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->b:I

    const-string v0, "id"

    .line 819
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->o:Ljava/lang/String;

    const-string v0, "source"

    .line 820
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->s:Ljava/lang/String;

    .line 821
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/d/b;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/d/b;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->p:Lcom/bytedance/sdk/openadsdk/core/d/b;

    .line 822
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->p:Lcom/bytedance/sdk/openadsdk/core/d/b;

    const-string v1, "pkg_name"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->d(Ljava/lang/String;)V

    .line 823
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->p:Lcom/bytedance/sdk/openadsdk/core/d/b;

    const-string v1, "name"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->c(Ljava/lang/String;)V

    .line 824
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->p:Lcom/bytedance/sdk/openadsdk/core/d/b;

    const-string v1, "download_url"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/d/b;->b(Ljava/lang/String;)V

    return-void
.end method

.method public c(Z)V
    .locals 0

    .line 677
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->A:Z

    return-void
.end method

.method public d()I
    .locals 1

    .line 170
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->M:I

    return v0
.end method

.method public d(I)V
    .locals 0

    .line 185
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->V:I

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .line 461
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ah:Ljava/lang/String;

    return-void
.end method

.method public d(Z)V
    .locals 0

    .line 685
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->B:Z

    return-void
.end method

.method public e()I
    .locals 1

    .line 181
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->V:I

    return v0
.end method

.method public e(I)V
    .locals 0

    .line 220
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->H:I

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .line 473
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->s:Ljava/lang/String;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 756
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 758
    :cond_1
    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 760
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/bytedance/sdk/openadsdk/core/d/l;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->t:Ljava/lang/String;

    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/d/l;->t:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public f(I)V
    .locals 0

    .line 236
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->X:I

    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    .line 509
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->e:Ljava/lang/String;

    return-void
.end method

.method public f()Z
    .locals 2

    .line 194
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->V:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public g()Lorg/json/JSONObject;
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->af:Lcom/bytedance/sdk/openadsdk/core/d/n;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->af:Lcom/bytedance/sdk/openadsdk/core/d/n;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/n;->b()Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public g(I)V
    .locals 0

    .line 244
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->W:I

    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .line 525
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->g:Ljava/lang/String;

    return-void
.end method

.method public h()Lcom/bytedance/sdk/openadsdk/core/d/n;
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->af:Lcom/bytedance/sdk/openadsdk/core/d/n;

    return-object v0
.end method

.method public h(I)V
    .locals 0

    .line 252
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->T:I

    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    .line 541
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->i:Ljava/lang/String;

    return-void
.end method

.method public hashCode()I
    .locals 2

    .line 766
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 767
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->t:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public i()I
    .locals 1

    .line 216
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->H:I

    return v0
.end method

.method public i(I)V
    .locals 0

    .line 260
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->P:I

    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    .line 565
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->l:Ljava/lang/String;

    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->I:Ljava/lang/String;

    return-object v0
.end method

.method public j(I)V
    .locals 0

    .line 304
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->S:I

    return-void
.end method

.method public j(Ljava/lang/String;)V
    .locals 0

    .line 573
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->m:Ljava/lang/String;

    return-void
.end method

.method public k()I
    .locals 1

    .line 232
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->X:I

    return v0
.end method

.method public k(I)V
    .locals 0

    .line 312
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->O:I

    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 0

    .line 581
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->n:Ljava/lang/String;

    return-void
.end method

.method public l()I
    .locals 1

    .line 240
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->W:I

    return v0
.end method

.method public l(I)V
    .locals 0

    .line 320
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->Z:I

    return-void
.end method

.method public l(Ljava/lang/String;)V
    .locals 0

    .line 589
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->o:Ljava/lang/String;

    return-void
.end method

.method public m()I
    .locals 1

    .line 248
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->T:I

    return v0
.end method

.method public m(I)V
    .locals 0

    .line 332
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->aa:I

    return-void
.end method

.method public m(Ljava/lang/String;)V
    .locals 0

    .line 613
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->t:Ljava/lang/String;

    return-void
.end method

.method public n()I
    .locals 1

    .line 256
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->P:I

    return v0
.end method

.method public n(I)V
    .locals 0

    .line 485
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->b:I

    return-void
.end method

.method public n(Ljava/lang/String;)V
    .locals 0

    .line 709
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->U:Ljava/lang/String;

    return-void
.end method

.method public o()Lcom/bytedance/sdk/openadsdk/core/d/e;
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->Q:Lcom/bytedance/sdk/openadsdk/core/d/e;

    return-object v0
.end method

.method public o(I)V
    .locals 0

    .line 517
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->f:I

    return-void
.end method

.method public o(Ljava/lang/String;)V
    .locals 0

    .line 742
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->a:Ljava/lang/String;

    return-void
.end method

.method public p()Lcom/bytedance/sdk/openadsdk/AdSlot;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->R:Lcom/bytedance/sdk/openadsdk/AdSlot;

    return-object v0
.end method

.method public p(I)V
    .locals 0

    .line 621
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->r:I

    return-void
.end method

.method public q(I)V
    .locals 0

    .line 661
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->x:I

    return-void
.end method

.method public q()Z
    .locals 2

    .line 280
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->r()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public r()I
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->e()I

    move-result v0

    :goto_0
    return v0
.end method

.method public r(I)V
    .locals 0

    .line 713
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ab:I

    return-void
.end method

.method public s(I)V
    .locals 0

    .line 729
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ac:I

    return-void
.end method

.method public s()Z
    .locals 2

    .line 288
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->t()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public t()I
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->i()I

    move-result v0

    :goto_0
    return v0
.end method

.method public t(I)V
    .locals 0

    .line 750
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->ae:I

    return-void
.end method

.method public u()I
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    if-nez v0, :cond_0

    const/16 v0, 0x12c

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->C:Lcom/bytedance/sdk/openadsdk/core/d/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/h;->j()I

    move-result v0

    :goto_0
    return v0
.end method

.method public u(I)V
    .locals 0

    .line 805
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->v:I

    return-void
.end method

.method public v()I
    .locals 1

    .line 300
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->S:I

    return v0
.end method

.method public w()I
    .locals 1

    .line 308
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->O:I

    return v0
.end method

.method public x()I
    .locals 1

    .line 316
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->Z:I

    return v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    .line 325
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->J:Ljava/lang/String;

    return-object v0
.end method

.method public z()I
    .locals 1

    .line 328
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/l;->aa:I

    return v0
.end method
