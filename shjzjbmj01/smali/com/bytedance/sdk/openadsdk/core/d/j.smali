.class public Lcom/bytedance/sdk/openadsdk/core/d/j;
.super Ljava/lang/Object;
.source "DynamicClickInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/d/j$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:J

.field public final f:J

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I


# direct methods
.method private constructor <init>(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)V
    .locals 2
    .param p1    # Lcom/bytedance/sdk/openadsdk/core/d/j$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->a(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->a:I

    .line 23
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->b(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->b:I

    .line 24
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->c(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->c:I

    .line 25
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->d(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->d:I

    .line 26
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->e(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->e:J

    .line 27
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->f(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->f:J

    .line 28
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->g(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->g:I

    .line 29
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->h(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->h:I

    .line 30
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->i(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->i:I

    .line 31
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->j(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/d/j;->j:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/sdk/openadsdk/core/d/j$a;Lcom/bytedance/sdk/openadsdk/core/d/j$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/d/j;-><init>(Lcom/bytedance/sdk/openadsdk/core/d/j$a;)V

    return-void
.end method
