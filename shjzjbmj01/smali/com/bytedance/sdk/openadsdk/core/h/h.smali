.class public Lcom/bytedance/sdk/openadsdk/core/h/h;
.super Ljava/lang/Object;
.source "TTSdkSettings.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/h/c;


# instance fields
.field private A:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private C:I

.field private D:I

.field private E:J

.field private F:I

.field private G:I

.field private H:I

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:I

.field private L:I

.field private M:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ljava/lang/String;

.field private O:I

.field private P:I

.field private volatile Q:Z

.field private final R:Lcom/bytedance/sdk/openadsdk/utils/af;

.field private S:I

.field private T:I

.field private a:I

.field private b:I

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/core/h/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/core/h/i;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/h/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Lorg/json/JSONObject;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:I

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:I

.field private x:I

.field private y:I

.field private z:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    .line 197
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    .line 201
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    .line 203
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    .line 204
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->d:Ljava/util/Map;

    .line 206
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->f:Ljava/util/Set;

    .line 208
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->h:Ljava/util/Set;

    .line 209
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->i:Ljava/util/List;

    .line 236
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    .line 237
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    .line 238
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    .line 239
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    const/4 v1, 0x0

    .line 240
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->o:Lorg/json/JSONObject;

    const-string v2, ""

    .line 241
    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    .line 245
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    .line 246
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    .line 249
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    .line 250
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    .line 251
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    const-wide/16 v2, 0x0

    .line 252
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->z:J

    .line 253
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    .line 254
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    .line 255
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->C:I

    .line 256
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    const-wide/32 v2, 0x7fffffff

    .line 258
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    .line 259
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    .line 261
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    .line 262
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->H:I

    .line 263
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    .line 264
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    .line 265
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    .line 266
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    .line 267
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    .line 272
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    .line 277
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    .line 279
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    const/4 v1, 0x0

    .line 281
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->Q:Z

    .line 285
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    .line 286
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    const-string v0, "tt_sdk_settings"

    .line 290
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/utils/af;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    return-void
.end method

.method private a(Z)I
    .locals 0

    if-eqz p1, :cond_0

    const/16 p1, 0x14

    return p1

    :cond_0
    const/4 p1, 0x5

    return p1
.end method

.method public static a(Ljava/util/Set;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    .line 1838
    :try_start_0
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    return-object p0

    .line 1840
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1841
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1842
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1843
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_2
    return-object v0

    .line 1848
    :catch_0
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 890
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "tt_sdk_settings"

    const-string v1, "url_ads"

    .line 892
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "url_alog"

    .line 893
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "xpath"

    .line 894
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->j:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "duration"

    .line 895
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "max"

    .line 896
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "download_config_dl_network"

    .line 897
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "download_config_dl_size"

    .line 898
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "download_config_storage_internal"

    .line 899
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "vbtt"

    .line 901
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "fetch_template"

    .line 902
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->H:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "web_info_wifi_enable"

    .line 903
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "web_info_page_count"

    .line 904
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "splash_load_type"

    .line 905
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "splash_check_type"

    .line 906
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "if_both_open"

    .line 907
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->C:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "support_tnc"

    .line 908
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "pyload_h5"

    .line 909
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->u:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "playableLoadH5Url"

    .line 910
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->v:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "app_list_control"

    .line 911
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "max_tpl_cnts"

    .line 912
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "fetch_tpl_timeout_ctrl"

    .line 913
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "gecko_hosts"

    .line 914
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "hit_app_list_time"

    .line 915
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->z:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "hit_app_list_data"

    .line 916
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "scheme_list_data"

    .line 917
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "circle_splash_switch"

    .line 918
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "circle_load_splash_time"

    .line 919
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "sp_key_if_sp_cache"

    .line 921
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "dyn_draw_engine_url"

    .line 922
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "download_sdk_config"

    .line 923
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v1, "enable_download_opt"

    .line 924
    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 927
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v1, "ab_test_version"

    .line 928
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "tt_sdk_settings"

    const-string v1, "ab_test_param"

    .line 931
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "tt_sdk_settings"

    const-string v1, "push_config"

    .line 934
    invoke-static {v0, v1, p2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_3

    const-string p2, "tt_sdk_settings"

    const-string v0, "ad_slot_conf"

    .line 937
    invoke-static {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->g:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    const-string p1, "tt_sdk_settings"

    const-string p2, "template_ids"

    .line 940
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->g:Ljava/lang/String;

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->e:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_5

    const-string p1, "tt_sdk_settings"

    const-string p2, "tpl_infos"

    .line 943
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->e:Ljava/lang/String;

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string p1, "tt_sdk_settings"

    const-string p2, "if_pre_connect"

    .line 946
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string p1, "tt_sdk_settings"

    const-string p2, "global_rate"

    .line 947
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    const-string p1, "tt_sdk_settings"

    const-string p2, "read_video_from_cache"

    .line 949
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-void

    .line 954
    :cond_6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "url_ads"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "url_alog"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "xpath"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "duration"

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;J)V

    .line 958
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "max"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 959
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "download_config_dl_network"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 960
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "download_config_dl_size"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 961
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "download_config_storage_internal"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 963
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "fetch_template"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->H:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 964
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "web_info_wifi_enable"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 965
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "web_info_page_count"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 966
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "pyload_h5"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "playableLoadH5Url"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "splash_load_type"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 969
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "splash_check_type"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 970
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "if_both_open"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->C:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 971
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "support_tnc"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 972
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "app_list_control"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 973
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "max_tpl_cnts"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 974
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "fetch_tpl_timeout_ctrl"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 975
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "gecko_hosts"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/util/Set;)V

    .line 976
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "hit_app_list_time"

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->z:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;J)V

    .line 977
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "hit_app_list_data"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/util/Set;)V

    .line 978
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "scheme_list_data"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/util/Set;)V

    .line 980
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "circle_splash_switch"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 981
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "circle_load_splash_time"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 982
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "dyn_draw_engine_url"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "sp_key_if_sp_cache"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 985
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "download_sdk_config"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "enable_download_opt"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 987
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 988
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "ab_test_version"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    :cond_7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 991
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "ab_test_param"

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    :cond_8
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "vbtt"

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 995
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 996
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "push_config"

    invoke-virtual {v0, v1, p2}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    :cond_9
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_a

    .line 1000
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v0, "ad_slot_conf"

    invoke-virtual {p2, v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    :cond_a
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->g:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_b

    .line 1004
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string p2, "template_ids"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->g:Ljava/lang/String;

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1007
    :cond_b
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->e:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_c

    .line 1008
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string p2, "tpl_infos"

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->e:Ljava/lang/String;

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    :cond_c
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string p2, "if_pre_connect"

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 1012
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string p2, "global_rate"

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    .line 1014
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string p2, "read_video_from_cache"

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private static b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/h/a;
    .locals 26

    move-object/from16 v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const-string v1, "code_id"

    .line 536
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "auto_play"

    const/4 v3, 0x1

    .line 537
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "voice_control"

    .line 538
    invoke-virtual {v0, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v5, "rv_preload"

    const/4 v6, 0x2

    .line 539
    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    const-string v7, "nv_preload"

    .line 540
    invoke-virtual {v0, v7, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v7

    const-string v8, "proportion_watching"

    const/16 v9, 0x64

    .line 542
    invoke-virtual {v0, v8, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    const-string v9, "skip_time_displayed"

    const/4 v10, 0x0

    .line 543
    invoke-virtual {v0, v9, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v9

    const-string v11, "video_skip_result"

    .line 544
    invoke-virtual {v0, v11, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v11

    const-string v12, "reg_creative_control"

    .line 545
    invoke-virtual {v0, v12, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v12

    const-string v13, "play_bar_show_time"

    const/4 v14, 0x3

    .line 546
    invoke-virtual {v0, v13, v14}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v13

    const-string v14, "rv_skip_time"

    const/4 v15, -0x1

    .line 547
    invoke-virtual {v0, v14, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v14

    const-string v10, "endcard_close_time"

    .line 548
    invoke-virtual {v0, v10, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v10

    const-string v3, "playable_endcard_close_time"

    .line 549
    invoke-virtual {v0, v3, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v15, "voice_control"

    .line 550
    invoke-virtual {v0, v15, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v15

    const-string v6, "if_show_win"

    move/from16 v16, v3

    const/4 v3, 0x1

    .line 551
    invoke-virtual {v0, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    const-string v3, "sp_preload"

    move/from16 v17, v6

    const/4 v6, 0x0

    .line 552
    invoke-virtual {v0, v3, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v6, "stop_time"

    move/from16 v18, v3

    const/16 v3, 0x5dc

    .line 553
    invoke-virtual {v0, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v6, "native_playable_delay"

    move/from16 v19, v3

    const/4 v3, 0x2

    .line 554
    invoke-virtual {v0, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v6, "time_out_control"

    move/from16 v20, v3

    const/4 v3, -0x1

    .line 555
    invoke-virtual {v0, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    const-string v3, "playable_duration_time"

    move/from16 v21, v6

    const/16 v6, 0x14

    .line 556
    invoke-virtual {v0, v3, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v6, "playable_close_time"

    move/from16 v22, v3

    const/4 v3, -0x1

    .line 557
    invoke-virtual {v0, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v6, "playable_reward_type"

    move/from16 v23, v3

    const/4 v3, 0x0

    .line 558
    invoke-virtual {v0, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    move/from16 v24, v6

    const-string v6, "reward_is_callback"

    .line 559
    invoke-virtual {v0, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v6, "iv_skip_time"

    move/from16 v25, v3

    const/4 v3, 0x5

    .line 560
    invoke-virtual {v0, v6, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v6, "parent_tpl_ids"

    .line 561
    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 563
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/h/h;->f(I)Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v4, 0x1

    .line 566
    :cond_1
    invoke-static {v15}, Lcom/bytedance/sdk/openadsdk/core/h/h;->f(I)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v15, 0x1

    .line 569
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a()Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v6

    .line 570
    invoke-virtual {v6, v1}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 571
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->n(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 572
    invoke-virtual {v1, v4}, Lcom/bytedance/sdk/openadsdk/core/h/a;->o(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 573
    invoke-virtual {v1, v5}, Lcom/bytedance/sdk/openadsdk/core/h/a;->p(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 574
    invoke-virtual {v1, v7}, Lcom/bytedance/sdk/openadsdk/core/h/a;->q(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 576
    invoke-virtual {v1, v8}, Lcom/bytedance/sdk/openadsdk/core/h/a;->r(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 577
    invoke-virtual {v1, v9}, Lcom/bytedance/sdk/openadsdk/core/h/a;->s(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 578
    invoke-virtual {v1, v11}, Lcom/bytedance/sdk/openadsdk/core/h/a;->t(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 579
    invoke-virtual {v1, v12}, Lcom/bytedance/sdk/openadsdk/core/h/a;->u(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 580
    invoke-virtual {v1, v13}, Lcom/bytedance/sdk/openadsdk/core/h/a;->m(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 581
    invoke-virtual {v1, v14}, Lcom/bytedance/sdk/openadsdk/core/h/a;->l(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 582
    invoke-virtual {v1, v10}, Lcom/bytedance/sdk/openadsdk/core/h/a;->j(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 583
    invoke-virtual {v1, v15}, Lcom/bytedance/sdk/openadsdk/core/h/a;->i(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v16

    .line 584
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->k(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v17

    .line 585
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->v(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v18

    .line 586
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->f(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v19

    .line 587
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->g(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v20

    .line 588
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->h(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v21

    .line 589
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->e(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v23

    .line 590
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v22

    .line 591
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->d(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v24

    .line 592
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->b(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    move/from16 v2, v25

    .line 593
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->c(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 594
    invoke-virtual {v1, v3}, Lcom/bytedance/sdk/openadsdk/core/h/a;->w(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v1

    .line 595
    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a(Lorg/json/JSONArray;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v0

    return-object v0
.end method

.method private c(Lorg/json/JSONObject;)I
    .locals 2

    const-string v0, "splash_load_type"

    const/4 v1, 0x2

    .line 813
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    .line 815
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    packed-switch p1, :pswitch_data_0

    return v1

    .line 820
    :pswitch_0
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    return p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private d(Lorg/json/JSONObject;)I
    .locals 2

    const-string v0, "splash_check_type"

    const/4 v1, 0x1

    .line 826
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    .line 828
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    packed-switch p1, :pswitch_data_0

    return v1

    .line 831
    :pswitch_0
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    return p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static f(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private s(Ljava/lang/String;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    .line 512
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 514
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_1

    .line 516
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/h/e;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/core/h/e;-><init>()V

    .line 517
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "action"

    .line 518
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/bytedance/sdk/openadsdk/core/h/e;->a:Ljava/lang/String;

    const-string v4, "service"

    .line 519
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/bytedance/sdk/openadsdk/core/h/e;->b:Ljava/lang/String;

    const-string v4, "package"

    .line 520
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/bytedance/sdk/openadsdk/core/h/e;->c:Ljava/lang/String;

    const-string v4, "wakeup_interval"

    .line 521
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/bytedance/sdk/openadsdk/core/h/e;->d:I

    .line 522
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->i:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 525
    :cond_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h/d;->a()Lcom/bytedance/sdk/openadsdk/core/h/d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/h/d;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 527
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    return-void
.end method

.method private t(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;
    .locals 3

    .line 1790
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 1794
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a()Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v2

    .line 1795
    invoke-virtual {v2, p1}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1796
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/h/a;->n(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1797
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/h/a;->o(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    const/4 v0, 0x2

    .line 1798
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/h/a;->p(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1799
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/h/a;->q(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    const/16 v2, 0x64

    .line 1801
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->r(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    const/4 v2, 0x0

    .line 1802
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->s(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1803
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/h/a;->u(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    const/4 v2, 0x3

    .line 1804
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->m(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    const/4 v2, -0x1

    .line 1805
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->l(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1806
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->j(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1807
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/h/a;->i(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1808
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->k(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1809
    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/h/a;->v(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1810
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->e(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1811
    invoke-virtual {p1, v2}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    const/16 v0, 0x14

    .line 1812
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/h/a;->d(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    const/4 v0, 0x5

    .line 1813
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/h/a;->w(I)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    const/4 v0, 0x0

    .line 1814
    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/h/a;->a(Lorg/json/JSONArray;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public A()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1626
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->z:J

    const-wide/32 v2, 0xa4cb800

    add-long/2addr v0, v2

    .line 1627
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 1631
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1632
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1633
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public B()I
    .locals 3

    .line 1639
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1640
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "sp_key_if_sp_cache"

    .line 1641
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    goto :goto_0

    .line 1643
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "sp_key_if_sp_cache"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    .line 1646
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    return v0
.end method

.method public C()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1654
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->z:J

    const-wide/32 v2, 0xa4cb800

    add-long/2addr v0, v2

    .line 1655
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 1658
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1659
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1660
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public D()Z
    .locals 3

    .line 1673
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    const/4 v1, 0x0

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1674
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "app_list_control"

    .line 1675
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    goto :goto_0

    .line 1677
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "app_list_control"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    .line 1680
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public E()I
    .locals 3

    .line 1684
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1685
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/16 v1, 0x64

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "max_tpl_cnts"

    .line 1686
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    goto :goto_0

    .line 1688
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "max_tpl_cnts"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    .line 1691
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    return v0
.end method

.method public F()I
    .locals 3

    .line 1697
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    const/16 v1, 0xbb8

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1698
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "fetch_tpl_timeout_ctrl"

    .line 1699
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    goto :goto_0

    .line 1701
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "fetch_tpl_timeout_ctrl"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    .line 1705
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    if-gtz v0, :cond_2

    .line 1706
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    .line 1710
    :cond_2
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    return v0
.end method

.method public G()[Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 1715
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 1717
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "tt_sdk_settings"

    const-string v2, "gecko_hosts"

    .line 1718
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    goto :goto_0

    .line 1720
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "gecko_hosts"

    invoke-virtual {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    .line 1722
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    .line 1723
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_2

    goto :goto_1

    .line 1727
    :cond_2
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_3
    :goto_1
    return-object v0

    :catch_0
    return-object v0
.end method

.method public H()I
    .locals 3

    .line 1749
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1750
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x5

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "vbtt"

    .line 1751
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    goto :goto_0

    .line 1753
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "vbtt"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    .line 1756
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    return v0
.end method

.method public I()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/h/e;",
            ">;"
        }
    .end annotation

    .line 1819
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1820
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "push_config"

    .line 1821
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1822
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->s(Ljava/lang/String;)V

    goto :goto_0

    .line 1824
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "push_config"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1825
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->s(Ljava/lang/String;)V

    .line 1828
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->i:Ljava/util/List;

    return-object v0
.end method

.method public J()Z
    .locals 1

    .line 1854
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->Q:Z

    return v0
.end method

.method public K()Z
    .locals 3

    .line 1859
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    const/4 v1, 0x0

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1860
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "if_pre_connect"

    .line 1861
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    goto :goto_0

    .line 1863
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "if_pre_connect"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    .line 1866
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public L()Z
    .locals 3

    .line 1872
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    const/4 v1, 0x1

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1873
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "read_video_from_cache"

    .line 1874
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    goto :goto_0

    .line 1876
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "read_video_from_cache"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    .line 1879
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public a(Ljava/lang/String;Z)I
    .locals 2

    if-nez p1, :cond_0

    .line 1559
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/h/h;->a(Z)I

    move-result p1

    return p1

    .line 1560
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1561
    iget v0, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->u:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->u:I

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/h/h;->a(Z)I

    move-result p1

    :goto_0
    return p1
.end method

.method public declared-synchronized a()V
    .locals 16

    move-object/from16 v1, p0

    monitor-enter p0

    const/4 v2, 0x1

    .line 297
    :try_start_0
    iput-boolean v2, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->Q:Z

    .line 298
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/16 v6, 0xbb8

    const/16 v7, 0x64

    const/4 v8, 0x2

    const/16 v9, 0xe10

    const/16 v10, 0x1e

    const/16 v11, 0x32

    const-wide/16 v12, 0x2710

    const/4 v14, 0x5

    const/4 v15, 0x0

    const/4 v3, 0x0

    if-eqz v0, :cond_6

    const-string v0, "tt_sdk_settings"

    const-string v4, "url_ads"

    const-string v5, "pangolin.snssdk.com"

    .line 300
    invoke-static {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "url_alog"

    const-string v5, "extlog.snssdk.com/service/2/app_log/"

    .line 301
    invoke-static {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "xpath"

    const-string v5, ""

    .line 302
    invoke-static {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->j:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "duration"

    .line 303
    invoke-static {v0, v4, v12, v13}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    const-string v0, "tt_sdk_settings"

    const-string v4, "max"

    .line 304
    invoke-static {v0, v4, v11}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "download_config_dl_network"

    .line 305
    invoke-static {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "download_config_dl_size"

    .line 306
    invoke-static {v0, v4, v10}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "download_config_storage_internal"

    .line 307
    invoke-static {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "vbtt"

    .line 309
    invoke-static {v0, v4, v14}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "fetch_template"

    .line 310
    invoke-static {v0, v4, v9}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->H:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "template_ids"

    .line 311
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->g:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "ab_test_version"

    .line 312
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "ab_test_param"

    .line 313
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "web_info_wifi_enable"

    .line 314
    invoke-static {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "web_info_page_count"

    .line 315
    invoke-static {v0, v4, v14}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "pyload_h5"

    .line 316
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->u:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "playableLoadH5Url"

    .line 317
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->v:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "splash_load_type"

    .line 318
    invoke-static {v0, v4, v8}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "splash_check_type"

    .line 319
    invoke-static {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "if_both_open"

    .line 320
    invoke-static {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->C:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "support_tnc"

    .line 321
    invoke-static {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "tpl_infos"

    .line 322
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->e:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "if_pre_connect"

    .line 324
    invoke-static {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "global_rate"

    .line 325
    invoke-static {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "app_list_control"

    .line 326
    invoke-static {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "max_tpl_cnts"

    .line 327
    invoke-static {v0, v4, v7}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "fetch_tpl_timeout_ctrl"

    .line 329
    invoke-static {v0, v4, v6}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    .line 330
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    :cond_0
    const-string v0, "tt_sdk_settings"

    const-string v4, "gecko_hosts"

    .line 333
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    .line 334
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    const-string v0, "tt_sdk_settings"

    const-string v4, "hit_app_list_time"

    const-wide/16 v5, 0x0

    .line 335
    invoke-static {v0, v4, v5, v6}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->z:J

    .line 336
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const-string v0, "tt_sdk_settings"

    const-string v4, "circle_splash_switch"

    .line 338
    invoke-static {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "circle_load_splash_time"

    const/4 v5, -0x1

    .line 339
    invoke-static {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "sp_key_if_sp_cache"

    .line 341
    invoke-static {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    const-string v0, "tt_sdk_settings"

    const-string v4, "dyn_draw_engine_url"

    const-string v5, "https://sf3-ttcdn-tos.pstatp.com/obj/ad-pattern/renderer/package.json"

    .line 344
    invoke-static {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    const-string v0, "tt_sdk_settings"

    const-string v4, "hit_app_list_data"

    .line 345
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 346
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 347
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 348
    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 351
    :cond_1
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const-string v0, "tt_sdk_settings"

    const-string v4, "scheme_list_data"

    .line 352
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 353
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 354
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 355
    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-string v0, "tt_sdk_settings"

    const-string v4, "push_config"

    .line 358
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 359
    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->s(Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v4, "ad_slot_conf"

    .line 362
    invoke-static {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_4

    .line 365
    :try_start_1
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 366
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 368
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v0, :cond_4

    .line 370
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 371
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/h/h;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 373
    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    iget-object v7, v5, Lcom/bytedance/sdk/openadsdk/core/h/a;->a:Ljava/lang/String;

    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :catch_0
    :cond_4
    :try_start_2
    const-string v0, "tt_sdk_settings"

    const-string v3, "download_sdk_config"

    const-string v4, ""

    .line 381
    invoke-static {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    .line 382
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v0, :cond_5

    .line 384
    :try_start_3
    new-instance v0, Lorg/json/JSONObject;

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->o:Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :catch_1
    move-exception v0

    .line 386
    :try_start_4
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :cond_5
    :goto_3
    const-string v0, "tt_sdk_settings"

    const-string v3, "enable_download_opt"

    .line 389
    invoke-static {v0, v3, v15}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    const-string v0, "tt_sdk_settings"

    const-string v3, "read_video_from_cache"

    .line 391
    invoke-static {v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    .line 393
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 394
    monitor-exit p0

    return-void

    .line 397
    :cond_6
    :try_start_5
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "url_ads"

    const-string v5, "pangolin.snssdk.com"

    invoke-virtual {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    .line 398
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "url_alog"

    const-string v5, "extlog.snssdk.com/service/2/app_log/"

    invoke-virtual {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    .line 399
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "xpath"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->j:Ljava/lang/String;

    .line 400
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "duration"

    invoke-virtual {v0, v4, v12, v13}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    .line 401
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "max"

    invoke-virtual {v0, v4, v11}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    .line 402
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "download_config_dl_network"

    invoke-virtual {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    .line 403
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "download_config_dl_size"

    invoke-virtual {v0, v4, v10}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    .line 404
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "download_config_storage_internal"

    invoke-virtual {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    .line 406
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "fetch_template"

    invoke-virtual {v0, v4, v9}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->H:I

    .line 407
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "ab_test_version"

    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    .line 408
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "ab_test_param"

    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    .line 410
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "vbtt"

    invoke-virtual {v0, v4, v14}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    .line 411
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "template_ids"

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->g:Ljava/lang/String;

    .line 412
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "web_info_wifi_enable"

    invoke-virtual {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    .line 413
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "web_info_page_count"

    invoke-virtual {v0, v4, v14}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    .line 414
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "pyload_h5"

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->u:Ljava/lang/String;

    .line 415
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "playableLoadH5Url"

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->v:Ljava/lang/String;

    .line 416
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "splash_load_type"

    invoke-virtual {v0, v4, v8}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    .line 417
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "splash_check_type"

    invoke-virtual {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    .line 418
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "if_both_open"

    invoke-virtual {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->C:I

    .line 419
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "support_tnc"

    invoke-virtual {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    .line 420
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "tpl_infos"

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->e:Ljava/lang/String;

    .line 422
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "if_pre_connect"

    invoke-virtual {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    .line 423
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "global_rate"

    invoke-virtual {v0, v4, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    .line 425
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "app_list_control"

    invoke-virtual {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    .line 426
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "max_tpl_cnts"

    invoke-virtual {v0, v4, v7}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    .line 428
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "fetch_tpl_timeout_ctrl"

    invoke-virtual {v0, v4, v6}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    .line 429
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    if-eqz v0, :cond_7

    .line 430
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 432
    :cond_7
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "gecko_hosts"

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    .line 433
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    .line 434
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "hit_app_list_time"

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v4, v5, v6}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->z:J

    .line 435
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 437
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "circle_splash_switch"

    invoke-virtual {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    .line 438
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "circle_load_splash_time"

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    .line 439
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "dyn_draw_engine_url"

    const-string v5, "https://sf3-ttcdn-tos.pstatp.com/obj/ad-pattern/renderer/package.json"

    invoke-virtual {v0, v4, v5}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    .line 440
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "sp_key_if_sp_cache"

    invoke-virtual {v0, v4, v15}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    .line 443
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "hit_app_list_data"

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 444
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    .line 445
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 446
    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 449
    :cond_8
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 450
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "scheme_list_data"

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 451
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    .line 452
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 453
    iget-object v5, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 456
    :cond_9
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "push_config"

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 457
    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->s(Ljava/lang/String;)V

    .line 460
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v4, "ad_slot_conf"

    invoke-virtual {v0, v4, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 461
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v3, :cond_b

    .line 463
    :try_start_6
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 464
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_b

    .line 466
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    const/4 v4, 0x0

    :goto_6
    if-ge v4, v0, :cond_b

    .line 468
    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 469
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/h/h;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v5

    if-eqz v5, :cond_a

    .line 471
    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    iget-object v7, v5, Lcom/bytedance/sdk/openadsdk/core/h/a;->a:Ljava/lang/String;

    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 479
    :catch_2
    :cond_b
    :try_start_7
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v3, "download_sdk_config"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    .line 480
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-nez v0, :cond_c

    .line 482
    :try_start_8
    new-instance v0, Lorg/json/JSONObject;

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->o:Lorg/json/JSONObject;
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_7

    :catch_3
    move-exception v0

    .line 484
    :try_start_9
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 487
    :cond_c
    :goto_7
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v3, "enable_download_opt"

    invoke-virtual {v0, v3, v15}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    .line 489
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v3, "read_video_from_cache"

    invoke-virtual {v0, v3, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    .line 491
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 492
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 296
    monitor-exit p0

    throw v0
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 9
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "app_common_config"

    .line 605
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v2, "circle_splash"

    .line 607
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    const-string v2, "circle_time"

    const/4 v3, -0x1

    .line 608
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    const-string v2, "if_sp_cache"

    .line 609
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->O:I

    const-string v2, "if_pre_connect"

    .line 610
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->P:I

    :cond_0
    const-string v0, "dyn_draw_engine_url"

    const-string v2, "https://sf3-ttcdn-tos.pstatp.com/obj/ad-pattern/renderer/package.json"

    .line 612
    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    const-string v0, "ads_url"

    const-string v2, "pangolin.snssdk.com"

    .line 613
    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    const-string v0, "app_log_url"

    const-string v2, "extlog.snssdk.com/service/2/app_log/"

    .line 614
    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    const-string v0, "xpath"

    .line 616
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->j:Ljava/lang/String;

    const-string v0, "feq_policy"

    .line 618
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "duration"

    .line 620
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long v2, v2, v4

    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    const-string v2, "max"

    .line 621
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    :cond_1
    const-string v0, "vbtt"

    const/4 v2, 0x5

    .line 624
    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->G:I

    const-string v0, "fetch_tpl_interval"

    const/16 v3, 0xe10

    .line 625
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->H:I

    const-string v0, "abtest"

    .line 628
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v3, "version"

    .line 632
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    const-string v3, "param"

    .line 633
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    goto :goto_0

    .line 636
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "tt_sdk_settings"

    const-string v3, "ab_test_version"

    .line 637
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "tt_sdk_settings"

    const-string v3, "ab_test_param"

    .line 638
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 640
    :cond_3
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v3, "ab_test_version"

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;)V

    .line 641
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v3, "ab_test_param"

    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;)V

    :goto_0
    const-string v0, "read_video_from_cache"

    const/4 v3, 0x1

    .line 645
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->S:I

    const-string v0, "web_info"

    .line 649
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v4, "web_info_wifi_enable"

    .line 651
    invoke-virtual {v0, v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    const-string v4, "web_info_page_count"

    .line 652
    invoke-virtual {v0, v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    :cond_4
    const-string v0, "log_rate_conf"

    .line 655
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v2, "global_rate"

    .line 657
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    :cond_5
    const-string v0, "pyload_h5"

    .line 660
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->u:Ljava/lang/String;

    const-string v0, "pure_pyload_h5"

    .line 661
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->v:Ljava/lang/String;

    .line 664
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->c(Lorg/json/JSONObject;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    const-string v0, "splashLoad"

    .line 665
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting-\u300bmSplashLoadType="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->d(Lorg/json/JSONObject;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    const-string v0, "splashLoad"

    .line 667
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting-\u300bmSplashCheckType="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "if_both_open"

    .line 669
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->C:I

    const-string v0, "support_tnc"

    .line 670
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    const-string v0, "al"

    .line 672
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->y:I

    const-string v0, "max_tpl_cnts"

    const/16 v2, 0x64

    .line 673
    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->K:I

    const-string v0, "app_common_config"

    .line 675
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string v2, "fetch_tpl_timeout_ctrl"

    const/16 v4, 0xbb8

    .line 677
    invoke-virtual {v0, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->L:I

    .line 681
    :try_start_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    const-string v2, "gecko_hosts"

    .line 682
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 683
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    .line 684
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v2, v4, :cond_6

    .line 685
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 688
    :cond_6
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->M:Ljava/util/Set;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 690
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GeckoLog: settings json error "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;)V

    .line 694
    :cond_7
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->z:J

    .line 695
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const-string v0, "spam_app_list"

    .line 696
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 698
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v2, :cond_9

    .line 700
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    .line 701
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 702
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->A:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 707
    :cond_9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const-string v0, "scheme_check_list"

    .line 708
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 710
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v2, :cond_b

    .line 712
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    .line 713
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 714
    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->B:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_b
    const-string v0, "download_config"

    .line 719
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_c

    const-string v2, "dl_network"

    .line 721
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    const-string v2, "dl_size"

    const/16 v4, 0x1e

    .line 722
    invoke-virtual {v0, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    const-string v2, "if_storage_internal"

    .line 723
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    const-string v2, "enable_download_opt"

    .line 724
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    :cond_c
    const-string v0, "download_sdk_config"

    .line 726
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->o:Lorg/json/JSONObject;

    .line 727
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->o:Lorg/json/JSONObject;

    if-eqz v0, :cond_d

    .line 728
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->o:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    goto :goto_5

    :cond_d
    const-string v0, ""

    .line 730
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    :goto_5
    const/4 v0, 0x0

    :try_start_1
    const-string v2, "push_config"

    .line 744
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 746
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 748
    :try_start_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    const/4 v5, 0x0

    :goto_6
    if-ge v5, v4, :cond_10

    .line 750
    new-instance v6, Lcom/bytedance/sdk/openadsdk/core/h/e;

    invoke-direct {v6}, Lcom/bytedance/sdk/openadsdk/core/h/e;-><init>()V

    .line 751
    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    if-eqz v7, :cond_e

    const-string v8, "action"

    .line 753
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/bytedance/sdk/openadsdk/core/h/e;->a:Ljava/lang/String;

    const-string v8, "service"

    .line 754
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/bytedance/sdk/openadsdk/core/h/e;->b:Ljava/lang/String;

    const-string v8, "package"

    .line 755
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/bytedance/sdk/openadsdk/core/h/e;->c:Ljava/lang/String;

    const-string v8, "wakeup_interval"

    .line 756
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v6, Lcom/bytedance/sdk/openadsdk/core/h/e;->d:I

    .line 757
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->i:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_f
    move-object v3, v0

    .line 762
    :cond_10
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/h/d;->a()Lcom/bytedance/sdk/openadsdk/core/h/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/h/d;->b()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_8

    :catch_1
    move-exception v2

    goto :goto_7

    :catch_2
    move-exception v2

    move-object v3, v0

    .line 764
    :goto_7
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_8
    const-string v2, "ad_slot_conf_list"

    .line 770
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_12

    .line 772
    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    .line 773
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_12

    .line 775
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    :goto_9
    if-ge v1, v2, :cond_12

    .line 777
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 778
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/h/h;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v4

    if-eqz v4, :cond_11

    .line 780
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    iget-object v6, v4, Lcom/bytedance/sdk/openadsdk/core/h/a;->a:Ljava/lang/String;

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 808
    :cond_12
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/downloadnew/a/g;->c()V

    .line 809
    invoke-direct {p0, v0, v3}, Lcom/bytedance/sdk/openadsdk/core/h/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(I)Z
    .locals 1

    .line 1389
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1390
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->c:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .line 1400
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1402
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->e:I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    return v0

    :pswitch_0
    return v0

    .line 1407
    :pswitch_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 1404
    :pswitch_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/x;->d(Landroid/content/Context;)Z

    move-result p1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1030
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1031
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v1, "url_ads"

    const-string v2, "pangolin.snssdk.com"

    .line 1033
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    goto :goto_0

    .line 1035
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "url_ads"

    const-string v2, "pangolin.snssdk.com"

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    .line 1037
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "pangolin.snssdk.com"

    .line 1038
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    .line 1041
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->I:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1611
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1612
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->n:I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .line 1429
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1430
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->g:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c(I)I
    .locals 0

    .line 1734
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1735
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->b:I

    return p1
.end method

.method public c(Ljava/lang/String;)I
    .locals 0

    .line 1440
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1441
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->s:I

    return p1
.end method

.method public c()Ljava/lang/String;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1051
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1052
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v1, "url_alog"

    const-string v2, "extlog.snssdk.com/service/2/app_log/"

    .line 1054
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    goto :goto_0

    .line 1056
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "url_alog"

    const-string v2, "extlog.snssdk.com/service/2/app_log/"

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    .line 1058
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "extlog.snssdk.com/service/2/app_log/"

    .line 1059
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    .line 1062
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->J:Ljava/lang/String;

    return-object v0
.end method

.method public d(I)I
    .locals 0

    .line 1739
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1740
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->i:I

    return p1
.end method

.method public d()Z
    .locals 3

    .line 1085
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    const/4 v1, 0x1

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1086
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "support_tnc"

    .line 1087
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    goto :goto_0

    .line 1089
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "support_tnc"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    .line 1092
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->D:I

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public d(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 1446
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1447
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->o:I

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e()I
    .locals 3

    .line 1096
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1097
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "download_config_dl_network"

    .line 1098
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    goto :goto_0

    .line 1100
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "download_config_dl_network"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    .line 1103
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->k:I

    return v0
.end method

.method public e(I)I
    .locals 0

    .line 1744
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1745
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->x:I

    return p1
.end method

.method public e(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 1460
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1461
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->p:I

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()I
    .locals 3

    .line 1107
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1108
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/16 v1, 0x1e

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "download_config_dl_size"

    .line 1109
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    goto :goto_0

    .line 1111
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "download_config_dl_size"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    .line 1115
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->l:I

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    return v0
.end method

.method public f(Ljava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    const/16 p1, 0x5dc

    return p1

    .line 1471
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1472
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->q:I

    return p1
.end method

.method public g(Ljava/lang/String;)I
    .locals 1

    .line 1482
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1483
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->k:I

    return p1
.end method

.method public g()Ljava/lang/String;
    .locals 3

    .line 1119
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1120
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v1, "ab_test_version"

    const/4 v2, 0x0

    .line 1121
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    goto :goto_0

    .line 1123
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "ab_test_version"

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    .line 1126
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->q:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 3

    .line 1130
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1131
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v1, "ab_test_param"

    const/4 v2, 0x0

    .line 1132
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    goto :goto_0

    .line 1134
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "ab_test_param"

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    .line 1137
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->r:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 6

    const/4 v0, 0x0

    .line 1489
    :try_start_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/a;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 1490
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 1493
    :cond_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 1494
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1495
    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/core/widget/webview/a/a;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/d/s;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1497
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "id"

    .line 1498
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/s;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v4, "md5"

    .line 1499
    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/s;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1500
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_2
    return-object v1

    :cond_3
    :goto_1
    return-object v0

    :catch_0
    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 3

    .line 1141
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1142
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v1, "xpath"

    const-string v2, ""

    .line 1143
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->j:Ljava/lang/String;

    goto :goto_0

    .line 1145
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "xpath"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->j:Ljava/lang/String;

    .line 1148
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->j:Ljava/lang/String;

    return-object v0
.end method

.method public i(Ljava/lang/String;)Z
    .locals 1

    .line 1512
    :try_start_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1513
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->y:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :catch_0
    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public j(Ljava/lang/String;)I
    .locals 0

    .line 1529
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1530
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->h:I

    return p1
.end method

.method public j()J
    .locals 5

    .line 1152
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 1153
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const-wide/16 v1, 0x2710

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v3, "duration"

    .line 1154
    invoke-static {v0, v3, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    goto :goto_0

    .line 1156
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v3, "duration"

    invoke-virtual {v0, v3, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    .line 1159
    :cond_1
    :goto_0
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->E:J

    return-wide v0
.end method

.method public k()I
    .locals 3

    .line 1163
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1164
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/16 v1, 0x32

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "max"

    .line 1165
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    goto :goto_0

    .line 1167
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "max"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    .line 1170
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->F:I

    return v0
.end method

.method public k(Ljava/lang/String;)I
    .locals 0

    .line 1540
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1541
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->j:I

    return p1
.end method

.method public l()I
    .locals 3

    .line 1196
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1197
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x5

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "web_info_page_count"

    .line 1198
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    goto :goto_0

    .line 1200
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "web_info_page_count"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    .line 1203
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->t:I

    return v0
.end method

.method public l(Ljava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    .line 1553
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1554
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->l:I

    return p1
.end method

.method public m(Ljava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 1568
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1569
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->v:I

    return p1
.end method

.method public m()Ljava/lang/String;
    .locals 3

    .line 1207
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1208
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "pyload_h5"

    .line 1209
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->u:Ljava/lang/String;

    goto :goto_0

    .line 1211
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "pyload_h5"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->u:Ljava/lang/String;

    .line 1214
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->u:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 3

    .line 1218
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1219
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "playableLoadH5Url"

    .line 1220
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->v:Ljava/lang/String;

    goto :goto_0

    .line 1222
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "playableLoadH5Url"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->v:Ljava/lang/String;

    .line 1225
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->v:Ljava/lang/String;

    return-object v0
.end method

.method public n(Ljava/lang/String;)Z
    .locals 0

    .line 1578
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->w:I

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public o(Ljava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    const/16 p1, 0x14

    return p1

    .line 1583
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1584
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->t:I

    return p1
.end method

.method public o()Lorg/json/JSONObject;
    .locals 3

    .line 1232
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->o:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v1, "download_sdk_config"

    const-string v2, ""

    .line 1233
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    .line 1234
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1236
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->p:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->o:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1238
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 1243
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->o:Lorg/json/JSONObject;

    return-object v0
.end method

.method public p(Ljava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    .line 1604
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object p1

    .line 1605
    iget p1, p1, Lcom/bytedance/sdk/openadsdk/core/h/a;->m:I

    return p1
.end method

.method public p()Z
    .locals 3

    .line 1247
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    const/4 v1, 0x0

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1248
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "enable_download_opt"

    .line 1249
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    goto :goto_0

    .line 1251
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "enable_download_opt"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    .line 1254
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->n:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public q()I
    .locals 3

    .line 1266
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1267
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "splash_load_type"

    .line 1268
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    goto :goto_0

    .line 1270
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "splash_load_type"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    .line 1274
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->w:I

    return v0
.end method

.method public q(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;
    .locals 7

    .line 1760
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "tt_sdk_settings"

    const-string v1, "ad_slot_conf"

    const/4 v2, 0x0

    .line 1761
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1762
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1764
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1765
    :try_start_1
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 1766
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1767
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    .line 1770
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 1771
    invoke-static {v4}, Lcom/bytedance/sdk/openadsdk/core/h/h;->b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1773
    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    iget-object v6, v4, Lcom/bytedance/sdk/openadsdk/core/h/a;->a:Ljava/lang/String;

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1778
    :cond_1
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 1784
    :catch_0
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/h/a;

    if-nez v0, :cond_3

    .line 1785
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->t(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/h/a;

    move-result-object v0

    :cond_3
    return-object v0
.end method

.method public r()I
    .locals 3

    .line 1278
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1279
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "splash_check_type"

    .line 1280
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    goto :goto_0

    .line 1282
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "splash_check_type"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    .line 1285
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->x:I

    return v0
.end method

.method public r(Ljava/lang/String;)V
    .locals 1

    .line 1832
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public s()Z
    .locals 1

    .line 1300
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->q()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public t()Z
    .locals 2

    .line 1310
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->r()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public u()Z
    .locals 3

    .line 1314
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    const/4 v1, 0x1

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1315
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "web_info_wifi_enable"

    .line 1316
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    goto :goto_0

    .line 1318
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "web_info_wifi_enable"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    .line 1322
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->s:I

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public v()Z
    .locals 3

    .line 1326
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    const/4 v1, 0x1

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1327
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "global_rate"

    .line 1328
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    goto :goto_0

    .line 1330
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "global_rate"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    .line 1334
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->T:I

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public w()Z
    .locals 3

    .line 1338
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    const/4 v1, 0x1

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_1

    .line 1339
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "download_config_storage_internal"

    .line 1340
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    goto :goto_0

    .line 1342
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "download_config_storage_internal"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    .line 1345
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->m:I

    if-ne v0, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public x()I
    .locals 3

    .line 1349
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1350
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "circle_splash_switch"

    .line 1351
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    goto :goto_0

    .line 1353
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "circle_splash_switch"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    .line 1357
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->a:I

    return v0
.end method

.method public y()I
    .locals 3

    .line 1361
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 1362
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v2, "circle_load_splash_time"

    .line 1363
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    goto :goto_0

    .line 1365
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v2, "circle_load_splash_time"

    invoke-virtual {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    .line 1368
    :cond_1
    :goto_0
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->b:I

    return v0
.end method

.method public z()Ljava/lang/String;
    .locals 3

    .line 1372
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1373
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "tt_sdk_settings"

    const-string v1, "dyn_draw_engine_url"

    const-string v2, "https://sf3-ttcdn-tos.pstatp.com/obj/ad-pattern/renderer/package.json"

    .line 1374
    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    goto :goto_0

    .line 1376
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->R:Lcom/bytedance/sdk/openadsdk/utils/af;

    const-string v1, "dyn_draw_engine_url"

    const-string v2, "https://sf3-ttcdn-tos.pstatp.com/obj/ad-pattern/renderer/package.json"

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    .line 1379
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/h/h;->N:Ljava/lang/String;

    return-object v0
.end method
