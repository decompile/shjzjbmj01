.class Lcom/bytedance/sdk/openadsdk/core/r$2;
.super Ljava/lang/Object;
.source "NetApiImpl.java"

# interfaces
.implements Lcom/bytedance/sdk/adnet/core/m$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/d/m;ILcom/bytedance/sdk/openadsdk/core/q$b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/bytedance/sdk/adnet/core/m$a<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/sdk/openadsdk/core/q$b;

.field final synthetic b:Lcom/bytedance/sdk/openadsdk/AdSlot;

.field final synthetic c:I

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/core/d/m;

.field final synthetic e:Lcom/bytedance/sdk/openadsdk/core/r;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/r;Lcom/bytedance/sdk/openadsdk/core/q$b;Lcom/bytedance/sdk/openadsdk/AdSlot;ILcom/bytedance/sdk/openadsdk/core/d/m;)V
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->a:Lcom/bytedance/sdk/openadsdk/core/q$b;

    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iput p4, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->c:I

    iput-object p5, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->d:Lcom/bytedance/sdk/openadsdk/core/d/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/bytedance/sdk/adnet/core/m;)V
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/adnet/core/m<",
            "Lorg/json/JSONObject;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 391
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    const/4 v0, 0x0

    :try_start_0
    const-string v3, "extra_time_start"

    .line 395
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/adnet/core/m;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v5, 0x1

    goto :goto_0

    :catch_0
    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    .line 399
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    .line 400
    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-object v7, v2, Lcom/bytedance/sdk/adnet/core/m;->a:Ljava/lang/Object;

    check-cast v7, Lorg/json/JSONObject;

    invoke-static {v6, v7}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v6

    if-nez v6, :cond_0

    .line 402
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->a:Lcom/bytedance/sdk/openadsdk/core/q$b;

    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;Lcom/bytedance/sdk/openadsdk/core/q$b;)V

    .line 404
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-wide v5, v2, Lcom/bytedance/sdk/adnet/core/m;->e:J

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v7

    iget v8, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->c:I

    const/4 v9, 0x0

    const/4 v10, -0x1

    const-string v11, "mate parse_fail"

    invoke-static/range {v4 .. v11}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/r$a;ILjava/lang/String;)V

    return-void

    .line 409
    :cond_0
    :try_start_1
    iget-object v7, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    iget-object v8, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->d:Lcom/bytedance/sdk/openadsdk/core/d/m;

    invoke-static {v6, v7, v8}, Lcom/bytedance/sdk/openadsdk/core/r$a;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/d/m;)Lcom/bytedance/sdk/openadsdk/core/r$a;

    move-result-object v15

    .line 411
    iget-object v7, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    invoke-static {v7}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;)Landroid/content/Context;

    move-result-object v7

    iget-object v8, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->i:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/bytedance/sdk/openadsdk/core/k;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 412
    iget v7, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->d:I

    const/16 v8, 0x4e20

    if-eq v7, v8, :cond_1

    .line 413
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->a:Lcom/bytedance/sdk/openadsdk/core/q$b;

    iget v3, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->d:I

    iget-object v4, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->e:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/q$b;->a(ILjava/lang/String;)V

    .line 415
    iget-object v9, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-wide v10, v2, Lcom/bytedance/sdk/adnet/core/m;->e:J

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v12

    iget v13, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->c:I

    iget v0, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->d:I

    iget v3, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->f:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    move-object v14, v15

    move v15, v0

    invoke-static/range {v9 .. v16}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/r$a;ILjava/lang/String;)V

    return-void

    .line 419
    :cond_1
    iget-object v7, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    if-nez v7, :cond_2

    .line 420
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->a:Lcom/bytedance/sdk/openadsdk/core/q$b;

    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;Lcom/bytedance/sdk/openadsdk/core/q$b;)V

    .line 422
    iget-object v9, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-wide v10, v2, Lcom/bytedance/sdk/adnet/core/m;->e:J

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v12

    iget v13, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->c:I

    const/4 v0, -0x1

    const-string v16, "parse_fail"

    move-object v14, v15

    move v15, v0

    invoke-static/range {v9 .. v16}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/r$a;ILjava/lang/String;)V

    return-void

    .line 426
    :cond_2
    iget-object v7, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c(Ljava/lang/String;)V

    .line 427
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    .line 428
    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->a:Lcom/bytedance/sdk/openadsdk/core/q$b;

    iget-object v7, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-interface {v6, v7}, Lcom/bytedance/sdk/openadsdk/core/q$b;->a(Lcom/bytedance/sdk/openadsdk/core/d/a;)V

    .line 430
    iget-object v6, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-static {v6}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(Lcom/bytedance/sdk/openadsdk/core/d/a;)Ljava/util/Map;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 432
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/d/a;->a()Lcom/bytedance/sdk/openadsdk/d/a;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/bytedance/sdk/openadsdk/d/a;->a(Ljava/util/Map;)V

    :cond_3
    if-eqz v5, :cond_4

    .line 437
    iget-object v5, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    .line 438
    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 439
    iget-object v5, v15, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-virtual {v5}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 440
    iget v5, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->c:I

    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/utils/ak;->b(I)Ljava/lang/String;

    move-result-object v16

    .line 441
    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-object v7, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->d:Lcom/bytedance/sdk/openadsdk/core/d/m;

    move-wide v8, v3

    move-wide/from16 v10, v17

    move-object v12, v15

    move-wide/from16 v13, v19

    move-object v5, v15

    move-object v15, v0

    invoke-static/range {v6 .. v16}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;Lcom/bytedance/sdk/openadsdk/core/d/m;JJLcom/bytedance/sdk/openadsdk/core/r$a;JLcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;)V

    .line 442
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-object v6, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->d:Lcom/bytedance/sdk/openadsdk/core/d/m;

    iget-wide v6, v6, Lcom/bytedance/sdk/openadsdk/core/d/m;->f:J

    const/4 v8, 0x0

    sub-long v22, v3, v6

    iget v5, v5, Lcom/bytedance/sdk/openadsdk/core/r$a;->a:I

    int-to-long v5, v5

    sub-long v26, v17, v3

    sub-long v28, v19, v17

    move-object/from16 v21, v0

    move-wide/from16 v24, v5

    invoke-static/range {v21 .. v29}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;JJJJ)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v3, "NetApiImpl"

    const-string v4, "get ad error: "

    .line 446
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 447
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->a:Lcom/bytedance/sdk/openadsdk/core/q$b;

    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;Lcom/bytedance/sdk/openadsdk/core/q$b;)V

    .line 450
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-wide v5, v2, Lcom/bytedance/sdk/adnet/core/m;->e:J

    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v7

    iget v8, v1, Lcom/bytedance/sdk/openadsdk/core/r$2;->c:I

    const/4 v9, 0x0

    const/4 v10, -0x1

    const-string v11, "parse_fail"

    invoke-static/range {v4 .. v11}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/r$a;ILjava/lang/String;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public b(Lcom/bytedance/sdk/adnet/core/m;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/adnet/core/m<",
            "Lorg/json/JSONObject;",
            ">;)V"
        }
    .end annotation

    .line 458
    iget-object v0, p1, Lcom/bytedance/sdk/adnet/core/m;->c:Lcom/bytedance/sdk/adnet/err/VAdError;

    .line 459
    instance-of v1, v0, Lcom/bytedance/sdk/adnet/err/e;

    if-eqz v1, :cond_0

    .line 460
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->a:Lcom/bytedance/sdk/openadsdk/core/q$b;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;Lcom/bytedance/sdk/openadsdk/core/q$b;)V

    .line 463
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-wide v3, p1, Lcom/bytedance/sdk/adnet/core/m;->e:J

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->c:I

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 p1, -0x1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v2 .. v9}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/r$a;ILjava/lang/String;)V

    return-void

    :cond_0
    const/4 v1, -0x2

    .line 469
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 471
    iget-wide v1, p1, Lcom/bytedance/sdk/adnet/core/m;->h:J

    long-to-int v1, v1

    .line 472
    invoke-virtual {v0}, Lcom/bytedance/sdk/adnet/err/VAdError;->getMessage()Ljava/lang/String;

    move-result-object v2

    move v9, v1

    goto :goto_0

    :cond_1
    const/4 v9, -0x2

    .line 475
    :goto_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->a:Lcom/bytedance/sdk/openadsdk/core/q$b;

    invoke-interface {v1, v9, v2}, Lcom/bytedance/sdk/openadsdk/core/q$b;->a(ILjava/lang/String;)V

    .line 478
    instance-of v0, v0, Lcom/bytedance/sdk/adnet/err/g;

    if-eqz v0, :cond_2

    const-string v0, "SocketTimeout"

    move-object v10, v0

    goto :goto_1

    :cond_2
    move-object v10, v2

    .line 481
    :goto_1
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->e:Lcom/bytedance/sdk/openadsdk/core/r;

    iget-wide v4, p1, Lcom/bytedance/sdk/adnet/core/m;->e:J

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->b:Lcom/bytedance/sdk/openadsdk/AdSlot;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getCodeId()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/bytedance/sdk/openadsdk/core/r$2;->c:I

    const/4 v8, 0x0

    invoke-static/range {v3 .. v10}, Lcom/bytedance/sdk/openadsdk/core/r;->a(Lcom/bytedance/sdk/openadsdk/core/r;JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/r$a;ILjava/lang/String;)V

    return-void
.end method
