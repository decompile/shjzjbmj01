.class public Lcom/bytedance/sdk/openadsdk/core/x;
.super Ljava/lang/Object;
.source "TTAndroidObject.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/e/b;
.implements Lcom/bytedance/sdk/openadsdk/utils/am$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/x$a;
    }
.end annotation


# static fields
.field private static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Z

.field private B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/d/l;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/openadsdk/core/j;",
            ">;"
        }
    .end annotation
.end field

.field private D:Z

.field private E:Z

.field private F:Lcom/bytedance/sdk/openadsdk/c/p;

.field private G:Lcom/bytedance/sdk/openadsdk/f/a/q;

.field private H:Ljava/lang/String;

.field protected a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field b:Z

.field c:Z

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/bytedance/sdk/openadsdk/utils/am;

.field private g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/bytedance/sdk/openadsdk/e/c;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field private o:Z

.field private p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

.field private q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

.field private r:Lorg/json/JSONObject;

.field private s:Lcom/bytedance/sdk/openadsdk/e/d;

.field private t:Lcom/bytedance/sdk/openadsdk/g/a;

.field private u:Lcom/bytedance/sdk/openadsdk/g/e;

.field private v:Lcom/bytedance/sdk/openadsdk/g/d;

.field private w:Lorg/json/JSONObject;

.field private x:Lcom/bytedance/sdk/openadsdk/core/a/d;

.field private y:Lcom/bytedance/sdk/openadsdk/g/b;

.field private z:Lcom/bytedance/sdk/openadsdk/g/h;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 95
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/x;->f:Ljava/util/Map;

    .line 147
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/x;->f:Ljava/util/Map;

    const-string v1, "log_event"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/x;->f:Ljava/util/Map;

    const-string v1, "private"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/x;->f:Ljava/util/Map;

    const-string v1, "dispatch_message"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/x;->f:Ljava/util/Map;

    const-string v1, "custom_event"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/x;->f:Ljava/util/Map;

    const-string v1, "log_event_v3"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 109
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->o:Z

    .line 122
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->A:Z

    const/4 v0, 0x0

    .line 126
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->D:Z

    .line 131
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->E:Z

    .line 133
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->c:Z

    .line 155
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    .line 156
    new-instance p1, Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Lcom/bytedance/sdk/openadsdk/utils/am;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/openadsdk/utils/am$a;)V

    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->e:Lcom/bytedance/sdk/openadsdk/utils/am;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/x;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->B:Ljava/util/List;

    return-object p1
.end method

.method private a(Lcom/bytedance/sdk/openadsdk/core/x$a;Lorg/json/JSONObject;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 1086
    :cond_0
    :try_start_0
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/x$5;

    invoke-direct {v1, p0, p2, p1}, Lcom/bytedance/sdk/openadsdk/core/x$5;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/core/x$a;)V

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/g/c;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/x;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->w()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/x;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->c(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/x;Lorg/json/JSONObject;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/x;->h(Lorg/json/JSONObject;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1

    .line 640
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->F:Lcom/bytedance/sdk/openadsdk/c/p;

    if-eqz v0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    if-eqz p2, :cond_1

    .line 644
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->F:Lcom/bytedance/sdk/openadsdk/c/p;

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/c/p;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 646
    :cond_1
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->F:Lcom/bytedance/sdk/openadsdk/c/p;

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/c/p;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    :goto_1
    return-void
.end method

.method private a(Lorg/json/JSONObject;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 388
    new-instance p2, Lorg/json/JSONArray;

    invoke-direct {p2}, Lorg/json/JSONArray;-><init>()V

    .line 389
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->r()Ljava/util/List;

    move-result-object v0

    .line 390
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 391
    invoke-virtual {p2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :cond_0
    const-string v0, "appName"

    .line 393
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "innerAppName"

    .line 394
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/a/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "aid"

    .line 395
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/a/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "sdkEdition"

    .line 396
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "appVersion"

    .line 397
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/a/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "netType"

    .line 398
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/a/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "supportList"

    .line 399
    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p2, "deviceId"

    .line 400
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/a/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private a(Ljava/lang/String;ILcom/bytedance/sdk/openadsdk/core/d/j;)Z
    .locals 2

    .line 1140
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->C:Ljava/util/HashMap;

    if-nez v0, :cond_0

    goto :goto_0

    .line 1143
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->C:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/j;

    if-eqz p1, :cond_1

    .line 1145
    invoke-virtual {p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/j;->a(ILcom/bytedance/sdk/openadsdk/core/d/j;)V

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1

    :cond_2
    :goto_0
    return v1
.end method

.method public static b(Ljava/util/List;)Lorg/json/JSONArray;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/d/l;",
            ">;)",
            "Lorg/json/JSONArray;"
        }
    .end annotation

    .line 1223
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    .line 1227
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 1229
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 1230
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aF()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/x;Lorg/json/JSONObject;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/x;->i(Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/x;Lorg/json/JSONObject;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/x;->k(Lorg/json/JSONObject;)V

    return-void
.end method

.method private c(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3

    .line 1267
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "__msg_type"

    const-string v2, "callback"

    .line 1268
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "__callback_id"

    .line 1269
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string p1, "__params"

    .line 1271
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1273
    :cond_0
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->p(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private d(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3

    .line 1294
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1297
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "__msg_type"

    const-string v2, "event"

    .line 1298
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "__event_id"

    .line 1299
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz p2, :cond_1

    const-string p1, "__params"

    .line 1301
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1303
    :cond_1
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->p(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private d(Lorg/json/JSONObject;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 404
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "cid"

    .line 405
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "log_extra"

    .line 409
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 412
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "download_url"

    .line 413
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 7

    .line 1310
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {p1, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    const-string v1, "TTAndroidObject"

    .line 1311
    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1314
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_4

    .line 1316
    new-instance v3, Lcom/bytedance/sdk/openadsdk/core/x$a;

    invoke-direct {v3}, Lcom/bytedance/sdk/openadsdk/core/x$a;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1319
    :try_start_1
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v5, "__msg_type"

    const/4 v6, 0x0

    .line 1321
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/bytedance/sdk/openadsdk/core/x$a;->a:Ljava/lang/String;

    const-string v5, "__callback_id"

    .line 1322
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/bytedance/sdk/openadsdk/core/x$a;->b:Ljava/lang/String;

    const-string v5, "func"

    .line 1323
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/bytedance/sdk/openadsdk/core/x$a;->c:Ljava/lang/String;

    const-string v5, "params"

    .line 1324
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    iput-object v5, v3, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    const-string v5, "JSSDK"

    .line 1325
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/bytedance/sdk/openadsdk/core/x$a;->e:I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1329
    :catch_0
    :cond_0
    :try_start_2
    iget-object v4, v3, Lcom/bytedance/sdk/openadsdk/core/x$a;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, v3, Lcom/bytedance/sdk/openadsdk/core/x$a;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_1

    .line 1332
    :cond_1
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/x;->e:Lcom/bytedance/sdk/openadsdk/utils/am;

    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Lcom/bytedance/sdk/openadsdk/utils/am;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    .line 1333
    iput-object v3, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1334
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/x;->e:Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-virtual {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/am;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1337
    :catch_1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/u;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "TTAndroidObject"

    .line 1338
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to parse jsbridge msg queue "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const-string p1, "TTAndroidObject"

    const-string v0, "failed to parse jsbridge msg queue"

    .line 1340
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_2
    return-void
.end method

.method private e(Lorg/json/JSONObject;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 418
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "playable_style"

    .line 419
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    return-void
.end method

.method private f(Lorg/json/JSONObject;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    if-eqz p1, :cond_1

    .line 633
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->F:Lcom/bytedance/sdk/openadsdk/c/p;

    if-nez v0, :cond_0

    goto :goto_0

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->F:Lcom/bytedance/sdk/openadsdk/c/p;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/c/p;->b(Lorg/json/JSONObject;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private f(Ljava/lang/String;)Z
    .locals 2

    .line 1409
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "click_other"

    .line 1412
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    .line 1415
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->h()Z

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_2
    return v1
.end method

.method private g(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1494
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    if-nez v0, :cond_0

    .line 1495
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->l:I

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(I)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private g(Lorg/json/JSONObject;)V
    .locals 5

    .line 665
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->y:Lcom/bytedance/sdk/openadsdk/g/b;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 668
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->y:Lcom/bytedance/sdk/openadsdk/g/b;

    const-string v1, "isRenderSuc"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "code"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "msg"

    const-string v4, ""

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, v2, p1}, Lcom/bytedance/sdk/openadsdk/g/b;->a(ZILjava/lang/String;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private h(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "bytedance://"

    .line 1504
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const-string v0, "bytedance://dispatch_message/"

    const-string v1, "bytedance://private/setresult/"

    .line 1510
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1511
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->p()Landroid/webkit/WebView;

    move-result-object p1

    if-eqz p1, :cond_4

    const-string v0, "javascript:ToutiaoJSBridge._fetchQueue()"

    .line 1514
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/t;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_0

    .line 1516
    :cond_2
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1517
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x26

    .line 1518
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    if-gtz v1, :cond_3

    return-void

    .line 1522
    :cond_3
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    .line 1523
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "SCENE_FETCHQUEUE"

    .line 1524
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 1525
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/x;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    :goto_0
    return-void
.end method

.method private h(Lorg/json/JSONObject;)V
    .locals 2

    .line 804
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    const-string v0, "mute"

    const/4 v1, 0x0

    .line 808
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p1

    .line 809
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;->f(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private i(Lorg/json/JSONObject;)V
    .locals 2

    .line 816
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    const-string v0, "stateType"

    const/4 v1, -0x1

    .line 820
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    .line 821
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;->d(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private j(Lorg/json/JSONObject;)Z
    .locals 7

    .line 834
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 837
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;->R()J

    move-result-wide v2

    long-to-double v2, v2

    .line 838
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;->S()I

    move-result v0

    :try_start_0
    const-string v4, "currentTime"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-wide v5, 0x408f400000000000L    # 1000.0

    .line 840
    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v2, v5

    :try_start_1
    invoke-virtual {p1, v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v2, "state"

    .line 841
    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "TTAndroidObject"

    .line 842
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currentTime,state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    return v1

    :cond_1
    :goto_0
    return v1
.end method

.method private k(Lorg/json/JSONObject;)V
    .locals 34

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v2, "TTAndroidObject"

    const-string v3, "TTAndroidObject handleClickEvent"

    .line 936
    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v2, "adId"

    .line 938
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "areaType"

    const/4 v4, 0x1

    .line 939
    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "clickInfo"

    .line 940
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-wide/16 v4, 0x0

    if-eqz v1, :cond_1

    const-string v6, "down_x"

    .line 944
    invoke-virtual {v1, v6, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v6

    const-string v8, "down_y"

    .line 945
    invoke-virtual {v1, v8, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    const-string v10, "up_x"

    .line 946
    invoke-virtual {v1, v10, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v10

    const-string v12, "up_y"

    .line 947
    invoke-virtual {v1, v12, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v12

    const-string v14, "down_time"

    .line 948
    invoke-virtual {v1, v14, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v14

    move-wide/from16 v16, v6

    const-string v6, "up_time"

    .line 949
    invoke-virtual {v1, v6, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v6

    move-wide/from16 v18, v6

    const-string v6, "button_x"

    .line 950
    invoke-virtual {v1, v6, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v6

    move-wide/from16 v20, v6

    const-string v6, "button_y"

    .line 951
    invoke-virtual {v1, v6, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v6

    move-wide/from16 v22, v6

    const-string v6, "button_width"

    .line 952
    invoke-virtual {v1, v6, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v6

    move-wide/from16 v24, v6

    const-string v6, "button_height"

    .line 953
    invoke-virtual {v1, v6, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object/from16 v26, v2

    move/from16 v27, v3

    move-wide/from16 v30, v4

    move-wide/from16 v4, v16

    move-wide/from16 v6, v18

    move-wide/from16 v0, v20

    move-wide/from16 v32, v22

    move-wide/from16 v28, v24

    goto :goto_0

    :cond_1
    move-object/from16 v26, v2

    move/from16 v27, v3

    move-wide v0, v4

    move-wide v6, v0

    move-wide v8, v6

    move-wide v10, v8

    move-wide v12, v10

    move-wide v14, v12

    move-wide/from16 v28, v14

    move-wide/from16 v30, v28

    move-wide/from16 v32, v30

    .line 955
    :goto_0
    :try_start_1
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;-><init>()V

    double-to-int v3, v4

    .line 956
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->d(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v2

    double-to-int v3, v8

    .line 957
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->c(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v2

    double-to-int v3, v10

    .line 958
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->b(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v2

    double-to-int v3, v12

    .line 959
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->a(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v2

    double-to-long v3, v14

    .line 960
    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->b(J)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v2

    double-to-long v3, v6

    .line 961
    invoke-virtual {v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->a(J)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v2

    double-to-int v0, v0

    .line 962
    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->e(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v0

    move-wide/from16 v4, v32

    double-to-int v1, v4

    .line 963
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->f(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v0

    move-wide/from16 v4, v28

    double-to-int v1, v4

    .line 964
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->g(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v0

    move-wide/from16 v4, v30

    double-to-int v1, v4

    .line 965
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->h(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v0

    .line 966
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->a()Lcom/bytedance/sdk/openadsdk/core/d/j;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object/from16 v2, v26

    move/from16 v3, v27

    move-object/from16 v1, p0

    .line 968
    :try_start_2
    invoke-direct {v1, v2, v3, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;ILcom/bytedance/sdk/openadsdk/core/d/j;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 969
    iget-object v2, v1, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    if-eqz v2, :cond_2

    .line 970
    iget-object v2, v1, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    invoke-interface {v2, v3, v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;->a(ILcom/bytedance/sdk/openadsdk/core/d/j;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_0
    move-object/from16 v1, p0

    goto :goto_1

    :catch_1
    move-object v1, v0

    .line 973
    :catch_2
    :goto_1
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    if-eqz v0, :cond_2

    .line 974
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;->a(ILcom/bytedance/sdk/openadsdk/core/d/j;)V

    :cond_2
    :goto_2
    return-void
.end method

.method private l(Lorg/json/JSONObject;)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 981
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    if-eqz v2, :cond_3

    if-nez v1, :cond_0

    goto/16 :goto_3

    .line 984
    :cond_0
    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/d/o;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/core/d/o;-><init>()V

    const/4 v3, 0x1

    .line 985
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/d/o;->a(I)V

    :try_start_0
    const-string v4, "isRenderSuc"

    .line 987
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string v5, "AdSize"

    .line 988
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    const-wide/16 v6, 0x0

    if-eqz v5, :cond_1

    const-string v6, "width"

    .line 992
    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v6

    const-string v8, "height"

    .line 993
    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v8

    goto :goto_0

    :cond_1
    move-wide v8, v6

    :goto_0
    const-string v5, "videoInfo"

    .line 995
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    if-eqz v5, :cond_2

    const-string v10, "x"

    .line 1001
    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v10

    const-string v12, "y"

    .line 1002
    invoke-virtual {v5, v12}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v12

    const-string v14, "width"

    .line 1003
    invoke-virtual {v5, v14}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v14

    const-string v3, "height"

    move-wide/from16 v16, v8

    .line 1004
    invoke-virtual {v5, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 1005
    invoke-virtual {v2, v10, v11}, Lcom/bytedance/sdk/openadsdk/core/d/o;->c(D)V

    .line 1006
    invoke-virtual {v2, v12, v13}, Lcom/bytedance/sdk/openadsdk/core/d/o;->d(D)V

    .line 1007
    invoke-virtual {v2, v14, v15}, Lcom/bytedance/sdk/openadsdk/core/d/o;->e(D)V

    .line 1008
    invoke-virtual {v2, v8, v9}, Lcom/bytedance/sdk/openadsdk/core/d/o;->f(D)V

    goto :goto_1

    :cond_2
    move-wide/from16 v16, v8

    :goto_1
    const-string v3, "message"

    const/16 v5, 0x65

    .line 1010
    invoke-static {v5}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v3, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "code"

    .line 1011
    invoke-virtual {v1, v8, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 1012
    invoke-virtual {v2, v4}, Lcom/bytedance/sdk/openadsdk/core/d/o;->a(Z)V

    .line 1013
    invoke-virtual {v2, v6, v7}, Lcom/bytedance/sdk/openadsdk/core/d/o;->a(D)V

    move-wide/from16 v6, v16

    .line 1014
    invoke-virtual {v2, v6, v7}, Lcom/bytedance/sdk/openadsdk/core/d/o;->b(D)V

    .line 1015
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/core/d/o;->a(Ljava/lang/String;)V

    .line 1016
    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/core/d/o;->b(I)V

    .line 1017
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;->a(Lcom/bytedance/sdk/openadsdk/core/d/o;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    const/16 v1, 0x65

    .line 1019
    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/core/d/o;->b(I)V

    .line 1020
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/core/d/o;->a(Ljava/lang/String;)V

    .line 1021
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;->a(Lcom/bytedance/sdk/openadsdk/core/d/o;)V

    :goto_2
    return-void

    :cond_3
    :goto_3
    return-void
.end method

.method private m(Lorg/json/JSONObject;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "TTAndroidObject"

    const-string v1, "TTAndroidObject handlerDynamicTrack"

    .line 1029
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "trackData"

    .line 1031
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1032
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 1033
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bytedance"

    .line 1034
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1035
    invoke-static {p1, p0}, Lcom/bytedance/sdk/openadsdk/utils/r;->a(Landroid/net/Uri;Lcom/bytedance/sdk/openadsdk/core/x;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    return-void
.end method

.method private n(Lorg/json/JSONObject;)V
    .locals 4

    if-eqz p1, :cond_2

    .line 1043
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->s:Lcom/bytedance/sdk/openadsdk/e/d;

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "temaiProductIds"

    .line 1047
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 1048
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 1049
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->s:Lcom/bytedance/sdk/openadsdk/e/d;

    const/4 v3, 0x1

    invoke-interface {v2, v3, p1}, Lcom/bytedance/sdk/openadsdk/e/d;->a(ZLorg/json/JSONArray;)V

    goto :goto_0

    .line 1051
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->s:Lcom/bytedance/sdk/openadsdk/e/d;

    invoke-interface {p1, v1, v0}, Lcom/bytedance/sdk/openadsdk/e/d;->a(ZLorg/json/JSONArray;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1054
    :catch_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->s:Lcom/bytedance/sdk/openadsdk/e/d;

    invoke-interface {p1, v1, v0}, Lcom/bytedance/sdk/openadsdk/e/d;->a(ZLorg/json/JSONArray;)V

    :goto_0
    return-void

    :cond_2
    :goto_1
    return-void
.end method

.method private o(Lorg/json/JSONObject;)Z
    .locals 3

    const/4 v0, 0x1

    .line 1066
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->B:Ljava/util/List;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object v1

    const-string v2, "creatives"

    .line 1067
    invoke-virtual {p1, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    return v0
.end method

.method private p()Landroid/webkit/WebView;
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private p(Lorg/json/JSONObject;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 1282
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->p()Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript:ToutiaoJSBridge._handleMessageFromToutiao("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1285
    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/t;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1286
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/utils/u;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "TTAndroidObject"

    .line 1287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "js_msg "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private q()Lorg/json/JSONObject;
    .locals 9

    const/4 v0, 0x0

    .line 240
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 241
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v1, :cond_3

    if-nez v2, :cond_0

    goto :goto_1

    .line 246
    :cond_0
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/al;->b(Landroid/view/View;)[I

    move-result-object v3

    .line 247
    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/utils/al;->b(Landroid/view/View;)[I

    move-result-object v2

    if-eqz v3, :cond_2

    if-nez v2, :cond_1

    goto :goto_0

    .line 252
    :cond_1
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "x"

    .line 253
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    aget v8, v3, v7

    aget v7, v2, v7

    sub-int/2addr v8, v7

    int-to-float v7, v8

    invoke-static {v6, v7}, Lcom/bytedance/sdk/openadsdk/utils/al;->b(Landroid/content/Context;F)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v5, "y"

    .line 254
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x1

    aget v3, v3, v7

    aget v2, v2, v7

    sub-int/2addr v3, v2

    int-to-float v2, v3

    invoke-static {v6, v2}, Lcom/bytedance/sdk/openadsdk/utils/al;->b(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {v4, v5, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "w"

    .line 255
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v3, v5}, Lcom/bytedance/sdk/openadsdk/utils/al;->b(Landroid/content/Context;F)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "h"

    .line 256
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v3, v5}, Lcom/bytedance/sdk/openadsdk/utils/al;->b(Landroid/content/Context;F)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "isExist"

    .line 257
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/al;->e(Landroid/view/View;)Z

    move-result v1

    invoke-virtual {v4, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-object v4

    :cond_2
    :goto_0
    const-string v1, "TTAndroidObject"

    const-string v2, "setCloseButtonInfo error position or webViewPosition is null"

    .line 249
    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_3
    :goto_1
    const-string v1, "TTAndroidObject"

    const-string v2, "setCloseButtonInfo error closeButton is null"

    .line 243
    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "TTAndroidObject"

    const-string v3, "setCloseButtonInfo error"

    .line 260
    invoke-static {v2, v3, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private q(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 4

    .line 1435
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->a:Ljava/util/Map;

    if-eqz v0, :cond_3

    if-nez p1, :cond_0

    .line 1437
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 1440
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "ad_extra_data"

    const/4 v2, 0x0

    .line 1441
    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1443
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1445
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1446
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1447
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1448
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    :cond_2
    const-string v1, "ad_extra_data"

    .line 1450
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1452
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-object p1
.end method

.method private r()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    .line 379
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "appInfo"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "adInfo"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "getTemplateInfo"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "getTeMaiAds"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 1

    .line 651
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->z:Lcom/bytedance/sdk/openadsdk/g/h;

    if-nez v0, :cond_0

    return-void

    .line 654
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->z:Lcom/bytedance/sdk/openadsdk/g/h;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/g/h;->a()V

    return-void
.end method

.method private t()V
    .locals 1

    .line 658
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->z:Lcom/bytedance/sdk/openadsdk/g/h;

    if-nez v0, :cond_0

    return-void

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->z:Lcom/bytedance/sdk/openadsdk/g/h;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/g/h;->b()V

    return-void
.end method

.method private u()V
    .locals 1

    .line 798
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;->Q()V

    :cond_0
    return-void
.end method

.method private v()Lorg/json/JSONObject;
    .locals 7

    .line 855
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 856
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 860
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->k:Ljava/lang/String;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->d(Ljava/lang/String;)I

    move-result v1

    .line 861
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->k:Ljava/lang/String;

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/utils/ak;->c(Ljava/lang/String;)I

    move-result v2

    .line 862
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/h/h;->g(Ljava/lang/String;)I

    move-result v3

    .line 863
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->e(I)I

    move-result v4

    .line 864
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/openadsdk/core/h/h;->b(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x7

    if-eq v2, v6, :cond_1

    const/16 v6, 0x8

    if-ne v2, v6, :cond_0

    goto :goto_0

    .line 870
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->a(I)Z

    move-result v1

    goto :goto_1

    .line 868
    :cond_1
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->b(I)Z

    move-result v1

    :goto_1
    const-string v2, "voice_control"

    .line 872
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "rv_skip_time"

    .line 873
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "fv_skip_show"

    .line 874
    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "iv_skip_time"

    .line 875
    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "show_dislike"

    .line 876
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aD()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "video_adaptation"

    .line 877
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->l()I

    move-result v3

    :cond_3
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    return-object v0

    :cond_4
    return-object v0
.end method

.method private w()V
    .locals 7

    .line 1119
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->B:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_2

    .line 1122
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->C:Ljava/util/HashMap;

    const/4 v0, 0x0

    .line 1124
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->d:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    .line 1125
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1128
    :cond_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    if-nez v1, :cond_2

    goto :goto_1

    .line 1131
    :cond_2
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->B:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 1132
    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v4

    .line 1133
    new-instance v5, Lcom/bytedance/sdk/openadsdk/core/j;

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/x;->H:Ljava/lang/String;

    invoke-direct {v5, v1, v3, v0, v6}, Lcom/bytedance/sdk/openadsdk/core/j;-><init>(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Landroid/view/View;Ljava/lang/String;)V

    .line 1134
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/x;->C:Ljava/util/HashMap;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    return-void

    :cond_4
    :goto_1
    return-void

    :cond_5
    :goto_2
    return-void
.end method

.method private x()Z
    .locals 3

    .line 1236
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->at()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 1237
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->C()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->D:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 1241
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->at()Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "parent_type"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    return v1

    .line 1244
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->c(Ljava/lang/String;)I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_3

    const/4 v2, 0x7

    if-ne v0, v2, :cond_2

    goto :goto_0

    :cond_2
    return v1

    :cond_3
    :goto_0
    const/4 v0, 0x1

    .line 1247
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->D:Z

    return v0

    :cond_4
    :goto_1
    return v1
.end method

.method private y()V
    .locals 1

    .line 1589
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    if-nez v0, :cond_0

    .line 1590
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {p0, v0}, Lcom/bytedance/sdk/openadsdk/e/a;->a(Lcom/bytedance/sdk/openadsdk/e/b;Lcom/bytedance/sdk/openadsdk/core/d/l;)Lcom/bytedance/sdk/openadsdk/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 304
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->l:I

    return-object p0
.end method

.method public a(Landroid/view/View;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 1

    .line 234
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->j:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/c/p;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->F:Lcom/bytedance/sdk/openadsdk/c/p;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/a/d;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->x:Lcom/bytedance/sdk/openadsdk/core/a/d;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/d/l;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 319
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->q:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/g;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 2

    .line 165
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Landroid/webkit/WebView;)Lcom/bytedance/sdk/openadsdk/f/a/j;

    move-result-object v0

    const-string v1, "ToutiaoJSBridge"

    .line 166
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/j;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/f/a/j;

    move-result-object v0

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/x$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/x$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;)V

    .line 167
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/j;->a(Lcom/bytedance/sdk/openadsdk/f/a/l;)Lcom/bytedance/sdk/openadsdk/f/a/j;

    move-result-object v0

    .line 182
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/i;->w()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/j;->a(Z)Lcom/bytedance/sdk/openadsdk/f/a/j;

    move-result-object v0

    const/4 v1, 0x1

    .line 183
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/f/a/j;->b(Z)Lcom/bytedance/sdk/openadsdk/f/a/j;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/f/a/j;->a()Lcom/bytedance/sdk/openadsdk/f/a/j;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/f/a/j;->b()Lcom/bytedance/sdk/openadsdk/f/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    .line 188
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/f/b/f;->a(Lcom/bytedance/sdk/openadsdk/f/a/q;Lcom/bytedance/sdk/openadsdk/core/x;)V

    .line 189
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-static {v0, p0}, Lcom/bytedance/sdk/openadsdk/f/b/b;->a(Lcom/bytedance/sdk/openadsdk/f/a/q;Lcom/bytedance/sdk/openadsdk/core/x;)V

    .line 190
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/f/b/h;->a(Lcom/bytedance/sdk/openadsdk/f/a/q;)V

    .line 191
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/f/b/e;->a(Lcom/bytedance/sdk/openadsdk/f/a/q;)V

    .line 192
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/f/b/g;->a(Lcom/bytedance/sdk/openadsdk/f/a/q;Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)V

    .line 193
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-static {p1, p0}, Lcom/bytedance/sdk/openadsdk/f/b/c;->a(Lcom/bytedance/sdk/openadsdk/f/a/q;Lcom/bytedance/sdk/openadsdk/core/x;)V

    .line 194
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-static {p1, p0}, Lcom/bytedance/sdk/openadsdk/f/b/d;->a(Lcom/bytedance/sdk/openadsdk/f/a/q;Lcom/bytedance/sdk/openadsdk/core/x;)V

    .line 195
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-static {p1, p0}, Lcom/bytedance/sdk/openadsdk/f/b/i;->a(Lcom/bytedance/sdk/openadsdk/f/a/q;Lcom/bytedance/sdk/openadsdk/core/x;)V

    .line 196
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-static {p1, p0}, Lcom/bytedance/sdk/openadsdk/f/b/a;->a(Lcom/bytedance/sdk/openadsdk/f/a/q;Lcom/bytedance/sdk/openadsdk/core/x;)V

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/e/d;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->s:Lcom/bytedance/sdk/openadsdk/e/d;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/g/a;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->t:Lcom/bytedance/sdk/openadsdk/g/a;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/g/b;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->y:Lcom/bytedance/sdk/openadsdk/g/b;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/g/d;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->v:Lcom/bytedance/sdk/openadsdk/g/d;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/g/e;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 359
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->u:Lcom/bytedance/sdk/openadsdk/g/e;

    return-object p0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/g/h;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->z:Lcom/bytedance/sdk/openadsdk/g/h;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->H:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/core/d/l;",
            ">;)",
            "Lcom/bytedance/sdk/openadsdk/core/x;"
        }
    .end annotation

    .line 369
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->B:Ljava/util/List;

    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/bytedance/sdk/openadsdk/core/x;"
        }
    .end annotation

    .line 349
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->a:Ljava/util/Map;

    return-object p0
.end method

.method public a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 339
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->r:Lorg/json/JSONObject;

    return-object p0
.end method

.method public a(Z)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 219
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->b:Z

    return-object p0
.end method

.method public a()Lcom/bytedance/sdk/openadsdk/f/a/q;
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    return-object v0
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/x$a;I)Lorg/json/JSONObject;
    .locals 13
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "call"

    .line 425
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 428
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "TTAndroidObject"

    .line 429
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[JSB-REQ] version:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " method:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " params="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const-string v2, ""

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 432
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x1

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string v2, "landscape_click"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xf

    goto/16 :goto_2

    :sswitch_1
    const-string v2, "skipVideo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x13

    goto/16 :goto_2

    :sswitch_2
    const-string v2, "playable_style"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    goto/16 :goto_2

    :sswitch_3
    const-string v2, "getNetworkData"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x19

    goto/16 :goto_2

    :sswitch_4
    const-string v2, "endcard_load"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x1a

    goto/16 :goto_2

    :sswitch_5
    const-string v2, "removeLoading"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x9

    goto/16 :goto_2

    :sswitch_6
    const-string v2, "renderDidFinish"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x11

    goto/16 :goto_2

    :sswitch_7
    const-string v2, "muteVideo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x14

    goto/16 :goto_2

    :sswitch_8
    const-string v2, "pauseWebViewTimers"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x1c

    goto/16 :goto_2

    :sswitch_9
    const-string v2, "getVolume"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x8

    goto/16 :goto_2

    :sswitch_a
    const-string v2, "getCurrentVideoState"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x16

    goto/16 :goto_2

    :sswitch_b
    const-string v2, "cancel_download_app_ad"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xd

    goto/16 :goto_2

    :sswitch_c
    const-string v2, "getTemplateInfo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    goto/16 :goto_2

    :sswitch_d
    const-string v2, "dynamicTrack"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x12

    goto/16 :goto_2

    :sswitch_e
    const-string v2, "sendReward"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xa

    goto/16 :goto_2

    :sswitch_f
    const-string v2, "isViewable"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    goto/16 :goto_2

    :sswitch_10
    const-string v2, "getCloseButtonInfo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x7

    goto/16 :goto_2

    :sswitch_11
    const-string v2, "getDownloadStatus"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x1e

    goto/16 :goto_2

    :sswitch_12
    const-string v2, "unsubscribe_app_ad"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xe

    goto/16 :goto_2

    :sswitch_13
    const-string v2, "download_app_ad"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xc

    goto/16 :goto_2

    :sswitch_14
    const-string v2, "getTeMaiAds"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    goto/16 :goto_2

    :sswitch_15
    const-string v2, "send_temai_product_ids"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x17

    goto :goto_2

    :sswitch_16
    const-string v2, "getMaterialMeta"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x18

    goto :goto_2

    :sswitch_17
    const-string v2, "getScreenSize"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x6

    goto :goto_2

    :sswitch_18
    const-string v2, "appInfo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_19
    const-string v2, "clickEvent"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x10

    goto :goto_2

    :sswitch_1a
    const-string v2, "webview_time_track"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x1d

    goto :goto_2

    :sswitch_1b
    const-string v2, "changeVideoState"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x15

    goto :goto_2

    :sswitch_1c
    const-string v2, "pauseWebView"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x1b

    goto :goto_2

    :sswitch_1d
    const-string v2, "adInfo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    goto :goto_2

    :sswitch_1e
    const-string v2, "subscribe_app_ad"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xb

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v1, -0x1

    :goto_2
    packed-switch v1, :pswitch_data_0

    goto/16 :goto_3

    .line 583
    :pswitch_0
    iget-object v0, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/f/b/e;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    goto/16 :goto_3

    .line 580
    :pswitch_1
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->f(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 577
    :pswitch_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->t()V

    goto/16 :goto_3

    .line 574
    :pswitch_3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->s()V

    goto/16 :goto_3

    .line 571
    :pswitch_4
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->g(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 568
    :pswitch_5
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/x$a;Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 565
    :pswitch_6
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->o(Lorg/json/JSONObject;)Z

    goto/16 :goto_3

    .line 562
    :pswitch_7
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->n(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 559
    :pswitch_8
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->j(Lorg/json/JSONObject;)Z

    goto/16 :goto_3

    .line 556
    :pswitch_9
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->i(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 553
    :pswitch_a
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->h(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 550
    :pswitch_b
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->u()V

    goto/16 :goto_3

    .line 547
    :pswitch_c
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->m(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 544
    :pswitch_d
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->l(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 541
    :pswitch_e
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->k(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 534
    :pswitch_f
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    if-eqz v1, :cond_8

    .line 535
    instance-of v2, v1, Lcom/bytedance/sdk/openadsdk/core/video/c/b;

    if-eqz v2, :cond_8

    .line 536
    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/video/c/b;

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/b;->W()V

    goto/16 :goto_3

    .line 528
    :pswitch_10
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    if-eqz v1, :cond_8

    .line 529
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    iget-object v2, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/e/c;->a(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 523
    :pswitch_11
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    if-eqz v1, :cond_8

    .line 524
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    iget-object v2, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/e/c;->b(Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 512
    :pswitch_12
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->x:Lcom/bytedance/sdk/openadsdk/core/a/d;

    if-eqz v1, :cond_4

    .line 513
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->x:Lcom/bytedance/sdk/openadsdk/core/a/d;

    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->A:Z

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/a/d;->a(Z)V

    goto/16 :goto_3

    .line 514
    :cond_4
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    if-eqz v1, :cond_8

    .line 516
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    if-eqz v1, :cond_8

    .line 517
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    iget-object v3, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    invoke-interface {v2, v1, v3}, Lcom/bytedance/sdk/openadsdk/e/c;->a(Landroid/content/Context;Lorg/json/JSONObject;)V

    goto/16 :goto_3

    .line 504
    :pswitch_13
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->y()V

    .line 506
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/content/Context;

    if-eqz v8, :cond_8

    .line 507
    iget-object v7, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    iget-object v9, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->d:Lorg/json/JSONObject;

    iget-object v10, p0, Lcom/bytedance/sdk/openadsdk/core/x;->k:Ljava/lang/String;

    iget v11, p0, Lcom/bytedance/sdk/openadsdk/core/x;->l:I

    iget-boolean v12, p0, Lcom/bytedance/sdk/openadsdk/core/x;->o:Z

    invoke-interface/range {v7 .. v12}, Lcom/bytedance/sdk/openadsdk/e/c;->a(Landroid/content/Context;Lorg/json/JSONObject;Ljava/lang/String;IZ)V

    goto/16 :goto_3

    .line 497
    :pswitch_14
    iput-boolean v6, p0, Lcom/bytedance/sdk/openadsdk/core/x;->c:Z

    .line 498
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->u:Lcom/bytedance/sdk/openadsdk/g/e;

    if-eqz v1, :cond_8

    .line 499
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->u:Lcom/bytedance/sdk/openadsdk/g/e;

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/g/e;->a()V

    goto/16 :goto_3

    .line 491
    :pswitch_15
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->v:Lcom/bytedance/sdk/openadsdk/g/d;

    if-eqz v1, :cond_8

    .line 492
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->v:Lcom/bytedance/sdk/openadsdk/g/d;

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/g/d;->a()V

    goto/16 :goto_3

    .line 479
    :pswitch_16
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v1

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    if-eqz v1, :cond_5

    .line 482
    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v5

    const-string v1, "TTAndroidObject"

    .line 483
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u97f3\u4e50\u97f3\u91cf >>>> AudioManager-->currentVolume="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    if-gtz v5, :cond_6

    const/4 v4, 0x1

    :cond_6
    const-string v1, "endcard_mute"

    .line 487
    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    goto :goto_3

    .line 472
    :pswitch_17
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->q()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_8

    move-object v0, v1

    goto :goto_3

    .line 463
    :pswitch_18
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->t:Lcom/bytedance/sdk/openadsdk/g/a;

    if-eqz v1, :cond_8

    .line 464
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->t:Lcom/bytedance/sdk/openadsdk/g/a;

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/g/a;->b()I

    move-result v1

    .line 465
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->t:Lcom/bytedance/sdk/openadsdk/g/a;

    invoke-interface {v2}, Lcom/bytedance/sdk/openadsdk/g/a;->a()I

    move-result v2

    const-string v3, "width"

    .line 466
    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "height"

    .line 467
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_3

    :pswitch_19
    const-string v1, "viewStatus"

    .line 459
    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->E:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_3

    .line 453
    :pswitch_1a
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->w:Lorg/json/JSONObject;

    if-eqz v1, :cond_8

    .line 454
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->w:Lorg/json/JSONObject;

    goto :goto_3

    .line 446
    :pswitch_1b
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->r:Lorg/json/JSONObject;

    if-eqz v0, :cond_7

    .line 447
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->r:Lorg/json/JSONObject;

    const-string v1, "setting"

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->v()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 449
    :cond_7
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->r:Lorg/json/JSONObject;

    goto :goto_3

    .line 442
    :pswitch_1c
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->e(Lorg/json/JSONObject;)V

    goto :goto_3

    .line 438
    :pswitch_1d
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->d(Lorg/json/JSONObject;)V

    goto :goto_3

    .line 434
    :pswitch_1e
    iget v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->e:I

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lorg/json/JSONObject;I)V

    :cond_8
    :goto_3
    if-ne p2, v6, :cond_9

    .line 592
    iget-object v1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 593
    iget-object p1, p1, Lcom/bytedance/sdk/openadsdk/core/x$a;->b:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->c(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 594
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/i;->w()Z

    move-result p1

    if-eqz p1, :cond_9

    const-string p1, "TTAndroidObject"

    .line 595
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[JSB-RSP] version:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " data="

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7966d06a -> :sswitch_1e
        -0x54d5e48f -> :sswitch_1d
        -0x4f555ebd -> :sswitch_1c
        -0x45af975a -> :sswitch_1b
        -0x325352a1 -> :sswitch_1a
        -0x2fbc0e0e -> :sswitch_19
        -0x2f57a591 -> :sswitch_18
        -0x2aa0497d -> :sswitch_17
        -0x1d2a69be -> :sswitch_16
        -0x1097c80a -> :sswitch_15
        -0xa5b419e -> :sswitch_14
        0x1a8c298 -> :sswitch_13
        0x642ec2f -> :sswitch_12
        0x14fecb90 -> :sswitch_11
        0x17d08ce2 -> :sswitch_10
        0x18049cc9 -> :sswitch_f
        0x1a6244d7 -> :sswitch_e
        0x220cf04c -> :sswitch_d
        0x26c16abe -> :sswitch_c
        0x281c12d3 -> :sswitch_b
        0x2a6ab279 -> :sswitch_a
        0x34c20a10 -> :sswitch_9
        0x420130f1 -> :sswitch_8
        0x44a639e2 -> :sswitch_7
        0x49bca8fc -> :sswitch_6
        0x5b52a418 -> :sswitch_5
        0x616caa3a -> :sswitch_4
        0x66233dc2 -> :sswitch_3
        0x673944c0 -> :sswitch_2
        0x7c55d63c -> :sswitch_1
        0x7d77e304 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Message;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 1558
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 1560
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/bytedance/sdk/openadsdk/core/x$a;

    if-eqz v0, :cond_2

    .line 1562
    :try_start_0
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/x$a;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lcom/bytedance/sdk/openadsdk/core/x$a;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 2

    .line 1254
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/x$8;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/x$8;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/g/c;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0

    .line 1596
    invoke-direct {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/x;->d(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/g/c;)V
    .locals 1

    const/4 v0, 0x0

    .line 1153
    invoke-virtual {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/g/c;Lcom/bytedance/sdk/openadsdk/AdSlot;)V

    return-void
.end method

.method public a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/g/c;Lcom/bytedance/sdk/openadsdk/AdSlot;)V
    .locals 6

    if-nez p2, :cond_0

    return-void

    .line 1162
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_2

    .line 1166
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->c(Ljava/lang/String;)I

    move-result v0

    .line 1168
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->p()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    move-object p3, v1

    .line 1173
    :goto_0
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/d/m;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/core/d/m;-><init>()V

    .line 1174
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->M()Lcom/bytedance/sdk/openadsdk/core/d/l$a;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v2, 0x2

    .line 1175
    iput v2, v1, Lcom/bytedance/sdk/openadsdk/core/d/m;->e:I

    .line 1177
    :cond_3
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->at()Lorg/json/JSONObject;

    move-result-object v2

    if-nez v2, :cond_4

    .line 1179
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    :cond_4
    if-eqz p1, :cond_5

    .line 1182
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 1183
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1184
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1185
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 1188
    :cond_5
    iput-object v2, v1, Lcom/bytedance/sdk/openadsdk/core/d/m;->j:Lorg/json/JSONObject;

    .line 1189
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->f()Lcom/bytedance/sdk/openadsdk/core/q;

    move-result-object p1

    new-instance v2, Lcom/bytedance/sdk/openadsdk/core/x$7;

    invoke-direct {v2, p0, p2}, Lcom/bytedance/sdk/openadsdk/core/x$7;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;Lcom/bytedance/sdk/openadsdk/g/c;)V

    invoke-interface {p1, p3, v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/core/q;->a(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/d/m;ILcom/bytedance/sdk/openadsdk/core/q$b;)V

    goto :goto_3

    :cond_6
    :goto_2
    const/4 p1, 0x0

    const/4 p3, 0x0

    .line 1163
    invoke-interface {p2, p1, p3}, Lcom/bytedance/sdk/openadsdk/g/c;->a(ZLjava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    const-string p2, "TTAndroidObject"

    const-string p3, "get ads error"

    .line 1205
    invoke-static {p2, p3, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_3
    return-void
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    :try_start_0
    const-string v1, "bytedance"

    .line 1350
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    .line 1353
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p1

    .line 1354
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/x;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    return p1

    :catch_0
    :cond_2
    return v0
.end method

.method public adInfo()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 674
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 676
    :try_start_0
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->d(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 679
    :catch_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public appInfo()Ljava/lang/String;
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 684
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const/4 v1, 0x0

    .line 686
    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lorg/json/JSONObject;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 689
    :catch_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/widget/webview/SSWebView;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 1

    .line 209
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->d:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->i:Ljava/lang/String;

    return-object p0
.end method

.method public b(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 344
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->w:Lorg/json/JSONObject;

    return-object p0
.end method

.method public b(Z)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 224
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->E:Z

    return-object p0
.end method

.method public b()V
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/sdk/openadsdk/activity/base/TTMiddlePageActivity;

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method public b(Landroid/net/Uri;)V
    .locals 10
    .param p1    # Landroid/net/Uri;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1365
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "log_event"

    .line 1366
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "custom_event"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "log_event_v3"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    const-string v1, "private"

    .line 1398
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "dispatch_message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const-string p1, "TTAndroidObject"

    const-string v0, "handlrUir: not match schema host"

    .line 1401
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1399
    :cond_2
    :goto_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/x;->h(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_3
    :goto_1
    const-string v0, "category"

    .line 1367
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "tag"

    .line 1369
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "label"

    .line 1370
    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1371
    invoke-direct {p0, v3}, Lcom/bytedance/sdk/openadsdk/core/x;->f(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v2, :cond_4

    return-void

    :cond_4
    const-wide/16 v4, 0x0

    :try_start_1
    const-string v2, "value"

    .line 1376
    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-wide v6, v4

    :goto_2
    :try_start_2
    const-string v2, "ext_value"

    .line 1381
    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    move-wide v8, v4

    :goto_3
    const/4 v2, 0x0

    :try_start_3
    const-string v4, "extra"

    .line 1385
    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1386
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    if-nez v4, :cond_5

    .line 1388
    :try_start_4
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-object v2, v4

    :catch_2
    :cond_5
    :try_start_5
    const-string p1, "click"

    .line 1393
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 1394
    invoke-direct {p0, v2}, Lcom/bytedance/sdk/openadsdk/core/x;->q(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1

    goto :goto_4

    :cond_6
    move-object p1, v2

    .line 1396
    :goto_4
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-wide v4, v6

    move-wide v6, v8

    move-object v8, p1

    .line 1397
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLorg/json/JSONObject;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_5

    :catch_3
    move-exception p1

    const-string v0, "TTAndroidObject"

    const-string v1, "handleUri exception: "

    .line 1404
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_5
    return-void
.end method

.method public b(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1

    .line 1602
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    if-eqz v0, :cond_0

    .line 1603
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->G:Lcom/bytedance/sdk/openadsdk/f/a/q;

    invoke-virtual {v0, p1, p2}, Lcom/bytedance/sdk/openadsdk/f/a/q;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "TTAndroidObject"

    const-string v0, "sendJsMsg2020 error"

    .line 1606
    invoke-static {p2, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method public c()Lcom/bytedance/sdk/openadsdk/core/d/l;
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->k:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 334
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->A:Z

    return-object p0
.end method

.method public c(Lorg/json/JSONObject;)V
    .locals 35

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v2, "TTAndroidObject"

    const-string v3, "TTAndroidObject handleNewClickEvent"

    .line 888
    invoke-static {v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    :try_start_0
    const-string v3, "adId"

    .line 890
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "areaType"

    .line 891
    invoke-virtual {v1, v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v5, "downloadDialogStatus"

    const/4 v6, 0x2

    .line 892
    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v2, :cond_1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    :goto_0
    const-string v6, "clickInfo"

    .line 894
    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-wide/16 v6, 0x0

    if-eqz v1, :cond_2

    const-string v8, "down_x"

    .line 898
    invoke-virtual {v1, v8, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v8

    const-string v10, "down_y"

    .line 899
    invoke-virtual {v1, v10, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v10

    const-string v12, "up_x"

    .line 900
    invoke-virtual {v1, v12, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v12

    const-string v14, "up_y"

    .line 901
    invoke-virtual {v1, v14, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v14

    const-string v2, "down_time"

    .line 902
    invoke-virtual {v1, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v16

    const-string v2, "up_time"

    .line 903
    invoke-virtual {v1, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v18

    const-string v2, "button_x"

    .line 904
    invoke-virtual {v1, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v20

    const-string v2, "button_y"

    .line 905
    invoke-virtual {v1, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v22

    const-string v2, "button_width"

    .line 906
    invoke-virtual {v1, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v24

    const-string v2, "button_height"

    .line 907
    invoke-virtual {v1, v2, v6, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v6

    move-object/from16 v26, v3

    move/from16 v27, v4

    move/from16 v34, v5

    move-wide/from16 v32, v6

    move-wide v6, v8

    move-wide/from16 v1, v16

    move-wide/from16 v8, v18

    move-wide/from16 v3, v20

    move-wide/from16 v28, v22

    move-wide/from16 v30, v24

    goto :goto_1

    :cond_2
    move-object/from16 v26, v3

    move/from16 v27, v4

    move/from16 v34, v5

    move-wide v1, v6

    move-wide v3, v1

    move-wide v8, v3

    move-wide v10, v8

    move-wide v12, v10

    move-wide v14, v12

    move-wide/from16 v28, v14

    move-wide/from16 v30, v28

    move-wide/from16 v32, v30

    .line 909
    :goto_1
    new-instance v5, Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    invoke-direct {v5}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;-><init>()V

    double-to-int v6, v6

    .line 910
    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->d(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v5

    double-to-int v6, v10

    .line 911
    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->c(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v5

    double-to-int v6, v12

    .line 912
    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->b(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v5

    double-to-int v6, v14

    .line 913
    invoke-virtual {v5, v6}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->a(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v5

    double-to-long v1, v1

    .line 914
    invoke-virtual {v5, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->b(J)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v1

    double-to-long v5, v8

    .line 915
    invoke-virtual {v1, v5, v6}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->a(J)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v1

    double-to-int v2, v3

    .line 916
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->e(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v1

    move-wide/from16 v6, v28

    double-to-int v2, v6

    .line 917
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->f(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v1

    move-wide/from16 v6, v30

    double-to-int v2, v6

    .line 918
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->g(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v1

    move-wide/from16 v6, v32

    double-to-int v2, v6

    .line 919
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->h(I)Lcom/bytedance/sdk/openadsdk/core/d/j$a;

    move-result-object v1

    .line 920
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/j$a;->a()Lcom/bytedance/sdk/openadsdk/core/d/j;

    move-result-object v1

    .line 921
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    if-eqz v2, :cond_3

    .line 922
    iget-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    const/4 v3, 0x1

    xor-int/lit8 v4, v34, 0x1

    move/from16 v3, v27

    invoke-interface {v2, v3, v1, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;->a(ILcom/bytedance/sdk/openadsdk/core/d/j;Z)V

    goto :goto_2

    :cond_3
    move/from16 v3, v27

    :goto_2
    move-object/from16 v2, v26

    .line 924
    invoke-direct {v0, v2, v3, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;ILcom/bytedance/sdk/openadsdk/core/d/j;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 926
    :catch_0
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    if-eqz v1, :cond_4

    .line 927
    iget-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/x;->p:Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/nativeexpress/j;->a(ILcom/bytedance/sdk/openadsdk/core/d/j;Z)V

    :cond_4
    :goto_3
    return-void
.end method

.method public changeVideoState(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 747
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 748
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 749
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->i(Lorg/json/JSONObject;)V

    goto :goto_0

    .line 752
    :cond_0
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/x$3;

    invoke-direct {v1, p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x$3;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;Lorg/json/JSONObject;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :goto_0
    return-void
.end method

.method public clickEvent(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 768
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 769
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 770
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->k(Lorg/json/JSONObject;)V

    goto :goto_0

    .line 773
    :cond_0
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/x$4;

    invoke-direct {v1, p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;Lorg/json/JSONObject;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :goto_0
    return-void
.end method

.method public d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/core/x;
    .locals 0

    .line 309
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->m:Ljava/lang/String;

    return-object p0
.end method

.method public d()Z
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public dynamicTrack(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 737
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 738
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->m(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public e()Z
    .locals 1

    .line 374
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->c:Z

    return v0
.end method

.method public f()V
    .locals 2

    .line 1107
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/x$6;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/x$6;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/g/c;)V

    return-void
.end method

.method public g()V
    .locals 3

    .line 1214
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1215
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->B:Ljava/util/List;

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/core/x;->b(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object v1

    const-string v2, "material"

    .line 1216
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "materialMeta"

    .line 1217
    invoke-virtual {p0, v1, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public getCurrentVideoState()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 792
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 793
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->j(Lorg/json/JSONObject;)Z

    .line 794
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTemplateInfo()Ljava/lang/String;
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "getTemplateInfo"

    const/4 v1, 0x1

    .line 694
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Z)V

    .line 696
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->r:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->r:Lorg/json/JSONObject;

    const-string v1, "setting"

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->v()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    const-string v0, "getTemplateInfo"

    const/4 v1, 0x0

    .line 699
    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/x;->a(Ljava/lang/String;Z)V

    .line 700
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->r:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const-string v0, ""

    return-object v0
.end method

.method h()Z
    .locals 3

    .line 1423
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1426
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->I()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    return v2

    :cond_1
    return v1
.end method

.method public i()V
    .locals 1

    .line 1460
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-nez v0, :cond_0

    return-void

    .line 1463
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->Q()Ljava/lang/String;

    move-result-object v0

    .line 1464
    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/g;->a(Ljava/lang/String;)V

    return-void
.end method

.method public j()V
    .locals 3

    .line 1468
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/g;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public k()Lorg/json/JSONObject;
    .locals 3

    .line 1472
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1473
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-nez v1, :cond_0

    return-object v0

    .line 1476
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/x;->n:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->Q()Ljava/lang/String;

    move-result-object v1

    .line 1478
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1480
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    goto :goto_0

    :catch_0
    move-exception v1

    .line 1482
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_0
    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 1490
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->l:I

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()V
    .locals 1

    .line 1533
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    if-eqz v0, :cond_0

    .line 1534
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/e/c;->a()V

    .line 1536
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1537
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->f()V

    :cond_1
    return-void
.end method

.method public muteVideo(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 718
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 719
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 720
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->h(Lorg/json/JSONObject;)V

    goto :goto_0

    .line 723
    :cond_0
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p1, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/x$2;

    invoke-direct {v1, p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x$2;-><init>(Lcom/bytedance/sdk/openadsdk/core/x;Lorg/json/JSONObject;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, "TTAndroidObject"

    const-string v0, ""

    .line 731
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public n()V
    .locals 1

    .line 1542
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    if-eqz v0, :cond_0

    .line 1543
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/e/c;->b()V

    :cond_0
    return-void
.end method

.method public o()V
    .locals 1

    .line 1548
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    if-eqz v0, :cond_0

    .line 1549
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/x;->h:Lcom/bytedance/sdk/openadsdk/e/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/e/c;->c()V

    :cond_0
    return-void
.end method

.method public renderDidFinish(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 709
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 710
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/x;->l(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public skipVideo()V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 787
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/x;->u()V

    return-void
.end method
