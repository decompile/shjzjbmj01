.class public abstract Lcom/bytedance/sdk/openadsdk/core/video/c/a;
.super Ljava/lang/Object;
.source "BaseVideoController.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;
.implements Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;
.implements Lcom/bytedance/sdk/openadsdk/utils/am$a;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;",
            ">;"
        }
    .end annotation
.end field

.field private E:I

.field private F:Z

.field private G:Z

.field private final H:Ljava/lang/Runnable;

.field private final I:Ljava/lang/Runnable;

.field private final J:Ljava/lang/Runnable;

.field private K:Z

.field private L:J

.field private final M:Landroid/content/BroadcastReceiver;

.field private N:I

.field private O:Z

.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field public c:J

.field protected d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected e:J

.field protected f:J

.field protected g:Z

.field protected h:Z

.field public i:J

.field private j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

.field private final k:Landroid/view/ViewGroup;

.field private final l:Lcom/bytedance/sdk/openadsdk/utils/am;

.field private m:J

.field private n:J

.field private o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

.field private p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

.field private q:J

.field private r:J

.field private s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private t:Z

.field private final u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:J


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/d/l;)V
    .locals 5

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/utils/am;-><init>(Lcom/bytedance/sdk/openadsdk/utils/am$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    const-wide/16 v0, 0x0

    .line 80
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->m:J

    .line 81
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->n:J

    .line 84
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    .line 85
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    const/4 v2, 0x0

    .line 89
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->v:Z

    .line 90
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->w:Z

    const/4 v3, 0x1

    .line 91
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->x:Z

    .line 92
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->y:Z

    .line 94
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->z:J

    .line 96
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->A:Z

    .line 97
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->B:Z

    .line 98
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->C:Z

    const/4 v4, 0x0

    .line 104
    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d:Ljava/util/Map;

    .line 107
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->e:J

    .line 108
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->f:J

    .line 110
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->g:Z

    .line 111
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->h:Z

    .line 112
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->F:Z

    .line 114
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->G:Z

    .line 339
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/a$2;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a$2;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->H:Ljava/lang/Runnable;

    .line 348
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/a$3;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a$3;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I:Ljava/lang/Runnable;

    .line 379
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/a$4;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->J:Ljava/lang/Runnable;

    .line 1161
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->K:Z

    .line 1228
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/c/a$5;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a$5;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->M:Landroid/content/BroadcastReceiver;

    .line 1240
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->N:I

    .line 1259
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->O:Z

    .line 127
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->N:I

    .line 128
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k:Landroid/view/ViewGroup;

    .line 129
    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    .line 130
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 131
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(Landroid/content/Context;)V

    .line 132
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->d(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->E:I

    .line 133
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x11

    if-lt p1, p2, :cond_0

    const/4 v2, 0x1

    :cond_0
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->u:Z

    return-void
.end method

.method private D()V
    .locals 5

    .line 360
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 364
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->E:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/h/h;->f(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x5

    goto :goto_1

    .line 362
    :cond_2
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->h()Lcom/bytedance/sdk/openadsdk/core/h/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/h/h;->H()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 366
    :goto_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 367
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I:Ljava/lang/Runnable;

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/am;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private E()V
    .locals 4

    .line 371
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->F()V

    .line 372
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->J:Ljava/lang/Runnable;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/am;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private F()V
    .locals 2

    .line 376
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->J:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private G()Z
    .locals 1

    .line 428
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private H()V
    .locals 2

    .line 451
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->s:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 454
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->s:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 455
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 456
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void

    :cond_2
    :goto_1
    return-void
.end method

.method private I()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 678
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    .line 679
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a()Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 680
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->M()Lcom/bytedance/sdk/openadsdk/core/d/l$a;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->n()I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private J()V
    .locals 13

    :try_start_0
    const-string v0, "changeVideoSize"

    .line 688
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeVideoSize start.......mMaterialMeta.getAdSlot()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->p()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "changeVideoSize"

    const-string v1, "changeVideoSize start check condition complete ... go .."

    .line 692
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/al;->b(Landroid/content/Context;)[I

    move-result-object v0

    .line 695
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a()Landroid/media/MediaPlayer;

    move-result-object v1

    .line 697
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ak()I

    move-result v2

    const/16 v3, 0xf

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 699
    :goto_0
    aget v3, v0, v4

    int-to-float v7, v3

    .line 700
    aget v0, v0, v5

    int-to-float v8, v0

    .line 701
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    int-to-float v9, v0

    .line 702
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    int-to-float v10, v0

    if-eqz v2, :cond_2

    cmpl-float v0, v9, v10

    if-lez v0, :cond_3

    const-string v0, "changeVideoSize"

    const-string v1, "\u6a2a\u8f6c\u7ad6\u5c4f\u5355\u72ec\u9002\u914d....."

    .line 707
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v11, 0x1

    move-object v6, p0

    .line 708
    invoke-direct/range {v6 .. v11}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(FFFFZ)V

    return-void

    :cond_2
    cmpg-float v0, v9, v10

    if-gez v0, :cond_3

    const-string v0, "changeVideoSize"

    const-string v1, "\u7ad6\u5c4f\u8f6c\u6a2a\u5355\u72ec\u9002\u914d....."

    .line 714
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v11, 0x0

    move-object v6, p0

    .line 715
    invoke-direct/range {v6 .. v11}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(FFFFZ)V

    return-void

    :cond_3
    div-float v0, v9, v10

    div-float v1, v7, v8

    const-string v3, "changeVideoSize"

    .line 723
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "screenHeight="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v11, ",screenWidth="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "changeVideoSize"

    .line 724
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "videoHeight="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v11, ",videoWidth="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "changeVideoSize"

    .line 725
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "\u89c6\u9891\u5bbd\u9ad8\u6bd4,videoScale="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v11, ",\u5c4f\u5e55\u5bbd\u9ad8\u6bd4.screenScale="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v11, ",VERTICAL_SCALE(9:16)="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/high16 v11, 0x3f100000    # 0.5625f

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v12, ",HORIZONTAL_SCALE(16:9) ="

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v12, 0x3fe38e39

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/high16 v3, 0x41800000    # 16.0f

    const/high16 v6, 0x41100000    # 9.0f

    if-eqz v2, :cond_4

    cmpg-float v1, v1, v11

    if-gez v1, :cond_5

    cmpl-float v0, v0, v11

    if-nez v0, :cond_5

    mul-float v6, v6, v8

    div-float v0, v6, v3

    move v9, v0

    move v0, v8

    goto :goto_1

    :cond_4
    cmpl-float v1, v1, v12

    if-lez v1, :cond_5

    cmpl-float v0, v0, v12

    if-nez v0, :cond_5

    mul-float v6, v6, v7

    div-float v0, v6, v3

    move v9, v7

    goto :goto_1

    :cond_5
    move v0, v10

    const/4 v5, 0x0

    :goto_1
    const-string v1, "changeVideoSize"

    .line 745
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u9002\u914d\u540e\u5bbd\u9ad8\uff1avideoHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v3, ",videoWidth="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v5, :cond_6

    const-string v0, "changeVideoSize"

    .line 751
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " \u5c4f\u5e55\u6bd4\u4f8b\u548c\u89c6\u9891\u6bd4\u4f8b\u76f8\u540c\uff0c\u4ee5\u53ca\u5176\u4ed6\u60c5\u51b5\u90fd\u6309\u7167\u5c4f\u5e55\u5bbd\u9ad8\u64ad\u653e\uff0cvideoHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "\uff0cvideoWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    goto :goto_2

    :cond_6
    move v7, v9

    .line 755
    :goto_2
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int v2, v7

    float-to-int v0, v0

    invoke-direct {v1, v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xd

    .line 756
    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 758
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 759
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v3

    instance-of v3, v3, Landroid/view/TextureView;

    if-eqz v3, :cond_7

    .line 760
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v3

    check-cast v3, Landroid/view/TextureView;

    invoke-virtual {v3, v1}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 761
    :cond_7
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v3

    instance-of v3, v3, Landroid/view/SurfaceView;

    if-eqz v3, :cond_8

    .line 762
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v3

    check-cast v3, Landroid/view/SurfaceView;

    invoke-virtual {v3, v1}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 767
    :cond_8
    :goto_3
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 769
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 770
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 771
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_9
    const-string v0, "changeVideoSize"

    const-string v1, "changeVideoSize .... complete ... end !!!"

    .line 774
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    const-string v1, "changeVideoSize"

    const-string v2, "changeSize error"

    .line 776
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_4
    return-void
.end method

.method private K()V
    .locals 10

    .line 785
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    .line 786
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a()Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-nez v0, :cond_0

    goto :goto_1

    .line 789
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ak()I

    move-result v0

    const/16 v1, 0xf

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_1

    const/4 v9, 0x1

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    .line 790
    :goto_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/al;->b(Landroid/content/Context;)[I

    move-result-object v0

    .line 791
    aget v1, v0, v2

    int-to-float v5, v1

    .line 792
    aget v0, v0, v3

    int-to-float v6, v0

    .line 794
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a()Landroid/media/MediaPlayer;

    move-result-object v0

    .line 795
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    int-to-float v7, v1

    .line 796
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    int-to-float v8, v0

    move-object v4, p0

    .line 797
    invoke-direct/range {v4 .. v9}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(FFFFZ)V

    const-string v0, "changeVideoSize"

    const-string v1, "changeSize=end"

    .line 798
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "changeVideoSize"

    const-string v2, "changeSize error"

    .line 800
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    return-void
.end method

.method private L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;
    .locals 1

    .line 1082
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1083
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 1084
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->o()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private M()V
    .locals 2

    .line 1179
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 1180
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(I)V

    .line 1181
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, v1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(ZZ)V

    .line 1182
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Z)V

    .line 1183
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b()V

    .line 1184
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->d()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/c/a;J)J
    .locals 0

    .line 59
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->m:J

    return-wide p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    return-object p0
.end method

.method private a(FFFFZ)V
    .locals 3

    :try_start_0
    const-string v0, "changeVideoSize"

    .line 815
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "screenWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ",screenHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "changeVideoSize"

    .line 816
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "videoHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ",videoWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    cmpg-float v1, p3, v0

    if-lez v1, :cond_0

    cmpg-float v1, p4, v0

    if-gtz v1, :cond_1

    .line 820
    :cond_0
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object p3

    invoke-virtual {p3}, Lcom/bytedance/sdk/openadsdk/core/d/u;->c()I

    move-result p3

    int-to-float p3, p3

    .line 821
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object p4

    invoke-virtual {p4}, Lcom/bytedance/sdk/openadsdk/core/d/u;->b()I

    move-result p4

    int-to-float p4, p4

    :cond_1
    cmpg-float v1, p4, v0

    if-lez v1, :cond_7

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_2

    goto :goto_1

    :cond_2
    const/16 v0, 0xd

    if-eqz p5, :cond_4

    cmpg-float p2, p3, p4

    if-gez p2, :cond_3

    return-void

    :cond_3
    const-string p2, "changeVideoSize"

    const-string p5, "\u7ad6\u5c4f\u6a21\u5f0f\u4e0b\u6309\u89c6\u9891\u5bbd\u5ea6\u8ba1\u7b97\u653e\u5927\u500d\u6570\u503c"

    .line 834
    invoke-static {p2, p5}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    mul-float p4, p4, p1

    div-float/2addr p4, p3

    .line 839
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int p1, p1

    float-to-int p3, p4

    invoke-direct {p2, p1, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 840
    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    move-object p1, p2

    goto :goto_0

    :cond_4
    cmpl-float p1, p3, p4

    if-lez p1, :cond_5

    return-void

    :cond_5
    const-string p1, "changeVideoSize"

    const-string p5, "\u6a2a\u5c4f\u6a21\u5f0f\u4e0b\u6309\u89c6\u9891\u9ad8\u5ea6\u8ba1\u7b97\u653e\u5927\u500d\u6570\u503c"

    .line 846
    invoke-static {p1, p5}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    mul-float p3, p3, p2

    div-float/2addr p3, p4

    .line 851
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int p3, p3

    float-to-int p2, p2

    invoke-direct {p1, p3, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 852
    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 855
    :goto_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p2

    if-eqz p2, :cond_8

    .line 856
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p2

    instance-of p2, p2, Landroid/view/TextureView;

    if-eqz p2, :cond_6

    .line 857
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p2

    check-cast p2, Landroid/view/TextureView;

    invoke-virtual {p2, p1}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 858
    :cond_6
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p2

    instance-of p2, p2, Landroid/view/SurfaceView;

    if-eqz p2, :cond_8

    .line 859
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object p2

    check-cast p2, Landroid/view/SurfaceView;

    invoke-virtual {p2, p1}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :cond_7
    :goto_1
    return-void

    :catch_0
    move-exception p1

    const-string p2, "changeVideoSize"

    const-string p3, "changeSize error"

    .line 863
    invoke-static {p2, p3, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_8
    :goto_2
    return-void
.end method

.method private a(JJ)V
    .locals 2

    .line 933
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    .line 934
    iput-wide p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    .line 935
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(JJ)V

    .line 936
    invoke-static {p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v0

    .line 937
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(I)V

    .line 939
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    if-eqz v0, :cond_0

    .line 940
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;->a(JJ)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "BaseVideoController"

    const-string p3, "onProgressUpdate error: "

    .line 943
    invoke-static {p2, p3, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method private a(JZ)V
    .locals 1

    .line 1165
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p3, :cond_1

    .line 1169
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->M()V

    .line 1171
    :cond_1
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p3, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(J)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 118
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v5

    .line 119
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-virtual {v5, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 120
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->e:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-virtual {v5, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 121
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 122
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 121
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const-string v3, "tt_video_play_layout_for_live"

    .line 122
    invoke-static {p1, v3}, Lcom/bytedance/sdk/openadsdk/utils/ad;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const/4 v4, 0x1

    move-object v1, v0

    move-object v7, p0

    invoke-direct/range {v1 .. v7}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;-><init>(Landroid/content/Context;Landroid/view/View;ZLjava/util/EnumSet;Lcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    .line 123
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/a;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/c/a;Landroid/content/Context;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b(Landroid/content/Context;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->t:Z

    if-eqz v0, :cond_1

    .line 437
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 439
    :cond_1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_2

    .line 301
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;-><init>()V

    .line 302
    iput-object p1, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Ljava/lang/String;

    .line 303
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v1, :cond_1

    .line 304
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 305
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/u;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:Ljava/lang/String;

    .line 307
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->d(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/String;

    :cond_1
    const/4 v1, 0x1

    .line 309
    iput v1, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:I

    .line 310
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V

    .line 312
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->m:J

    .line 313
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 314
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->d(I)V

    .line 315
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->d(I)V

    .line 317
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/video/c/a$1;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)V

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(Ljava/lang/Runnable;)V

    :cond_3
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)Lcom/bytedance/sdk/openadsdk/core/video/d/d;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    return-object p0
.end method

.method private b(I)V
    .locals 6

    .line 399
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->G()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 402
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-nez p1, :cond_1

    return-void

    .line 405
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 406
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    .line 407
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->m:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->n:J

    .line 408
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    if-eqz p1, :cond_2

    .line 409
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->n:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    invoke-static {v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v2

    invoke-interface {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;->a(JI)V

    .line 413
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->d(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_3

    .line 414
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1, v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/ref/WeakReference;Z)V

    .line 417
    :cond_3
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->w:Z

    if-nez p1, :cond_4

    .line 418
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b()V

    .line 419
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->w:Z

    .line 420
    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(JJ)V

    .line 421
    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    .line 423
    :cond_4
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->C:Z

    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 1

    .line 1243
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->G()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1246
    :cond_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result p1

    .line 1248
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->N:I

    if-ne v0, p1, :cond_1

    return-void

    .line 1252
    :cond_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->B:Z

    if-nez v0, :cond_2

    const/4 v0, 0x2

    .line 1253
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d(I)Z

    .line 1256
    :cond_2
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->N:I

    return-void
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->s:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 445
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->s:Ljava/util/ArrayList;

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)J
    .locals 2

    .line 59
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    return-wide v0
.end method

.method private c(I)Z
    .locals 1

    .line 1175
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(I)Z

    move-result p1

    return p1
.end method

.method private d(I)Z
    .locals 4

    .line 1208
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x1

    if-eq v0, v2, :cond_0

    if-eqz v0, :cond_0

    .line 1210
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->h()V

    .line 1211
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->A:Z

    .line 1212
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->B:Z

    .line 1213
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_1

    .line 1214
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(ILcom/bytedance/sdk/openadsdk/core/d/u;Z)Z

    move-result p1

    return p1

    :cond_0
    if-ne v0, v2, :cond_1

    .line 1217
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->A:Z

    .line 1218
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_1

    .line 1219
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->q()V

    :cond_1
    return v3
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)Z
    .locals 0

    .line 59
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->y:Z

    return p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)Lcom/bytedance/sdk/openadsdk/utils/am;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)Ljava/lang/Runnable;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->H:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->E()V

    return-void
.end method

.method private g(Z)V
    .locals 0

    .line 1059
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->K:Z

    return-void
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/core/video/c/a;)Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    return-object p0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .line 1158
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected B()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1298
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1299
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 1300
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1301
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1305
    :cond_0
    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->f:J

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(JLcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/core/video/d/d;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1307
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1308
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    return-object v0
.end method

.method protected C()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1316
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;JLcom/bytedance/sdk/openadsdk/core/video/d/d;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1318
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1319
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1322
    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d:Ljava/util/Map;

    if-eqz v1, :cond_1

    .line 1323
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1324
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    return-object v0
.end method

.method protected abstract a()I
.end method

.method public a(I)V
    .locals 3

    .line 986
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->G()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 991
    :goto_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 992
    instance-of v2, v1, Landroid/app/Activity;

    if-nez v2, :cond_3

    return-void

    .line 995
    :cond_3
    check-cast v1, Landroid/app/Activity;

    .line 998
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/16 p1, 0x400

    if-nez v0, :cond_4

    .line 1002
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1, p1}, Landroid/view/Window;->setFlags(II)V

    goto :goto_2

    .line 1004
    :cond_4
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->clearFlags(I)V

    :goto_2
    return-void
.end method

.method protected abstract a(II)V
.end method

.method public a(J)V
    .locals 3

    .line 195
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    .line 196
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    goto :goto_0

    :cond_0
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    :goto_0
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 6

    .line 577
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_d

    if-eqz p1, :cond_d

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    .line 578
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 581
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x0

    const/16 v2, 0x134

    if-eq v0, v2, :cond_b

    const/16 v2, 0x137

    if-eq v0, v2, :cond_9

    const/16 v2, 0x13a

    if-eq v0, v2, :cond_8

    packed-switch v0, :pswitch_data_0

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_3

    .line 659
    :pswitch_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 660
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_c

    .line 661
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    goto/16 :goto_3

    .line 622
    :pswitch_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    if-eqz p1, :cond_1

    .line 623
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 625
    :cond_1
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->u:Z

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->v:Z

    if-nez p1, :cond_2

    .line 626
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->e:J

    sub-long/2addr v0, v3

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->f:J

    .line 627
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->f()V

    .line 628
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->v:Z

    .line 630
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_c

    .line 631
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    goto/16 :goto_3

    .line 602
    :pswitch_2
    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 603
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v3, 0x3

    if-eqz v0, :cond_5

    if-eq p1, v3, :cond_4

    const/16 v0, 0x2be

    if-ne p1, v0, :cond_3

    goto :goto_0

    :cond_3
    const/16 v0, 0x2bd

    if-ne p1, v0, :cond_5

    .line 609
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->t()V

    .line 610
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->D()V

    .line 611
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->F:Z

    goto :goto_1

    .line 605
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    .line 606
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I:Ljava/lang/Runnable;

    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 607
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->F:Z

    .line 614
    :cond_5
    :goto_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->u:Z

    if-eqz v0, :cond_c

    if-ne p1, v3, :cond_c

    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->v:Z

    if-nez p1, :cond_c

    .line 615
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->e:J

    sub-long/2addr v0, v3

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->f:J

    .line 616
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->e()V

    .line 617
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->v:Z

    .line 618
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->h:Z

    goto/16 :goto_3

    .line 646
    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 647
    iget p1, p1, Landroid/os/Message;->arg2:I

    .line 648
    invoke-virtual {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(II)V

    .line 650
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 651
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_6

    .line 652
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    .line 654
    :cond_6
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    if-eqz p1, :cond_c

    .line 655
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->n:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    invoke-static {v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v2

    invoke-interface {p1, v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;->b(JI)V

    goto/16 :goto_3

    .line 592
    :pswitch_4
    iget p1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b(I)V

    goto :goto_3

    .line 635
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 636
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    .line 637
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    cmp-long p1, v0, v2

    if-lez p1, :cond_7

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    goto :goto_2

    :cond_7
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    :goto_2
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    .line 638
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(JJ)V

    goto :goto_3

    .line 595
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 596
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_c

    .line 597
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    goto :goto_3

    .line 588
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->e:J

    goto :goto_3

    .line 668
    :cond_9
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz p1, :cond_a

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->l()I

    move-result p1

    if-nez p1, :cond_a

    .line 669
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->K()V

    goto :goto_3

    .line 671
    :cond_a
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->J()V

    goto :goto_3

    .line 642
    :cond_b
    invoke-virtual {p0, v2, v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(II)V

    :cond_c
    :goto_3
    return-void

    :cond_d
    :goto_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x6c
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x12e
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;I)V
    .locals 2

    .line 898
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez p1, :cond_0

    return-void

    .line 901
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->E()V

    .line 902
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L:J

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c(I)Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(JZ)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;IZ)V
    .locals 4

    .line 917
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->G()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 920
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    int-to-long p2, p2

    .line 921
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    mul-long p2, p2, v0

    long-to-float p2, p2

    const/high16 p3, 0x3f800000    # 1.0f

    mul-float p2, p2, p3

    const-string p3, "tt_video_progress_max"

    invoke-static {p1, p3}, Lcom/bytedance/sdk/openadsdk/utils/ad;->l(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p2, p1

    float-to-long p1, p2

    .line 922
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    const-wide/16 v2, 0x0

    cmp-long p3, v0, v2

    if-lez p3, :cond_1

    long-to-int p1, p1

    int-to-long p1, p1

    .line 923
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L:J

    goto :goto_0

    .line 925
    :cond_1
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L:J

    .line 927
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_2

    .line 928
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-wide p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->L:J

    invoke-virtual {p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(J)V

    :cond_2
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/graphics/SurfaceTexture;)V
    .locals 1

    const/4 p1, 0x1

    .line 1106
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->t:Z

    .line 1108
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez v0, :cond_0

    return-void

    .line 1111
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_1

    .line 1112
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Z)V

    .line 1114
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Landroid/graphics/SurfaceTexture;)V

    .line 1115
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->H()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/SurfaceHolder;)V
    .locals 1

    const/4 p1, 0x1

    .line 1070
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->t:Z

    .line 1071
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez v0, :cond_0

    return-void

    .line 1074
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_1

    .line 1075
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Z)V

    .line 1077
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Landroid/view/SurfaceHolder;)V

    .line 1078
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->H()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 2

    .line 871
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->G()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_1

    .line 874
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g()Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    .line 875
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->h()V

    .line 876
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(ZZ)V

    .line 877
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c()V

    goto :goto_0

    .line 879
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i()Z

    move-result p1

    if-nez p1, :cond_3

    .line 880
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_2

    .line 881
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(Landroid/view/ViewGroup;)V

    .line 883
    :cond_2
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d(J)V

    .line 884
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_4

    .line 885
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p2, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(ZZ)V

    goto :goto_0

    .line 888
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k()V

    .line 889
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_4

    .line 890
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p2, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(ZZ)V

    :cond_4
    :goto_0
    return-void

    :cond_5
    :goto_1
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;Z)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;ZZ)V
    .locals 1

    .line 1020
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->x:Z

    if-eqz p1, :cond_0

    .line 1021
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->h()V

    :cond_0
    if-eqz p3, :cond_1

    .line 1023
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->x:Z

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->z()Z

    move-result p1

    if-nez p1, :cond_1

    .line 1024
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->A()Z

    move-result p2

    const/4 p3, 0x1

    xor-int/2addr p2, p3

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(ZZ)V

    .line 1025
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p4, p3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(ZZZ)V

    .line 1027
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1028
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c()V

    .line 1029
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b()V

    goto :goto_0

    .line 1031
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c()V

    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;)V
    .locals 0

    .line 463
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;)V
    .locals 1

    .line 394
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->D:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/widget/h$a;Ljava/lang/String;)V
    .locals 0

    .line 1191
    sget-object p2, Lcom/bytedance/sdk/openadsdk/core/video/c/a$6;->a:[I

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/h$a;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1199
    :pswitch_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k()V

    const/4 p1, 0x0

    .line 1200
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->A:Z

    .line 1201
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->B:Z

    goto :goto_0

    .line 1196
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(Z)V

    goto :goto_0

    .line 1193
    :pswitch_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->h()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 295
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d:Ljava/util/Map;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 567
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l()V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/lang/String;JZ)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "JZ)Z"
        }
    .end annotation

    const-string p2, "BaseVideoController"

    .line 140
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string p6, "video local url "

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    invoke-static {p2, p5}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 p5, 0x0

    if-eqz p2, :cond_0

    const-string p1, "BaseVideoController"

    const-string p2, "No video info"

    .line 142
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    return p5

    :cond_0
    const-string p2, "http"

    .line 145
    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p2

    const/4 p6, 0x1

    xor-int/2addr p2, p6

    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->g:Z

    .line 146
    iput-boolean p9, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->y:Z

    const-wide/16 v0, 0x0

    cmp-long p2, p7, v0

    if-lez p2, :cond_2

    .line 148
    iput-wide p7, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    .line 149
    iget-wide p7, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    cmp-long p2, p7, v2

    if-lez p2, :cond_1

    iget-wide p7, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    goto :goto_0

    :cond_1
    iget-wide p7, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    :goto_0
    iput-wide p7, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    .line 151
    :cond_2
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p2, :cond_3

    .line 152
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->e()V

    .line 154
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->d()V

    .line 155
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(II)V

    .line 156
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k:Landroid/view/ViewGroup;

    invoke-virtual {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(Landroid/view/ViewGroup;)V

    .line 160
    :cond_3
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez p2, :cond_4

    .line 161
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-direct {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;-><init>(Landroid/os/Handler;)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    .line 164
    :cond_4
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->n:J

    .line 166
    :try_start_0
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p6

    :catch_0
    return p5
.end method

.method protected abstract b()V
.end method

.method public b(J)V
    .locals 0

    .line 220
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->z:J

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;I)V
    .locals 0

    .line 907
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_0

    .line 908
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->F()V

    .line 910
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_1

    .line 911
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c()V

    :cond_1
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/graphics/SurfaceTexture;)V
    .locals 0

    const/4 p1, 0x0

    .line 1120
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->t:Z

    .line 1121
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p2, :cond_0

    .line 1122
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Z)V

    :cond_0
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/SurfaceHolder;)V
    .locals 0

    const/4 p1, 0x0

    .line 1097
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->t:Z

    .line 1098
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p2, :cond_0

    .line 1099
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Z)V

    :cond_0
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 949
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;ZZ)V

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;ZZ)V
    .locals 0

    .line 954
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->G()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 957
    :cond_0
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->K:Z

    const/4 p2, 0x1

    xor-int/2addr p1, p2

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->g(Z)V

    .line 958
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Landroid/app/Activity;

    if-nez p1, :cond_1

    const-string p1, "BaseVideoController"

    const-string p2, "context is not activity, not support this function."

    .line 959
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 962
    :cond_1
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->K:Z

    const/4 p4, 0x0

    if-eqz p1, :cond_3

    if-eqz p3, :cond_2

    const/16 p1, 0x8

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 963
    :goto_0
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(I)V

    .line 965
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_4

    .line 966
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k:Landroid/view/ViewGroup;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Landroid/view/ViewGroup;)V

    .line 967
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Z)V

    goto :goto_1

    .line 970
    :cond_3
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(I)V

    .line 972
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_4

    .line 973
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k:Landroid/view/ViewGroup;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Landroid/view/ViewGroup;)V

    .line 974
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Z)V

    .line 978
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->D:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->D:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_6

    .line 980
    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->K:Z

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->a(Z)V

    :cond_6
    return-void
.end method

.method public b(Z)V
    .locals 1

    .line 201
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->x:Z

    .line 202
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(Z)V

    return-void
.end method

.method protected abstract c()V
.end method

.method public c(J)V
    .locals 0

    .line 228
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    return-void
.end method

.method public c(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 0

    .line 1011
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_0

    .line 1012
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->g()V

    :cond_0
    const/4 p1, 0x1

    .line 1014
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(Z)V

    return-void
.end method

.method public c(Z)V
    .locals 1

    .line 246
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->y:Z

    .line 247
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Z)V

    :cond_0
    return-void
.end method

.method protected abstract d()V
.end method

.method public d(J)V
    .locals 3

    .line 523
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    .line 524
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    goto :goto_0

    :cond_0
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    :goto_0
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    .line 525
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_1

    .line 526
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->e()V

    .line 528
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_2

    .line 529
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->y:Z

    const/4 v2, 0x1

    xor-int/2addr p2, v2

    invoke-virtual {p1, v2, v0, v1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(ZJZ)V

    .line 530
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->E()V

    :cond_2
    return-void
.end method

.method public d(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 1

    .line 1037
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->K:Z

    const/4 p2, 0x1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 1038
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->g(Z)V

    .line 1039
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_0

    .line 1040
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->k:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Landroid/view/ViewGroup;)V

    .line 1042
    :cond_0
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(I)V

    goto :goto_0

    .line 1044
    :cond_1
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(Z)V

    :goto_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    .line 264
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->C:Z

    return-void
.end method

.method protected abstract e()V
.end method

.method public e(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 1050
    invoke-virtual {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;Z)V

    return-void
.end method

.method public e(Z)V
    .locals 0

    return-void
.end method

.method protected abstract f()V
.end method

.method public f(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public f(Z)V
    .locals 0

    .line 284
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->G:Z

    return-void
.end method

.method public g()V
    .locals 2

    .line 1142
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 1143
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b()V

    .line 1144
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->e()V

    .line 1147
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_1

    .line 1148
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->u()V

    :cond_1
    const-wide/16 v0, -0x1

    .line 1150
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d(J)V

    .line 1151
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_2

    .line 1152
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->m()V

    :cond_2
    return-void
.end method

.method public h()V
    .locals 2

    .line 474
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->i:J

    .line 475
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b()V

    .line 478
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->w:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->v:Z

    if-eqz v0, :cond_1

    .line 479
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c()V

    :cond_1
    return-void
.end method

.method public i()V
    .locals 1

    .line 489
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b()V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    .line 1292
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    .line 1293
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->d()V

    :cond_0
    return-void
.end method

.method public k()V
    .locals 5

    .line 496
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->e()V

    .line 498
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->q()V

    .line 499
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->u()V

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_1

    .line 503
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    iget-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->y:Z

    xor-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(ZJZ)V

    .line 504
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->E()V

    .line 507
    :cond_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->w:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->v:Z

    if-eqz v0, :cond_2

    .line 508
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d()V

    :cond_2
    return-void
.end method

.method public l()V
    .locals 3

    .line 539
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->c()V

    .line 541
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->g()V

    .line 547
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    if-eqz v0, :cond_2

    .line 548
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->I:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 549
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->H:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 550
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l:Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 551
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->F()V

    .line 556
    :cond_2
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->p:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    return-void
.end method

.method public m()V
    .locals 0

    .line 572
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->l()V

    return-void
.end method

.method public n()J
    .locals 2

    .line 190
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->q:J

    return-wide v0
.end method

.method public o()J
    .locals 2

    .line 233
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->n()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public p()J
    .locals 4

    .line 238
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->o()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->z:J

    add-long/2addr v0, v2

    :goto_0
    return-wide v0
.end method

.method public q()J
    .locals 4

    .line 254
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->p()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->z:J

    add-long/2addr v0, v2

    :goto_0
    return-wide v0
.end method

.method public r()I
    .locals 4

    .line 290
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->r:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v0

    return v0
.end method

.method public s()J
    .locals 2

    .line 224
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->c:J

    return-wide v0
.end method

.method public t()Z
    .locals 1

    .line 216
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->A:Z

    return v0
.end method

.method public u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    return-object v0
.end method

.method public v()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->j:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    return-object v0
.end method

.method public w()Z
    .locals 1

    .line 259
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->C:Z

    return v0
.end method

.method public x()Z
    .locals 1

    .line 279
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->F:Z

    return v0
.end method

.method public y()V
    .locals 1

    .line 516
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->w:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->v:Z

    if-eqz v0, :cond_0

    .line 517
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->d()V

    :cond_0
    return-void
.end method

.method public z()Z
    .locals 1

    .line 1136
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/c/a;->o:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->l()Z

    move-result v0

    return v0
.end method
