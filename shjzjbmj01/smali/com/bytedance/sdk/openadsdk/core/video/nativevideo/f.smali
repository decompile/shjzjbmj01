.class public Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;
.super Ljava/lang/Object;
.source "NativeVideoController.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;
.implements Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/d;
.implements Lcom/bytedance/sdk/openadsdk/utils/am$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$a;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$a;",
            ">;"
        }
    .end annotation
.end field

.field private G:I

.field private H:I

.field private I:I

.field private J:Z

.field private K:Z

.field private final L:Ljava/lang/Runnable;

.field private final M:Ljava/lang/Runnable;

.field private final N:Ljava/lang/Runnable;

.field private O:I

.field private P:J

.field private Q:J

.field private R:J

.field private S:Z

.field private T:J

.field private final U:Landroid/content/BroadcastReceiver;

.field private V:I

.field private W:Z

.field a:Ljava/lang/Runnable;

.field private b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/bytedance/sdk/openadsdk/utils/am;

.field private e:J

.field private f:J

.field private g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

.field private h:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

.field private i:J

.field private j:J

.field private k:J

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private final n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Z

.field private p:Z

.field private q:Z

.field private final r:Lcom/bytedance/sdk/openadsdk/core/d/l;

.field private s:Z

.field private t:Z

.field private u:Ljava/lang/String;

.field private v:Z

.field private w:Z

.field private x:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;",
            ">;"
        }
    .end annotation
.end field

.field private y:J

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;ZZ)V
    .locals 5

    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/utils/am;-><init>(Lcom/bytedance/sdk/openadsdk/utils/am$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    const-wide/16 v0, 0x0

    .line 72
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e:J

    .line 73
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->f:J

    .line 76
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    .line 77
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    const/4 v2, 0x0

    .line 80
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    .line 83
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p:Z

    .line 84
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->q:Z

    const/4 v3, 0x1

    .line 86
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    .line 87
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->t:Z

    const-string v4, "embeded_ad"

    .line 88
    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    .line 90
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->v:Z

    .line 91
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->w:Z

    .line 96
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->y:J

    .line 98
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z:Z

    .line 99
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A:Z

    .line 100
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->B:Z

    .line 103
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->C:Z

    .line 105
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->D:Z

    .line 112
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->G:I

    .line 113
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->H:I

    .line 114
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->I:I

    .line 115
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J:Z

    .line 117
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->K:Z

    .line 488
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$3;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$3;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->L:Ljava/lang/Runnable;

    .line 497
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$4;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->M:Ljava/lang/Runnable;

    .line 515
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$5;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$5;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->N:Ljava/lang/Runnable;

    .line 538
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->O:I

    .line 716
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->P:J

    .line 772
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$6;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$6;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a:Ljava/lang/Runnable;

    .line 785
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->Q:J

    .line 786
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->R:J

    .line 1422
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    .line 1499
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$7;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$7;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->U:Landroid/content/BroadcastReceiver;

    .line 1511
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->V:I

    .line 1572
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->W:Z

    .line 284
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->V:I

    .line 286
    :try_start_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->G:I

    .line 287
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->H:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :catch_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    .line 291
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    .line 292
    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    .line 293
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 294
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b(Landroid/content/Context;)V

    .line 296
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x11

    if-lt p1, p2, :cond_0

    const/4 v2, 0x1

    :cond_0
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->o:Z

    .line 298
    iput-boolean p5, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->v:Z

    .line 299
    iput-boolean p6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->w:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;ZZZ)V
    .locals 5

    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/utils/am;-><init>(Lcom/bytedance/sdk/openadsdk/utils/am$a;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    const-wide/16 v0, 0x0

    .line 72
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e:J

    .line 73
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->f:J

    .line 76
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    .line 77
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    const/4 v2, 0x0

    .line 80
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    .line 83
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p:Z

    .line 84
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->q:Z

    const/4 v3, 0x1

    .line 86
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    .line 87
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->t:Z

    const-string v4, "embeded_ad"

    .line 88
    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    .line 90
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->v:Z

    .line 91
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->w:Z

    .line 96
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->y:J

    .line 98
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z:Z

    .line 99
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A:Z

    .line 100
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->B:Z

    .line 103
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->C:Z

    .line 105
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->D:Z

    .line 112
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->G:I

    .line 113
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->H:I

    .line 114
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->I:I

    .line 115
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J:Z

    .line 117
    iput-boolean v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->K:Z

    .line 488
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$3;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$3;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->L:Ljava/lang/Runnable;

    .line 497
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$4;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->M:Ljava/lang/Runnable;

    .line 515
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$5;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$5;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->N:Ljava/lang/Runnable;

    .line 538
    iput v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->O:I

    .line 716
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->P:J

    .line 772
    new-instance v4, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$6;

    invoke-direct {v4, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$6;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a:Ljava/lang/Runnable;

    .line 785
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->Q:J

    .line 786
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->R:J

    .line 1422
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    .line 1499
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$7;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$7;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->U:Landroid/content/BroadcastReceiver;

    .line 1511
    iput v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->V:I

    .line 1572
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->W:Z

    .line 264
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->V:I

    .line 265
    invoke-virtual {p0, p5}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b(Z)V

    .line 266
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    .line 268
    :try_start_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result p4

    iput p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->G:I

    .line 269
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result p4

    iput p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->H:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :catch_0
    new-instance p4, Ljava/lang/ref/WeakReference;

    invoke-direct {p4, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    .line 273
    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    .line 274
    iput-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 275
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b(Landroid/content/Context;)V

    .line 277
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x11

    if-lt p1, p2, :cond_0

    const/4 v2, 0x1

    :cond_0
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->o:Z

    .line 279
    iput-boolean p6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->v:Z

    .line 280
    iput-boolean p7, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->w:Z

    return-void
.end method

.method private A()V
    .locals 4

    .line 507
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->B()V

    .line 508
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->N:Ljava/lang/Runnable;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/am;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private B()V
    .locals 2

    .line 512
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private C()Z
    .locals 1

    .line 574
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private D()V
    .locals 2

    .line 597
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->l:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 600
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->l:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 601
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 602
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 604
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    :cond_2
    :goto_1
    return-void
.end method

.method private E()V
    .locals 9

    .line 686
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    .line 687
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    iget-boolean v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->t:Z

    xor-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(ZJZ)V

    .line 688
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A()V

    .line 691
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p:Z

    if-eqz v0, :cond_1

    .line 692
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    const-string v4, "feed_continue"

    .line 693
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p()J

    move-result-wide v5

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r()I

    move-result v7

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c()Ljava/util/Map;

    move-result-object v8

    .line 692
    invoke-static/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    :cond_1
    return-void
.end method

.method private F()V
    .locals 6

    .line 790
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->Q:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->R:J

    .line 791
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p:Z

    if-nez v0, :cond_2

    .line 792
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->R:J

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(JLcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/core/video/d/d;)Ljava/util/Map;

    move-result-object v0

    .line 793
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->D:Z

    if-eqz v1, :cond_0

    .line 794
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    const-string v4, "feed_auto_play"

    invoke-static {v1, v2, v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/c/d;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 796
    :cond_0
    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-gtz v5, :cond_1

    .line 797
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    const-string v4, "feed_play"

    invoke-static {v1, v2, v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/c/d;->c(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 800
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p:Z

    :cond_2
    return-void
.end method

.method private G()V
    .locals 4

    .line 1006
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/y;->a(Landroid/view/View;II)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NativeVideoController"

    const-string v1, "onStateError \u51fa\u9519\u540e\u5c55\u793a\u7ed3\u679c\u9875\u3001\u3001\u3001\u3001\u3001\u3001\u3001"

    .line 1007
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0, v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/ref/WeakReference;Z)V

    const/4 v0, 0x1

    .line 1009
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d(Z)V

    .line 1010
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m()V

    :cond_0
    return-void
.end method

.method private H()V
    .locals 3

    const-string v0, "NativeVideoController"

    const-string v1, "before auseWhenInvisible\u3001\u3001\u3001\u3001\u3001\u3001\u3001"

    .line 1015
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/y;->a(Landroid/view/View;II)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NativeVideoController"

    const-string v1, "in pauseWhenInvisible\u3001\u3001\u3001\u3001\u3001\u3001\u3001"

    .line 1017
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i()V

    :cond_0
    return-void
.end method

.method private I()V
    .locals 8

    const-string v0, "ChangeVideoSize"

    const-string v1, "[step-0]  TAG is \'ChangeVideoSize\' ....... start  changeVideoSize >>>>>>>>>>>>>>>>>>>>>>>"

    .line 1026
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    .line 1029
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a()Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    .line 1030
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 1037
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a()Landroid/media/MediaPlayer;

    move-result-object v0

    .line 1039
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    .line 1040
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    .line 1041
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    .line 1042
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v2, :cond_a

    if-lez v3, :cond_a

    if-lez v0, :cond_a

    if-gtz v1, :cond_1

    goto/16 :goto_2

    :cond_1
    if-ne v1, v0, :cond_3

    if-le v2, v3, :cond_2

    move v0, v3

    :goto_0
    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const/high16 v6, 0x3f800000    # 1.0f

    if-le v1, v0, :cond_4

    int-to-float v1, v1

    mul-float v1, v1, v6

    int-to-float v0, v0

    div-float/2addr v1, v0

    int-to-double v6, v2

    .line 1066
    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v6, v6, v4

    float-to-double v0, v1

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v6, v0

    double-to-int v0, v6

    move v1, v2

    goto :goto_1

    :cond_4
    int-to-float v0, v0

    mul-float v0, v0, v6

    int-to-float v1, v1

    div-float/2addr v0, v1

    int-to-double v6, v3

    .line 1073
    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v6, v6, v4

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v6, v0

    double-to-int v0, v6

    move v1, v0

    move v0, v3

    :goto_1
    if-gt v0, v3, :cond_5

    if-gtz v0, :cond_6

    :cond_5
    move v0, v3

    :cond_6
    if-gt v1, v2, :cond_7

    if-gtz v1, :cond_8

    :cond_7
    move v1, v2

    .line 1085
    :cond_8
    :try_start_1
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xd

    .line 1086
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1087
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    instance-of v0, v0, Landroid/view/TextureView;

    if-eqz v0, :cond_9

    .line 1088
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    invoke-virtual {v0, v2}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v0, "ChangeVideoSize"

    const-string v1, "[step-9] >>>>> setLayoutParams to TextureView complete ! >>>>>>>"

    .line 1089
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1090
    :cond_9
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    instance-of v0, v0, Landroid/view/SurfaceView;

    if-eqz v0, :cond_f

    .line 1091
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v0, "ChangeVideoSize"

    const-string v1, "[step-9] >>>>> setLayoutParams to SurfaceView complete !>>>>>>>"

    .line 1092
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_a
    :goto_2
    const-string v0, "ChangeVideoSize"

    const-string v1, " container or video exist size <= 0"

    .line 1044
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_b
    :goto_3
    const-string v0, "ChangeVideoSize"

    .line 1031
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[step-1] >>>>> mContextRef="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ",mContextRef.get()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    goto :goto_4

    :cond_c
    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ",getIRenderView() ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ChangeVideoSize"

    .line 1032
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[step-1] >>>>> mMediaPlayerProxy == null:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_d

    const/4 v2, 0x1

    goto :goto_5

    :cond_d
    const/4 v2, 0x0

    :goto_5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ",mMediaPlayerProxy.getMediaPlayer() == null:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a()Landroid/media/MediaPlayer;

    move-result-object v2

    if-nez v2, :cond_e

    const/4 v3, 0x1

    :cond_e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "ChangeVideoSize"

    .line 1096
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[step-11] >>>>> changeVideoSize error !!!!! \uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    :goto_6
    return-void
.end method

.method private J()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;
    .locals 2

    .line 1101
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1102
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1103
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 1104
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->o()Lcom/bytedance/sdk/openadsdk/core/video/renderview/b;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private K()V
    .locals 3

    .line 1301
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1304
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h(Z)V

    .line 1305
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    const-string v0, "NativeVideoController"

    const-string v1, "context is not activity, not support this function."

    .line 1306
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 1309
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_2

    .line 1310
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Landroid/view/ViewGroup;)V

    .line 1311
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Z)V

    .line 1313
    :cond_2
    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(I)V

    .line 1314
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->x:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->x:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    .line 1316
    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->a(Z)V

    :cond_4
    return-void
.end method

.method private L()V
    .locals 2

    .line 1440
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 1441
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(I)V

    .line 1442
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, v1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(ZZ)V

    .line 1443
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Z)V

    .line 1444
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b()V

    .line 1445
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->d()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;J)J
    .locals 0

    .line 66
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e:J

    return-wide p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    return-object p0
.end method

.method private a(JJ)V
    .locals 2

    .line 1175
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    .line 1176
    iput-wide p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    .line 1177
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(JJ)V

    .line 1178
    invoke-static {p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v0

    .line 1179
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(I)V

    .line 1181
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    if-eqz v0, :cond_0

    .line 1182
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;->a(JJ)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "NativeVideoController"

    const-string p3, "onProgressUpdate error: "

    .line 1185
    invoke-static {p2, p3, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method private a(JZ)V
    .locals 1

    .line 1426
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p3, :cond_1

    .line 1430
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->L()V

    .line 1432
    :cond_1
    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p3, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(J)V

    return-void
.end method

.method private a(Landroid/content/Context;I)V
    .locals 1

    .line 1514
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 1523
    :cond_1
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->V:I

    if-ne p1, p2, :cond_2

    return-void

    .line 1527
    :cond_2
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->V:I

    const/4 p1, 0x4

    if-eq p2, p1, :cond_3

    if-eqz p2, :cond_3

    const/4 p1, 0x0

    .line 1530
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A:Z

    .line 1533
    :cond_3
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A:Z

    if-nez p1, :cond_4

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->w()Z

    move-result p1

    if-nez p1, :cond_4

    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->v:Z

    if-eqz p1, :cond_4

    const/4 p1, 0x2

    .line 1534
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d(I)Z

    .line 1537
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 1538
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$a;

    iget p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->V:I

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$a;->a(I)V

    :cond_5
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;Landroid/content/Context;)V
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d(Landroid/content/Context;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    if-eqz v0, :cond_1

    .line 583
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 585
    :cond_1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    const-string v0, "tag_video_play"

    const-string v1, "[video] NativeVideoController#playVideo has invoke !"

    .line 445
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 447
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/video/b/a;-><init>()V

    .line 448
    iput-object p1, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Ljava/lang/String;

    .line 449
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v2, :cond_1

    .line 450
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 451
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/u;->l()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:Ljava/lang/String;

    .line 453
    :cond_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/sdk/openadsdk/utils/ak;->d(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->b:Ljava/lang/String;

    .line 455
    :cond_1
    iput v1, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:I

    .line 456
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V

    const-string v0, "tag_video_play"

    const-string v2, "[video] MediaPlayerProxy has setDataSource !"

    .line 457
    invoke-static {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e:J

    .line 460
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 461
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->d(I)V

    .line 462
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->d(I)V

    .line 464
    new-instance p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$2;

    invoke-direct {p1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$2;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Ljava/lang/Runnable;)V

    .line 481
    :cond_3
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-eqz p1, :cond_4

    .line 482
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->f()V

    :cond_4
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)Lcom/bytedance/sdk/openadsdk/core/video/d/d;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    return-object p0
.end method

.method private b(I)V
    .locals 10

    .line 541
    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->O:I

    const/4 v0, 0x1

    add-int/2addr p1, v0

    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->O:I

    .line 542
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->C()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 545
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-nez p1, :cond_1

    return-void

    .line 548
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    .line 549
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    if-eqz p1, :cond_2

    .line 550
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    iget-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->f:J

    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    iget-wide v5, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    invoke-static {v3, v4, v5, v6}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v3

    invoke-interface {p1, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;->a(JI)V

    .line 552
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->f:J

    .line 553
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->b(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result p1

    const/4 v1, 0x2

    if-eqz p1, :cond_3

    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->O:I

    if-lt p1, v1, :cond_4

    .line 554
    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1, v2, v3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/ref/WeakReference;Z)V

    .line 556
    :cond_4
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->q:Z

    if-nez p1, :cond_5

    .line 557
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Landroid/content/Context;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    const-string v5, "feed_over"

    .line 558
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p()J

    move-result-wide v6

    const/16 v8, 0x64

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c()Ljava/util/Map;

    move-result-object v9

    .line 557
    invoke-static/range {v2 .. v9}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    .line 559
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->q:Z

    .line 560
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(JJ)V

    .line 561
    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    .line 563
    :cond_5
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-nez p1, :cond_6

    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    if-eqz p1, :cond_6

    .line 564
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V

    .line 566
    :cond_6
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->B:Z

    .line 567
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/ak;->b(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result p1

    if-eqz p1, :cond_7

    iget p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->O:I

    if-ge p1, v1, :cond_7

    .line 568
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g()V

    :cond_7
    return-void
.end method

.method private b(II)V
    .locals 4

    .line 986
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-nez v0, :cond_0

    return-void

    .line 990
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v1, "play_error"

    goto :goto_0

    :cond_1
    const-string v1, "play_start_error"

    .line 992
    :goto_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v3

    invoke-static {v2, p1, p2, v3}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;IILcom/bytedance/sdk/openadsdk/core/video/d/d;)Ljava/util/Map;

    move-result-object p1

    if-eqz v0, :cond_2

    const-string p2, "duration"

    .line 994
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "percent"

    .line 995
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "buffers_time"

    .line 996
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->o()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 998
    :cond_2
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/content/Context;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    invoke-static {p2, v0, v2, v1, p1}, Lcom/bytedance/sdk/openadsdk/c/d;->d(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 165
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v5

    .line 166
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->a:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-virtual {v5, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 167
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;->e:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b$a;

    invoke-virtual {v5, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 169
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-eqz v0, :cond_0

    .line 170
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    :goto_0
    move-object v3, v0

    goto :goto_1

    .line 173
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 172
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const-string v1, "tt_video_detail_layout"

    .line 173
    invoke-static {p1, v1}, Lcom/bytedance/sdk/openadsdk/utils/ad;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :goto_1
    if-nez v3, :cond_1

    return-void

    .line 176
    :cond_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-eqz v0, :cond_2

    .line 177
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a()Z

    move-result v8

    move-object v1, v0

    move-object v2, p1

    move-object v7, p0

    invoke-direct/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;-><init>(Landroid/content/Context;Landroid/view/View;ZLjava/util/EnumSet;Lcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    goto :goto_2

    .line 179
    :cond_2
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/g;

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    const/4 v8, 0x0

    move-object v1, v0

    move-object v2, p1

    move-object v7, p0

    invoke-direct/range {v1 .. v8}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/g;-><init>(Landroid/content/Context;Landroid/view/View;ZLjava/util/EnumSet;Lcom/bytedance/sdk/openadsdk/core/d/l;Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c;Z)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    .line 181
    :goto_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/a;)V

    return-void
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 1

    .line 590
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->l:Ljava/util/List;

    if-nez v0, :cond_0

    .line 591
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->l:Ljava/util/List;

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)J
    .locals 2

    .line 66
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    return-wide v0
.end method

.method private c(Landroid/content/Context;)Landroid/view/View;
    .locals 12

    .line 193
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 195
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const-string v2, "tt_root_view"

    .line 196
    invoke-static {p1, v2}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setId(I)V

    const/high16 v2, -0x1000000

    .line 197
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 199
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 200
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const-string v5, "tt_video_loading_retry_layout"

    .line 201
    invoke-static {p1, v5}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setId(I)V

    const/4 v5, 0x0

    .line 202
    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    const/16 v6, 0x11

    .line 203
    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 204
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 207
    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 208
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const-string v7, "tt_video_loading_cover_image"

    .line 209
    invoke-static {p1, v7}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setId(I)V

    .line 210
    sget-object v7, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 211
    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 214
    new-instance v3, Landroid/widget/ProgressBar;

    invoke-direct {v3, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 215
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    const/high16 v8, 0x42700000    # 60.0f

    const/4 v9, 0x1

    invoke-static {v9, v8, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    invoke-static {v9, v8, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v8

    float-to-int v8, v8

    invoke-direct {v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const-string v7, "tt_video_loading_progress"

    .line 216
    invoke-static {p1, v7}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v3, v7}, Landroid/widget/ProgressBar;->setId(I)V

    const/16 v7, 0xd

    .line 217
    invoke-virtual {v6, v7, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 218
    invoke-virtual {v3, v6}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v6, "tt_video_loading_progress_bar"

    .line 219
    invoke-static {p1, v6}, Lcom/bytedance/sdk/openadsdk/utils/ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 220
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 222
    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 223
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    invoke-direct {v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const-string v8, "tt_video_play"

    .line 224
    invoke-static {p1, v8}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setId(I)V

    .line 225
    invoke-virtual {v3, v7, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 226
    sget-object v8, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const-string v8, "tt_play_movebar_textpage"

    .line 227
    invoke-static {p1, v8}, Lcom/bytedance/sdk/openadsdk/utils/ad;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    const/16 v8, 0x8

    .line 228
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 229
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 230
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 232
    new-instance v2, Landroid/widget/ProgressBar;

    const-string v3, "tt_Widget_ProgressBar_Horizontal"

    invoke-static {p1, v3}, Lcom/bytedance/sdk/openadsdk/utils/ad;->g(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    const/4 v10, 0x0

    invoke-direct {v2, p1, v10, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 233
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/high16 v11, 0x3fc00000    # 1.5f

    invoke-static {v9, v11, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {v3, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0x64

    .line 234
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setMax(I)V

    const-string v0, "tt_video_progress"

    .line 235
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setId(I)V

    .line 236
    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setBackgroundColor(I)V

    .line 237
    invoke-virtual {v2, v10}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "tt_video_progress_drawable"

    .line 238
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/ad;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 239
    invoke-virtual {v2, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const/16 v0, 0xc

    .line 240
    invoke-virtual {v3, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 241
    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 242
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 244
    new-instance v0, Landroid/view/ViewStub;

    invoke-direct {v0, p1}, Landroid/view/ViewStub;-><init>(Landroid/content/Context;)V

    .line 245
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const-string v3, "tt_video_ad_cover"

    .line 246
    invoke-static {p1, v3}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/ViewStub;->setId(I)V

    .line 247
    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v2, "tt_video_ad_cover_layout"

    .line 248
    invoke-static {p1, v2}, Lcom/bytedance/sdk/openadsdk/utils/ad;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 249
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 252
    new-instance v0, Landroid/view/ViewStub;

    invoke-direct {v0, p1}, Landroid/view/ViewStub;-><init>(Landroid/content/Context;)V

    .line 253
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 254
    invoke-virtual {v2, v7, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const-string v3, "tt_video_draw_layout_viewStub"

    .line 255
    invoke-static {p1, v3}, Lcom/bytedance/sdk/openadsdk/utils/ad;->e(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/ViewStub;->setId(I)V

    .line 256
    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v2, "tt_video_draw_btn_layout"

    .line 257
    invoke-static {p1, v2}, Lcom/bytedance/sdk/openadsdk/utils/ad;->f(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 258
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v1
.end method

.method private c(I)Z
    .locals 1

    .line 1436
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(I)Z

    move-result p1

    return p1
.end method

.method private c(II)Z
    .locals 3

    const-string v0, "TTVideoWebPageActivity"

    .line 1638
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnError - Error code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " Extra code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0x3f2

    const/4 v1, 0x1

    if-eq p1, v0, :cond_0

    const/16 v0, -0x3ef

    if-eq p1, v0, :cond_0

    const/16 v0, -0x3ec

    if-eq p1, v0, :cond_0

    const/16 v0, -0x6e

    if-eq p1, v0, :cond_0

    const/16 v0, 0x64

    if-eq p1, v0, :cond_0

    const/16 v0, 0xc8

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    if-eq p2, v1, :cond_1

    const/16 v0, 0x2bc

    if-eq p2, v0, :cond_1

    const/16 v0, 0x320

    if-eq p2, v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private d(Landroid/content/Context;)V
    .locals 1

    .line 1544
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v0

    .line 1546
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Landroid/content/Context;I)V

    const/4 p1, 0x4

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    .line 1549
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z:Z

    :cond_0
    return-void
.end method

.method private d(I)Z
    .locals 6

    .line 1469
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    .line 1472
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h()V

    .line 1473
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z:Z

    .line 1474
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v3, :cond_0

    .line 1475
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3, v4, v5, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/ref/WeakReference;Z)V

    :cond_0
    const/4 v3, 0x4

    if-eq v0, v3, :cond_2

    if-eqz v0, :cond_2

    .line 1478
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_1

    .line 1479
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->e()V

    .line 1481
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h()V

    .line 1482
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z:Z

    .line 1483
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A:Z

    .line 1484
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    if-eqz v0, :cond_3

    .line 1485
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v1

    iget-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->w:Z

    invoke-virtual {v0, p1, v1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(ILcom/bytedance/sdk/openadsdk/core/d/u;Z)Z

    move-result p1

    return p1

    :cond_2
    if-ne v0, v3, :cond_3

    .line 1488
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z:Z

    .line 1489
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_3

    .line 1490
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->q()V

    :cond_3
    return v2
.end method

.method static synthetic d(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)Z
    .locals 0

    .line 66
    iget-boolean p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->t:Z

    return p0
.end method

.method static synthetic e(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)Lcom/bytedance/sdk/openadsdk/utils/am;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)Ljava/lang/Runnable;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->L:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)V
    .locals 0

    .line 66
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A()V

    return-void
.end method

.method static synthetic h(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    return-object p0
.end method

.method private h(Z)V
    .locals 0

    .line 1321
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    return-void
.end method

.method static synthetic i(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)J
    .locals 2

    .line 66
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    return-wide v0
.end method

.method static synthetic j(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)Lcom/bytedance/sdk/openadsdk/core/d/l;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    return-object p0
.end method

.method static synthetic k(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;)Ljava/lang/ref/WeakReference;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method private z()Z
    .locals 5

    const-string v0, "NativeVideoController"

    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "retryCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->I:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->I:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-gt v1, v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    .line 153
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0, v1, v3, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/ref/WeakReference;Z)V

    :cond_0
    return v2

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez v0, :cond_2

    return v2

    .line 158
    :cond_2
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->I:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->I:I

    const-string v0, "NativeVideoController"

    .line 159
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPlaying="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, ",isPaused="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, ",isPrepared="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, ",isStarted="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h()Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :cond_4
    :goto_0
    return v1
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .line 1228
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 1233
    :goto_1
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 1234
    instance-of v2, v1, Landroid/app/Activity;

    if-nez v2, :cond_3

    return-void

    .line 1237
    :cond_3
    check-cast v1, Landroid/app/Activity;

    .line 1240
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/16 p1, 0x400

    if-nez v0, :cond_4

    .line 1244
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1, p1}, Landroid/view/Window;->setFlags(II)V

    goto :goto_2

    .line 1246
    :cond_4
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->clearFlags(I)V

    :goto_2
    return-void
.end method

.method public a(II)V
    .locals 3

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 143
    :cond_0
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->G:I

    .line 144
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->H:I

    const-string v0, "NativeVideoController"

    .line 145
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "height="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .line 370
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    .line 371
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    goto :goto_0

    :cond_0
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    :goto_0
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .line 1562
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v0

    .line 1564
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Landroid/content/Context;I)V

    const/4 p1, 0x4

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    .line 1567
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z:Z

    .line 1568
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k()V

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 10

    .line 806
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_11

    if-eqz p1, :cond_11

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    .line 807
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 810
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_1

    packed-switch v0, :pswitch_data_2

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_3

    .line 814
    :pswitch_0
    :try_start_0
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 817
    :catch_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->Q:J

    goto/16 :goto_3

    .line 877
    :pswitch_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->H()V

    .line 878
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_10

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_10

    .line 879
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$a;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$a;->g()V

    goto/16 :goto_3

    .line 959
    :pswitch_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "NativeVideoController"

    const-string v0, "CALLBACK_ON_RETRY_VIDEO_TIME-....\u91cd\u8bd5...."

    .line 960
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m()V

    const/4 p1, 0x0

    .line 962
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    .line 964
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/u;->i()Ljava/lang/String;

    move-result-object v1

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->G:I

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->H:I

    const/4 v5, 0x0

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 965
    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object v6

    const-wide/16 v7, 0x0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b()Z

    move-result v9

    move-object v0, p0

    .line 964
    invoke-virtual/range {v0 .. v9}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/lang/String;JZ)Z

    goto/16 :goto_3

    :cond_1
    const-string p1, "NativeVideoController"

    const-string v0, "\u4e0d\u6ee1\u8db3\u6761\u4ef6\uff0c\u65e0\u6cd5\u91cd\u8bd5"

    .line 967
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, 0x138

    .line 968
    invoke-direct {p0, p1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b(II)V

    goto/16 :goto_3

    .line 979
    :pswitch_3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->I()V

    goto/16 :goto_3

    :pswitch_4
    const-string p1, "NativeVideoController"

    const-string v0, "SSMediaPlayerWrapper \u91ca\u653e\u4e86\u3002\u3002\u3002\u3002\u3002"

    .line 972
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :pswitch_5
    const-string p1, "NativeVideoController"

    const-string v0, "\u64ad\u653e\u5668\u72b6\u6001\u51fa\u9519 STAT_ERROR 200 \u3001\u3001\u3001\u3001\u3001\u3001\u3001"

    .line 891
    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->G()V

    const/16 p1, 0x134

    .line 894
    invoke-direct {p0, p1, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b(II)V

    goto/16 :goto_3

    .line 952
    :pswitch_6
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_10

    .line 953
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    goto/16 :goto_3

    .line 861
    :pswitch_7
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->E:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->E:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 862
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->E:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;->f()V

    .line 865
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    if-eqz p1, :cond_3

    .line 866
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->M:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 868
    :cond_3
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->o:Z

    if-nez p1, :cond_4

    .line 869
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F()V

    .line 871
    :cond_4
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_5

    .line 872
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    .line 874
    :cond_5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/16 :goto_3

    .line 831
    :pswitch_8
    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 832
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v3, 0x3

    if-eqz v0, :cond_8

    if-eq p1, v3, :cond_7

    const/16 v0, 0x2be

    if-ne p1, v0, :cond_6

    goto :goto_0

    :cond_6
    const/16 v0, 0x2bd

    if-ne p1, v0, :cond_8

    .line 838
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->t()V

    .line 840
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1f40

    invoke-virtual {v0, v2, v4, v5}, Lcom/bytedance/sdk/openadsdk/utils/am;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 841
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J:Z

    goto :goto_1

    .line 834
    :cond_7
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    .line 835
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 836
    iput-boolean v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J:Z

    .line 846
    :cond_8
    :goto_1
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->o:Z

    if-eqz v0, :cond_a

    if-ne p1, v3, :cond_a

    .line 847
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-eqz v0, :cond_9

    .line 848
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 849
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$a;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$a;->g()V

    .line 852
    :cond_9
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 855
    :cond_a
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->o:Z

    if-eqz v0, :cond_10

    if-ne p1, v3, :cond_10

    .line 856
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F()V

    goto/16 :goto_3

    .line 897
    :pswitch_9
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 898
    iget p1, p1, Landroid/os/Message;->arg2:I

    .line 899
    invoke-direct {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b(II)V

    const-string v3, "NativeVideoController"

    const-string v4, "CALLBACK_ON_ERROR\u3001\u3001before isVideoPlaying\u3001\u3001\u3001\u3001\u3001"

    .line 900
    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, -0x3ec

    if-eq p1, v3, :cond_b

    return-void

    :cond_b
    const-string v3, "NativeVideoController"

    .line 905
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\u51fa\u9519\u540e errorcode,extra\u3001\u3001\u3001\u3001\u3001\u3001\u3001"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    invoke-direct {p0, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c(II)Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "NativeVideoController"

    const-string v4, "\u51fa\u9519\u540e\u5c55\u793a\u7ed3\u679c\u9875\u3001\u3001\u3001\u3001\u3001\u3001\u3001"

    .line 908
    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v5, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3, v4, v5, v2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/ref/WeakReference;Z)V

    .line 910
    invoke-virtual {p0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d(Z)V

    .line 911
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m()V

    .line 914
    :cond_c
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v1, :cond_d

    .line 915
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->w()V

    .line 917
    :cond_d
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    if-eqz v1, :cond_e

    .line 918
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->f:J

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    invoke-static {v4, v5, v6, v7}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;->b(JI)V

    .line 945
    :cond_e
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->E:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->E:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 946
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e()Z

    move-result v1

    if-nez v1, :cond_10

    .line 947
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->E:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;

    invoke-interface {v1, v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;->a(II)V

    goto :goto_3

    .line 821
    :pswitch_a
    iget p1, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b(I)V

    goto :goto_3

    .line 883
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 884
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    .line 885
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    cmp-long p1, v0, v2

    if-lez p1, :cond_f

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    goto :goto_2

    :cond_f
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    :goto_2
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    .line 886
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(JJ)V

    goto :goto_3

    .line 824
    :pswitch_c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 825
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_10

    .line 826
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    :cond_10
    :goto_3
    return-void

    :cond_11
    :goto_4
    return-void

    :pswitch_data_0
    .packed-switch 0x6c
        :pswitch_c
        :pswitch_b
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x12e
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x134
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x137
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/TTDrawFeedAd$DrawVideoListener;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView$b;)V
    .locals 2

    .line 126
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$1;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView$b;)V

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/NativeVideoTsView$b;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;I)V
    .locals 2

    .line 1140
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez p1, :cond_0

    return-void

    .line 1143
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A()V

    .line 1144
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->T:J

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c(I)Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(JZ)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;IZ)V
    .locals 4

    .line 1159
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->C()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 1162
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    int-to-long p2, p2

    .line 1163
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    mul-long p2, p2, v0

    long-to-float p2, p2

    const/high16 p3, 0x3f800000    # 1.0f

    mul-float p2, p2, p3

    const-string p3, "tt_video_progress_max"

    invoke-static {p1, p3}, Lcom/bytedance/sdk/openadsdk/utils/ad;->l(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p2, p1

    float-to-long p1, p2

    .line 1164
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    const-wide/16 v2, 0x0

    cmp-long p3, v0, v2

    if-lez p3, :cond_1

    long-to-int p1, p1

    int-to-long p1, p1

    .line 1165
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->T:J

    goto :goto_0

    .line 1167
    :cond_1
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->T:J

    .line 1169
    :goto_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_2

    .line 1170
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-wide p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->T:J

    invoke-virtual {p1, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(J)V

    :cond_2
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/graphics/SurfaceTexture;)V
    .locals 1

    const/4 p1, 0x1

    .line 1364
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    .line 1365
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_0

    .line 1366
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Z)V

    .line 1368
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez p1, :cond_1

    return-void

    .line 1371
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Landroid/graphics/SurfaceTexture;)V

    .line 1372
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->D()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/SurfaceHolder;)V
    .locals 1

    const/4 p1, 0x1

    .line 1332
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    .line 1333
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_0

    .line 1334
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Z)V

    .line 1336
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez p1, :cond_1

    return-void

    .line 1339
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Landroid/view/SurfaceHolder;)V

    .line 1340
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->D()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 2

    .line 1113
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->C()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_1

    .line 1116
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g()Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    .line 1117
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h()V

    .line 1118
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(ZZ)V

    .line 1119
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c()V

    goto :goto_0

    .line 1121
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i()Z

    move-result p1

    if-nez p1, :cond_3

    .line 1122
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_2

    .line 1123
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(Landroid/view/ViewGroup;)V

    .line 1125
    :cond_2
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d(J)V

    .line 1126
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_4

    .line 1127
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p2, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(ZZ)V

    goto :goto_0

    .line 1130
    :cond_3
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g(Z)V

    .line 1131
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_4

    .line 1132
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p2, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(ZZ)V

    :cond_4
    :goto_0
    return-void

    :cond_5
    :goto_1
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;Z)V
    .locals 0

    .line 1297
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->K()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;ZZ)V
    .locals 1

    .line 1262
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-eqz p1, :cond_0

    .line 1263
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h()V

    :cond_0
    if-eqz p3, :cond_1

    .line 1265
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d()Z

    move-result p1

    if-nez p1, :cond_1

    .line 1266
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->e()Z

    move-result p2

    const/4 p3, 0x1

    xor-int/2addr p2, p3

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(ZZ)V

    .line 1267
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p4, p3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(ZZZ)V

    .line 1269
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1270
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c()V

    .line 1271
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b()V

    goto :goto_0

    .line 1273
    :cond_2
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c()V

    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;)V
    .locals 0

    .line 609
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$a;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/c$c;)V
    .locals 1

    .line 308
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->E:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;)V
    .locals 1

    .line 530
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->x:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$a;)V
    .locals 1

    .line 1601
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->F:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/widget/h$a;Ljava/lang/String;)V
    .locals 0

    .line 1452
    sget-object p2, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f$8;->a:[I

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/widget/h$a;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1460
    :pswitch_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k()V

    const/4 p1, 0x0

    .line 1461
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z:Z

    .line 1462
    iput-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A:Z

    goto :goto_0

    .line 1457
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Z)V

    goto :goto_0

    .line 1454
    :pswitch_2
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public a(Z)V
    .locals 8

    .line 724
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-eqz v0, :cond_0

    .line 725
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->P:J

    .line 727
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->q:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p:Z

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    .line 729
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    const-string v3, "feed_break"

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->P:J

    .line 730
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r()I

    move-result v6

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c()Ljava/util/Map;

    move-result-object v7

    .line 729
    invoke-static/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    const/4 p1, 0x0

    .line 731
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->q:Z

    goto :goto_0

    .line 733
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    const-string v3, "feed_pause"

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->P:J

    .line 734
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r()I

    move-result v6

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c()Ljava/util/Map;

    move-result-object v7

    .line 733
    invoke-static/range {v0 .. v7}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    .line 738
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m()V

    return-void
.end method

.method public a()Z
    .locals 1

    .line 384
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/lang/String;JZ)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "JZ)Z"
        }
    .end annotation

    const-string p2, "tag_video_play"

    .line 315
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string p6, "[video] start NativeVideoController#playVideoUrl and video url is :\r\n"

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    invoke-static {p2, p5}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 p5, 0x0

    if-eqz p2, :cond_0

    const-string p1, "tag_video_play"

    const-string p2, "[video] play video stop , because no video info"

    .line 317
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    return p5

    .line 320
    :cond_0
    iput-boolean p9, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->t:Z

    .line 321
    iput-wide p7, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    const-wide/16 v0, 0x0

    cmp-long p2, p7, v0

    if-gtz p2, :cond_1

    .line 323
    iput-boolean p5, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->q:Z

    iput-boolean p5, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p:Z

    :cond_1
    cmp-long p2, p7, v0

    if-lez p2, :cond_3

    .line 326
    iput-wide p7, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    .line 327
    iget-wide p6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    iget-wide p8, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    cmp-long p2, p6, p8

    if-lez p2, :cond_2

    iget-wide p6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    goto :goto_0

    :cond_2
    iget-wide p6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    :goto_0
    iput-wide p6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    .line 329
    :cond_3
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p2, :cond_5

    .line 330
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->e()V

    .line 332
    iget p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->O:I

    if-nez p2, :cond_4

    .line 333
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->d()V

    .line 335
    :cond_4
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(II)V

    .line 336
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object p6, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {p6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p6

    check-cast p6, Landroid/view/ViewGroup;

    invoke-virtual {p2, p6}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(Landroid/view/ViewGroup;)V

    .line 337
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p2, p3, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(II)V

    .line 339
    :cond_5
    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez p2, :cond_6

    .line 340
    new-instance p2, Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    iget-object p3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-direct {p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;-><init>(Landroid/os/Handler;)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    :cond_6
    const-string p2, "tag_video_play"

    const-string p3, "[video] new MediaPlayer"

    .line 342
    invoke-static {p2, p3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->f:J

    .line 345
    :try_start_0
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    const-string p2, "tag_video_play"

    .line 347
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "[video] invoke NativeVideoController#playVideo cause exception :"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    return p5
.end method

.method public b(J)V
    .locals 0

    .line 397
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->y:J

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;I)V
    .locals 0

    .line 1149
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_0

    .line 1150
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->B()V

    .line 1152
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_1

    .line 1153
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c()V

    :cond_1
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/graphics/SurfaceTexture;)V
    .locals 0

    const/4 p1, 0x0

    .line 1383
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    .line 1384
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_0

    .line 1385
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Z)V

    :cond_0
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/SurfaceHolder;)V
    .locals 0

    const/4 p1, 0x0

    .line 1350
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    .line 1351
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_0

    .line 1352
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m:Z

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Z)V

    :cond_0
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 1191
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;ZZ)V

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;ZZ)V
    .locals 0

    .line 1196
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->C()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 1199
    :cond_0
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    const/4 p2, 0x1

    xor-int/2addr p1, p2

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h(Z)V

    .line 1200
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Landroid/app/Activity;

    if-nez p1, :cond_1

    const-string p1, "NativeVideoController"

    const-string p2, "context is not activity, not support this function."

    .line 1201
    invoke-static {p1, p2}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 1204
    :cond_1
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    const/4 p4, 0x0

    if-eqz p1, :cond_3

    if-eqz p3, :cond_2

    const/16 p1, 0x8

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 1205
    :goto_0
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(I)V

    .line 1207
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_4

    .line 1208
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Landroid/view/ViewGroup;)V

    .line 1209
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Z)V

    goto :goto_1

    .line 1212
    :cond_3
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(I)V

    .line 1214
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_4

    .line 1215
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    invoke-virtual {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Landroid/view/ViewGroup;)V

    .line 1216
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1, p4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Z)V

    .line 1220
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->x:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->x:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_6

    .line 1222
    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    invoke-interface {p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/e;->a(Z)V

    :cond_6
    return-void
.end method

.method public b(Z)V
    .locals 1

    .line 376
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    .line 377
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->c(Z)V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .line 420
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->t:Z

    return v0
.end method

.method protected c()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 613
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->o()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/utils/ak;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;JLcom/bytedance/sdk/openadsdk/core/video/d/d;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public c(J)V
    .locals 0

    .line 405
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    return-void
.end method

.method public c(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 0

    .line 1253
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_0

    .line 1254
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->g()V

    :cond_0
    const/4 p1, 0x1

    .line 1256
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Z)V

    return-void
.end method

.method public c(Z)V
    .locals 1

    .line 424
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->t:Z

    .line 425
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Z)V

    :cond_0
    return-void
.end method

.method public d(J)V
    .locals 3

    .line 699
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    .line 700
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    goto :goto_0

    :cond_0
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    :goto_0
    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    .line 701
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_1

    .line 702
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->e()V

    .line 704
    :cond_1
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz p1, :cond_2

    .line 705
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->t:Z

    const/4 v2, 0x1

    xor-int/2addr p2, v2

    invoke-virtual {p1, v2, v0, v1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(ZJZ)V

    .line 706
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->A()V

    :cond_2
    return-void
.end method

.method public d(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 1

    .line 1279
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->S:Z

    const/4 p2, 0x1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 1280
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->h(Z)V

    .line 1281
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz p1, :cond_0

    .line 1282
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->b(Landroid/view/ViewGroup;)V

    .line 1284
    :cond_0
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(I)V

    goto :goto_0

    .line 1286
    :cond_1
    invoke-virtual {p0, p2}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Z)V

    :goto_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    .line 1614
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->B:Z

    return-void
.end method

.method public d()Z
    .locals 1

    .line 1399
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public e(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 1292
    invoke-virtual {p0, p1, p2, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;Z)V

    return-void
.end method

.method public e(Z)V
    .locals 0

    .line 1624
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->D:Z

    return-void
.end method

.method public e()Z
    .locals 1

    .line 1419
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()V
    .locals 3

    .line 1575
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->W:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->K:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 1578
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    .line 1579
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->W:Z

    .line 1580
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    .line 1581
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1583
    :try_start_0
    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->U:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method public f(Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/b;Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public f(Z)V
    .locals 0

    .line 1634
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->K:Z

    return-void
.end method

.method public g()V
    .locals 11

    .line 1405
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/x;->c(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1410
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->m()V

    .line 1411
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->R()Lcom/bytedance/sdk/openadsdk/core/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/u;->i()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->G:I

    iget v5, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->H:I

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 1413
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object v7

    const-wide/16 v8, 0x0

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b()Z

    move-result v10

    move-object v1, p0

    .line 1411
    invoke-virtual/range {v1 .. v10}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/lang/String;JZ)Z

    const/4 v0, 0x0

    .line 1414
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d(Z)V

    return-void
.end method

.method public g(Z)V
    .locals 1

    .line 673
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->e()V

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 677
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->u()V

    .line 679
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->E()V

    return-void
.end method

.method public h()V
    .locals 10

    .line 618
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b()V

    .line 622
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->q:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p:Z

    if-eqz v0, :cond_4

    .line 624
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    const-string v0, "sp_multi_single_app_data_class"

    const-string v2, "IsCanLoadPauseLog"

    .line 625
    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 628
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/content/Context;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    const-string v5, "feed_pause"

    .line 629
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r()I

    move-result v8

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c()Ljava/util/Map;

    move-result-object v9

    .line 628
    invoke-static/range {v2 .. v9}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    :cond_1
    const-string v0, "sp_multi_single_app_data_class"

    const-string v2, "IsCanLoadPauseLog"

    .line 632
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/bytedance/sdk/openadsdk/multipro/d/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 634
    :cond_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/u;->a()Lcom/bytedance/sdk/openadsdk/core/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/u;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 635
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/content/Context;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u:Ljava/lang/String;

    const-string v5, "feed_pause"

    .line 636
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->p()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r()I

    move-result v8

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->c()Ljava/util/Map;

    move-result-object v9

    .line 635
    invoke-static/range {v2 .. v9}, Lcom/bytedance/sdk/openadsdk/c/d;->a(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/String;Ljava/lang/String;JILjava/util/Map;)V

    .line 638
    :cond_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/u;->a()Lcom/bytedance/sdk/openadsdk/core/u;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/u;->a(Z)V

    :cond_4
    :goto_0
    return-void
.end method

.method public i()V
    .locals 1

    .line 649
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    .line 650
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b()V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 1

    .line 656
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->d()V

    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    .line 663
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->e()V

    .line 666
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    if-eqz v0, :cond_1

    .line 667
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->u()V

    .line 669
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->E()V

    return-void
.end method

.method public l()V
    .locals 1

    const/4 v0, 0x1

    .line 713
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->a(Z)V

    return-void
.end method

.method public m()V
    .locals 5

    .line 746
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->c()V

    .line 748
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    .line 750
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/ak;->b(Lcom/bytedance/sdk/openadsdk/core/d/l;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->O:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 751
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->r:Lcom/bytedance/sdk/openadsdk/core/d/l;

    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->n:Ljava/lang/ref/WeakReference;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;->a(Lcom/bytedance/sdk/openadsdk/core/d/l;Ljava/lang/ref/WeakReference;Z)V

    .line 757
    :cond_2
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    if-eqz v0, :cond_3

    .line 758
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 759
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->M:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 760
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->L:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 761
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->d:Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/am;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 763
    :cond_3
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->B()V

    .line 764
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->l:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 765
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 767
    :cond_4
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->s:Z

    if-eqz v0, :cond_5

    .line 768
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->y()V

    :cond_5
    return-void
.end method

.method public n()J
    .locals 2

    .line 365
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->i:J

    return-wide v0
.end method

.method public o()J
    .locals 2

    .line 535
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->n()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public p()J
    .locals 4

    .line 410
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->o()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->y:J

    add-long/2addr v0, v2

    :goto_0
    return-wide v0
.end method

.method public q()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public r()I
    .locals 4

    .line 437
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->j:J

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    invoke-static {v0, v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/core/video/e/a;->a(JJ)I

    move-result v0

    return v0
.end method

.method public s()J
    .locals 2

    .line 401
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->k:J

    return-wide v0
.end method

.method public t()Z
    .locals 1

    .line 393
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->z:Z

    return v0
.end method

.method public u()Lcom/bytedance/sdk/openadsdk/core/video/d/d;
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->g:Lcom/bytedance/sdk/openadsdk/core/video/d/d;

    return-object v0
.end method

.method public v()Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;
    .locals 1

    .line 303
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->b:Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/h;

    return-object v0
.end method

.method public w()Z
    .locals 1

    .line 431
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->B:Z

    return v0
.end method

.method public x()Z
    .locals 1

    .line 1629
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->J:Z

    return v0
.end method

.method public y()V
    .locals 2

    .line 1589
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->W:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->K:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 1592
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    .line 1593
    iput-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->W:Z

    .line 1595
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/nativevideo/f;->U:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    :cond_1
    :goto_0
    return-void
.end method
