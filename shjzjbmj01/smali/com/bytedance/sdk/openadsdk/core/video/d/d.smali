.class public Lcom/bytedance/sdk/openadsdk/core/video/d/d;
.super Ljava/lang/Object;
.source "SSMediaPlayerWrapper.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/core/video/d/c$a;
.implements Lcom/bytedance/sdk/openadsdk/core/video/d/c$b;
.implements Lcom/bytedance/sdk/openadsdk/core/video/d/c$c;
.implements Lcom/bytedance/sdk/openadsdk/core/video/d/c$d;
.implements Lcom/bytedance/sdk/openadsdk/core/video/d/c$e;
.implements Lcom/bytedance/sdk/openadsdk/core/video/d/c$f;
.implements Lcom/bytedance/sdk/openadsdk/core/video/d/c$g;
.implements Lcom/bytedance/sdk/openadsdk/utils/am$a;


# static fields
.field private static m:Z = false

.field private static final o:Landroid/util/SparseIntArray;


# instance fields
.field private a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I

.field private g:J

.field private final h:Landroid/os/Handler;

.field private i:Landroid/os/Handler;

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:I

.field private n:Ljava/lang/String;

.field private p:Z

.field private q:Ljava/lang/Runnable;

.field private r:Z

.field private final s:Ljava/lang/Object;

.field private t:Ljava/lang/StringBuilder;

.field private u:Z

.field private v:J

.field private w:J

.field private x:J

.field private y:J

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 122
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->o:Landroid/util/SparseIntArray;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    const/4 v0, -0x1

    .line 128
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;-><init>(Landroid/os/Handler;I)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;I)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "unused"
        }
    .end annotation

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p2, 0x0

    .line 92
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    const/4 v0, 0x0

    .line 93
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b:Z

    .line 95
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->c:Z

    const/16 v1, 0xc9

    .line 99
    iput v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    const-wide/16 v1, -0x1

    .line 100
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g:J

    .line 116
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k:I

    const-string v1, "0"

    .line 120
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->n:Ljava/lang/String;

    .line 213
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d$1;

    invoke-direct {v1, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/d/d;)V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->q:Ljava/lang/Runnable;

    .line 955
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->s:Ljava/lang/Object;

    .line 956
    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->t:Ljava/lang/StringBuilder;

    .line 973
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->u:Z

    const-wide/16 v1, 0x0

    .line 974
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->v:J

    .line 975
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    .line 976
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->x:J

    .line 977
    iput-wide v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->y:J

    .line 980
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->z:Z

    .line 134
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k:I

    .line 136
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    .line 137
    new-instance p1, Landroid/os/HandlerThread;

    const-string p2, "VideoManager"

    invoke-direct {p1, p2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    .line 139
    new-instance p2, Lcom/bytedance/sdk/openadsdk/utils/am;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {p2, p1, p0}, Lcom/bytedance/sdk/openadsdk/utils/am;-><init>(Landroid/os/Looper;Lcom/bytedance/sdk/openadsdk/utils/am$a;)V

    iput-object p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    .line 140
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x11

    if-lt p1, p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->z:Z

    .line 141
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->q()V

    return-void
.end method

.method private A()I
    .locals 2

    .line 927
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    .line 928
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method private B()V
    .locals 2

    .line 937
    sget-boolean v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->m:Z

    if-eqz v0, :cond_0

    .line 938
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->l:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(IZ)V

    .line 939
    sput-boolean v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->m:Z

    :cond_0
    return-void
.end method

.method private C()V
    .locals 5

    .line 983
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 984
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    :cond_0
    return-void
.end method

.method private D()V
    .locals 8

    .line 994
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 995
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->v:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->v:J

    .line 996
    iput-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/d/d;I)I
    .locals 0

    .line 37
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    return p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/d/d;)Landroid/os/Handler;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    return-object p0
.end method

.method private a(ILjava/lang/Object;)V
    .locals 1

    const/16 v0, 0x135

    if-ne p1, v0, :cond_0

    .line 688
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->B()V

    .line 690
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 691
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    return-void
.end method

.method private a(IZ)V
    .locals 2

    if-eqz p2, :cond_0

    .line 915
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->A()I

    move-result p2

    if-eq p2, p1, :cond_0

    const/4 v0, 0x1

    .line 917
    sput-boolean v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->m:Z

    .line 918
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->l:I

    .line 921
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object p2

    const-string v0, "audio"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/media/AudioManager;

    if-eqz p2, :cond_1

    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 923
    invoke-virtual {p2, v0, p1, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .line 860
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 861
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    .line 863
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 649
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 650
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Ljava/io/FileDescriptor;)V

    .line 651
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    return-void
.end method

.method private a(II)Z
    .locals 3

    const-string v0, "SSMediaPlayeWrapper"

    .line 755
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnError - Error code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " Extra code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0x3f2

    const/4 v1, 0x1

    if-eq p1, v0, :cond_0

    const/16 v0, -0x3ef

    if-eq p1, v0, :cond_0

    const/16 v0, -0x3ec

    if-eq p1, v0, :cond_0

    const/16 v0, -0x6e

    if-eq p1, v0, :cond_0

    const/16 v0, 0x64

    if-eq p1, v0, :cond_0

    const/16 v0, 0xc8

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    if-eq p2, v1, :cond_1

    const/16 v0, 0x2bc

    if-eq p2, v0, :cond_1

    const/16 v0, 0x320

    if-eq p2, v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method static synthetic a(Lcom/bytedance/sdk/openadsdk/core/video/d/d;Z)Z
    .locals 0

    .line 37
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->r:Z

    return p1
.end method

.method private b(II)V
    .locals 6

    const/16 p2, 0x2bd

    if-ne p1, p2, :cond_0

    .line 798
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->D()V

    .line 799
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->x:J

    goto :goto_0

    :cond_0
    const/16 p2, 0x2be

    const-wide/16 v0, 0x0

    if-ne p1, p2, :cond_2

    .line 801
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    cmp-long v2, p1, v0

    if-gtz v2, :cond_1

    .line 802
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    .line 804
    :cond_1
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->x:J

    cmp-long v2, p1, v0

    if-lez v2, :cond_3

    .line 805
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->y:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->x:J

    sub-long/2addr v2, v4

    add-long/2addr p1, v2

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->y:J

    .line 806
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->x:J

    goto :goto_0

    .line 808
    :cond_2
    iget-boolean p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->z:Z

    if-eqz p2, :cond_3

    const/4 p2, 0x3

    if-ne p1, p2, :cond_3

    .line 810
    iget-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    cmp-long v2, p1, v0

    if-gtz v2, :cond_3

    .line 811
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    :cond_3
    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/bytedance/sdk/openadsdk/core/video/d/d;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->q()V

    return-void
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 905
    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->e:Z

    if-nez v0, :cond_1

    .line 906
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 908
    :cond_1
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .line 959
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    if-eqz p1, :cond_0

    .line 960
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v0, 0xc9

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 962
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->s:Ljava/lang/Object;

    monitor-enter p1

    .line 963
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->t:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 964
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->t:Ljava/lang/StringBuilder;

    .line 966
    :cond_1
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic c(Lcom/bytedance/sdk/openadsdk/core/video/d/d;)Lcom/bytedance/sdk/openadsdk/core/video/d/c;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    return-object p0
.end method

.method private q()V
    .locals 3

    .line 149
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    if-nez v0, :cond_0

    const-string v0, "SSMediaPlayeWrapper"

    const-string v1, "SSMediaPlayerWrapper use System Mediaplayer"

    .line 150
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/d/b;

    invoke-direct {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/b;-><init>()V

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    const-string v0, "0"

    .line 152
    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->n:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$e;)V

    .line 154
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$b;)V

    .line 155
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$c;)V

    .line 156
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$a;)V

    .line 157
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$f;)V

    .line 158
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$d;)V

    .line 159
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$g;)V

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    iget-boolean v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b:Z

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->b(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SSMediaPlayeWrapper"

    const-string v2, "setLooping error: "

    .line 163
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    const/4 v0, 0x0

    .line 165
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->c:Z

    :cond_0
    return-void
.end method

.method private r()V
    .locals 2

    const-string v0, "tag_video_play"

    const-string v1, "[video] MediaPlayerProxy#start first play prepare invoke !"

    .line 274
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d$2;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d$2;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/d/d;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method private s()V
    .locals 3

    .line 655
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    if-nez v0, :cond_0

    return-void

    .line 657
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->l()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SSMediaPlayeWrapper"

    const-string v2, "releaseMediaplayer error1: "

    .line 659
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 661
    :goto_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$b;)V

    .line 662
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$g;)V

    .line 663
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$a;)V

    .line 664
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$d;)V

    .line 665
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$c;)V

    .line 666
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$e;)V

    .line 667
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Lcom/bytedance/sdk/openadsdk/core/video/d/c$f;)V

    .line 669
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->k()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "SSMediaPlayeWrapper"

    const-string v2, "releaseMediaplayer error2: "

    .line 671
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method private t()V
    .locals 3

    .line 676
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "SSMediaPlayeWrapper"

    const-string v1, "onDestory............"

    .line 678
    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SSMediaPlayeWrapper"

    const-string v2, "onDestroy error: "

    .line 681
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method private u()V
    .locals 4

    .line 707
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->o:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 709
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->o:Landroid/util/SparseIntArray;

    iget v2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k:I

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0

    .line 711
    :cond_0
    sget-object v2, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->o:Landroid/util/SparseIntArray;

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseIntArray;->put(II)V

    :goto_0
    return-void
.end method

.method private v()V
    .locals 5

    .line 844
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->z:Z

    if-nez v0, :cond_0

    .line 845
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 846
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    :cond_0
    return-void
.end method

.method private w()V
    .locals 2

    .line 867
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 870
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->d:Z

    .line 871
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 872
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 873
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 875
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    .line 876
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->d:Z

    return-void
.end method

.method private x()V
    .locals 1

    .line 880
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 883
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w()V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method private y()V
    .locals 1

    .line 887
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 891
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w()V

    return-void

    .line 888
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->t()V

    return-void
.end method

.method private z()V
    .locals 1

    .line 895
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 898
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public a()Landroid/media/MediaPlayer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/video/d/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/b;->e()Landroid/media/MediaPlayer;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(J)V
    .locals 2

    .line 299
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->D()V

    .line 300
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    const/16 v1, 0xce

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    const/16 v1, 0xd1

    if-ne v0, v1, :cond_1

    .line 302
    :cond_0
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d$4;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/d/d;J)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .line 314
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d$5;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d$5;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/d/d;Landroid/graphics/SurfaceTexture;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Landroid/os/Message;)V
    .locals 16

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    .line 413
    iget v2, v0, Landroid/os/Message;->what:I

    const-string v3, "tag_video_play"

    .line 414
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[video]  execute , mCurrentState = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " handlerMsg="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    const/4 v5, 0x1

    if-eqz v3, :cond_15

    .line 416
    iget v3, v0, Landroid/os/Message;->what:I

    const/16 v6, 0xc9

    if-eq v3, v6, :cond_15

    const/16 v8, 0xa

    const/16 v9, 0xcb

    const/16 v10, 0xca

    const/16 v11, 0xcd

    const/16 v12, 0xd0

    const/16 v13, 0xd1

    const-wide/16 v14, 0x0

    const/16 v4, 0xcf

    const/16 v7, 0xce

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_7

    .line 571
    :pswitch_0
    :try_start_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_0

    .line 572
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/SurfaceTexture;

    .line 573
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    new-instance v4, Landroid/view/Surface;

    invoke-direct {v4, v0}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-interface {v3, v4}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Landroid/view/Surface;)V

    .line 575
    :cond_0
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, v5}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Z)V

    .line 576
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3, v8}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Landroid/content/Context;I)V

    .line 577
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->x()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_7

    :catch_0
    move-exception v0

    const-string v3, "SSMediaPlayeWrapper"

    const-string v4, "OP_SET_SURFACE error: "

    .line 579
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 584
    :pswitch_1
    :try_start_1
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/SurfaceHolder;

    .line 585
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Landroid/view/SurfaceHolder;)V

    .line 586
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 587
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3, v8}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Landroid/content/Context;I)V

    .line 589
    :cond_1
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, v5}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_7

    :catch_1
    move-exception v0

    const-string v3, "SSMediaPlayeWrapper"

    const-string v4, "OP_SET_DISPLAY error: "

    .line 592
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 621
    :pswitch_2
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v7, :cond_2

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-ne v0, v4, :cond_3

    .line 623
    :cond_2
    :try_start_2
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->i()J

    move-result-wide v3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v3, "SSMediaPlayeWrapper"

    const-string v4, "OP_REQUEST_CUR_POSITION error: "

    .line 625
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    move-wide v3, v14

    :goto_0
    cmp-long v0, v3, v14

    if-lez v0, :cond_15

    const/16 v0, 0x6d

    .line 629
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(ILjava/lang/Object;)V

    goto/16 :goto_7

    .line 610
    :pswitch_3
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v7, :cond_4

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-ne v0, v4, :cond_5

    .line 612
    :cond_4
    :try_start_3
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->j()J

    move-result-wide v3
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    move-wide v14, v3

    goto :goto_1

    :catch_3
    move-exception v0

    const-string v3, "SSMediaPlayeWrapper"

    const-string v4, "OP_REQUEST_DURATION error: "

    .line 614
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    :goto_1
    const/16 v0, 0x6c

    .line 617
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(ILjava/lang/Object;)V

    goto/16 :goto_7

    .line 515
    :pswitch_4
    iget v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v3, v6, :cond_6

    iget v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-ne v3, v9, :cond_10

    .line 517
    :cond_6
    :try_start_4
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;

    .line 518
    iget-object v3, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Ljava/lang/String;

    .line 520
    invoke-static {v3}, Lcom/bytedance/sdk/openadsdk/core/video/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 521
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    move-object v3, v4

    .line 525
    :cond_7
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    return-void

    :cond_8
    const-string v4, "/"

    .line 528
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_c

    iget v4, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->c:I

    if-ne v4, v5, :cond_9

    goto/16 :goto_2

    .line 538
    :cond_9
    new-instance v4, Lcom/bytedance/sdk/openadsdk/k/f/b;

    invoke-direct {v4}, Lcom/bytedance/sdk/openadsdk/k/f/b;-><init>()V

    iget-object v6, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->a:Ljava/lang/String;

    .line 539
    invoke-virtual {v4, v6}, Lcom/bytedance/sdk/openadsdk/k/f/b;->a(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/f/b;

    move-result-object v4

    iget-object v6, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:Ljava/lang/String;

    .line 540
    invoke-virtual {v4, v6}, Lcom/bytedance/sdk/openadsdk/k/f/b;->b(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/k/f/b;

    move-result-object v4

    .line 542
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x17

    if-lt v6, v7, :cond_a

    const-string v6, "http"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 543
    iget-object v4, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    iget-object v0, v0, Lcom/bytedance/sdk/openadsdk/core/video/b/a;->d:Ljava/lang/String;

    invoke-interface {v4, v3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 545
    :cond_a
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/k/f/a;->a()Lcom/bytedance/sdk/openadsdk/k/f/a;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/bytedance/sdk/openadsdk/k/f/a;->b(Lcom/bytedance/sdk/openadsdk/k/f/b;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "cache009"

    .line 546
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\u4f7f\u7528Video\u7f13\u5b58-OP_SET_DATASOURCE-proxyurl="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_b

    .line 547
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v3

    if-eqz v3, :cond_b

    const-string v3, "file"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 548
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v3, "cache010"

    .line 549
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\u4f7f\u7528uri parse ="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 553
    :cond_b
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v3, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 530
    :cond_c
    :goto_2
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/multipro/b;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "/"

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 532
    invoke-direct {v1, v3}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 534
    :cond_d
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0, v3}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(Ljava/lang/String;)V

    .line 557
    :goto_3
    iput v10, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    .line 559
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz v0, :cond_15

    .line 560
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v3, 0x13a

    invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_7

    :catch_4
    move-exception v0

    const-string v3, "SSMediaPlayeWrapper"

    const-string v4, "OP_SET_DATASOURCE error: "

    .line 563
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 503
    :pswitch_5
    iget v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v3, v7, :cond_e

    iget v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v3, v4, :cond_e

    iget v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-ne v3, v13, :cond_10

    .line 506
    :cond_e
    :try_start_5
    iget-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v3, v6, v7}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(J)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_7

    :catch_5
    move-exception v0

    const-string v3, "SSMediaPlayeWrapper"

    const-string v4, "OP_SEEKTO error: "

    .line 508
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 596
    :pswitch_6
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v11, :cond_f

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v7, :cond_f

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v12, :cond_f

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v4, :cond_f

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-ne v0, v13, :cond_10

    .line 599
    :cond_f
    :try_start_6
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->g()V

    .line 600
    iput v12, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_7

    :catch_6
    move-exception v0

    const-string v3, "SSMediaPlayeWrapper"

    const-string v4, "OP_STOP error: "

    .line 602
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 460
    :pswitch_7
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v10, :cond_11

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-ne v0, v12, :cond_10

    goto :goto_4

    :cond_10
    const/4 v3, 0x1

    goto/16 :goto_8

    .line 462
    :cond_11
    :goto_4
    :try_start_7
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    check-cast v0, Lcom/bytedance/sdk/openadsdk/core/video/d/b;

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/b;->e()Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    const-string v0, "tag_video_play"

    const-string v3, "[video] OP_PREPARE_ASYNC execute , mMediaPlayer real prepareAsync !"

    .line 463
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz v0, :cond_15

    .line 465
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const-wide/16 v3, 0x2710

    const/16 v6, 0x138

    invoke-virtual {v0, v6, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_7

    goto/16 :goto_7

    :catch_7
    move-exception v0

    const-string v3, "NativeVideoController"

    const-string v4, "OP_PREPARE_ASYNC error: "

    .line 470
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 478
    :pswitch_8
    :try_start_8
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->s()V

    const-string v0, "tag_video_play"

    const-string v3, "[video] OP_RELEASE execute , releaseMediaplayer !"

    .line 479
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_8

    goto :goto_5

    :catch_8
    move-exception v0

    const-string v3, "NativeVideoController"

    const-string v4, "OP_RELEASE error: "

    .line 481
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_5
    const/4 v3, 0x0

    .line 483
    iput-boolean v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->e:Z

    const/16 v0, 0x135

    const/4 v3, 0x0

    .line 484
    invoke-direct {v1, v0, v3}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(ILjava/lang/Object;)V

    .line 485
    iput v9, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    .line 486
    iput-object v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    .line 490
    invoke-direct/range {p0 .. p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->y()V

    goto/16 :goto_7

    .line 495
    :pswitch_9
    :try_start_9
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->l()V

    const-string v0, "tag_video_play"

    const-string v3, "[video] OP_RELEASE execute , releaseMediaplayer !"

    .line 496
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    iput v6, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_9

    goto/16 :goto_7

    :catch_9
    move-exception v0

    const-string v3, "SSMediaPlayeWrapper"

    const-string v4, "OP_RESET error: "

    .line 499
    invoke-static {v3, v4, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 445
    :pswitch_a
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v7, :cond_12

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v4, :cond_12

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-ne v0, v13, :cond_10

    :cond_12
    :try_start_a
    const-string v0, "tag_video_play"

    const-string v3, "[video] OP_PAUSE execute , mMediaPlayer  OP_PAUSE !"

    .line 448
    invoke-static {v0, v3}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->h()V

    .line 450
    iput v4, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_b

    const/4 v3, 0x0

    .line 451
    :try_start_b
    iput-boolean v3, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->r:Z
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_a

    goto :goto_8

    :catch_a
    move-exception v0

    goto :goto_6

    :catch_b
    move-exception v0

    const/4 v3, 0x0

    :goto_6
    const-string v4, "NativeVideoController"

    const-string v6, "OP_PAUSE error: "

    .line 453
    invoke-static {v4, v6, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    :pswitch_b
    const/4 v3, 0x0

    .line 418
    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v11, :cond_13

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v7, :cond_13

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-eq v0, v4, :cond_13

    iget v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    if-ne v0, v13, :cond_10

    .line 421
    :cond_13
    :try_start_c
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->f()V

    const-string v0, "tag_video_play"

    const-string v4, "[video] OP_START execute , mMediaPlayer real start !"

    .line 422
    invoke-static {v0, v4}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    iput v7, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    .line 425
    iget-wide v6, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g:J

    cmp-long v0, v6, v14

    if-ltz v0, :cond_14

    .line 426
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    iget-wide v6, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g:J

    invoke-interface {v0, v6, v7}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(J)V

    const-wide/16 v6, -0x1

    .line 427
    iput-wide v6, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g:J

    .line 430
    :cond_14
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz v0, :cond_16

    .line 431
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v4, 0x138

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 432
    iget-object v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v4, 0x139

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_c

    goto :goto_8

    :catch_c
    move-exception v0

    const-string v4, "tag_video_play"

    const-string v6, "OP_START error: "

    .line 436
    invoke-static {v4, v6, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    :cond_15
    :goto_7
    const/4 v3, 0x0

    :cond_16
    :goto_8
    if-eqz v3, :cond_17

    const/16 v0, 0xc8

    .line 640
    iput v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    .line 641
    iget-boolean v0, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->c:Z

    if-nez v0, :cond_17

    const/16 v0, 0x134

    .line 642
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(ILjava/lang/Object;)V

    .line 643
    iput-boolean v5, v1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->c:Z

    :cond_17
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/view/SurfaceHolder;)V
    .locals 1

    .line 334
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d$6;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d$6;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/d/d;Landroid/view/SurfaceHolder;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V
    .locals 1

    .line 366
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d$7;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d$7;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/d/d;Lcom/bytedance/sdk/openadsdk/core/video/b/a;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/d/c;)V
    .locals 1

    .line 717
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b:Z

    if-nez p1, :cond_0

    const/16 p1, 0xd1

    goto :goto_0

    :cond_0
    const/16 p1, 0xce

    :goto_0
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    .line 718
    sget-object p1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->o:Landroid/util/SparseIntArray;

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k:I

    invoke-virtual {p1, v0}, Landroid/util/SparseIntArray;->delete(I)V

    .line 719
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz p1, :cond_1

    .line 720
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v0, 0x12e

    invoke-virtual {p1, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    const-string p1, "completion"

    .line 723
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Ljava/lang/String;)V

    .line 724
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->D()V

    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/d/c;I)V
    .locals 1

    .line 697
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    if-eq v0, p1, :cond_0

    return-void

    .line 700
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz p1, :cond_1

    .line 701
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v0, 0x12d

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/d/c;IIII)V
    .locals 0

    .line 1031
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz p1, :cond_0

    .line 1032
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 p4, 0x137

    invoke-virtual {p1, p4, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 145
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->p:Z

    return-void
.end method

.method public a(ZJZ)V
    .locals 3

    const-string v0, "tag_video_play"

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[video] MediaPlayerProxy#start firstSeekToPosition="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ",firstPlay :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ",isPauseOtherMusicVolume="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 179
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->r:Z

    if-nez p4, :cond_0

    .line 182
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    if-eqz p4, :cond_1

    const/4 p4, 0x1

    .line 183
    invoke-virtual {p0, p4}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Z)V

    goto :goto_0

    .line 186
    :cond_0
    iget-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    if-eqz p4, :cond_1

    .line 187
    invoke-virtual {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Z)V

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    const-string p1, "tag_video_play"

    const-string p4, "[video] first start , SSMediaPlayer  start method !"

    .line 191
    invoke-static {p1, p4}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->r()V

    .line 194
    iput-wide p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g:J

    goto :goto_3

    .line 196
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->C()V

    .line 197
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    if-eqz p1, :cond_4

    .line 199
    :try_start_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->i()J

    move-result-wide v0

    cmp-long p1, p2, v0

    if-lez p1, :cond_3

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->i()J

    move-result-wide p2

    :goto_1
    iput-wide p2, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    const-string p2, "tag_video_play"

    .line 201
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "[video] MediaPlayerProxy#start  error: getCurrentPosition :"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_4
    :goto_2
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->p:Z

    if-eqz p1, :cond_5

    .line 205
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->q:Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Ljava/lang/Runnable;)V

    goto :goto_3

    .line 207
    :cond_5
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->q:Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(Ljava/lang/Runnable;)V

    :goto_3
    return-void
.end method

.method public a(Lcom/bytedance/sdk/openadsdk/core/video/d/c;II)Z
    .locals 2

    const-string p1, "SSMediaPlayeWrapper"

    .line 729
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "what="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "extra="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->u()V

    const/16 p1, 0xc8

    .line 735
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    .line 736
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz p1, :cond_0

    .line 737
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v0, 0x12f

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    .line 739
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    if-eqz p1, :cond_1

    .line 740
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v0, 0x6c

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 741
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v0, 0x6d

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 743
    :cond_1
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->c:Z

    const/4 v0, 0x1

    if-nez p1, :cond_2

    const/16 p1, 0x134

    .line 744
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(ILjava/lang/Object;)V

    .line 745
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->c:Z

    .line 747
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a(II)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 748
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->t()V

    :cond_3
    return v0
.end method

.method public b()V
    .locals 2

    .line 225
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x1

    .line 226
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->r:Z

    .line 227
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 228
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->D()V

    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/d/c;)V
    .locals 3

    const/16 p1, 0xcd

    .line 818
    iput p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    .line 820
    iget-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->r:Z

    if-eqz p1, :cond_0

    .line 821
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d$8;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d$8;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/d/d;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 834
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v1, 0x64

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 836
    :goto_0
    sget-object p1, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->o:Landroid/util/SparseIntArray;

    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k:I

    invoke-virtual {p1, v0}, Landroid/util/SparseIntArray;->delete(I)V

    .line 837
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz p1, :cond_1

    .line 838
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v0, 0x131

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 840
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->v()V

    return-void
.end method

.method public b(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 946
    :try_start_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    const/4 v0, 0x0

    invoke-interface {p1, v0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(FF)V

    goto :goto_1

    :catch_0
    move-exception p1

    goto :goto_0

    .line 948
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p1, v0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/c;->a(FF)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :goto_0
    const-string v0, "SSMediaPlayeWrapper"

    const-string v1, "setQuietPlay error: "

    .line 951
    invoke-static {v0, v1, p1}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method public b(Lcom/bytedance/sdk/openadsdk/core/video/d/c;II)Z
    .locals 3

    const-string v0, "SSMediaPlayeWrapper"

    .line 782
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "what,extra:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    const/4 v1, 0x0

    if-eq v0, p1, :cond_0

    return v1

    .line 786
    :cond_0
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz p1, :cond_1

    .line 787
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v0, 0x130

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    const/16 p1, -0x3ec

    if-ne p3, p1, :cond_1

    .line 789
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v0, 0x12f

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    .line 792
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(II)V

    return v1
.end method

.method public c()V
    .locals 3

    const/16 v0, 0xcb

    .line 245
    iput v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    .line 246
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->D()V

    .line 250
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->z()V

    .line 251
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "release"

    .line 253
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 255
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->a:Lcom/bytedance/sdk/openadsdk/core/video/d/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 256
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->e:Z

    .line 257
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 260
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->t()V

    const-string v1, "SSMediaPlayeWrapper"

    const-string v2, "release error: "

    .line 261
    invoke-static {v1, v2, v0}, Lcom/bytedance/sdk/openadsdk/utils/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public c(Lcom/bytedance/sdk/openadsdk/core/video/d/c;)V
    .locals 1

    .line 854
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    if-eqz p1, :cond_0

    .line 855
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i:Landroid/os/Handler;

    const/16 v0, 0x132

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .line 288
    new-instance v0, Lcom/bytedance/sdk/openadsdk/core/video/d/d$3;

    invoke-direct {v0, p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d$3;-><init>(Lcom/bytedance/sdk/openadsdk/core/video/d/d;)V

    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public e()V
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v1, 0x6c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .line 352
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v1, 0x6d

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public g()Z
    .locals 2

    .line 382
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    const/16 v1, 0xce

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->r:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public h()Z
    .locals 1

    .line 386
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->k()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->g()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public i()Z
    .locals 2

    .line 390
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->r:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->h:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public j()Z
    .locals 2

    .line 394
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    const/16 v1, 0xcb

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public k()Z
    .locals 2

    .line 398
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    const/16 v1, 0xcd

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public l()Z
    .locals 2

    .line 402
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->f:I

    const/16 v1, 0xd1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public m()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 989
    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->v:J

    .line 990
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    return-void
.end method

.method public n()J
    .locals 2

    .line 1004
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->y:J

    return-wide v0
.end method

.method public o()J
    .locals 2

    .line 1008
    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->D()V

    .line 1009
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->v:J

    return-wide v0
.end method

.method public p()J
    .locals 6

    .line 1013
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 1014
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->v:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->v:J

    .line 1015
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->w:J

    .line 1017
    :cond_0
    iget-wide v0, p0, Lcom/bytedance/sdk/openadsdk/core/video/d/d;->v:J

    return-wide v0
.end method
