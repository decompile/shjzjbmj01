.class public Lcom/bytedance/sdk/openadsdk/core/t;
.super Ljava/lang/Object;
.source "SecSdkHelperInner.java"


# static fields
.field private static a:Lcom/bytedance/sdk/openadsdk/core/t;


# instance fields
.field private b:Lcom/pgl/sys/ces/d/b;

.field private volatile c:Z

.field private d:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 4

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/t;->c:Z

    const/4 v1, 0x0

    .line 32
    iput-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/t;->d:Ljava/lang/String;

    .line 40
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v1

    const-string v2, "df979cdb-05a7-448c-bece-92d5005a1247"

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/core/t;->d()Lcom/pgl/sys/ces/d/a;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lcom/pgl/sys/ces/d/c;->a(Landroid/content/Context;Ljava/lang/String;ILcom/pgl/sys/ces/d/a;)Lcom/pgl/sys/ces/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/t;->b:Lcom/pgl/sys/ces/d/b;

    .line 45
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "app_id"

    .line 47
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/sdk/openadsdk/core/i;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/t;->b:Lcom/pgl/sys/ces/d/b;

    invoke-interface {v1, v0}, Lcom/pgl/sys/ces/d/b;->setCustomInfo(Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method public static b()Lcom/bytedance/sdk/openadsdk/core/t;
    .locals 2

    .line 160
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/t;->a:Lcom/bytedance/sdk/openadsdk/core/t;

    if-nez v0, :cond_1

    .line 161
    const-class v0, Lcom/bytedance/sdk/openadsdk/core/t;

    monitor-enter v0

    .line 162
    :try_start_0
    sget-object v1, Lcom/bytedance/sdk/openadsdk/core/t;->a:Lcom/bytedance/sdk/openadsdk/core/t;

    if-nez v1, :cond_0

    .line 163
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/t;

    invoke-direct {v1}, Lcom/bytedance/sdk/openadsdk/core/t;-><init>()V

    sput-object v1, Lcom/bytedance/sdk/openadsdk/core/t;->a:Lcom/bytedance/sdk/openadsdk/core/t;

    .line 165
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 167
    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/sdk/openadsdk/core/t;->a:Lcom/bytedance/sdk/openadsdk/core/t;

    return-object v0
.end method

.method private d()Lcom/pgl/sys/ces/d/a;
    .locals 2

    .line 53
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/core/i;->e()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v0

    .line 54
    new-instance v1, Lcom/bytedance/sdk/openadsdk/core/t$1;

    invoke-direct {v1, p0, v0}, Lcom/bytedance/sdk/openadsdk/core/t$1;-><init>(Lcom/bytedance/sdk/openadsdk/core/t;Lcom/bytedance/sdk/openadsdk/TTCustomController;)V

    return-object v1
.end method

.method private d(Ljava/lang/String;)Z
    .locals 5

    .line 212
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, ":"

    .line 213
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 215
    array-length v0, p1

    const/16 v2, 0x14

    if-lt v0, v2, :cond_1

    .line 216
    array-length v0, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    const-string v4, "00"

    .line 217
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return v1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/t;->b:Lcom/pgl/sys/ces/d/b;

    invoke-interface {v0}, Lcom/pgl/sys/ces/d/b;->onEvent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .line 140
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 144
    :cond_0
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/t;->d:Ljava/lang/String;

    .line 145
    iget-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/t;->b:Lcom/pgl/sys/ces/d/b;

    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/t;->d:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/pgl/sys/ces/d/b;->setParams(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/sdk/openadsdk/core/i;->g()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 148
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    const-string v0, "app_id"

    .line 149
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/i;->d()Lcom/bytedance/sdk/openadsdk/core/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/i;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/t;->b:Lcom/pgl/sys/ces/d/b;

    invoke-interface {v0, p1}, Lcom/pgl/sys/ces/d/b;->setCustomInfo(Ljava/util/HashMap;)V

    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 176
    iget-boolean v0, p0, Lcom/bytedance/sdk/openadsdk/core/t;->c:Z

    if-eqz v0, :cond_0

    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/t;->b:Lcom/pgl/sys/ces/d/b;

    invoke-interface {v0, p1}, Lcom/pgl/sys/ces/d/b;->reportNow(Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 180
    iput-boolean p1, p0, Lcom/bytedance/sdk/openadsdk/core/t;->c:Z

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/t;->b:Lcom/pgl/sys/ces/d/b;

    invoke-interface {v0}, Lcom/pgl/sys/ces/d/b;->pullSg()Ljava/lang/String;

    move-result-object v0

    .line 190
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/t;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 193
    :cond_0
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/core/p;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/utils/e;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 194
    invoke-direct {p0, v0}, Lcom/bytedance/sdk/openadsdk/core/t;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const-string v0, ""

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 236
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 239
    :cond_0
    invoke-static {p1}, Lcom/bytedance/sdk/openadsdk/utils/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 240
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, ""

    return-object p1

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/bytedance/sdk/openadsdk/core/t;->b:Lcom/pgl/sys/ces/d/b;

    invoke-interface {v0, p1}, Lcom/pgl/sys/ces/d/b;->pullVer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
