.class Lcom/bytedance/sdk/openadsdk/core/r$4;
.super Lcom/bytedance/sdk/openadsdk/l/g;
.source "NetApiImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/sdk/openadsdk/core/r;->a(JLjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/r$a;ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:I

.field final synthetic d:Lcom/bytedance/sdk/openadsdk/core/r$a;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:J

.field final synthetic g:Lcom/bytedance/sdk/openadsdk/core/r;


# direct methods
.method constructor <init>(Lcom/bytedance/sdk/openadsdk/core/r;Ljava/lang/String;ILjava/lang/String;ILcom/bytedance/sdk/openadsdk/core/r$a;Ljava/lang/String;J)V
    .locals 0

    .line 583
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->g:Lcom/bytedance/sdk/openadsdk/core/r;

    iput p3, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->a:I

    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->b:Ljava/lang/String;

    iput p5, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->c:I

    iput-object p6, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    iput-object p7, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->e:Ljava/lang/String;

    iput-wide p8, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->f:J

    invoke-direct {p0, p2}, Lcom/bytedance/sdk/openadsdk/l/g;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 586
    iget v0, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->a:I

    .line 587
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/bytedance/sdk/openadsdk/core/h;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->b:Ljava/lang/String;

    .line 588
    :goto_0
    new-instance v2, Lcom/bytedance/sdk/openadsdk/h/a/b;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/h/a/b;-><init>()V

    iget v3, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->c:I

    .line 589
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/h/a/b;->a(I)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v2

    check-cast v2, Lcom/bytedance/sdk/openadsdk/h/a/b;

    .line 590
    invoke-virtual {v2, v0}, Lcom/bytedance/sdk/openadsdk/h/a/b;->b(I)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/h/a/b;

    .line 591
    invoke-virtual {v0, v1}, Lcom/bytedance/sdk/openadsdk/h/a/b;->g(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/openadsdk/h/a/b;

    const/4 v1, 0x0

    :try_start_0
    const-string v2, ""

    const-string v3, ""

    .line 597
    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    .line 598
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    .line 599
    invoke-virtual {v4}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 600
    iget-object v1, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    iget-object v1, v1, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->c()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/core/d/l;

    .line 601
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->aj()Ljava/lang/String;

    move-result-object v2

    .line 602
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v4, :cond_1

    .line 604
    :try_start_1
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v5, "req_id"

    .line 605
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-object v3, v4

    .line 611
    :catch_0
    :cond_1
    :try_start_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    iget-object v4, v4, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    if-eqz v4, :cond_2

    .line 612
    iget-object v3, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    iget-object v3, v3, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    invoke-virtual {v3}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a()Ljava/lang/String;

    move-result-object v3

    .line 615
    :cond_2
    invoke-virtual {v0, v3}, Lcom/bytedance/sdk/openadsdk/h/a/b;->f(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v3

    check-cast v3, Lcom/bytedance/sdk/openadsdk/h/a/b;

    if-eqz v1, :cond_3

    .line 616
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/core/d/l;->ag()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string v1, ""

    :goto_1
    invoke-virtual {v3, v1}, Lcom/bytedance/sdk/openadsdk/h/a/b;->d(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/h/a/b;

    .line 617
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/h/a/b;->h(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/h/a/b;

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->e:Ljava/lang/String;

    .line 618
    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/h/a/b;->c(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/h/a/c;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/h/a/b;

    iget-wide v2, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->f:J

    .line 619
    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/h/a/b;->a(J)Lcom/bytedance/sdk/openadsdk/h/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/bytedance/sdk/openadsdk/core/r$4;->d:Lcom/bytedance/sdk/openadsdk/core/r$a;

    iget v2, v2, Lcom/bytedance/sdk/openadsdk/core/r$a;->a:I

    int-to-long v2, v2

    goto :goto_2

    :cond_4
    const-wide/16 v2, 0x0

    .line 620
    :goto_2
    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/h/a/b;->b(J)Lcom/bytedance/sdk/openadsdk/h/a/b;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    move-exception v1

    const-string v2, "NetApiImpl"

    const-string v3, "uploadAdTypeTimeOutEvent throws exception "

    .line 622
    invoke-static {v2, v3, v1}, Lcom/bytedance/sdk/openadsdk/utils/u;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 624
    :goto_3
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/h/a;->a()Lcom/bytedance/sdk/openadsdk/h/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bytedance/sdk/openadsdk/h/a;->e(Lcom/bytedance/sdk/openadsdk/h/a/c;)V

    return-void
.end method
