.class public Lcom/bytedance/sdk/openadsdk/core/r$a;
.super Ljava/lang/Object;
.source "NetApiImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/core/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field final a:I

.field final b:J

.field final c:J

.field final d:I

.field final e:Ljava/lang/String;

.field final f:I

.field final g:Ljava/lang/String;

.field public final h:Lcom/bytedance/sdk/openadsdk/core/d/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field final i:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;ILjava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/a;JJ)V
    .locals 0
    .param p7    # Lcom/bytedance/sdk/openadsdk/core/d/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1257
    iput p2, p0, Lcom/bytedance/sdk/openadsdk/core/r$a;->a:I

    .line 1258
    iput p3, p0, Lcom/bytedance/sdk/openadsdk/core/r$a;->d:I

    .line 1259
    iput-object p4, p0, Lcom/bytedance/sdk/openadsdk/core/r$a;->e:Ljava/lang/String;

    .line 1260
    iput-object p6, p0, Lcom/bytedance/sdk/openadsdk/core/r$a;->g:Ljava/lang/String;

    .line 1261
    iput-object p7, p0, Lcom/bytedance/sdk/openadsdk/core/r$a;->h:Lcom/bytedance/sdk/openadsdk/core/d/a;

    .line 1262
    iput-object p1, p0, Lcom/bytedance/sdk/openadsdk/core/r$a;->i:Ljava/lang/String;

    .line 1263
    iput p5, p0, Lcom/bytedance/sdk/openadsdk/core/r$a;->f:I

    .line 1264
    iput-wide p8, p0, Lcom/bytedance/sdk/openadsdk/core/r$a;->b:J

    .line 1265
    iput-wide p10, p0, Lcom/bytedance/sdk/openadsdk/core/r$a;->c:J

    return-void
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/bytedance/sdk/openadsdk/core/r$a;
    .locals 1

    const/4 v0, 0x0

    .line 1269
    invoke-static {p0, v0, v0}, Lcom/bytedance/sdk/openadsdk/core/r$a;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/d/m;)Lcom/bytedance/sdk/openadsdk/core/r$a;

    move-result-object p0

    return-object p0
.end method

.method public static a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/d/m;)Lcom/bytedance/sdk/openadsdk/core/r$a;
    .locals 13

    const-string v0, "did"

    .line 1273
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "processing_time_ms"

    .line 1274
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    const-string v0, "s_receive_ts"

    .line 1275
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v9

    const-string v0, "s_send_ts"

    .line 1276
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v11

    const-string v0, "status_code"

    .line 1277
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    const-string v0, "desc"

    .line 1278
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "request_id"

    .line 1279
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "reason"

    .line 1280
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    .line 1281
    invoke-static {p0, p1, p2}, Lcom/bytedance/sdk/openadsdk/core/c;->a(Lorg/json/JSONObject;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/core/d/m;)Lcom/bytedance/sdk/openadsdk/core/d/a;

    move-result-object v8

    if-eqz v8, :cond_0

    const-string p1, "request_after"

    .line 1284
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide p0

    invoke-virtual {v8, p0, p1}, Lcom/bytedance/sdk/openadsdk/core/d/a;->a(J)V

    .line 1286
    :cond_0
    new-instance p0, Lcom/bytedance/sdk/openadsdk/core/r$a;

    move-object v1, p0

    invoke-direct/range {v1 .. v12}, Lcom/bytedance/sdk/openadsdk/core/r$a;-><init>(Ljava/lang/String;IILjava/lang/String;ILjava/lang/String;Lcom/bytedance/sdk/openadsdk/core/d/a;JJ)V

    return-object p0
.end method
