.class public final Lcom/bytedance/sdk/openadsdk/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/sdk/openadsdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I

.field public static final tt_00_00:I

.field public static final tt_ad:I

.field public static final tt_ad_logo_txt:I

.field public static final tt_app_name:I

.field public static final tt_app_privacy_dialog_title:I

.field public static final tt_appdownloader_button_cancel_download:I

.field public static final tt_appdownloader_button_queue_for_wifi:I

.field public static final tt_appdownloader_button_start_now:I

.field public static final tt_appdownloader_download_percent:I

.field public static final tt_appdownloader_download_remaining:I

.field public static final tt_appdownloader_download_unknown_title:I

.field public static final tt_appdownloader_duration_hours:I

.field public static final tt_appdownloader_duration_minutes:I

.field public static final tt_appdownloader_duration_seconds:I

.field public static final tt_appdownloader_jump_unknown_source:I

.field public static final tt_appdownloader_label_cancel:I

.field public static final tt_appdownloader_label_cancel_directly:I

.field public static final tt_appdownloader_label_ok:I

.field public static final tt_appdownloader_label_reserve_wifi:I

.field public static final tt_appdownloader_notification_download:I

.field public static final tt_appdownloader_notification_download_complete_open:I

.field public static final tt_appdownloader_notification_download_complete_with_install:I

.field public static final tt_appdownloader_notification_download_complete_without_install:I

.field public static final tt_appdownloader_notification_download_continue:I

.field public static final tt_appdownloader_notification_download_delete:I

.field public static final tt_appdownloader_notification_download_failed:I

.field public static final tt_appdownloader_notification_download_install:I

.field public static final tt_appdownloader_notification_download_open:I

.field public static final tt_appdownloader_notification_download_pause:I

.field public static final tt_appdownloader_notification_download_restart:I

.field public static final tt_appdownloader_notification_download_resume:I

.field public static final tt_appdownloader_notification_download_space_failed:I

.field public static final tt_appdownloader_notification_download_waiting_net:I

.field public static final tt_appdownloader_notification_download_waiting_wifi:I

.field public static final tt_appdownloader_notification_downloading:I

.field public static final tt_appdownloader_notification_install_finished_open:I

.field public static final tt_appdownloader_notification_insufficient_space_error:I

.field public static final tt_appdownloader_notification_need_wifi_for_size:I

.field public static final tt_appdownloader_notification_no_internet_error:I

.field public static final tt_appdownloader_notification_no_wifi_and_in_net:I

.field public static final tt_appdownloader_notification_paused_in_background:I

.field public static final tt_appdownloader_notification_pausing:I

.field public static final tt_appdownloader_notification_prepare:I

.field public static final tt_appdownloader_notification_request_btn_no:I

.field public static final tt_appdownloader_notification_request_btn_yes:I

.field public static final tt_appdownloader_notification_request_message:I

.field public static final tt_appdownloader_notification_request_title:I

.field public static final tt_appdownloader_notification_waiting_download_complete_handler:I

.field public static final tt_appdownloader_resume_in_wifi:I

.field public static final tt_appdownloader_tip:I

.field public static final tt_appdownloader_wifi_recommended_body:I

.field public static final tt_appdownloader_wifi_recommended_title:I

.field public static final tt_appdownloader_wifi_required_body:I

.field public static final tt_appdownloader_wifi_required_title:I

.field public static final tt_auto_play_cancel_text:I

.field public static final tt_cancel:I

.field public static final tt_comment_num:I

.field public static final tt_comment_num_backup:I

.field public static final tt_comment_score:I

.field public static final tt_common_download_app_detail:I

.field public static final tt_common_download_app_privacy:I

.field public static final tt_common_download_cancel:I

.field public static final tt_confirm_download:I

.field public static final tt_confirm_download_have_app_name:I

.field public static final tt_dislike_header_tv_back:I

.field public static final tt_dislike_header_tv_title:I

.field public static final tt_full_screen_skip_tx:I

.field public static final tt_label_cancel:I

.field public static final tt_label_ok:I

.field public static final tt_no_network:I

.field public static final tt_open_app_detail_developer:I

.field public static final tt_open_app_detail_privacy:I

.field public static final tt_open_app_detail_privacy_list:I

.field public static final tt_open_app_name:I

.field public static final tt_open_app_version:I

.field public static final tt_open_landing_page_app_name:I

.field public static final tt_permission_denied:I

.field public static final tt_playable_btn_play:I

.field public static final tt_request_permission_descript_external_storage:I

.field public static final tt_request_permission_descript_location:I

.field public static final tt_request_permission_descript_read_phone_state:I

.field public static final tt_reward_feedback:I

.field public static final tt_reward_screen_skip_tx:I

.field public static final tt_splash_skip_tv_text:I

.field public static final tt_tip:I

.field public static final tt_unlike:I

.field public static final tt_video_bytesize:I

.field public static final tt_video_bytesize_M:I

.field public static final tt_video_bytesize_MB:I

.field public static final tt_video_continue_play:I

.field public static final tt_video_dial_phone:I

.field public static final tt_video_dial_replay:I

.field public static final tt_video_download_apk:I

.field public static final tt_video_mobile_go_detail:I

.field public static final tt_video_retry_des_txt:I

.field public static final tt_video_without_wifi_tips:I

.field public static final tt_web_title_default:I

.field public static final tt_will_play:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 481
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->app_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->app_name:I

    .line 482
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_00_00:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_00_00:I

    .line 483
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_ad:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_ad:I

    .line 484
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_ad_logo_txt:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_ad_logo_txt:I

    .line 485
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_app_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_app_name:I

    .line 486
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_app_privacy_dialog_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_app_privacy_dialog_title:I

    .line 487
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_button_cancel_download:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_button_cancel_download:I

    .line 488
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_button_queue_for_wifi:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_button_queue_for_wifi:I

    .line 489
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_button_start_now:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_button_start_now:I

    .line 490
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_download_percent:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_download_percent:I

    .line 491
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_download_remaining:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_download_remaining:I

    .line 492
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_download_unknown_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_download_unknown_title:I

    .line 493
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_duration_hours:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_duration_hours:I

    .line 494
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_duration_minutes:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_duration_minutes:I

    .line 495
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_duration_seconds:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_duration_seconds:I

    .line 496
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_jump_unknown_source:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_jump_unknown_source:I

    .line 497
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_label_cancel:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_label_cancel:I

    .line 498
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_label_cancel_directly:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_label_cancel_directly:I

    .line 499
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_label_ok:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_label_ok:I

    .line 500
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_label_reserve_wifi:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_label_reserve_wifi:I

    .line 501
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download:I

    .line 502
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_complete_open:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_complete_open:I

    .line 503
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_complete_with_install:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_complete_with_install:I

    .line 504
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_complete_without_install:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_complete_without_install:I

    .line 505
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_continue:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_continue:I

    .line 506
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_delete:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_delete:I

    .line 507
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_failed:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_failed:I

    .line 508
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_install:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_install:I

    .line 509
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_open:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_open:I

    .line 510
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_pause:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_pause:I

    .line 511
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_restart:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_restart:I

    .line 512
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_resume:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_resume:I

    .line 513
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_space_failed:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_space_failed:I

    .line 514
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_waiting_net:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_waiting_net:I

    .line 515
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_waiting_wifi:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_download_waiting_wifi:I

    .line 516
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_downloading:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_downloading:I

    .line 517
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_install_finished_open:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_install_finished_open:I

    .line 518
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_insufficient_space_error:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_insufficient_space_error:I

    .line 519
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_need_wifi_for_size:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_need_wifi_for_size:I

    .line 520
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_no_internet_error:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_no_internet_error:I

    .line 521
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_no_wifi_and_in_net:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_no_wifi_and_in_net:I

    .line 522
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_paused_in_background:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_paused_in_background:I

    .line 523
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_pausing:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_pausing:I

    .line 524
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_prepare:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_prepare:I

    .line 525
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_request_btn_no:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_request_btn_no:I

    .line 526
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_request_btn_yes:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_request_btn_yes:I

    .line 527
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_request_message:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_request_message:I

    .line 528
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_request_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_request_title:I

    .line 529
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_waiting_download_complete_handler:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_notification_waiting_download_complete_handler:I

    .line 530
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_resume_in_wifi:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_resume_in_wifi:I

    .line 531
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_tip:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_tip:I

    .line 532
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_wifi_recommended_body:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_wifi_recommended_body:I

    .line 533
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_wifi_recommended_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_wifi_recommended_title:I

    .line 534
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_wifi_required_body:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_wifi_required_body:I

    .line 535
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_wifi_required_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_appdownloader_wifi_required_title:I

    .line 536
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_auto_play_cancel_text:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_auto_play_cancel_text:I

    .line 537
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_cancel:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_cancel:I

    .line 538
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_comment_num:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_comment_num:I

    .line 539
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_comment_num_backup:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_comment_num_backup:I

    .line 540
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_comment_score:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_comment_score:I

    .line 541
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_common_download_app_detail:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_common_download_app_detail:I

    .line 542
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_common_download_app_privacy:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_common_download_app_privacy:I

    .line 543
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_common_download_cancel:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_common_download_cancel:I

    .line 544
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_confirm_download:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_confirm_download:I

    .line 545
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_confirm_download_have_app_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_confirm_download_have_app_name:I

    .line 546
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_dislike_header_tv_back:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_dislike_header_tv_back:I

    .line 547
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_dislike_header_tv_title:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_dislike_header_tv_title:I

    .line 548
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_full_screen_skip_tx:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_full_screen_skip_tx:I

    .line 549
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_label_cancel:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_label_cancel:I

    .line 550
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_label_ok:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_label_ok:I

    .line 551
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_no_network:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_no_network:I

    .line 552
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_open_app_detail_developer:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_open_app_detail_developer:I

    .line 553
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_open_app_detail_privacy:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_open_app_detail_privacy:I

    .line 554
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_open_app_detail_privacy_list:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_open_app_detail_privacy_list:I

    .line 555
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_open_app_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_open_app_name:I

    .line 556
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_open_app_version:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_open_app_version:I

    .line 557
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_open_landing_page_app_name:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_open_landing_page_app_name:I

    .line 558
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_permission_denied:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_permission_denied:I

    .line 559
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_playable_btn_play:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_playable_btn_play:I

    .line 560
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_request_permission_descript_external_storage:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_request_permission_descript_external_storage:I

    .line 561
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_request_permission_descript_location:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_request_permission_descript_location:I

    .line 562
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_request_permission_descript_read_phone_state:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_request_permission_descript_read_phone_state:I

    .line 563
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_reward_feedback:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_reward_feedback:I

    .line 564
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_reward_screen_skip_tx:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_reward_screen_skip_tx:I

    .line 565
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_splash_skip_tv_text:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_splash_skip_tv_text:I

    .line 566
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_tip:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_tip:I

    .line 567
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_unlike:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_unlike:I

    .line 568
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_bytesize:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_bytesize:I

    .line 569
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_bytesize_M:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_bytesize_M:I

    .line 570
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_bytesize_MB:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_bytesize_MB:I

    .line 571
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_continue_play:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_continue_play:I

    .line 572
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_dial_phone:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_dial_phone:I

    .line 573
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_dial_replay:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_dial_replay:I

    .line 574
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_download_apk:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_download_apk:I

    .line 575
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_mobile_go_detail:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_mobile_go_detail:I

    .line 576
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_retry_des_txt:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_retry_des_txt:I

    .line 577
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_video_without_wifi_tips:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_video_without_wifi_tips:I

    .line 578
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_web_title_default:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_web_title_default:I

    .line 579
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_will_play:I

    sput v0, Lcom/bytedance/sdk/openadsdk/R$string;->tt_will_play:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
