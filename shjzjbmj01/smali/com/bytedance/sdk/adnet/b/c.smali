.class public Lcom/bytedance/sdk/adnet/b/c;
.super Lcom/bytedance/sdk/adnet/core/Request;
.source "FileRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/sdk/adnet/b/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bytedance/sdk/adnet/core/Request<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private c:Ljava/io/File;

.field private d:Ljava/io/File;

.field private final e:Ljava/lang/Object;

.field private f:Lcom/bytedance/sdk/adnet/core/m$a;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation

    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bytedance/sdk/adnet/core/m$a<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/sdk/adnet/core/m$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/bytedance/sdk/adnet/core/m$a<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .line 80
    invoke-direct {p0, p2, p3}, Lcom/bytedance/sdk/adnet/core/Request;-><init>(Ljava/lang/String;Lcom/bytedance/sdk/adnet/core/m$a;)V

    .line 65
    new-instance p2, Ljava/lang/Object;

    invoke-direct {p2}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/bytedance/sdk/adnet/b/c;->e:Ljava/lang/Object;

    .line 81
    iput-object p3, p0, Lcom/bytedance/sdk/adnet/b/c;->f:Lcom/bytedance/sdk/adnet/core/m$a;

    .line 82
    new-instance p2, Ljava/io/File;

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    .line 83
    new-instance p2, Ljava/io/File;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".tmp"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/bytedance/sdk/adnet/b/c;->d:Ljava/io/File;

    .line 86
    :try_start_0
    iget-object p1, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    .line 87
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_0

    .line 88
    iget-object p1, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :catch_0
    :cond_0
    new-instance p1, Lcom/bytedance/sdk/adnet/core/e;

    const/16 p2, 0x61a8

    const/4 p3, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p1, p2, p3, v0}, Lcom/bytedance/sdk/adnet/core/e;-><init>(IIF)V

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/adnet/b/c;->setRetryPolicy(Lcom/bytedance/sdk/adnet/face/d;)Lcom/bytedance/sdk/adnet/core/Request;

    const/4 p1, 0x0

    .line 98
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/adnet/b/c;->setShouldCache(Z)Lcom/bytedance/sdk/adnet/core/Request;

    return-void
.end method

.method private a(Lcom/bytedance/sdk/adnet/core/HttpResponse;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_1

    .line 292
    invoke-virtual {p1}, Lcom/bytedance/sdk/adnet/core/HttpResponse;->getHeaders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/bytedance/sdk/adnet/core/HttpResponse;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 293
    invoke-virtual {p1}, Lcom/bytedance/sdk/adnet/core/HttpResponse;->getHeaders()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/sdk/adnet/core/Header;

    if-eqz v0, :cond_0

    .line 294
    invoke-virtual {v0}, Lcom/bytedance/sdk/adnet/core/Header;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    invoke-virtual {v0}, Lcom/bytedance/sdk/adnet/core/Header;->getValue()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private b(Lcom/bytedance/sdk/adnet/core/HttpResponse;)Z
    .locals 1

    const-string v0, "Content-Encoding"

    .line 280
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/adnet/b/c;->a(Lcom/bytedance/sdk/adnet/core/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "gzip"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method

.method private c(Lcom/bytedance/sdk/adnet/core/HttpResponse;)Z
    .locals 2

    const-string v0, "Accept-Ranges"

    .line 284
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/adnet/b/c;->a(Lcom/bytedance/sdk/adnet/core/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "bytes"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "Content-Range"

    .line 287
    invoke-direct {p0, p1, v0}, Lcom/bytedance/sdk/adnet/b/c;->a(Lcom/bytedance/sdk/adnet/core/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    const-string v0, "bytes"

    .line 288
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private h()V
    .locals 1

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :catch_0
    :try_start_1
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method


# virtual methods
.method protected a(Lcom/bytedance/sdk/adnet/core/i;)Lcom/bytedance/sdk/adnet/core/m;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/adnet/core/i;",
            ")",
            "Lcom/bytedance/sdk/adnet/core/m<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 134
    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 136
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->d:Ljava/io/File;

    iget-object v1, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 137
    invoke-static {p1}, Lcom/bytedance/sdk/adnet/d/c;->a(Lcom/bytedance/sdk/adnet/core/i;)Lcom/bytedance/sdk/adnet/face/a$a;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/sdk/adnet/core/m;->a(Ljava/lang/Object;Lcom/bytedance/sdk/adnet/face/a$a;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object p1

    return-object p1

    .line 139
    :cond_0
    invoke-direct {p0}, Lcom/bytedance/sdk/adnet/b/c;->h()V

    .line 140
    new-instance p1, Lcom/bytedance/sdk/adnet/err/VAdError;

    const-string v0, "Can\'t rename the download temporary file!"

    const/16 v1, 0x261

    invoke-direct {p1, v0, v1}, Lcom/bytedance/sdk/adnet/err/VAdError;-><init>(Ljava/lang/String;I)V

    invoke-static {p1}, Lcom/bytedance/sdk/adnet/core/m;->a(Lcom/bytedance/sdk/adnet/err/VAdError;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object p1

    return-object p1

    .line 144
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/sdk/adnet/b/c;->h()V

    .line 145
    new-instance p1, Lcom/bytedance/sdk/adnet/err/VAdError;

    const-string v0, "Download temporary file was invalid!"

    const/16 v1, 0x262

    invoke-direct {p1, v0, v1}, Lcom/bytedance/sdk/adnet/err/VAdError;-><init>(Ljava/lang/String;I)V

    invoke-static {p1}, Lcom/bytedance/sdk/adnet/core/m;->a(Lcom/bytedance/sdk/adnet/err/VAdError;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object p1

    return-object p1

    .line 149
    :cond_2
    invoke-direct {p0}, Lcom/bytedance/sdk/adnet/b/c;->h()V

    .line 150
    new-instance p1, Lcom/bytedance/sdk/adnet/err/VAdError;

    const-string v0, "Request was Canceled!"

    const/16 v1, 0x263

    invoke-direct {p1, v0, v1}, Lcom/bytedance/sdk/adnet/err/VAdError;-><init>(Ljava/lang/String;I)V

    invoke-static {p1}, Lcom/bytedance/sdk/adnet/core/m;->a(Lcom/bytedance/sdk/adnet/err/VAdError;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object p1

    return-object p1
.end method

.method protected a(JJ)V
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 319
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/adnet/b/c;->f:Lcom/bytedance/sdk/adnet/core/m$a;

    .line 320
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    instance-of v0, v1, Lcom/bytedance/sdk/adnet/b/c$a;

    if-eqz v0, :cond_0

    .line 323
    check-cast v1, Lcom/bytedance/sdk/adnet/b/c$a;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/bytedance/sdk/adnet/b/c$a;->a(JJ)V

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    .line 320
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method protected a(Lcom/bytedance/sdk/adnet/core/m;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/sdk/adnet/core/m<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .line 305
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 306
    :try_start_0
    iget-object v1, p0, Lcom/bytedance/sdk/adnet/b/c;->f:Lcom/bytedance/sdk/adnet/core/m$a;

    .line 307
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 310
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    iget-object p1, p1, Lcom/bytedance/sdk/adnet/core/m;->b:Lcom/bytedance/sdk/adnet/face/a$a;

    invoke-static {v0, p1}, Lcom/bytedance/sdk/adnet/core/m;->a(Ljava/lang/Object;Lcom/bytedance/sdk/adnet/face/a$a;)Lcom/bytedance/sdk/adnet/core/m;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/bytedance/sdk/adnet/core/m$a;->a(Lcom/bytedance/sdk/adnet/core/m;)V

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    .line 307
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public a(Lcom/bytedance/sdk/adnet/core/HttpResponse;)[B
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/bytedance/sdk/adnet/err/f;
        }
    .end annotation

    .line 169
    invoke-virtual {p1}, Lcom/bytedance/sdk/adnet/core/HttpResponse;->getContentLength()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    const/4 v5, 0x0

    if-gtz v4, :cond_0

    const-string v4, "Response doesn\'t present Content-Length!"

    .line 171
    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v4, v6}, Lcom/bytedance/sdk/adnet/core/o;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->g()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 175
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/adnet/b/c;->c(Lcom/bytedance/sdk/adnet/core/HttpResponse;)Z

    move-result v4

    const/4 v8, -0x1

    if-eqz v4, :cond_2

    add-long/2addr v0, v6

    const-string v9, "Content-Range"

    .line 182
    invoke-direct {p0, p1, v9}, Lcom/bytedance/sdk/adnet/b/c;->a(Lcom/bytedance/sdk/adnet/core/HttpResponse;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 184
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 185
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "bytes "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v11, "-"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v11, 0x1

    sub-long v11, v0, v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 186
    invoke-static {v9, v10}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v11

    if-eq v11, v8, :cond_1

    goto :goto_0

    .line 187
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The Content-Range Header is invalid Assume["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] vs Real["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "], please remove the temporary file ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->g()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "]."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    cmp-long v9, v0, v2

    const/4 v10, 0x0

    if-lez v9, :cond_3

    .line 197
    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->f()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v11

    cmp-long v9, v11, v0

    if-nez v9, :cond_3

    .line 199
    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->f()Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->g()Ljava/io/File;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 201
    invoke-virtual {p0, v0, v1, v0, v1}, Lcom/bytedance/sdk/adnet/b/c;->a(JJ)V

    return-object v10

    .line 205
    :cond_3
    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->f()Ljava/io/File;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->f()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 206
    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->f()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 211
    :cond_4
    :try_start_0
    new-instance v9, Ljava/io/RandomAccessFile;

    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->g()Ljava/io/File;

    move-result-object v11

    const-string v12, "rw"

    invoke-direct {v9, v11, v12}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_5

    .line 215
    :try_start_1
    invoke-virtual {v9, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    goto :goto_1

    .line 218
    :cond_5
    invoke-virtual {v9, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-wide v6, v2

    goto :goto_1

    :catch_0
    move-object v9, v10

    .line 226
    :catch_1
    :goto_1
    :try_start_2
    invoke-virtual {p1}, Lcom/bytedance/sdk/adnet/core/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v2
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 228
    :try_start_3
    invoke-direct {p0, p1}, Lcom/bytedance/sdk/adnet/b/c;->b(Lcom/bytedance/sdk/adnet/core/HttpResponse;)Z

    move-result p1

    if-eqz p1, :cond_6

    instance-of p1, v2, Ljava/util/zip/GZIPInputStream;

    if-nez p1, :cond_6

    .line 229
    new-instance p1, Ljava/util/zip/GZIPInputStream;

    invoke-direct {p1, v2}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :cond_6
    move-object p1, v2

    :goto_2
    const/16 v2, 0x400

    .line 231
    :try_start_4
    new-array v2, v2, [B

    .line 235
    invoke-virtual {p0, v6, v7, v0, v1}, Lcom/bytedance/sdk/adnet/b/c;->a(JJ)V

    .line 237
    :cond_7
    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-eq v3, v8, :cond_8

    .line 238
    invoke-virtual {v9, v2, v5, v3}, Ljava/io/RandomAccessFile;->write([BII)V

    int-to-long v3, v3

    add-long/2addr v6, v3

    .line 243
    invoke-virtual {p0, v6, v7, v0, v1}, Lcom/bytedance/sdk/adnet/b/c;->a(JJ)V

    .line 245
    invoke-virtual {p0}, Lcom/bytedance/sdk/adnet/b/c;->isCanceled()Z

    move-result v3
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v3, :cond_7

    :cond_8
    if-eqz p1, :cond_9

    .line 254
    :try_start_5
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3

    :catch_2
    const-string v0, "Error occured when calling InputStream.close"

    .line 256
    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_9
    :goto_3
    if-eqz p1, :cond_b

    .line 262
    :try_start_6
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_6

    :catch_3
    const-string p1, "Error occured when calling consumingContent"

    .line 267
    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/bytedance/sdk/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    :catchall_0
    move-exception p1

    move-object v0, p1

    move-object p1, v2

    goto :goto_8

    :catch_4
    move-object p1, v2

    goto :goto_4

    :catchall_1
    move-exception p1

    move-object v0, p1

    move-object p1, v10

    goto :goto_8

    :catch_5
    move-object p1, v10

    :catch_6
    :goto_4
    :try_start_7
    const-string v0, "Error occured when FileRequest.parseHttpResponse"

    .line 250
    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz p1, :cond_a

    .line 254
    :try_start_8
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_7

    goto :goto_5

    :catch_7
    const-string v0, "Error occured when calling InputStream.close"

    .line 256
    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/bytedance/sdk/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_a
    :goto_5
    if-eqz p1, :cond_b

    .line 262
    :try_start_9
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_8

    goto :goto_6

    :catch_8
    const-string p1, "Error occured when calling consumingContent"

    .line 267
    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/bytedance/sdk/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    :cond_b
    :goto_6
    :try_start_a
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_9

    goto :goto_7

    :catch_9
    const-string p1, "Error occured when calling tmpFile.close"

    .line 272
    new-array v0, v5, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/bytedance/sdk/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_7
    return-object v10

    :catchall_2
    move-exception v0

    :goto_8
    if-eqz p1, :cond_c

    .line 254
    :try_start_b
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_a

    goto :goto_9

    :catch_a
    const-string v1, "Error occured when calling InputStream.close"

    .line 256
    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/bytedance/sdk/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_c
    :goto_9
    if-eqz p1, :cond_d

    .line 262
    :try_start_c
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_b

    goto :goto_a

    .line 267
    :catch_b
    new-array p1, v5, [Ljava/lang/Object;

    const-string v1, "Error occured when calling consumingContent"

    invoke-static {v1, p1}, Lcom/bytedance/sdk/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    :cond_d
    :goto_a
    :try_start_d
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_c

    goto :goto_b

    .line 272
    :catch_c
    new-array p1, v5, [Ljava/lang/Object;

    const-string v1, "Error occured when calling tmpFile.close"

    invoke-static {v1, p1}, Lcom/bytedance/sdk/adnet/core/o;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    :goto_b
    throw v0
.end method

.method public cancel()V
    .locals 2

    .line 111
    invoke-super {p0}, Lcom/bytedance/sdk/adnet/core/Request;->cancel()V

    .line 112
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->e:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 113
    :try_start_0
    iput-object v1, p0, Lcom/bytedance/sdk/adnet/b/c;->f:Lcom/bytedance/sdk/adnet/core/m$a;

    .line 114
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public f()Ljava/io/File;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->c:Ljava/io/File;

    return-object v0
.end method

.method public g()Ljava/io/File;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/bytedance/sdk/adnet/b/c;->d:Ljava/io/File;

    return-object v0
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/bytedance/sdk/adnet/err/a;
        }
    .end annotation

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "Range"

    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bytes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/bytedance/sdk/adnet/b/c;->d:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Accept-Encoding"

    const-string v2, "identity"

    .line 124
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public getPriority()Lcom/bytedance/sdk/adnet/core/Request$b;
    .locals 1

    .line 329
    sget-object v0, Lcom/bytedance/sdk/adnet/core/Request$b;->a:Lcom/bytedance/sdk/adnet/core/Request$b;

    return-object v0
.end method
