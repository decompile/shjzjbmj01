.class public Lcom/bytedance/applog/k;
.super Lcom/bytedance/applog/f;
.source ""


# static fields
.field public static final f:[J

.field public static final g:[J

.field public static final h:[J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xb

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/bytedance/applog/k;->f:[J

    const/16 v0, 0x8

    new-array v0, v0, [J

    fill-array-data v0, :array_1

    sput-object v0, Lcom/bytedance/applog/k;->g:[J

    const/16 v0, 0xe

    new-array v0, v0, [J

    fill-array-data v0, :array_2

    sput-object v0, Lcom/bytedance/applog/k;->h:[J

    return-void

    :array_0
    .array-data 8
        0xea60
        0xea60
        0xea60
        0x1d4c0
        0x1d4c0
        0x2bf20
        0x2bf20
        0x57e40
        0x57e40
        0x83d60
        0x83d60
    .end array-data

    :array_1
    .array-data 8
        0x2bf20
        0x2bf20
        0x57e40
        0x57e40
        0x83d60
        0x83d60
        0xafc80
        0xafc80
    .end array-data

    :array_2
    .array-data 8
        0x2710
        0x2710
        0x4e20
        0x4e20
        0xea60
        0xea60
        0x1d4c0
        0x1d4c0
        0x2bf20
        0x2bf20
        0x57e40
        0x57e40
        0x83d60
        0x83d60
    .end array-data
.end method

.method public constructor <init>(Lcom/bytedance/applog/h;)V
    .locals 4

    .line 1
    iget-object v0, p1, Lcom/bytedance/applog/h;->h:Lcom/bytedance/applog/y;

    .line 2
    iget-object v0, v0, Lcom/bytedance/applog/y;->d:Lorg/json/JSONObject;

    const-string v1, "register_time"

    const-wide/16 v2, 0x0

    .line 3
    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 4
    invoke-direct {p0, p1, v0, v1}, Lcom/bytedance/applog/f;-><init>(Lcom/bytedance/applog/h;J)V

    return-void
.end method


# virtual methods
.method public c()Z
    .locals 19

    move-object/from16 v1, p0

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    iget-object v0, v1, Lcom/bytedance/applog/f;->a:Lcom/bytedance/applog/h;

    .line 1
    iget-object v3, v0, Lcom/bytedance/applog/h;->h:Lcom/bytedance/applog/y;

    .line 2
    iget-object v0, v0, Lcom/bytedance/applog/h;->d:Lcom/bytedance/applog/x;

    .line 3
    iget-object v4, v0, Lcom/bytedance/applog/x;->b:Lcom/bytedance/applog/InitConfig;

    invoke-virtual {v4}, Lcom/bytedance/applog/InitConfig;->getPreInstallCallback()Lcom/bytedance/applog/w2;

    .line 4
    invoke-virtual {v3}, Lcom/bytedance/applog/y;->b()Lorg/json/JSONObject;

    move-result-object v4

    const/4 v5, 0x0

    if-eqz v4, :cond_2f

    .line 5
    iget-object v7, v0, Lcom/bytedance/applog/x;->b:Lcom/bytedance/applog/InitConfig;

    .line 6
    invoke-virtual {v7}, Lcom/bytedance/applog/InitConfig;->getCommonHeader()Ljava/util/Map;

    move-result-object v7

    .line 7
    iget-object v8, v0, Lcom/bytedance/applog/x;->b:Lcom/bytedance/applog/InitConfig;

    .line 8
    invoke-virtual {v8}, Lcom/bytedance/applog/InitConfig;->isAntiCheatingEnable()Z

    move-result v8

    const/4 v9, 0x1

    if-eqz v8, :cond_0

    invoke-static {}, Lcom/bytedance/applog/AppLog;->reportPhoneDetailInfo()Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    if-nez v7, :cond_1

    const/4 v8, 0x0

    goto :goto_0

    :cond_1
    const/4 v8, 0x1

    :goto_0
    if-eqz v8, :cond_2

    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    invoke-static {v8, v4}, Lcom/bytedance/applog/y1;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-object v4, v8

    .line 9
    :cond_2
    iget-object v0, v0, Lcom/bytedance/applog/x;->b:Lcom/bytedance/applog/InitConfig;

    .line 10
    invoke-virtual {v0}, Lcom/bytedance/applog/InitConfig;->isAntiCheatingEnable()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-static {}, Lcom/bytedance/applog/AppLog;->reportPhoneDetailInfo()Z

    move-result v0

    if-eqz v0, :cond_2b

    iget-object v0, v1, Lcom/bytedance/applog/f;->a:Lcom/bytedance/applog/h;

    .line 11
    iget-object v0, v0, Lcom/bytedance/applog/h;->c:Landroid/app/Application;

    if-nez v0, :cond_3

    goto/16 :goto_1c

    .line 12
    :cond_3
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    :try_start_0
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    if-nez v0, :cond_4

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    sput-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    const-string v0, "android.os.SystemProperties"

    .line 13
    :try_start_1
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    :goto_1
    const/4 v0, 0x0

    .line 14
    :goto_2
    sget-object v10, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v11, "band"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v10, :cond_5

    const-string v10, "gsm.version.baseband"

    .line 15
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v12, "get"

    :try_start_4
    new-array v13, v9, [Ljava/lang/Class;

    const-class v14, Ljava/lang/String;

    aput-object v14, v13, v5

    invoke-virtual {v11, v12, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    new-array v12, v9, [Ljava/lang/Object;

    aput-object v10, v12, v5

    invoke-virtual {v11, v0, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    :catch_3
    const-string v10, ""

    .line 16
    :goto_3
    :try_start_5
    sget-object v11, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v12, "band"

    invoke-static {v11, v12, v10}, Lcom/bytedance/applog/f1;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :catch_4
    move-exception v0

    goto/16 :goto_7

    .line 18
    :cond_5
    :goto_4
    sget-object v10, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v11, "props"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_7

    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    const/16 v11, 0x10

    new-array v11, v11, [Ljava/lang/String;

    const-string v12, "gsm.version.baseband"

    aput-object v12, v11, v5

    const-string v12, "ro.debuggable"

    aput-object v12, v11, v9

    const/4 v12, 0x2

    const-string v13, "ro.serialno"

    aput-object v13, v11, v12

    const/4 v12, 0x3

    const-string v13, "ro.boot.hardware"

    aput-object v13, v11, v12

    const/4 v12, 0x4

    const-string v13, "ro.build.tags"

    aput-object v13, v11, v12

    const/4 v12, 0x5

    const-string v13, "ro.build.date.utc"

    aput-object v13, v11, v12

    const/4 v12, 0x6

    const-string v13, "ro.product.name"

    aput-object v13, v11, v12

    const/4 v12, 0x7

    const-string v13, "gsm.network.type"

    aput-object v13, v11, v12

    const/16 v12, 0x8

    const-string v13, "gsm.sim.state"

    aput-object v13, v11, v12

    const/16 v12, 0x9

    const-string v13, "persist.sys.country"

    aput-object v13, v11, v12

    const/16 v12, 0xa

    const-string v13, "persist.sys.language"

    aput-object v13, v11, v12

    const/16 v12, 0xb

    const-string v13, "sys.usb.state"

    aput-object v13, v11, v12

    const/16 v12, 0xc

    const-string v13, "net.dns1"

    aput-object v13, v11, v12

    const/16 v12, 0xd

    const-string v13, "net.hostname"

    aput-object v13, v11, v12

    const/16 v12, 0xe

    const-string v13, "net.eth0.gw"

    aput-object v13, v11, v12

    const/16 v12, 0xf

    const-string v13, "net.gprs.local-ip"

    aput-object v13, v11, v12

    array-length v12, v11

    const/4 v13, 0x0

    :goto_5
    if-ge v13, v12, :cond_6

    aget-object v14, v11, v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 19
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v15
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v6, "get"

    :try_start_7
    new-array v5, v9, [Ljava/lang/Class;

    const-class v18, Ljava/lang/String;

    const/16 v17, 0x0

    aput-object v18, v5, v17

    invoke-virtual {v15, v6, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v14, v6, v17

    invoke-virtual {v5, v0, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_6

    :catch_5
    const-string v5, ""

    .line 20
    :goto_6
    :try_start_8
    invoke-static {v10, v14, v5}, Lcom/bytedance/applog/f1;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v13, v13, 0x1

    const/4 v5, 0x0

    goto :goto_5

    :catch_6
    move-exception v0

    .line 17
    :goto_7
    throw v0

    .line 20
    :cond_6
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "props"

    invoke-virtual {v0, v5, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_7
    if-nez v8, :cond_8

    goto :goto_8

    .line 21
    :cond_8
    :try_start_9
    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v5, "wifi"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v6
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_9

    :catch_7
    move-exception v0

    :try_start_a
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_8
    const/4 v6, 0x0

    :goto_9
    if-eqz v6, :cond_a

    .line 22
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/ScanResult;

    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    const-string v11, "ap"

    :try_start_b
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v6, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, ","

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, v6, Landroid/net/wifi/ScanResult;->level:I

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v11, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v0, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_a

    :cond_9
    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v6, "aps"

    invoke-virtual {v5, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_a
    if-nez v8, :cond_b

    goto :goto_b

    .line 23
    :cond_b
    :try_start_c
    sget-object v0, Lcom/bytedance/applog/g1;->a:Lcom/bytedance/applog/g1$b;

    invoke-virtual {v0, v8}, Lcom/bytedance/applog/g1$b;->a(Landroid/content/Context;)Lorg/json/JSONArray;

    move-result-object v6
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_c

    :catch_8
    move-exception v0

    .line 24
    :try_start_d
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_b
    const/4 v6, 0x0

    :goto_c
    if-eqz v6, :cond_c

    .line 25
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "cell"

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 26
    :cond_c
    :try_start_e
    invoke-static {v8}, Lcom/bytedance/applog/f1;->e(Landroid/content/Context;)Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-nez v0, :cond_d

    goto :goto_d

    :cond_d
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v6
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_9
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_e

    :catch_9
    move-exception v0

    :try_start_f
    const-string v5, ""

    invoke-static {v5, v0}, Lcom/bytedance/applog/x1;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_d
    const/4 v6, 0x0

    .line 27
    :goto_e
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "bssid"

    invoke-static {v0, v5, v6}, Lcom/bytedance/applog/f1;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "mac"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-static {v8}, Lcom/bytedance/applog/f1;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v6, "mac"

    invoke-static {v5, v6, v0}, Lcom/bytedance/applog/f1;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "imsi"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 28
    invoke-static {}, Lcom/bytedance/applog/AppLog;->getInitConfig()Lcom/bytedance/applog/InitConfig;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-static {}, Lcom/bytedance/applog/AppLog;->getInitConfig()Lcom/bytedance/applog/InitConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/applog/InitConfig;->getSensitiveInfoProvider()Lcom/bytedance/applog/ISensitiveInfoProvider;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-static {}, Lcom/bytedance/applog/AppLog;->getInitConfig()Lcom/bytedance/applog/InitConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/applog/InitConfig;->getSensitiveInfoProvider()Lcom/bytedance/applog/ISensitiveInfoProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/applog/ISensitiveInfoProvider;->getImsi()Ljava/lang/String;

    move-result-object v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :goto_f
    move-object v6, v0

    goto :goto_11

    :cond_f
    if-nez v8, :cond_10

    goto :goto_10

    :cond_10
    :try_start_10
    const-string v0, "phone"

    .line 29
    invoke-virtual {v8, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_b
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    if-eqz v0, :cond_11

    :try_start_11
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0
    :try_end_11
    .catch Ljava/lang/SecurityException; {:try_start_11 .. :try_end_11} :catch_a
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_b
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto :goto_f

    :catch_a
    move-exception v0

    move-object v5, v0

    :try_start_12
    invoke-virtual {v5}, Ljava/lang/SecurityException;->printStackTrace()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_b
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto :goto_10

    :catch_b
    move-exception v0

    :try_start_13
    const-string v5, ""

    invoke-static {v5, v0}, Lcom/bytedance/applog/x1;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_11
    :goto_10
    const/4 v6, 0x0

    .line 30
    :goto_11
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "imsi"

    invoke-static {v0, v5, v6}, Lcom/bytedance/applog/f1;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    const-string v5, "t"

    :try_start_14
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v0, v5, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "cpuModel"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    if-nez v0, :cond_13

    :try_start_15
    invoke-static {}, Lcom/bytedance/applog/f1;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v6, "cpuModel"

    invoke-static {v5, v6, v0}, Lcom/bytedance/applog/f1;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_c
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    :catch_c
    :cond_13
    :try_start_16
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "btmac"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    if-nez v0, :cond_18

    if-nez v8, :cond_14

    goto :goto_13

    .line 31
    :cond_14
    :try_start_17
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v6

    const-string v0, "02:00:00:00:00:00"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v5, "bluetooth_address"

    invoke-static {v0, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    goto :goto_14

    :cond_15
    const-string v0, "\u84dd\u7259\u672a\u5f00\u542f"

    goto :goto_12

    :cond_16
    const-string v0, "\u6ca1\u6709\u84dd\u7259\u8bbe\u5907"

    :goto_12
    const/4 v5, 0x0

    .line 32
    :try_start_18
    invoke-static {v0, v5}, Lcom/bytedance/applog/x1;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_d
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    goto :goto_13

    :catch_d
    move-exception v0

    :try_start_19
    const-string v5, ""

    .line 33
    invoke-static {v5, v0}, Lcom/bytedance/applog/x1;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_13
    const/4 v6, 0x0

    .line 34
    :cond_17
    :goto_14
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "btmac"

    invoke-static {v0, v5, v6}, Lcom/bytedance/applog/f1;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "boot"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    const-string v5, "boot"

    :try_start_1a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    invoke-virtual {v0, v5, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 35
    :cond_19
    invoke-static {v8}, Lcom/bytedance/applog/f1;->e(Landroid/content/Context;)Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-nez v0, :cond_1a

    const/4 v6, 0x0

    goto :goto_15

    :cond_1a
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v6

    .line 36
    :goto_15
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1b

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "ssid"

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1b
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "accid"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1d

    invoke-static {v8}, Lcom/bytedance/applog/f1;->d(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1d

    array-length v5, v0

    if-lez v5, :cond_1d

    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    array-length v6, v0

    const/4 v10, 0x0

    :goto_16
    if-ge v10, v6, :cond_1c

    aget-object v11, v0, v10

    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    const-string v13, "accid"

    invoke-virtual {v12, v13, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v11

    invoke-virtual {v5, v11}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v10, v10, 0x1

    goto :goto_16

    :cond_1c
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v6, "accid"

    invoke-virtual {v0, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1d
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "mem"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-static {}, Lcom/bytedance/applog/f1;->c()J

    move-result-wide v5

    const-wide/16 v10, 0x0

    cmp-long v0, v5, v10

    if-ltz v0, :cond_1e

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v10, "mem"

    invoke-virtual {v0, v10, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    :cond_1e
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "cpuFreq"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    invoke-static {}, Lcom/bytedance/applog/f1;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/bytedance/applog/f1;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1f

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_20

    :cond_1f
    sget-object v6, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    const-string v10, "cpuFreq"

    :try_start_1b
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " - "

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v10, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37
    :cond_20
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 38
    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v6, "brand"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_21

    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v6, "brand"

    invoke-virtual {v5, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_21
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "proc"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    if-nez v0, :cond_25

    if-nez v8, :cond_22

    goto :goto_17

    :cond_22
    const-string v6, ""

    .line 39
    :try_start_1c
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v10, "activity"

    invoke-virtual {v5, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager;

    invoke-virtual {v5}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_23
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_24

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v11, v10, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v11, v0, :cond_23

    iget-object v6, v10, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_e
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    goto :goto_18

    :catch_e
    :goto_17
    const/4 v6, 0x0

    .line 40
    :cond_24
    :goto_18
    :try_start_1d
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "proc"

    invoke-static {v0, v5, v6}, Lcom/bytedance/applog/f1;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    :cond_25
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    const-string v5, "sim"

    if-nez v8, :cond_26

    goto :goto_19

    .line 41
    :cond_26
    :try_start_1e
    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v10, "phone"

    invoke-virtual {v6, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    if-nez v6, :cond_27

    :goto_19
    const/4 v6, 0x0

    goto :goto_1a

    :cond_27
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v6

    .line 42
    :goto_1a
    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v5, "cpi_abi2"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_29

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    const-string v5, "cpi_abi2"

    .line 43
    :try_start_1f
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x15

    if-lt v6, v10, :cond_28

    sget-object v6, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    const-string v10, ","

    invoke-static {v10, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1b

    :cond_28
    sget-object v6, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    .line 44
    :goto_1b
    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 45
    :cond_29
    sget-object v0, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    .line 46
    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v6, "diaplay"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2a

    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    const-string v6, "diaplay"

    .line 47
    :try_start_20
    sget-object v10, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    .line 48
    invoke-virtual {v5, v6, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    const-string v6, "manufacturer"

    .line 49
    :try_start_21
    sget-object v10, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 50
    invoke-virtual {v5, v6, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    const-string v6, "hardware"

    invoke-virtual {v5, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_0

    const-string v5, "product"

    .line 51
    :try_start_22
    sget-object v6, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 52
    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2a
    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    invoke-static {v8, v0, v3}, Lcom/bytedance/applog/n1;->a(Landroid/content/Context;Lorg/json/JSONObject;Lcom/bytedance/applog/y;)V

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    if-eqz v0, :cond_2b

    sget-object v0, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v0
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    if-lez v0, :cond_2b

    const-string v0, "anti_cheating"

    :try_start_23
    sget-object v5, Lcom/bytedance/applog/f1;->a:Lorg/json/JSONObject;

    invoke-virtual {v4, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_23
    .catch Lorg/json/JSONException; {:try_start_23 .. :try_end_23} :catch_f
    .catchall {:try_start_23 .. :try_end_23} :catchall_0

    goto :goto_1c

    :catchall_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :catch_f
    :cond_2b
    :goto_1c
    if-eqz v7, :cond_2d

    .line 53
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2c
    :goto_1d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2c

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1d

    :cond_2d
    const-string v0, "header"

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "magic_tag"

    const-string v4, "ss_app_log"

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-string v0, "_gen_time"

    invoke-virtual {v2, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    iget-object v0, v1, Lcom/bytedance/applog/f;->a:Lcom/bytedance/applog/h;

    .line 54
    iget-object v0, v0, Lcom/bytedance/applog/h;->c:Landroid/app/Application;

    .line 55
    invoke-virtual {v3}, Lcom/bytedance/applog/y;->b()Lorg/json/JSONObject;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, v1, Lcom/bytedance/applog/f;->a:Lcom/bytedance/applog/h;

    invoke-virtual {v6}, Lcom/bytedance/applog/h;->c()Lcom/bytedance/applog/UriConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/bytedance/applog/UriConfig;->getRegisterUri()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v4, v5, v9}, Lcom/bytedance/applog/s0;->a(Landroid/content/Context;Lorg/json/JSONObject;Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-static {}, Lcom/bytedance/applog/r0;->a()Ljava/util/HashMap;

    move-result-object v4

    :try_start_24
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/applog/f1;->d(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {}, Lcom/bytedance/applog/AppLog;->getNetClient()Lcom/bytedance/applog/network/INetworkClient;

    move-result-object v5

    invoke-static {v0}, Lcom/bytedance/applog/r0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0, v2, v4}, Lcom/bytedance/applog/network/INetworkClient;->post(Ljava/lang/String;[BLjava/util/Map;)Ljava/lang/String;

    move-result-object v6
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_10

    goto :goto_1e

    :catch_10
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v6, 0x0

    :goto_1e
    if-eqz v6, :cond_2e

    :try_start_25
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_25
    .catch Lorg/json/JSONException; {:try_start_25 .. :try_end_25} :catch_12

    :try_start_26
    invoke-static {v2}, Lcom/bytedance/applog/r0;->a(Lorg/json/JSONObject;)V
    :try_end_26
    .catch Lorg/json/JSONException; {:try_start_26 .. :try_end_26} :catch_11

    move-object v4, v2

    goto :goto_20

    :catch_11
    move-exception v0

    move-object/from16 v16, v2

    goto :goto_1f

    :catch_12
    move-exception v0

    const/16 v16, 0x0

    :goto_1f
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object/from16 v4, v16

    goto :goto_20

    :cond_2e
    const/4 v4, 0x0

    :goto_20
    if-eqz v4, :cond_30

    const-string v0, "device_id"

    const-string v2, ""

    .line 57
    invoke-virtual {v4, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "install_id"

    const-string v2, ""

    invoke-virtual {v4, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "ssid"

    const-string v2, ""

    invoke-virtual {v4, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "bd_did"

    const-string v2, ""

    invoke-virtual {v4, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v0, "cd"

    const-string v2, ""

    invoke-virtual {v4, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v3 .. v9}, Lcom/bytedance/applog/y;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    :cond_2f
    const-string v0, "U SHALL NOT PASS!"

    const/4 v2, 0x0

    .line 58
    invoke-static {v0, v2}, Lcom/bytedance/applog/x1;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_30
    const/4 v2, 0x0

    return v2
.end method

.method public d()Ljava/lang/String;
    .locals 1

    const-string v0, "register"

    return-object v0
.end method

.method public e()[J
    .locals 2

    iget-object v0, p0, Lcom/bytedance/applog/f;->a:Lcom/bytedance/applog/h;

    .line 1
    iget-object v0, v0, Lcom/bytedance/applog/h;->h:Lcom/bytedance/applog/y;

    .line 2
    invoke-virtual {v0}, Lcom/bytedance/applog/y;->c()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    const-string v1, "U SHALL NOT PASS!"

    .line 3
    invoke-static {v1, v0}, Lcom/bytedance/applog/x1;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 4
    :cond_0
    sget-object v0, Lcom/bytedance/applog/k;->f:[J

    goto :goto_1

    :cond_1
    :goto_0
    sget-object v0, Lcom/bytedance/applog/k;->g:[J

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/bytedance/applog/k;->h:[J

    :goto_1
    return-object v0
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public g()J
    .locals 2

    iget-object v0, p0, Lcom/bytedance/applog/f;->a:Lcom/bytedance/applog/h;

    .line 1
    iget-object v0, v0, Lcom/bytedance/applog/h;->n:Lcom/bytedance/applog/m;

    .line 2
    iget-boolean v0, v0, Lcom/bytedance/applog/m;->h:Z

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x1499700

    goto :goto_0

    :cond_0
    const-wide/32 v0, 0x2932e00

    :goto_0
    return-wide v0
.end method
