.class public Lcom/bytedance/applog/n0;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[I

.field public static final d:[J


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/16 v0, 0x25

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "aid"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "region"

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const-string v1, "os"

    const/4 v4, 0x2

    aput-object v1, v0, v4

    const-string v1, "package"

    const/4 v5, 0x3

    aput-object v1, v0, v5

    const-string v1, "app_version"

    const/4 v5, 0x4

    aput-object v1, v0, v5

    const-string v1, "sdk_version"

    const/4 v5, 0x5

    aput-object v1, v0, v5

    const-string v1, "os_version"

    const/4 v5, 0x6

    aput-object v1, v0, v5

    const-string v1, "device_model"

    const/4 v5, 0x7

    aput-object v1, v0, v5

    const-string v1, "resolution"

    const/16 v5, 0x8

    aput-object v1, v0, v5

    const-string v1, "language"

    const/16 v5, 0x9

    aput-object v1, v0, v5

    const-string v1, "timezone"

    const/16 v5, 0xa

    aput-object v1, v0, v5

    const-string v1, "access"

    const/16 v5, 0xb

    aput-object v1, v0, v5

    const-string v1, "display_name"

    const/16 v5, 0xc

    aput-object v1, v0, v5

    const-string v1, "channel"

    const/16 v5, 0xd

    aput-object v1, v0, v5

    const-string v1, "carrier"

    const/16 v5, 0xe

    aput-object v1, v0, v5

    const-string v1, "app_language"

    const/16 v5, 0xf

    aput-object v1, v0, v5

    const-string v1, "app_region"

    const/16 v5, 0x10

    aput-object v1, v0, v5

    const-string v1, "tz_name"

    const/16 v5, 0x11

    aput-object v1, v0, v5

    const-string v1, "tz_offset"

    const/16 v5, 0x12

    aput-object v1, v0, v5

    const-string v1, "install_id"

    const/16 v5, 0x13

    aput-object v1, v0, v5

    const-string v1, "openudid"

    const/16 v5, 0x14

    aput-object v1, v0, v5

    const-string v1, "mcc_mnc"

    const/16 v5, 0x15

    aput-object v1, v0, v5

    const-string v1, "rom"

    const/16 v5, 0x16

    aput-object v1, v0, v5

    const-string v1, "manifest_version_code"

    const/16 v5, 0x17

    aput-object v1, v0, v5

    const-string v1, "device_manufacturer"

    const/16 v5, 0x18

    aput-object v1, v0, v5

    const-string v1, "clientudid"

    const/16 v5, 0x19

    aput-object v1, v0, v5

    const-string v1, "sig_hash"

    const/16 v5, 0x1a

    aput-object v1, v0, v5

    const-string v1, "display_density"

    const/16 v5, 0x1b

    aput-object v1, v0, v5

    const-string v1, "os_api"

    const/16 v5, 0x1c

    aput-object v1, v0, v5

    const-string v1, "update_version_code"

    const/16 v5, 0x1d

    aput-object v1, v0, v5

    const-string v1, "density_dpi"

    const/16 v5, 0x1e

    aput-object v1, v0, v5

    const-string v1, "version_code"

    const/16 v5, 0x1f

    aput-object v1, v0, v5

    const-string v1, "sim_serial_number"

    const/16 v5, 0x20

    aput-object v1, v0, v5

    const-string v1, "release_build"

    const/16 v5, 0x21

    aput-object v1, v0, v5

    const-string v1, "udid"

    const/16 v5, 0x22

    aput-object v1, v0, v5

    const-string v1, "cpu_abi"

    const/16 v5, 0x23

    aput-object v1, v0, v5

    const-string v1, "google_aid"

    const/16 v5, 0x24

    aput-object v1, v0, v5

    sput-object v0, Lcom/bytedance/applog/n0;->a:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "setOnce"

    aput-object v1, v0, v2

    const-string v1, "synchronize"

    aput-object v1, v0, v3

    sput-object v0, Lcom/bytedance/applog/n0;->b:[Ljava/lang/String;

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/bytedance/applog/n0;->c:[I

    new-array v0, v4, [J

    fill-array-data v0, :array_1

    sput-object v0, Lcom/bytedance/applog/n0;->d:[J

    return-void

    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data

    :array_1
    .array-data 8
        -0x1
        -0x1
    .end array-data
.end method

.method public static a(Lcom/bytedance/applog/h;ILorg/json/JSONObject;Lcom/bytedance/applog/profile/UserProfileCallback;Landroid/os/Handler;Z)V
    .locals 10

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez p5, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sget-object p5, Lcom/bytedance/applog/n0;->d:[J

    aget-wide v5, p5, p1

    sub-long/2addr v3, v5

    const-wide/32 v5, 0xea60

    cmp-long p5, v3, v5

    if-lez p5, :cond_0

    const/4 p5, 0x1

    goto :goto_0

    :cond_0
    const/4 p5, 0x0

    :goto_0
    if-eqz p2, :cond_1

    sget-object v3, Lcom/bytedance/applog/n0;->c:[I

    aget v3, v3, p1

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exec "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/bytedance/applog/x1;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    if-nez p5, :cond_4

    if-eqz p3, :cond_2

    const/4 p0, 0x4

    invoke-interface {p3, p0}, Lcom/bytedance/applog/profile/UserProfileCallback;->onFail(I)V

    :cond_2
    return-void

    :cond_3
    const/4 v3, 0x1

    :cond_4
    if-eqz v3, :cond_8

    invoke-static {}, Lcom/bytedance/applog/AppLog;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {}, Lcom/bytedance/applog/AppLog;->getDid()Ljava/lang/String;

    move-result-object p5

    invoke-static {}, Lcom/bytedance/applog/AppLog;->getAid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/bytedance/applog/h;->c()Lcom/bytedance/applog/UriConfig;

    move-result-object p0

    invoke-virtual {p0}, Lcom/bytedance/applog/UriConfig;->getProfileUri()Ljava/lang/String;

    move-result-object p0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p5

    if-nez p5, :cond_7

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p5

    if-nez p5, :cond_7

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p5

    if-nez p5, :cond_7

    const/4 p5, 0x2

    new-array p5, p5, [Ljava/lang/Object;

    aput-object v6, p5, v1

    sget-object v1, Lcom/bytedance/applog/n0;->b:[Ljava/lang/String;

    aget-object v1, v1, p1

    aput-object v1, p5, v2

    const-string v1, "/service/api/v3/userprofile/%s/%s"

    invoke-static {v1, p5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    const-string p5, "header"

    .line 2
    :try_start_0
    invoke-static {}, Lcom/bytedance/applog/AppLog;->getHeader()Lorg/json/JSONObject;

    move-result-object v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    sget-object v3, Lcom/bytedance/applog/n0;->a:[Ljava/lang/String;

    invoke-direct {v2, v1, v3}, Lorg/json/JSONObject;-><init>(Lorg/json/JSONObject;[Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    const-string v3, "sdk_version"

    :try_start_2
    const-string v4, "sdk_version"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    const-string v3, "tz_offset"

    :try_start_3
    const-string v4, "tz_offset"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    :try_start_4
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 3
    :goto_2
    invoke-virtual {p0, p5, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p5, "profile"

    invoke-virtual {p0, p5, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    const-string p5, "user"

    .line 4
    :try_start_5
    invoke-static {}, Lcom/bytedance/applog/AppLog;->getHeader()Lorg/json/JSONObject;

    move-result-object v1

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "device_id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "device_id"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "user_id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "user_id"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "ssid"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v3, "ssid"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 5
    invoke-virtual {p0, p5, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_3

    :catch_1
    move-exception p5

    invoke-virtual {p5}, Lorg/json/JSONException;->printStackTrace()V

    :goto_3
    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    .line 6
    new-instance v8, Lcom/bytedance/applog/n0$a;

    invoke-direct {v8, p1, p2, p3}, Lcom/bytedance/applog/n0$a;-><init>(ILorg/json/JSONObject;Lcom/bytedance/applog/profile/UserProfileCallback;)V

    new-instance p0, Lcom/bytedance/applog/q0;

    move-object v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/bytedance/applog/q0;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/bytedance/applog/profile/UserProfileCallback;Landroid/content/Context;)V

    if-eqz p4, :cond_5

    invoke-virtual {p4, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_4

    :cond_5
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    if-ne p1, p2, :cond_6

    const-string p1, "U SHALL NOT PASS!"

    .line 7
    invoke-static {p1, v0}, Lcom/bytedance/applog/x1;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 8
    :cond_6
    invoke-virtual {p0}, Lcom/bytedance/applog/q0;->run()V

    :goto_4
    return-void

    :cond_7
    if-eqz p3, :cond_9

    const/4 p0, 0x3

    invoke-interface {p3, p0}, Lcom/bytedance/applog/profile/UserProfileCallback;->onFail(I)V

    goto :goto_5

    :cond_8
    if-eqz p3, :cond_9

    invoke-interface {p3}, Lcom/bytedance/applog/profile/UserProfileCallback;->onSuccess()V

    :cond_9
    :goto_5
    return-void
.end method
