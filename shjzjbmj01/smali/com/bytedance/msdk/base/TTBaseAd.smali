.class public abstract Lcom/bytedance/msdk/base/TTBaseAd;
.super Ljava/lang/Object;
.source "TTBaseAd.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/bytedance/msdk/base/TTBaseAd;",
        ">;"
    }
.end annotation


# instance fields
.field private eventMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mActionText:Ljava/lang/String;

.field private mAdNetWorkName:Ljava/lang/String;

.field private mAdNetworkSlotId:Ljava/lang/String;

.field private mAdNetworkSlotType:I

.field private mAdType:I

.field private mAppSize:I

.field private mCacheSuccess:Z

.field private mCpm:D

.field private mDescription:Ljava/lang/String;

.field private mExchangeRate:Ljava/lang/String;

.field private mIconUrl:Ljava/lang/String;

.field private mImageHeight:I

.field private mImageMode:I

.field private mImageUrl:Ljava/lang/String;

.field private mImageWidth:I

.field private mImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mInteractionType:I

.field private mIsAppDownload:Z

.field private mIsExpressAd:Z

.field private mLoadSort:I

.field private mPackageName:Ljava/lang/String;

.field private mRating:D

.field private mRit:Ljava/lang/String;

.field private mSdkNum:I

.field private mSdkVersion:Ljava/lang/String;

.field private mShowSort:I

.field private mSource:Ljava/lang/String;

.field private mStore:Ljava/lang/String;

.field protected mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

.field protected mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

.field protected mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImages:Ljava/util/List;

    const-string v0, "1"

    .line 92
    iput-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mExchangeRate:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->eventMap:Ljava/util/Map;

    return-void
.end method

.method private removeSelfAddView(Landroid/view/ViewGroup;)V
    .locals 6
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    :try_start_0
    check-cast p1, Lcom/bytedance/msdk/api/format/TTNativeAdView;

    .line 3
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    if-eq v0, v1, :cond_2

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 9
    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "tt_gdt_developer_view_root"

    .line 10
    :try_start_1
    sget v3, Lcom/bytedance/ad/sdk/ad_mediation_sdk/R$id;->tt_mediation_gdt_developer_view_root_tag_key:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 11
    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    const/4 v1, 0x0

    .line 12
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 13
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 15
    sget v4, Lcom/bytedance/ad/sdk/ad_mediation_sdk/R$id;->tt_mediation_gdt_developer_view_tag_key:I

    invoke-virtual {v3, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    .line 16
    instance-of v5, v4, Ljava/lang/String;

    if-eqz v5, :cond_1

    const-string v5, "tt_gdt_developer_view"

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 17
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 18
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 19
    invoke-virtual {p1, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 31
    :cond_2
    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 32
    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v1, "tt_admob_native_view_root_tag"

    .line 33
    :try_start_2
    sget v3, Lcom/bytedance/ad/sdk/ad_mediation_sdk/R$id;->tt_mediation_admob_developer_view_root_tag_key:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 34
    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 35
    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_4

    .line 36
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 38
    sget v3, Lcom/bytedance/ad/sdk/ad_mediation_sdk/R$id;->tt_mediation_admob_developer_view_tag_key:I

    invoke-virtual {v1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    .line 39
    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_3

    const-string v4, "tt_admob_native_view_tag"

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 40
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 41
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 42
    invoke-virtual {p1, v1, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 53
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_4
    return-void
.end method


# virtual methods
.method public adnHasAdExpiredApi()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public adnHasAdVideoCachedApi()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public clearLogoView(Landroid/view/View;)V
    .locals 1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 3
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    goto :goto_0

    .line 5
    :cond_0
    instance-of v0, p1, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 6
    check-cast p1, Landroid/widget/ImageView;

    sget v0, Lcom/bytedance/ad/sdk/ad_mediation_sdk/R$color;->tt_mediation_transparent:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public compareTo(Lcom/bytedance/msdk/base/TTBaseAd;)I
    .locals 4

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 2
    :cond_0
    iget v1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mLoadSort:I

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getLoadSort()I

    move-result v2

    if-le v1, v2, :cond_1

    return v0

    .line 6
    :cond_1
    iget v1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mLoadSort:I

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getLoadSort()I

    move-result v2

    const/4 v3, -0x1

    if-ge v1, v2, :cond_2

    return v3

    .line 10
    :cond_2
    iget v1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mShowSort:I

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result v2

    if-le v1, v2, :cond_3

    return v0

    .line 14
    :cond_3
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mShowSort:I

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result p1

    if-ge v0, p1, :cond_4

    return v3

    :cond_4
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/base/TTBaseAd;->compareTo(Lcom/bytedance/msdk/base/TTBaseAd;)I

    move-result p1

    return p1
.end method

.method public getActionText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mActionText:Ljava/lang/String;

    return-object v0
.end method

.method public getAdDescription()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getAdId()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getAdLogoView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetWorkName:Ljava/lang/String;

    return-object v0
.end method

.method public getAdNetworkPlatformId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mSdkNum:I

    return v0
.end method

.method public getAdNetworkSlotId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetworkSlotId:Ljava/lang/String;

    return-object v0
.end method

.method public getAdNetworkSlotType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetworkSlotType:I

    return v0
.end method

.method public getAdTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getAdType()I
.end method

.method public getAdView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getAppSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAppSize:I

    return v0
.end method

.method public getCpm()D
    .locals 4

    .line 1
    :try_start_0
    iget-wide v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mCpm:D

    iget-object v2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mExchangeRate:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    mul-double v0, v0, v2

    return-wide v0

    :catch_0
    move-exception v0

    .line 3
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 5
    iget-wide v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mCpm:D

    return-wide v0
.end method

.method public getCreativeId()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getDislikeDialog(Landroid/app/Activity;)Lcom/bytedance/msdk/api/TTAdDislike;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getEventLoadSort()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetworkSlotType:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 2
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mLoadSort:I

    add-int/lit16 v0, v0, 0x12c

    return v0

    .line 4
    :cond_0
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mLoadSort:I

    return v0
.end method

.method public getEventMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->eventMap:Ljava/util/Map;

    return-object v0
.end method

.method public getExchangeRate()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mExchangeRate:Ljava/lang/String;

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mIconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getImageHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImageHeight:I

    return v0
.end method

.method public getImageMode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImageMode:I

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getImageWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImageWidth:I

    return v0
.end method

.method public getImages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImages:Ljava/util/List;

    return-object v0
.end method

.method public getInteractionType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mInteractionType:I

    return v0
.end method

.method public getIsAppDownload()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mIsAppDownload:Z

    return v0
.end method

.method public getLoadSort()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mLoadSort:I

    return v0
.end method

.method public getMediaExtraInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getNetWorkPlatFormCpm()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->isBidingAd()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->isPAd()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const-string v0, "-1"

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getReqId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getRit()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mRit:Ljava/lang/String;

    return-object v0
.end method

.method public getSdkNum()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mSdkNum:I

    return v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mSdkVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getShowSort()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mShowSort:I

    return v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mSource:Ljava/lang/String;

    return-object v0
.end method

.method public getStarRating()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mRating:D

    return-wide v0
.end method

.method public getStore()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mStore:Ljava/lang/String;

    return-object v0
.end method

.method public abstract hasDestroyed()Z
.end method

.method public hasDislike()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isAdExpired()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isBidingAd()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->isServerBiddingAd()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->isClientBiddingAd()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isCacheSuccess()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mCacheSuccess:Z

    return v0
.end method

.method public isClientBiddingAd()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetworkSlotType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isExpressAd()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mIsExpressAd:Z

    return v0
.end method

.method public isPAd()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetworkSlotType:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isReady()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isServerBiddingAd()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetworkSlotType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    iput-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    .line 2
    iput-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public putEventParam(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 2
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->eventMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public putEventParams(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->eventMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/bytedance/msdk/base/TTBaseAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V

    return-void
.end method

.method public registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;",
            ")V"
        }
    .end annotation

    .line 2
    instance-of p2, p1, Lcom/bytedance/msdk/api/format/TTNativeAdView;

    if-eqz p2, :cond_1

    .line 3
    invoke-direct {p0, p1}, Lcom/bytedance/msdk/base/TTBaseAd;->removeSelfAddView(Landroid/view/ViewGroup;)V

    .line 4
    sget p2, Lcom/bytedance/ad/sdk/ad_mediation_sdk/R$id;->tt_mediation_mtg_ad_choice:I

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    const/16 p3, 0x8

    .line 6
    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz p4, :cond_1

    .line 9
    iget p2, p4, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->logoLayoutId:I

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 10
    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/base/TTBaseAd;->clearLogoView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public removeSelfFromParent(Landroid/view/View;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 1
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 3
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 7
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public render()V
    .locals 0

    return-void
.end method

.method public setActionText(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mActionText:Ljava/lang/String;

    return-void
.end method

.method public setAdDescription(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mDescription:Ljava/lang/String;

    return-void
.end method

.method public setAdNetWorkName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetWorkName:Ljava/lang/String;

    return-void
.end method

.method public setAdNetworkSlotId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetworkSlotId:Ljava/lang/String;

    return-void
.end method

.method public setAdNetworkSlotType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdNetworkSlotType:I

    return-void
.end method

.method public setAdType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAdType:I

    return-void
.end method

.method public setAppSize(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mAppSize:I

    return-void
.end method

.method public setCacheSuccess(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mCacheSuccess:Z

    return-void
.end method

.method public setCpm(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mCpm:D

    return-void
.end method

.method public setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/msdk/api/TTDislikeCallback;)V
    .locals 0

    return-void
.end method

.method public setExchangeRate(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mExchangeRate:Ljava/lang/String;

    return-void
.end method

.method public setExpressAd(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mIsExpressAd:Z

    return-void
.end method

.method public setIconUrl(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mIconUrl:Ljava/lang/String;

    return-void
.end method

.method public setImageHeight(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImageHeight:I

    return-void
.end method

.method public setImageMode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImageMode:I

    return-void
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImageUrl:Ljava/lang/String;

    return-void
.end method

.method public setImageWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImageWidth:I

    return-void
.end method

.method public setImages(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mImages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method public setInteractionType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mInteractionType:I

    return-void
.end method

.method public setIsAppDownload(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mIsAppDownload:Z

    return-void
.end method

.method public setLoadSort(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mLoadSort:I

    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mPackageName:Ljava/lang/String;

    return-void
.end method

.method public setRating(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mRating:D

    return-void
.end method

.method public setRit(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mRit:Ljava/lang/String;

    return-void
.end method

.method public setSdkNum(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mSdkNum:I

    return-void
.end method

.method public setSdkVersion(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mSdkVersion:Ljava/lang/String;

    return-void
.end method

.method public setShowSort(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mShowSort:I

    return-void
.end method

.method public setSource(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mSource:Ljava/lang/String;

    return-void
.end method

.method public setStore(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mStore:Ljava/lang/String;

    return-void
.end method

.method public setTTAdatperCallback(Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method public setTTNativeAdListener(Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-void
.end method

.method public setTTVideoListener(Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTitle:Ljava/lang/String;

    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public showSplashAd(Landroid/view/ViewGroup;)V
    .locals 0

    return-void
.end method

.method public unregisterView()V
    .locals 0

    return-void
.end method
