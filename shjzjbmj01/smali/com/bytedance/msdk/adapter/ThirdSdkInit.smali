.class public Lcom/bytedance/msdk/adapter/ThirdSdkInit;
.super Ljava/lang/Object;
.source "ThirdSdkInit.java"


# static fields
.field private static a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdConfig;
    .locals 0

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p0

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c()Ljava/lang/String;

    move-result-object p1

    .line 5
    :cond_0
    new-instance p0, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    invoke-direct {p0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;-><init>()V

    .line 6
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->appId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 7
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->w()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->useTextureView(Z)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 8
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->appName(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 9
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->v()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->paid(Z)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 10
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->l()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->titleBarTheme(I)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 11
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->t()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->allowShowNotify(Z)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 12
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->u()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->allowShowPageWhenScreenLock(Z)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 13
    invoke-static {}, Lcom/bytedance/msdk/adapter/util/Logger;->isDebug()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->debug(Z)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 14
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->m()[I

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->directDownloadNetworkType([I)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    const/4 p1, 0x0

    .line 15
    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->supportMultiProcess(Z)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 16
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->i()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->data(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 17
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->k()[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->needClearTaskReset([Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 18
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->h()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->customController(Lcom/bytedance/sdk/openadsdk/TTCustomController;)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 19
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->keywords(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;

    move-result-object p0

    .line 20
    invoke-virtual {p0}, Lcom/bytedance/sdk/openadsdk/TTAdConfig$Builder;->build()Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    move-result-object p0

    return-object p0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .line 1
    const-class v0, Lcom/bytedance/msdk/adapter/ThirdSdkInit;

    monitor-enter v0

    .line 2
    :try_start_0
    sget-boolean v1, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->a:Z

    if-nez v1, :cond_1

    .line 3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const-string p0, "TTMediationSDK"

    const-string p1, "pangle_init_error.........:appId\u4e3a\u7a7a"

    .line 4
    invoke-static {p0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string p0, "appId\u4e3a\u7a7a"

    .line 5
    :try_start_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->g()Z

    move-result p1

    const-string v1, "pangle"

    invoke-static {v2, p0, p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(ILjava/lang/String;ZLjava/lang/String;)V

    .line 6
    monitor-exit v0

    return-void

    .line 8
    :cond_0
    sput-boolean v2, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->a:Z

    .line 9
    new-instance v1, Lcom/bytedance/msdk/adapter/ThirdSdkInit$2;

    invoke-direct {v1, p0}, Lcom/bytedance/msdk/adapter/ThirdSdkInit$2;-><init>(Landroid/content/Context;)V

    invoke-static {v1}, Lcom/bytedance/embedapplog/AppLog;->addDataObserver(Lcom/bytedance/embedapplog/IDataObserver;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v1, "TTMediationSDK_SDK_Init"

    .line 38
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pangle_init_start.........:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-static {p0, p1}, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/TTAdConfig;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->init(Landroid/content/Context;Lcom/bytedance/sdk/openadsdk/TTAdConfig;)Lcom/bytedance/sdk/openadsdk/TTAdManager;

    const-string p0, "TTMediationSDK_SDK_Init"

    const-string p1, "pangle_init_end.........:"

    .line 40
    invoke-static {p0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 2
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 3
    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getDid()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static getTTPangleAppId()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    const-string v1, "pangle"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 5
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public static hasPangleSdkInit()Z
    .locals 2

    .line 1
    const-class v0, Lcom/bytedance/msdk/adapter/ThirdSdkInit;

    monitor-enter v0

    .line 2
    :try_start_0
    sget-boolean v1, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->a:Z

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 3
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static initTTGDTSDK(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->c()Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const-string v1, "gdt"

    .line 8
    invoke-static {v1}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClassNameByAdnName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;

    if-nez v0, :cond_2

    return-void

    .line 13
    :cond_2
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "app_id"

    .line 14
    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    new-instance p1, Lcom/bytedance/msdk/adapter/ThirdSdkInit$1;

    invoke-direct {p1}, Lcom/bytedance/msdk/adapter/ThirdSdkInit$1;-><init>()V

    invoke-interface {v0, p0, v1, p1}, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;->initializeNetwork(Landroid/content/Context;Ljava/util/Map;Lcom/bytedance/msdk/adapter/config/TTOnNetworkInitializationFinishedListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 23
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 24
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "GDT SDK \u521d\u59cb\u5316\u5931\u8d25\u3002\u3002 e="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "TTMediationSDK_SDK_Init"

    invoke-static {p1, p0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static initTTPangleSDK(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->getTTPangleAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->initTTPangleSDK(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static initTTPangleSDK(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .line 2
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    invoke-static {}, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->getTTPangleAppId()Ljava/lang/String;

    move-result-object p1

    .line 5
    :cond_0
    invoke-static {p0, p1}, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->c(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 8
    const-class p1, Lcom/bytedance/msdk/adapter/ThirdSdkInit;

    monitor-enter p1

    const/4 v0, 0x0

    .line 9
    :try_start_1
    sput-boolean v0, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->a:Z

    .line 10
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 11
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->g()Z

    move-result p1

    const/4 v0, 0x2

    const-string v1, "\u521d\u59cb\u5316\u629b\u51fa\u5f02\u5e38"

    const-string v2, "pangle"

    invoke-static {v0, v1, p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(ILjava/lang/String;ZLjava/lang/String;)V

    .line 12
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 13
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "TTSDK \u521d\u59cb\u5316\u5931\u8d25\u3002\u3002 e="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "TTMediationSDK_SDK_Init"

    invoke-static {p1, p0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :catchall_1
    move-exception p0

    .line 14
    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p0
.end method

.method public static initUnitySDK(Landroid/app/Activity;)V
    .locals 7

    const-string v0, "com.bytedance.msdk.adapter.init.UnitySdkInit"

    .line 1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "initUnitySDK"

    const/4 v2, 0x2

    .line 2
    :try_start_1
    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Landroid/app/Activity;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-class v4, Ljava/lang/String;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 3
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v1

    const-string v3, "unity"

    invoke-virtual {v1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    .line 5
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v5

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v2, v6

    invoke-virtual {v0, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string p0, "TTMediationSDK_SDK_Init"

    const-string v0, "unitySdkInit \u521d\u59cb\u5316\u5931\u8d25\u3002\u3002 \u914d\u7f6e\u4fe1\u606f\u4e3anull"

    .line 7
    invoke-static {p0, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 10
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unitySdkInit \u521d\u59cb\u5316\u5931\u8d25\u3002\u3002 e="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "TTMediationSDK_SDK_Init"

    invoke-static {v0, p0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
