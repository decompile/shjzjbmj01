.class Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""

# interfaces
.implements Lcom/baidu/mobads/AdViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BaiduBannerAd"
.end annotation


# instance fields
.field private a:Lcom/baidu/mobads/AdView;

.field private b:Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

.field private c:Z

.field private d:Z

.field private e:Landroid/widget/FrameLayout;

.field final synthetic f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->b:Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    new-instance v0, Lcom/baidu/mobads/AdView;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/baidu/mobads/AdView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->a:Lcom/baidu/mobads/AdView;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->a:Lcom/baidu/mobads/AdView;

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/AdView;->setListener(Lcom/baidu/mobads/AdViewListener;)V

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->e:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->e:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->a:Lcom/baidu/mobads/AdView;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    return-void
.end method

.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public getAdView()Landroid/view/View;
    .locals 3

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->e:Landroid/widget/FrameLayout;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu banner ad getView is null!! ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->a:Lcom/baidu/mobads/AdView;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onAdClick(Lorg/json/JSONObject;)V
    .locals 2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Baidu banner ad click ......."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TTMediationSDK"

    invoke-static {v0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->b:Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;->onAdClicked()V

    :cond_0
    return-void
.end method

.method public onAdClose(Lorg/json/JSONObject;)V
    .locals 2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Baidu banner ad close ......."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TTMediationSDK"

    invoke-static {v0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->b:Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;->onAdClosed()V

    :cond_0
    return-void
.end method

.method public onAdFailed(Ljava/lang/String;)V
    .locals 2

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->d:Z

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    const/16 v1, 0x7538

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void
.end method

.method public onAdReady(Lcom/baidu/mobads/AdView;)V
    .locals 1

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->c:Z

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->d:Z

    if-nez v0, :cond_0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->d:Z

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {p1, p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    :cond_0
    return-void
.end method

.method public onAdShow(Lorg/json/JSONObject;)V
    .locals 2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Baidu banner ad show ......."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TTMediationSDK"

    invoke-static {v0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->b:Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method public onAdSwitch()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->f:Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu banner ad switch ......."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->a:Lcom/baidu/mobads/AdView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/AdView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduBannerAdapter$BaiduBannerAd;->a:Lcom/baidu/mobads/AdView;

    :cond_0
    return-void
.end method
