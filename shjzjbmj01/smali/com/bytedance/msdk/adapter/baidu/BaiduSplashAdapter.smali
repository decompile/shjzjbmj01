.class public Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/bytedance/msdk/api/TTVideoOption;

.field private c:Lcom/bytedance/msdk/api/BaiduRequestParameters;

.field private d:Lcom/bytedance/msdk/api/BaiduSplashParams;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    const/16 v0, 0xbb8

    iput v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->e:I

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/BaiduRequestParameters;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->c:Lcom/bytedance/msdk/api/BaiduRequestParameters;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/BaiduSplashParams;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->d:Lcom/bytedance/msdk/api/BaiduSplashParams;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)I
    .locals 0

    iget p0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->e:I

    return p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "baidu"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getMajorVersionNumber()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->a:Landroid/content/Context;

    if-eqz p2, :cond_3

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->getBaiduExtraOption()Lcom/bytedance/msdk/api/BaiduExtraOptions;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/BaiduExtraOptions;->getBaiduRequestParameters()Lcom/bytedance/msdk/api/BaiduRequestParameters;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->c:Lcom/bytedance/msdk/api/BaiduRequestParameters;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/BaiduExtraOptions;->getBaiduSplashParams()Lcom/bytedance/msdk/api/BaiduSplashParams;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->d:Lcom/bytedance/msdk/api/BaiduSplashParams;

    :cond_0
    const-string p1, "ad_load_timeout"

    invoke-interface {p2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "ad_load_timeout"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_1
    iget p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->e:I

    :goto_0
    iput p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->e:I

    const-string p1, "tt_ad_network_callback"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 p2, 0x0

    instance-of v0, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_2

    move-object p2, p1

    check-cast p2, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    :cond_2
    new-instance p1, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;

    invoke-direct {p1, p0, p2}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;-><init>(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;)V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;)V

    :cond_3
    return-void
.end method
