.class Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""

# interfaces
.implements Lcom/baidu/mobads/InterstitialAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BaiduInterstitialAd"
.end annotation


# instance fields
.field private a:Lcom/baidu/mobads/InterstitialAd;

.field final synthetic b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method private b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 3

    new-instance v0, Lcom/baidu/mobads/InterstitialAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/baidu/mobads/InterstitialAd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->a:Lcom/baidu/mobads/InterstitialAd;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu interstitial ad load 1 ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->a:Lcom/baidu/mobads/InterstitialAd;

    invoke-virtual {v0, p0}, Lcom/baidu/mobads/InterstitialAd;->setListener(Lcom/baidu/mobads/InterstitialAdListener;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu interstitial ad load 2 ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->a:Lcom/baidu/mobads/InterstitialAd;

    invoke-virtual {v0}, Lcom/baidu/mobads/InterstitialAd;->loadAd()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu interstitial ad load 3 ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->a:Lcom/baidu/mobads/InterstitialAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isReady()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->a:Lcom/baidu/mobads/InterstitialAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/InterstitialAd;->isAdReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onAdClick(Lcom/baidu/mobads/InterstitialAd;)V
    .locals 2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Baidu interstitial ad  onAdClick ..."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TTMediationSDK"

    invoke-static {v0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;->onInterstitialAdClick()V

    :cond_0
    return-void
.end method

.method public onAdDismissed()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu interstitial ad  close ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;->onInterstitialClosed()V

    :cond_0
    return-void
.end method

.method public onAdFailed(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    const/16 v1, 0x7538

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onAdPresent()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu interstitial ad onAdPresent --> show ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;->onInterstitialShow()V

    :cond_0
    return-void
.end method

.method public onAdReady()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter;

    invoke-virtual {v0, p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->a:Lcom/baidu/mobads/InterstitialAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/InterstitialAd;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->a:Lcom/baidu/mobads/InterstitialAd;

    :cond_0
    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 1

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduInterstitialAdapter$BaiduInterstitialAd;->a:Lcom/baidu/mobads/InterstitialAd;

    invoke-virtual {v0, p1}, Lcom/baidu/mobads/InterstitialAd;->showAd(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method
