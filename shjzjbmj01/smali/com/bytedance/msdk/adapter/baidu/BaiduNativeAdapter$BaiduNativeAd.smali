.class Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BaiduNativeAd"
.end annotation


# instance fields
.field private a:Lcom/baidu/mobad/feeds/NativeResponse;

.field private b:Lcom/baidu/mobads/component/FeedNativeView;

.field private c:Z

.field d:Landroid/view/View$OnClickListener;

.field final synthetic e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;Lcom/baidu/mobad/feeds/NativeResponse;)V
    .locals 5

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->c:Z

    new-instance v1, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$4;

    invoke-direct {v1, p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$4;-><init>(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)V

    iput-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->d:Landroid/view/View$OnClickListener;

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setTitle(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getDesc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdDescription(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getAppSize()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setAppSize(I)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getIconUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setIconUrl(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getBrandName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setSource(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageUrl(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getMainPicHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageHeight(I)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getMainPicWidth()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageWidth(I)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getMultiPicUrls()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImages(Ljava/util/List;)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getAppPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setPackageName(Ljava/lang/String;)V

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdType(I)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->isDownloadApp()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/bytedance/msdk/base/TTBaseAd;->setIsAppDownload(Z)V

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getECPMLevel()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getECPMLevel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    goto :goto_0

    :cond_0
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    :goto_0
    invoke-virtual {p0, v2, v3}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "Baidu \u539f\u751fNative \u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TTMediationSDK_ECMP"

    invoke-static {v3, v2}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->isDownloadApp()Z

    move-result v2

    const/4 v3, 0x4

    const/4 v4, 0x3

    if-eqz v2, :cond_2

    invoke-virtual {p0, v3}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v4}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    :goto_1
    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v2

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->isNativeSmartOpt(I)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v0

    const/16 v2, 0x1c

    if-eq v0, v2, :cond_7

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v0

    const/16 v2, 0x1d

    if-eq v0, v2, :cond_7

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v0

    const/16 v2, 0x1e

    if-eq v0, v2, :cond_7

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v0

    const/16 v2, 0x21

    if-eq v0, v2, :cond_7

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v0

    const/16 v2, 0x22

    if-ne v0, v2, :cond_3

    goto :goto_3

    :cond_3
    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v0

    const/16 v2, 0x23

    if-eq v0, v2, :cond_6

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v0

    const/16 v2, 0x24

    if-ne v0, v2, :cond_4

    goto :goto_2

    :cond_4
    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v0

    const/16 v2, 0x25

    if-eq v0, v2, :cond_5

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    move-result-object p2

    sget-object v0, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    if-ne p2, v0, :cond_8

    :cond_5
    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_4

    :cond_6
    :goto_2
    invoke-virtual {p0, v3}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_4

    :cond_7
    :goto_3
    invoke-virtual {p0, v4}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    :cond_8
    :goto_4
    new-instance p2, Lcom/baidu/mobads/component/FeedNativeView;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/baidu/mobads/component/FeedNativeView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    invoke-virtual {p2}, Lcom/baidu/mobads/component/FeedNativeView;->getParent()Landroid/view/ViewParent;

    move-result-object p2

    if-eqz p2, :cond_9

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    invoke-virtual {p2}, Lcom/baidu/mobads/component/FeedNativeView;->getParent()Landroid/view/ViewParent;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_9
    iget-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    check-cast v0, Lcom/baidu/mobad/feeds/XAdNativeResponse;

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/component/FeedNativeView;->setAdData(Lcom/baidu/mobad/feeds/XAdNativeResponse;)V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object p2

    if-eqz p2, :cond_a

    new-instance p2, Lcom/baidu/mobads/component/StyleParams$Builder;

    invoke-direct {p2}, Lcom/baidu/mobads/component/StyleParams$Builder;-><init>()V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object v0

    iget v0, v0, Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;->mTitleFontColor:I

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/component/StyleParams$Builder;->setTitleFontColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;

    move-result-object p2

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object v0

    iget v0, v0, Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;->mTitleFontSizeSp:I

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/component/StyleParams$Builder;->setTitleFontSizeSp(I)Lcom/baidu/mobads/component/StyleParams$Builder;

    move-result-object p2

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;->mTitleFontTypeFace:Landroid/graphics/Typeface;

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/component/StyleParams$Builder;->setTitleFontTypeFace(Landroid/graphics/Typeface;)Lcom/baidu/mobads/component/StyleParams$Builder;

    move-result-object p2

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;->mImageBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/component/StyleParams$Builder;->setImageBackground(Landroid/graphics/drawable/Drawable;)Lcom/baidu/mobads/component/StyleParams$Builder;

    move-result-object p2

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object v0

    iget v0, v0, Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;->mBrandLeftDp:I

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/component/StyleParams$Builder;->setBrandLeftDp(I)Lcom/baidu/mobads/component/StyleParams$Builder;

    move-result-object p2

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object v0

    iget v0, v0, Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;->mBrandFontColor:I

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/component/StyleParams$Builder;->setBrandFontColor(I)Lcom/baidu/mobads/component/StyleParams$Builder;

    move-result-object p2

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object v0

    iget-object v0, v0, Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;->mBrandFontTypeFace:Landroid/graphics/Typeface;

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/component/StyleParams$Builder;->setBrandFontTypeFace(Landroid/graphics/Typeface;)Lcom/baidu/mobads/component/StyleParams$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/baidu/mobads/component/StyleParams$Builder;->build()Lcom/baidu/mobads/component/StyleParams;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    invoke-virtual {v0, p2}, Lcom/baidu/mobads/component/FeedNativeView;->changeViewLayoutParams(Ljava/lang/Object;)V

    :cond_a
    iget-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    new-instance v0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$1;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$1;-><init>(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)V

    invoke-virtual {p2, v0}, Lcom/baidu/mobads/component/FeedNativeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_6

    :cond_b
    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getMultiPicUrls()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_c

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getMultiPicUrls()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ne p1, v4, :cond_c

    invoke-virtual {p0, v3}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_6

    :cond_c
    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getVideoUrl()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_e

    invoke-interface {p2}, Lcom/baidu/mobad/feeds/NativeResponse;->getMaterialType()Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    move-result-object p1

    sget-object p2, Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;->VIDEO:Lcom/baidu/mobad/feeds/NativeResponse$MaterialType;

    if-ne p1, p2, :cond_d

    goto :goto_5

    :cond_d
    invoke-virtual {p0, v4}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_6

    :cond_e
    :goto_5
    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    :goto_6
    return-void
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a()V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->d:Landroid/view/View$OnClickListener;

    invoke-static {p1, v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->a(Ljava/util/List;Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->d:Landroid/view/View$OnClickListener;

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->a(Ljava/util/List;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic k(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic m(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic n(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic o(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic p(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/baidu/mobad/feeds/NativeResponse;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    return-object p0
.end method

.method static synthetic q(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic r(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method


# virtual methods
.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->c(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public getAdView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    return-object v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->c:Z

    return v0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onDestroy()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->c:Z

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onPause()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->isDownloadApp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->pauseAppDownload()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onResume()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->isDownloadApp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->resumeAppDownload()V

    :cond_0
    return-void
.end method

.method public registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V

    return-void
.end method

.method public registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
    .locals 10
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;",
            ")V"
        }
    .end annotation

    invoke-super {p0, p1, p2, p3, p4}, Lcom/bytedance/msdk/base/TTBaseAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_2

    instance-of v0, p1, Lcom/bytedance/msdk/api/format/TTNativeAdView;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/bytedance/msdk/api/format/TTNativeAdView;

    iget v0, p4, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->logoLayoutId:I

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v2}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v2

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->isNativeSmartOpt(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    new-instance v3, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v4}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v5}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v6, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v6}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x41700000    # 15.0f

    invoke-static {v6, v7}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->dp2px(Landroid/content/Context;F)I

    move-result v6

    iget-object v8, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v8}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v7}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->dp2px(Landroid/content/Context;F)I

    move-result v7

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v5, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v6}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v7, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v7}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v7

    const/high16 v8, 0x41c80000    # 25.0f

    invoke-static {v7, v8}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->dp2px(Landroid/content/Context;F)I

    move-result v7

    iget-object v8, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v8}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v8

    const/high16 v9, 0x41500000    # 13.0f

    invoke-static {v8, v9}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->dp2px(Landroid/content/Context;F)I

    move-result v8

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v6, Lcom/bytedance/msdk/adapter/util/DownloadImageTask;

    invoke-direct {v6, v4}, Lcom/bytedance/msdk/adapter/util/DownloadImageTask;-><init>(Landroid/widget/ImageView;)V

    const/4 v4, 0x1

    new-array v7, v4, [Ljava/lang/String;

    iget-object v8, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v8}, Lcom/baidu/mobad/feeds/NativeResponse;->getBaiduLogoUrl()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v6, v7}, Lcom/bytedance/msdk/adapter/util/DownloadImageTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    new-instance v6, Lcom/bytedance/msdk/adapter/util/DownloadImageTask;

    invoke-direct {v6, v5}, Lcom/bytedance/msdk/adapter/util/DownloadImageTask;-><init>(Landroid/widget/ImageView;)V

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {v5}, Lcom/baidu/mobad/feeds/NativeResponse;->getAdLogoUrl()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v6, v4}, Lcom/bytedance/msdk/adapter/util/DownloadImageTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v4}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x42180000    # 38.0f

    invoke-static {v4, v5}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->dp2px(Landroid/content/Context;F)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v4}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v9}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->dp2px(Landroid/content/Context;F)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v3, v1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    :cond_0
    iget p4, p4, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->mediaViewId:I

    invoke-virtual {p1, p4}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->findViewById(I)Landroid/view/View;

    move-result-object p4

    check-cast p4, Lcom/bytedance/msdk/api/format/TTMediaView;

    if-eqz p4, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getImageMode()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_1

    new-instance v0, Lcom/baidu/mobads/component/XNativeView;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/baidu/mobads/component/XNativeView;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/component/XNativeView;->setNativeItem(Lcom/baidu/mobad/feeds/NativeResponse;)V

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->e(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/component/XNativeView;->setVideoMute(Z)V

    new-instance v2, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;

    invoke-direct {v2, p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;-><init>(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)V

    invoke-virtual {v0, v2}, Lcom/baidu/mobads/component/XNativeView;->setNativeVideoListener(Lcom/baidu/mobads/component/INativeVideoListener;)V

    invoke-virtual {p4}, Lcom/bytedance/msdk/api/format/TTMediaView;->removeAllViews()V

    invoke-virtual {p4, v0, v1, v1}, Lcom/bytedance/msdk/api/format/TTMediaView;->addView(Landroid/view/View;II)V

    new-instance p4, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$3;

    invoke-direct {p4, p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$3;-><init>(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)V

    invoke-virtual {v0, p4}, Lcom/baidu/mobads/component/XNativeView;->setNativeViewClickListener(Lcom/baidu/mobads/component/XNativeView$INativeViewClickListener;)V

    invoke-virtual {v0}, Lcom/baidu/mobads/component/XNativeView;->render()V

    :cond_1
    iget-object p4, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    invoke-interface {p4, p1}, Lcom/baidu/mobad/feeds/NativeResponse;->recordImpression(Landroid/view/View;)V

    invoke-direct {p0, p2, p3}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a(Ljava/util/List;Ljava/util/List;)V

    :cond_2
    return-void
.end method

.method public render()V
    .locals 4

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->render()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/baidu/mobad/feeds/NativeResponse;->getStyleType()I

    move-result v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduAdapterUtil;->isNativeSmartOpt(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->a:Lcom/baidu/mobad/feeds/NativeResponse;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    invoke-interface {v0, v1}, Lcom/baidu/mobad/feeds/NativeResponse;->recordImpression(Landroid/view/View;)V

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;->onAdShow()V

    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    instance-of v1, v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, -0x40000000    # -2.0f

    invoke-interface {v0, v1, v2, v3}, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;->onRenderSuccess(Landroid/view/View;FF)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    instance-of v1, v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b:Lcom/baidu/mobads/component/FeedNativeView;

    const/16 v2, 0x68

    invoke-static {v2}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3, v2}, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;->onRenderFail(Landroid/view/View;Ljava/lang/String;I)V

    :cond_2
    :goto_0
    return-void
.end method
