.class Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""

# interfaces
.implements Lcom/baidu/mobads/SplashLpCloseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BaiduSplashAd"
.end annotation


# instance fields
.field private a:Lcom/baidu/mobads/SplashAd;

.field b:Landroid/widget/FrameLayout;

.field final synthetic c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method private a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;)V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->b()V

    return-void
.end method

.method private b()V
    .locals 11

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->b:Landroid/widget/FrameLayout;

    new-instance v0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    invoke-direct {v0}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;-><init>()V

    const/16 v1, 0x780

    invoke-virtual {v0, v1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->setHeight(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    move-result-object v0

    const/16 v1, 0x438

    invoke-virtual {v0, v1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->setWidth(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->build()Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/BaiduRequestParameters;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    invoke-direct {v0}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/BaiduRequestParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/BaiduRequestParameters;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->setHeight(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/BaiduRequestParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/BaiduRequestParameters;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->setWidth(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/BaiduRequestParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/BaiduRequestParameters;->getAPPConfirmPolicy()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->downloadAppConfirmPolicy(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/BaiduRequestParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/BaiduRequestParameters;->toHashMap()Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :catchall_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    :try_start_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/baidu/mobad/feeds/RequestParameters$Builder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->build()Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object v0

    :cond_1
    move-object v7, v0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->c(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/BaiduSplashParams;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->c(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/BaiduSplashParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/BaiduSplashParams;->getMaxVideoCacheCapacityMb()I

    move-result v0

    invoke-static {v0}, Lcom/baidu/mobads/SplashAd;->setMaxVideoCacheCapacityMb(I)V

    :cond_2
    new-instance v0, Lcom/baidu/mobads/SplashAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->b:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)I

    move-result v8

    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v1, v0

    move-object v4, p0

    invoke-direct/range {v1 .. v10}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;ZLcom/baidu/mobad/feeds/RequestParameters;IZZ)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a:Lcom/baidu/mobads/SplashAd;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a:Lcom/baidu/mobads/SplashAd;

    invoke-virtual {v0}, Lcom/baidu/mobads/SplashAd;->load()V

    return-void
.end method


# virtual methods
.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;->e(Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a:Lcom/baidu/mobads/SplashAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onADLoaded()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    invoke-virtual {v0, p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void
.end method

.method public onAdClick()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdClicked()V

    :cond_0
    return-void
.end method

.method public onAdDismissed()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdDismiss()V

    :cond_0
    return-void
.end method

.method public onAdFailed(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->c:Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter;

    const/16 v1, 0x7538

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onAdPresent()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a:Lcom/baidu/mobads/SplashAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/SplashAd;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a:Lcom/baidu/mobads/SplashAd;

    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->b:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    :cond_1
    return-void
.end method

.method public onLpClosed()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdDismiss()V

    :cond_0
    return-void
.end method

.method public showSplashAd(Landroid/view/ViewGroup;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a:Lcom/baidu/mobads/SplashAd;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->b:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduSplashAdapter$BaiduSplashAd;->a:Lcom/baidu/mobads/SplashAd;

    invoke-virtual {p1}, Lcom/baidu/mobads/SplashAd;->show()V

    :cond_0
    return-void
.end method
