.class Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/baidu/mobads/component/INativeVideoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->f(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->g(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoCompleted()V

    :cond_0
    return-void
.end method

.method public onError()V
    .locals 4

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->h(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->i(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/api/AdError;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoError(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->c(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoPause()V

    :cond_0
    return-void
.end method

.method public onRenderingStart()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->q(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->r(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoStart()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd$2;->a:Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;->e(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoResume()V

    :cond_0
    return-void
.end method
