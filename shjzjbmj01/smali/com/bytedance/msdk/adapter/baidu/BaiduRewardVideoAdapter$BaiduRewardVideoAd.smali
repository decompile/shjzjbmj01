.class Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""

# interfaces
.implements Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BaiduRewardVideoAd"
.end annotation


# instance fields
.field private a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

.field final synthetic b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method private b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 4

    new-instance v0, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-static {v3}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;)Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-static {v3}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;->b(Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;)Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/msdk/api/TTVideoOption;->useSurfaceView()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-direct {v0, v1, v2, p0, v3}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobads/rewardvideo/RewardVideoAd$RewardVideoAdListener;Z)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-virtual {v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->load()V

    return-void
.end method

.method public adnHasAdExpiredApi()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;->c(Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAdExpired()Z
    .locals 1

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->isReady()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isReady()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onAdClick()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu rewardVideo ad onAdClick ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onRewardClick()V

    :cond_0
    return-void
.end method

.method public onAdClose(F)V
    .locals 2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Baidu rewardVideo ad onAdClose ..."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TTMediationSDK"

    invoke-static {v0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onRewardedAdClosed()V

    :cond_0
    return-void
.end method

.method public onAdFailed(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    const/16 v1, 0x7538

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onAdShow()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu rewardVideo ad onAdShow ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onRewardedAdShow()V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;->a(Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;Landroid/content/Context;)Landroid/content/Context;

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->pause()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->resume()V

    :cond_0
    return-void
.end method

.method public onVideoDownloadFailed()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu rewardVideo ad video  cache failed ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onVideoDownloadSuccess()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu rewardVideo ad video load and cache success ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v0, p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    new-instance v1, Lcom/bytedance/msdk/api/AdError;

    const-string v2, "sdk no cache callback "

    const/16 v3, 0x753a

    invoke-direct {v1, v3, v2}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdVideoCache(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public playCompletion()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b:Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Baidu rewardVideo video complete and reward verify ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onVideoComplete()V

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd$1;

    invoke-direct {v1, p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd$1;-><init>(Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;)V

    invoke-interface {v0, v1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onRewardVerify(Lcom/bytedance/msdk/api/reward/RewardItem;)V

    :cond_0
    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->isReady()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduRewardVideoAdapter$BaiduRewardVideoAd;->a:Lcom/baidu/mobads/rewardvideo/RewardVideoAd;

    invoke-virtual {p1}, Lcom/baidu/mobads/rewardvideo/RewardVideoAd;->show()V

    :cond_0
    return-void
.end method
