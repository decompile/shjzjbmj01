.class public Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/bytedance/msdk/api/TTVideoOption;

.field private c:Z

.field private d:Lcom/bytedance/msdk/api/BaiduExtraOptions;

.field private e:Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/baidu/mobad/feeds/NativeResponse;",
            "Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$BaiduNativeAd;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeLoadAdListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->f:Ljava/util/Map;

    new-instance v0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$1;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter$1;-><init>(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->g:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeLoadAdListener;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->f:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->e:Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->c:Z

    return p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "baidu"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/baidu/mobads/constants/XAdSDKProxyVersion;->getMajorVersionNumber()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->a:Landroid/content/Context;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->isMuted()Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->c:Z

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->getBaiduExtraOption()Lcom/bytedance/msdk/api/BaiduExtraOptions;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d:Lcom/bytedance/msdk/api/BaiduExtraOptions;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d:Lcom/bytedance/msdk/api/BaiduExtraOptions;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/BaiduExtraOptions;->getBaiduNativeSmartOptStyleParams()Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->e:Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    :cond_0
    new-instance p1, Lcom/baidu/mobad/feeds/BaiduNative;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->g:Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeLoadAdListener;

    invoke-direct {p1, p2, v0, v1}, Lcom/baidu/mobad/feeds/BaiduNative;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/baidu/mobad/feeds/BaiduNative$BaiduNativeNetworkListener;)V

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_1

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result p2

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    new-instance v2, Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    invoke-direct {v2}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;-><init>()V

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d:Lcom/bytedance/msdk/api/BaiduExtraOptions;

    const/4 v4, 0x1

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/bytedance/msdk/api/BaiduExtraOptions;->getDownloadAppConfirmPolicy()I

    move-result v3

    goto :goto_2

    :cond_3
    const/4 v3, 0x1

    :goto_2
    invoke-virtual {v2, v3}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->downloadAppConfirmPolicy(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    move-result-object v2

    if-lez p2, :cond_4

    if-lez v1, :cond_4

    invoke-virtual {v2, p2}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->setWidth(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    move-result-object p2

    invoke-virtual {p2, v1}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->setHeight(I)Lcom/baidu/mobad/feeds/RequestParameters$Builder;

    :cond_4
    iget-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d:Lcom/bytedance/msdk/api/BaiduExtraOptions;

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/BaiduExtraOptions;->isCacheVideoOnlyWifi()Z

    move-result p2

    if-eqz p2, :cond_6

    :cond_5
    const/4 v0, 0x1

    :cond_6
    invoke-virtual {p1, v0}, Lcom/baidu/mobad/feeds/BaiduNative;->setCacheVideoOnlyWifi(Z)V

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/baidu/BaiduNativeAdapter;->d:Lcom/bytedance/msdk/api/BaiduExtraOptions;

    if-eqz p2, :cond_7

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/BaiduExtraOptions;->isCacheVideoOnlyWifi()Z

    move-result p2

    if-eqz p2, :cond_8

    :cond_7
    invoke-virtual {p1}, Lcom/baidu/mobad/feeds/BaiduNative;->preloadVideoMaterial()V

    :cond_8
    invoke-virtual {v2}, Lcom/baidu/mobad/feeds/RequestParameters$Builder;->build()Lcom/baidu/mobad/feeds/RequestParameters;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/baidu/mobad/feeds/BaiduNative;->makeRequest(Lcom/baidu/mobad/feeds/RequestParameters;)V

    return-void
.end method
