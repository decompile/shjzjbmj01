.class Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TTKsNativeAd"
.end annotation


# instance fields
.field a:Lcom/kwad/sdk/api/KsNativeAd;

.field b:Lcom/kwad/sdk/api/KsNativeAd$AdInteractionListener;

.field c:Lcom/kwad/sdk/api/KsNativeAd$VideoPlayListener;

.field final synthetic d:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;


# direct methods
.method public constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;Lcom/kwad/sdk/api/KsNativeAd;)V
    .locals 12

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->d:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance v0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$1;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$1;-><init>(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd$AdInteractionListener;

    new-instance v0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;-><init>(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->c:Lcom/kwad/sdk/api/KsNativeAd$VideoPlayListener;

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getActionDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setActionText(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getAdDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdDescription(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getAppIconUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setIconUrl(Ljava/lang/String;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdType(I)V

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setTitle(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getAdSource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setSource(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getInteractionType()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setIsAppDownload(Z)V

    invoke-virtual {p0, v2}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getAppScore()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setStore(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getInteractionType()I

    move-result v1

    const/4 v4, -0x1

    const/4 v5, 0x2

    const/4 v6, 0x4

    const/4 v7, 0x3

    if-ne v1, v3, :cond_1

    invoke-virtual {p0, v6}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    goto :goto_1

    :cond_1
    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getInteractionType()I

    move-result v1

    if-ne v1, v5, :cond_2

    invoke-virtual {p0, v7}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v4}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    :goto_1
    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getECPM()I

    move-result v1

    int-to-double v8, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object p1

    invoke-static {v10, p1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "ks native \u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "TTMediationSDK_ECMP"

    invoke-static {v1, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v10, 0x0

    cmpl-double p1, v8, v10

    if-lez p1, :cond_3

    goto :goto_2

    :cond_3
    move-wide v8, v10

    :goto_2
    invoke-virtual {p0, v8, v9}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    :cond_4
    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getMaterialType()I

    move-result p1

    if-ne p1, v5, :cond_5

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getImageList()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_9

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getImageList()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_9

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getImageList()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_9

    invoke-virtual {p0, v7}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getImageList()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/kwad/sdk/api/KsImage;

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsImage;->getImageUrl()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageUrl(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsImage;->getHeight()I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageHeight(I)V

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsImage;->getWidth()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageWidth(I)V

    goto :goto_4

    :cond_5
    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getMaterialType()I

    move-result p1

    if-ne p1, v7, :cond_7

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getImageList()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_9

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getImageList()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_9

    invoke-virtual {p0, v6}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getImageList()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kwad/sdk/api/KsImage;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsImage;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImages(Ljava/util/List;)V

    goto :goto_4

    :cond_7
    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getMaterialType()I

    move-result p1

    if-ne p1, v3, :cond_8

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_4

    :cond_8
    invoke-virtual {p0, v4}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    :cond_9
    :goto_4
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->c:Lcom/kwad/sdk/api/KsNativeAd$VideoPlayListener;

    invoke-interface {p1, p2}, Lcom/kwad/sdk/api/KsNativeAd;->setVideoPlayListener(Lcom/kwad/sdk/api/KsNativeAd$VideoPlayListener;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method


# virtual methods
.method public getAdType()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/kwad/sdk/api/KsNativeAd;->setVideoPlayListener(Lcom/kwad/sdk/api/KsNativeAd$VideoPlayListener;)V

    iput-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    :cond_0
    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onDestroy()V

    return-void
.end method

.method public registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V

    return-void
.end method

.method public registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;",
            ")V"
        }
    .end annotation

    invoke-super {p0, p1, p2, p3, p4}, Lcom/bytedance/msdk/base/TTBaseAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-interface {p2, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    instance-of p3, p1, Lcom/bytedance/msdk/api/format/TTNativeAdView;

    if-eqz p3, :cond_5

    iget-object p3, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->b:Lcom/kwad/sdk/api/KsNativeAd$AdInteractionListener;

    invoke-interface {p3, p1, p2, v0}, Lcom/kwad/sdk/api/KsNativeAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Lcom/kwad/sdk/api/KsNativeAd$AdInteractionListener;)V

    :cond_1
    iget-object p2, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    const/4 p3, -0x1

    if-eqz p2, :cond_3

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsNativeAd;->getSdkLogo()Landroid/graphics/Bitmap;

    move-result-object p2

    if-eqz p2, :cond_3

    iget p2, p4, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->logoLayoutId:I

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    check-cast p2, Landroid/view/ViewGroup;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->d:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->a(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v1}, Lcom/kwad/sdk/api/KsNativeAd;->getSdkLogo()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {p2, v0, p3, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    goto :goto_0

    :cond_2
    instance-of v0, p2, Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    check-cast p2, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsNativeAd;->getSdkLogo()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_3
    :goto_0
    iget p2, p4, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->mediaViewId:I

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/bytedance/msdk/api/format/TTMediaView;

    if-eqz p1, :cond_5

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->a:Lcom/kwad/sdk/api/KsNativeAd;

    iget-object p4, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->d:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;

    invoke-static {p4}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->a(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)Landroid/content/Context;

    move-result-object p4

    const/4 v0, 0x0

    invoke-interface {p2, p4, v0}, Lcom/kwad/sdk/api/KsNativeAd;->getVideoView(Landroid/content/Context;Lcom/kwad/sdk/api/KsAdVideoPlayConfig;)Landroid/view/View;

    move-result-object p2

    if-nez p2, :cond_4

    return-void

    :cond_4
    invoke-virtual {p0, p2}, Lcom/bytedance/msdk/base/TTBaseAd;->removeSelfFromParent(Landroid/view/View;)V

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/format/TTMediaView;->removeAllViews()V

    invoke-virtual {p1, p2, p3, p3}, Lcom/bytedance/msdk/api/format/TTMediaView;->addView(Landroid/view/View;II)V

    :cond_5
    return-void
.end method
