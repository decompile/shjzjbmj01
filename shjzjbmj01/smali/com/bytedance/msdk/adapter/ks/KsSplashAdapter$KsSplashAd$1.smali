.class Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/kwad/sdk/api/KsSplashScreenAd$SplashScreenAdInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->showSplashAd(Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClicked()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->a(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->b(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdClicked()V

    :cond_0
    return-void
.end method

.method public onAdShowEnd()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->c(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->b(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdDismiss()V

    :cond_0
    return-void
.end method

.method public onAdShowError(ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onAdShowStart()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->d(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->b(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method public onSkippedAd()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->e(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->b(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdSkip()V

    :cond_0
    return-void
.end method
