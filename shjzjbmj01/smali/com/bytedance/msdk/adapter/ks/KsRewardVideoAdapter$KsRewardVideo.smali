.class Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KsRewardVideo"
.end annotation


# instance fields
.field private a:Lcom/kwad/sdk/api/KsRewardVideoAd;

.field private b:Z

.field c:Lcom/kwad/sdk/api/KsLoadManager$RewardVideoAdListener;

.field final synthetic d:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;


# direct methods
.method public constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->d:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance p1, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;-><init>(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->c:Lcom/kwad/sdk/api/KsLoadManager$RewardVideoAdListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method private a()Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;Lcom/kwad/sdk/api/KsRewardVideoAd;)Lcom/kwad/sdk/api/KsRewardVideoAd;
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a:Lcom/kwad/sdk/api/KsRewardVideoAd;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;Lcom/kwad/sdk/api/KsScene;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a(Lcom/kwad/sdk/api/KsScene;)V

    return-void
.end method

.method private a(Lcom/kwad/sdk/api/KsScene;)V
    .locals 2

    invoke-static {}, Lcom/kwad/sdk/api/KsAdSDK;->getLoadManager()Lcom/kwad/sdk/api/KsLoadManager;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->c:Lcom/kwad/sdk/api/KsLoadManager$RewardVideoAdListener;

    invoke-interface {v0, p1, v1}, Lcom/kwad/sdk/api/KsLoadManager;->loadRewardVideoAd(Lcom/kwad/sdk/api/KsScene;Lcom/kwad/sdk/api/KsLoadManager$RewardVideoAdListener;)V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->b:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/kwad/sdk/api/KsRewardVideoAd;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a:Lcom/kwad/sdk/api/KsRewardVideoAd;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a()Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method public adnHasAdExpiredApi()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->d:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;->a(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a:Lcom/kwad/sdk/api/KsRewardVideoAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAdExpired()Z
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a:Lcom/kwad/sdk/api/KsRewardVideoAd;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsRewardVideoAd;->isAdEnable()Z

    move-result v0

    xor-int/2addr v0, v1

    return v0

    :cond_0
    return v1
.end method

.method public isReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a:Lcom/kwad/sdk/api/KsRewardVideoAd;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsRewardVideoAd;->isAdEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a:Lcom/kwad/sdk/api/KsRewardVideoAd;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/kwad/sdk/api/KsRewardVideoAd;->setRewardAdInteractionListener(Lcom/kwad/sdk/api/KsRewardVideoAd$RewardAdInteractionListener;)V

    iput-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a:Lcom/kwad/sdk/api/KsRewardVideoAd;

    :cond_0
    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 3

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a:Lcom/kwad/sdk/api/KsRewardVideoAd;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsRewardVideoAd;->isAdEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->d:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;->f(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->d:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;->g(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getOrientation()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    new-instance v0, Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;

    invoke-direct {v0}, Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;->showLandscape(Z)Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;->build()Lcom/kwad/sdk/api/KsVideoPlayConfig;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a:Lcom/kwad/sdk/api/KsRewardVideoAd;

    invoke-interface {v1, p1, v0}, Lcom/kwad/sdk/api/KsRewardVideoAd;->showRewardVideoAd(Landroid/app/Activity;Lcom/kwad/sdk/api/KsVideoPlayConfig;)V

    :cond_1
    return-void
.end method
