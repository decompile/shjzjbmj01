.class public Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;,
        Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/bytedance/msdk/api/TTVideoOption;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->a:Landroid/content/Context;

    return-object p0
.end method

.method private a(Lcom/kwad/sdk/api/KsScene;)V
    .locals 2

    invoke-static {}, Lcom/kwad/sdk/api/KsAdSDK;->getLoadManager()Lcom/kwad/sdk/api/KsLoadManager;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$1;

    invoke-direct {v1, p0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$1;-><init>(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)V

    invoke-interface {v0, p1, v1}, Lcom/kwad/sdk/api/KsLoadManager;->loadFeedAd(Lcom/kwad/sdk/api/KsScene;Lcom/kwad/sdk/api/KsLoadManager$FeedAdListener;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)Lcom/bytedance/msdk/api/TTVideoOption;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    return-object p0
.end method

.method private b(Lcom/kwad/sdk/api/KsScene;)V
    .locals 2

    invoke-static {}, Lcom/kwad/sdk/api/KsAdSDK;->getLoadManager()Lcom/kwad/sdk/api/KsLoadManager;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$2;

    invoke-direct {v1, p0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$2;-><init>(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)V

    invoke-interface {v0, p1, v1}, Lcom/kwad/sdk/api/KsLoadManager;->loadNativeAd(Lcom/kwad/sdk/api/KsScene;Lcom/kwad/sdk/api/KsLoadManager$NativeAdListener;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "ks"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/kwad/sdk/api/KsAdSDK;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->a:Landroid/content/Context;

    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Lcom/kwad/sdk/api/KsScene$Builder;

    invoke-direct {v0, p1, p2}, Lcom/kwad/sdk/api/KsScene$Builder;-><init>(J)V

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdLoadCount()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/kwad/sdk/api/KsScene$Builder;->adNum(I)Lcom/kwad/sdk/api/KsScene$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/kwad/sdk/api/KsScene$Builder;->build()Lcom/kwad/sdk/api/KsScene;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object p2

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result p2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->a(Lcom/kwad/sdk/api/KsScene;)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result p2

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    invoke-direct {p0, p1}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->b(Lcom/kwad/sdk/api/KsScene;)V

    :cond_1
    :goto_0
    return-void

    :catch_0
    const p1, 0x9c59

    const-string p2, "ad slotId error!"

    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method
