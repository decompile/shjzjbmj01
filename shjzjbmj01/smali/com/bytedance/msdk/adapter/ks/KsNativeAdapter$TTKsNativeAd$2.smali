.class Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/kwad/sdk/api/KsNativeAd$VideoPlayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoPlayComplete()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->h(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->i(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoCompleted()V

    :cond_0
    return-void
.end method

.method public onVideoPlayError(II)V
    .locals 4

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->j(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->b(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/api/AdError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Android MediaPlay Error Code :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, p1, p2}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoError(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void
.end method

.method public onVideoPlayStart()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->f(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;->g(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoStart()V

    :cond_0
    return-void
.end method
