.class Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/kwad/sdk/api/KsLoadManager$SplashScreenAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;

    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onSplashScreenAdLoad(Lcom/kwad/sdk/api/KsSplashScreenAd;)V
    .locals 5
    .param p1    # Lcom/kwad/sdk/api/KsSplashScreenAd;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;

    const/16 v0, 0x4e21

    const-string v1, "load success, but no ad fill !"

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-static {v0, p1}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->a(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;Lcom/kwad/sdk/api/KsSplashScreenAd;)Lcom/kwad/sdk/api/KsSplashScreenAd;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsSplashScreenAd;->getECPM()I

    move-result p1

    int-to-double v0, p1

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_1

    goto :goto_0

    :cond_1
    move-wide v0, v2

    :goto_0
    invoke-virtual {p1, v0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    :cond_2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;->a:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void
.end method
