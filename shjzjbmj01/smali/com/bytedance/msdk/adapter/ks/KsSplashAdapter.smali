.class public Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    const/16 v0, 0xbb8

    iput v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->b:I

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "ks"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/kwad/sdk/api/KsAdSDK;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->a:Landroid/content/Context;

    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v2, Lcom/kwad/sdk/api/KsScene$Builder;

    invoke-direct {v2, v0, v1}, Lcom/kwad/sdk/api/KsScene$Builder;-><init>(J)V

    invoke-virtual {v2}, Lcom/kwad/sdk/api/KsScene$Builder;->build()Lcom/kwad/sdk/api/KsScene;

    move-result-object v0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->a:Landroid/content/Context;

    if-eqz p2, :cond_2

    const-string p1, "ad_load_timeout"

    invoke-interface {p2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "ad_load_timeout"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->b:I

    :goto_0
    iput p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->b:I

    const-string p1, "tt_ad_network_callback"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 p2, 0x0

    instance-of v1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v1, :cond_1

    move-object p2, p1

    check-cast p2, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    :cond_1
    new-instance p1, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-direct {p1, p0, p2}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;-><init>(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->a(Lcom/kwad/sdk/api/KsScene;)V

    :cond_2
    return-void

    :catch_0
    const p1, 0x9c59

    const-string p2, "ad slotId error!"

    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method
