.class Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TTKsExpressAd"
.end annotation


# instance fields
.field a:Lcom/kwad/sdk/api/KsFeedAd;

.field b:Lcom/bytedance/msdk/api/TTDislikeCallback;

.field final synthetic c:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;


# direct methods
.method public constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;Lcom/kwad/sdk/api/KsFeedAd;)V
    .locals 4

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;->c:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;->a:Lcom/kwad/sdk/api/KsFeedAd;

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Lcom/kwad/sdk/api/KsFeedAd;->getECPM()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "ks \u6a21\u677f\u5e7f\u544a\u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TTMediationSDK_ECMP"

    invoke-static {v2, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-lez v0, :cond_0

    int-to-double v0, v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;->a:Lcom/kwad/sdk/api/KsFeedAd;

    new-instance v2, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd$1;

    invoke-direct {v2, p0, p1}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd$1;-><init>(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)V

    invoke-interface {v1, v2}, Lcom/kwad/sdk/api/KsFeedAd;->setAdInteractionListener(Lcom/kwad/sdk/api/KsFeedAd$AdInteractionListener;)V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->b(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->b(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->isMuted()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-interface {p2, v0}, Lcom/kwad/sdk/api/KsFeedAd;->setVideoSoundEnable(Z)V

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method


# virtual methods
.method public getAdType()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public getAdView()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;->a:Lcom/kwad/sdk/api/KsFeedAd;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;->c:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->a(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/kwad/sdk/api/KsFeedAd;->getFeedView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasDestroyed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasDislike()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public render()V
    .locals 4

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->render()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;->a:Lcom/kwad/sdk/api/KsFeedAd;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;->c:Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;->a(Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/kwad/sdk/api/KsFeedAd;->getFeedView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    instance-of v2, v1, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    if-eqz v2, :cond_2

    check-cast v1, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, -0x40000000    # -2.0f

    invoke-interface {v1, v0, v2, v3}, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;->onRenderSuccess(Landroid/view/View;FF)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    instance-of v1, v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    instance-of v1, v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    if-eqz v1, :cond_2

    :goto_0
    check-cast v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    const/16 v1, 0x6a

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v3, v2, v1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;->onRenderFail(Landroid/view/View;Ljava/lang/String;I)V

    :cond_2
    :goto_1
    return-void
.end method

.method public setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/msdk/api/TTDislikeCallback;)V
    .locals 0

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/ks/KsNativeAdapter$TTKsExpressAd;->b:Lcom/bytedance/msdk/api/TTDislikeCallback;

    return-void
.end method
