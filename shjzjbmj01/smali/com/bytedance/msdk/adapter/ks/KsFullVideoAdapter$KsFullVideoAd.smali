.class Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KsFullVideoAd"
.end annotation


# instance fields
.field private a:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

.field private b:Z

.field c:Lcom/kwad/sdk/api/KsLoadManager$FullScreenVideoAdListener;

.field final synthetic d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance p1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;-><init>(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->c:Lcom/kwad/sdk/api/KsLoadManager$FullScreenVideoAdListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method private a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;Lcom/kwad/sdk/api/KsFullScreenVideoAd;)Lcom/kwad/sdk/api/KsFullScreenVideoAd;
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->b:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/kwad/sdk/api/KsFullScreenVideoAd;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method public adnHasAdExpiredApi()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;->c(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAdExpired()Z
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsFullScreenVideoAd;->isAdEnable()Z

    move-result v0

    xor-int/2addr v0, v1

    return v0

    :cond_0
    return v1
.end method

.method public isReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->b:Z

    return v0
.end method

.method public loadAd(Lcom/kwad/sdk/api/KsScene;)V
    .locals 2

    invoke-static {}, Lcom/kwad/sdk/api/KsAdSDK;->getLoadManager()Lcom/kwad/sdk/api/KsLoadManager;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->c:Lcom/kwad/sdk/api/KsLoadManager$FullScreenVideoAdListener;

    invoke-interface {v0, p1, v1}, Lcom/kwad/sdk/api/KsLoadManager;->loadFullScreenVideoAd(Lcom/kwad/sdk/api/KsScene;Lcom/kwad/sdk/api/KsLoadManager$FullScreenVideoAdListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/kwad/sdk/api/KsFullScreenVideoAd;->setFullScreenVideoAdInteractionListener(Lcom/kwad/sdk/api/KsFullScreenVideoAd$FullScreenVideoAdInteractionListener;)V

    iput-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    :cond_0
    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 3

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/kwad/sdk/api/KsFullScreenVideoAd;->isAdEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;->a(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;->b(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getOrientation()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    new-instance v0, Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;

    invoke-direct {v0}, Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;->showLandscape(Z)Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kwad/sdk/api/KsVideoPlayConfig$Builder;->build()Lcom/kwad/sdk/api/KsVideoPlayConfig;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a:Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    invoke-interface {v1, p1, v0}, Lcom/kwad/sdk/api/KsFullScreenVideoAd;->showFullScreenVideoAd(Landroid/app/Activity;Lcom/kwad/sdk/api/KsVideoPlayConfig;)V

    :cond_1
    return-void
.end method
