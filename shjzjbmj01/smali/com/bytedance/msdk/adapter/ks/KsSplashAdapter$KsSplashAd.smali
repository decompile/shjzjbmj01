.class Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KsSplashAd"
.end annotation


# instance fields
.field private a:Lcom/kwad/sdk/api/KsSplashScreenAd;

.field b:Lcom/kwad/sdk/api/KsLoadManager$SplashScreenAdListener;

.field final synthetic c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance p1, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$2;-><init>(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->b:Lcom/kwad/sdk/api/KsLoadManager$SplashScreenAdListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method private a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;Lcom/kwad/sdk/api/KsSplashScreenAd;)Lcom/kwad/sdk/api/KsSplashScreenAd;
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->a:Lcom/kwad/sdk/api/KsSplashScreenAd;

    return-object p1
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method a(Lcom/kwad/sdk/api/KsScene;)V
    .locals 2

    invoke-static {}, Lcom/kwad/sdk/api/KsAdSDK;->getLoadManager()Lcom/kwad/sdk/api/KsLoadManager;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->b:Lcom/kwad/sdk/api/KsLoadManager$SplashScreenAdListener;

    invoke-interface {v0, p1, v1}, Lcom/kwad/sdk/api/KsLoadManager;->loadSplashScreenAd(Lcom/kwad/sdk/api/KsScene;Lcom/kwad/sdk/api/KsLoadManager$SplashScreenAdListener;)V

    return-void
.end method

.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->b(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->a:Lcom/kwad/sdk/api/KsSplashScreenAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->a:Lcom/kwad/sdk/api/KsSplashScreenAd;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->a:Lcom/kwad/sdk/api/KsSplashScreenAd;

    :cond_0
    return-void
.end method

.method public showSplashAd(Landroid/view/ViewGroup;)V
    .locals 3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->a:Lcom/kwad/sdk/api/KsSplashScreenAd;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;->c:Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;->a(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;

    invoke-direct {v2, p0}, Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd$1;-><init>(Lcom/bytedance/msdk/adapter/ks/KsSplashAdapter$KsSplashAd;)V

    invoke-interface {v0, v1, v2}, Lcom/kwad/sdk/api/KsSplashScreenAd;->getView(Landroid/content/Context;Lcom/kwad/sdk/api/KsSplashScreenAd$SplashScreenAdInteractionListener;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    return-void
.end method
