.class public Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "PgRewardVideoAdapter"


# instance fields
.field private a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->onDestroy()V

    :cond_0
    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "ks"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/kwad/sdk/api/KsAdSDK;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-instance p1, Lcom/kwad/sdk/api/KsScene$Builder;

    invoke-direct {p1, v0, v1}, Lcom/kwad/sdk/api/KsScene$Builder;-><init>(J)V

    invoke-virtual {p1}, Lcom/kwad/sdk/api/KsScene$Builder;->build()Lcom/kwad/sdk/api/KsScene;

    move-result-object p1

    if-eqz p2, :cond_1

    const-string v0, "tt_ad_network_callback"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    const/4 v0, 0x0

    instance-of v1, p2, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    :cond_0
    new-instance p2, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-direct {p2, p0, v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;-><init>(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;)V

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;Lcom/kwad/sdk/api/KsScene;)V

    :cond_1
    return-void

    :catch_0
    const p1, 0x9c59

    const-string p2, "ad slotId error!"

    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method
