.class Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/kwad/sdk/api/KsRewardVideoAd$RewardAdInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->onRewardVideoAdLoad(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClicked()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->e(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->f(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onRewardClick()V

    :cond_0
    return-void
.end method

.method public onPageDismiss()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->g(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->f(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onRewardedAdClosed()V

    :cond_0
    return-void
.end method

.method public onRewardVerify()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->c(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->f(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1$1;

    invoke-direct {v1, p0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1$1;-><init>(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;)V

    invoke-interface {v0, v1}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onRewardVerify(Lcom/bytedance/msdk/api/reward/RewardItem;)V

    :cond_0
    return-void
.end method

.method public onVideoPlayEnd()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->a(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->f(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onVideoComplete()V

    :cond_0
    return-void
.end method

.method public onVideoPlayError(II)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->h(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->f(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onVideoError()V

    :cond_0
    return-void
.end method

.method public onVideoPlayStart()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->b(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;->f(Lcom/bytedance/msdk/adapter/ks/KsRewardVideoAdapter$KsRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onRewardedAdShow()V

    :cond_0
    return-void
.end method
