.class Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/kwad/sdk/api/KsFullScreenVideoAd$FullScreenVideoAdInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->onFullScreenVideoAdLoad(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClicked()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->e(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onFullVideoAdClick()V

    :cond_0
    return-void
.end method

.method public onPageDismiss()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->f(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->e(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onFullVideoAdClosed()V

    :cond_0
    return-void
.end method

.method public onSkippedVideo()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->b(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->e(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onSkippedVideo()V

    :cond_0
    return-void
.end method

.method public onVideoPlayEnd()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->h(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->e(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onVideoComplete()V

    :cond_0
    return-void
.end method

.method public onVideoPlayError(II)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->g(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->e(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onVideoError()V

    :cond_0
    return-void
.end method

.method public onVideoPlayStart()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->e(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onFullVideoAdShow()V

    :cond_0
    return-void
.end method
