.class Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/kwad/sdk/api/KsLoadManager$FullScreenVideoAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;Z)Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u52a0\u8f7dks\u5168\u5c4f\u89c6\u9891\u5e7f\u544a\u5931\u8d25\uff1acode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "   msg:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onFullScreenVideoAdLoad(Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/kwad/sdk/api/KsFullScreenVideoAd;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u52a0\u8f7dks\u5168\u5c4f\u89c6\u9891\u5e7f\u544a\u6210\u529f\uff0c\u4f46\u6ca1\u6709\u8fd4\u56de\u6570\u636e"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u52a0\u8f7dks\u5168\u5c4f\u89c6\u9891\u5e7f\u544a\u6210\u529f\uff0c\u6570\u91cf:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :goto_1
    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_3

    :cond_1
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    invoke-static {v1, p1}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;Lcom/kwad/sdk/api/KsFullScreenVideoAd;)Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->c(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    move-result-object p1

    invoke-interface {p1}, Lcom/kwad/sdk/api/KsFullScreenVideoAd;->getECPM()I

    move-result p1

    int-to-double v0, p1

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_2

    goto :goto_2

    :cond_2
    move-wide v0, v2

    :goto_2
    invoke-virtual {p1, v0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    :cond_3
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->c(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;)Lcom/kwad/sdk/api/KsFullScreenVideoAd;

    move-result-object p1

    new-instance v1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;

    invoke-direct {v1, p0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1$1;-><init>(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;)V

    invoke-interface {p1, v1}, Lcom/kwad/sdk/api/KsFullScreenVideoAd;->setFullScreenVideoAdInteractionListener(Lcom/kwad/sdk/api/KsFullScreenVideoAd$FullScreenVideoAdInteractionListener;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    invoke-static {p1, v0}, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->a(Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;Z)Z

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    new-instance v1, Lcom/bytedance/msdk/api/AdError;

    const/16 v2, 0x753a

    const-string v3, "sdk no cache callback "

    invoke-direct {v1, v2, v3}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdVideoCache(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdError;)V

    return-void

    :cond_4
    :goto_3
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter$KsFullVideoAd;->d:Lcom/bytedance/msdk/adapter/ks/KsFullVideoAdapter;

    const/16 v0, 0x4e21

    const-string v1, "load success, but no ad fill !"

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method
