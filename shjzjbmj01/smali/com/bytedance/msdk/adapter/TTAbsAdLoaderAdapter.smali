.class public abstract Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.super Ljava/lang/Object;
.source "TTAbsAdLoaderAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;,
        Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;
    }
.end annotation


# static fields
.field protected static final MAX_INTERVAL_TIME:I = 0x78

.field protected static final MIN_INTERVAL_TIME:I = 0x1e


# instance fields
.field private a:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:D

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:J

.field private i:J

.field private j:I

.field private k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

.field private l:I

.field private m:I

.field protected mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

.field protected mTTExtraModel:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;

.field protected mWaterfallAbTestParam:Ljava/lang/String;

.field private n:Z

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()I
    .locals 2

    .line 60
    iget v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->m:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 61
    iget v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c:I

    add-int/lit16 v0, v0, 0x12c

    return v0

    .line 63
    :cond_0
    iget v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c:I

    return v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/api/AdError;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/api/AdError;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/bytedance/msdk/api/AdError;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 3

    .line 37
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    .line 38
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->o:Ljava/lang/String;

    .line 39
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b:Ljava/lang/String;

    .line 40
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    iget v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->m:I

    .line 41
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->g:Ljava/lang/String;

    .line 42
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    .line 43
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getSdkVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p1, :cond_0

    iget v2, p1, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorCode:I

    goto :goto_0

    :cond_0
    const/4 v2, -0x1

    .line 44
    :goto_0
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorMessage:Ljava/lang/String;

    goto :goto_1

    :cond_1
    const-string p1, "unknown error"

    .line 45
    :goto_1
    invoke-virtual {v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    .line 46
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a()I

    move-result v1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    iget v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->d:I

    .line 47
    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    iget-wide v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->e:D

    .line 48
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->j(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    iget-wide v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->i:J

    .line 49
    invoke-virtual {p1, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mWaterfallAbTestParam:Ljava/lang/String;

    const-string v2, "waterfall_abtest"

    .line 50
    invoke-virtual {p1, v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->f:Ljava/lang/String;

    .line 51
    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    .line 53
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mTTExtraModel:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;

    if-eqz p1, :cond_2

    .line 54
    iget-object p1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;->a:Ljava/lang/String;

    const-string v1, "server_bidding_extra"

    invoke-virtual {v0, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    .line 56
    :cond_2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    if-eqz p1, :cond_3

    .line 57
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result p1

    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    .line 58
    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    .line 59
    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_3
    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    const-string v0, "gdt"

    .line 6
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result p2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result p2

    const/4 v1, 0x5

    if-ne v1, p2, :cond_3

    .line 8
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 9
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->getGDTExtraOption()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 10
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->getGDTExtraOption()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getFeedExpressType()I

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    if-ne p1, v0, :cond_2

    .line 13
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "(1.0)"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 p2, 0x2

    if-ne p1, p2, :cond_3

    .line 15
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "(2.0)"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    return-object p3
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/base/TTBaseAd;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void
.end method

.method private a(Lcom/bytedance/msdk/base/TTBaseAd;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    .line 16
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-nez v4, :cond_1

    iget-wide v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->e:D

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_1

    .line 17
    invoke-virtual {p1, v0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    .line 19
    :cond_1
    iget v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->m:I

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdNetworkSlotType(I)V

    .line 20
    iget v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c:I

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setLoadSort(I)V

    .line 21
    iget v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->d:I

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setShowSort(I)V

    .line 22
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getSdkVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setSdkVersion(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdNetWorkName(Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExchangeRate(Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdNetworkSlotId(Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setRit(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mWaterfallAbTestParam:Ljava/lang/String;

    const-string v1, "waterfall_abtest"

    invoke-virtual {p1, v1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->putEventParam(Ljava/lang/String;Ljava/lang/Object;)V

    .line 28
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/base/a;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setSdkNum(I)V

    .line 31
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    if-eqz v0, :cond_2

    .line 32
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "win_call_back"

    invoke-virtual {p1, v1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->putEventParam(Ljava/lang/String;Ljava/lang/Object;)V

    .line 33
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fail_call_back"

    invoke-virtual {p1, v1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->putEventParam(Ljava/lang/String;Ljava/lang/Object;)V

    .line 35
    :cond_2
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mTTExtraModel:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;

    if-eqz v0, :cond_3

    .line 36
    iget-object v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;->a:Ljava/lang/String;

    const-string v1, "server_bidding_extra"

    invoke-virtual {p1, v1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->putEventParam(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/bytedance/msdk/base/TTBaseAd;Ljava/util/List;Lcom/bytedance/msdk/api/AdError;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/bytedance/msdk/base/TTBaseAd;",
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/base/TTBaseAd;",
            ">;",
            "Lcom/bytedance/msdk/api/AdError;",
            ")V"
        }
    .end annotation

    .line 5
    new-instance v6, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;-><init>(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Ljava/lang/String;Lcom/bytedance/msdk/base/TTBaseAd;Ljava/util/List;Lcom/bytedance/msdk/api/AdError;)V

    invoke-static {v6}, Lcom/bytedance/msdk/adapter/util/ThreadHelper;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->o:Ljava/lang/String;

    return-object p0
.end method

.method private b()Z
    .locals 3

    .line 2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/m;->d(Landroid/content/Context;)Z

    move-result v0

    .line 3
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/m;->e(Landroid/content/Context;)Z

    move-result v1

    .line 4
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->u()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b()Z

    move-result p0

    return p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->d:I

    return p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c:I

    return p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->n:Z

    return p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->m:I

    return p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->f:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->i:J

    return-wide v0
.end method

.method static synthetic k(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->j:I

    return p0
.end method


# virtual methods
.method public abstract destroy()V
.end method

.method public final getAdLoadCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->l:I

    return v0
.end method

.method public abstract getAdNetWorkName()Ljava/lang/String;
.end method

.method public final getAdSlotId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->o:Ljava/lang/String;

    return-object v0
.end method

.method public getAdapterRit()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final getAdm()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isServerBidding()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    if-nez v0, :cond_1

    return-object v1

    .line 9
    :cond_1
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getSdkVersion()Ljava/lang/String;
.end method

.method public final isBidding()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isServerBidding()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isClientBidding()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->m:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isServerBidding()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->m:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isSmartLookRequest()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->n:Z

    return v0
.end method

.method public abstract loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation
.end method

.method public final loadAdInter(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Ljava/util/Map;Lcom/bytedance/msdk/api/AdSlot;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/bytedance/msdk/api/AdSlot;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/bytedance/msdk/api/AdSlot;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;",
            "I)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p4}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b:Ljava/lang/String;

    .line 2
    invoke-virtual {p4}, Lcom/bytedance/msdk/api/AdSlot;->getAdCount()I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->l:I

    .line 3
    invoke-virtual {p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->f:Ljava/lang/String;

    .line 4
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->i()I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c:I

    .line 5
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->n()I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->d:I

    .line 6
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->o:Ljava/lang/String;

    .line 7
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->m()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    .line 8
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->g:Ljava/lang/String;

    .line 9
    iput-object p4, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    .line 10
    iput-object p5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mTTExtraModel:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;

    .line 11
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result p5

    iput p5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->m:I

    const-string p5, "tt_is_smart_look_request"

    .line 12
    invoke-interface {p3, p5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p5

    if-eqz p5, :cond_0

    const-string p5, "tt_is_smart_look_request"

    invoke-interface {p3, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Boolean;

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p5

    if-eqz p5, :cond_0

    const/4 p5, 0x1

    goto :goto_0

    :cond_0
    const/4 p5, 0x0

    :goto_0
    iput-boolean p5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->n:Z

    .line 13
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->o()Ljava/lang/String;

    move-result-object p5

    iput-object p5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mWaterfallAbTestParam:Ljava/lang/String;

    .line 14
    iput p6, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->j:I

    .line 15
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isBidding()Z

    move-result p5

    if-nez p5, :cond_1

    .line 16
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->e:D

    .line 20
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->h:J

    .line 22
    iget-boolean p5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->n:Z

    if-nez p5, :cond_2

    if-eqz p2, :cond_2

    .line 25
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->p()Z

    move-result p5

    if-nez p5, :cond_2

    .line 27
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getSdkVersion()Ljava/lang/String;

    move-result-object p5

    invoke-static {p2, p4, p5, p6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;I)V

    :cond_2
    if-eqz p2, :cond_3

    .line 33
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;

    move-result-object p4

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getSdkVersion()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p4, p2, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    :cond_3
    new-instance p2, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$1;

    invoke-direct {p2, p0, p1, p3}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$1;-><init>(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Landroid/content/Context;Ljava/util/Map;)V

    invoke-static {p2}, Lcom/bytedance/msdk/adapter/util/ThreadHelper;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->h:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->i:J

    const-string v0, "failed"

    const/4 v1, 0x0

    .line 2
    invoke-direct {p0, v0, v1, v1, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Ljava/lang/String;Lcom/bytedance/msdk/base/TTBaseAd;Ljava/util/List;Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public final notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->h:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->i:J

    const-string v0, "adload_ad"

    const/4 v1, 0x0

    .line 2
    invoke-direct {p0, v0, p1, v1, v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Ljava/lang/String;Lcom/bytedance/msdk/base/TTBaseAd;Ljava/util/List;Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public final notifyAdLoaded(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/base/TTBaseAd;",
            ">;)V"
        }
    .end annotation

    .line 3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->h:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->i:J

    const-string v0, "adload_ads"

    const/4 v1, 0x0

    .line 4
    invoke-direct {p0, v0, v1, p1, v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Ljava/lang/String;Lcom/bytedance/msdk/base/TTBaseAd;Ljava/util/List;Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public final notifyAdVideoCache(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdError;)V
    .locals 2

    const-string v0, "ad_video_cache"

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, v0, p1, v1, p2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Ljava/lang/String;Lcom/bytedance/msdk/base/TTBaseAd;Ljava/util/List;Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public final setAdapterListener(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;

    return-void
.end method

.method public startSigmobPreloadWhenClose(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
