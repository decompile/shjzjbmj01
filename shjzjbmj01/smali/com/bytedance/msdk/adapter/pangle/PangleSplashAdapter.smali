.class public Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source "PangleSplashAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;
    }
.end annotation


# instance fields
.field private p:Landroid/content/Context;

.field private q:I

.field private r:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    const/16 v0, 0xbb8

    .line 7
    iput v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->q:I

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->p:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mWaterfallAbTestParam:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->q:I

    return p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic k(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->s:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "pangle"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/bytedance/msdk/adapter/pangle/TTPangleSDKInitManager;->get()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->p:Landroid/content/Context;

    if-eqz p2, :cond_2

    const-string p1, "tt_smart_look_info"

    .line 3
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->s:Ljava/lang/String;

    const-string p1, "ad_load_timeout"

    .line 4
    invoke-interface {p2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "ad_load_timeout"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->q:I

    :goto_0
    iput p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->q:I

    const-string p1, "tt_ad_network_callback"

    .line 5
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 p2, 0x0

    .line 7
    instance-of v0, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_1

    .line 8
    move-object p2, p1

    check-cast p2, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    .line 10
    :cond_1
    new-instance p1, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-direct {p1, p0, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;-><init>(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->r:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    .line 11
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter;->r:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;->loadAd()V

    :cond_2
    return-void
.end method
