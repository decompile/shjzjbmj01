.class Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$1;
.super Ljava/lang/Object;
.source "PangleNativeLoader.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTAdNative$FeedAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader;->loadAd(Landroid/content/Context;ZLcom/bytedance/sdk/openadsdk/TTAdNative;Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

.field final synthetic b:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader;Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$1;->b:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader;

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$1;->a:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$1;->a:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onFeedAdLoad(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/TTFeedAd;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 6
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/sdk/openadsdk/TTFeedAd;

    .line 8
    new-instance v2, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$1;->b:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader;

    invoke-direct {v2, v3, v1}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;-><init>(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader;Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 10
    :cond_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$1;->a:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Ljava/util/List;)V

    return-void

    .line 11
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$1;->a:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    const/16 v0, 0x4e21

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method
