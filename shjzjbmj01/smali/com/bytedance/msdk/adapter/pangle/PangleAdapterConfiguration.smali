.class public Lcom/bytedance/msdk/adapter/pangle/PangleAdapterConfiguration;
.super Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;
.source "PangleAdapterConfiguration.java"


# static fields
.field public static final AD_PLACEMENT_ID_EXTRA_KEY:Ljava/lang/String; = "ad_placement_id"

.field public static final APP_ID_EXTRA_KEY:Ljava/lang/String; = "app_id"

.field private static final b:Ljava/lang/String; = "PangleAdapterConfiguration"

.field private static c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    if-nez p0, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    sget-boolean v0, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterConfiguration;->c:Z

    if-nez v0, :cond_1

    .line 8
    invoke-static {p0, p1}, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->initTTPangleSDK(Landroid/content/Context;Ljava/lang/String;)V

    const/4 p0, 0x1

    .line 9
    sput-boolean p0, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterConfiguration;->c:Z

    :cond_1
    return-void

    .line 10
    :cond_2
    :goto_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p1, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterConfiguration;->b:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ",Invalid Pangle app ID. Ensure the app id is valid on the MoPub dashboard."

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "TTMediationSDK_ADAPTER"

    invoke-static {p1, p0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(ILcom/bytedance/msdk/api/AdSlot;)[I
    .locals 3

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 11
    :pswitch_0
    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez p1, :cond_0

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result p1

    if-lez p1, :cond_0

    .line 12
    new-array p1, v0, [I

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    aput v0, p1, v2

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result p2

    aput p2, p1, v1

    return-object p1

    .line 13
    :cond_0
    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result p1

    if-lez p1, :cond_1

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result p1

    if-gez p1, :cond_1

    .line 14
    new-array p1, v0, [I

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result p2

    aput p2, p1, v2

    aput v2, p1, v1

    return-object p1

    .line 15
    :pswitch_1
    new-array p1, v0, [I

    fill-array-data p1, :array_0

    return-object p1

    .line 16
    :pswitch_2
    new-array p1, v0, [I

    fill-array-data p1, :array_1

    return-object p1

    .line 17
    :pswitch_3
    new-array p1, v0, [I

    fill-array-data p1, :array_2

    return-object p1

    .line 18
    :pswitch_4
    new-array p1, v0, [I

    fill-array-data p1, :array_3

    return-object p1

    .line 19
    :pswitch_5
    new-array p1, v0, [I

    fill-array-data p1, :array_4

    return-object p1

    .line 35
    :cond_1
    :goto_0
    new-array p1, v0, [I

    fill-array-data p1, :array_5

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x2d8
        0x5a
    .end array-data

    :array_1
    .array-data 4
        0x1d4
        0x3c
    .end array-data

    :array_2
    .array-data 4
        0x12c
        0xfa
    .end array-data

    :array_3
    .array-data 4
        0x140
        0x64
    .end array-data

    :array_4
    .array-data 4
        0x140
        0x32
    .end array-data

    :array_5
    .array-data 4
        0x140
        0x32
    .end array-data
.end method


# virtual methods
.method public getAdNetworkName()Ljava/lang/String;
    .locals 1

    const-string v0, "pangle"

    return-object v0
.end method

.method public getAdapterVersion()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->getSDKVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBiddingToken(Landroid/content/Context;Ljava/util/Map;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-eqz p2, :cond_e

    .line 1
    sget-object p1, Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;->TT_MSDK_ADSLOT_INFO:Ljava/lang/String;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 2
    instance-of p2, p1, Lcom/bytedance/msdk/api/AdSlot;

    if-eqz p2, :cond_e

    .line 3
    check-cast p1, Lcom/bytedance/msdk/api/AdSlot;

    .line 4
    new-instance p2, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    .line 5
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setAdType(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p2

    .line 6
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdCount()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setAdCount(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p2

    .line 7
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p2

    .line 8
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getOrientation()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setOrientation(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p2

    .line 9
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getRewardAmount()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setRewardAmount(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p2

    .line 10
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setRewardName(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p2

    .line 11
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->isSupportDeepLink()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setSupportDeepLink(Z)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p2

    .line 12
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getUserID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setUserID(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p2

    .line 13
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getMediaExtra()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setMediaExtra(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object p2

    .line 15
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x5

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-ne v0, v1, :cond_2

    .line 16
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 21
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    goto/16 :goto_3

    :cond_1
    :goto_0
    const/high16 v0, 0x44870000    # 1080.0f

    const/high16 v1, 0x44f00000    # 1920.0f

    .line 22
    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    const/16 v0, 0x438

    const/16 v1, 0x780

    .line 23
    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    goto/16 :goto_3

    .line 28
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    if-eq v0, v4, :cond_3

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    if-eq v0, v2, :cond_3

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    if-ne v0, v3, :cond_a

    .line 29
    :cond_3
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v0

    if-eq v0, v4, :cond_7

    .line 30
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    if-eq v0, v4, :cond_7

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    if-ne v0, v3, :cond_4

    goto :goto_2

    .line 43
    :cond_4
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    if-gtz v0, :cond_5

    goto :goto_1

    .line 46
    :cond_5
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    goto :goto_3

    :cond_6
    :goto_1
    const/16 v0, 0x280

    const/16 v1, 0x154

    .line 47
    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    goto :goto_3

    .line 48
    :cond_7
    :goto_2
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    if-ne v0, v4, :cond_8

    .line 49
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getBannerSize()I

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterConfiguration;->a(ILcom/bytedance/msdk/api/AdSlot;)[I

    move-result-object v0

    const/4 v1, 0x0

    .line 50
    aget v1, v0, v1

    int-to-float v1, v1

    aget v0, v0, v4

    int-to-float v0, v0

    invoke-virtual {p2, v1, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    goto :goto_3

    .line 52
    :cond_8
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v0

    if-gtz v0, :cond_9

    .line 53
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    goto :goto_3

    .line 54
    :cond_9
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v0

    if-lez v0, :cond_a

    .line 55
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 69
    :cond_a
    :goto_3
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v0

    if-ne v0, v4, :cond_b

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    if-ne v0, v2, :cond_b

    .line 70
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setNativeAdType(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 72
    :cond_b
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    if-eq v0, v4, :cond_c

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    if-ne v0, v3, :cond_d

    .line 73
    :cond_c
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setNativeAdType(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 76
    :cond_d
    invoke-virtual {p2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object p1

    goto :goto_4

    :cond_e
    const/4 p1, 0x0

    .line 80
    :goto_4
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->getBiddingToken(Lcom/bytedance/sdk/openadsdk/AdSlot;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNetworkSdkVersion()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initializeNetwork(Landroid/content/Context;Ljava/util/Map;Lcom/bytedance/msdk/adapter/config/TTOnNetworkInitializationFinishedListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/bytedance/msdk/adapter/config/TTOnNetworkInitializationFinishedListener;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/bytedance/msdk/adapter/util/Preconditions;->checkNotNull(Ljava/lang/Object;)V

    .line 2
    invoke-static {p3}, Lcom/bytedance/msdk/adapter/util/Preconditions;->checkNotNull(Ljava/lang/Object;)V

    .line 3
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;->isInitedSuccess()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :try_start_0
    const-string v0, "app_id"

    .line 5
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 6
    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterConfiguration;->a(Landroid/content/Context;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 7
    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;->setInitedSuccess(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "TTMediationSDK_ADAPTER"

    const-string v0, "Initializing Pangle has encountered an exception."

    .line 9
    invoke-static {p2, v0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 11
    :goto_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;->isInitedSuccess()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 12
    new-instance p1, Lcom/bytedance/msdk/api/error/InitSdkError;

    const/16 p2, 0x753b

    const-string v0, "pangle"

    invoke-direct {p1, p2, v0}, Lcom/bytedance/msdk/api/error/InitSdkError;-><init>(ILjava/lang/String;)V

    const-class p2, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterConfiguration;

    invoke-interface {p3, p2, p1}, Lcom/bytedance/msdk/adapter/config/TTOnNetworkInitializationFinishedListener;->onNetworkInitializationFinished(Ljava/lang/Class;Lcom/bytedance/msdk/api/AdError;)V

    goto :goto_1

    .line 15
    :cond_0
    new-instance p1, Lcom/bytedance/msdk/api/error/InitSdkError;

    const/16 p2, 0x753c

    const-string v0, "pangle"

    invoke-direct {p1, p2, v0}, Lcom/bytedance/msdk/api/error/InitSdkError;-><init>(ILjava/lang/String;)V

    const-class p2, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterConfiguration;

    invoke-interface {p3, p2, p1}, Lcom/bytedance/msdk/adapter/config/TTOnNetworkInitializationFinishedListener;->onNetworkInitializationFinished(Ljava/lang/Class;Lcom/bytedance/msdk/api/AdError;)V

    :cond_1
    :goto_1
    return-void
.end method
