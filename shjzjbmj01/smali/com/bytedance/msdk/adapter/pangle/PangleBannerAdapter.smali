.class public Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source "PangleBannerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;
    }
.end annotation


# instance fields
.field private p:Landroid/content/Context;

.field private q:I

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    const/16 v0, 0x1e

    .line 13
    iput v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->q:I

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->p:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mWaterfallAbTestParam:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->r:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->q:I

    return p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic k(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "pangle"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->p:Landroid/content/Context;

    if-eqz p2, :cond_4

    const-string p1, "tt_ad_network_callback"

    .line 3
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 v0, 0x0

    .line 5
    instance-of v1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz v1, :cond_0

    .line 6
    move-object v0, p1

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    :cond_0
    const-string p1, "tt_smart_look_info"

    .line 9
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->r:Ljava/lang/String;

    const-string p1, "refresh_time"

    .line 11
    invoke-interface {p2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    const/16 v1, 0x1e

    if-eqz p1, :cond_1

    const-string p1, "refresh_time"

    .line 12
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_1
    const/16 p1, 0x1e

    :goto_0
    iput p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->q:I

    .line 14
    iget p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->q:I

    if-gez p1, :cond_2

    .line 15
    iput v1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->q:I

    goto :goto_1

    :cond_2
    const/16 p2, 0x78

    if-le p1, p2, :cond_3

    .line 17
    iput p2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->q:I

    .line 20
    :cond_3
    :goto_1
    new-instance p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    invoke-direct {p1, p0, v0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;-><init>(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;)V

    .line 21
    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->loadAd()V

    :cond_4
    return-void
.end method
