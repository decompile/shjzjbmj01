.class Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;
.super Ljava/lang/Object;
.source "PangleNativeLoader.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTFeedAd$VideoAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressUpdate(JJ)V
    .locals 0

    return-void
.end method

.method public onVideoAdComplete(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->g(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->h(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoCompleted()V

    :cond_0
    return-void
.end method

.method public onVideoAdContinuePlay(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->e(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->f(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoResume()V

    :cond_0
    return-void
.end method

.method public onVideoAdPaused(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->c(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->d(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoPause()V

    :cond_0
    return-void
.end method

.method public onVideoAdStartPlay(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->a(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->b(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoStart()V

    :cond_0
    return-void
.end method

.method public onVideoError(II)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->o(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;->p(Lcom/bytedance/msdk/adapter/pangle/PangleNativeLoader$PangleNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/api/AdError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Android MediaPlay Error Code :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, p1, p2}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoError(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void
.end method

.method public onVideoLoad(Lcom/bytedance/sdk/openadsdk/TTFeedAd;)V
    .locals 0

    return-void
.end method
