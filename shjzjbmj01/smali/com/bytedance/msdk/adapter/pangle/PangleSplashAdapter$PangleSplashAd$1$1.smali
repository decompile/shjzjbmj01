.class Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;
.super Ljava/lang/Object;
.source "PangleSplashAdapter.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTSplashAd$AdInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;->onSplashAdLoad(Lcom/bytedance/sdk/openadsdk/TTSplashAd;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClicked(Landroid/view/View;I)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;->b(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;->c(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/splash/TTSplashAdListener;->onAdClicked()V

    :cond_0
    return-void
.end method

.method public onAdShow(Landroid/view/View;I)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;->d(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;->c(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/splash/TTSplashAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method public onAdSkip()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;->e(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;->c(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/splash/TTSplashAdListener;->onAdSkip()V

    :cond_0
    return-void
.end method

.method public onAdTimeOver()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;->f(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;->c(Lcom/bytedance/msdk/adapter/pangle/PangleSplashAdapter$PangleSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/splash/TTSplashAdListener;->onAdDismiss()V

    :cond_0
    return-void
.end method
