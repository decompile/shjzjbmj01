.class public Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source "PangleInterstitialAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter$PangleExpressInterstitialAd;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "PangleInterstitialAdapter"


# instance fields
.field private p:Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter$PangleExpressInterstitialAd;

.field private q:Landroid/content/Context;

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;->q:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mWaterfallAbTestParam:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;->r:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;->p:Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter$PangleExpressInterstitialAd;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter$PangleExpressInterstitialAd;->onDestroy()V

    :cond_0
    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "pangle"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;->q:Landroid/content/Context;

    if-eqz p2, :cond_1

    const-string p1, "tt_ad_network_callback"

    .line 3
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 v0, 0x0

    .line 5
    instance-of v1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz v1, :cond_0

    .line 6
    move-object v0, p1

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    :cond_0
    const-string p1, "tt_smart_look_info"

    .line 8
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;->r:Ljava/lang/String;

    .line 9
    new-instance p1, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter$PangleExpressInterstitialAd;

    invoke-direct {p1, p0, v0}, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter$PangleExpressInterstitialAd;-><init>(Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;->p:Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter$PangleExpressInterstitialAd;

    .line 10
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter;->p:Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter$PangleExpressInterstitialAd;

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleInterstitialAdapter$PangleExpressInterstitialAd;->loadAd()V

    :cond_1
    return-void
.end method
