.class public Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source "PangleFullVideoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter$PangleFullVideoAd;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "PangleFullVideoAdapter"


# instance fields
.field private p:Landroid/content/Context;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;->p:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mWaterfallAbTestParam:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;->q:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "pangle"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/bytedance/sdk/openadsdk/TTAdSdk;->getAdManager()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;->p:Landroid/content/Context;

    if-eqz p2, :cond_1

    const-string p1, "tt_ad_network_callback"

    .line 3
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 v0, 0x0

    .line 5
    instance-of v1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz v1, :cond_0

    .line 6
    move-object v0, p1

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    :cond_0
    const-string p1, "tt_smart_look_info"

    .line 8
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;->q:Ljava/lang/String;

    .line 9
    new-instance p1, Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter$PangleFullVideoAd;

    invoke-direct {p1, p0, v0}, Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter$PangleFullVideoAd;-><init>(Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;)V

    .line 10
    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleFullVideoAdapter$PangleFullVideoAd;->loadAd()V

    :cond_1
    return-void
.end method
