.class Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source "PangleRewardVideoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PangleRewardVideo"
.end annotation


# instance fields
.field private a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

.field private b:Z

.field c:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

.field final synthetic d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;


# direct methods
.method public constructor <init>(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    .line 89
    new-instance p1, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo$1;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo$1;-><init>(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    .line 90
    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method private a()Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;)Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;
    .locals 0

    .line 3
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->loadAd()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->b:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a()Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method private loadAd()V
    .locals 4

    .line 1
    invoke-static {}, Lcom/bytedance/msdk/adapter/pangle/TTPangleSDKInitManager;->get()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->a(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->createAdNative(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/TTAdNative;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->f(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->updateData(Ljava/lang/String;)V

    .line 4
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->k(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getMediaExtra()Ljava/lang/String;

    move-result-object v1

    .line 5
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->l(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getCustomData()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "pangle"

    .line 7
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 8
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v1, v2

    .line 13
    :cond_0
    new-instance v2, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    .line 14
    invoke-virtual {v3}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    .line 15
    invoke-static {v3}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->c(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/msdk/api/AdSlot;->isSupportDeepLink()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setSupportDeepLink(Z)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    .line 16
    invoke-static {v3}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->b(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setRewardName(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    .line 17
    invoke-static {v3}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->q(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/msdk/api/AdSlot;->getUserID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setUserID(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    .line 18
    invoke-static {v3}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->p(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v3

    invoke-virtual {v3}, Lcom/bytedance/msdk/api/AdSlot;->getRewardAmount()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setRewardAmount(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    .line 19
    invoke-virtual {v2, v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setMediaExtra(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v1

    const/16 v2, 0x438

    const/16 v3, 0x780

    .line 20
    invoke-virtual {v1, v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setImageAcceptedSize(II)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    .line 21
    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->o(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getAdloadSeq()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setAdloadSeq(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    .line 22
    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->n(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setPrimeRit(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    .line 23
    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->m(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getOrientation()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setOrientation(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v1

    .line 25
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdm()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 26
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->withBid(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 29
    :cond_1
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isSmartLookRequest()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->d(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 30
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->d(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExtraParam(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 33
    :cond_2
    invoke-virtual {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->c:Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;

    invoke-interface {v0, v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadRewardVideoAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$RewardVideoAdListener;)V

    return-void
.end method


# virtual methods
.method public adnHasAdVideoCachedApi()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAdId()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;->getMediaExtraInfo()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->getAdId(Ljava/util/Map;)J

    move-result-wide v0

    return-wide v0

    .line 4
    :cond_0
    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAdType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->d:Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;->e(Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public getCreativeId()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;->getMediaExtraInfo()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->getCreativeId(Ljava/util/Map;)J

    move-result-wide v0

    return-wide v0

    .line 4
    :cond_0
    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCreativeId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getReqId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;->getMediaExtraInfo()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->getReqId(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 4
    :cond_0
    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getReqId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasDestroyed()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isReady()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->b:Z

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 2
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;->setDownloadListener(Lcom/bytedance/sdk/openadsdk/TTAppDownloadListener;)V

    .line 3
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;->setRewardAdInteractionListener(Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd$RewardAdInteractionListener;)V

    .line 4
    iput-object v1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    :cond_0
    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleRewardVideoAdapter$PangleRewardVideo;->a:Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTRewardVideoAd;->showRewardVideoAd(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method
