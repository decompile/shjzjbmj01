.class Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;
.super Ljava/lang/Object;
.source "PangleBannerAdapter.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->loadAd()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onNativeExpressAdLoad(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    .line 1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->b(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    iput-object p1, v0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    .line 7
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->i(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)I

    move-result p1

    mul-int/lit16 p1, p1, 0x3e8

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setSlideIntervalTime(I)V

    .line 8
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    .line 9
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getInteractionType()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    .line 10
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getImageMode()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    .line 12
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 13
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getMediaExtraInfo()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_2

    const-string v0, "price"

    .line 16
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->getValue(Ljava/lang/Object;)D

    move-result-wide v0

    .line 17
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v3, v3, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {v3}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "pangle banner \u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "TTMediationSDK_ECMP"

    invoke-static {v2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_1

    goto :goto_0

    :cond_1
    move-wide v0, v2

    :goto_0
    invoke-virtual {p1, v0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    .line 21
    :cond_2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->e:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    invoke-interface {v0, p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;)V

    .line 22
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {p1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->render()V

    .line 23
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->b(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->b:Landroid/view/View;

    .line 24
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void

    .line 25
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    const/16 v0, 0x4e21

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method
