.class Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source "PangleBannerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TTBannerView"
.end annotation


# instance fields
.field a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

.field b:Landroid/view/View;

.field final c:Ljava/lang/Object;

.field private d:Z

.field e:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

.field final synthetic f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    .line 149
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->c:Ljava/lang/Object;

    .line 195
    new-instance p1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;-><init>(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->e:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;

    .line 196
    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method private a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->d:Z

    return p1
.end method

.method private a(I)[I
    .locals 2

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 4
    :pswitch_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->j(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result p1

    if-lez p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->k(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result p1

    if-lez p1, :cond_0

    .line 5
    new-array p1, v0, [I

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->l(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    const/4 v1, 0x0

    aput v0, p1, v1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->c(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v0

    const/4 v1, 0x1

    aput v0, p1, v1

    return-object p1

    .line 6
    :pswitch_1
    new-array p1, v0, [I

    fill-array-data p1, :array_0

    return-object p1

    .line 7
    :pswitch_2
    new-array p1, v0, [I

    fill-array-data p1, :array_1

    return-object p1

    .line 8
    :pswitch_3
    new-array p1, v0, [I

    fill-array-data p1, :array_2

    return-object p1

    .line 9
    :pswitch_4
    new-array p1, v0, [I

    fill-array-data p1, :array_3

    return-object p1

    .line 10
    :pswitch_5
    new-array p1, v0, [I

    fill-array-data p1, :array_4

    return-object p1

    .line 24
    :cond_0
    :goto_0
    new-array p1, v0, [I

    fill-array-data p1, :array_5

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x2d8
        0x5a
    .end array-data

    :array_1
    .array-data 4
        0x1d4
        0x3c
    .end array-data

    :array_2
    .array-data 4
        0x12c
        0xfa
    .end array-data

    :array_3
    .array-data 4
        0x140
        0x64
    .end array-data

    :array_4
    .array-data 4
        0x140
        0x32
    .end array-data

    :array_5
    .array-data 4
        0x140
        0x32
    .end array-data
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method public getAdId()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getMediaExtraInfo()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->getAdId(Ljava/util/Map;)J

    move-result-wide v0

    return-wide v0

    .line 4
    :cond_0
    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAdType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->d(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public declared-synchronized getAdView()Landroid/view/View;
    .locals 7

    monitor-enter p0

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->c:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2
    :try_start_1
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->b:Landroid/view/View;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    .line 4
    :try_start_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x7d0

    add-long/2addr v3, v1

    .line 6
    :goto_0
    iget-boolean v5, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->d:Z

    if-nez v5, :cond_0

    cmp-long v5, v1, v3

    if-gez v5, :cond_0

    .line 7
    iget-object v5, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->c:Ljava/lang/Object;

    const/4 v6, 0x0

    sub-long v1, v3, v1

    invoke-virtual {v5, v1, v2}, Ljava/lang/Object;->wait(J)V

    .line 8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 11
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 14
    :cond_0
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 15
    :try_start_4
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->b:Landroid/view/View;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    .line 16
    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    .line 0
    monitor-exit p0

    throw v0
.end method

.method public getCreativeId()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getMediaExtraInfo()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->getCreativeId(Ljava/util/Map;)J

    move-result-wide v0

    return-wide v0

    .line 4
    :cond_0
    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCreativeId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getReqId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->getMediaExtraInfo()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->getReqId(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 4
    :cond_0
    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getReqId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasDestroyed()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasDislike()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public loadAd()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->a(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getBannerSize()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a(I)[I

    move-result-object v0

    .line 2
    invoke-static {}, Lcom/bytedance/msdk/adapter/pangle/TTPangleSDKInitManager;->get()Lcom/bytedance/sdk/openadsdk/TTAdManager;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->b(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/bytedance/sdk/openadsdk/TTAdManager;->createAdNative(Landroid/content/Context;)Lcom/bytedance/sdk/openadsdk/TTAdNative;

    move-result-object v1

    .line 3
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->e(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->updateData(Ljava/lang/String;)V

    .line 4
    new-instance v2, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    invoke-direct {v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;-><init>()V

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    .line 5
    invoke-virtual {v3}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setCodeId(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 6
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setSupportDeepLink(Z)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    .line 7
    invoke-static {v4}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->g(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v4

    invoke-virtual {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getAdloadSeq()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setAdloadSeq(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    .line 8
    invoke-static {v4}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->f(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setPrimeRit(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    .line 9
    invoke-virtual {v2, v3}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setAdCount(I)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v2

    const/4 v4, 0x0

    aget v4, v0, v4

    int-to-float v4, v4

    aget v0, v0, v3

    int-to-float v0, v0

    .line 10
    invoke-virtual {v2, v4, v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExpressViewAcceptedSize(FF)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    move-result-object v0

    .line 11
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdm()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 12
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->withBid(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 15
    :cond_0
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isSmartLookRequest()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->h(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 16
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;->h(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->setExtraParam(Ljava/lang/String;)Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;

    .line 20
    :cond_1
    invoke-virtual {v0}, Lcom/bytedance/sdk/openadsdk/AdSlot$Builder;->build()Lcom/bytedance/sdk/openadsdk/AdSlot;

    move-result-object v0

    new-instance v2, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;

    invoke-direct {v2, p0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$1;-><init>(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)V

    invoke-interface {v1, v0, v2}, Lcom/bytedance/sdk/openadsdk/TTAdNative;->loadBannerExpressAd(Lcom/bytedance/sdk/openadsdk/AdSlot;Lcom/bytedance/sdk/openadsdk/TTAdNative$NativeExpressAdListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onDestroy()V

    .line 2
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 3
    invoke-interface {v0, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setExpressInteractionListener(Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$AdInteractionListener;)V

    .line 4
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    invoke-interface {v0}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->destroy()V

    :cond_0
    return-void
.end method

.method public setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/msdk/api/TTDislikeCallback;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a:Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;

    if-eqz v0, :cond_0

    .line 2
    new-instance v1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$2;

    invoke-direct {v1, p0, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$2;-><init>(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;Lcom/bytedance/msdk/api/TTDislikeCallback;)V

    invoke-interface {v0, p1, v1}, Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd;->setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/sdk/openadsdk/TTAdDislike$DislikeInteractionCallback;)V

    :cond_0
    return-void
.end method
