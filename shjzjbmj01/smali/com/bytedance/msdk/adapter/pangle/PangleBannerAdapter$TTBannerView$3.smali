.class Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;
.super Ljava/lang/Object;
.source "PangleBannerAdapter.java"

# interfaces
.implements Lcom/bytedance/sdk/openadsdk/TTNativeExpressAd$ExpressAdInteractionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClicked(Landroid/view/View;I)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->c(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->b(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/banner/TTAdBannerListener;->onAdClicked()V

    :cond_0
    return-void
.end method

.method public onAdShow(Landroid/view/View;I)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->d(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->b(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/banner/TTAdBannerListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method public onRenderFail(Landroid/view/View;Ljava/lang/String;I)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;Z)Z

    .line 2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "TTBannerView onRenderFail -> code="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ",msg="

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "TTMediationSDK_banner"

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onRenderSuccess(Landroid/view/View;FF)V
    .locals 1

    .line 1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p3, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object p3, p3, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {p3}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object p3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->f:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "TTBannerView onRenderSuccess \u6e32\u67d3\u6210\u529f\uff01\uff01"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "TTMediationSDK_banner"

    invoke-static {p3, p2}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    iget-object p2, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    iget-object p2, p2, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->b:Landroid/view/View;

    instance-of p3, p2, Landroid/widget/FrameLayout;

    if-eqz p3, :cond_0

    .line 3
    check-cast p2, Landroid/widget/FrameLayout;

    new-instance p3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p3, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p2, p1, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView$3;->a:Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;->a(Lcom/bytedance/msdk/adapter/pangle/PangleBannerAdapter$TTBannerView;Z)Z

    return-void
.end method
