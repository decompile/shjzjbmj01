.class Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SigMobFullVideoAd"
.end annotation


# instance fields
.field private a:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

.field private b:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;

.field private c:Z

.field d:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAdListener;

.field final synthetic e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;-><init>(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->d:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAdListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method private a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;->b(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->c:Z

    return v0
.end method

.method public loadAd()V
    .locals 4

    invoke-static {}, Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;->sharedInstance()Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->d:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAdListener;

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;->setWindFullScreenVideoAdListener(Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAdListener;)V

    new-instance v0, Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getUserID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->b:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->b:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;->loadAd(Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;)Z

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;->setWindFullScreenVideoAdListener(Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAdListener;)V

    iput-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

    :cond_0
    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 2

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->b:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;->isReady(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->b:Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;

    invoke-virtual {v0, p1, v1}, Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAd;->show(Landroid/app/Activity;Lcom/sigmob/windad/fullscreenvideo/WindFullScreenAdRequest;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method
