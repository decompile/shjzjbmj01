.class Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/sigmob/windad/fullscreenvideo/WindFullScreenVideoAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFullScreenVideoAdClicked(Ljava/lang/String;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->d(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->b(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onFullVideoAdClick()V

    :cond_0
    return-void
.end method

.method public onFullScreenVideoAdClosed(Ljava/lang/String;)V
    .locals 2

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;->c(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->startSigmobPreloadWhenClose(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->b(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onFullVideoAdClosed()V

    :cond_0
    return-void
.end method

.method public onFullScreenVideoAdLoadError(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V
    .locals 1

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;Z)Z

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sigmob/windad/WindAdError;->getErrorCode()I

    move-result p2

    invoke-virtual {p1}, Lcom/sigmob/windad/WindAdError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/bytedance/msdk/api/AdErrorUtil;->getSigmobError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/sigmob/windad/WindAdError;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p2, Lcom/bytedance/msdk/api/AdError;->message:Ljava/lang/String;

    :cond_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    const/16 p2, 0x4e21

    const-string v0, "load success, but no ad fill !"

    invoke-static {p2, v0}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p2

    :goto_0
    invoke-virtual {p1, p2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onFullScreenVideoAdLoadSuccess(Ljava/lang/String;)V
    .locals 2

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1, v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;Z)Z

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdVideoCache(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onFullScreenVideoAdPlayEnd(Ljava/lang/String;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->c(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->b(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onVideoComplete()V

    :cond_0
    return-void
.end method

.method public onFullScreenVideoAdPlayError(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->f(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->b(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onVideoError()V

    :cond_0
    return-void
.end method

.method public onFullScreenVideoAdPlayStart(Ljava/lang/String;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->b(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;->onFullVideoAdShow()V

    :cond_0
    return-void
.end method

.method public onFullScreenVideoAdPreLoadFail(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    const/4 v1, 0x0

    invoke-static {v1, p1}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onFullScreenVideoAdPreLoadSuccess(Ljava/lang/String;)V
    .locals 1

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-static {p1, v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;Z)Z

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void
.end method
