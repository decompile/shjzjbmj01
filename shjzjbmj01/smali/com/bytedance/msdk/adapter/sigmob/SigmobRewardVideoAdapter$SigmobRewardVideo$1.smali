.class Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoAdClicked(Ljava/lang/String;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->g(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onRewardClick()V

    :cond_0
    return-void
.end method

.method public onVideoAdClosed(Lcom/sigmob/windad/rewardedVideo/WindRewardInfo;Ljava/lang/String;)V
    .locals 1

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;->b(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;)Landroid/content/Context;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->startSigmobPreloadWhenClose(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->b(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onRewardedAdClosed()V

    :cond_0
    return-void
.end method

.method public onVideoAdLoadError(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V
    .locals 1

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;Z)Z

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sigmob/windad/WindAdError;->getErrorCode()I

    move-result p2

    invoke-virtual {p1}, Lcom/sigmob/windad/WindAdError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/bytedance/msdk/api/AdErrorUtil;->getSigmobError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/sigmob/windad/WindAdError;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p2, Lcom/bytedance/msdk/api/AdError;->message:Ljava/lang/String;

    :cond_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    const/16 p2, 0x4e21

    const-string v0, "load success, but no ad fill !"

    invoke-static {p2, v0}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p2

    :goto_0
    invoke-virtual {p1, p2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onVideoAdLoadSuccess(Ljava/lang/String;)V
    .locals 2

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1, v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;Z)Z

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdVideoCache(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onVideoAdPlayEnd(Ljava/lang/String;)V
    .locals 1

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->f(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onVideoComplete()V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p1

    new-instance v0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1$1;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1$1;-><init>(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;)V

    invoke-interface {p1, v0}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onRewardVerify(Lcom/bytedance/msdk/api/reward/RewardItem;)V

    :cond_0
    return-void
.end method

.method public onVideoAdPlayError(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->c(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onVideoError()V

    :cond_0
    return-void
.end method

.method public onVideoAdPlayStart(Ljava/lang/String;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->d(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object p1

    instance-of p1, p1, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;->onRewardedAdShow()V

    :cond_0
    return-void
.end method

.method public onVideoAdPreLoadFail(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;Z)Z

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-static {v1, p1}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->buildAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onVideoAdPreLoadSuccess(Ljava/lang/String;)V
    .locals 1

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    invoke-static {p1, v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;Z)Z

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    iget-object v0, p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void
.end method
