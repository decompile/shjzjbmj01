.class public Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "SigmobFullVideoAdapter"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;->a:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "sigmob"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/sigmob/windad/WindAds;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;->a:Landroid/content/Context;

    if-eqz p2, :cond_1

    const-string p1, "tt_ad_network_callback"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 p2, 0x0

    instance-of v0, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz v0, :cond_0

    move-object p2, p1

    check-cast p2, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    :cond_0
    new-instance p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;

    invoke-direct {p1, p0, p2}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;-><init>(Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;)V

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/sigmob/SigmobFullVideoAdapter$SigMobFullVideoAd;->loadAd()V

    :cond_1
    return-void
.end method
