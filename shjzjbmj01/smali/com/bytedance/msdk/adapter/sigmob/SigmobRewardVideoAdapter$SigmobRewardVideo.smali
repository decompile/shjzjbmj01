.class Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SigmobRewardVideo"
.end annotation


# instance fields
.field private a:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

.field private b:Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;

.field private c:Z

.field d:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAdListener;

.field final synthetic e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;


# direct methods
.method public constructor <init>(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance p1, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;-><init>(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->d:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAdListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method private a()Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->b()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method private b()V
    .locals 4

    invoke-static {}, Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;->sharedInstance()Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->d:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAdListener;

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;->setWindRewardedVideoAdListener(Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAdListener;)V

    new-instance v0, Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;->a(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getUserID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->b:Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->b:Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;->loadAd(Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;)Z

    return-void
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a()Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method public getAdType()I
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;->c(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->c:Z

    return v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;->setWindRewardedVideoAdListener(Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAdListener;)V

    iput-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

    :cond_0
    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 2

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->b:Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;->isReady(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->a:Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->b:Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;

    invoke-virtual {v0, p1, v1}, Lcom/sigmob/windad/rewardedVideo/WindRewardedVideoAd;->show(Landroid/app/Activity;Lcom/sigmob/windad/rewardedVideo/WindRewardAdRequest;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method
