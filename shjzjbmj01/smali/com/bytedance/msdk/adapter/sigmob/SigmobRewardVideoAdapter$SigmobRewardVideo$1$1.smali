.class Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/bytedance/msdk/api/reward/RewardItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->onVideoAdPlayEnd(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAmount()F
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;->d(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getRewardAmount()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public getCustomData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getRewardName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter$SigmobRewardVideo;->e:Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;->e(Lcom/bytedance/msdk/adapter/sigmob/SigmobRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public rewardVerify()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
