.class Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;
.super Ljava/lang/Object;
.source "TTAbsAdLoaderAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallBackRunnable"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/bytedance/msdk/base/TTBaseAd;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/base/TTBaseAd;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/bytedance/msdk/api/AdError;

.field final synthetic e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Ljava/lang/String;Lcom/bytedance/msdk/base/TTBaseAd;Ljava/util/List;Lcom/bytedance/msdk/api/AdError;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/bytedance/msdk/base/TTBaseAd;",
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/base/TTBaseAd;",
            ">;",
            "Lcom/bytedance/msdk/api/AdError;",
            ")V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->a:Ljava/lang/String;

    .line 3
    iput-object p3, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 4
    iput-object p4, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->c:Ljava/util/List;

    .line 5
    iput-object p5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->d:Lcom/bytedance/msdk/api/AdError;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;)Lcom/bytedance/msdk/base/TTBaseAd;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    return-object p0
.end method


# virtual methods
.method public run()V
    .locals 14

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 3
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;-><init>()V

    .line 4
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    .line 5
    invoke-static {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->h(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    .line 6
    invoke-static {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->g(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;->a(Z)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    .line 7
    invoke-static {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->f(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;->c(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    .line 8
    invoke-static {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->e(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;->d(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    .line 9
    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    .line 10
    invoke-static {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;

    .line 12
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->a:Ljava/lang/String;

    const-string v2, "adload_ads"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x2

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->a:Ljava/lang/String;

    const-string v3, "adload_ad"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_1

    .line 44
    :cond_0
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->a:Ljava/lang/String;

    const-string v3, "failed"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 45
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->g(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 47
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->h(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v1

    if-eq v1, v2, :cond_1

    .line 48
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->d:Lcom/bytedance/msdk/api/AdError;

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/api/AdError;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v3, v2, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->k(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v2

    invoke-static {v1, v3, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Lcom/bytedance/msdk/api/AdSlot;I)V

    .line 51
    :cond_1
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/a;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->d:Lcom/bytedance/msdk/api/AdError;

    invoke-direct {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/a;-><init>(Lcom/bytedance/msdk/api/AdError;)V

    .line 52
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mWaterfallAbTestParam:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/a;->a(Ljava/lang/String;)V

    .line 53
    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;->onAdFailed(Lcom/bytedance/msdk/api/AdError;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;)V

    .line 54
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->d:Lcom/bytedance/msdk/api/AdError;

    if-eqz v0, :cond_12

    .line 55
    sget-boolean v0, Lcom/bytedance/msdk/base/b;->b:Z

    if-eqz v0, :cond_2

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fill_fail"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "AdNetWorkName["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] AdUnitId["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] AdType["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v2, v1, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v4, v4, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v4}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v4

    iget-object v5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v5, v5, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v5}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/bytedance/msdk/base/a;->a(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] \u8bf7\u6c42\u5931\u8d25 (loadSort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->f(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",showSort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->e(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "),error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->d:Lcom/bytedance/msdk/api/AdError;

    iget v1, v1, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",msg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->d:Lcom/bytedance/msdk/api/AdError;

    iget-object v1, v1, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 58
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fill_fail"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "AdNetWorkName["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] AdType["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v2, v1, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v4, v4, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v4}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v4

    iget-object v5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v5, v5, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v5}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/bytedance/msdk/base/a;->a(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] \u8bf7\u6c42\u5931\u8d25 error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->d:Lcom/bytedance/msdk/api/AdError;

    iget v1, v1, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",msg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->d:Lcom/bytedance/msdk/api/AdError;

    iget-object v1, v1, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 62
    :cond_3
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->a:Ljava/lang/String;

    const-string v1, "ad_video_cache"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 63
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdType()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdType()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_8

    .line 65
    :cond_4
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->d:Lcom/bytedance/msdk/api/AdError;

    if-eqz v0, :cond_5

    iget v0, v0, Lcom/bytedance/msdk/api/AdError;->code:I

    const/16 v1, 0x753a

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->d(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 66
    new-instance v0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable$1;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable$1;-><init>(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;)V

    const-wide/16 v1, 0x3e8

    invoke-static {v0, v1, v2}, Lcom/bytedance/msdk/adapter/util/ThreadHelper;->postOnUiThreadDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 76
    :cond_5
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isServerBidding()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 77
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->b(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    .line 79
    :cond_6
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setCacheSuccess(Z)V

    .line 80
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;->onAdVideoCache()V

    .line 81
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/base/TTBaseAd;I)V

    .line 83
    :goto_0
    sget-boolean v0, Lcom/bytedance/msdk/base/b;->b:Z

    if-eqz v0, :cond_7

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fill"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "AdNetWorkName["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] AdUnitId["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] AdType["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/base/a;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] \u89c6\u9891\u7f13\u5b58\u6210\u529f (loadSort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->f(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",showSort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->e(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 86
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fill"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "AdNetWorkName["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] AdType["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/base/a;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] \u89c6\u9891\u7f13\u5b58\u6210\u529f "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 89
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fill"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "onAdVideoCache-----ttAd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_9
    :goto_1
    const/4 v1, 0x0

    .line 90
    iget-object v3, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->a:Ljava/lang/String;

    const-string v4, "adload_ads"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/16 v4, 0x4e21

    const/16 v5, 0x4e20

    if-eqz v3, :cond_d

    .line 91
    iget-object v3, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->c:Ljava/util/List;

    if-eqz v3, :cond_a

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_a

    const/16 v4, 0x4e20

    .line 92
    :cond_a
    iget-object v3, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v6, :cond_b

    .line 94
    iget-object v7, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v7, v6}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/base/TTBaseAd;)V

    if-nez v1, :cond_b

    move-object v1, v6

    goto :goto_2

    .line 98
    :cond_c
    iget-object v3, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v3}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;

    move-result-object v3

    iget-object v6, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->c:Ljava/util/List;

    invoke-interface {v3, v6, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;->onAdLoaded(Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;)V

    goto :goto_3

    .line 100
    :cond_d
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v1, :cond_e

    const/16 v4, 0x4e20

    .line 101
    :cond_e
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-static {v1, v3}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/base/TTBaseAd;)V

    .line 102
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;

    move-result-object v1

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-interface {v1, v3, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$AdapterLoaderListener;->onAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;)V

    .line 103
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->b:Lcom/bytedance/msdk/base/TTBaseAd;

    :goto_3
    move-object v6, v1

    move v7, v4

    .line 106
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->g(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Z

    move-result v0

    if-nez v0, :cond_12

    if-ne v7, v5, :cond_f

    const-string v0, "load success"

    :goto_4
    move-object v8, v0

    goto :goto_5

    :cond_f
    const-string v0, "load success, but no ad fill !"

    goto :goto_4

    .line 110
    :goto_5
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->h(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v0

    if-eq v0, v2, :cond_10

    .line 111
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->i(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->j(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)J

    move-result-wide v10

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v12, v0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->k(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v13

    invoke-static/range {v6 .. v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;ILjava/lang/String;Ljava/lang/String;JLcom/bytedance/msdk/api/AdSlot;I)V

    .line 113
    :cond_10
    sget-boolean v0, Lcom/bytedance/msdk/base/b;->b:Z

    if-eqz v0, :cond_11

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fill"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "AdNetWorkName["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] AdUnitId["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->b(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] AdType["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v2, v1, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v4, v4, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v4}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v4

    iget-object v5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v5, v5, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v5}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/bytedance/msdk/base/a;->a(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] \u8bf7\u6c42\u6210\u529f (loadSort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->f(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",showSort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->e(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 116
    :cond_11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->c(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "fill"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "AdNetWorkName["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] AdType["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v2, v1, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v4, v4, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v4}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v4

    iget-object v5, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter$CallBackRunnable;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iget-object v5, v5, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v5}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/bytedance/msdk/base/a;->a(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->a(Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "] \u8bf7\u6c42\u6210\u529f"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    :goto_6
    return-void
.end method
