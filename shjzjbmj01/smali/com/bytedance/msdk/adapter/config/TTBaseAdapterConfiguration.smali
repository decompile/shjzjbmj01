.class public abstract Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;
.super Ljava/lang/Object;
.source "TTBaseAdapterConfiguration.java"

# interfaces
.implements Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;


# static fields
.field public static final APP_ID_EXTRA_KEY:Ljava/lang/String; = "app_id"

.field public static final APP_KEY_EXTRA_KEY:Ljava/lang/String; = "app_key"

.field public static final APP_NAME_EXTRA_KEY:Ljava/lang/String; = "app_name"

.field public static final BAIDU_IS_READ_DEVICE_ID:Ljava/lang/String; = "baidu_is_read_device_id"

.field public static TT_MSDK_ADSLOT_INFO:Ljava/lang/String; = "tt_msdk_adslot_info"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;->a:Z

    return-void
.end method


# virtual methods
.method public getMsdkRequestOptions()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public isInitedSuccess()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;->a:Z

    return v0
.end method

.method public setInitedSuccess(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;->a:Z

    return-void
.end method

.method public setMsdkRequestOptions(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
