.class public Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/bytedance/msdk/api/TTVideoOption;

.field private c:Lcom/bytedance/msdk/api/GDTExtraOption;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)Lcom/bytedance/msdk/api/GDTExtraOption;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->c:Lcom/bytedance/msdk/api/GDTExtraOption;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)I
    .locals 0

    iget p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->e:I

    return p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)I
    .locals 0

    iget p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->d:I

    return p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "gdt"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/qq/e/comm/managers/status/SDKStatus;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->a:Landroid/content/Context;

    if-eqz p2, :cond_2

    const-string p1, "tt_ad_network_config_appid"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    const-string p1, "tt_ad_network_callback"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object p2

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/TTVideoOption;->getGDTExtraOption()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object p2

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->c:Lcom/bytedance/msdk/api/GDTExtraOption;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->c:Lcom/bytedance/msdk/api/GDTExtraOption;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTMinVideoDuration()I

    move-result p2

    iput p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->d:I

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->c:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTMaxVideoDuration()I

    move-result p2

    iput p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->e:I

    :cond_0
    const/4 p2, 0x0

    instance-of v0, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    if-eqz v0, :cond_1

    move-object p2, p1

    check-cast p2, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    :cond_1
    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;

    invoke-direct {p1, p0, p2}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;)V

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a()V

    :cond_2
    return-void
.end method
