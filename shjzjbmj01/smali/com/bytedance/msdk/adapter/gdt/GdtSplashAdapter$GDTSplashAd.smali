.class Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GDTSplashAd"
.end annotation


# instance fields
.field private a:Lcom/qq/e/ads/splash/SplashAD;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/RelativeLayout;

.field private d:Landroid/widget/FrameLayout;

.field e:Lcom/qq/e/ads/splash/SplashADListener;

.field final synthetic f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;)V
    .locals 6
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x10
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    new-instance v0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->e:Lcom/qq/e/ads/splash/SplashADListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    new-instance p2, Landroid/widget/RelativeLayout;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    new-instance v2, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$1;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3, p1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;Landroid/content/Context;Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)V

    new-instance v3, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v3}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    new-instance v4, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v4, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v4}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    const-string v5, "#80000000"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v4}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const-string v1, "#80000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->a(I)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const/16 v1, 0x14

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)Landroid/content/Context;

    move-result-object p1

    const/high16 v3, 0x42960000    # 75.0f

    invoke-static {p1, v3}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->dp2px(Landroid/content/Context;F)I

    move-result p1

    const/4 v3, -0x2

    invoke-direct {v1, p1, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 p1, 0xa

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 p1, 0x15

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 p1, 0x1e

    iput p1, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput p1, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p2, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->d:Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->b:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->c:Landroid/widget/RelativeLayout;

    return-void
.end method

.method private a(I)Landroid/graphics/drawable/GradientDrawable;
    .locals 2

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setGradientType(I)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)Landroid/content/Context;

    move-result-object p1

    const/high16 v1, 0x41000000    # 8.0f

    invoke-static {p1, v1}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->dp2px(Landroid/content/Context;F)I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    return-object v0
.end method

.method private a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->a()Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->b:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method public getAdType()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->a:Lcom/qq/e/ads/splash/SplashAD;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public loadAd()V
    .locals 8

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->b(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    const/16 v1, 0xbb8

    :goto_0
    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;I)I

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->b(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)I

    move-result v0

    const/16 v1, 0x1388

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    goto :goto_0

    :cond_1
    :goto_1
    new-instance v0, Lcom/qq/e/ads/splash/SplashAD;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->e:Lcom/qq/e/ads/splash/SplashADListener;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->b(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;)I

    move-result v7

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/qq/e/ads/splash/SplashAD;-><init>(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Lcom/qq/e/ads/splash/SplashADListener;I)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->a:Lcom/qq/e/ads/splash/SplashAD;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->a:Lcom/qq/e/ads/splash/SplashAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/splash/SplashAD;->fetchAdOnly()V

    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->a:Lcom/qq/e/ads/splash/SplashAD;

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->e:Lcom/qq/e/ads/splash/SplashADListener;

    iput-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;Landroid/content/Context;)Landroid/content/Context;

    return-void
.end method

.method public showSplashAd(Landroid/view/ViewGroup;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->a:Lcom/qq/e/ads/splash/SplashAD;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->c:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->d:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->a:Lcom/qq/e/ads/splash/SplashAD;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->d:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Lcom/qq/e/ads/splash/SplashAD;->showAd(Landroid/view/ViewGroup;)V

    :cond_0
    return-void
.end method
