.class Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/nativ/NativeADMediaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoClicked()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->h(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->i(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;->onAdClick()V

    :cond_0
    const-string v0, "GdtNativeLoader"

    const-string v1, "onVideoClicked"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoCompleted()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->d(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoCompleted()V

    :cond_0
    return-void
.end method

.method public onVideoError(Lcom/qq/e/comm/util/AdError;)V
    .locals 3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->f(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->g(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/api/AdError;

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v2

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoError(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void
.end method

.method public onVideoInit()V
    .locals 2

    const-string v0, "GdtNativeLoader"

    const-string v1, "onVideoInit: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoLoaded(I)V
    .locals 1

    const-string p1, "GdtNativeLoader"

    const-string v0, "onVideoLoaded: "

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoLoading()V
    .locals 2

    const-string v0, "GdtNativeLoader"

    const-string v1, "onVideoLoading: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoPause()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->p(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoPause()V

    :cond_0
    return-void
.end method

.method public onVideoReady()V
    .locals 2

    const-string v0, "GdtNativeLoader"

    const-string v1, "onVideoReady"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoResume()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->c(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoResume()V

    :cond_0
    return-void
.end method

.method public onVideoStart()V
    .locals 2

    const-string v0, "GdtNativeLoader"

    const-string v1, "onVideoStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->n(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->o(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoStart()V

    :cond_0
    return-void
.end method

.method public onVideoStop()V
    .locals 2

    const-string v0, "GdtNativeLoader"

    const-string v1, "onVideoStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
