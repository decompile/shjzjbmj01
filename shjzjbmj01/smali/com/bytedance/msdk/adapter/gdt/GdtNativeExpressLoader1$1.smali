.class Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/nativ/NativeExpressAD$NativeExpressADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onADClicked(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 1

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;->onAdClick()V

    :cond_0
    return-void
.end method

.method public onADCloseOverlay(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    return-void
.end method

.method public onADClosed(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 2

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;

    instance-of v1, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->closeAd()V

    :cond_0
    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/qq/e/ads/nativ/NativeExpressADView;->destroy()V

    :cond_2
    return-void
.end method

.method public onADExposure(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 1

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method public onADLeftApplication(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    return-void
.end method

.method public onADLoaded(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/qq/e/ads/nativ/NativeExpressADView;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qq/e/ads/nativ/NativeExpressADView;

    if-eqz v1, :cond_1

    new-instance v2, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    invoke-direct {v2, v3, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;Lcom/qq/e/ads/nativ/NativeExpressADView;)V

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Ljava/util/List;)V

    return-void

    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    move-result-object p1

    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x4e21

    const-string v2, "load success, but no ad fill !"

    invoke-direct {v0, v1, v2}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onADOpenOverlay(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    return-void
.end method

.method public onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->getGdtError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void
.end method

.method public onRenderFail(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 3

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;

    if-eqz v0, :cond_0

    const/16 v1, 0x6a

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v2, v1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;->onRenderFail(Landroid/view/View;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public onRenderSuccess(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 3

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;

    if-eqz v0, :cond_0

    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, -0x40000000    # -2.0f

    invoke-interface {v0, p1, v1, v2}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;->onRenderSuccess(Landroid/view/View;FF)V

    :cond_0
    return-void
.end method
