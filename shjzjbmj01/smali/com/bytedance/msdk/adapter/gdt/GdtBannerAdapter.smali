.class public Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;
    }
.end annotation


# instance fields
.field private a:I

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    const/16 v0, 0x1e

    iput v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->a:I

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;)I
    .locals 0

    iget p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->a:I

    return p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "gdt"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/qq/e/comm/managers/status/SDKStatus;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->b:Landroid/content/Context;

    if-eqz p2, :cond_4

    const-string p1, "tt_ad_network_config_appid"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    const-string p1, "tt_ad_network_callback"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    :cond_0
    const-string p1, "refresh_time"

    invoke-interface {p2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    const/16 v1, 0x1e

    if-eqz p1, :cond_1

    const-string p1, "refresh_time"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_1
    const/16 p1, 0x1e

    :goto_0
    iput p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->a:I

    iget p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->a:I

    if-gez p1, :cond_2

    iput v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->a:I

    goto :goto_1

    :cond_2
    const/16 p2, 0x78

    if-le p1, p2, :cond_3

    iput p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->a:I

    :cond_3
    :goto_1
    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-direct {p1, p0, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;)V

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a()V

    :cond_4
    return-void
.end method
