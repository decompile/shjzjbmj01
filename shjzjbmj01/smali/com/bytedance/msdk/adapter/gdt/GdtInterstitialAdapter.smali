.class public Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/bytedance/msdk/api/TTVideoOption;

.field private c:Lcom/bytedance/msdk/api/GDTExtraOption;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)Lcom/bytedance/msdk/api/GDTExtraOption;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->c:Lcom/bytedance/msdk/api/GDTExtraOption;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)I
    .locals 0

    iget p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->e:I

    return p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)I
    .locals 0

    iget p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->d:I

    return p0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "gdt"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/qq/e/comm/managers/status/SDKStatus;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->a:Landroid/content/Context;

    if-eqz p2, :cond_4

    const-string p1, "tt_ad_network_config_appid"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    const-string p1, "tt_ad_network_callback"

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "minVideoDuration"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "minVideoDuration"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->d:I

    const-string v0, "maxVideoDuration"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "maxVideoDuration"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_1
    iput v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->e:I

    const/4 p2, 0x0

    instance-of v0, p1, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz v0, :cond_2

    move-object p2, p1

    check-cast p2, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    :cond_2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->getGDTExtraOption()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->c:Lcom/bytedance/msdk/api/GDTExtraOption;

    :cond_3
    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-direct {p1, p0, p2}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;)V

    invoke-virtual {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a()V

    :cond_4
    return-void
.end method
