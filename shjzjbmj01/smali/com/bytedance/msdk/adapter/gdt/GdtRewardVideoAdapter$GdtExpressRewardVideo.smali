.class Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GdtExpressRewardVideo"
.end annotation


# instance fields
.field private a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

.field private b:Z

.field c:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAdListener;

.field final synthetic d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo$1;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->c:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAdListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;)Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->b:Z

    return p1
.end method

.method private b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    return-object v0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method a()V
    .locals 4

    new-instance v0, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;->b(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->c:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAdListener;

    invoke-direct {v0, v1, v2, v3}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAdListener;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;)Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;)Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/TTVideoOption;->isMuted()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {v1, v0}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->setVolumeOn(Z)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;->g(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;->h(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getUserID()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;->i(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;)Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getCustomData()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "gdt"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    new-instance v2, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    invoke-direct {v2}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;-><init>()V

    invoke-virtual {v2, v1}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setCustomData(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->setUserId(Ljava/lang/String;)Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {v2}, Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions$Builder;->build()Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->setServerSideVerificationOptions(Lcom/qq/e/ads/rewardvideo/ServerSideVerificationOptions;)V

    :cond_1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->loadAD()V

    return-void
.end method

.method public adnHasAdExpiredApi()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAdType()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAdExpired()Z
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->getExpireTimestamp()J

    move-result-wide v4

    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :cond_1
    :goto_0
    return v3
.end method

.method public isReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->hasShown()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->b:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    iput-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    :cond_0
    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtExpressRewardVideo;->a:Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/rewardvideo2/ExpressRewardVideoAD;->showAD(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method
