.class Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GdtIntersitialAd"
.end annotation


# instance fields
.field private a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

.field private b:Z

.field c:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;

.field final synthetic d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->c:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->b:Z

    return p1
.end method

.method private b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    return-object v0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method a()V
    .locals 4

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->c:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;

    invoke-direct {v0, v1, v2, v3}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->b(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->getVideoOption(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v1, v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setVideoOption(Lcom/qq/e/ads/cfg/VideoOption;)V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->c(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->c(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setMaxVideoDuration(I)V

    :cond_0
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->d(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->d(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setMinVideoDuration(I)V

    :cond_1
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/cfg/VideoOption;->getAutoPlayPolicy()I

    move-result v0

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->getVideoPlayPolicy(ILandroid/content/Context;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setVideoPlayPolicy(I)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->loadAD()V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "GDT--\u63d2\u5c4f-\u4f20\u5165mContext \u4e0d\u662f Activity \u8bf7\u6c42\u4e2d\u65ad"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK_interisitial"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public getAdType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->b:Z

    return v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->destroy()V

    iput-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    :cond_0
    iput-object v1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onDestroy()V

    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->show(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method
