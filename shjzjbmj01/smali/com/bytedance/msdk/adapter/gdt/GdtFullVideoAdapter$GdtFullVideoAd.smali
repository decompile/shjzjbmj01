.class Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GdtFullVideoAd"
.end annotation


# instance fields
.field private a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

.field private b:Z

.field c:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;

.field d:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialMediaListener;

.field final synthetic e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd$1;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->c:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;

    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd$2;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd$2;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->d:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialMediaListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->b:Z

    return p1
.end method

.method private b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    return-object v0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;)Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterFullVideoAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method a()V
    .locals 4

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->c:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;

    invoke-direct {v0, v1, v2, v3}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->b(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->getVideoOption(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v1, v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setVideoOption(Lcom/qq/e/ads/cfg/VideoOption;)V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->c(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->c(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setMaxVideoDuration(I)V

    :cond_0
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->d(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->d(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setMinVideoDuration(I)V

    :cond_1
    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/cfg/VideoOption;->getAutoPlayPolicy()I

    move-result v0

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->e:Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->getVideoPlayPolicy(ILandroid/content/Context;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->setVideoPlayPolicy(I)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->loadFullScreenAD()V

    :cond_2
    return-void
.end method

.method public adnHasAdVideoCachedApi()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAdType()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->b:Z

    return v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->destroy()V

    iput-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    :cond_0
    iput-object v1, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onDestroy()V

    return-void
.end method

.method public showAd(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtFullVideoAdapter$GdtFullVideoAd;->a:Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->showFullScreenAD(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method
