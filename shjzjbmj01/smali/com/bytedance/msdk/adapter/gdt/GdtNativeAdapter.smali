.class public Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;
.super Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/bytedance/msdk/api/TTVideoOption;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public getAdNetWorkName()Ljava/lang/String;
    .locals 1

    const-string v0, "gdt"

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/qq/e/comm/managers/status/SDKStatus;->getSDKVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;->a:Landroid/content/Context;

    if-eqz p2, :cond_3

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result p1

    const/4 p2, 0x2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->getGDTExtraOption()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getFeedExpressType()I

    move-result v0

    :cond_0
    if-ne v0, p2, :cond_1

    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    invoke-direct {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;-><init>()V

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdLoadCount()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->a(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    move-result-object p2

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->a(Ljava/lang/String;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->c(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->b(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->a(Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v0

    invoke-virtual {p1, p2, v0, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->loadAd(Landroid/content/Context;ZLcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    invoke-direct {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;-><init>()V

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdLoadCount()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    move-result-object p2

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a(Ljava/lang/String;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->c(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->mAdSolt:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->b(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a(Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v0

    invoke-virtual {p1, p2, v0, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->loadAd(Landroid/content/Context;ZLcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)V

    goto :goto_0

    :cond_2
    if-ne p1, p2, :cond_3

    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;

    invoke-direct {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;-><init>()V

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdLoadCount()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->a(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;

    move-result-object p2

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->a(Ljava/lang/String;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;

    move-result-object p2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;->b:Lcom/bytedance/msdk/api/TTVideoOption;

    invoke-virtual {p2, v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->a(Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeAdapter;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v0

    invoke-virtual {p1, p2, v0, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->loadAd(Landroid/content/Context;ZLcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)V

    :cond_3
    :goto_0
    return-void
.end method
