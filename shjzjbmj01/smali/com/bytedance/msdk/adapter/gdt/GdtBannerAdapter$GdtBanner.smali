.class Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GdtBanner"
.end annotation


# instance fields
.field private a:Lcom/qq/e/ads/banner2/UnifiedBannerView;

.field private b:Z

.field private c:Lcom/qq/e/ads/banner2/UnifiedBannerADListener;

.field final synthetic d:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->d:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->b:Z

    new-instance p1, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;

    invoke-direct {p1, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->c:Lcom/qq/e/ads/banner2/UnifiedBannerADListener;

    iput-object p2, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->b:Z

    return p0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->b:Z

    return p1
.end method

.method private b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    check-cast v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    return-object v0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;
    .locals 0

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->b()Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    return-object p0
.end method


# virtual methods
.method a()V
    .locals 4

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->d:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/qq/e/ads/banner2/UnifiedBannerView;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->d:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->a(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->d:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->c:Lcom/qq/e/ads/banner2/UnifiedBannerADListener;

    invoke-direct {v0, v1, v2, v3}, Lcom/qq/e/ads/banner2/UnifiedBannerView;-><init>(Landroid/app/Activity;Ljava/lang/String;Lcom/qq/e/ads/banner2/UnifiedBannerADListener;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a:Lcom/qq/e/ads/banner2/UnifiedBannerView;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a:Lcom/qq/e/ads/banner2/UnifiedBannerView;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->d:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;->b(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/banner2/UnifiedBannerView;->setRefresh(I)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a:Lcom/qq/e/ads/banner2/UnifiedBannerView;

    invoke-virtual {v0}, Lcom/qq/e/ads/banner2/UnifiedBannerView;->loadAD()V

    :cond_0
    return-void
.end method

.method public getAdType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAdView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a:Lcom/qq/e/ads/banner2/UnifiedBannerView;

    return-object v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a:Lcom/qq/e/ads/banner2/UnifiedBannerView;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a:Lcom/qq/e/ads/banner2/UnifiedBannerView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/qq/e/ads/banner2/UnifiedBannerView;->destroy()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTAdatperCallback:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a:Lcom/qq/e/ads/banner2/UnifiedBannerView;

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onDestroy()V

    return-void
.end method
