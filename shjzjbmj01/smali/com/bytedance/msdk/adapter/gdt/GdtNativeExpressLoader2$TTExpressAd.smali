.class Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""

# interfaces
.implements Lcom/qq/e/ads/nativ/express2/AdEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TTExpressAd"
.end annotation


# instance fields
.field a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

.field b:Lcom/bytedance/msdk/api/TTDislikeCallback;

.field c:Z

.field private d:Lcom/qq/e/ads/nativ/express2/MediaEventListener;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)V
    .locals 3

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->d:Lcom/qq/e/ads/nativ/express2/MediaEventListener;

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    invoke-interface {p2, p0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->setAdEventListener(Lcom/qq/e/ads/nativ/express2/AdEventListener;)V

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->isVideoAd()Z

    move-result p2

    const/4 v0, 0x3

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->d:Lcom/qq/e/ads/nativ/express2/MediaEventListener;

    invoke-interface {p2, v1}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->setMediaListener(Lcom/qq/e/ads/nativ/express2/MediaEventListener;)V

    const/4 p2, 0x5

    invoke-virtual {p0, p2}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    :goto_0
    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    invoke-interface {p1}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->getECPMLevel()Ljava/lang/String;

    move-result-object p1

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    invoke-static {p1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    goto :goto_1

    :cond_1
    const-wide/16 v0, 0x0

    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    const-string p2, ""

    invoke-static {p2}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string p2, "TTMediationSDK_ECMP"

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GDT \u6a21\u677fNative \u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GDT \u6a21\u677fNative \u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a\u8f6c\u6362\u5f02\u5e38\uff0ceCPMLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ",\u5f02\u5e38\uff1a"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "TTMediationSDK"

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_2
    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->b:Lcom/bytedance/msdk/api/TTDislikeCallback;

    if-eqz v0, :cond_0

    const/4 v1, -0x1

    const-string v2, "ad close !"

    invoke-interface {v0, v1, v2}, Lcom/bytedance/msdk/api/TTDislikeCallback;->onSelected(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method


# virtual methods
.method public getAdType()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public getAdView()Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->getAdView()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasDislike()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onAdClosed()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->destroy()V

    :cond_0
    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a()V

    return-void
.end method

.method public onClick()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;->onAdClick()V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->destroy()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->b:Lcom/bytedance/msdk/api/TTDislikeCallback;

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onDestroy()V

    return-void
.end method

.method public onExposed()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method public onRenderFail()V
    .locals 4

    const-string v0, "TTMediationSDK"

    const-string v1, "GDT----->onRenderFail1"

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    instance-of v1, v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    const/16 v1, 0x6a

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v3, v2, v1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;->onRenderFail(Landroid/view/View;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public onRenderSuccess()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->c:Z

    const-string v0, "TTMediationSDK"

    const-string v1, "GDT----->onRenderSuccess1"

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    instance-of v1, v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    invoke-interface {v1}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->getAdView()Landroid/view/View;

    move-result-object v1

    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, -0x40000000    # -2.0f

    invoke-interface {v0, v1, v2, v3}, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;->onRenderSuccess(Landroid/view/View;FF)V

    :cond_0
    return-void
.end method

.method public render()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a:Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;->render()V

    :cond_0
    return-void
.end method

.method public setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/msdk/api/TTDislikeCallback;)V
    .locals 0

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->b:Lcom/bytedance/msdk/api/TTDislikeCallback;

    return-void
.end method
