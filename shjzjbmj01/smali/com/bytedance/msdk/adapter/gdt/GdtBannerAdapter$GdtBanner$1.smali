.class Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/banner2/UnifiedBannerADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onADClicked()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->e(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->c(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;->onAdClicked()V

    :cond_0
    return-void
.end method

.method public onADCloseOverlay()V
    .locals 0

    return-void
.end method

.method public onADClosed()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->d(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->c(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;->onAdClosed()V

    :cond_0
    return-void
.end method

.method public onADExposure()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->b(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->c(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method public onADLeftApplication()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->f(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->c(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;->onAdLeftApplication()V

    :cond_0
    return-void
.end method

.method public onADOpenOverlay()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->g(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->c(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;->onAdOpened()V

    :cond_0
    return-void
.end method

.method public onADReceive()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    iget-object v1, v0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->d:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;

    invoke-virtual {v1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;Z)Z

    :cond_0
    return-void
.end method

.method public onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->a(Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;Z)Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->d:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->getGdtError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter$GdtBanner;->d:Lcom/bytedance/msdk/adapter/gdt/GdtBannerAdapter;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    :cond_1
    :goto_0
    return-void
.end method
