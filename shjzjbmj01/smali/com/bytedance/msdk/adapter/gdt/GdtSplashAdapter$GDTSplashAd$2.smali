.class Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/splash/SplashADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

.field public millisUntilFinished:J


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onADClicked()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "GDT-\u5f00\u5c4f\u5e7f\u544a--onADClicked...."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->d(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->b(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdClicked()V

    :cond_0
    return-void
.end method

.method public onADDismissed()V
    .locals 2

    const-string v0, "TTMediationSDK"

    const-string v1, "GDT-\u5f00\u5c4f\u5e7f\u544a--onADDismissed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->c(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->b(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdDismiss()V

    :cond_0
    return-void
.end method

.method public onADExposure()V
    .locals 3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->b(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;->onAdShow()V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "GDT-\u5f00\u5c4f\u5e7f\u544a_onADExposure......onADExposure....."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onADLoaded(J)V
    .locals 3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr p1, v0

    const-wide/16 v0, 0x3e8

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    iget-object p2, p1, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-virtual {p2, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    const/16 p2, 0x4e21

    const-string v0, "Ad has expired !"

    invoke-static {p2, v0}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    :goto_0
    return-void
.end method

.method public onADPresent()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "GDT-\u5f00\u5c4f\u5e7f\u544a_onADPresent......onADPresent....."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onADTick(J)V
    .locals 2

    iput-wide p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->millisUntilFinished:J

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    long-to-float p1, p1

    const/high16 p2, 0x447a0000    # 1000.0f

    div-float/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v1, p2

    const-string p1, "%d | \u8df3\u8fc7"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter$GDTSplashAd;->f:Lcom/bytedance/msdk/adapter/gdt/GdtSplashAdapter;

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->getGdtError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method
