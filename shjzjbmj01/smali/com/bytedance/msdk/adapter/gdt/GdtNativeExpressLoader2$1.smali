.class Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/nativ/express2/NativeExpressAD2$AdLoadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadSuccess(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;

    if-eqz v1, :cond_1

    new-instance v2, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    iget-object v3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    invoke-direct {v2, v3, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;Lcom/qq/e/ads/nativ/express2/NativeExpressADData2;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Ljava/util/List;)V

    return-void

    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    move-result-object p1

    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x4e21

    const-string v2, "load success, but no ad fill !"

    invoke-direct {v0, v1, v2}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->getGdtError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    move-result-object p1

    const/16 v0, 0x4e21

    const-string v1, "load success, but no ad fill !"

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    :goto_0
    return-void
.end method
