.class public Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;
    }
.end annotation


# static fields
.field private static k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/qq/e/ads/nativ/NativeExpressADView;",
            "Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Landroid/content/Context;

.field private d:Lcom/bytedance/msdk/api/TTVideoOption;

.field private e:Lcom/bytedance/msdk/api/GDTExtraOption;

.field private f:Lcom/qq/e/ads/nativ/NativeExpressAD;

.field private g:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

.field private h:I

.field private i:Z

.field private final j:Lcom/qq/e/ads/nativ/NativeExpressAD$NativeExpressADListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->k:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$1;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->j:Lcom/qq/e/ads/nativ/NativeExpressAD$NativeExpressADListener;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;Lcom/qq/e/comm/pi/AdData$VideoPlayer;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a(Lcom/qq/e/comm/pi/AdData$VideoPlayer;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/qq/e/comm/pi/AdData$VideoPlayer;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/qq/e/comm/pi/AdData$VideoPlayer;->getVideoState()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "duration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/qq/e/comm/pi/AdData$VideoPlayer;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "position:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/qq/e/comm/pi/AdData$VideoPlayer;->getCurrentPosition()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "}"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->k:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->i:Z

    return p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->g:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    return-object p0
.end method

.method private b()Lcom/qq/e/ads/nativ/ADSize;
    .locals 3

    new-instance v0, Lcom/qq/e/ads/nativ/ADSize;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v2, v1}, Lcom/qq/e/ads/nativ/ADSize;-><init>(II)V

    iget v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->h:I

    if-lez v2, :cond_0

    new-instance v0, Lcom/qq/e/ads/nativ/ADSize;

    invoke-direct {v0, v2, v1}, Lcom/qq/e/ads/nativ/ADSize;-><init>(II)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method a(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;
    .locals 0

    iput p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->b:I

    return-object p0
.end method

.method a(Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->d:Lcom/bytedance/msdk/api/TTVideoOption;

    return-object p0
.end method

.method a(Ljava/lang/String;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a:Ljava/lang/String;

    return-object p0
.end method

.method b(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;
    .locals 0

    return-object p0
.end method

.method c(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;
    .locals 0

    iput p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->h:I

    return-object p0
.end method

.method public loadAd(Landroid/content/Context;ZLcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)V
    .locals 2

    if-eqz p1, :cond_3

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->c:Landroid/content/Context;

    iput-object p3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->g:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iput-boolean p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->i:Z

    new-instance p1, Lcom/qq/e/ads/nativ/NativeExpressAD;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->c:Landroid/content/Context;

    invoke-direct {p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->b()Lcom/qq/e/ads/nativ/ADSize;

    move-result-object p3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->j:Lcom/qq/e/ads/nativ/NativeExpressAD$NativeExpressADListener;

    invoke-direct {p1, p2, p3, v0, v1}, Lcom/qq/e/ads/nativ/NativeExpressAD;-><init>(Landroid/content/Context;Lcom/qq/e/ads/nativ/ADSize;Ljava/lang/String;Lcom/qq/e/ads/nativ/NativeExpressAD$NativeExpressADListener;)V

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->f:Lcom/qq/e/ads/nativ/NativeExpressAD;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->d:Lcom/bytedance/msdk/api/TTVideoOption;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->getGDTExtraOption()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->e:Lcom/bytedance/msdk/api/GDTExtraOption;

    :cond_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->e:Lcom/bytedance/msdk/api/GDTExtraOption;

    if-eqz p1, :cond_2

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->f:Lcom/qq/e/ads/nativ/NativeExpressAD;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTMinVideoDuration()I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/NativeExpressAD;->setMinVideoDuration(I)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->f:Lcom/qq/e/ads/nativ/NativeExpressAD;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->e:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTMaxVideoDuration()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/qq/e/ads/nativ/NativeExpressAD;->setMaxVideoDuration(I)V

    :cond_2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->e:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->getVideoOption(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object p1

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->f:Lcom/qq/e/ads/nativ/NativeExpressAD;

    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/NativeExpressAD;->setVideoOption(Lcom/qq/e/ads/cfg/VideoOption;)V

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->f:Lcom/qq/e/ads/nativ/NativeExpressAD;

    invoke-virtual {p1}, Lcom/qq/e/ads/cfg/VideoOption;->getAutoPlayPolicy()I

    move-result p1

    iget-object p3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->c:Landroid/content/Context;

    invoke-static {p1, p3}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->getVideoPlayPolicy(ILandroid/content/Context;)I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/NativeExpressAD;->setVideoPlayPolicy(I)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->f:Lcom/qq/e/ads/nativ/NativeExpressAD;

    iget p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->b:I

    invoke-virtual {p1, p2}, Lcom/qq/e/ads/nativ/NativeExpressAD;->loadAD(I)V

    :cond_3
    :goto_0
    return-void
.end method
