.class public Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Lcom/bytedance/msdk/api/TTVideoOption;

.field private d:Lcom/bytedance/msdk/api/GDTExtraOption;

.field private e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

.field private f:Z

.field private g:I

.field private final h:Lcom/qq/e/ads/nativ/express2/NativeExpressAD2$AdLoadListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$1;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->h:Lcom/qq/e/ads/nativ/express2/NativeExpressAD2$AdLoadListener;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->f:Z

    return p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;)Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    return-object p0
.end method


# virtual methods
.method a(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;
    .locals 0

    iput p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->b:I

    return-object p0
.end method

.method a(Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->c:Lcom/bytedance/msdk/api/TTVideoOption;

    return-object p0
.end method

.method a(Ljava/lang/String;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->a:Ljava/lang/String;

    return-object p0
.end method

.method b(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;
    .locals 0

    return-object p0
.end method

.method c(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;
    .locals 0

    iput p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->g:I

    return-object p0
.end method

.method public loadAd(Landroid/content/Context;ZLcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)V
    .locals 1

    if-eqz p1, :cond_9

    if-nez p3, :cond_0

    goto/16 :goto_6

    :cond_0
    iput-object p3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->e:Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;

    iput-boolean p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->f:Z

    new-instance p2, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;

    iget-object p3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->h:Lcom/qq/e/ads/nativ/express2/NativeExpressAD2$AdLoadListener;

    invoke-direct {p2, p1, p3, v0}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/nativ/express2/NativeExpressAD2$AdLoadListener;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->c:Lcom/bytedance/msdk/api/TTVideoOption;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->getGDTExtraOption()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->d:Lcom/bytedance/msdk/api/GDTExtraOption;

    :cond_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->d:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->getVideoOption2(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/qq/e/ads/nativ/express2/VideoOption2;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;->setVideoOption2(Lcom/qq/e/ads/nativ/express2/VideoOption2;)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->d:Lcom/bytedance/msdk/api/GDTExtraOption;

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getBrowserType()I

    move-result p1

    const/4 p3, 0x1

    if-nez p1, :cond_2

    sget-object p1, Lcom/qq/e/ads/cfg/BrowserType;->Default:Lcom/qq/e/ads/cfg/BrowserType;

    :goto_0
    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;->setBrowserType(Lcom/qq/e/ads/cfg/BrowserType;)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->d:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getBrowserType()I

    move-result p1

    if-ne p1, p3, :cond_3

    sget-object p1, Lcom/qq/e/ads/cfg/BrowserType;->Inner:Lcom/qq/e/ads/cfg/BrowserType;

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->d:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getBrowserType()I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    sget-object p1, Lcom/qq/e/ads/cfg/BrowserType;->Sys:Lcom/qq/e/ads/cfg/BrowserType;

    goto :goto_0

    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->d:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getDownAPPConfirmPolicy()I

    move-result p1

    if-nez p1, :cond_5

    sget-object p1, Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;->Default:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    :goto_2
    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;->setDownAPPConfirmPolicy(Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;)V

    goto :goto_3

    :cond_5
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->d:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getDownAPPConfirmPolicy()I

    move-result p1

    if-ne p1, p3, :cond_6

    sget-object p1, Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;->NOConfirm:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    goto :goto_2

    :cond_6
    :goto_3
    iget p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->g:I

    const/4 p3, -0x2

    if-lez p1, :cond_7

    :goto_4
    invoke-virtual {p2, p1, p3}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;->setAdSize(II)V

    goto :goto_5

    :cond_7
    const/4 p1, -0x1

    goto :goto_4

    :cond_8
    :goto_5
    iget p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2;->b:I

    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/express2/NativeExpressAD2;->loadAd(I)V

    :cond_9
    :goto_6
    return-void
.end method
