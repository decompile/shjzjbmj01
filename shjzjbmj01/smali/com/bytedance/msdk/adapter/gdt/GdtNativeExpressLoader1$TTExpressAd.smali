.class Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""

# interfaces
.implements Lcom/bytedance/msdk/adapter/listener/ITTAdapterNativeExpressAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TTExpressAd"
.end annotation


# instance fields
.field a:Lcom/qq/e/ads/nativ/NativeExpressADView;

.field b:Lcom/bytedance/msdk/api/TTDislikeCallback;

.field private final c:Lcom/qq/e/ads/nativ/NativeExpressMediaListener;

.field final synthetic d:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 5

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->c:Lcom/qq/e/ads/nativ/NativeExpressMediaListener;

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressADView;

    invoke-virtual {p2}, Lcom/qq/e/ads/nativ/NativeExpressADView;->getBoundData()Lcom/qq/e/comm/pi/AdData;

    move-result-object v0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/AdData;->getAdPatternType()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x3

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    invoke-virtual {p2}, Lcom/qq/e/ads/nativ/NativeExpressADView;->preloadVideo()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->c:Lcom/qq/e/ads/nativ/NativeExpressMediaListener;

    invoke-virtual {p2, v1}, Lcom/qq/e/ads/nativ/NativeExpressADView;->setMediaListener(Lcom/qq/e/ads/nativ/NativeExpressMediaListener;)V

    const/4 p2, 0x5

    invoke-virtual {p0, p2}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_1

    :cond_0
    invoke-interface {v0}, Lcom/qq/e/comm/pi/AdData;->getAdPatternType()I

    move-result p2

    const/4 v1, 0x4

    if-eq p2, v1, :cond_2

    invoke-interface {v0}, Lcom/qq/e/comm/pi/AdData;->getAdPatternType()I

    move-result p2

    if-ne p2, v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lcom/qq/e/comm/pi/AdData;->getAdPatternType()I

    move-result p2

    if-ne p2, v3, :cond_2

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual {p0, v3}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    :goto_1
    invoke-virtual {p0, v2}, Lcom/bytedance/msdk/base/TTBaseAd;->setExpressAd(Z)V

    invoke-interface {v0}, Lcom/qq/e/comm/pi/AdData;->getTitle()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/bytedance/msdk/base/TTBaseAd;->setTitle(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/qq/e/comm/pi/AdData;->getDesc()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdDescription(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {v0}, Lcom/qq/e/comm/pi/AdData;->getECPM()I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_3

    invoke-interface {v0}, Lcom/qq/e/comm/pi/AdData;->getECPM()I

    move-result p1

    int-to-double p1, p1

    goto :goto_2

    :cond_3
    const-wide/16 p1, 0x0

    :goto_2
    invoke-virtual {p0, p1, p2}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    const-string p1, ""

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "GDT \u6a21\u677fNative \u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "TTMediationSDK_ECMP"

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method


# virtual methods
.method public closeAd()V
    .locals 3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->b:Lcom/bytedance/msdk/api/TTDislikeCallback;

    if-eqz v0, :cond_0

    const/4 v1, -0x1

    const-string v2, "ad close !"

    invoke-interface {v0, v1, v2}, Lcom/bytedance/msdk/api/TTDislikeCallback;->onSelected(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getAdType()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public getAdView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressADView;

    return-object v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressADView;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasDislike()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onAdClick()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;->onAdClick()V

    :cond_0
    return-void
.end method

.method public onAdShow()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;->onAdShow()V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressADView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/qq/e/ads/nativ/NativeExpressADView;->destroy()V

    :cond_0
    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a()Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressADView;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressADView;

    :cond_1
    iput-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->b:Lcom/bytedance/msdk/api/TTDislikeCallback;

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onDestroy()V

    return-void
.end method

.method public onRenderFail(Landroid/view/View;Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    instance-of v1, v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;->onRenderFail(Landroid/view/View;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public onRenderSuccess(Landroid/view/View;FF)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    instance-of v1, v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;->onRenderSuccess(Landroid/view/View;FF)V

    :cond_0
    return-void
.end method

.method public render()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->a:Lcom/qq/e/ads/nativ/NativeExpressADView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/qq/e/ads/nativ/NativeExpressADView;->render()V

    :cond_0
    return-void
.end method

.method public setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/msdk/api/TTDislikeCallback;)V
    .locals 0

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->b:Lcom/bytedance/msdk/api/TTDislikeCallback;

    return-void
.end method
