.class Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/nativ/NativeADEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onADClicked()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->l(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->m(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;->onAdClick()V

    :cond_0
    const-string v0, "GdtNativeLoader"

    const-string v1, "Native GDT --- onADClicked\u3002\u3002\u3002\u3002"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onADError(Lcom/qq/e/comm/util/AdError;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GDT --- onADError error code :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "  error msg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TTMediationSDK"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onADExposed()V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->j(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$2;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->k(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;->onAdShow()V

    :cond_0
    const-string v0, "GdtNativeLoader"

    const-string v1, "Native GDT --- onADExposed\u3002\u3002\u3002\u3002"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onADStatusChanged()V
    .locals 0

    return-void
.end method
