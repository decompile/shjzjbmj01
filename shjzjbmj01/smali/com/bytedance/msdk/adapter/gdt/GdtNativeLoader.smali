.class public Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;
    }
.end annotation


# instance fields
.field private a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

.field private b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/content/Context;

.field private h:Lcom/bytedance/msdk/api/TTVideoOption;

.field private i:Lcom/bytedance/msdk/api/GDTExtraOption;

.field private j:Z

.field private k:Lcom/qq/e/ads/cfg/VideoOption;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->j:Z

    return p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->g:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;)Lcom/qq/e/ads/cfg/VideoOption;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->k:Lcom/qq/e/ads/cfg/VideoOption;

    return-object p0
.end method


# virtual methods
.method a(I)Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;
    .locals 0

    iput p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->d:I

    return-object p0
.end method

.method a(Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->h:Lcom/bytedance/msdk/api/TTVideoOption;

    return-object p0
.end method

.method a(Ljava/lang/String;)Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->c:Ljava/lang/String;

    return-object p0
.end method

.method public loadAd(Landroid/content/Context;ZLcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)V
    .locals 2

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->g:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->j:Z

    new-instance p2, Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->c:Ljava/lang/String;

    new-instance v1, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$1;

    invoke-direct {v1, p0, p3}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;)V

    invoke-direct {p2, p1, v0, v1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/nativ/NativeADUnifiedListener;)V

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->h:Lcom/bytedance/msdk/api/TTVideoOption;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTVideoOption;->getGDTExtraOption()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->i:Lcom/bytedance/msdk/api/GDTExtraOption;

    :cond_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->i:Lcom/bytedance/msdk/api/GDTExtraOption;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTMaxVideoDuration()I

    move-result p1

    iput p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->f:I

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->i:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTMinVideoDuration()I

    move-result p1

    iput p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->e:I

    :cond_1
    iget p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->e:I

    if-lez p1, :cond_2

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setMinVideoDuration(I)V

    :cond_2
    iget p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->f:I

    if-lez p1, :cond_3

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    invoke-virtual {p2, p1}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setMaxVideoDuration(I)V

    :cond_3
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->i:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->getVideoOption(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->k:Lcom/qq/e/ads/cfg/VideoOption;

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    iget-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->k:Lcom/qq/e/ads/cfg/VideoOption;

    invoke-virtual {p2}, Lcom/qq/e/ads/cfg/VideoOption;->getAutoPlayPolicy()I

    move-result p2

    iget-object p3, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->g:Landroid/content/Context;

    invoke-static {p2, p3}, Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;->getVideoPlayPolicy(ILandroid/content/Context;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setVideoPlayPolicy(I)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setVideoADContainerRender(I)V

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->i:Lcom/bytedance/msdk/api/GDTExtraOption;

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getBrowserType()I

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    sget-object p3, Lcom/qq/e/ads/cfg/BrowserType;->Default:Lcom/qq/e/ads/cfg/BrowserType;

    :goto_0
    invoke-virtual {p1, p3}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setBrowserType(Lcom/qq/e/ads/cfg/BrowserType;)V

    goto :goto_1

    :cond_4
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->i:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getBrowserType()I

    move-result p1

    if-ne p1, p2, :cond_5

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    sget-object p3, Lcom/qq/e/ads/cfg/BrowserType;->Inner:Lcom/qq/e/ads/cfg/BrowserType;

    goto :goto_0

    :cond_5
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->i:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getBrowserType()I

    move-result p1

    const/4 p3, 0x2

    if-ne p1, p3, :cond_6

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    sget-object p3, Lcom/qq/e/ads/cfg/BrowserType;->Sys:Lcom/qq/e/ads/cfg/BrowserType;

    goto :goto_0

    :cond_6
    :goto_1
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->i:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getDownAPPConfirmPolicy()I

    move-result p1

    if-nez p1, :cond_7

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    sget-object p2, Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;->Default:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    :goto_2
    invoke-virtual {p1, p2}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->setDownAPPConfirmPolicy(Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;)V

    goto :goto_3

    :cond_7
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->i:Lcom/bytedance/msdk/api/GDTExtraOption;

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/GDTExtraOption;->getDownAPPConfirmPolicy()I

    move-result p1

    if-ne p1, p2, :cond_8

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    sget-object p2, Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;->NOConfirm:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    goto :goto_2

    :cond_8
    :goto_3
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz p1, :cond_9

    invoke-interface {p1}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->destroy()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    :cond_9
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b:Lcom/qq/e/ads/nativ/NativeUnifiedAD;

    if-eqz p1, :cond_a

    iget p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->d:I

    invoke-virtual {p1, p2}, Lcom/qq/e/ads/nativ/NativeUnifiedAD;->loadData(I)V

    :cond_a
    return-void
.end method
