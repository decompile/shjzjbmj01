.class Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/nativ/NativeExpressMediaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoCached(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 1

    const-string p1, "GdtNativeExpressLoader"

    const-string v0, "onVideoCached"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoComplete(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->f(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->g(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoCompleted()V

    :cond_0
    return-void
.end method

.method public onVideoError(Lcom/qq/e/ads/nativ/NativeExpressADView;Lcom/qq/e/comm/util/AdError;)V
    .locals 1

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->h(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    invoke-virtual {p2}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v0

    invoke-virtual {p2}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/bytedance/msdk/api/AdErrorUtil;->getGdtError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoError(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void
.end method

.method public onVideoInit(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onVideoInit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;

    invoke-virtual {p1}, Lcom/qq/e/ads/nativ/NativeExpressADView;->getBoundData()Lcom/qq/e/comm/pi/AdData;

    move-result-object p1

    const-class v2, Lcom/qq/e/comm/pi/AdData$VideoPlayer;

    invoke-interface {p1, v2}, Lcom/qq/e/comm/pi/AdData;->getProperty(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/qq/e/comm/pi/AdData$VideoPlayer;

    invoke-static {v1, p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;->a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1;Lcom/qq/e/comm/pi/AdData$VideoPlayer;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "GdtNativeExpressLoader"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoLoading(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 1

    const-string p1, "GdtNativeExpressLoader"

    const-string v0, "onVideoLoading"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoPageClose(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    return-void
.end method

.method public onVideoPageOpen(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    return-void
.end method

.method public onVideoPause(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->d(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoPause()V

    :cond_0
    return-void
.end method

.method public onVideoReady(Lcom/qq/e/ads/nativ/NativeExpressADView;J)V
    .locals 0

    return-void
.end method

.method public onVideoStart(Lcom/qq/e/ads/nativ/NativeExpressADView;)V
    .locals 0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;->c(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader1$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoStart()V

    :cond_0
    return-void
.end method
