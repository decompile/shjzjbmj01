.class Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;
.super Lcom/bytedance/msdk/base/TTBaseAd;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GdtNativeAd"
.end annotation


# instance fields
.field a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

.field b:Lcom/qq/e/ads/nativ/NativeADMediaListener;

.field final synthetic c:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;Lcom/qq/e/ads/nativ/NativeUnifiedADData;)V
    .locals 5

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->c:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;

    invoke-direct {p0}, Lcom/bytedance/msdk/base/TTBaseAd;-><init>()V

    new-instance v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;

    invoke-direct {v0, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$3;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)V

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->b:Lcom/qq/e/ads/nativ/NativeADMediaListener;

    iput-object p2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setTitle(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getDesc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdDescription(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getCTAText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setActionText(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getIconUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setIconUrl(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getImgUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageUrl(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getPictureWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageWidth(I)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getPictureHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageHeight(I)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getImgList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setImages(Ljava/util/List;)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAppScore()I

    move-result v0

    int-to-double v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setRating(D)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setAdType(I)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setSource(Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->isAppAd()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setIsAppDownload(Z)V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getECPM()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getECPM()I

    move-result v1

    int-to-double v1, v1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    :goto_0
    invoke-virtual {p0, v1, v2}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GDT \u539f\u751fNative \u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TTMediationSDK_ECMP"

    invoke-static {v2, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAdPatternType()I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x3

    const/4 v4, 0x4

    if-ne v1, v2, :cond_2

    invoke-virtual {p0, v0}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    new-instance v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$1;

    invoke-direct {v0, p0, p1}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;)V

    invoke-interface {p2, v0}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->preloadVideo(Lcom/qq/e/ads/nativ/VideoPreloadListener;)V

    goto :goto_2

    :cond_2
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAdPatternType()I

    move-result p1

    if-eq p1, v4, :cond_4

    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAdPatternType()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    goto :goto_1

    :cond_3
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getAdPatternType()I

    move-result p1

    if-ne p1, v3, :cond_5

    invoke-virtual {p0, v4}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_2

    :cond_4
    :goto_1
    invoke-virtual {p0, v3}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    :cond_5
    :goto_2
    invoke-interface {p2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->isAppAd()Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-virtual {p0, v4}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    goto :goto_3

    :cond_6
    invoke-virtual {p0, v3}, Lcom/bytedance/msdk/base/TTBaseAd;->setInteractionType(I)V

    :goto_3
    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic k(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic m(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTNativeAdListener:Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;

    return-object p0
.end method

.method static synthetic n(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic o(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method

.method static synthetic p(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;
    .locals 0

    iget-object p0, p0, Lcom/bytedance/msdk/base/TTBaseAd;->mTTVideoListener:Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    return-object p0
.end method


# virtual methods
.method public getAdType()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public hasDestroyed()Z
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    :cond_0
    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onPause()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->pauseVideo()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->onResume()V

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->resume()V

    :cond_0
    return-void
.end method

.method public registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V

    return-void
.end method

.method public registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
    .locals 15
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-super/range {p0 .. p4}, Lcom/bytedance/msdk/base/TTBaseAd;->registerViewForInteraction(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V

    iget-object v3, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    if-eqz v3, :cond_8

    instance-of v3, v1, Lcom/bytedance/msdk/api/format/TTNativeAdView;

    if-eqz v3, :cond_8

    check-cast v1, Lcom/bytedance/msdk/api/format/TTNativeAdView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v4, v4, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    const/4 v5, -0x1

    if-eqz v4, :cond_3

    invoke-virtual {v1, v3}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    :goto_0
    invoke-virtual {v4}, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;->getChildCount()I

    move-result v6

    if-ge v3, v6, :cond_2

    invoke-virtual {v4, v3}, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_1

    sget v7, Lcom/bytedance/msdk/adapter/gdt/R$id;->tt_mediation_gdt_developer_view_tag_key:I

    invoke-virtual {v6, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_0

    instance-of v8, v7, Ljava/lang/String;

    if-eqz v8, :cond_0

    check-cast v7, Ljava/lang/String;

    const-string v8, "tt_gdt_developer_view"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v4, v6}, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    move-object v11, v4

    goto :goto_4

    :cond_3
    new-instance v4, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;

    iget-object v6, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->c:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;

    invoke-static {v6}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;-><init>(Landroid/content/Context;)V

    sget v6, Lcom/bytedance/msdk/adapter/gdt/R$id;->tt_mediation_gdt_developer_view_root_tag_key:I

    const-string v7, "tt_gdt_developer_view_root"

    invoke-virtual {v4, v6, v7}, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;->setTag(ILjava/lang/Object;)V

    :cond_4
    :goto_3
    invoke-virtual {v1}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->getChildCount()I

    move-result v6

    if-lez v6, :cond_5

    invoke-virtual {v1, v3}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    sget v7, Lcom/bytedance/msdk/adapter/gdt/R$id;->tt_mediation_gdt_developer_view_tag_key:I

    const-string v8, "tt_gdt_developer_view"

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v1, v6}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->indexOfChild(Landroid/view/View;)I

    move-result v7

    invoke-virtual {v1, v6}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->removeViewInLayout(Landroid/view/View;)V

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    invoke-virtual {v4, v6, v7, v8}, Lcom/qq/e/ads/nativ/widget/NativeAdContainer;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v1}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->removeAllViews()V

    invoke-virtual {v1, v4, v5, v5}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->addView(Landroid/view/View;II)V

    goto :goto_2

    :goto_4
    iget v3, v2, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->mediaViewId:I

    invoke-virtual {v1, v3}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/bytedance/msdk/api/format/TTMediaView;

    iget-object v9, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    iget-object v4, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->c:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;

    invoke-static {v4}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;)Landroid/content/Context;

    move-result-object v10

    const/4 v12, 0x0

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    invoke-interface/range {v9 .. v14}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->bindAdToView(Landroid/content/Context;Lcom/qq/e/ads/nativ/widget/NativeAdContainer;Landroid/widget/FrameLayout$LayoutParams;Ljava/util/List;Ljava/util/List;)V

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getImageMode()I

    move-result v4

    const/4 v6, 0x5

    if-ne v4, v6, :cond_6

    new-instance v4, Lcom/qq/e/ads/nativ/MediaView;

    iget-object v6, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->c:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;

    invoke-static {v6}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/qq/e/ads/nativ/MediaView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/bytedance/msdk/api/format/TTMediaView;->removeAllViews()V

    invoke-virtual {v3, v4, v5, v5}, Lcom/bytedance/msdk/api/format/TTMediaView;->addView(Landroid/view/View;II)V

    iget-object v3, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    iget-object v5, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->c:Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;

    invoke-static {v5}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;->c(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader;)Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object v5

    iget-object v6, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->b:Lcom/qq/e/ads/nativ/NativeADMediaListener;

    invoke-interface {v3, v4, v5, v6}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->bindMediaView(Lcom/qq/e/ads/nativ/MediaView;Lcom/qq/e/ads/cfg/VideoOption;Lcom/qq/e/ads/nativ/NativeADMediaListener;)V

    :cond_6
    iget-object v3, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    invoke-interface {v3}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->getCTAText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    iget v2, v2, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->callToActionId:I

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/format/TTNativeAdView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    invoke-interface {v1, v2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->bindCTAViews(Ljava/util/List;)V

    :cond_7
    iget-object v1, v0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;->a:Lcom/qq/e/ads/nativ/NativeUnifiedADData;

    new-instance v2, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$2;

    invoke-direct {v2, p0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd$2;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeLoader$GdtNativeAd;)V

    invoke-interface {v1, v2}, Lcom/qq/e/ads/nativ/NativeUnifiedADData;->setNativeAdEventListener(Lcom/qq/e/ads/nativ/NativeADEventListener;)V

    :cond_8
    return-void
.end method

.method public unregisterView()V
    .locals 0

    return-void
.end method
