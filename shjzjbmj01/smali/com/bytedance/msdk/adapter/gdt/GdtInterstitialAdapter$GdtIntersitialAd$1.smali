.class Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/interstitial2/UnifiedInterstitialADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onADClicked()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->g(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;->onInterstitialAdClick()V

    :cond_0
    return-void
.end method

.method public onADClosed()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->b(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;->onInterstitialClosed()V

    :cond_0
    return-void
.end method

.method public onADExposure()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->f(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;->onInterstitialShow()V

    :cond_0
    return-void
.end method

.method public onADLeftApplication()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;->onAdLeftApplication()V

    :cond_0
    return-void
.end method

.method public onADOpened()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterInterstitialListener;->onAdOpened()V

    :cond_0
    return-void
.end method

.method public onADReceive()V
    .locals 3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;Z)Z

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->c(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->getECPM()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->c(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->getECPM()I

    move-result v1

    int-to-double v1, v1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "GDT \u63d2\u5c4f \u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK_ECMP"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->c(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;)Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;

    move-result-object v0

    invoke-virtual {v0}, Lcom/qq/e/ads/interstitial2/UnifiedInterstitialAD;->getAdPatternType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    const/4 v1, 0x5

    :goto_1
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/base/TTBaseAd;->setImageMode(I)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    const/4 v1, 0x3

    goto :goto_1

    :goto_2
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    iget-object v1, v0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-virtual {v1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void
.end method

.method public onNoAD(Lcom/qq/e/comm/util/AdError;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->a(Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;Z)Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->getGdtError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter$GdtIntersitialAd;->d:Lcom/bytedance/msdk/adapter/gdt/GdtInterstitialAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    :goto_0
    return-void
.end method

.method public onVideoCached()V
    .locals 0

    return-void
.end method
