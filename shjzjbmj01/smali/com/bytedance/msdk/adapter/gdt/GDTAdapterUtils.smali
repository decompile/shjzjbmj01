.class public Lcom/bytedance/msdk/adapter/gdt/GDTAdapterUtils;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final CPM_DEFLAUT_VALUE:D


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dp2px(Landroid/content/Context;F)I
    .locals 0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    iget p0, p0, Landroid/util/DisplayMetrics;->density:F

    mul-float p1, p1, p0

    const/high16 p0, 0x3f000000    # 0.5f

    add-float/2addr p1, p0

    float-to-int p0, p1

    return p0
.end method

.method public static getVideoOption(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/qq/e/ads/cfg/VideoOption;
    .locals 2

    new-instance v0, Lcom/qq/e/ads/cfg/VideoOption$Builder;

    invoke-direct {v0}, Lcom/qq/e/ads/cfg/VideoOption$Builder;-><init>()V

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTAutoPlayPolicy()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayPolicy(I)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->isGDTAutoPlayMuted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setAutoPlayMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->isGDTDetailPageMuted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setDetailPageMuted(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->isGDTEnableDetailPage()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setEnableDetailPage(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->isGDTEnableUserControl()Z

    move-result p0

    invoke-virtual {v0, p0}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->setEnableUserControl(Z)Lcom/qq/e/ads/cfg/VideoOption$Builder;

    :cond_0
    invoke-virtual {v0}, Lcom/qq/e/ads/cfg/VideoOption$Builder;->build()Lcom/qq/e/ads/cfg/VideoOption;

    move-result-object p0

    return-object p0
.end method

.method public static getVideoOption2(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/qq/e/ads/nativ/express2/VideoOption2;
    .locals 3

    new-instance v0, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    invoke-direct {v0}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;-><init>()V

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTAutoPlayPolicy()I

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->WIFI:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setAutoPlayPolicy(Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTAutoPlayPolicy()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->ALWAYS:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTAutoPlayPolicy()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    sget-object v1, Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;->NEVER:Lcom/qq/e/ads/nativ/express2/VideoOption2$AutoPlayPolicy;

    goto :goto_0

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->isGDTAutoPlayMuted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setAutoPlayMuted(Z)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->isGDTDetailPageMuted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setDetailPageMuted(Z)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTMaxVideoDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setMaxVideoDuration(I)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/GDTExtraOption;->getGDTMinVideoDuration()I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->setMinVideoDuration(I)Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;

    :cond_3
    invoke-virtual {v0}, Lcom/qq/e/ads/nativ/express2/VideoOption2$Builder;->build()Lcom/qq/e/ads/nativ/express2/VideoOption2;

    move-result-object p0

    return-object p0
.end method

.method public static getVideoPlayPolicy(ILandroid/content/Context;)I
    .locals 2

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x2

    if-nez p0, :cond_2

    const-string p0, "connectivity"

    invoke-virtual {p1, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/ConnectivityManager;

    invoke-virtual {p0, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_2
    if-ne p0, v1, :cond_3

    return v1

    :cond_3
    const/4 p0, 0x0

    return p0
.end method
