.class Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/nativ/express2/MediaEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoCache()V
    .locals 2

    const-string v0, "GdtNativeExpressLoader"

    const-string v1, "onVideoCached"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoComplete()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->a(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->c(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoCompleted()V

    :cond_0
    return-void
.end method

.method public onVideoError()V
    .locals 4

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->d(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->e(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/api/AdError;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoError(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void
.end method

.method public onVideoPause()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->f(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->g(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoPause()V

    :cond_0
    return-void
.end method

.method public onVideoResume()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->h(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->i(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoResume()V

    :cond_0
    return-void
.end method

.method public onVideoStart()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->j(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;->b(Lcom/bytedance/msdk/adapter/gdt/GdtNativeExpressLoader2$TTExpressAd;)Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;->onVideoStart()V

    :cond_0
    return-void
.end method
