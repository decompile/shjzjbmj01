.class Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;


# direct methods
.method constructor <init>(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)V
    .locals 0

    iput-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onADClick()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->a(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->f(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onRewardClick()V

    :cond_0
    return-void
.end method

.method public onADClose()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->c(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->f(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onRewardedAdClosed()V

    :cond_0
    return-void
.end method

.method public onADExpose()V
    .locals 0

    return-void
.end method

.method public onADLoad()V
    .locals 6

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->a(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;Z)Z

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-virtual {v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->isClientBidding()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->getECPM()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->getECPM()I

    move-result v1

    int-to-double v1, v1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/msdk/base/TTBaseAd;->setCpm(D)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "GDT Reward \u8fd4\u56de\u7684 cpm\u4ef7\u683c\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK_ECMP"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v2}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    move-result-object v2

    invoke-virtual {v2}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->getExpireTimestamp()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    sub-long/2addr v2, v4

    cmp-long v4, v0, v2

    if-gez v4, :cond_2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object v1, v0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-virtual {v1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdLoaded(Lcom/bytedance/msdk/base/TTBaseAd;)V

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object v1, v1, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-virtual {v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdapterRit()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object v2, v2, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-virtual {v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->getAdSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "eCPM = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->getECPM()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " , eCPMLevel = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/qq/e/ads/rewardvideo/RewardVideoAD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->getECPMLevel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GdtRewardVideoAdapter"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    :goto_1
    return-void
.end method

.method public onADShow()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->e(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->f(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onRewardedAdShow()V

    :cond_0
    return-void
.end method

.method public onError(Lcom/qq/e/comm/util/AdError;)V
    .locals 2

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->a(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;Z)Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object v0, v0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorCode()I

    move-result v1

    invoke-virtual {p1}, Lcom/qq/e/comm/util/AdError;->getErrorMsg()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/bytedance/msdk/api/AdErrorUtil;->getGdtError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object p1, p1, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdFailed(Lcom/bytedance/msdk/api/AdError;)V

    :goto_0
    return-void
.end method

.method public onReward(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->g(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->f(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1$1;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1$1;-><init>(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;Ljava/util/Map;)V

    invoke-interface {v0, v1}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onRewardVerify(Lcom/bytedance/msdk/api/reward/RewardItem;)V

    :cond_0
    return-void
.end method

.method public onVideoCached()V
    .locals 3

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    iget-object v1, v0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->d:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/bytedance/msdk/adapter/TTAbsAdLoaderAdapter;->notifyAdVideoCache(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdError;)V

    return-void
.end method

.method public onVideoComplete()V
    .locals 1

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->b(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    move-result-object v0

    instance-of v0, v0, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo$1;->a:Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;->f(Lcom/bytedance/msdk/adapter/gdt/GdtRewardVideoAdapter$GdtRewardVideo;)Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/bytedance/msdk/adapter/listener/ITTAdapterRewardedAdListener;->onVideoComplete()V

    :cond_0
    return-void
.end method
