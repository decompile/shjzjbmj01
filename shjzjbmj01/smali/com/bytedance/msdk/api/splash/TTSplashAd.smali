.class public Lcom/bytedance/msdk/api/splash/TTSplashAd;
.super Lcom/bytedance/msdk/api/base/TTLoadBase;
.source "TTSplashAd.java"


# instance fields
.field private a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3
    invoke-direct {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;-><init>()V

    .line 4
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    invoke-direct {v0, p1, p2, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;-><init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;-><init>()V

    .line 2
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    invoke-direct {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/splash/TTSplashAd;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->m()V

    :cond_0
    return-void
.end method

.method public getAdNetworkPlatformId()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->n()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x2

    return v0
.end method

.method public getAdNetworkRitId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "-2"

    return-object v0
.end method

.method public getPreEcpm()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "-2"

    return-object v0
.end method

.method public loadAd(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/TTNetworkRequestInfo;Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;I)V
    .locals 7

    if-eqz p2, :cond_1

    .line 2
    invoke-virtual {p2}, Lcom/bytedance/msdk/api/TTNetworkRequestInfo;->getAdNetworkFlatFromId()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/TTNetworkRequestInfo;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3
    invoke-virtual {p2}, Lcom/bytedance/msdk/api/TTNetworkRequestInfo;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->getTTPangleAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    .line 5
    new-instance p1, Lcom/bytedance/msdk/api/AdError;

    const p2, 0x9c5c

    invoke-static {p2}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p1, p2, p4}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {p3, p1}, Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;->onSplashAdLoadFail(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void

    .line 10
    :cond_1
    iget-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    if-eqz v0, :cond_6

    .line 12
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p3, :cond_2

    .line 14
    new-instance p1, Lcom/bytedance/msdk/api/AdError;

    const p2, 0x9c5f

    invoke-static {p2}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p1, p2, p4}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {p3, p1}, Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;->onSplashAdLoadFail(Lcom/bytedance/msdk/api/AdError;)V

    return-void

    .line 21
    :cond_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->s()Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz p3, :cond_3

    .line 23
    new-instance p1, Lcom/bytedance/msdk/api/AdError;

    const p2, 0x9c62

    invoke-static {p2}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p1, p2, p4}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {p3, p1}, Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;->onSplashAdLoadFail(Lcom/bytedance/msdk/api/AdError;)V

    :cond_3
    return-void

    .line 29
    :cond_4
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 31
    new-instance v0, Lcom/bytedance/msdk/api/splash/TTSplashAd$1;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/bytedance/msdk/api/splash/TTSplashAd$1;-><init>(Lcom/bytedance/msdk/api/splash/TTSplashAd;Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/TTNetworkRequestInfo;Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;I)V

    invoke-static {v0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->registerConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V

    goto :goto_0

    .line 38
    :cond_5
    iget-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    invoke-virtual {v0, p1, p2, p3, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/TTNetworkRequestInfo;Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;I)V

    :cond_6
    :goto_0
    return-void
.end method

.method public loadAd(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;I)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/bytedance/msdk/api/splash/TTSplashAd;->loadAd(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/TTNetworkRequestInfo;Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;I)V

    return-void
.end method

.method public setTTAdSplashListener(Lcom/bytedance/msdk/api/splash/TTSplashAdListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->a(Lcom/bytedance/msdk/api/splash/TTSplashAdListener;)V

    :cond_0
    return-void
.end method

.method public showAd(Landroid/view/ViewGroup;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/splash/TTSplashAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->a(Landroid/view/ViewGroup;)V

    :cond_0
    return-void
.end method
