.class public Lcom/bytedance/msdk/api/activity/TTDelegateActivity;
.super Landroid/app/Activity;
.source "TTDelegateActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/api/activity/TTDelegateActivity$updateDeviceInfoRunnable;
    }
.end annotation


# static fields
.field public static final INTENT_TYPE:Ljava/lang/String; = "type"

.field public static final INTENT_TYPE_READ_PHONE_STATE_CHECK:I = 0x1

.field public static final INTENT_TYPE_REQUEST_PERMISSION:I = 0x2

.field public static final PERMISSION_CONTENT_KEY:Ljava/lang/String; = "permission_content_key"

.field public static final PERMISSION_ID_KEY:Ljava/lang/String; = "permission_id_key"


# instance fields
.field private a:Landroid/content/Intent;

.field b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 142
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->b:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private a()V
    .locals 3

    .line 29
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 31
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    .line 32
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    .line 33
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "android.permission.READ_PHONE_STATE"

    .line 34
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 35
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 37
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 38
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/e;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/e;

    move-result-object v0

    new-instance v2, Lcom/bytedance/msdk/api/activity/TTDelegateActivity$3;

    invoke-direct {v2, p0}, Lcom/bytedance/msdk/api/activity/TTDelegateActivity$3;-><init>(Lcom/bytedance/msdk/api/activity/TTDelegateActivity;)V

    invoke-virtual {v0, p0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/e;->a(Landroid/app/Activity;[Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 58
    :catch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_0
    const-string v0, "TT_AD_SDK"

    const-string v1, "\u5df2\u7ecf\u6709Read phone state\u6743\u9650"

    .line 61
    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    array-length v0, p2

    if-gtz v0, :cond_0

    goto :goto_1

    .line 6
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 8
    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/e;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/e;

    move-result-object v0

    new-instance v1, Lcom/bytedance/msdk/api/activity/TTDelegateActivity$2;

    invoke-direct {v1, p0, p1}, Lcom/bytedance/msdk/api/activity/TTDelegateActivity$2;-><init>(Lcom/bytedance/msdk/api/activity/TTDelegateActivity;Ljava/lang/String;)V

    invoke-virtual {v0, p0, p2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/e;->a(Landroid/app/Activity;[Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 23
    :catch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    const-string p1, "TT_AD_SDK"

    const-string p2, "\u5df2\u7ecf\u6709\u6743\u9650"

    .line 26
    invoke-static {p1, p2}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    .line 28
    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private b()V
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->a:Landroid/content/Intent;

    const-string v1, "type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 13
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->a:Landroid/content/Intent;

    const-string v1, "permission_id_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->a:Landroid/content/Intent;

    const-string v2, "permission_content_key"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 16
    invoke-direct {p0, v0, v1}, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->a(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 17
    :cond_1
    invoke-direct {p0}, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 29
    :catch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void
.end method

.method private c()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, 0x0

    .line 3
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->alpha:F

    .line 4
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public static requestPermission(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    .line 2
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "type"

    const/4 v2, 0x2

    .line 3
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "permission_id_key"

    .line 4
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "permission_content_key"

    .line 5
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 6
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 7
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    new-instance p1, Lcom/bytedance/msdk/api/activity/TTDelegateActivity$1;

    invoke-direct {p1}, Lcom/bytedance/msdk/api/activity/TTDelegateActivity$1;-><init>()V

    invoke-static {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c;->a(Landroid/content/Context;Landroid/content/Intent;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c$a;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 2
    invoke-direct {p0}, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->c()V

    .line 3
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->a:Landroid/content/Intent;

    .line 5
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    if-nez p1, :cond_0

    .line 6
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->b(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 3
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 4
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->b(Landroid/content/Context;)V

    .line 8
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    .line 9
    iput-object p1, p0, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->a:Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 0
    .param p2    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [I
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/e;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/e;

    move-result-object p1

    invoke-virtual {p1, p0, p2, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_case1/e;->a(Landroid/app/Activity;[Ljava/lang/String;[I)V

    .line 2
    iget-object p1, p0, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->b:Ljava/util/concurrent/ExecutorService;

    new-instance p2, Lcom/bytedance/msdk/api/activity/TTDelegateActivity$updateDeviceInfoRunnable;

    invoke-direct {p2}, Lcom/bytedance/msdk/api/activity/TTDelegateActivity$updateDeviceInfoRunnable;-><init>()V

    invoke-interface {p1, p2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 3
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 2
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5
    invoke-direct {p0}, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;->b()V

    :cond_0
    return-void
.end method
