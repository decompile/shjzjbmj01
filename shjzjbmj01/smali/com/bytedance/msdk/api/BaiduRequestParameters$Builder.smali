.class public Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;
.super Ljava/lang/Object;
.source "BaiduRequestParameters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/api/BaiduRequestParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->b:Ljava/util/Map;

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->c:Z

    const/16 v0, 0x280

    .line 4
    iput v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->d:I

    const/16 v0, 0x1e0

    .line 5
    iput v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->e:I

    const/4 v0, 0x1

    .line 6
    iput v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->f:I

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->d:I

    return p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->e:I

    return p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->c:Z

    return p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->f:I

    return p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->b:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public final addExtra(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;
    .locals 3

    const-string v0, "page_title"

    .line 1
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->b:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mpt"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final build()Lcom/bytedance/msdk/api/BaiduRequestParameters;
    .locals 2

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/BaiduRequestParameters;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/msdk/api/BaiduRequestParameters;-><init>(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;Lcom/bytedance/msdk/api/BaiduRequestParameters$1;)V

    return-object v0
.end method

.method public final confirmDownloading(Z)Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    .line 1
    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->downloadAppConfirmPolicy(I)Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;

    goto :goto_0

    :cond_0
    const/4 p1, 0x3

    .line 3
    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->downloadAppConfirmPolicy(I)Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;

    :goto_0
    return-object p0
.end method

.method public final downloadAppConfirmPolicy(I)Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->f:I

    return-object p0
.end method

.method public final setHeight(I)Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->e:I

    return-object p0
.end method

.method public final setKeywords(Ljava/lang/String;)Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final setWidth(I)Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->d:I

    return-object p0
.end method
