.class public final Lcom/bytedance/msdk/api/TTMediationAdSdk;
.super Ljava/lang/Object;
.source "TTMediationAdSdk.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static configLoadSuccess()Z
    .locals 1

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->l()Z

    move-result v0

    return v0
.end method

.method public static getSdkVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "2.6.0.0"

    return-object v0
.end method

.method public static initUnityForBanner(Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a(Landroid/app/Activity;)V

    return-void
.end method

.method public static initialize(Landroid/content/Context;Lcom/bytedance/msdk/api/TTAdConfig;)V
    .locals 2

    if-nez p1, :cond_0

    const-string p0, "TTMediationSDK_SDK_Init"

    const-string p1, "TTMediationAdSdk\u521d\u59cb\u5316\u5931\u8d25\uff0cTTAdConfig\u4e0d\u80fd\u662fnull"

    .line 1
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    invoke-static {}, Lcom/bytedance/msdk/adapter/util/Logger;->openDebugMode()V

    :cond_1
    if-nez p0, :cond_2

    const-string p0, "TTMediationSDK_SDK_Init"

    const-string p1, "TTMediationAdSdk\u521d\u59cb\u5316\u5931\u8d25\uff0ccontext\u4e0d\u80fd\u662fnull"

    .line 8
    invoke-static {p0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v0, "TTMediationSDK_SDK_Init"

    const-string v1, "msdk_init............."

    .line 11
    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->a(Ljava/lang/String;)V

    .line 13
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->b(Ljava/lang/String;)V

    .line 14
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->isPangleAllowShowNotify()Z

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->d(Z)V

    .line 15
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->isPangleAllowShowPageWhenScreenLock()Z

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->e(Z)V

    .line 16
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getPangleTitleBarTheme()I

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->a(I)V

    .line 17
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getPangleDirectDownloadNetworkType()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->a([I)V

    .line 18
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getPangleNeedClearTaskReset()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->a([Ljava/lang/String;)V

    .line 19
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->isPangleUseTextureView()Z

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->g(Z)V

    .line 20
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->isPanglePaid()Z

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->f(Z)V

    .line 21
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getPublisherDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->e(Ljava/lang/String;)V

    .line 22
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->isOpenAdnTest()Z

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->b(Z)V

    .line 23
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getPangleData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c(Ljava/lang/String;)V

    .line 24
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getExtraData()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->a(Ljava/util/Map;)V

    .line 25
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getPangleCustomController()Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->a(Lcom/bytedance/sdk/openadsdk/TTCustomController;)V

    .line 26
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->allowBaiduSdkReadDeviceId()Z

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->a(Z)V

    .line 27
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getAdapterConfigurationClasses()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->a(Ljava/util/Set;)V

    .line 28
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getMediatedNetworkConfigurations()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->b(Ljava/util/Map;)V

    .line 29
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/TTAdConfig;->getRequestOptions()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c(Ljava/util/Map;)V

    .line 30
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a(Landroid/content/Context;)V

    .line 31
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->b()V

    return-void
.end method

.method public static registerConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V
    .locals 1

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V

    return-void
.end method

.method public static requestPermissionIfNecessary(Landroid/content/Context;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/bytedance/msdk/api/activity/TTDelegateActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    .line 2
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "type"

    const/4 v2, 0x1

    .line 3
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz p0, :cond_0

    const/4 v1, 0x0

    .line 5
    invoke-static {p0, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c;->a(Landroid/content/Context;Landroid/content/Intent;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c$a;)V

    :cond_0
    return-void
.end method

.method public static setP(Z)V
    .locals 1

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c(Z)V

    return-void
.end method

.method public static unregisterConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V
    .locals 1

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->b(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V

    return-void
.end method

.method public static updatePangleConfig(Lcom/bytedance/msdk/api/TTAdConfig;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    .line 1
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/TTAdConfig;->getPangleData()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/TTAdConfig;->getPangleData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c(Ljava/lang/String;)V

    .line 5
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/TTAdConfig;->getPangleData()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 6
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/TTAdConfig;->getPangleKeywords()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->d(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public static updatePanglePaid(Z)V
    .locals 1

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->f(Z)V

    return-void
.end method
