.class public Lcom/bytedance/msdk/api/TTAdConfig$Builder;
.super Ljava/lang/Object;
.source "TTAdConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/api/TTAdConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:I

.field private f:Z

.field private g:Z

.field private h:[I

.field private i:Z

.field private j:[Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcom/bytedance/sdk/openadsdk/TTCustomController;

.field private q:Ljava/lang/String;

.field private r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 4
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->c:Z

    .line 5
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->d:Z

    .line 6
    iput v0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->e:I

    const/4 v1, 0x1

    .line 7
    iput-boolean v1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->f:Z

    .line 8
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->g:Z

    .line 10
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->i:Z

    .line 13
    iput-boolean v1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->l:Z

    .line 16
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->o:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)[I
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->h:[I

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->i:Z

    return p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->n:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->o:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Lcom/bytedance/sdk/openadsdk/TTCustomController;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->p:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    return-object p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->q:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/util/Set;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->r:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->s:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic k(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->t:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic l(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->l:Z

    return p0
.end method

.method static synthetic m(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->c:Z

    return p0
.end method

.method static synthetic n(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->d:Z

    return p0
.end method

.method static synthetic o(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->k:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic p(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->m:Z

    return p0
.end method

.method static synthetic q(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->e:I

    return p0
.end method

.method static synthetic r(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)[Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->j:[Ljava/lang/String;

    return-object p0
.end method

.method static synthetic s(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->f:Z

    return p0
.end method

.method static synthetic t(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->g:Z

    return p0
.end method


# virtual methods
.method public allowPangleShowNotify(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->f:Z

    return-object p0
.end method

.method public allowPangleShowPageWhenScreenLock(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->g:Z

    return-object p0
.end method

.method public appId(Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->a:Ljava/lang/String;

    return-object p0
.end method

.method public appName(Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->b:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/bytedance/msdk/api/TTAdConfig;
    .locals 2

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConfig;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/msdk/api/TTAdConfig;-><init>(Lcom/bytedance/msdk/api/TTAdConfig$Builder;Lcom/bytedance/msdk/api/TTAdConfig$1;)V

    return-object v0
.end method

.method public customController(Lcom/bytedance/sdk/openadsdk/TTCustomController;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->p:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    return-object p0
.end method

.method public data(Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->n:Ljava/lang/String;

    return-object p0
.end method

.method public data(Ljava/lang/String;Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 1

    .line 2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->o:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object p0
.end method

.method public dataMap(Ljava/util/Map;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/bytedance/msdk/api/TTAdConfig$Builder;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_0
    return-object p0
.end method

.method public isPanglePaid(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->d:Z

    return-object p0
.end method

.method public varargs needPangleClearTaskReset([Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->j:[Ljava/lang/String;

    return-object p0
.end method

.method public openAdnTest(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->m:Z

    return-object p0
.end method

.method public openDebugLog(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->c:Z

    return-object p0
.end method

.method public setBaiduSdkReadDeviceId(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->l:Z

    return-object p0
.end method

.method public setKeywords(Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->q:Ljava/lang/String;

    return-object p0
.end method

.method public varargs setPangleDirectDownloadNetworkType([I)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->h:[I

    return-object p0
.end method

.method public setPangleTitleBarTheme(I)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->e:I

    return-object p0
.end method

.method public setPublisherDid(Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->k:Ljava/lang/String;

    return-object p0
.end method

.method public usePangleTextureView(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->i:Z

    return-object p0
.end method
