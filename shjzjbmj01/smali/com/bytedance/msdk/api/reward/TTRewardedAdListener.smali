.class public interface abstract Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;
.super Ljava/lang/Object;
.source "TTRewardedAdListener.java"

# interfaces
.implements Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;


# virtual methods
.method public abstract onRewardClick()V
.end method

.method public abstract onRewardVerify(Lcom/bytedance/msdk/api/reward/RewardItem;)V
.end method

.method public abstract onRewardedAdClosed()V
.end method

.method public abstract onRewardedAdShow()V
.end method

.method public abstract onSkippedVideo()V
.end method

.method public abstract onVideoComplete()V
.end method

.method public abstract onVideoError()V
.end method
