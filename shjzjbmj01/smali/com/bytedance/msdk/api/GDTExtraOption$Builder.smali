.class public final Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
.super Ljava/lang/Object;
.source "GDTExtraOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/api/GDTExtraOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 13
    iput v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->l:I

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->a:Z

    return p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->b:Z

    return p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->k:I

    return p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->l:I

    return p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->c:Z

    return p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->d:Z

    return p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->e:I

    return p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->f:I

    return p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->g:I

    return p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->h:I

    return p0
.end method

.method static synthetic k(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->i:I

    return p0
.end method

.method static synthetic l(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->j:I

    return p0
.end method


# virtual methods
.method public final build()Lcom/bytedance/msdk/api/GDTExtraOption;
    .locals 2

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/GDTExtraOption;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/msdk/api/GDTExtraOption;-><init>(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;Lcom/bytedance/msdk/api/GDTExtraOption$1;)V

    return-object v0
.end method

.method public setAutoPlayPolicy(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->g:I

    return-object p0
.end method

.method public setBrowserType(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->h:I

    return-object p0
.end method

.method public setDownAPPConfirmPolicy(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->i:I

    return-object p0
.end method

.method public setFeedExpressType(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->l:I

    return-object p0
.end method

.method public setGDTAutoPlayMuted(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->b:Z

    return-object p0
.end method

.method public setGDTDetailPageMuted(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->c:Z

    return-object p0
.end method

.method public setGDTEnableDetailPage(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->a:Z

    return-object p0
.end method

.method public setGDTEnableUserControl(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->d:Z

    return-object p0
.end method

.method public setGDTMaxVideoDuration(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->f:I

    return-object p0
.end method

.method public setGDTMinVideoDuration(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->e:I

    return-object p0
.end method

.method public setHeight(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->k:I

    return-object p0
.end method

.method public setWidth(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->j:I

    return-object p0
.end method
