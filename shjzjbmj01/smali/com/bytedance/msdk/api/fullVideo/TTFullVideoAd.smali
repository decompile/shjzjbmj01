.class public Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;
.super Lcom/bytedance/msdk/api/base/TTLoadBase;
.source "TTFullVideoAd.java"


# instance fields
.field private a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;-><init>()V

    const-string v0, "context cannot be null"

    .line 2
    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    invoke-direct {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;->m()V

    :cond_0
    return-void
.end method

.method public getAdNetworkPlatformId()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;->hasPlatFormPermission()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x3

    return v0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->n()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, -0x2

    return v0
.end method

.method public getAdNetworkRitId()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;->hasPlatFormPermission()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "-3"

    return-object v0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "-2"

    return-object v0
.end method

.method public getPreEcpm()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;->hasPlatFormPermission()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "-3"

    return-object v0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "-2"

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/d;->x()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public loadFullAd(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAdLoadCallback;)V
    .locals 3
    .param p2    # Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAdLoadCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "adSlot cannot be null"

    .line 1
    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    iget-object v0, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    if-eqz v0, :cond_3

    .line 4
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->b()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 6
    new-instance p1, Lcom/bytedance/msdk/api/AdError;

    const v0, 0x9c5f

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAdLoadCallback;->onFullVideoLoadFail(Lcom/bytedance/msdk/api/AdError;)V

    return-void

    .line 12
    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_1

    .line 14
    new-instance p1, Lcom/bytedance/msdk/api/AdError;

    const v0, 0x9c65

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAdLoadCallback;->onFullVideoLoadFail(Lcom/bytedance/msdk/api/AdError;)V

    :cond_1
    return-void

    .line 19
    :cond_2
    iget-object v0, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    invoke-virtual {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;->a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAdLoadCallback;)V

    :cond_3
    return-void
.end method

.method public showFullAd(Landroid/app/Activity;Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAdListener;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/c;->a(Landroid/app/Activity;Lcom/bytedance/msdk/api/fullVideo/TTFullVideoAdListener;)V

    :cond_0
    return-void
.end method
