.class public Lcom/bytedance/msdk/api/AdErrorUtil;
.super Ljava/lang/Object;
.source "AdErrorUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAdmobError(I)Lcom/bytedance/msdk/api/AdError;
    .locals 4

    packed-switch p0, :pswitch_data_0

    .line 1
    new-instance p0, Lcom/bytedance/msdk/api/AdError;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v0, v2}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object p0

    .line 2
    :pswitch_0
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x4e21

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 3
    :pswitch_1
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x4e22

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 4
    :pswitch_2
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x4e23

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 5
    :pswitch_3
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x4e24

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "internal error , maybe server no response ."

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getGdtError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;
    .locals 3

    const/16 v0, 0xbb9

    if-eq p0, v0, :cond_4

    const/16 v0, 0xbbb

    if-eq p0, v0, :cond_3

    const/16 v0, 0xfa1

    if-eq p0, v0, :cond_2

    const/16 v0, 0xfa3

    if-eq p0, v0, :cond_1

    const/16 v0, 0x138d

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    packed-switch p0, :pswitch_data_2

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 2
    :pswitch_0
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, -0xf

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 3
    :pswitch_1
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x69

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 5
    :pswitch_2
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/4 v1, -0x1

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 6
    :pswitch_3
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const v1, 0xc351

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 4
    :cond_0
    :pswitch_4
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/4 v1, -0x8

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 7
    :cond_1
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const v1, 0x9c59

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 8
    :cond_2
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const v1, 0x9c50

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 9
    :cond_3
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, -0xe

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 10
    :cond_4
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/4 v1, -0x2

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1389
        :pswitch_3
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x138f
        :pswitch_2
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1393
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getSigmobError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;
    .locals 5

    const v0, 0xc351

    const/16 v1, 0x4e24

    const/16 v2, 0x4e21

    const v3, 0x9c5e

    const v4, 0x9c44

    sparse-switch p0, :sswitch_data_0

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 2
    :sswitch_0
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x7530

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x7530

    invoke-direct {v0, v2, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 3
    :sswitch_1
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v4}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v4, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 4
    :sswitch_2
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x7531

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 5
    :sswitch_3
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const v1, 0x11172

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 6
    :sswitch_4
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, 0x7533

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 7
    :sswitch_5
    new-instance v1, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v1

    .line 8
    :sswitch_6
    new-instance v1, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v1

    .line 9
    :sswitch_7
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/4 v1, -0x2

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 10
    :sswitch_8
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const v1, 0x9c46

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 11
    :sswitch_9
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v3}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 12
    :sswitch_a
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v4}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v4, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 13
    :sswitch_b
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const v1, 0x9c42

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 14
    :sswitch_c
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/16 v1, -0xd

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 15
    :sswitch_d
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 16
    :sswitch_e
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v4}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v4, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 17
    :sswitch_f
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const v1, 0x9c50

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 18
    :sswitch_10
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const v1, 0x9c59

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 19
    :sswitch_11
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v4}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v4, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 20
    :sswitch_12
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v3}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 21
    :sswitch_13
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v3}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 22
    :sswitch_14
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 23
    :sswitch_15
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v2}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    .line 51
    :sswitch_16
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    invoke-static {v2}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x30d40 -> :sswitch_16
        0x7a2b2 -> :sswitch_15
        0x7a2c4 -> :sswitch_14
        0x7a2c6 -> :sswitch_13
        0x7a2c8 -> :sswitch_12
        0x7a2cc -> :sswitch_11
        0x7a2ce -> :sswitch_10
        0x7a2d0 -> :sswitch_f
        0x7a2d1 -> :sswitch_e
        0x7a2d3 -> :sswitch_d
        0x7a2d4 -> :sswitch_c
        0x7a2f9 -> :sswitch_b
        0x7a2ff -> :sswitch_a
        0x7a302 -> :sswitch_9
        0x7a3dd -> :sswitch_8
        0x92824 -> :sswitch_7
        0x92825 -> :sswitch_6
        0x92826 -> :sswitch_5
        0x92828 -> :sswitch_4
        0x92829 -> :sswitch_3
        0x92b44 -> :sswitch_2
        0x92b45 -> :sswitch_1
        0x92b4a -> :sswitch_0
    .end sparse-switch
.end method

.method public static obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;
    .locals 3

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/AdError;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method
