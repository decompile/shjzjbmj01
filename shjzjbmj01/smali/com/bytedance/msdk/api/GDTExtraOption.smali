.class public Lcom/bytedance/msdk/api/GDTExtraOption;
.super Ljava/lang/Object;
.source "GDTExtraOption.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/api/GDTExtraOption$Builder;,
        Lcom/bytedance/msdk/api/GDTExtraOption$DownAPPConfirmPolicy;,
        Lcom/bytedance/msdk/api/GDTExtraOption$BrowserType;,
        Lcom/bytedance/msdk/api/GDTExtraOption$VideoPlayPolicy;,
        Lcom/bytedance/msdk/api/GDTExtraOption$AutoPlayPolicy;,
        Lcom/bytedance/msdk/api/GDTExtraOption$FeedExpressType;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private l:I


# direct methods
.method private constructor <init>(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 3
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->a:Z

    .line 4
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->b:Z

    const/4 v1, 0x0

    .line 5
    iput-boolean v1, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->c:Z

    .line 6
    iput-boolean v1, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->d:Z

    .line 7
    iput v1, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->e:I

    .line 14
    iput v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->l:I

    .line 86
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->a(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->a:Z

    .line 87
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->b(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->b:Z

    .line 88
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->e(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->c:Z

    .line 89
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->f(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->d:Z

    .line 90
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->g(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->f:I

    .line 91
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->h(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->g:I

    .line 92
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->i(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->e:I

    .line 93
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->j(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->h:I

    .line 94
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->k(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->i:I

    .line 95
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->l(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->j:I

    .line 96
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->c(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->k:I

    .line 97
    invoke-static {p1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->d(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)I

    move-result p1

    iput p1, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->l:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;Lcom/bytedance/msdk/api/GDTExtraOption$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/msdk/api/GDTExtraOption;-><init>(Lcom/bytedance/msdk/api/GDTExtraOption$Builder;)V

    return-void
.end method


# virtual methods
.method public getBrowserType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->h:I

    return v0
.end method

.method public getDownAPPConfirmPolicy()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->i:I

    return v0
.end method

.method public getFeedExpressType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->l:I

    return v0
.end method

.method public getGDTAutoPlayPolicy()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->e:I

    return v0
.end method

.method public getGDTMaxVideoDuration()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->g:I

    return v0
.end method

.method public getGDTMinVideoDuration()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->f:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->k:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->j:I

    return v0
.end method

.method public isGDTAutoPlayMuted()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->b:Z

    return v0
.end method

.method public isGDTDetailPageMuted()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->c:Z

    return v0
.end method

.method public isGDTEnableDetailPage()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->a:Z

    return v0
.end method

.method public isGDTEnableUserControl()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/GDTExtraOption;->d:Z

    return v0
.end method
