.class public Lcom/bytedance/msdk/api/AdError;
.super Ljava/lang/Object;
.source "AdError.java"


# static fields
.field public static final ADTYPE_NOT_MATCH_RIT:I = -0xd

.field public static final AD_BIDDING_REQUEST_ERROR:I = -0x10

.field public static final AD_DATA_ERROR:I = -0x4

.field public static final AD_ERROR_INTERNAL_ERROR_MSG:Ljava/lang/String; = "internal error , maybe server no response ."

.field public static final AD_EXPIRED_ERROR:I = -0xf

.field public static final AD_INTERNAL_ERROR_MSG:Ljava/lang/String; = "internal error\uff0c server no response !"

.field public static final AD_LOAD_AD_TIME_OUT_ERROR_MSG:Ljava/lang/String; = "load ad timeout !!!"

.field public static final AD_LOAD_FAIL_MSG:Ljava/lang/String; = "load fail"

.field public static final AD_LOAD_SUCCESS_MSG:Ljava/lang/String; = "load success"

.field public static final AD_LOAD_TIMEOUT_MSG:Ljava/lang/String; = "load ad timeout"

.field public static final AD_NETWORK_ERROR_MSG:Ljava/lang/String; = "network error!"

.field public static final AD_NO_FILL:Ljava/lang/String; = "load success, but no ad fill !"

.field public static final AD_SLOTID_ERROR_MSG:Ljava/lang/String; = "ad slotId error!"

.field public static final AD_THIRD_SDK_ADAPTER_CONFIGURATION_ERROR_MSG:Ljava/lang/String; = "{0} sdk configuration error or initialization fail "

.field public static final AD_THIRD_SDK_ADAPTER_INITIALIZATION_SUCCESS_MSG:Ljava/lang/String; = "{0} sdk initialization success "

.field public static final AD_THIRD_SDK_AD_BLOCKER_DETECTED_MSG:Ljava/lang/String; = "{0} sdk ad blocker "

.field public static final AD_THIRD_SDK_AD_SHOW_ERROR_MSG:Ljava/lang/String; = "{0} sdk ad show error "

.field public static final AD_THIRD_SDK_DEVICE_ID_ERROR_MSG:Ljava/lang/String; = "{0} sdk device id error "

.field public static final AD_THIRD_SDK_FILE_IO_ERROR_MSG:Ljava/lang/String; = "{0} sdk file io error "

.field public static final AD_THIRD_SDK_INIT_FAIL_MSG:Ljava/lang/String; = "{0} sdk initialized fail !!!!"

.field public static final AD_THIRD_SDK_INTERNAL_ERROR_MSG:Ljava/lang/String; = "{0} sdk internal error "

.field public static final AD_THIRD_SDK_NOT_INIT_MSG:Ljava/lang/String; = "{0} sdk not initialized !!!! "

.field public static final AD_THIRD_SDK_NO_CACHE_CALLBACK_MSG:Ljava/lang/String; = "sdk no cache callback "

.field public static final AD_THIRD_SDK_PARARM_ERROR_MSG:Ljava/lang/String; = "{0} sdk params error !!"

.field public static final AD_UNKNOWN_ERROR_MSG:Ljava/lang/String; = "unknown error!"

.field public static final BANNER_AD_LOAD_IMAGE_ERROR:I = -0x5

.field public static final ERROR_CODE_ACCESS_METHOD_API_SDK:I = 0x9c51

.field public static final ERROR_CODE_ACCESS_METHOD_PASS:I = 0x9c4c

.field public static final ERROR_CODE_ADAPTER_CONFIGURATION_ERROR:I = 0x753c

.field public static final ERROR_CODE_ADAPTER_INITIALIZATION_SUCCESS:I = 0x753b

.field public static final ERROR_CODE_ADCOUNT_ERROR:I = 0x9c47

.field public static final ERROR_CODE_ADSLOT_CONFIG_ERROR:I = 0x9c5a

.field public static final ERROR_CODE_ADSLOT_EMPTY:I = 0x9c44

.field public static final ERROR_CODE_ADSLOT_ERROR:I = 0x9c59

.field public static final ERROR_CODE_ADSLOT_ID_ERROR:I = 0x9c46

.field public static final ERROR_CODE_ADSLOT_SIZE_EMPTY:I = 0x9c45

.field public static final ERROR_CODE_ADTYPE_DIFFER:I = 0x9c53

.field public static final ERROR_CODE_AD_LOAD_SUCCESS:I = 0x4e20

.field public static final ERROR_CODE_AD_TYPE:I = 0x9c4b

.field public static final ERROR_CODE_APK_SIGN_CHECK_ERROR:I = 0x9c55

.field public static final ERROR_CODE_APP_EMPTY:I = 0x9c42

.field public static final ERROR_CODE_BANNER_MODULE_UNABLE:I = 0x9c60

.field public static final ERROR_CODE_CACHE_AD_MATERIAL_FAIL:I = 0x11172

.field public static final ERROR_CODE_CONTENT_TYPE:I = 0x9c40

.field public static final ERROR_CODE_FEED_MODULE_UNABLE:I = 0x9c63

.field public static final ERROR_CODE_FULL_MODULE_UNABLE:I = 0x9c65

.field public static final ERROR_CODE_IMAGE_SIZE:I = 0x9c48

.field public static final ERROR_CODE_INTERACTION_MODULE_UNABLE:I = 0x9c61

.field public static final ERROR_CODE_INTERNAL_ERROR:I = 0x4e24

.field public static final ERROR_CODE_LOAD_AD_TIMEOUT:I = 0xfab

.field public static final ERROR_CODE_MEDIA_ID:I = 0x9c49

.field public static final ERROR_CODE_MEDIA_TYPE:I = 0x9c4a

.field public static final ERROR_CODE_MSDK_NOT_INIT:I = 0x1

.field public static final ERROR_CODE_NATIVE_MODULE_UNABLE:I = 0x9c66

.field public static final ERROR_CODE_NETWORK_ERROR:I = 0x4e22

.field public static final ERROR_CODE_NEW_REGISTER_LIMIT:I = 0x9c54

.field public static final ERROR_CODE_NO_AD:I = 0x4e21

.field public static final ERROR_CODE_NO_DEVICE_INFO:I = 0x9c5e

.field public static final ERROR_CODE_ORIGIN_AD_ERROR:I = 0x9c56

.field public static final ERROR_CODE_PACKAGE_NAME:I = 0x9c52

.field public static final ERROR_CODE_PANGLE_APPID_NO_SAME:I = 0x9c5c

.field public static final ERROR_CODE_REDIRECT:I = 0x9c4e

.field public static final ERROR_CODE_REQUEST_ERROR:I = 0x11170

.field public static final ERROR_CODE_REQUEST_INVALID:I = 0x9c4f

.field public static final ERROR_CODE_REQUEST_PB_ERROR:I = 0x9c41

.field public static final ERROR_CODE_REQUEST_TIME_OUT:I = 0x11171

.field public static final ERROR_CODE_REWARD_MODULE_UNABLE:I = 0x9c64

.field public static final ERROR_CODE_RIT_ADTYPE_NO_SAME:I = 0x9c5f

.field public static final ERROR_CODE_SLOT_ID_APP_ID_DIFFER:I = 0x9c50

.field public static final ERROR_CODE_SLOT_ID_ERROR:I = 0x4e23

.field public static final ERROR_CODE_SPLASH_AD_TYPE:I = 0x9c4d

.field public static final ERROR_CODE_SPLASH_CARRY_BOTTOM:I = 0x9c5b

.field public static final ERROR_CODE_SPLASH_MODULE_UNABLE:I = 0x9c62

.field public static final ERROR_CODE_SYS_ERROR:I = 0xc351

.field public static final ERROR_CODE_THIRD_SDK_AD_BLOCKER_DETECTED:I = 0x7534

.field public static final ERROR_CODE_THIRD_SDK_AD_SHOW_ERROR:I = 0x7536

.field public static final ERROR_CODE_THIRD_SDK_CONFIG_ERROR:I = 0x7539

.field public static final ERROR_CODE_THIRD_SDK_DEVICE_ID_ERROR:I = 0x7535

.field public static final ERROR_CODE_THIRD_SDK_FILE_IO_ERROR:I = 0x7533

.field public static final ERROR_CODE_THIRD_SDK_INIT_FAIL:I = 0x7530

.field public static final ERROR_CODE_THIRD_SDK_INTERNAL_ERROR:I = 0x7537

.field public static final ERROR_CODE_THIRD_SDK_LOAD_FAIL:I = 0x7538

.field public static final ERROR_CODE_THIRD_SDK_NOT_INIT:I = 0x7531

.field public static final ERROR_CODE_THIRD_SDK_NO_CACHE_CALLBACK:I = 0x753a

.field public static final ERROR_CODE_THIRD_SDK_PARARM_ERROR:I = 0x7532

.field public static final ERROR_CODE_UNION_OS_ERROR:I = 0x9c57

.field public static final ERROR_CODE_UNION_SDK_TOO_OLD:I = 0x9c58

.field public static final ERROR_CODE_UNKNOWN_ERROR:I = 0x0

.field public static final ERROR_CODE_VERIFY_REWARD:I = 0xea67

.field public static final ERROR_CODE_WAP_EMPTY:I = 0x9c43

.field public static final FREQUENT_CALL_ERROE:I = -0x8

.field public static final INSERT_AD_LOAD_IMAGE_ERROR:I = -0x6

.field public static final LOAD_AD_TIME_OUT_ERROR:I = 0x2713

.field public static final NET_ERROR:I = -0x2

.field public static final NET_TYPE_ERROR:I = -0xe

.field public static final NO_AD_PARSE:I = -0x3

.field public static final PARSE_FAIL:I = -0x1

.field public static final REDER_AD_LOAD_TIMEOUT:I = 0x6c

.field public static final REDER_MAIN_TEMPLATE_LOAD_ERROR:I = 0x6d

.field public static final RENDER_DIFF_TEMPLATE_INVALID:I = 0x67

.field public static final RENDER_FAIL_META_INVALID:I = 0x68

.field public static final RENDER_FAIL_TEMPLATE_PARSE_ERROR:I = 0x69

.field public static final RENDER_FAIL_TIMEOUT:I = 0x6b

.field public static final RENDER_FAIL_UNKNOWN:I = 0x6a

.field public static final RENDER_MAIN_TEMPLATE_INVALID:I = 0x66

.field public static final RENDER_RENDER_PARSE_ERROR:I = 0x65

.field public static final REQUEST_BODY_ERROR:I = -0x9

.field public static final SPLASH_AD_LOAD_IMAGE_ERROR:I = -0x7

.field public static final SPLASH_CACHE_EXPIRED_ERROR:I = -0xb

.field public static final SPLASH_CACHE_PARSE_ERROR:I = -0xa

.field public static final SPLASH_NOT_HAVE_CACHE_ERROR:I = -0xc


# instance fields
.field public code:I

.field public message:Ljava/lang/String;

.field public thirdSdkErrorCode:I

.field public thirdSdkErrorMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/bytedance/msdk/api/AdError;->code:I

    .line 3
    iput-object p2, p0, Lcom/bytedance/msdk/api/AdError;->message:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iput p1, p0, Lcom/bytedance/msdk/api/AdError;->code:I

    .line 6
    iput-object p2, p0, Lcom/bytedance/msdk/api/AdError;->message:Ljava/lang/String;

    .line 7
    iput p3, p0, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorCode:I

    .line 8
    iput-object p4, p0, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorMessage:Ljava/lang/String;

    return-void
.end method

.method public static errorMsg(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;
    .locals 2

    const/16 v0, 0x753b

    const/4 v1, 0x0

    if-eq p0, v0, :cond_1

    const/16 v0, 0x753c

    if-eq p0, v0, :cond_0

    const-string p0, "unknown error!"

    .line 1
    invoke-static {v1, p0}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p0

    return-object p0

    .line 2
    :cond_0
    new-array p0, v1, [Ljava/lang/String;

    const-string v1, "{0} sdk configuration error or initialization fail "

    invoke-static {v1, p1, p0}, Lcom/bytedance/msdk/api/AdError;->messageFormat(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p0

    return-object p0

    .line 3
    :cond_1
    new-array p0, v1, [Ljava/lang/String;

    const-string v1, "{0} sdk initialization success "

    invoke-static {v1, p1, p0}, Lcom/bytedance/msdk/api/AdError;->messageFormat(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/bytedance/msdk/api/AdErrorUtil;->obtainAdError(ILjava/lang/String;)Lcom/bytedance/msdk/api/AdError;

    move-result-object p0

    return-object p0
.end method

.method public static getMessage(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/16 v0, 0xfab

    if-eq p0, v0, :cond_3

    const/16 v0, 0x2713

    if-eq p0, v0, :cond_2

    const v0, 0xc351

    if-eq p0, v0, :cond_1

    const v0, 0xea67

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    packed-switch p0, :pswitch_data_2

    packed-switch p0, :pswitch_data_3

    packed-switch p0, :pswitch_data_4

    packed-switch p0, :pswitch_data_5

    packed-switch p0, :pswitch_data_6

    const-string p0, "unknown error!"

    return-object p0

    :pswitch_0
    const-string p0, "\u89e3\u6790\u5931\u8d25"

    return-object p0

    :pswitch_1
    const-string p0, "\u7f51\u7edc\u8bf7\u6c42\u5931\u8d25"

    return-object p0

    :pswitch_2
    const-string p0, "\u6ca1\u6709\u89e3\u6790\u5230\u5e7f\u544a"

    return-object p0

    :pswitch_3
    const-string p0, "\u8fd4\u56de\u6570\u636e\u7f3a\u5c11\u5fc5\u8981\u5b57\u6bb5"

    return-object p0

    :pswitch_4
    const-string p0, "Banner\u5e7f\u544a\u52a0\u8f7d\u56fe\u7247\u5931\u8d25"

    return-object p0

    :pswitch_5
    const-string p0, "\u63d2\u5c4f\u5e7f\u544a\u56fe\u7247\u52a0\u8f7d\u5931\u8d25"

    return-object p0

    :pswitch_6
    const-string p0, "\u5f00\u5c4f\u5e7f\u544a\u56fe\u7247\u52a0\u8f7d\u5931\u8d25"

    return-object p0

    :pswitch_7
    const-string p0, "\u5e7f\u544a\u8bf7\u6c42\u9891\u7387\u8fc7\u9ad8"

    return-object p0

    :pswitch_8
    const-string p0, "\u8bf7\u6c42\u5b9e\u4f53\u4e3a\u7a7a"

    return-object p0

    :pswitch_9
    const-string p0, "\u7f13\u5b58\u89e3\u6790\u5931\u8d25"

    return-object p0

    :pswitch_a
    const-string p0, "\u7f13\u5b58\u8fc7\u671f"

    return-object p0

    :pswitch_b
    const-string p0, "\u7f13\u5b58\u4e2d\u6ca1\u6709\u5f00\u5c4f\u5e7f\u544a"

    return-object p0

    :pswitch_c
    const-string p0, "\u5e7f\u544a\u8bf7\u6c42\u4f7f\u7528\u4e86\u9519\u8bef\u4ee3\u7801\u4f4d"

    return-object p0

    :pswitch_d
    const-string p0, "\u7f51\u7edc\u7c7b\u578b\u9519\u8bef\uff0c\u5f53\u524d\u8bbe\u5907\u7684\u7f51\u7edc\u7c7b\u578b\u4e0d\u7b26\u5408\u5f00\u5c4f\u5e7f\u544a\u7684\u52a0\u8f7d\u6761\u4ef6,\u68c0\u67e5\u7f51\u7edc\u72b6\u6001\u5e76\u91cd\u8bd5"

    return-object p0

    :pswitch_e
    const-string p0, "\u5e7f\u544a\u6570\u636e\u8fc7\u671f"

    return-object p0

    :pswitch_f
    const-string p0, "Bidding \u8bf7\u6c42\u51fa\u9519"

    return-object p0

    :pswitch_10
    const-string p0, "\u6a21\u677f\u4e3b\u5f15\u64ce\u52a0\u8f7d\u5931\u8d25"

    return-object p0

    :pswitch_11
    const-string p0, "\u6a21\u677f\u5e7f\u544a\u52a0\u8f7d\u8d85\u65f6\u65e0\u8fd4\u56de"

    return-object p0

    :pswitch_12
    const-string p0, "\u6e32\u67d3\u8d85\u65f6\u672a\u56de\u8c03"

    return-object p0

    :pswitch_13
    const-string p0, "\u6e32\u67d3\u672a\u77e5\u62a5\u9519"

    return-object p0

    :pswitch_14
    const-string p0, "\u6a21\u677f\u6570\u636e\u89e3\u6790\u5f02\u5e38"

    return-object p0

    :pswitch_15
    const-string p0, "\u6a21\u677f\u7269\u6599\u6570\u636e\u5f02\u5e38"

    return-object p0

    :pswitch_16
    const-string p0, "\u6a21\u677f\u5dee\u91cf\u65e0\u6548"

    return-object p0

    :pswitch_17
    const-string p0, "\u4e3b\u6a21\u677f\u65e0\u6548"

    return-object p0

    :pswitch_18
    const-string p0, "\u6e32\u67d3\u7ed3\u679c\u6570\u636e\u89e3\u6790\u5931\u8d25"

    return-object p0

    :pswitch_19
    const-string p0, "internal error\uff0c server no response !"

    return-object p0

    :pswitch_1a
    const-string p0, "ad slotId error!"

    return-object p0

    :pswitch_1b
    const-string p0, "network error!"

    return-object p0

    :pswitch_1c
    const-string p0, "load success, but no ad fill !"

    return-object p0

    :pswitch_1d
    const-string p0, "load success"

    return-object p0

    :pswitch_1e
    const-string p0, "\u5a92\u4f53\u8bf7\u6c42\u7d20\u6750\u662f\u5426\u539f\u751f\u4e0e\u5a92\u4f53\u5e73\u53f0\u5f55\u5165\u4e0d\u4e00\u81f4"

    return-object p0

    :pswitch_1f
    const-string p0, "apk\u7b7e\u540dsha1\u503c\u4e0e\u5a92\u4f53\u5e73\u53f0\u5f55\u5165\u4e0d\u4e00\u81f4"

    return-object p0

    :pswitch_20
    const-string p0, "\u5f00\u53d1\u6ce8\u518c\u65b0\u4e0a\u7ebf\u5e7f\u544a\u4f4d\u8d85\u51fa\u65e5\u8bf7\u6c42\u91cf\u9650\u5236"

    return-object p0

    :pswitch_21
    const-string p0, "\u5a92\u4f53\u914d\u7f6eadtype\u548c\u8bf7\u6c42\u4e0d\u4e00\u81f4"

    return-object p0

    :pswitch_22
    const-string p0, "\u5a92\u4f53\u5305\u540d\u5f02\u5e38"

    return-object p0

    :pswitch_23
    const-string p0, "SlotId\u548cAppId\u5339\u914d\u5f02\u5e38"

    return-object p0

    :pswitch_24
    const-string p0, "\u5a92\u4f53\u6574\u6539\u8d85\u8fc7\u671f\u9650\uff0c\u8bf7\u6c42\u975e\u6cd5"

    return-object p0

    :pswitch_25
    const-string p0, "redirect\u53c2\u6570\u4e0d\u6b63\u786e"

    return-object p0

    :pswitch_26
    const-string p0, "\u5f00\u5c4f\u5e7f\u544a\u7c7b\u578b\u5f02\u5e38"

    return-object p0

    :pswitch_27
    const-string p0, "\u5a92\u4f53\u63a5\u5165\u7c7b\u578b\u4e0d\u5408\u6cd5"

    return-object p0

    :pswitch_28
    const-string p0, "\u5e7f\u544a\u7c7b\u578b\u4e0d\u5408\u6cd5"

    return-object p0

    :pswitch_29
    const-string p0, "\u5a92\u4f53\u7c7b\u578b\u4e0d\u5408\u6cd5"

    return-object p0

    :pswitch_2a
    const-string p0, "\u5a92\u4f53\u5e94\u7528ID\u4e0d\u5408\u6cd5"

    return-object p0

    :pswitch_2b
    const-string p0, "\u56fe\u7247\u5c3a\u5bf8\u9519\u8bef"

    return-object p0

    :pswitch_2c
    const-string p0, "\u5e7f\u544a\u6570\u91cf\u9519\u8bef"

    return-object p0

    :pswitch_2d
    const-string p0, "\u5e7f\u544a\u4f4dID\u4e0d\u5408\u6cd5\u6216\u8005\u672a\u542f\u7528\uff0c\u8bf7\u68c0\u67e5\u5e7f\u544a\u805a\u5408\u5e73\u53f0\u7684\u76f8\u5173\u914d\u7f6e"

    return-object p0

    :pswitch_2e
    const-string p0, "\u5e7f\u544a\u4f4d\u5c3a\u5bf8\u4e0d\u80fd\u4e3a\u7a7a"

    return-object p0

    :pswitch_2f
    const-string p0, "\u5e7f\u544a\u4f4d\u4e0d\u80fd\u4e3a\u7a7a"

    return-object p0

    :pswitch_30
    const-string p0, "\u8bf7\u6c42wap\u4e0d\u80fd\u4e3a\u7a7a"

    return-object p0

    :pswitch_31
    const-string p0, "\u8bf7\u6c42app\u4e0d\u80fd\u4e3a\u7a7a "

    return-object p0

    :pswitch_32
    const-string p0, "http request pb\u9519\u8bef"

    return-object p0

    :pswitch_33
    const-string p0, "http conent_type\u9519\u8bef"

    return-object p0

    :pswitch_34
    const-string p0, "\u7a7f\u5c71\u7532appID\u4e0e\u521d\u59cb\u5316appID\u4e0d\u4e00\u81f4\uff0c\u8bf7\u68c0\u67e5\u5e76\u4f20\u5165\u4e0e\u521d\u59cb\u5316\u76f8\u540cappID"

    return-object p0

    :pswitch_35
    const-string p0, "\u5f00\u5c4f\u5e7f\u544a\u81ea\u5b9a\u4e49\u515c\u5e95\u53c2\u6570\u4e0d\u6b63\u786e\uff0c\u8bf7\u6821\u9a8c"

    return-object p0

    :pswitch_36
    const-string p0, "adSlot \u4e0d\u80fd\u4e3a null"

    return-object p0

    :pswitch_37
    const-string p0, "\u5e7f\u544a\u4f4d\u9519\u8bef"

    return-object p0

    :pswitch_38
    const-string p0, "sdk \u7248\u672c\u8fc7\u4f4e\u4e0d\u8fd4\u56de\u5e7f\u544a"

    return-object p0

    :pswitch_39
    const-string p0, "\u4fe1\u606f\u6d41\u81ea\u6e32\u67d3\u5e7f\u544a\u6682\u65e0\u5e7f\u544a\u8fd4\u56de\uff0c\u8bf7\u7a0d\u540e\u91cd\u8bd5"

    return-object p0

    :pswitch_3a
    const-string p0, "\u5168\u5c4f\u89c6\u9891\u5e7f\u544a\u6682\u65e0\u5e7f\u544a\u8fd4\u56de\uff0c\u8bf7\u7a0d\u540e\u91cd\u8bd5"

    return-object p0

    :pswitch_3b
    const-string p0, "\u6fc0\u52b1\u89c6\u9891\u5e7f\u544a\u6682\u65e0\u5e7f\u544a\u8fd4\u56de\uff0c\u8bf7\u7a0d\u540e\u91cd\u8bd5"

    return-object p0

    :pswitch_3c
    const-string p0, "\u4fe1\u606f\u6d41\u6a21\u677f\u5e7f\u544a\u6682\u65e0\u5e7f\u544a\u8fd4\u56de\uff0c\u8bf7\u7a0d\u540e\u91cd\u8bd5"

    return-object p0

    :pswitch_3d
    const-string p0, "\u5f00\u5c4f\u5e7f\u544a\u6682\u65e0\u5e7f\u544a\u8fd4\u56de\uff0c\u8bf7\u7a0d\u540e\u91cd\u8bd5"

    return-object p0

    :pswitch_3e
    const-string p0, "\u63d2\u5c4f\u5e7f\u544a\u6682\u65e0\u5e7f\u544a\u8fd4\u56de\uff0c\u8bf7\u7a0d\u540e\u91cd\u8bd5"

    return-object p0

    :pswitch_3f
    const-string p0, "Banner\u5e7f\u544a\u6682\u65e0\u5e7f\u544a\u8fd4\u56de\uff0c\u8bf7\u7a0d\u540e\u91cd\u8bd5"

    return-object p0

    :pswitch_40
    const-string p0, "\u805a\u5408\u5e7f\u544a\u4f4d\u5bf9\u5e94\u7684\u5e7f\u544a\u7c7b\u578b\u548c\u5f53\u524d\u5e7f\u544a\u7c7b\u578b\u4e0d\u4e00\u81f4"

    return-object p0

    :pswitch_41
    const-string p0, "\u8bf7\u6c42\u53c2\u6570\u7f3a\u5c11\u8bbe\u5907\u4fe1\u606f\u6216\u8005\u7f3a\u5c11\u8bbe\u5907id\u76f8\u5173\u4fe1\u606f"

    return-object p0

    :pswitch_42
    const-string p0, "\u7f13\u5b58\u7269\u6599\u5931\u8d25"

    return-object p0

    :pswitch_43
    const-string p0, "\u8bf7\u6c42\u8d85\u65f6"

    return-object p0

    :pswitch_44
    const-string p0, "\u8bf7\u6c42\u65f6\u4f7f\u7528\u4e86\u9519\u8bef\u7684\u53c2\u6570\uff0c\u6bd4\u5982\u4f7f\u7528\u9519\u8bef\u7684\u5e7f\u544a\u4f4dID"

    return-object p0

    :cond_0
    const-string p0, "\u6fc0\u52b1\u89c6\u9891\u9a8c\u8bc1\u670d\u52a1\u5668\u5f02\u5e38\u6216\u5904\u7406\u5931\u8d25"

    return-object p0

    :cond_1
    const-string p0, "\u670d\u52a1\u5668\u9519\u8bef"

    return-object p0

    :cond_2
    const-string p0, "load ad timeout !!!"

    return-object p0

    :cond_3
    const-string p0, "load ad timeout"

    return-object p0

    :cond_4
    const-string p0, "MSDK\u8fd8\u672a\u521d\u59cb\u5316"

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch -0x10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x65
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x4e20
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x9c40
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_27
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x9c58
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x9c5e
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x11170
        :pswitch_44
        :pswitch_43
        :pswitch_42
    .end packed-switch
.end method

.method public static varargs messageFormat(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_1

    .line 5
    array-length v2, p2

    if-lez v2, :cond_1

    const/4 v2, 0x2

    .line 6
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    aput-object p2, v2, v1

    invoke-static {p0, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 8
    :cond_1
    new-array p2, v1, [Ljava/lang/Object;

    aput-object p1, p2, v0

    invoke-static {p0, p2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_2
    :goto_1
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdError{code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/bytedance/msdk/api/AdError;->code:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", message=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/bytedance/msdk/api/AdError;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", thirdSdkErrorCode="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorCode:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", thirdSdkErrorMessage=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorMessage:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
