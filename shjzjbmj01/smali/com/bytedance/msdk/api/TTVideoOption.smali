.class public Lcom/bytedance/msdk/api/TTVideoOption;
.super Ljava/lang/Object;
.source "TTVideoOption.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/api/TTVideoOption$Builder;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z

.field private c:F

.field private d:Lcom/bytedance/msdk/api/GDTExtraOption;

.field private e:Lcom/bytedance/msdk/api/BaiduExtraOptions;


# direct methods
.method private constructor <init>(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->a(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTVideoOption;->a:Z

    .line 4
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->b(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)F

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/TTVideoOption;->c:F

    .line 5
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->c(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTVideoOption;->d:Lcom/bytedance/msdk/api/GDTExtraOption;

    .line 6
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->d(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTVideoOption;->b:Z

    .line 7
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->e(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)Lcom/bytedance/msdk/api/BaiduExtraOptions;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/api/TTVideoOption;->e:Lcom/bytedance/msdk/api/BaiduExtraOptions;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/msdk/api/TTVideoOption$Builder;Lcom/bytedance/msdk/api/TTVideoOption$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/msdk/api/TTVideoOption;-><init>(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)V

    return-void
.end method


# virtual methods
.method public getAdmobAppVolume()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/TTVideoOption;->c:F

    return v0
.end method

.method public getBaiduExtraOption()Lcom/bytedance/msdk/api/BaiduExtraOptions;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTVideoOption;->e:Lcom/bytedance/msdk/api/BaiduExtraOptions;

    return-object v0
.end method

.method public getGDTExtraOption()Lcom/bytedance/msdk/api/GDTExtraOption;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTVideoOption;->d:Lcom/bytedance/msdk/api/GDTExtraOption;

    return-object v0
.end method

.method public isMuted()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/TTVideoOption;->a:Z

    return v0
.end method

.method public useSurfaceView()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/TTVideoOption;->b:Z

    return v0
.end method
