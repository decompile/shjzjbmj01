.class public Lcom/bytedance/msdk/api/AdSlot;
.super Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;
.source "AdSlot.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/api/AdSlot$Builder;
    }
.end annotation


# static fields
.field public static final AUTO_HEIGHT:I = -0x2

.field public static final CUSTOM_DATA_KEY_ADMOB:Ljava/lang/String; = "admob"

.field public static final CUSTOM_DATA_KEY_BAIDU:Ljava/lang/String; = "baidu"

.field public static final CUSTOM_DATA_KEY_GDT:Ljava/lang/String; = "gdt"

.field public static final CUSTOM_DATA_KEY_KS:Ljava/lang/String; = "ks"

.field public static final CUSTOM_DATA_KEY_MINTEGRAL:Ljava/lang/String; = "mintegral"

.field public static final CUSTOM_DATA_KEY_PANGLE:Ljava/lang/String; = "pangle"

.field public static final CUSTOM_DATA_KEY_SIGMOB:Ljava/lang/String; = "sigmob"

.field public static final CUSTOM_DATA_KEY_UNITY:Ljava/lang/String; = "unity"

.field public static final FULL_WIDTH:I = -0x1

.field public static final TYPE_BANNER:I = 0x1

.field public static final TYPE_CACHED_SPLASH:I = 0x4

.field public static final TYPE_DRAW_FEED:I = 0x9

.field public static final TYPE_EXPRESS_AD:I = 0x1

.field public static final TYPE_FEED:I = 0x5

.field public static final TYPE_FULL_SCREEN_VIDEO:I = 0x8

.field public static final TYPE_INTERACTION_AD:I = 0x2

.field public static final TYPE_NATIVE_AD:I = 0x2

.field public static final TYPE_REWARD_VIDEO:I = 0x7

.field public static final TYPE_SPLASH:I = 0x3


# instance fields
.field private e:Ljava/lang/String;

.field private f:J

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:I

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:Ljava/lang/String;

.field private p:I

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:I

.field private u:Lcom/bytedance/msdk/api/TTVideoOption;

.field private v:Lcom/bytedance/msdk/api/TTRequestExtraParams;

.field private w:Lcom/bytedance/msdk/api/AdmobNativeAdOptions;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 2
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;-><init>()V

    const/4 v0, 0x2

    .line 3
    iput v0, p0, Lcom/bytedance/msdk/api/AdSlot;->m:I

    const/4 v0, 0x3

    .line 4
    iput v0, p0, Lcom/bytedance/msdk/api/AdSlot;->n:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/msdk/api/AdSlot$1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/api/AdSlot;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/AdSlot;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->t:I

    return p1
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/AdmobNativeAdOptions;)Lcom/bytedance/msdk/api/AdmobNativeAdOptions;
    .locals 0

    .line 4
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->w:Lcom/bytedance/msdk/api/AdmobNativeAdOptions;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/TTRequestExtraParams;)Lcom/bytedance/msdk/api/TTRequestExtraParams;
    .locals 0

    .line 3
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->v:Lcom/bytedance/msdk/api/TTRequestExtraParams;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/api/TTVideoOption;
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->u:Lcom/bytedance/msdk/api/TTVideoOption;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 6
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/AdSlot;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .line 7
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->s:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/AdSlot;Z)Z
    .locals 0

    .line 5
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/AdSlot;->k:Z

    return p1
.end method

.method static synthetic b(Lcom/bytedance/msdk/api/AdSlot;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->j:I

    return p1
.end method

.method static synthetic b(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->q:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/bytedance/msdk/api/AdSlot;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->l:I

    return p1
.end method

.method static synthetic c(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->r:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/bytedance/msdk/api/AdSlot;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->m:I

    return p1
.end method

.method static synthetic e(Lcom/bytedance/msdk/api/AdSlot;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->n:I

    return p1
.end method

.method static synthetic f(Lcom/bytedance/msdk/api/AdSlot;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->h:I

    return p1
.end method

.method static synthetic g(Lcom/bytedance/msdk/api/AdSlot;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->i:I

    return p1
.end method

.method static synthetic h(Lcom/bytedance/msdk/api/AdSlot;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->p:I

    return p1
.end method


# virtual methods
.method public getAdCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/AdSlot;->j:I

    return v0
.end method

.method public getAdStyleType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/AdSlot;->m:I

    return v0
.end method

.method public getAdType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/AdSlot;->l:I

    return v0
.end method

.method public getAdUnitId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getAdmobNativeAdOptions()Lcom/bytedance/msdk/api/AdmobNativeAdOptions;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->w:Lcom/bytedance/msdk/api/AdmobNativeAdOptions;

    return-object v0
.end method

.method public getBannerSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/AdSlot;->n:I

    return v0
.end method

.method public getCustomData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->s:Ljava/util/Map;

    return-object v0
.end method

.method public getImgAcceptedHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/AdSlot;->i:I

    return v0
.end method

.method public getImgAcceptedWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/AdSlot;->h:I

    return v0
.end method

.method public getMediaExtra()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/AdSlot;->t:I

    return v0
.end method

.method public getReuestParam()Lcom/bytedance/msdk/api/TTRequestExtraParams;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->v:Lcom/bytedance/msdk/api/TTRequestExtraParams;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lcom/bytedance/msdk/api/TTRequestExtraParams;

    invoke-direct {v0}, Lcom/bytedance/msdk/api/TTRequestExtraParams;-><init>()V

    iput-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->v:Lcom/bytedance/msdk/api/TTRequestExtraParams;

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->v:Lcom/bytedance/msdk/api/TTRequestExtraParams;

    return-object v0
.end method

.method public getRewardAmount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/AdSlot;->p:I

    return v0
.end method

.method public getRewardName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->o:Ljava/lang/String;

    return-object v0
.end method

.method public getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->u:Lcom/bytedance/msdk/api/TTVideoOption;

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->r:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/AdSlot;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getWaterfallId()J
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-wide v0, p0, Lcom/bytedance/msdk/api/AdSlot;->f:J

    return-wide v0
.end method

.method public isSupportDeepLink()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/AdSlot;->k:Z

    return v0
.end method

.method public setAdCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->j:I

    return-void
.end method

.method public setAdType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot;->l:I

    return-void
.end method

.method public setAdUnitId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->e:Ljava/lang/String;

    return-void
.end method

.method public setTTVideoOption(Lcom/bytedance/msdk/api/TTVideoOption;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->u:Lcom/bytedance/msdk/api/TTVideoOption;

    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot;->g:Ljava/lang/String;

    return-void
.end method

.method public setWaterfallId(J)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput-wide p1, p0, Lcom/bytedance/msdk/api/AdSlot;->f:J

    return-void
.end method
