.class public Lcom/bytedance/msdk/api/BaiduRequestParameters;
.super Ljava/lang/Object;
.source "BaiduRequestParameters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;
    }
.end annotation


# static fields
.field public static final ADS_TYPE_DOWNLOAD:I = 0x2

.field public static final ADS_TYPE_OPENPAGE:I = 0x1

.field public static final DOWNLOAD_APP_CONFIRM_ALWAYS:I = 0x2

.field public static final DOWNLOAD_APP_CONFIRM_CUSTOM_BY_APP:I = 0x4

.field public static final DOWNLOAD_APP_CONFIRM_NEVER:I = 0x3

.field public static final DOWNLOAD_APP_CONFIRM_ONLY_MOBILE:I = 0x1

.field public static final MAX_ASSETS_RESERVED:I = 0xf


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method private constructor <init>(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->d:I

    .line 4
    iput v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->e:I

    .line 5
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->a(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->a:Ljava/lang/String;

    .line 6
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->b(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->d:I

    .line 7
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->c(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->e:I

    .line 8
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->d(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->b:Z

    .line 9
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->e(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->f:I

    .line 10
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;->f(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters;->setExtras(Ljava/util/Map;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;Lcom/bytedance/msdk/api/BaiduRequestParameters$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/msdk/api/BaiduRequestParameters;-><init>(Lcom/bytedance/msdk/api/BaiduRequestParameters$Builder;)V

    return-void
.end method


# virtual methods
.method public getAPPConfirmPolicy()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->f:I

    return v0
.end method

.method public getExtras()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->c:Ljava/util/Map;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->e:I

    return v0
.end method

.method public final getKeywords()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->d:I

    return v0
.end method

.method public isConfirmDownloading()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->b:Z

    return v0
.end method

.method public setExtras(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->c:Ljava/util/Map;

    return-void
.end method

.method public toHashMap()Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2
    iget-object v1, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->a:Ljava/lang/String;

    const-string v2, "mKeywords"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iget-boolean v1, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "confirmDownloading"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 5
    iget-object v2, p0, Lcom/bytedance/msdk/api/BaiduRequestParameters;->c:Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 6
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 8
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 9
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 10
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v2, "extras"

    .line 13
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method
