.class public Lcom/bytedance/msdk/api/AdSlot$Builder;
.super Ljava/lang/Object;
.source "AdSlot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/api/AdSlot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:I

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/String;

.field private j:I

.field private k:I

.field private l:Lcom/bytedance/msdk/api/TTVideoOption;

.field private m:Lcom/bytedance/msdk/api/TTRequestExtraParams;

.field private n:I

.field private o:I

.field private p:Lcom/bytedance/msdk/api/AdmobNativeAdOptions;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x280

    .line 4
    iput v0, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->a:I

    const/16 v0, 0x140

    .line 5
    iput v0, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->b:I

    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->c:Z

    .line 7
    iput v0, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->d:I

    const/4 v0, 0x2

    .line 17
    iput v0, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->n:I

    const/4 v0, 0x3

    .line 18
    iput v0, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->o:I

    return-void
.end method


# virtual methods
.method public build()Lcom/bytedance/msdk/api/AdSlot;
    .locals 2

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/AdSlot;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;-><init>(Lcom/bytedance/msdk/api/AdSlot$1;)V

    .line 2
    iget v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->d:I

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->b(Lcom/bytedance/msdk/api/AdSlot;I)I

    .line 3
    iget-boolean v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->c:Z

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->a(Lcom/bytedance/msdk/api/AdSlot;Z)Z

    .line 4
    iget v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->a:I

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->f(Lcom/bytedance/msdk/api/AdSlot;I)I

    .line 5
    iget v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->b:I

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->g(Lcom/bytedance/msdk/api/AdSlot;I)I

    .line 6
    iget-object v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Ljava/lang/String;

    .line 7
    iget v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->f:I

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->h(Lcom/bytedance/msdk/api/AdSlot;I)I

    .line 8
    iget-object v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->b(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Ljava/lang/String;

    .line 9
    iget-object v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->h:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/util/Map;)Ljava/util/Map;

    .line 10
    iget-object v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->c(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Ljava/lang/String;

    .line 11
    iget v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->j:I

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->a(Lcom/bytedance/msdk/api/AdSlot;I)I

    .line 12
    iget v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->k:I

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->c(Lcom/bytedance/msdk/api/AdSlot;I)I

    .line 13
    iget v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->n:I

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->d(Lcom/bytedance/msdk/api/AdSlot;I)I

    .line 14
    iget-object v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->l:Lcom/bytedance/msdk/api/TTVideoOption;

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/api/TTVideoOption;

    .line 15
    iget-object v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->m:Lcom/bytedance/msdk/api/TTRequestExtraParams;

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/TTRequestExtraParams;)Lcom/bytedance/msdk/api/TTRequestExtraParams;

    .line 16
    iget-object v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->p:Lcom/bytedance/msdk/api/AdmobNativeAdOptions;

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/AdmobNativeAdOptions;)Lcom/bytedance/msdk/api/AdmobNativeAdOptions;

    .line 17
    iget v1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->o:I

    invoke-static {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->e(Lcom/bytedance/msdk/api/AdSlot;I)I

    return-object v0
.end method

.method public setAdCount(I)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->d:I

    return-object p0
.end method

.method public setAdStyleType(I)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->n:I

    return-object p0
.end method

.method public setAdType(I)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->k:I

    return-object p0
.end method

.method public setAdmobNativeAdOptions(Lcom/bytedance/msdk/api/AdmobNativeAdOptions;)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->p:Lcom/bytedance/msdk/api/AdmobNativeAdOptions;

    return-object p0
.end method

.method public setBannerSize(I)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->o:I

    return-object p0
.end method

.method public setCustomData(Ljava/util/Map;)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/bytedance/msdk/api/AdSlot$Builder;"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->h:Ljava/util/Map;

    return-object p0
.end method

.method public setImageAdSize(II)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->a:I

    .line 2
    iput p2, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->b:I

    return-object p0
.end method

.method public setMediaExtra(Ljava/lang/String;)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->g:Ljava/lang/String;

    return-object p0
.end method

.method public setOrientation(I)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->j:I

    return-object p0
.end method

.method public setRewardAmount(I)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->f:I

    return-object p0
.end method

.method public setRewardName(Ljava/lang/String;)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->e:Ljava/lang/String;

    return-object p0
.end method

.method public setSupportDeepLink(Z)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->c:Z

    return-object p0
.end method

.method public setTTRequestExtraParams(Lcom/bytedance/msdk/api/TTRequestExtraParams;)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->m:Lcom/bytedance/msdk/api/TTRequestExtraParams;

    return-object p0
.end method

.method public setTTVideoOption(Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->l:Lcom/bytedance/msdk/api/TTVideoOption;

    return-object p0
.end method

.method public setUserID(Ljava/lang/String;)Lcom/bytedance/msdk/api/AdSlot$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/AdSlot$Builder;->i:Ljava/lang/String;

    return-object p0
.end method
