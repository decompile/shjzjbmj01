.class public Lcom/bytedance/msdk/api/banner/TTBannerView;
.super Lcom/bytedance/msdk/api/base/TTLoadBase;
.source "TTBannerView.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;-><init>()V

    .line 2
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    invoke-direct {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->m()V

    :cond_0
    return-void
.end method

.method public getAdNetworkPlatformId()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;->hasPlatFormPermission()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x3

    return v0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->n()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, -0x2

    return v0
.end method

.method public getAdNetworkRitId()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;->hasPlatFormPermission()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "-3"

    return-object v0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "-2"

    return-object v0
.end method

.method public getBannerView()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->x()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPreEcpm()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/base/TTLoadBase;->hasPlatFormPermission()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "-3"

    return-object v0

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "-2"

    return-object v0
.end method

.method public loadAd(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;)V

    :cond_0
    return-void
.end method

.method public setAllowShowCloseBtn(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->b(Z)V

    :cond_0
    return-void
.end method

.method public setRefreshTime(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->b(I)V

    :cond_0
    return-void
.end method

.method public setTTAdBannerListener(Lcom/bytedance/msdk/api/banner/TTAdBannerListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/banner/TTBannerView;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->a(Lcom/bytedance/msdk/api/banner/TTAdBannerListener;)V

    :cond_0
    return-void
.end method
