.class public interface abstract Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;
.super Ljava/lang/Object;
.source "TTNativeAd.java"


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract getActionText()Ljava/lang/String;
.end method

.method public abstract getAdImageMode()I
.end method

.method public abstract getAdLogoView()Landroid/view/View;
.end method

.method public abstract getAdNetworkPlatformId()I
.end method

.method public abstract getAdNetworkRitId()Ljava/lang/String;
.end method

.method public abstract getDescription()Ljava/lang/String;
.end method

.method public abstract getDislikeDialog(Landroid/app/Activity;)Lcom/bytedance/msdk/api/TTAdDislike;
.end method

.method public abstract getExpressView()Landroid/view/View;
.end method

.method public abstract getIconUrl()Ljava/lang/String;
.end method

.method public abstract getImageHeight()I
.end method

.method public abstract getImageList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getImageUrl()Ljava/lang/String;
.end method

.method public abstract getImageWidth()I
.end method

.method public abstract getInteractionType()I
.end method

.method public abstract getPackageName()Ljava/lang/String;
.end method

.method public abstract getPreEcpm()Ljava/lang/String;
.end method

.method public abstract getSdkNumType()I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getSource()Ljava/lang/String;
.end method

.method public abstract getStarRating()D
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract hasDislike()Z
.end method

.method public abstract isExpressAd()Z
.end method

.method public abstract isServerBidding()Z
.end method

.method public abstract onPause()V
.end method

.method public abstract registerView(Landroid/view/ViewGroup;Ljava/util/List;Ljava/util/List;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;)V
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;",
            ")V"
        }
    .end annotation
.end method

.method public abstract render()V
.end method

.method public abstract resume()V
.end method

.method public abstract setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/msdk/api/TTDislikeCallback;)V
.end method

.method public abstract setTTNativeAdListener(Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;)V
.end method

.method public abstract setTTVideoListener(Lcom/bytedance/msdk/api/nativeAd/TTVideoListener;)V
.end method

.method public abstract unregisterView()V
.end method
