.class public Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;
.super Ljava/lang/Object;
.source "TTViewBinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    }
.end annotation


# instance fields
.field public final callToActionId:I

.field public final decriptionTextId:I

.field public final extras:Ljava/util/Map;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final groupImage1Id:I

.field public final groupImage2Id:I

.field public final groupImage3Id:I

.field public final iconImageId:I

.field public final layoutId:I

.field public final logoLayoutId:I

.field public final mainImageId:I

.field public final mediaViewId:I

.field public final sourceId:I

.field public final titleId:I


# direct methods
.method private constructor <init>(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)V
    .locals 1
    .param p1    # Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->a(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->layoutId:I

    .line 4
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->f(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->titleId:I

    .line 5
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->g(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->decriptionTextId:I

    .line 6
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->h(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->callToActionId:I

    .line 7
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->i(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->iconImageId:I

    .line 8
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->j(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->mainImageId:I

    .line 9
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->k(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->mediaViewId:I

    .line 10
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->l(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->sourceId:I

    .line 11
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->m(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->extras:Ljava/util/Map;

    .line 12
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->b(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->groupImage1Id:I

    .line 13
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->c(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->groupImage2Id:I

    .line 14
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->d(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->groupImage3Id:I

    .line 15
    invoke-static {p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->e(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I

    move-result p1

    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;->logoLayoutId:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;-><init>(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)V

    return-void
.end method
