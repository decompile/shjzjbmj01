.class public final Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
.super Ljava/lang/Object;
.source "TTViewBinder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:Ljava/util/Map;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->m:Ljava/util/Map;

    .line 5
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->a:I

    .line 6
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->m:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->a:I

    return p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->j:I

    return p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->k:I

    return p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->l:I

    return p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->i:I

    return p0
.end method

.method static synthetic f(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->b:I

    return p0
.end method

.method static synthetic g(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->c:I

    return p0
.end method

.method static synthetic h(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->d:I

    return p0
.end method

.method static synthetic i(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->e:I

    return p0
.end method

.method static synthetic j(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->f:I

    return p0
.end method

.method static synthetic k(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->g:I

    return p0
.end method

.method static synthetic l(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->h:I

    return p0
.end method

.method static synthetic m(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->m:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public final addExtra(Ljava/lang/String;I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final addExtras(Ljava/util/Map;)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->m:Ljava/util/Map;

    return-object p0
.end method

.method public final build()Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder;-><init>(Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$1;)V

    return-object v0
.end method

.method public final callToActionId(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->d:I

    return-object p0
.end method

.method public final decriptionTextId(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->c:I

    return-object p0
.end method

.method public final groupImage1Id(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->j:I

    return-object p0
.end method

.method public final groupImage2Id(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->k:I

    return-object p0
.end method

.method public final groupImage3Id(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->l:I

    return-object p0
.end method

.method public final iconImageId(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->e:I

    return-object p0
.end method

.method public final logoLayoutId(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->i:I

    return-object p0
.end method

.method public final mainImageId(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->f:I

    return-object p0
.end method

.method public final mediaViewIdId(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->g:I

    return-object p0
.end method

.method public final sourceId(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->h:I

    return-object p0
.end method

.method public final titleId(I)Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/nativeAd/TTViewBinder$Builder;->b:I

    return-object p0
.end method
