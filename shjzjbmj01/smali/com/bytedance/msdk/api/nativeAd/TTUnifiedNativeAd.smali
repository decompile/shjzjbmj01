.class public Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;
.super Ljava/lang/Object;
.source "TTUnifiedNativeAd.java"


# instance fields
.field private a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;

    invoke-direct {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;->m()V

    :cond_0
    return-void
.end method

.method public loadAd(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/nativeAd/TTNativeAdLoadCallback;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;

    if-eqz v0, :cond_5

    .line 3
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    iget-object v1, p0, Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 5
    new-instance p1, Lcom/bytedance/msdk/api/AdError;

    const v0, 0x9c5f

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdLoadCallback;->onAdLoadedFial(Lcom/bytedance/msdk/api/AdError;)V

    return-void

    :cond_0
    if-eqz p1, :cond_4

    .line 12
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 13
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m()Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz p2, :cond_1

    .line 15
    new-instance p1, Lcom/bytedance/msdk/api/AdError;

    const v0, 0x9c63

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdLoadCallback;->onAdLoadedFial(Lcom/bytedance/msdk/api/AdError;)V

    :cond_1
    return-void

    .line 19
    :cond_2
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 21
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->p()Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz p2, :cond_3

    .line 23
    new-instance p1, Lcom/bytedance/msdk/api/AdError;

    const v0, 0x9c66

    invoke-static {v0}, Lcom/bytedance/msdk/api/AdError;->getMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/bytedance/msdk/api/AdError;-><init>(ILjava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAdLoadCallback;->onAdLoadedFial(Lcom/bytedance/msdk/api/AdError;)V

    :cond_3
    return-void

    .line 31
    :cond_4
    iget-object v0, p0, Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;

    invoke-virtual {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/d;->a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/nativeAd/TTNativeAdLoadCallback;)V

    :cond_5
    return-void
.end method
