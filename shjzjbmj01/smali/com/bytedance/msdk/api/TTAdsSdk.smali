.class public final Lcom/bytedance/msdk/api/TTAdsSdk;
.super Ljava/lang/Object;
.source "TTAdsSdk.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static configLoadSuccess()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->configLoadSuccess()Z

    move-result v0

    return v0
.end method

.method public static getSdkVersion()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->getSdkVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static initUnityForBanner(Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->initUnityForBanner(Landroid/app/Activity;)V

    return-void
.end method

.method public static initialize(Landroid/content/Context;Lcom/bytedance/msdk/api/TTAdConfig;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->initialize(Landroid/content/Context;Lcom/bytedance/msdk/api/TTAdConfig;)V

    return-void
.end method

.method public static registerConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->registerConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V

    return-void
.end method

.method public static requestPermissionIfNecessary(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->requestPermissionIfNecessary(Landroid/content/Context;)V

    return-void
.end method

.method public static setP(Z)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->setP(Z)V

    return-void
.end method

.method public static unregisterConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->unregisterConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V

    return-void
.end method

.method public static updatePangleConfig(Lcom/bytedance/msdk/api/TTAdConfig;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->updatePangleConfig(Lcom/bytedance/msdk/api/TTAdConfig;)V

    return-void
.end method

.method public static updatePanglePaid(Z)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->updatePanglePaid(Z)V

    return-void
.end method
