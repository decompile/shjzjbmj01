.class public final enum Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;
.super Ljava/lang/Enum;
.source "TTAdConstant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/api/TTAdConstant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RitScenes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CUSTOMIZE_SCENES:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum GAME_FINISH_REWARDS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum GAME_GIFT_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum GAME_MORE_OPPORTUNITIES:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum GAME_REDUCE_WAITING:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum GAME_START_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum HOME_GET_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum HOME_GET_PROPS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum HOME_GIFT_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum HOME_OPEN_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum HOME_SVIP_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field public static final enum HOME_TRY_PROPS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

.field private static final synthetic b:[Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;


# instance fields
.field final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "CUSTOMIZE_SCENES"

    const-string v2, "customize_scenes"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->CUSTOMIZE_SCENES:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 2
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "HOME_OPEN_BONUS"

    const-string v2, "home_open_bonus"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_OPEN_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 3
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "HOME_SVIP_BONUS"

    const-string v2, "home_svip_bonus"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_SVIP_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 4
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "HOME_GET_PROPS"

    const-string v2, "home_get_props"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_GET_PROPS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 5
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "HOME_TRY_PROPS"

    const-string v2, "home_try_props"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_TRY_PROPS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 6
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "HOME_GET_BONUS"

    const-string v2, "home_get_bonus"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_GET_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 7
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "HOME_GIFT_BONUS"

    const-string v2, "home_gift_bonus"

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_GIFT_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 8
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "GAME_START_BONUS"

    const-string v2, "game_start_bonus"

    const/4 v10, 0x7

    invoke-direct {v0, v1, v10, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_START_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 9
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "GAME_REDUCE_WAITING"

    const-string v2, "geme_reduce_waiting"

    const/16 v11, 0x8

    invoke-direct {v0, v1, v11, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_REDUCE_WAITING:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 10
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "GAME_MORE_OPPORTUNITIES"

    const-string v2, "game_more_opportunities"

    const/16 v12, 0x9

    invoke-direct {v0, v1, v12, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_MORE_OPPORTUNITIES:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 11
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "GAME_FINISH_REWARDS"

    const-string v2, "game_finish_rewards"

    const/16 v13, 0xa

    invoke-direct {v0, v1, v13, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_FINISH_REWARDS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    .line 12
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const-string v1, "GAME_GIFT_BONUS"

    const-string v2, "game_gift_bonus"

    const/16 v14, 0xb

    invoke-direct {v0, v1, v14, v2}, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_GIFT_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    const/16 v0, 0xc

    .line 13
    new-array v0, v0, [Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->CUSTOMIZE_SCENES:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_OPEN_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_SVIP_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_GET_PROPS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v6

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_TRY_PROPS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v7

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_GET_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v8

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->HOME_GIFT_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v9

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_START_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v10

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_REDUCE_WAITING:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v11

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_MORE_OPPORTUNITIES:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v12

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_FINISH_REWARDS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v13

    sget-object v1, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->GAME_GIFT_BONUS:Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    aput-object v1, v0, v14

    sput-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->b:[Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput-object p3, p0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->a:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;
    .locals 1

    .line 1
    const-class v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    return-object p0
.end method

.method public static values()[Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;
    .locals 1

    .line 1
    sget-object v0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->b:[Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    invoke-virtual {v0}, [Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;

    return-object v0
.end method


# virtual methods
.method public getScenesName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConstant$RitScenes;->a:Ljava/lang/String;

    return-object v0
.end method
