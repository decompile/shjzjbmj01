.class public Lcom/bytedance/msdk/api/BaiduExtraOptions;
.super Ljava/lang/Object;
.source "BaiduExtraOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;
    }
.end annotation


# static fields
.field public static final DOWNLOAD_APP_CONFIRM_ALWAYS:I = 0x2

.field public static final DOWNLOAD_APP_CONFIRM_CUSTOM_BY_APP:I = 0x4

.field public static final DOWNLOAD_APP_CONFIRM_NEVER:I = 0x3

.field public static final DOWNLOAD_APP_CONFIRM_ONLY_MOBILE:I = 0x1


# instance fields
.field private a:Z

.field private b:I

.field private c:Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

.field private d:Lcom/bytedance/msdk/api/BaiduRequestParameters;

.field private e:Lcom/bytedance/msdk/api/BaiduSplashParams;


# direct methods
.method private constructor <init>(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->a(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->a:Z

    .line 4
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->b(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->b:I

    .line 5
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->c(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->c:Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    .line 6
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->d(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)Lcom/bytedance/msdk/api/BaiduRequestParameters;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->d:Lcom/bytedance/msdk/api/BaiduRequestParameters;

    .line 7
    invoke-static {p1}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->e(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)Lcom/bytedance/msdk/api/BaiduSplashParams;

    move-result-object p1

    iput-object p1, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->e:Lcom/bytedance/msdk/api/BaiduSplashParams;

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;Lcom/bytedance/msdk/api/BaiduExtraOptions$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/msdk/api/BaiduExtraOptions;-><init>(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)V

    return-void
.end method


# virtual methods
.method public getBaiduNativeSmartOptStyleParams()Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->c:Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    return-object v0
.end method

.method public getBaiduRequestParameters()Lcom/bytedance/msdk/api/BaiduRequestParameters;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->d:Lcom/bytedance/msdk/api/BaiduRequestParameters;

    return-object v0
.end method

.method public getBaiduSplashParams()Lcom/bytedance/msdk/api/BaiduSplashParams;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->e:Lcom/bytedance/msdk/api/BaiduSplashParams;

    return-object v0
.end method

.method public getDownloadAppConfirmPolicy()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->b:I

    return v0
.end method

.method public isCacheVideoOnlyWifi()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions;->a:Z

    return v0
.end method
