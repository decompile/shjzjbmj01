.class public final Lcom/bytedance/msdk/api/TTVideoOption$Builder;
.super Ljava/lang/Object;
.source "TTVideoOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/api/TTVideoOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private a:Z

.field private b:F

.field private c:Lcom/bytedance/msdk/api/GDTExtraOption;

.field private d:Z

.field private e:Lcom/bytedance/msdk/api/BaiduExtraOptions;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->a:Z

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->a:Z

    return p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->b:F

    return p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)Lcom/bytedance/msdk/api/GDTExtraOption;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->c:Lcom/bytedance/msdk/api/GDTExtraOption;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->d:Z

    return p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/api/TTVideoOption$Builder;)Lcom/bytedance/msdk/api/BaiduExtraOptions;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->e:Lcom/bytedance/msdk/api/BaiduExtraOptions;

    return-object p0
.end method


# virtual methods
.method public final build()Lcom/bytedance/msdk/api/TTVideoOption;
    .locals 2

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/TTVideoOption;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/msdk/api/TTVideoOption;-><init>(Lcom/bytedance/msdk/api/TTVideoOption$Builder;Lcom/bytedance/msdk/api/TTVideoOption$1;)V

    return-object v0
.end method

.method public setAdmobAppVolume(F)Lcom/bytedance/msdk/api/TTVideoOption$Builder;
    .locals 3

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v0

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    cmpg-float v0, p1, v2

    if-gez v0, :cond_1

    const/4 p1, 0x0

    .line 1
    :cond_1
    :goto_0
    iput p1, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->b:F

    return-object p0
.end method

.method public setBaiduExtraOption(Lcom/bytedance/msdk/api/BaiduExtraOptions;)Lcom/bytedance/msdk/api/TTVideoOption$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->e:Lcom/bytedance/msdk/api/BaiduExtraOptions;

    return-object p0
.end method

.method public setGDTExtraOption(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/bytedance/msdk/api/TTVideoOption$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->c:Lcom/bytedance/msdk/api/GDTExtraOption;

    return-object p0
.end method

.method public final setMuted(Z)Lcom/bytedance/msdk/api/TTVideoOption$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->a:Z

    return-object p0
.end method

.method public final useSurfaceView(Z)Lcom/bytedance/msdk/api/TTVideoOption$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->d:Z

    return-object p0
.end method
