.class public final Lcom/bytedance/msdk/api/TTAdConfig;
.super Ljava/lang/Object;
.source "TTAdConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bytedance/msdk/api/TTAdConfig$Builder;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:I

.field private h:[Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:[I

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/bytedance/sdk/openadsdk/TTCustomController;

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->c:Z

    .line 4
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->d:Z

    const/4 v1, 0x0

    .line 5
    iput-object v1, p0, Lcom/bytedance/msdk/api/TTAdConfig;->e:Ljava/lang/String;

    .line 8
    iput v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->g:I

    const/4 v1, 0x1

    .line 12
    iput-boolean v1, p0, Lcom/bytedance/msdk/api/TTAdConfig;->i:Z

    .line 14
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->j:Z

    .line 18
    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->l:Z

    .line 25
    iput-boolean v1, p0, Lcom/bytedance/msdk/api/TTAdConfig;->p:Z

    .line 36
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->a(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->a:Ljava/lang/String;

    .line 37
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->b(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->b:Ljava/lang/String;

    .line 38
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->m(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->c:Z

    .line 39
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->n(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->d:Z

    .line 40
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->o(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->e:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->p(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->f:Z

    .line 42
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->q(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)I

    move-result v0

    iput v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->g:I

    .line 43
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->r(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->h:[Ljava/lang/String;

    .line 44
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->s(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->i:Z

    .line 45
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->t(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->j:Z

    .line 46
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->c(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->k:[I

    .line 47
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->d(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->l:Z

    .line 48
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->e(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->m:Ljava/lang/String;

    .line 49
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->f(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->n:Ljava/util/Map;

    .line 50
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->g(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Lcom/bytedance/sdk/openadsdk/TTCustomController;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->o:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    .line 51
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->h(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->q:Ljava/lang/String;

    .line 52
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->i(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->r:Ljava/util/Set;

    .line 53
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->j(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->s:Ljava/util/Map;

    .line 54
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->k(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->t:Ljava/util/Map;

    .line 55
    invoke-static {p1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->l(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/bytedance/msdk/api/TTAdConfig;->p:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/bytedance/msdk/api/TTAdConfig$Builder;Lcom/bytedance/msdk/api/TTAdConfig$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/bytedance/msdk/api/TTAdConfig;-><init>(Lcom/bytedance/msdk/api/TTAdConfig$Builder;)V

    return-void
.end method


# virtual methods
.method public allowBaiduSdkReadDeviceId()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->p:Z

    return v0
.end method

.method public getAdapterConfigurationClasses()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->r:Ljava/util/Set;

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->n:Ljava/util/Map;

    return-object v0
.end method

.method public getMediatedNetworkConfigurations()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->s:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getPangleCustomController()Lcom/bytedance/sdk/openadsdk/TTCustomController;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->o:Lcom/bytedance/sdk/openadsdk/TTCustomController;

    return-object v0
.end method

.method public getPangleData()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getPangleDirectDownloadNetworkType()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->k:[I

    return-object v0
.end method

.method public getPangleKeywords()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getPangleNeedClearTaskReset()[Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public getPangleTitleBarTheme()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->g:I

    return v0
.end method

.method public getPublisherDid()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestOptions()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->t:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public isDebug()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->c:Z

    return v0
.end method

.method public isOpenAdnTest()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->f:Z

    return v0
.end method

.method public isPangleAllowShowNotify()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->i:Z

    return v0
.end method

.method public isPangleAllowShowPageWhenScreenLock()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->j:Z

    return v0
.end method

.method public isPanglePaid()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->d:Z

    return v0
.end method

.method public isPangleUseTextureView()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/bytedance/msdk/api/TTAdConfig;->l:Z

    return v0
.end method
