.class public final Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;
.super Ljava/lang/Object;
.source "BaiduExtraOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bytedance/msdk/api/BaiduExtraOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private a:Z

.field private b:I

.field private c:Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

.field private d:Lcom/bytedance/msdk/api/BaiduRequestParameters;

.field private e:Lcom/bytedance/msdk/api/BaiduSplashParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->a:Z

    return p0
.end method

.method static synthetic b(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->b:I

    return p0
.end method

.method static synthetic c(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->c:Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    return-object p0
.end method

.method static synthetic d(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)Lcom/bytedance/msdk/api/BaiduRequestParameters;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->d:Lcom/bytedance/msdk/api/BaiduRequestParameters;

    return-object p0
.end method

.method static synthetic e(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;)Lcom/bytedance/msdk/api/BaiduSplashParams;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->e:Lcom/bytedance/msdk/api/BaiduSplashParams;

    return-object p0
.end method


# virtual methods
.method public final build()Lcom/bytedance/msdk/api/BaiduExtraOptions;
    .locals 2

    .line 1
    new-instance v0, Lcom/bytedance/msdk/api/BaiduExtraOptions;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/bytedance/msdk/api/BaiduExtraOptions;-><init>(Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;Lcom/bytedance/msdk/api/BaiduExtraOptions$1;)V

    return-object v0
.end method

.method public setBaiduNativeSmartOptStyleParams(Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;)Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->c:Lcom/bytedance/msdk/api/BaiduNativeSmartOptStyleParams;

    return-object p0
.end method

.method public setBaiduRequestParameters(Lcom/bytedance/msdk/api/BaiduRequestParameters;)Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->d:Lcom/bytedance/msdk/api/BaiduRequestParameters;

    return-object p0
.end method

.method public setBaiduSplashParams(Lcom/bytedance/msdk/api/BaiduSplashParams;)Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->e:Lcom/bytedance/msdk/api/BaiduSplashParams;

    return-object p0
.end method

.method public setCacheVideoOnlyWifi(Z)Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->a:Z

    return-object p0
.end method

.method public setGDTExtraOption(I)Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->b:I

    return-object p0
.end method
