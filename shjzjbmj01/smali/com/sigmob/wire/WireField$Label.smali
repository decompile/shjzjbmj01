.class public final enum Lcom/sigmob/wire/WireField$Label;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sigmob/wire/WireField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Label"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/wire/WireField$Label;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sigmob/wire/WireField$Label;

.field public static final enum ONE_OF:Lcom/sigmob/wire/WireField$Label;

.field public static final enum OPTIONAL:Lcom/sigmob/wire/WireField$Label;

.field public static final enum PACKED:Lcom/sigmob/wire/WireField$Label;

.field public static final enum REPEATED:Lcom/sigmob/wire/WireField$Label;

.field public static final enum REQUIRED:Lcom/sigmob/wire/WireField$Label;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/sigmob/wire/WireField$Label;

    const-string v1, "REQUIRED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sigmob/wire/WireField$Label;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/wire/WireField$Label;->REQUIRED:Lcom/sigmob/wire/WireField$Label;

    new-instance v0, Lcom/sigmob/wire/WireField$Label;

    const-string v1, "OPTIONAL"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/sigmob/wire/WireField$Label;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/wire/WireField$Label;->OPTIONAL:Lcom/sigmob/wire/WireField$Label;

    new-instance v0, Lcom/sigmob/wire/WireField$Label;

    const-string v1, "REPEATED"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/sigmob/wire/WireField$Label;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/wire/WireField$Label;->REPEATED:Lcom/sigmob/wire/WireField$Label;

    new-instance v0, Lcom/sigmob/wire/WireField$Label;

    const-string v1, "ONE_OF"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/sigmob/wire/WireField$Label;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/wire/WireField$Label;->ONE_OF:Lcom/sigmob/wire/WireField$Label;

    new-instance v0, Lcom/sigmob/wire/WireField$Label;

    const-string v1, "PACKED"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/sigmob/wire/WireField$Label;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/wire/WireField$Label;->PACKED:Lcom/sigmob/wire/WireField$Label;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sigmob/wire/WireField$Label;

    sget-object v1, Lcom/sigmob/wire/WireField$Label;->REQUIRED:Lcom/sigmob/wire/WireField$Label;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/wire/WireField$Label;->OPTIONAL:Lcom/sigmob/wire/WireField$Label;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/wire/WireField$Label;->REPEATED:Lcom/sigmob/wire/WireField$Label;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sigmob/wire/WireField$Label;->ONE_OF:Lcom/sigmob/wire/WireField$Label;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sigmob/wire/WireField$Label;->PACKED:Lcom/sigmob/wire/WireField$Label;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sigmob/wire/WireField$Label;->$VALUES:[Lcom/sigmob/wire/WireField$Label;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/wire/WireField$Label;
    .locals 1

    const-class v0, Lcom/sigmob/wire/WireField$Label;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/wire/WireField$Label;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/wire/WireField$Label;
    .locals 1

    sget-object v0, Lcom/sigmob/wire/WireField$Label;->$VALUES:[Lcom/sigmob/wire/WireField$Label;

    invoke-virtual {v0}, [Lcom/sigmob/wire/WireField$Label;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/wire/WireField$Label;

    return-object v0
.end method


# virtual methods
.method isOneOf()Z
    .locals 1

    sget-object v0, Lcom/sigmob/wire/WireField$Label;->ONE_OF:Lcom/sigmob/wire/WireField$Label;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isPacked()Z
    .locals 1

    sget-object v0, Lcom/sigmob/wire/WireField$Label;->PACKED:Lcom/sigmob/wire/WireField$Label;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isRepeated()Z
    .locals 1

    sget-object v0, Lcom/sigmob/wire/WireField$Label;->REPEATED:Lcom/sigmob/wire/WireField$Label;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/sigmob/wire/WireField$Label;->PACKED:Lcom/sigmob/wire/WireField$Label;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
