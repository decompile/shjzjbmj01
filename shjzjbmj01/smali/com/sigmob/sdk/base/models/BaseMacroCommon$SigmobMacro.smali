.class public final enum Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sigmob/sdk/base/models/BaseMacroCommon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SigmobMacro"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum _AAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _ANDROIDIDMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _ANDROIDID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _BUNDLEID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _COUNTRY_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _GAIDMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _GAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _IMEI1MD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _IMEI1_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _IMEI2MD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _IMEI2_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _IMEIMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _IMEI_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _LANGUAGE_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _MACMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _MAC_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _MC_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _MSAUDID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _OAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _OSVERSION_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _TIMEMILLIS_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _TIMESTAMP_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field public static final enum _VAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

.field private static final synthetic a:[Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_ANDROIDID_"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_ANDROIDID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_ANDROIDIDMD5_"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_ANDROIDIDMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_IMEI_"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_IMEIMD5_"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEIMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_IMEI1_"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI1_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_IMEI1MD5_"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI1MD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_IMEI2_"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI2_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_IMEI2MD5_"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI2MD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_MAC_"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_MAC_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_MACMD5_"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_MACMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_GAID_"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_GAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_GAIDMD5_"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_GAIDMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_MC_"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_MC_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_COUNTRY_"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_COUNTRY_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_TIMESTAMP_"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_TIMESTAMP_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_OSVERSION_"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_OSVERSION_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_BUNDLEID_"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_BUNDLEID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_LANGUAGE_"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_LANGUAGE_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_TIMEMILLIS_"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_TIMEMILLIS_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_OAID_"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_OAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_VAID_"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_VAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_AAID_"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_AAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    new-instance v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const-string v1, "_MSAUDID_"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_MSAUDID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_ANDROIDID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_ANDROIDIDMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEIMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI1_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI1MD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI2_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_IMEI2MD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v9

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_MAC_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_MACMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_GAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v12

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_GAIDMD5_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v13

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_MC_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    aput-object v1, v0, v14

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_COUNTRY_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_TIMESTAMP_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_OSVERSION_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_BUNDLEID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_LANGUAGE_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_TIMEMILLIS_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_OAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_VAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_AAID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->_MSAUDID_:Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sput-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->a:[Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    :try_start_0
    sget-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$1;->a:[I

    invoke-static {p0}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->valueOf(Ljava/lang/String;)Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p0, :pswitch_data_0

    const-string p0, "unFind"

    goto/16 :goto_0

    :pswitch_0
    invoke-static {}, Lcom/sigmob/sdk/common/a;->P()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_1
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->O()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_2
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->c()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_5
    invoke-static {}, Lcom/sigmob/sdk/common/a;->q()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_6
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->N()Ljava/util/Locale;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_7
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->C()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_8
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->N()Ljava/util/Locale;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_9
    invoke-static {}, Lcom/sigmob/sdk/common/a;->H()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/sigmob/sdk/common/f/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_a
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0, v0}, Lcom/sigmob/sdk/common/a;->a(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/sigmob/sdk/common/f/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_b
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0, v0}, Lcom/sigmob/sdk/common/a;->a(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_c
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0, v1}, Lcom/sigmob/sdk/common/a;->a(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/sigmob/sdk/common/f/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_d
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0, v1}, Lcom/sigmob/sdk/common/a;->a(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_e
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->h()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/sigmob/sdk/common/f/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_f
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->h()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_10
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->g()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/sigmob/sdk/common/f/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_11
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->g()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_12
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->d()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/sigmob/sdk/common/f/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_13
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->d()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_14
    invoke-static {}, Lcom/sigmob/sdk/common/a;->H()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_15
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p0

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->G()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p0

    :catch_0
    const-string p0, "unFind"

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;
    .locals 1

    const-class v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->a:[Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    invoke-virtual {v0}, [Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/sdk/base/models/BaseMacroCommon$SigmobMacro;

    return-object v0
.end method
