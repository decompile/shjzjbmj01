.class Lcom/sigmob/sdk/base/common/t;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/sdk/base/common/o;


# instance fields
.field private a:I

.field private b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

.field private c:Z

.field private d:I

.field private e:Ljava/lang/String;

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    const-string v1, ""

    iput-object v1, p0, Lcom/sigmob/sdk/base/common/t;->e:Ljava/lang/String;

    iput v0, p0, Lcom/sigmob/sdk/base/common/t;->f:I

    return-void
.end method

.method static synthetic a(Lcom/sigmob/sdk/base/common/t;)I
    .locals 0

    iget p0, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    return p0
.end method

.method private a(I)Ljava/lang/String;
    .locals 0

    if-nez p1, :cond_0

    const-string p1, "0"

    return-object p1

    :cond_0
    div-int/lit16 p1, p1, 0x3e8

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/sigmob/sdk/base/common/t;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/sigmob/sdk/base/common/t;->b(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    new-instance v1, Lcom/sigmob/sdk/base/common/t$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/sigmob/sdk/base/common/t$1;-><init>(Lcom/sigmob/sdk/base/common/t;ILjava/lang/String;)V

    invoke-static {p1, p3, v0, v1}, Lcom/sigmob/sdk/base/common/s;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Lcom/sigmob/sdk/base/common/s$a;)V

    return-void
.end method

.method static synthetic b(Lcom/sigmob/sdk/base/common/t;)I
    .locals 0

    iget p0, p0, Lcom/sigmob/sdk/base/common/t;->d:I

    return p0
.end method

.method private b(I)Ljava/lang/String;
    .locals 4

    if-nez p1, :cond_0

    const-string p1, "0"

    return-object p1

    :cond_0
    const-string v0, "%.2f"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    int-to-float p1, p1

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr p1, v3

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic b(Lcom/sigmob/sdk/base/common/t;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private b()Z
    .locals 2

    iget v0, p0, Lcom/sigmob/sdk/base/common/t;->f:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sigmob/sdk/base/common/t;->f:I

    mul-int/lit16 v0, v0, 0x3e8

    iget v1, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private c()I
    .locals 2

    iget v0, p0, Lcom/sigmob/sdk/base/common/t;->f:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sigmob/sdk/base/common/t;->f:I

    mul-int/lit16 v0, v0, 0x3e8

    iget v1, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/sigmob/sdk/base/common/t;->f:I

    mul-int/lit16 v0, v0, 0x3e8

    return v0

    :cond_0
    iget v0, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    return v0
.end method

.method static synthetic c(Lcom/sigmob/sdk/base/common/t;)I
    .locals 0

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->c()I

    move-result p0

    return p0
.end method

.method static synthetic d(Lcom/sigmob/sdk/base/common/t;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/sigmob/sdk/base/common/t;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic e(Lcom/sigmob/sdk/base/common/t;)I
    .locals 0

    iget p0, p0, Lcom/sigmob/sdk/base/common/t;->f:I

    return p0
.end method

.method static synthetic f(Lcom/sigmob/sdk/base/common/t;)Z
    .locals 0

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->b()Z

    move-result p0

    return p0
.end method

.method static synthetic g(Lcom/sigmob/sdk/base/common/t;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/sigmob/sdk/base/common/t;->c:Z

    return p0
.end method


# virtual methods
.method public a()Ljava/lang/Boolean;
    .locals 3

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->q:Lcom/sigmob/sdk/base/common/a;

    invoke-static {v0, v1}, Lcom/sigmob/sdk/base/a/c;->a(Lcom/sigmob/sdk/base/models/BaseAdUnit;Lcom/sigmob/sdk/base/common/a;)V

    const-string v0, "endcard"

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->c()I

    move-result v1

    const-string v2, "close"

    invoke-direct {p0, v0, v1, v2}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(II)Ljava/lang/Boolean;
    .locals 1

    iput p1, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getRvAdSetting()Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    move-result-object p1

    iget-object p1, p1, Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;->end_time:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/sigmob/sdk/base/common/t;->f:I

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of p2, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz p2, :cond_0

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_VIDEOTIME_"

    iget v0, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    invoke-direct {p0, v0}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public a(Landroid/content/Context;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 0

    iput-object p2, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    iput-object p3, p0, Lcom/sigmob/sdk/base/common/t;->e:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/sigmob/sdk/base/common/a;I)Ljava/lang/Boolean;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/sigmob/sdk/base/common/t$2;->a:[I

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/common/a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/common/a;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_b

    :pswitch_0
    invoke-virtual {p1}, Lcom/sigmob/sdk/base/common/a;->a()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;I)V

    goto/16 :goto_c

    :pswitch_1
    const-string p1, "play"

    const-string v1, "0.85"

    invoke-direct {p0, p1, p2, v1}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_PROGRESS_"

    const-string v1, "85"

    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->k:Lcom/sigmob/sdk/base/common/a;

    :goto_1
    invoke-static {p1, p2}, Lcom/sigmob/sdk/base/a/c;->a(Lcom/sigmob/sdk/base/models/BaseAdUnit;Lcom/sigmob/sdk/base/common/a;)V

    goto/16 :goto_c

    :pswitch_2
    const-string p1, "play"

    const-string v1, "0.75"

    invoke-direct {p0, p1, p2, v1}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_1

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_PROGRESS_"

    const-string v1, "75"

    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->j:Lcom/sigmob/sdk/base/common/a;

    goto :goto_1

    :pswitch_3
    const-string p1, "play"

    const-string v1, "0.50"

    invoke-direct {p0, p1, p2, v1}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_2

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_PROGRESS_"

    const-string v1, "50"

    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->i:Lcom/sigmob/sdk/base/common/a;

    goto :goto_1

    :pswitch_4
    const-string p1, "play"

    const-string v1, "0.25"

    invoke-direct {p0, p1, p2, v1}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_3

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_PROGRESS_"

    const-string v1, "25"

    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->e:Lcom/sigmob/sdk/base/common/a;

    goto/16 :goto_1

    :pswitch_5
    const-string p1, "play"

    const-string v1, "0"

    :goto_2
    invoke-direct {p0, p1, p2, v1}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_c

    :pswitch_6
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p1

    if-eqz p1, :cond_13

    const-string p1, "screenswitch"

    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/a;->ag()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :pswitch_7
    const-string p1, "silent"

    const-string v1, "0"

    goto :goto_2

    :pswitch_8
    const-string p1, "silent"

    const-string v1, "1"

    goto :goto_2

    :pswitch_9
    const-string p1, "vclose"

    goto/16 :goto_0

    :pswitch_a
    const-string p1, "finish"

    invoke-direct {p0, p1, p2}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;I)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    if-lez v1, :cond_4

    if-lez p2, :cond_4

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_PROGRESS_"

    mul-int/lit8 v3, p2, 0x64

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->c()I

    move-result v4

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    :goto_3
    invoke-virtual {v1, v2, v3}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_4
    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_PROGRESS_"

    const-string v3, "0"

    goto :goto_3

    :goto_4
    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_COMPLETED_"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_PLAYLASTFRAME_"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v1, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v1, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->b:Lcom/sigmob/sdk/base/common/a;

    goto/16 :goto_1

    :pswitch_b
    if-nez p2, :cond_6

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->c()I

    move-result p2

    :cond_6
    const-string p1, "complete"

    invoke-direct {p0, p1, p2}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;I)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_7

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_PROGRESS_"

    const-string v1, "100"

    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->l:Lcom/sigmob/sdk/base/common/a;

    goto/16 :goto_1

    :pswitch_c
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_9

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_SHOWSKIPTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    if-lez v1, :cond_8

    if-lez p2, :cond_8

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_PROGRESS_"

    mul-int/lit8 v3, p2, 0x64

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->c()I

    move-result v4

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    :goto_5
    invoke-virtual {v1, v2, v3}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_8
    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_PROGRESS_"

    const-string v3, "0"

    goto :goto_5

    :goto_6
    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_VIDEOTIME_"

    iget v1, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    invoke-direct {p0, v1}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->F:Lcom/sigmob/sdk/base/common/a;

    goto/16 :goto_1

    :pswitch_d
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_b

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    if-lez v1, :cond_a

    if-lez p2, :cond_a

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v1, "_PROGRESS_"

    mul-int/lit8 p2, p2, 0x64

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->c()I

    move-result v2

    div-int/2addr p2, v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v1, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    :cond_a
    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_PROGRESS_"

    const-string v1, "0"

    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    :goto_7
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->o:Lcom/sigmob/sdk/base/common/a;

    goto/16 :goto_1

    :pswitch_e
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->c:Lcom/sigmob/sdk/base/common/a;

    goto/16 :goto_1

    :pswitch_f
    if-nez p2, :cond_c

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->c()I

    move-result p2

    :cond_c
    const-string p1, "endcard"

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->d:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {v1}, Lcom/sigmob/sdk/base/common/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, p2, v1}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_e

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    if-lez v1, :cond_d

    if-lez p2, :cond_d

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v1, "_PROGRESS_"

    mul-int/lit8 p2, p2, 0x64

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->c()I

    move-result v2

    div-int/2addr p2, v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v1, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :cond_d
    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_PROGRESS_"

    const-string v1, "0"

    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    :goto_8
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->d:Lcom/sigmob/sdk/base/common/a;

    goto/16 :goto_1

    :pswitch_10
    const-string p1, "skip"

    invoke-direct {p0, p1, p2}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;I)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of v1, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz v1, :cond_10

    move-object v1, p1

    check-cast v1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v2, "_ENDTIME_"

    invoke-direct {p0, p2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    if-lez v1, :cond_f

    if-lez p2, :cond_f

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v1, "_PROGRESS_"

    mul-int/lit8 p2, p2, 0x64

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->c()I

    move-result v2

    div-int/2addr p2, v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v1, p2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    :cond_f
    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_PROGRESS_"

    const-string v1, "0"

    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    :goto_9
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->m:Lcom/sigmob/sdk/base/common/a;

    goto/16 :goto_1

    :pswitch_11
    const-string p1, "start"

    invoke-direct {p0, p1, p2, v0}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getMacroCommon()Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    move-result-object p1

    instance-of p2, p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    if-eqz p2, :cond_12

    move-object p2, p1

    check-cast p2, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v1, "_SETCLOSETIME_"

    iget v2, p0, Lcom/sigmob/sdk/base/common/t;->f:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, p1

    check-cast p2, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v1, "_ENDTIME_"

    const-string v2, "0"

    invoke-virtual {p2, v1, v2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, p1

    check-cast p2, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v1, "_VIDEOTIME_"

    iget v2, p0, Lcom/sigmob/sdk/base/common/t;->a:I

    invoke-direct {p0, v2}, Lcom/sigmob/sdk/base/common/t;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, p1

    check-cast p2, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v1, "_COMPLETED_"

    const-string v2, "0"

    invoke-virtual {p2, v1, v2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, p1

    check-cast p2, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string v1, "_PLAYFIRSTFRAME_"

    const-string v2, "0"

    invoke-virtual {p2, v1, v2}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;

    const-string p2, "_ISTRUNCATION_"

    invoke-direct {p0}, Lcom/sigmob/sdk/base/common/t;->b()Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v1, "1"

    goto :goto_a

    :cond_11
    const-string v1, "0"

    :goto_a
    invoke-virtual {p1, p2, v1}, Lcom/sigmob/sdk/base/models/RewardVideoMacroCommon;->addMarcoKey(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object p2, Lcom/sigmob/sdk/base/common/a;->a:Lcom/sigmob/sdk/base/common/a;

    goto/16 :goto_1

    :goto_b
    invoke-direct {p0, v1, p2}, Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;I)V

    iget-object p2, p0, Lcom/sigmob/sdk/base/common/t;->b:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-static {p2, p1}, Lcom/sigmob/sdk/base/a/c;->a(Lcom/sigmob/sdk/base/models/BaseAdUnit;Lcom/sigmob/sdk/base/common/a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_c

    :catch_0
    move-exception p1

    const-string p2, "recordDisplayEvent "

    invoke-static {p2, p1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_13
    :goto_c
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(ZI)Ljava/lang/Boolean;
    .locals 0

    iput-boolean p1, p0, Lcom/sigmob/sdk/base/common/t;->c:Z

    iput p2, p0, Lcom/sigmob/sdk/base/common/t;->d:I

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
