.class Lcom/sigmob/sdk/base/common/t$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/sdk/base/common/s$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sigmob/sdk/base/common/t;->a(Ljava/lang/String;ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/sigmob/sdk/base/common/t;


# direct methods
.method constructor <init>(Lcom/sigmob/sdk/base/common/t;ILjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    iput p2, p0, Lcom/sigmob/sdk/base/common/t$1;->a:I

    iput-object p3, p0, Lcom/sigmob/sdk/base/common/t$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 4

    instance-of v0, p1, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    iget-object v1, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    invoke-static {v1}, Lcom/sigmob/sdk/base/common/t;->a(Lcom/sigmob/sdk/base/common/t;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sigmob/sdk/base/common/t;->a(Lcom/sigmob/sdk/base/common/t;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;->setVtime(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    iget-object v1, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    invoke-static {v1}, Lcom/sigmob/sdk/base/common/t;->b(Lcom/sigmob/sdk/base/common/t;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sigmob/sdk/base/common/t;->a(Lcom/sigmob/sdk/base/common/t;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;->setSkip_show_time(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    iget v1, p0, Lcom/sigmob/sdk/base/common/t$1;->a:I

    invoke-static {v0, v1}, Lcom/sigmob/sdk/base/common/t;->a(Lcom/sigmob/sdk/base/common/t;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;->setCurrent_time(Ljava/lang/String;)V

    iget v0, p0, Lcom/sigmob/sdk/base/common/t$1;->a:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v0, v0, v2

    iget-object v2, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    invoke-static {v2}, Lcom/sigmob/sdk/base/common/t;->c(Lcom/sigmob/sdk/base/common/t;)I

    move-result v2

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;->setPlay_process(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t$1;->b:Ljava/lang/String;

    const-string v1, "start"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    invoke-static {v0}, Lcom/sigmob/sdk/base/common/t;->d(Lcom/sigmob/sdk/base/common/t;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;->setAd_scene(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    iget v1, p0, Lcom/sigmob/sdk/base/common/t$1;->a:I

    invoke-static {v0, v1}, Lcom/sigmob/sdk/base/common/t;->b(Lcom/sigmob/sdk/base/common/t;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;->setPlay_time(Ljava/lang/String;)V

    const-string v0, "%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    invoke-static {v3}, Lcom/sigmob/sdk/base/common/t;->e(Lcom/sigmob/sdk/base/common/t;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;->setSet_close_time(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    invoke-static {v0}, Lcom/sigmob/sdk/base/common/t;->f(Lcom/sigmob/sdk/base/common/t;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1"

    goto :goto_0

    :cond_1
    const-string v0, "0"

    :goto_0
    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;->setIs_truncation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/t$1;->c:Lcom/sigmob/sdk/base/common/t;

    invoke-static {v0}, Lcom/sigmob/sdk/base/common/t;->g(Lcom/sigmob/sdk/base/common/t;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "1"

    goto :goto_1

    :cond_2
    const-string v0, "0"

    :goto_1
    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntitySigmob;->setIs_force(Ljava/lang/String;)V

    :cond_3
    return-void
.end method
