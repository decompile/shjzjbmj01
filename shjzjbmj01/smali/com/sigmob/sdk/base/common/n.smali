.class public Lcom/sigmob/sdk/base/common/n;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/sigmob/volley/toolbox/g;

.field private static b:Lcom/sigmob/volley/toolbox/g;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/sigmob/volley/toolbox/g;
    .locals 4

    sget-object v0, Lcom/sigmob/sdk/base/common/n;->a:Lcom/sigmob/volley/toolbox/g;

    if-nez v0, :cond_1

    const-class v0, Lcom/sigmob/sdk/base/common/n;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sigmob/sdk/base/common/n;->a:Lcom/sigmob/volley/toolbox/g;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sigmob/sdk/common/e/f;->d()Lcom/sigmob/sdk/common/e/l;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/sigmob/volley/toolbox/g;

    invoke-static {}, Lcom/sigmob/sdk/common/e/f;->d()Lcom/sigmob/sdk/common/e/l;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/sigmob/volley/toolbox/g;-><init>(Lcom/sigmob/volley/n;I)V

    sput-object v1, Lcom/sigmob/sdk/base/common/n;->a:Lcom/sigmob/volley/toolbox/g;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/sigmob/sdk/base/common/n;->a:Lcom/sigmob/volley/toolbox/g;

    return-object v0
.end method

.method public static b()Lcom/sigmob/volley/toolbox/g;
    .locals 4

    sget-object v0, Lcom/sigmob/sdk/base/common/n;->b:Lcom/sigmob/volley/toolbox/g;

    if-nez v0, :cond_1

    const-class v0, Lcom/sigmob/sdk/base/common/n;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sigmob/sdk/base/common/n;->b:Lcom/sigmob/volley/toolbox/g;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sigmob/sdk/common/e/f;->e()Lcom/sigmob/sdk/common/e/l;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/sigmob/volley/toolbox/g;

    invoke-static {}, Lcom/sigmob/sdk/common/e/f;->e()Lcom/sigmob/sdk/common/e/l;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/sigmob/volley/toolbox/g;-><init>(Lcom/sigmob/volley/n;I)V

    sput-object v1, Lcom/sigmob/sdk/base/common/n;->b:Lcom/sigmob/volley/toolbox/g;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/sigmob/sdk/base/common/n;->b:Lcom/sigmob/volley/toolbox/g;

    return-object v0
.end method

.method public static c()Lcom/sigmob/volley/toolbox/g;
    .locals 4

    sget-object v0, Lcom/sigmob/sdk/base/common/n;->a:Lcom/sigmob/volley/toolbox/g;

    if-nez v0, :cond_1

    const-class v0, Lcom/sigmob/sdk/base/common/n;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sigmob/sdk/base/common/n;->a:Lcom/sigmob/volley/toolbox/g;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sigmob/sdk/common/e/f;->e()Lcom/sigmob/sdk/common/e/l;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/sigmob/volley/toolbox/g;

    invoke-static {}, Lcom/sigmob/sdk/common/e/f;->e()Lcom/sigmob/sdk/common/e/l;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/sigmob/volley/toolbox/g;-><init>(Lcom/sigmob/volley/n;I)V

    sput-object v1, Lcom/sigmob/sdk/base/common/n;->a:Lcom/sigmob/volley/toolbox/g;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/sigmob/sdk/base/common/n;->a:Lcom/sigmob/volley/toolbox/g;

    return-object v0
.end method
