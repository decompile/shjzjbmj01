.class public final enum Lcom/sigmob/sdk/base/common/j;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/sdk/base/common/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sigmob/sdk/base/common/j;

.field public static final enum b:Lcom/sigmob/sdk/base/common/j;

.field public static final enum c:Lcom/sigmob/sdk/base/common/j;

.field public static final enum d:Lcom/sigmob/sdk/base/common/j;

.field public static final enum e:Lcom/sigmob/sdk/base/common/j;

.field public static final enum f:Lcom/sigmob/sdk/base/common/j;

.field public static final enum g:Lcom/sigmob/sdk/base/common/j;

.field public static final enum h:Lcom/sigmob/sdk/base/common/j;

.field private static final synthetic j:[Lcom/sigmob/sdk/base/common/j;


# instance fields
.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    new-instance v0, Lcom/sigmob/sdk/base/common/j;

    const-string v1, "CreativeTypeVideo_Tar"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/sigmob/sdk/base/common/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/base/common/j;->a:Lcom/sigmob/sdk/base/common/j;

    new-instance v0, Lcom/sigmob/sdk/base/common/j;

    const-string v1, "CreativeTypeImage"

    const/4 v4, 0x3

    invoke-direct {v0, v1, v3, v4}, Lcom/sigmob/sdk/base/common/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/base/common/j;->b:Lcom/sigmob/sdk/base/common/j;

    new-instance v0, Lcom/sigmob/sdk/base/common/j;

    const-string v1, "CreativeTypeVideo_Html_Snippet"

    const/4 v5, 0x2

    const/4 v6, 0x4

    invoke-direct {v0, v1, v5, v6}, Lcom/sigmob/sdk/base/common/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/base/common/j;->c:Lcom/sigmob/sdk/base/common/j;

    new-instance v0, Lcom/sigmob/sdk/base/common/j;

    const-string v1, "CreativeTypeVideo_Tar_Companion"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v4, v7}, Lcom/sigmob/sdk/base/common/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/base/common/j;->d:Lcom/sigmob/sdk/base/common/j;

    new-instance v0, Lcom/sigmob/sdk/base/common/j;

    const-string v1, "CreativeTypeVideo_transparent_html"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v6, v8}, Lcom/sigmob/sdk/base/common/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/base/common/j;->e:Lcom/sigmob/sdk/base/common/j;

    new-instance v0, Lcom/sigmob/sdk/base/common/j;

    const-string v1, "CreativeTypeVideo_EndCardURL"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v7, v9}, Lcom/sigmob/sdk/base/common/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/base/common/j;->f:Lcom/sigmob/sdk/base/common/j;

    new-instance v0, Lcom/sigmob/sdk/base/common/j;

    const-string v1, "CreativeTypeSplashVideo"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v8, v10}, Lcom/sigmob/sdk/base/common/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/base/common/j;->g:Lcom/sigmob/sdk/base/common/j;

    new-instance v0, Lcom/sigmob/sdk/base/common/j;

    const-string v1, "CreativeTypeMRAID"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v9, v11}, Lcom/sigmob/sdk/base/common/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/base/common/j;->h:Lcom/sigmob/sdk/base/common/j;

    new-array v0, v10, [Lcom/sigmob/sdk/base/common/j;

    sget-object v1, Lcom/sigmob/sdk/base/common/j;->a:Lcom/sigmob/sdk/base/common/j;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/j;->b:Lcom/sigmob/sdk/base/common/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/sdk/base/common/j;->c:Lcom/sigmob/sdk/base/common/j;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sigmob/sdk/base/common/j;->d:Lcom/sigmob/sdk/base/common/j;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sigmob/sdk/base/common/j;->e:Lcom/sigmob/sdk/base/common/j;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sigmob/sdk/base/common/j;->f:Lcom/sigmob/sdk/base/common/j;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sigmob/sdk/base/common/j;->g:Lcom/sigmob/sdk/base/common/j;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sigmob/sdk/base/common/j;->h:Lcom/sigmob/sdk/base/common/j;

    aput-object v1, v0, v9

    sput-object v0, Lcom/sigmob/sdk/base/common/j;->j:[Lcom/sigmob/sdk/base/common/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sigmob/sdk/base/common/j;->i:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/sdk/base/common/j;
    .locals 1

    const-class v0, Lcom/sigmob/sdk/base/common/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/sdk/base/common/j;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/sdk/base/common/j;
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/base/common/j;->j:[Lcom/sigmob/sdk/base/common/j;

    invoke-virtual {v0}, [Lcom/sigmob/sdk/base/common/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/sdk/base/common/j;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/sigmob/sdk/base/common/j;->i:I

    return v0
.end method
