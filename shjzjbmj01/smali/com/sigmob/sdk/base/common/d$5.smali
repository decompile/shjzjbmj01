.class final Lcom/sigmob/sdk/base/common/d$5;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/sdk/common/a/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sigmob/sdk/base/common/d;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/common/a/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sigmob/sdk/common/a/d;


# direct methods
.method constructor <init>(Lcom/sigmob/sdk/common/a/d;)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/sdk/base/common/d$5;->a:Lcom/sigmob/sdk/common/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Error;)V
    .locals 1

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/d$5;->a:Lcom/sigmob/sdk/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/d$5;->a:Lcom/sigmob/sdk/common/a/d;

    invoke-interface {v0, p1}, Lcom/sigmob/sdk/common/a/d;->a(Ljava/lang/Error;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/Map;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/sigmob/sdk/base/common/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/d$5;->a:Lcom/sigmob/sdk/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/d$5;->a:Lcom/sigmob/sdk/common/a/d;

    invoke-interface {v0, p1}, Lcom/sigmob/sdk/common/a/d;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/d$5;->a:Lcom/sigmob/sdk/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/d$5;->a:Lcom/sigmob/sdk/common/a/d;

    new-instance v1, Ljava/lang/Error;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/sigmob/sdk/common/a/d;->a(Ljava/lang/Error;)V

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/sigmob/sdk/base/common/d$5;->a:Lcom/sigmob/sdk/common/a/d;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/sigmob/sdk/base/common/d$5;->a:Lcom/sigmob/sdk/common/a/d;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/sigmob/sdk/common/a/d;->a(Ljava/util/List;)V

    :cond_2
    :goto_1
    return-void
.end method
