.class public final enum Lcom/sigmob/sdk/base/common/a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/sdk/base/common/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/sigmob/sdk/base/common/a;

.field public static final enum B:Lcom/sigmob/sdk/base/common/a;

.field public static final enum C:Lcom/sigmob/sdk/base/common/a;

.field public static final enum D:Lcom/sigmob/sdk/base/common/a;

.field public static final enum E:Lcom/sigmob/sdk/base/common/a;

.field public static final enum F:Lcom/sigmob/sdk/base/common/a;

.field public static final enum G:Lcom/sigmob/sdk/base/common/a;

.field public static final enum H:Lcom/sigmob/sdk/base/common/a;

.field private static final synthetic K:[Lcom/sigmob/sdk/base/common/a;

.field public static final enum a:Lcom/sigmob/sdk/base/common/a;

.field public static final enum b:Lcom/sigmob/sdk/base/common/a;

.field public static final enum c:Lcom/sigmob/sdk/base/common/a;

.field public static final enum d:Lcom/sigmob/sdk/base/common/a;

.field public static final enum e:Lcom/sigmob/sdk/base/common/a;

.field public static final enum f:Lcom/sigmob/sdk/base/common/a;

.field public static final enum g:Lcom/sigmob/sdk/base/common/a;

.field public static final enum h:Lcom/sigmob/sdk/base/common/a;

.field public static final enum i:Lcom/sigmob/sdk/base/common/a;

.field public static final enum j:Lcom/sigmob/sdk/base/common/a;

.field public static final enum k:Lcom/sigmob/sdk/base/common/a;

.field public static final enum l:Lcom/sigmob/sdk/base/common/a;

.field public static final enum m:Lcom/sigmob/sdk/base/common/a;

.field public static final enum n:Lcom/sigmob/sdk/base/common/a;

.field public static final enum o:Lcom/sigmob/sdk/base/common/a;

.field public static final enum p:Lcom/sigmob/sdk/base/common/a;

.field public static final enum q:Lcom/sigmob/sdk/base/common/a;

.field public static final enum r:Lcom/sigmob/sdk/base/common/a;

.field public static final enum s:Lcom/sigmob/sdk/base/common/a;

.field public static final enum t:Lcom/sigmob/sdk/base/common/a;

.field public static final enum u:Lcom/sigmob/sdk/base/common/a;

.field public static final enum v:Lcom/sigmob/sdk/base/common/a;

.field public static final enum w:Lcom/sigmob/sdk/base/common/a;

.field public static final enum x:Lcom/sigmob/sdk/base/common/a;

.field public static final enum y:Lcom/sigmob/sdk/base/common/a;

.field public static final enum z:Lcom/sigmob/sdk/base/common/a;


# instance fields
.field private final I:Ljava/lang/String;

.field private final J:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_START"

    const-string v2, "start"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->a:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_FINISH"

    const-string v2, "finish"

    const/4 v5, 0x1

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->b:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_CLICK"

    const-string v2, "click"

    const/4 v6, 0x2

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->c:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_SHOW"

    const-string v2, "show"

    const/4 v7, 0x3

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->d:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_PLAY_QUARTER"

    const-string v2, "play_quarter"

    const/4 v8, 0x4

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->e:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_PLAYING"

    const-string v2, "ad_playing"

    const/4 v9, 0x5

    invoke-direct {v0, v1, v9, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->f:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_PLAY_LOAD"

    const-string v2, "ad_play_load"

    const/4 v10, 0x6

    invoke-direct {v0, v1, v10, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->g:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_PAUSE"

    const-string v2, "pause"

    const/4 v11, 0x7

    invoke-direct {v0, v1, v11, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->h:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_PLAY_TWO_QUARTERS"

    const-string v2, "play_two_quarters"

    const/16 v12, 0x8

    invoke-direct {v0, v1, v12, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->i:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_PLAY_THREE_QUARTERS"

    const-string v2, "play_three_quarters"

    const/16 v13, 0x9

    invoke-direct {v0, v1, v13, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->j:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_PLAY_COMPLETE"

    const-string v2, "play_complete"

    const/16 v14, 0xa

    invoke-direct {v0, v1, v14, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->k:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_COMPLETE"

    const-string v2, "complete"

    const/16 v15, 0xb

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->l:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_SKIP"

    const-string v2, "skip"

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->m:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_CLICK_SKIP"

    const-string v2, "click_skip"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->n:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_COMPANION_CLICK"

    const-string v2, "companion_click"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->o:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_VIDEO_CLICK"

    const-string v2, "full_video_click"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->p:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_CLOSE"

    const-string v2, "ad_close"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->q:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_VCLOSE"

    const-string v2, "ad_vclose"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->r:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_LOAD"

    const-string v2, "load"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->s:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_LOAD_SUCCESS"

    const-string v2, "load_success"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->t:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_LOAD_FAILURE"

    const-string v2, "load_failure"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->u:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_MUTE"

    const-string v2, "mute"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->v:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_UNMUTE"

    const-string v2, "unmute"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->w:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_ROTATION"

    const-string v2, "rotation"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->x:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_DOWNLOAD_START"

    const-string v2, "download_start"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->y:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_DOWNLOAD_FINISH"

    const-string v2, "download_finish"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->z:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_INSTALL_START"

    const-string v2, "install_start"

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->A:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_INSTALL_FINISH"

    const-string v2, "install_finish"

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->B:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_OPEN_DEEPLINK"

    const-string v2, "open_deeplink"

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->C:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_OPEN_DEEPLINK_FAIL"

    const-string v2, "open_deeplink_failed"

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->D:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_ERROR"

    const-string v2, "error"

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->E:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_SHOW_SKIP"

    const-string v2, "show_skip"

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->F:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_REWARD"

    const-string v2, "reward"

    const/16 v15, 0x20

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->G:Lcom/sigmob/sdk/base/common/a;

    new-instance v0, Lcom/sigmob/sdk/base/common/a;

    const-string v1, "AD_VIDEO_START"

    const-string v2, "ad_video_start"

    const/16 v15, 0x21

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/sigmob/sdk/base/common/a;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->H:Lcom/sigmob/sdk/base/common/a;

    const/16 v0, 0x22

    new-array v0, v0, [Lcom/sigmob/sdk/base/common/a;

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->a:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->b:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->c:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->d:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->e:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->f:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v9

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->g:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->h:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->i:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v12

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->j:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v13

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->k:Lcom/sigmob/sdk/base/common/a;

    aput-object v1, v0, v14

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->l:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->m:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->n:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->o:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->p:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->q:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->r:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->s:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->t:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->u:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->v:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->w:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->x:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->y:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->z:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->A:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->B:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->C:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->D:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->E:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->F:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->G:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->H:Lcom/sigmob/sdk/base/common/a;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sput-object v0, Lcom/sigmob/sdk/base/common/a;->K:[Lcom/sigmob/sdk/base/common/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/sigmob/sdk/base/common/a;->I:Ljava/lang/String;

    iput-object p4, p0, Lcom/sigmob/sdk/base/common/a;->J:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/sdk/base/common/a;
    .locals 1

    const-class v0, Lcom/sigmob/sdk/base/common/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/sdk/base/common/a;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/sdk/base/common/a;
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->K:[Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {v0}, [Lcom/sigmob/sdk/base/common/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/sdk/base/common/a;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sigmob/sdk/base/common/a;->I:Ljava/lang/String;

    return-object v0
.end method
