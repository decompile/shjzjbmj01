.class public final Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;
.super Lcom/sigmob/wire/AndroidMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$ProtoAdapter_AdSetting;,
        Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sigmob/wire/AndroidMessage<",
        "Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;",
        "Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/sigmob/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sigmob/wire/ProtoAdapter<",
            "Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RETRY_COUNT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final retry_count:Ljava/lang/Integer;
    .annotation runtime Lcom/sigmob/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;
    .annotation runtime Lcom/sigmob/wire/WireField;
        adapter = "com.sigmob.sdk.common.models.sigdsp.pb.RvAdSetting#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;
    .annotation runtime Lcom/sigmob/wire/WireField;
        adapter = "com.sigmob.sdk.common.models.sigdsp.pb.SplashAdSetting#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$ProtoAdapter_AdSetting;

    invoke-direct {v0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$ProtoAdapter_AdSetting;-><init>()V

    sput-object v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->ADAPTER:Lcom/sigmob/wire/ProtoAdapter;

    sget-object v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->ADAPTER:Lcom/sigmob/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/sigmob/wire/AndroidMessage;->newCreator(Lcom/sigmob/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->DEFAULT_RETRY_COUNT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;Ljava/lang/Integer;)V
    .locals 1

    sget-object v0, Lcom/sigmob/wire/okio/ByteString;->EMPTY:Lcom/sigmob/wire/okio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;-><init>(Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;Ljava/lang/Integer;Lcom/sigmob/wire/okio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;Ljava/lang/Integer;Lcom/sigmob/wire/okio/ByteString;)V
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->ADAPTER:Lcom/sigmob/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/sigmob/wire/AndroidMessage;-><init>(Lcom/sigmob/wire/ProtoAdapter;Lcom/sigmob/wire/okio/ByteString;)V

    iput-object p1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    iput-object p2, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;

    iput-object p3, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->retry_count:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->unknownFields()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->unknownFields()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sigmob/wire/okio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    iget-object v3, p1, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    invoke-static {v1, v3}, Lcom/sigmob/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;

    iget-object v3, p1, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;

    invoke-static {v1, v3}, Lcom/sigmob/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->retry_count:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->retry_count:Ljava/lang/Integer;

    invoke-static {v1, p1}, Lcom/sigmob/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/sigmob/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->unknownFields()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->retry_count:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->retry_count:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    iput v0, p0, Lcom/sigmob/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;
    .locals 2

    new-instance v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;

    invoke-direct {v0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;-><init>()V

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    iput-object v1, v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;->rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;

    iput-object v1, v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;->splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->retry_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;->retry_count:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->unknownFields()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;->addUnknownFields(Lcom/sigmob/wire/okio/ByteString;)Lcom/sigmob/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/sigmob/wire/Message$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->newBuilder()Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    if-eqz v1, :cond_0

    const-string v1, ", rv_setting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->rv_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/RvAdSetting;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;

    if-eqz v1, :cond_1

    const-string v1, ", splash_setting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->splash_setting:Lcom/sigmob/sdk/common/models/sigdsp/pb/SplashAdSetting;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->retry_count:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", retry_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSetting;->retry_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AdSetting{"

    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
