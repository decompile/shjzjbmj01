.class public Lcom/kwad/sdk/core/report/o;
.super Lcom/kwad/sdk/core/network/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kwad/sdk/core/report/o$a;
    }
.end annotation


# instance fields
.field b:I

.field private c:Lcom/kwad/sdk/core/response/model/AdTemplate;

.field private d:Lcom/kwad/sdk/core/report/o$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private e:Lorg/json/JSONObject;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/kwad/sdk/core/response/model/AdTemplate;ILcom/kwad/sdk/core/report/o$a;Lorg/json/JSONObject;)V
    .locals 0
    .param p1    # Lcom/kwad/sdk/core/response/model/AdTemplate;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/kwad/sdk/core/report/o$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/kwad/sdk/core/network/b;-><init>()V

    iput-object p1, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iput p2, p0, Lcom/kwad/sdk/core/report/o;->b:I

    iput-object p3, p0, Lcom/kwad/sdk/core/report/o;->d:Lcom/kwad/sdk/core/report/o$a;

    iput-object p4, p0, Lcom/kwad/sdk/core/report/o;->e:Lorg/json/JSONObject;

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/kwad/sdk/core/report/o$a;)V
    .locals 1
    .param p2    # Lcom/kwad/sdk/core/report/o$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p2, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget p1, p2, Lcom/kwad/sdk/core/report/o$a;->b:I

    if-eqz p1, :cond_1

    const-string p1, "itemClickType"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->b:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_1
    iget-object p1, p2, Lcom/kwad/sdk/core/report/o$a;->f:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    const-string p1, "payload"

    iget-object p2, p2, Lcom/kwad/sdk/core/report/o$a;->f:Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/kwad/sdk/core/report/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/kwad/sdk/core/response/model/AdTemplate;)V
    .locals 0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    iget p1, p2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mInitVoiceStatus:I

    if-eqz p1, :cond_1

    const-string p1, "initVoiceStatus"

    iget p2, p2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mInitVoiceStatus:I

    invoke-virtual {p0, p1, p2}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 3
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    :try_start_0
    const-string v0, "clientTimestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/Throwable;)V

    :goto_0
    const-string v0, "extData"

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/kwad/sdk/core/report/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;Lcom/kwad/sdk/core/report/o$a;)V
    .locals 1
    .param p2    # Lcom/kwad/sdk/core/report/o$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p2, :cond_c

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto/16 :goto_0

    :cond_0
    iget p1, p2, Lcom/kwad/sdk/core/report/o$a;->c:I

    if-eqz p1, :cond_1

    const-string p1, "itemCloseType"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->c:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_1
    iget p1, p2, Lcom/kwad/sdk/core/report/o$a;->a:I

    if-lez p1, :cond_2

    const-string p1, "photoPlaySecond"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->a:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_2
    iget p1, p2, Lcom/kwad/sdk/core/report/o$a;->d:I

    if-eqz p1, :cond_3

    const-string p1, "elementType"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->d:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_3
    iget-object p1, p2, Lcom/kwad/sdk/core/report/o$a;->f:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    const-string p1, "payload"

    iget-object v0, p2, Lcom/kwad/sdk/core/report/o$a;->f:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget p1, p2, Lcom/kwad/sdk/core/report/o$a;->g:I

    if-lez p1, :cond_5

    const-string p1, "deeplinkType"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->g:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_5
    iget p1, p2, Lcom/kwad/sdk/core/report/o$a;->h:I

    if-lez p1, :cond_6

    const-string p1, "download_source"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->h:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_6
    const-string p1, "is_package_changed"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->i:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    const-string p1, "installed_from"

    iget-object v0, p2, Lcom/kwad/sdk/core/report/o$a;->j:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "isChangedEndcard"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->l:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    iget-object p1, p2, Lcom/kwad/sdk/core/report/o$a;->k:Ljava/lang/String;

    if-eqz p1, :cond_7

    const-string p1, "downloadFailedReason"

    iget-object v0, p2, Lcom/kwad/sdk/core/report/o$a;->k:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object p1, p2, Lcom/kwad/sdk/core/report/o$a;->n:Ljava/lang/String;

    invoke-static {p1}, Lcom/kwad/sdk/utils/ab;->a(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_8

    const-string p1, "installedPackageName"

    iget-object v0, p2, Lcom/kwad/sdk/core/report/o$a;->n:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object p1, p2, Lcom/kwad/sdk/core/report/o$a;->m:Ljava/lang/String;

    invoke-static {p1}, Lcom/kwad/sdk/utils/ab;->a(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_9

    const-string p1, "serverPackageName"

    iget-object v0, p2, Lcom/kwad/sdk/core/report/o$a;->m:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget p1, p2, Lcom/kwad/sdk/core/report/o$a;->p:I

    if-lez p1, :cond_a

    const-string p1, "closeButtonClickTime"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->p:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_a
    iget p1, p2, Lcom/kwad/sdk/core/report/o$a;->o:I

    if-lez p1, :cond_b

    const-string p1, "closeButtonImpressionTime"

    iget v0, p2, Lcom/kwad/sdk/core/report/o$a;->o:I

    invoke-virtual {p0, p1, v0}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_b
    iget p1, p2, Lcom/kwad/sdk/core/report/o$a;->q:I

    if-ltz p1, :cond_c

    const-string p1, "downloadStatus"

    iget p2, p2, Lcom/kwad/sdk/core/report/o$a;->q:I

    invoke-virtual {p0, p1, p2}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;I)V

    :cond_c
    :goto_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/c;->g(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object v0

    iget v1, p0, Lcom/kwad/sdk/core/report/o;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v0, v0, Lcom/kwad/sdk/core/response/model/AdInfo;->adBaseInfo:Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;

    iget-object v0, v0, Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;->showUrl:Ljava/lang/String;

    const-string v1, "__PR__"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget v2, v2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mBidEcpm:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "__TYPE__"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget-object v2, v2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mVideoPlayerStatus:Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;

    iget v2, v2, Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;->mVideoPlayerType:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "__BEHAVIOR__"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget-object v2, v2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mVideoPlayerStatus:Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;

    iget v2, v2, Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;->mVideoPlayerBehavior:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    invoke-direct {p0, v0, v1}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;Lcom/kwad/sdk/core/response/model/AdTemplate;)V

    goto/16 :goto_1

    :cond_0
    iget v1, p0, Lcom/kwad/sdk/core/report/o;->b:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-object v0, v0, Lcom/kwad/sdk/core/response/model/AdInfo;->adBaseInfo:Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;

    iget-object v0, v0, Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;->clickUrl:Ljava/lang/String;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/o;->d:Lcom/kwad/sdk/core/report/o$a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/kwad/sdk/core/report/o;->d:Lcom/kwad/sdk/core/report/o$a;

    iget-object v1, v1, Lcom/kwad/sdk/core/report/o$a;->e:Lcom/kwad/sdk/utils/o$a;

    invoke-static {v0, v1}, Lcom/kwad/sdk/utils/o;->b(Ljava/lang/String;Lcom/kwad/sdk/utils/o$a;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-string v1, "__PR__"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget v2, v2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mBidEcpm:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "__TYPE__"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget-object v2, v2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mVideoPlayerStatus:Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;

    iget v2, v2, Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;->mVideoPlayerType:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "__BEHAVIOR__"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget-object v2, v2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mVideoPlayerStatus:Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;

    iget v2, v2, Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;->mVideoPlayerBehavior:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/kwad/sdk/core/report/o;->d:Lcom/kwad/sdk/core/report/o$a;

    invoke-direct {p0, v0, v1}, Lcom/kwad/sdk/core/report/o;->a(Ljava/lang/String;Lcom/kwad/sdk/core/report/o$a;)V

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/kwad/sdk/core/response/model/AdInfo;->adBaseInfo:Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;

    iget-object v0, v0, Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;->convUrl:Ljava/lang/String;

    const-string v1, "__ACTION__"

    iget v2, p0, Lcom/kwad/sdk/core/report/o;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "__PR__"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget v2, v2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mBidEcpm:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "__TYPE__"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget-object v2, v2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mVideoPlayerStatus:Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;

    iget v2, v2, Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;->mVideoPlayerType:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "__BEHAVIOR__"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget-object v2, v2, Lcom/kwad/sdk/core/response/model/AdTemplate;->mVideoPlayerStatus:Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;

    iget v2, v2, Lcom/kwad/sdk/core/response/model/VideoPlayerStatus;->mVideoPlayerBehavior:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/kwad/sdk/core/report/o;->d:Lcom/kwad/sdk/core/report/o$a;

    invoke-direct {p0, v0, v1}, Lcom/kwad/sdk/core/report/o;->b(Ljava/lang/String;Lcom/kwad/sdk/core/report/o$a;)V

    :goto_1
    iget-object v1, p0, Lcom/kwad/sdk/core/report/o;->e:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/kwad/sdk/core/report/o;->a(Lorg/json/JSONObject;)V

    return-object v0
.end method

.method public d()Lorg/json/JSONObject;
    .locals 1

    iget-object v0, p0, Lcom/kwad/sdk/core/report/o;->a:Lorg/json/JSONObject;

    return-object v0
.end method

.method protected e()V
    .locals 0

    return-void
.end method

.method protected f()V
    .locals 0

    return-void
.end method

.method g()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/kwad/sdk/core/report/o;->c:Lcom/kwad/sdk/core/response/model/AdTemplate;

    invoke-static {v1}, Lcom/kwad/sdk/core/response/b/c;->g(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object v1

    iget-object v2, v1, Lcom/kwad/sdk/core/response/model/AdInfo;->adTrackInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v1, v1, Lcom/kwad/sdk/core/response/model/AdInfo;->adTrackInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/kwad/sdk/core/response/model/AdInfo$AdTrackInfo;

    iget v4, v2, Lcom/kwad/sdk/core/response/model/AdInfo$AdTrackInfo;->type:I

    iget v5, p0, Lcom/kwad/sdk/core/report/o;->b:I

    if-ne v4, v5, :cond_0

    iget-object v4, v2, Lcom/kwad/sdk/core/response/model/AdInfo$AdTrackInfo;->urls:[Ljava/lang/String;

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_1
    move-object v2, v3

    :goto_0
    if-eqz v2, :cond_3

    iget v1, v2, Lcom/kwad/sdk/core/response/model/AdInfo$AdTrackInfo;->type:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/kwad/sdk/core/report/o;->d:Lcom/kwad/sdk/core/report/o$a;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/kwad/sdk/core/report/o;->d:Lcom/kwad/sdk/core/report/o$a;

    iget-object v3, v1, Lcom/kwad/sdk/core/report/o$a;->e:Lcom/kwad/sdk/utils/o$a;

    :cond_2
    iget-object v1, v2, Lcom/kwad/sdk/core/response/model/AdInfo$AdTrackInfo;->urls:[Ljava/lang/String;

    array-length v2, v1

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_3

    aget-object v5, v1, v4

    invoke-static {v5, v3}, Lcom/kwad/sdk/utils/o;->a(Ljava/lang/String;Lcom/kwad/sdk/utils/o$a;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    return-object v0
.end method
