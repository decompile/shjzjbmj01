.class public Lcom/kwad/sdk/core/report/ReportAction;
.super Lcom/kwad/sdk/core/report/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;,
        Lcom/kwad/sdk/core/report/ReportAction$b;,
        Lcom/kwad/sdk/core/report/ReportAction$a;
    }
.end annotation


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:I

.field public E:I

.field public F:J

.field public G:Lorg/json/JSONArray;

.field public H:Z

.field private I:J

.field private J:Ljava/lang/String;

.field private K:I

.field private L:J

.field private M:J

.field private N:J

.field private O:Lorg/json/JSONObject;

.field private P:Lorg/json/JSONObject;

.field private Q:J

.field private R:I

.field private S:I

.field private T:J

.field private U:J

.field private V:J

.field private W:J

.field private X:J

.field private Y:J

.field private Z:J

.field private aA:Lorg/json/JSONArray;

.field private aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

.field private aC:Lorg/json/JSONArray;

.field private aa:I

.field private ab:I

.field private ac:Ljava/lang/String;

.field private ad:J

.field private ae:I

.field private af:I

.field private ag:I

.field private ah:Lorg/json/JSONArray;

.field private ai:Lorg/json/JSONArray;

.field private aj:Ljava/lang/String;

.field private ak:Ljava/lang/String;

.field private al:Ljava/lang/String;

.field private am:J

.field private an:J

.field private ao:I

.field private ap:Ljava/lang/String;

.field private aq:Lcom/kwad/sdk/core/report/ReportAction$a;

.field private ar:I

.field private as:Ljava/lang/String;

.field private at:J

.field private au:Ljava/lang/String;

.field private av:Ljava/lang/String;

.field private aw:J

.field private ax:I

.field private ay:I

.field private az:I

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:J

.field public h:J

.field public i:Ljava/lang/String;

.field public j:I

.field public k:Lcom/kwad/sdk/core/scene/URLPackage;

.field public l:Lcom/kwad/sdk/core/scene/URLPackage;

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:J

.field public t:Ljava/lang/String;

.field public u:I

.field public v:I

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Lorg/json/JSONObject;

.field public z:J


# direct methods
.method constructor <init>(Lcom/kwad/sdk/core/report/a;)V
    .locals 7
    .param p1    # Lcom/kwad/sdk/core/report/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/kwad/sdk/core/report/d;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->R:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:I

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:J

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aa:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ab:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ag:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    new-instance v4, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;-><init>(Lcom/kwad/sdk/core/report/ReportAction$1;)V

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->v:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->D:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->E:I

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:J

    const/4 v4, 0x3

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->ar:I

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->c:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->M:J

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->a:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->I:J

    invoke-static {}, Lcom/kwad/sdk/core/report/m;->b()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->J:Ljava/lang/String;

    invoke-static {}, Lcom/kwad/sdk/core/report/m;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:J

    invoke-static {}, Lcom/kwad/sdk/core/report/m;->e()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->L:J

    invoke-virtual {p1}, Lcom/kwad/sdk/core/report/a;->a()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->d:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->W:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->e:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->X:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->f:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->Y:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->g:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->Z:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->i:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->d:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->l:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->e:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->m:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->f:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->n:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->j:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->h:J

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->o:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->aa:I

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->p:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->ab:I

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->q:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->ae:I

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->r:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->af:I

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->s:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->ag:I

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->t:Lorg/json/JSONArray;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->ah:Lorg/json/JSONArray;

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->u:Lorg/json/JSONArray;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->ai:Lorg/json/JSONArray;

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->v:Ljava/lang/String;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->aj:Ljava/lang/String;

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->A:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:I

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->x:Ljava/lang/String;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->al:Ljava/lang/String;

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->y:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->am:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->z:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->an:J

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->h:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->c:J

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->B:Ljava/lang/String;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->i:Ljava/lang/String;

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->G:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->j:I

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->J:Lorg/json/JSONArray;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->aA:Lorg/json/JSONArray;

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->N:Ljava/lang/String;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->t:Ljava/lang/String;

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->O:Ljava/lang/String;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->x:Ljava/lang/String;

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->Q:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->v:I

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->P:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->u:I

    iget-wide v4, p1, Lcom/kwad/sdk/core/report/a;->R:J

    iput-wide v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->z:J

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->K:Lorg/json/JSONArray;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->aC:Lorg/json/JSONArray;

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->L:Lorg/json/JSONObject;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->y:Lorg/json/JSONObject;

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->U:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->C:I

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->V:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->D:I

    iget v4, p1, Lcom/kwad/sdk/core/report/a;->W:I

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->E:I

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->S:Ljava/lang/String;

    iput-object v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->w:Ljava/lang/String;

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    if-eqz v4, :cond_0

    :goto_0
    invoke-virtual {v4}, Lcom/kwad/sdk/internal/api/SceneImpl;->getPosId()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->Q:J

    invoke-virtual {v4}, Lcom/kwad/sdk/internal/api/SceneImpl;->getUrlPackage()Lcom/kwad/sdk/core/scene/URLPackage;

    move-result-object v5

    iput-object v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    invoke-virtual {v4}, Lcom/kwad/sdk/internal/api/SceneImpl;->getAdStyle()I

    move-result v4

    iput v4, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:I

    goto :goto_1

    :cond_0
    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->a:Lcom/kwad/sdk/core/response/model/AdTemplate;

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->a:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget-object v4, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mAdScene:Lcom/kwad/sdk/internal/api/SceneImpl;

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->a:Lcom/kwad/sdk/core/response/model/AdTemplate;

    iget-object v4, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mAdScene:Lcom/kwad/sdk/internal/api/SceneImpl;

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v4, p1, Lcom/kwad/sdk/core/report/a;->a:Lcom/kwad/sdk/core/response/model/AdTemplate;

    if-eqz v4, :cond_8

    invoke-static {v4}, Lcom/kwad/sdk/core/response/b/c;->d(Lcom/kwad/sdk/core/response/model/AdTemplate;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->N:J

    invoke-static {v4}, Lcom/kwad/sdk/core/response/b/c;->e(Lcom/kwad/sdk/core/response/model/AdTemplate;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/kwad/sdk/core/report/ReportAction;->O:Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v5

    invoke-static {v5}, Lcom/kwad/sdk/core/d/a;->b(Ljava/lang/Throwable;)V

    :cond_2
    :goto_2
    invoke-static {v4}, Lcom/kwad/sdk/core/response/b/c;->f(Lcom/kwad/sdk/core/response/model/AdTemplate;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    :try_start_1
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/kwad/sdk/core/report/ReportAction;->P:Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v5

    invoke-static {v5}, Lcom/kwad/sdk/core/d/a;->b(Ljava/lang/Throwable;)V

    :cond_3
    :goto_3
    invoke-static {v4}, Lcom/kwad/sdk/core/response/b/c;->b(Lcom/kwad/sdk/core/response/model/AdTemplate;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->Q:J

    invoke-static {v4}, Lcom/kwad/sdk/core/response/b/c;->c(Lcom/kwad/sdk/core/response/model/AdTemplate;)I

    move-result v5

    iput v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->R:I

    iget v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->realShowType:I

    iput v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:I

    iget v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v5}, Lcom/kwad/sdk/core/response/b/d;->c(Lcom/kwad/sdk/core/response/model/PhotoInfo;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->T:J

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v5}, Lcom/kwad/sdk/core/response/b/d;->b(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->V:J

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v5}, Lcom/kwad/sdk/core/response/b/d;->e(Lcom/kwad/sdk/core/response/model/PhotoInfo;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->ad:J

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v5}, Lcom/kwad/sdk/core/response/b/d;->d(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:Ljava/lang/String;

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v5}, Lcom/kwad/sdk/core/response/b/d;->g(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->as:Ljava/lang/String;

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v5}, Lcom/kwad/sdk/core/response/b/d;->f(Lcom/kwad/sdk/core/response/model/PhotoInfo;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->at:J

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v5}, Lcom/kwad/sdk/core/response/b/d;->h(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->au:Ljava/lang/String;

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v5}, Lcom/kwad/sdk/core/response/b/d;->i(Lcom/kwad/sdk/core/response/model/PhotoInfo;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->aw:J

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->photoInfo:Lcom/kwad/sdk/core/response/model/PhotoInfo;

    invoke-static {v5}, Lcom/kwad/sdk/core/response/b/d;->j(Lcom/kwad/sdk/core/response/model/PhotoInfo;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->av:Ljava/lang/String;

    iget-object v5, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPreloadData:Lcom/kwad/sdk/core/response/model/PreloadData;

    if-eqz v5, :cond_4

    iget-object v1, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPreloadData:Lcom/kwad/sdk/core/response/model/PreloadData;

    iget-boolean v1, v1, Lcom/kwad/sdk/core/response/model/PreloadData;->isPreload:Z

    :cond_4
    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->az:I

    goto :goto_5

    :cond_5
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:I

    const/4 v5, 0x2

    if-ne v1, v5, :cond_6

    invoke-static {v4}, Lcom/kwad/sdk/core/response/b/c;->g(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/kwad/sdk/core/response/b/a;->i(Lcom/kwad/sdk/core/response/model/AdInfo;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->T:J

    iget-object v5, v1, Lcom/kwad/sdk/core/response/model/AdInfo;->adBaseInfo:Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;

    iget-wide v5, v5, Lcom/kwad/sdk/core/response/model/AdInfo$AdBaseInfo;->creativeId:J

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->s:J

    invoke-static {v1}, Lcom/kwad/sdk/core/response/b/a;->b(Lcom/kwad/sdk/core/response/model/AdInfo;)I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->V:J

    iget-object v1, v1, Lcom/kwad/sdk/core/response/model/AdInfo;->advertiserInfo:Lcom/kwad/sdk/core/response/model/AdInfo$AdvertiserInfo;

    iget-wide v5, v1, Lcom/kwad/sdk/core/response/model/AdInfo$AdvertiserInfo;->userId:J

    :goto_4
    iput-wide v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->ad:J

    goto :goto_5

    :cond_6
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:I

    const/4 v5, 0x4

    if-ne v1, v5, :cond_7

    invoke-static {v4}, Lcom/kwad/sdk/core/response/b/c;->j(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/live/mode/LiveInfo;

    move-result-object v1

    iget-object v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    invoke-static {v1}, Lcom/kwad/sdk/live/mode/a;->a(Lcom/kwad/sdk/live/mode/LiveInfo;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;->liveStreamId:Ljava/lang/String;

    iget-object v5, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    invoke-static {v1}, Lcom/kwad/sdk/live/mode/a;->c(Lcom/kwad/sdk/live/mode/LiveInfo;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;->expTag:Ljava/lang/String;

    invoke-static {v1}, Lcom/kwad/sdk/live/mode/a;->b(Lcom/kwad/sdk/live/mode/LiveInfo;)J

    move-result-wide v5

    goto :goto_4

    :cond_7
    :goto_5
    iget v1, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mMediaPlayerType:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->m:I

    iget v1, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mIsLeftSlipStatus:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ay:I

    iget v1, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPhotoResponseType:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->p:I

    iget-object v1, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPageInfo:Lcom/kwad/sdk/core/response/model/PageInfo;

    if-eqz v1, :cond_8

    iget-object v1, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mPageInfo:Lcom/kwad/sdk/core/response/model/PageInfo;

    iget v1, v1, Lcom/kwad/sdk/core/response/model/PageInfo;->pageType:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->A:I

    :cond_8
    iget-wide v5, p1, Lcom/kwad/sdk/core/report/a;->C:J

    cmp-long v1, v5, v2

    if-eqz v1, :cond_9

    iget-wide v1, p1, Lcom/kwad/sdk/core/report/a;->C:J

    iput-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aw:J

    :cond_9
    iget-object v1, p1, Lcom/kwad/sdk/core/report/a;->D:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/kwad/sdk/core/report/a;->D:Ljava/lang/String;

    iput-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->av:Ljava/lang/String;

    :cond_a
    iget v1, p1, Lcom/kwad/sdk/core/report/a;->E:I

    if-eq v1, v0, :cond_b

    iget v0, p1, Lcom/kwad/sdk/core/report/a;->E:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    :cond_b
    iget v0, p1, Lcom/kwad/sdk/core/report/a;->F:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ax:I

    iget-object v0, p1, Lcom/kwad/sdk/core/report/a;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->q:Ljava/lang/String;

    iget-object v0, p1, Lcom/kwad/sdk/core/report/a;->I:Ljava/lang/String;

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->r:Ljava/lang/String;

    invoke-static {}, Lcom/kwad/sdk/core/report/ReportAction$a;->a()Lcom/kwad/sdk/core/report/ReportAction$a;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:Lcom/kwad/sdk/core/report/ReportAction$a;

    iget v0, p1, Lcom/kwad/sdk/core/report/a;->M:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->o:I

    iget v0, p1, Lcom/kwad/sdk/core/report/a;->T:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->B:I

    iget-object v0, p1, Lcom/kwad/sdk/core/report/a;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p1, Lcom/kwad/sdk/core/report/a;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ak:Ljava/lang/String;

    :cond_c
    iget-object v0, p1, Lcom/kwad/sdk/core/report/a;->b:Lcom/kwad/sdk/internal/api/SceneImpl;

    if-nez v0, :cond_d

    if-eqz v4, :cond_d

    iget-object v0, v4, Lcom/kwad/sdk/core/response/model/AdTemplate;->mAdScene:Lcom/kwad/sdk/internal/api/SceneImpl;

    :cond_d
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/kwad/sdk/internal/api/SceneImpl;->getPosId()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->Q:J

    invoke-virtual {v0}, Lcom/kwad/sdk/internal/api/SceneImpl;->getUrlPackage()Lcom/kwad/sdk/core/scene/URLPackage;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    :cond_e
    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    if-eqz v0, :cond_f

    invoke-static {}, Lcom/kwad/sdk/core/scene/a;->a()Lcom/kwad/sdk/core/scene/a;

    move-result-object v0

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    iget-object v1, v1, Lcom/kwad/sdk/core/scene/URLPackage;->identity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/core/scene/a;->b(Ljava/lang/String;)Lcom/kwad/sdk/core/scene/EntryPackage;

    move-result-object v0

    iget-object v1, v0, Lcom/kwad/sdk/core/scene/EntryPackage;->entryPageSource:Ljava/lang/String;

    iput-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ac:Ljava/lang/String;

    iget-object v0, v0, Lcom/kwad/sdk/core/scene/EntryPackage;->entryId:Ljava/lang/String;

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ak:Ljava/lang/String;

    invoke-static {}, Lcom/kwad/sdk/core/scene/a;->a()Lcom/kwad/sdk/core/scene/a;

    move-result-object v0

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    iget-object v1, v1, Lcom/kwad/sdk/core/scene/URLPackage;->identity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/core/scene/a;->a(Ljava/lang/String;)Lcom/kwad/sdk/core/scene/URLPackage;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->l:Lcom/kwad/sdk/core/scene/URLPackage;

    :cond_f
    iget-wide v0, p1, Lcom/kwad/sdk/core/report/a;->k:J

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:J

    iget-object v0, p1, Lcom/kwad/sdk/core/report/a;->X:Lorg/json/JSONArray;

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->G:Lorg/json/JSONArray;

    iget-boolean p1, p1, Lcom/kwad/sdk/core/report/a;->Y:Z

    iput-boolean p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Lcom/kwad/sdk/core/report/d;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->R:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:I

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:J

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aa:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ab:I

    iput v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ag:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    new-instance v1, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;-><init>(Lcom/kwad/sdk/core/report/ReportAction$1;)V

    iput-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->v:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->D:I

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->E:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:J

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/kwad/sdk/core/report/ReportAction;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public static a(Lcom/kwad/sdk/core/report/a;)Lcom/kwad/sdk/core/report/ReportAction;
    .locals 1

    new-instance v0, Lcom/kwad/sdk/core/report/ReportAction;

    invoke-direct {v0, p0}, Lcom/kwad/sdk/core/report/ReportAction;-><init>(Lcom/kwad/sdk/core/report/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 4
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/kwad/sdk/core/report/d;->a(Lorg/json/JSONObject;)V

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x3

    :try_start_0
    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ar:I

    const-string v0, "actionType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->M:J

    const-string v0, "timestamp"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->I:J

    const-string v0, "sessionId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "sessionId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->J:Ljava/lang/String;

    :cond_1
    const-string v0, "seq"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:J

    const-string v0, "listId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->L:J

    const-string v0, "position"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:J

    const-string v0, "entryId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ak:Ljava/lang/String;

    const-string v0, "pushUrl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->al:Ljava/lang/String;

    const-string v0, "effectivePlayDuration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->W:J

    const-string v0, "playDuration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->X:J

    const-string v0, "startDuration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->Y:J

    const-string v0, "stayDuration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->Z:J

    const-string v0, "blockDuration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->d:J

    const-string v0, "intervalDuration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->e:J

    const-string v0, "allIntervalDuration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->f:J

    const-string v0, "flowSdk"

    const-wide/16 v1, -0x1

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:J

    const-string v0, "blockTimes"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->h:J

    const-string v0, "enterType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aa:I

    const-string v0, "leaveType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ab:I

    const-string v0, "likeStatus"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ae:I

    const-string v0, "likeType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->af:I

    const-string v0, "shareResult"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ag:I

    const-string v0, "stayLength"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->c:J

    const-string v0, "appInstalled"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "appInstalled"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ah:Lorg/json/JSONArray;

    :cond_2
    const-string v0, "appUninstalled"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "appUninstalled"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ai:Lorg/json/JSONArray;

    :cond_3
    const-string v0, "coverUrl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "coverUrl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aj:Ljava/lang/String;

    :cond_4
    const-string v0, "llsid"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->N:J

    const-string v0, "extra"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->O:Lorg/json/JSONObject;

    const-string v0, "impAdExtra"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->P:Lorg/json/JSONObject;

    const-string v0, "posId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->Q:J

    const-string v0, "adStyle"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:I

    const-string v0, "contentType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->R:I

    const-string v0, "realShowType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:I

    const-string v0, "photoId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->T:J

    const-string v0, "photoDuration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->V:J

    const-string v0, "authorId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ad:J

    const-string v0, "entryPageSource"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ac:Ljava/lang/String;

    const-string v0, "urlPackage"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/kwad/sdk/core/scene/URLPackage;

    invoke-direct {v0}, Lcom/kwad/sdk/core/scene/URLPackage;-><init>()V

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    const-string v2, "urlPackage"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/kwad/sdk/core/scene/URLPackage;->parseJson(Lorg/json/JSONObject;)V

    :cond_5
    const-string v0, "referURLPackage"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/kwad/sdk/core/scene/URLPackage;

    invoke-direct {v0}, Lcom/kwad/sdk/core/scene/URLPackage;-><init>()V

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->l:Lcom/kwad/sdk/core/scene/URLPackage;

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->l:Lcom/kwad/sdk/core/scene/URLPackage;

    const-string v2, "referURLPackage"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/kwad/sdk/core/scene/URLPackage;->parseJson(Lorg/json/JSONObject;)V

    :cond_6
    const-string v0, "commentId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->am:J

    const-string v0, "seenCount"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->an:J

    const-string v0, "clickType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:I

    const-string v0, "recoExt"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "recoExt"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:Ljava/lang/String;

    :cond_7
    const-string v0, "clientExt"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lcom/kwad/sdk/core/report/ReportAction$a;

    invoke-direct {v0}, Lcom/kwad/sdk/core/report/ReportAction$a;-><init>()V

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:Lcom/kwad/sdk/core/report/ReportAction$a;

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:Lcom/kwad/sdk/core/report/ReportAction$a;

    const-string v2, "clientExt"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/kwad/sdk/core/report/ReportAction$a;->a(Lorg/json/JSONObject;)V

    :cond_8
    const-string v0, "playerType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->m:I

    const-string v0, "tabName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->i:Ljava/lang/String;

    const-string v0, "tubeName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->as:Ljava/lang/String;

    const-string v0, "tubeId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->at:J

    const-string v0, "episodeName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->au:Ljava/lang/String;

    const-string v0, "trendId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aw:J

    const-string v0, "trendName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->av:Ljava/lang/String;

    const-string v0, "preloadType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->az:I

    const-string v0, "preloadPhotoList"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "preloadPhotoList"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aA:Lorg/json/JSONArray;

    :cond_9
    const-string v0, "closeType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ax:I

    const-string v0, "hotCompType"

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    const-string v0, "nextPageType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->j:I

    iget-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    const-string v1, "liveLogInfo"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;->parseJson(Lorg/json/JSONObject;)V

    const-string v0, "failUrl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->q:Ljava/lang/String;

    const-string v0, "errorMsg"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->r:Ljava/lang/String;

    const-string v0, "creativeId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->s:J

    const-string v0, "isLeftSlipStatus"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->ay:I

    const-string v0, "photoResponseType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->p:I

    const-string v0, "refreshType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->o:I

    const-string v0, "moduleName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->t:Ljava/lang/String;

    const-string v0, "componentPosition"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->x:Ljava/lang/String;

    const-string v0, "num"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->v:I

    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->u:I

    const-string v0, "appRunningInfoList"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->aC:Lorg/json/JSONArray;

    const-string v0, "downloadDuration"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->z:J

    const-string v0, "pageType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->A:I

    const-string v0, "guideTimes"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->B:I

    const-string v0, "speedLimitStatus"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->C:I

    const-string v0, "speedLimitThreshold"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->D:I

    const-string v0, "currentRealDownloadSpeed"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->E:I

    const-string v0, "appExt"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->y:Lorg/json/JSONObject;

    const-string v0, "cacheFailedReason"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->w:Ljava/lang/String;

    const-string v0, "timeSpend"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:J

    const-string v0, "sdkPlatform"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, p0, Lcom/kwad/sdk/core/report/ReportAction;->G:Lorg/json/JSONArray;

    const-string v0, "isKsUnion"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 6

    invoke-super {p0}, Lcom/kwad/sdk/core/report/d;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "uiType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ar:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "timestamp"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->I:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "sessionId"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->J:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "seq"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "listId"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->L:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "position"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "entryId"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ak:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "pushUrl"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->al:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "actionType"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->M:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "llsid"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->N:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->O:Lorg/json/JSONObject;

    if-eqz v1, :cond_0

    const-string v1, "extra"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->O:Lorg/json/JSONObject;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_0
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->P:Lorg/json/JSONObject;

    if-eqz v1, :cond_1

    const-string v1, "impAdExtra"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->P:Lorg/json/JSONObject;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_1
    const-string v1, "posId"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->Q:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:I

    if-lez v1, :cond_2

    const-string v1, "adStyle"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->K:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_2
    const-string v1, "contentType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->R:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "realShowType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "photoId"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->T:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "photoDuration"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->V:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "startDuration"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->Y:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "playDuration"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->X:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "stayDuration"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->Z:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "intervalDuration"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->e:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "allIntervalDuration"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->f:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "flowSdk"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "effectivePlayDuration"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->W:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "blockDuration"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->d:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "blockTimes"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->h:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "enterType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aa:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "leaveType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ab:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "entryPageSource"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ac:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    if-eqz v1, :cond_3

    const-string v1, "urlPackage"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    invoke-virtual {v2}, Lcom/kwad/sdk/core/scene/URLPackage;->toJson()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_3
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->l:Lcom/kwad/sdk/core/scene/URLPackage;

    if-eqz v1, :cond_4

    const-string v1, "referURLPackage"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->l:Lcom/kwad/sdk/core/scene/URLPackage;

    invoke-virtual {v2}, Lcom/kwad/sdk/core/scene/URLPackage;->toJson()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_4
    const-string v1, "stayLength"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->c:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "authorId"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ad:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "likeStatus"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ae:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "likeType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->af:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "shareResult"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ag:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ah:Lorg/json/JSONArray;

    if-eqz v1, :cond_5

    const-string v1, "appInstalled"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ah:Lorg/json/JSONArray;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONArray;)V

    :cond_5
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ai:Lorg/json/JSONArray;

    if-eqz v1, :cond_6

    const-string v1, "appUninstalled"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ai:Lorg/json/JSONArray;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONArray;)V

    :cond_6
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->q:Ljava/lang/String;

    invoke-static {v1}, Lcom/kwad/sdk/utils/ab;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "failUrl"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->q:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->r:Ljava/lang/String;

    invoke-static {v1}, Lcom/kwad/sdk/utils/ab;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "errorMsg"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->r:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->t:Ljava/lang/String;

    invoke-static {v1}, Lcom/kwad/sdk/utils/ab;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "moduleName"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->t:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->x:Ljava/lang/String;

    invoke-static {v1}, Lcom/kwad/sdk/utils/ab;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "componentPosition"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->x:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->v:I

    if-lez v1, :cond_b

    const-string v1, "num"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->v:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_b
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->u:I

    if-eqz v1, :cond_c

    const-string v1, "state"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->u:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_c
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->w:Ljava/lang/String;

    invoke-static {v1}, Lcom/kwad/sdk/utils/ab;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "cacheFailedReason"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->w:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    const-string v1, "coverUrl"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aj:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "commentId"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->am:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "seenCount"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->an:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "clickType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ao:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "recoExt"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ap:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:Lcom/kwad/sdk/core/report/ReportAction$a;

    if-eqz v1, :cond_e

    const-string v1, "clientExt"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aq:Lcom/kwad/sdk/core/report/ReportAction$a;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lcom/kwad/sdk/core/b;)V

    :cond_e
    const-string v1, "playerType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->m:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "tabName"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->i:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "tubeName"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->as:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "tubeId"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->at:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "episodeName"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->au:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "trendId"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aw:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "trendName"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->av:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "preloadType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->az:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aA:Lorg/json/JSONArray;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aA:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_f

    const-string v1, "preloadPhotoList"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aA:Lorg/json/JSONArray;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONArray;)V

    :cond_f
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_10

    const-string v1, "hotCompType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    :cond_10
    const-string v1, "closeType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ax:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "nextPageType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->j:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "liveLogInfo"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aB:Lcom/kwad/sdk/core/report/ReportAction$LiveLogInfo;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lcom/kwad/sdk/core/b;)V

    const-string v1, "creativeId"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->s:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "isLeftSlipStatus"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->ay:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "photoResponseType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->p:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "refreshType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->o:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "appRunningInfoList"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->aC:Lorg/json/JSONArray;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONArray;)V

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->y:Lorg/json/JSONObject;

    if-eqz v1, :cond_11

    const-string v1, "appExt"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->y:Lorg/json/JSONObject;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_11
    const-string v1, "downloadDuration"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->z:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "pageType"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->A:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "guideTimes"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->B:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "speedLimitStatus"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->C:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "speedLimitThreshold"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->D:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "currentRealDownloadSpeed"

    iget v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->E:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_12

    const-string v1, "timeSpend"

    iget-wide v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    :cond_12
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->G:Lorg/json/JSONArray;

    if-eqz v1, :cond_13

    const-string v1, "sdkPlatform"

    iget-object v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->G:Lorg/json/JSONArray;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONArray;)V

    :cond_13
    const-string v1, "isKsUnion"

    iget-boolean v2, p0, Lcom/kwad/sdk/core/report/ReportAction;->H:Z

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "[actionType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->M:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",moduleName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",componentPosition:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",tubeId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->at:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",entryId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ak:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",blockDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",blockTimes:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",intervalDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",allIntervalDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->f:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",flowSdk:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",trendId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aw:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",trendName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->av:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",tubeName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->as:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",episodeName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->au:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",seq:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",extra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->O:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v1, ",impAdExtra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->P:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v1, ",actionId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",position:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->U:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",contentType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->R:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",playerType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",preloadType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->az:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",realShowType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->S:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",photoDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->V:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",startDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->Y:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",playDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->X:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",stayDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->Z:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, ",enterType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->aa:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ",entryPageSource:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ",stayLength:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const-string v1, ",hotCompType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_0
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    if-eqz v1, :cond_1

    const-string v1, ",urlPackage:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->k:Lcom/kwad/sdk/core/scene/URLPackage;

    iget v1, v1, Lcom/kwad/sdk/core/scene/URLPackage;->page:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_1
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->l:Lcom/kwad/sdk/core/scene/URLPackage;

    if-eqz v1, :cond_2

    const-string v1, ",referPage:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->l:Lcom/kwad/sdk/core/scene/URLPackage;

    iget v1, v1, Lcom/kwad/sdk/core/scene/URLPackage;->page:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_2
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->v:I

    if-le v1, v2, :cond_3

    const-string v1, ",num:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->v:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_3
    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->u:I

    if-eqz v1, :cond_4

    const-string v1, ",state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->u:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    :cond_4
    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->y:Lorg/json/JSONObject;

    if-eqz v1, :cond_5

    const-string v1, ",appExt:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->y:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    :cond_5
    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_6

    const-string v1, ",timeSpend:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lcom/kwad/sdk/core/report/ReportAction;->F:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    :cond_6
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
