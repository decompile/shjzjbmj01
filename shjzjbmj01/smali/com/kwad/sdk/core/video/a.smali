.class public Lcom/kwad/sdk/core/video/a;
.super Landroid/view/TextureView;


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    iget v0, p0, Lcom/kwad/sdk/core/video/a;->b:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/kwad/sdk/core/video/a;->a:I

    if-eq v0, p2, :cond_0

    iput p1, p0, Lcom/kwad/sdk/core/video/a;->b:I

    iput p2, p0, Lcom/kwad/sdk/core/video/a;->a:I

    invoke-virtual {p0}, Lcom/kwad/sdk/core/video/a;->requestLayout()V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    invoke-virtual {p0}, Lcom/kwad/sdk/core/video/a;->getRotation()F

    move-result v0

    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    move v5, p2

    move p2, p1

    move p1, v5

    :cond_1
    iget v0, p0, Lcom/kwad/sdk/core/video/a;->b:I

    invoke-static {v0, p1}, Lcom/kwad/sdk/core/video/a;->getDefaultSize(II)I

    move-result v0

    iget v1, p0, Lcom/kwad/sdk/core/video/a;->a:I

    invoke-static {v1, p2}, Lcom/kwad/sdk/core/video/a;->getDefaultSize(II)I

    move-result v1

    iget v2, p0, Lcom/kwad/sdk/core/video/a;->b:I

    if-lez v2, :cond_9

    iget v2, p0, Lcom/kwad/sdk/core/video/a;->a:I

    if-lez v2, :cond_9

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v0, v2, :cond_3

    if-ne v1, v2, :cond_3

    iget v0, p0, Lcom/kwad/sdk/core/video/a;->b:I

    mul-int v0, v0, p2

    iget v1, p0, Lcom/kwad/sdk/core/video/a;->a:I

    mul-int v1, v1, p1

    if-ge v0, v1, :cond_2

    :goto_0
    iget p1, p0, Lcom/kwad/sdk/core/video/a;->b:I

    mul-int p1, p1, p2

    iget v0, p0, Lcom/kwad/sdk/core/video/a;->a:I

    div-int v0, p1, v0

    move p1, v0

    goto :goto_4

    :cond_2
    iget v0, p0, Lcom/kwad/sdk/core/video/a;->b:I

    mul-int v0, v0, p2

    iget v1, p0, Lcom/kwad/sdk/core/video/a;->a:I

    mul-int v1, v1, p1

    if-le v0, v1, :cond_a

    :goto_1
    iget p2, p0, Lcom/kwad/sdk/core/video/a;->a:I

    mul-int p2, p2, p1

    iget v0, p0, Lcom/kwad/sdk/core/video/a;->b:I

    div-int v1, p2, v0

    goto :goto_3

    :cond_3
    const/high16 v3, -0x80000000

    if-ne v0, v2, :cond_5

    iget v0, p0, Lcom/kwad/sdk/core/video/a;->a:I

    mul-int v0, v0, p1

    iget v2, p0, Lcom/kwad/sdk/core/video/a;->b:I

    div-int/2addr v0, v2

    if-ne v1, v3, :cond_4

    if-le v0, p2, :cond_4

    goto :goto_0

    :cond_4
    move p2, v0

    goto :goto_4

    :cond_5
    if-ne v1, v2, :cond_7

    iget v1, p0, Lcom/kwad/sdk/core/video/a;->b:I

    mul-int v1, v1, p2

    iget v2, p0, Lcom/kwad/sdk/core/video/a;->a:I

    div-int/2addr v1, v2

    if-ne v0, v3, :cond_6

    if-le v1, p1, :cond_6

    goto :goto_1

    :cond_6
    move p1, v1

    goto :goto_4

    :cond_7
    iget v2, p0, Lcom/kwad/sdk/core/video/a;->b:I

    iget v4, p0, Lcom/kwad/sdk/core/video/a;->a:I

    if-ne v1, v3, :cond_8

    if-le v4, p2, :cond_8

    iget v1, p0, Lcom/kwad/sdk/core/video/a;->b:I

    mul-int v1, v1, p2

    iget v2, p0, Lcom/kwad/sdk/core/video/a;->a:I

    div-int/2addr v1, v2

    goto :goto_2

    :cond_8
    move v1, v2

    move p2, v4

    :goto_2
    if-ne v0, v3, :cond_6

    if-le v1, p1, :cond_6

    goto :goto_1

    :cond_9
    move p1, v0

    :goto_3
    move p2, v1

    :cond_a
    :goto_4
    invoke-virtual {p0, p1, p2}, Lcom/kwad/sdk/core/video/a;->setMeasuredDimension(II)V

    return-void
.end method

.method public setRotation(F)V
    .locals 1

    invoke-virtual {p0}, Lcom/kwad/sdk/core/video/a;->getRotation()F

    move-result v0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/TextureView;->setRotation(F)V

    invoke-virtual {p0}, Lcom/kwad/sdk/core/video/a;->requestLayout()V

    :cond_0
    return-void
.end method
