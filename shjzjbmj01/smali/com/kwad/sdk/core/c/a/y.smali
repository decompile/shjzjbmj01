.class public Lcom/kwad/sdk/core/c/a/y;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/kwad/sdk/core/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/kwad/sdk/core/c<",
        "Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;)Lorg/json/JSONObject;
    .locals 4

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "appearTime"

    iget-wide v2, p1, Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;->appearTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "showInterval"

    iget v2, p1, Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;->showInterval:I

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    const-string v1, "showCount"

    iget p1, p1, Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;->showCount:I

    invoke-static {v0, v1, p1}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    return-object v0
.end method

.method public bridge synthetic a(Lcom/kwad/sdk/core/response/a/a;)Lorg/json/JSONObject;
    .locals 0

    check-cast p1, Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;

    invoke-virtual {p0, p1}, Lcom/kwad/sdk/core/c/a/y;->a(Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;)Lorg/json/JSONObject;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;Lorg/json/JSONObject;)V
    .locals 3

    if-nez p2, :cond_0

    return-void

    :cond_0
    const-string v0, "appearTime"

    new-instance v1, Ljava/lang/Long;

    const-string v2, "2000"

    invoke-direct {v1, v2}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p2, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;->appearTime:J

    const-string v0, "showInterval"

    new-instance v1, Ljava/lang/Integer;

    const-string v2, "3"

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;->showInterval:I

    const-string v0, "showCount"

    new-instance v1, Ljava/lang/Integer;

    const-string v2, "2"

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p2

    iput p2, p1, Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;->showCount:I

    return-void
.end method

.method public bridge synthetic a(Lcom/kwad/sdk/core/response/a/a;Lorg/json/JSONObject;)V
    .locals 0

    check-cast p1, Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;

    invoke-virtual {p0, p1, p2}, Lcom/kwad/sdk/core/c/a/y;->a(Lcom/kwad/sdk/core/config/item/ProfileGuideConfigItem$ProfileGuideConfig;Lorg/json/JSONObject;)V

    return-void
.end method
