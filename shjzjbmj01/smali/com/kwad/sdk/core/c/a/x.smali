.class public Lcom/kwad/sdk/core/c/a/x;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/kwad/sdk/core/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/kwad/sdk/core/c<",
        "Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;)Lorg/json/JSONObject;
    .locals 4

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "spreadTime"

    iget-wide v2, p1, Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;->spreadTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "isValidReturned"

    iget p1, p1, Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;->isValidReturned:I

    invoke-static {v0, v1, p1}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;I)V

    return-object v0
.end method

.method public bridge synthetic a(Lcom/kwad/sdk/core/response/a/a;)Lorg/json/JSONObject;
    .locals 0

    check-cast p1, Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;

    invoke-virtual {p0, p1}, Lcom/kwad/sdk/core/c/a/x;->a(Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;)Lorg/json/JSONObject;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;Lorg/json/JSONObject;)V
    .locals 2

    if-nez p2, :cond_0

    return-void

    :cond_0
    const-string v0, "spreadTime"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;->spreadTime:J

    const-string v0, "isValidReturned"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p2

    iput p2, p1, Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;->isValidReturned:I

    return-void
.end method

.method public bridge synthetic a(Lcom/kwad/sdk/core/response/a/a;Lorg/json/JSONObject;)V
    .locals 0

    check-cast p1, Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;

    invoke-virtual {p0, p1, p2}, Lcom/kwad/sdk/core/c/a/x;->a(Lcom/kwad/sdk/core/preload/SplashPreloadManager$PreLoadPara;Lorg/json/JSONObject;)V

    return-void
.end method
