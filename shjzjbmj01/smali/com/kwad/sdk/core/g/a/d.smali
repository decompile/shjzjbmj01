.class public Lcom/kwad/sdk/core/g/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/kwad/sdk/core/b;


# instance fields
.field private a:D

.field private b:D


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/kwad/sdk/core/g/a/d;
    .locals 4

    new-instance v0, Lcom/kwad/sdk/core/g/a/d;

    invoke-direct {v0}, Lcom/kwad/sdk/core/g/a/d;-><init>()V

    invoke-static {}, Lcom/kwad/sdk/KsAdSDKImpl;->get()Lcom/kwad/sdk/KsAdSDKImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kwad/sdk/KsAdSDKImpl;->getProxyForAdLocation()Lcom/kwad/sdk/export/proxy/AdLocationProxy;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/kwad/sdk/export/proxy/AdLocationProxy;->getLatitude()D

    move-result-wide v2

    iput-wide v2, v0, Lcom/kwad/sdk/core/g/a/d;->a:D

    invoke-interface {v1}, Lcom/kwad/sdk/export/proxy/AdLocationProxy;->getLongitude()D

    move-result-wide v1

    :goto_0
    iput-wide v1, v0, Lcom/kwad/sdk/core/g/a/d;->b:D

    goto :goto_1

    :cond_0
    invoke-static {}, Lcom/kwad/sdk/KsAdSDKImpl;->get()Lcom/kwad/sdk/KsAdSDKImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/kwad/sdk/KsAdSDKImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/kwad/sdk/utils/k;->a(Landroid/content/Context;)Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iput-wide v2, v0, Lcom/kwad/sdk/core/g/a/d;->a:D

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    goto :goto_0

    :cond_1
    :goto_1
    return-object v0
.end method


# virtual methods
.method public toJson()Lorg/json/JSONObject;
    .locals 4

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "latitude"

    iget-wide v2, p0, Lcom/kwad/sdk/core/g/a/d;->a:D

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;D)V

    const-string v1, "longitude"

    iget-wide v2, p0, Lcom/kwad/sdk/core/g/a/d;->b:D

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;D)V

    return-object v0
.end method
