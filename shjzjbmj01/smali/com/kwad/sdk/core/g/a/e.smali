.class public Lcom/kwad/sdk/core/g/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/kwad/sdk/core/b;


# instance fields
.field public a:Lcom/kwad/sdk/internal/api/SceneImpl;

.field public b:J

.field public c:J


# direct methods
.method public constructor <init>(Lcom/kwad/sdk/api/KsScene;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p1, Lcom/kwad/sdk/internal/api/SceneImpl;

    iput-object p1, p0, Lcom/kwad/sdk/core/g/a/e;->a:Lcom/kwad/sdk/internal/api/SceneImpl;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/kwad/sdk/core/g/a/e;->b:J

    iput-wide v0, p0, Lcom/kwad/sdk/core/g/a/e;->c:J

    return-void
.end method


# virtual methods
.method public toJson()Lorg/json/JSONObject;
    .locals 4

    iget-object v0, p0, Lcom/kwad/sdk/core/g/a/e;->a:Lcom/kwad/sdk/internal/api/SceneImpl;

    invoke-virtual {v0}, Lcom/kwad/sdk/internal/api/SceneImpl;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "pageScene"

    iget-wide v2, p0, Lcom/kwad/sdk/core/g/a/e;->b:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    const-string v1, "subPageScene"

    iget-wide v2, p0, Lcom/kwad/sdk/core/g/a/e;->c:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    return-object v0
.end method
