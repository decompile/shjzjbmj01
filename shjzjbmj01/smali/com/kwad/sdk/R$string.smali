.class public final Lcom/kwad/sdk/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kwad/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f080000

.field public static final abc_action_bar_up_description:I = 0x7f080001

.field public static final abc_action_menu_overflow_description:I = 0x7f080002

.field public static final abc_action_mode_done:I = 0x7f080003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f080004

.field public static final abc_activitychooserview_choose_application:I = 0x7f080005

.field public static final abc_capital_off:I = 0x7f080006

.field public static final abc_capital_on:I = 0x7f080007

.field public static final abc_search_hint:I = 0x7f080012

.field public static final abc_searchview_description_clear:I = 0x7f080013

.field public static final abc_searchview_description_query:I = 0x7f080014

.field public static final abc_searchview_description_search:I = 0x7f080015

.field public static final abc_searchview_description_submit:I = 0x7f080016

.field public static final abc_searchview_description_voice:I = 0x7f080017

.field public static final abc_shareactionprovider_share_with:I = 0x7f080018

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f080019

.field public static final abc_toolbar_collapse_description:I = 0x7f08001a

.field public static final app_name:I = 0x7f08002d

.field public static final ksad_ad_default_author:I = 0x7f080030

.field public static final ksad_ad_default_username:I = 0x7f080031

.field public static final ksad_ad_default_username_normal:I = 0x7f080032

.field public static final ksad_ad_function_disable:I = 0x7f080033

.field public static final ksad_click_to_next_video:I = 0x7f080034

.field public static final ksad_data_error_toast:I = 0x7f080035

.field public static final ksad_default_no_more_tip_or_toast_txt:I = 0x7f080036

.field public static final ksad_entry_tab_like_format:I = 0x7f080037

.field public static final ksad_half_page_loading_error_tip:I = 0x7f080038

.field public static final ksad_half_page_loading_no_comment_tip:I = 0x7f080039

.field public static final ksad_half_page_loading_no_related_tip:I = 0x7f08003a

.field public static final ksad_look_related_button:I = 0x7f08003b

.field public static final ksad_look_related_title:I = 0x7f08003c

.field public static final ksad_network_dataFlow_tip:I = 0x7f08003d

.field public static final ksad_network_error_toast:I = 0x7f08003e

.field public static final ksad_page_load_more_tip:I = 0x7f08003f

.field public static final ksad_page_load_no_more_tip:I = 0x7f080040

.field public static final ksad_page_loading_data_error_sub_title:I = 0x7f080041

.field public static final ksad_page_loading_data_error_title:I = 0x7f080042

.field public static final ksad_page_loading_data_limit_error_title:I = 0x7f080043

.field public static final ksad_page_loading_error_retry:I = 0x7f080044

.field public static final ksad_page_loading_network_error_sub_title:I = 0x7f080045

.field public static final ksad_page_loading_network_error_title:I = 0x7f080046

.field public static final ksad_photo_hot_enter_label_text:I = 0x7f080047

.field public static final ksad_photo_hot_enter_watch_count_format:I = 0x7f080048

.field public static final ksad_photo_hot_enter_watch_extra_button_format:I = 0x7f080049

.field public static final ksad_photo_hot_enter_watch_extra_button_format_v2:I = 0x7f08004a

.field public static final ksad_photo_hot_scroll_more_hot_label:I = 0x7f08004b

.field public static final ksad_reward_default_tip:I = 0x7f08004c

.field public static final ksad_reward_success_tip:I = 0x7f08004d

.field public static final ksad_slide_left_tips:I = 0x7f08004e

.field public static final ksad_slide_up_tips:I = 0x7f08004f

.field public static final ksad_text_placeholder:I = 0x7f080050

.field public static final ksad_trend_is_no_valid:I = 0x7f080051

.field public static final ksad_trend_list_item_photo_count_format:I = 0x7f080052

.field public static final ksad_trend_list_panel_title:I = 0x7f080053

.field public static final ksad_trend_title_info_format:I = 0x7f080054

.field public static final ksad_tube_author_name_label_text:I = 0x7f080055

.field public static final ksad_tube_enter_paly_count:I = 0x7f080056

.field public static final ksad_tube_episode_index:I = 0x7f080057

.field public static final ksad_tube_hot_list_label_string:I = 0x7f080058

.field public static final ksad_tube_more_episode:I = 0x7f080059

.field public static final ksad_tube_update_default:I = 0x7f08005a

.field public static final ksad_tube_update_finished_format_text:I = 0x7f08005b

.field public static final ksad_tube_update_unfinished_format_text:I = 0x7f08005c

.field public static final ksad_video_no_found:I = 0x7f08005d

.field public static final ksad_watch_next_video:I = 0x7f08005e

.field public static final search_menu_title:I = 0x7f08001b

.field public static final status_bar_notification_info_overflow:I = 0x7f08001c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
