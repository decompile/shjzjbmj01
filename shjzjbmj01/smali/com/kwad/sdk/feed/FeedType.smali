.class public final enum Lcom/kwad/sdk/feed/FeedType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/kwad/sdk/feed/FeedType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/kwad/sdk/feed/FeedType;

.field public static final enum FEED_TYPE_TEXT_ABOVE:Lcom/kwad/sdk/feed/FeedType;

.field public static final enum FEED_TYPE_TEXT_ABOVE_GROUP:Lcom/kwad/sdk/feed/FeedType;

.field public static final enum FEED_TYPE_TEXT_BELOW:Lcom/kwad/sdk/feed/FeedType;

.field public static final enum FEED_TYPE_TEXT_IMMERSE:Lcom/kwad/sdk/feed/FeedType;

.field public static final enum FEED_TYPE_TEXT_LEFT:Lcom/kwad/sdk/feed/FeedType;

.field public static final enum FEED_TYPE_TEXT_NEW:Lcom/kwad/sdk/feed/FeedType;

.field public static final enum FEED_TYPE_TEXT_RIGHT:Lcom/kwad/sdk/feed/FeedType;

.field public static final enum FEED_TYPE_UNKNOWN:Lcom/kwad/sdk/feed/FeedType;


# instance fields
.field private type:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/kwad/sdk/feed/FeedType;

    const-string v1, "FEED_TYPE_UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/kwad/sdk/feed/FeedType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_UNKNOWN:Lcom/kwad/sdk/feed/FeedType;

    new-instance v0, Lcom/kwad/sdk/feed/FeedType;

    const-string v1, "FEED_TYPE_TEXT_IMMERSE"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v3}, Lcom/kwad/sdk/feed/FeedType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_IMMERSE:Lcom/kwad/sdk/feed/FeedType;

    new-instance v0, Lcom/kwad/sdk/feed/FeedType;

    const-string v1, "FEED_TYPE_TEXT_LEFT"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4, v4}, Lcom/kwad/sdk/feed/FeedType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_LEFT:Lcom/kwad/sdk/feed/FeedType;

    new-instance v0, Lcom/kwad/sdk/feed/FeedType;

    const-string v1, "FEED_TYPE_TEXT_RIGHT"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5, v5}, Lcom/kwad/sdk/feed/FeedType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_RIGHT:Lcom/kwad/sdk/feed/FeedType;

    new-instance v0, Lcom/kwad/sdk/feed/FeedType;

    const-string v1, "FEED_TYPE_TEXT_ABOVE"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6, v6}, Lcom/kwad/sdk/feed/FeedType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_ABOVE:Lcom/kwad/sdk/feed/FeedType;

    new-instance v0, Lcom/kwad/sdk/feed/FeedType;

    const-string v1, "FEED_TYPE_TEXT_BELOW"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7, v7}, Lcom/kwad/sdk/feed/FeedType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_BELOW:Lcom/kwad/sdk/feed/FeedType;

    new-instance v0, Lcom/kwad/sdk/feed/FeedType;

    const-string v1, "FEED_TYPE_TEXT_ABOVE_GROUP"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8, v8}, Lcom/kwad/sdk/feed/FeedType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_ABOVE_GROUP:Lcom/kwad/sdk/feed/FeedType;

    new-instance v0, Lcom/kwad/sdk/feed/FeedType;

    const-string v1, "FEED_TYPE_TEXT_NEW"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9, v9}, Lcom/kwad/sdk/feed/FeedType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_NEW:Lcom/kwad/sdk/feed/FeedType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/kwad/sdk/feed/FeedType;

    sget-object v1, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_UNKNOWN:Lcom/kwad/sdk/feed/FeedType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_IMMERSE:Lcom/kwad/sdk/feed/FeedType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_LEFT:Lcom/kwad/sdk/feed/FeedType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_RIGHT:Lcom/kwad/sdk/feed/FeedType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_ABOVE:Lcom/kwad/sdk/feed/FeedType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_BELOW:Lcom/kwad/sdk/feed/FeedType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_ABOVE_GROUP:Lcom/kwad/sdk/feed/FeedType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_NEW:Lcom/kwad/sdk/feed/FeedType;

    aput-object v1, v0, v9

    sput-object v0, Lcom/kwad/sdk/feed/FeedType;->$VALUES:[Lcom/kwad/sdk/feed/FeedType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/kwad/sdk/feed/FeedType;->type:I

    return-void
.end method

.method public static checkTypeValid(Lcom/kwad/sdk/core/response/model/AdTemplate;)Z
    .locals 4
    .param p0    # Lcom/kwad/sdk/core/response/model/AdTemplate;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {p0}, Lcom/kwad/sdk/core/response/b/c;->g(Lcom/kwad/sdk/core/response/model/AdTemplate;)Lcom/kwad/sdk/core/response/model/AdInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/kwad/sdk/core/response/b/a;->E(Lcom/kwad/sdk/core/response/model/AdInfo;)I

    move-result v0

    iget v1, p0, Lcom/kwad/sdk/core/response/model/AdTemplate;->type:I

    sget-object v2, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_ABOVE_GROUP:Lcom/kwad/sdk/feed/FeedType;

    iget v2, v2, Lcom/kwad/sdk/feed/FeedType;->type:I

    const/4 v3, 0x1

    if-le v1, v2, :cond_0

    return v3

    :cond_0
    iget p0, p0, Lcom/kwad/sdk/core/response/model/AdTemplate;->type:I

    invoke-static {p0}, Lcom/kwad/sdk/feed/FeedType;->fromInt(I)Lcom/kwad/sdk/feed/FeedType;

    move-result-object p0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    return v1

    :pswitch_0
    sget-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_UNKNOWN:Lcom/kwad/sdk/feed/FeedType;

    if-eq p0, v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    :pswitch_1
    sget-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_UNKNOWN:Lcom/kwad/sdk/feed/FeedType;

    if-eq p0, v0, :cond_2

    sget-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_ABOVE_GROUP:Lcom/kwad/sdk/feed/FeedType;

    if-eq p0, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1

    :pswitch_2
    sget-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_ABOVE:Lcom/kwad/sdk/feed/FeedType;

    if-eq p0, v0, :cond_3

    sget-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_BELOW:Lcom/kwad/sdk/feed/FeedType;

    if-ne p0, v0, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static fromInt(I)Lcom/kwad/sdk/feed/FeedType;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {}, Lcom/kwad/sdk/feed/FeedType;->values()[Lcom/kwad/sdk/feed/FeedType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget v4, v3, Lcom/kwad/sdk/feed/FeedType;->type:I

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_NEW:Lcom/kwad/sdk/feed/FeedType;

    invoke-virtual {v0}, Lcom/kwad/sdk/feed/FeedType;->getType()I

    move-result v0

    if-lt p0, v0, :cond_2

    sget-object p0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_TEXT_NEW:Lcom/kwad/sdk/feed/FeedType;

    return-object p0

    :cond_2
    sget-object p0, Lcom/kwad/sdk/feed/FeedType;->FEED_TYPE_UNKNOWN:Lcom/kwad/sdk/feed/FeedType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/kwad/sdk/feed/FeedType;
    .locals 1

    const-class v0, Lcom/kwad/sdk/feed/FeedType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/kwad/sdk/feed/FeedType;

    return-object p0
.end method

.method public static values()[Lcom/kwad/sdk/feed/FeedType;
    .locals 1

    sget-object v0, Lcom/kwad/sdk/feed/FeedType;->$VALUES:[Lcom/kwad/sdk/feed/FeedType;

    invoke-virtual {v0}, [Lcom/kwad/sdk/feed/FeedType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kwad/sdk/feed/FeedType;

    return-object v0
.end method


# virtual methods
.method public getType()I
    .locals 1

    iget v0, p0, Lcom/kwad/sdk/feed/FeedType;->type:I

    return v0
.end method
