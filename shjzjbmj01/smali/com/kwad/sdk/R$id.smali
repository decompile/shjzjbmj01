.class public final Lcom/kwad/sdk/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kwad/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f0d0096

.field public static final action_bar_activity_content:I = 0x7f0d0021

.field public static final action_bar_container:I = 0x7f0d0095

.field public static final action_bar_root:I = 0x7f0d0091

.field public static final action_bar_spinner:I = 0x7f0d0022

.field public static final action_bar_subtitle:I = 0x7f0d0073

.field public static final action_bar_title:I = 0x7f0d0072

.field public static final action_container:I = 0x7f0d0188

.field public static final action_context_bar:I = 0x7f0d0097

.field public static final action_divider:I = 0x7f0d0193

.field public static final action_image:I = 0x7f0d0189

.field public static final action_menu_divider:I = 0x7f0d0023

.field public static final action_menu_presenter:I = 0x7f0d0024

.field public static final action_mode_bar:I = 0x7f0d0093

.field public static final action_mode_bar_stub:I = 0x7f0d0092

.field public static final action_mode_close_button:I = 0x7f0d0074

.field public static final action_text:I = 0x7f0d018a

.field public static final actions:I = 0x7f0d0194

.field public static final activity_chooser_view_content:I = 0x7f0d0075

.field public static final add:I = 0x7f0d004a

.field public static final alertTitle:I = 0x7f0d0088

.field public static final async:I = 0x7f0d0061

.field public static final blocking:I = 0x7f0d0062

.field public static final buttonPanel:I = 0x7f0d007b

.field public static final checkbox:I = 0x7f0d008f

.field public static final chronometer:I = 0x7f0d0190

.field public static final contentPanel:I = 0x7f0d007e

.field public static final custom:I = 0x7f0d0085

.field public static final customPanel:I = 0x7f0d0084

.field public static final decor_content_parent:I = 0x7f0d0094

.field public static final default_activity_button:I = 0x7f0d0078

.field public static final edit_query:I = 0x7f0d0098

.field public static final expand_activities_button:I = 0x7f0d0076

.field public static final expanded_menu:I = 0x7f0d008e

.field public static final forever:I = 0x7f0d0063

.field public static final home:I = 0x7f0d0025

.field public static final icon:I = 0x7f0d007a

.field public static final icon_group:I = 0x7f0d0195

.field public static final image:I = 0x7f0d0077

.field public static final info:I = 0x7f0d0191

.field public static final italic:I = 0x7f0d0064

.field public static final ksad_actionbar_black_style_h5:I = 0x7f0d00c5

.field public static final ksad_actionbar_landscape_vertical:I = 0x7f0d00c8

.field public static final ksad_actionbar_logo:I = 0x7f0d0162

.field public static final ksad_actionbar_portrait_horizontal:I = 0x7f0d00c7

.field public static final ksad_ad_desc:I = 0x7f0d011b

.field public static final ksad_ad_dislike:I = 0x7f0d0117

.field public static final ksad_ad_dislike_logo:I = 0x7f0d0116

.field public static final ksad_ad_download_container:I = 0x7f0d0121

.field public static final ksad_ad_h5_container:I = 0x7f0d0122

.field public static final ksad_ad_image:I = 0x7f0d0120

.field public static final ksad_ad_image_left:I = 0x7f0d011d

.field public static final ksad_ad_image_mid:I = 0x7f0d011e

.field public static final ksad_ad_image_right:I = 0x7f0d011f

.field public static final ksad_ad_label_play_bar:I = 0x7f0d00c9

.field public static final ksad_ad_light_convert_btn:I = 0x7f0d0101

.field public static final ksad_ad_normal_container:I = 0x7f0d00fc

.field public static final ksad_ad_normal_convert_btn:I = 0x7f0d0100

.field public static final ksad_ad_normal_des:I = 0x7f0d00ff

.field public static final ksad_ad_normal_logo:I = 0x7f0d00fe

.field public static final ksad_ad_normal_title:I = 0x7f0d00fd

.field public static final ksad_app_ad_desc:I = 0x7f0d010c

.field public static final ksad_app_container:I = 0x7f0d0107

.field public static final ksad_app_desc:I = 0x7f0d0113

.field public static final ksad_app_download:I = 0x7f0d0136

.field public static final ksad_app_download_before:I = 0x7f0d0114

.field public static final ksad_app_download_btn:I = 0x7f0d010d

.field public static final ksad_app_download_btn_cover:I = 0x7f0d0165

.field public static final ksad_app_download_count:I = 0x7f0d010b

.field public static final ksad_app_download_cover:I = 0x7f0d0115

.field public static final ksad_app_icon:I = 0x7f0d0108

.field public static final ksad_app_introduce:I = 0x7f0d016a

.field public static final ksad_app_name:I = 0x7f0d0109

.field public static final ksad_app_score:I = 0x7f0d010a

.field public static final ksad_app_title:I = 0x7f0d0112

.field public static final ksad_blur_video_cover:I = 0x7f0d00d5

.field public static final ksad_bottom_container:I = 0x7f0d0164

.field public static final ksad_card_ad_desc:I = 0x7f0d00f6

.field public static final ksad_card_app_close:I = 0x7f0d00ec

.field public static final ksad_card_app_container:I = 0x7f0d0102

.field public static final ksad_card_app_desc:I = 0x7f0d00f2

.field public static final ksad_card_app_download_btn:I = 0x7f0d00f3

.field public static final ksad_card_app_download_count:I = 0x7f0d00f1

.field public static final ksad_card_app_icon:I = 0x7f0d00eb

.field public static final ksad_card_app_name:I = 0x7f0d00ed

.field public static final ksad_card_app_score:I = 0x7f0d00f0

.field public static final ksad_card_app_score_container:I = 0x7f0d00ef

.field public static final ksad_card_close:I = 0x7f0d00f5

.field public static final ksad_card_h5_container:I = 0x7f0d0103

.field public static final ksad_card_h5_open_btn:I = 0x7f0d00f7

.field public static final ksad_card_logo:I = 0x7f0d00ee

.field public static final ksad_click_mask:I = 0x7f0d00ea

.field public static final ksad_close_btn:I = 0x7f0d0168

.field public static final ksad_container:I = 0x7f0d00c2

.field public static final ksad_continue_btn:I = 0x7f0d0169

.field public static final ksad_data_flow_container:I = 0x7f0d0132

.field public static final ksad_data_flow_play_btn:I = 0x7f0d0133

.field public static final ksad_data_flow_play_tip:I = 0x7f0d0134

.field public static final ksad_detail_call_btn:I = 0x7f0d013d

.field public static final ksad_detail_close_btn:I = 0x7f0d0150

.field public static final ksad_detail_reward_icon:I = 0x7f0d014b

.field public static final ksad_detail_reward_icon_new:I = 0x7f0d014f

.field public static final ksad_detail_reward_tip_new:I = 0x7f0d014e

.field public static final ksad_download_bar:I = 0x7f0d016b

.field public static final ksad_download_bar_cover:I = 0x7f0d015e

.field public static final ksad_download_container:I = 0x7f0d00c3

.field public static final ksad_download_icon:I = 0x7f0d0145

.field public static final ksad_download_install:I = 0x7f0d0149

.field public static final ksad_download_name:I = 0x7f0d0146

.field public static final ksad_download_percent_num:I = 0x7f0d014a

.field public static final ksad_download_progress:I = 0x7f0d00f8

.field public static final ksad_download_progress_cover:I = 0x7f0d00f9

.field public static final ksad_download_size:I = 0x7f0d0148

.field public static final ksad_download_status:I = 0x7f0d0147

.field public static final ksad_download_tips_web_card_webView:I = 0x7f0d00e7

.field public static final ksad_draw_h5_logo:I = 0x7f0d00f4

.field public static final ksad_draw_tailframe_logo:I = 0x7f0d0111

.field public static final ksad_end_close_btn:I = 0x7f0d00e0

.field public static final ksad_end_left_call_btn:I = 0x7f0d013f

.field public static final ksad_end_reward_icon:I = 0x7f0d0152

.field public static final ksad_end_reward_icon_layout:I = 0x7f0d0151

.field public static final ksad_end_reward_icon_new_left:I = 0x7f0d0153

.field public static final ksad_end_reward_icon_new_right:I = 0x7f0d0154

.field public static final ksad_end_right_call_btn:I = 0x7f0d0140

.field public static final ksad_feed_logo:I = 0x7f0d0118

.field public static final ksad_feed_video_container:I = 0x7f0d0125

.field public static final ksad_h5_ad_desc:I = 0x7f0d010f

.field public static final ksad_h5_container:I = 0x7f0d010e

.field public static final ksad_h5_desc:I = 0x7f0d0119

.field public static final ksad_h5_open:I = 0x7f0d0138

.field public static final ksad_h5_open_btn:I = 0x7f0d0110

.field public static final ksad_h5_open_cover:I = 0x7f0d011a

.field public static final ksad_image_container:I = 0x7f0d011c

.field public static final ksad_item_touch_helper_previous_elevation:I = 0x7f0d0026

.field public static final ksad_kwad_titlebar_title:I = 0x7f0d00b8

.field public static final ksad_kwad_web_navi_back:I = 0x7f0d00b6

.field public static final ksad_kwad_web_navi_close:I = 0x7f0d00b7

.field public static final ksad_kwad_web_title_bar:I = 0x7f0d00b5

.field public static final ksad_kwad_web_titlebar:I = 0x7f0d00bd

.field public static final ksad_landing_page_root:I = 0x7f0d00bc

.field public static final ksad_logo_container:I = 0x7f0d0141

.field public static final ksad_logo_icon:I = 0x7f0d0142

.field public static final ksad_logo_text:I = 0x7f0d0143

.field public static final ksad_message_toast_txt:I = 0x7f0d00e3

.field public static final ksad_middle_end_card:I = 0x7f0d00da

.field public static final ksad_middle_end_card_native:I = 0x7f0d00dd

.field public static final ksad_middle_end_card_webview_container:I = 0x7f0d00dc

.field public static final ksad_mini_web_card_container:I = 0x7f0d00d6

.field public static final ksad_mini_web_card_webView:I = 0x7f0d00d7

.field public static final ksad_play_detail_top_toolbar:I = 0x7f0d00ce

.field public static final ksad_play_end_top_toolbar:I = 0x7f0d00d4

.field public static final ksad_play_end_web_card_container:I = 0x7f0d0105

.field public static final ksad_play_web_card_webView:I = 0x7f0d00cd

.field public static final ksad_playable_webview:I = 0x7f0d00d8

.field public static final ksad_preload_container:I = 0x7f0d015a

.field public static final ksad_product_name:I = 0x7f0d0161

.field public static final ksad_progress_bg:I = 0x7f0d00e8

.field public static final ksad_recycler_container:I = 0x7f0d00b9

.field public static final ksad_recycler_view:I = 0x7f0d00e6

.field public static final ksad_reward_container_new:I = 0x7f0d014c

.field public static final ksad_reward_mini_card_close:I = 0x7f0d00d9

.field public static final ksad_root_container:I = 0x7f0d00c4

.field public static final ksad_score_fifth:I = 0x7f0d00e2

.field public static final ksad_score_fourth:I = 0x7f0d00e1

.field public static final ksad_skip_icon:I = 0x7f0d013e

.field public static final ksad_splash_background:I = 0x7f0d0156

.field public static final ksad_splash_foreground:I = 0x7f0d0158

.field public static final ksad_splash_logo_container:I = 0x7f0d00db

.field public static final ksad_splash_preload_tips:I = 0x7f0d015b

.field public static final ksad_splash_root_container:I = 0x7f0d0155

.field public static final ksad_splash_skip_time:I = 0x7f0d015c

.field public static final ksad_splash_sound:I = 0x7f0d015d

.field public static final ksad_splash_video_player:I = 0x7f0d0157

.field public static final ksad_splash_web_card_webView:I = 0x7f0d0159

.field public static final ksad_status_tv:I = 0x7f0d00e9

.field public static final ksad_tf_h5_ad_desc:I = 0x7f0d016c

.field public static final ksad_tf_h5_open_btn:I = 0x7f0d016d

.field public static final ksad_title:I = 0x7f0d0167

.field public static final ksad_top_container:I = 0x7f0d0160

.field public static final ksad_top_container_product:I = 0x7f0d0166

.field public static final ksad_top_outer:I = 0x7f0d015f

.field public static final ksad_video_app_tail_frame:I = 0x7f0d016f

.field public static final ksad_video_complete_app_container:I = 0x7f0d0135

.field public static final ksad_video_complete_h5_container:I = 0x7f0d0137

.field public static final ksad_video_container:I = 0x7f0d0123

.field public static final ksad_video_control_container:I = 0x7f0d0128

.field public static final ksad_video_control_fullscreen:I = 0x7f0d012d

.field public static final ksad_video_control_fullscreen_container:I = 0x7f0d0126

.field public static final ksad_video_control_fullscreen_title:I = 0x7f0d0127

.field public static final ksad_video_control_play_button:I = 0x7f0d0129

.field public static final ksad_video_control_play_duration:I = 0x7f0d012a

.field public static final ksad_video_control_play_status:I = 0x7f0d012e

.field public static final ksad_video_control_play_total:I = 0x7f0d012c

.field public static final ksad_video_count_down:I = 0x7f0d013b

.field public static final ksad_video_count_down_new:I = 0x7f0d014d

.field public static final ksad_video_cover:I = 0x7f0d0106

.field public static final ksad_video_cover_image:I = 0x7f0d0144

.field public static final ksad_video_error_container:I = 0x7f0d0139

.field public static final ksad_video_fail_tip:I = 0x7f0d00fa

.field public static final ksad_video_first_frame:I = 0x7f0d00fb

.field public static final ksad_video_first_frame_container:I = 0x7f0d0124

.field public static final ksad_video_h5_tail_frame:I = 0x7f0d0170

.field public static final ksad_video_landscape_horizontal:I = 0x7f0d00d1

.field public static final ksad_video_landscape_vertical:I = 0x7f0d00d2

.field public static final ksad_video_network_unavailable:I = 0x7f0d0131

.field public static final ksad_video_place_holder:I = 0x7f0d0163

.field public static final ksad_video_play_bar_app_landscape:I = 0x7f0d00cb

.field public static final ksad_video_play_bar_app_portrait:I = 0x7f0d00ca

.field public static final ksad_video_play_bar_h5:I = 0x7f0d00cc

.field public static final ksad_video_player:I = 0x7f0d00c6

.field public static final ksad_video_portrait_horizontal:I = 0x7f0d00cf

.field public static final ksad_video_portrait_vertical:I = 0x7f0d00d0

.field public static final ksad_video_progress:I = 0x7f0d013a

.field public static final ksad_video_sound_switch:I = 0x7f0d013c

.field public static final ksad_video_tail_frame:I = 0x7f0d0104

.field public static final ksad_video_tail_frame_container:I = 0x7f0d0176

.field public static final ksad_video_text_below:I = 0x7f0d012f

.field public static final ksad_video_tf_logo:I = 0x7f0d0171

.field public static final ksad_video_thumb_container:I = 0x7f0d0172

.field public static final ksad_video_thumb_image:I = 0x7f0d0130

.field public static final ksad_video_thumb_img:I = 0x7f0d016e

.field public static final ksad_video_thumb_left:I = 0x7f0d0173

.field public static final ksad_video_thumb_mid:I = 0x7f0d0174

.field public static final ksad_video_thumb_right:I = 0x7f0d0175

.field public static final ksad_video_webView:I = 0x7f0d00e5

.field public static final ksad_video_webview:I = 0x7f0d00be

.field public static final ksad_web_card_container:I = 0x7f0d00d3

.field public static final ksad_web_card_frame:I = 0x7f0d00de

.field public static final ksad_web_card_webView:I = 0x7f0d00df

.field public static final ksad_web_download_container:I = 0x7f0d00ba

.field public static final ksad_web_download_progress:I = 0x7f0d00bb

.field public static final ksad_web_tip_bar:I = 0x7f0d00bf

.field public static final ksad_web_tip_bar_textview:I = 0x7f0d00c0

.field public static final ksad_web_tip_close_btn:I = 0x7f0d00c1

.field public static final ksad_web_video_seek_bar:I = 0x7f0d012b

.field public static final line1:I = 0x7f0d0027

.field public static final line3:I = 0x7f0d0028

.field public static final listMode:I = 0x7f0d0040

.field public static final list_item:I = 0x7f0d0079

.field public static final message:I = 0x7f0d00a5

.field public static final multiply:I = 0x7f0d004b

.field public static final none:I = 0x7f0d0045

.field public static final normal:I = 0x7f0d0041

.field public static final notification_background:I = 0x7f0d018b

.field public static final notification_main_column:I = 0x7f0d018d

.field public static final notification_main_column_container:I = 0x7f0d018c

.field public static final parentPanel:I = 0x7f0d007d

.field public static final progress_circular:I = 0x7f0d0029

.field public static final progress_horizontal:I = 0x7f0d002a

.field public static final radio:I = 0x7f0d0090

.field public static final right_icon:I = 0x7f0d0192

.field public static final right_side:I = 0x7f0d018e

.field public static final root:I = 0x7f0d00e4

.field public static final screen:I = 0x7f0d004c

.field public static final scrollIndicatorDown:I = 0x7f0d0083

.field public static final scrollIndicatorUp:I = 0x7f0d007f

.field public static final scrollView:I = 0x7f0d0080

.field public static final search_badge:I = 0x7f0d009a

.field public static final search_bar:I = 0x7f0d0099

.field public static final search_button:I = 0x7f0d009b

.field public static final search_close_btn:I = 0x7f0d00a0

.field public static final search_edit_frame:I = 0x7f0d009c

.field public static final search_go_btn:I = 0x7f0d00a2

.field public static final search_mag_icon:I = 0x7f0d009d

.field public static final search_plate:I = 0x7f0d009e

.field public static final search_src_text:I = 0x7f0d009f

.field public static final search_voice_btn:I = 0x7f0d00a3

.field public static final select_dialog_listview:I = 0x7f0d00a4

.field public static final shortcut:I = 0x7f0d008c

.field public static final spacer:I = 0x7f0d007c

.field public static final split_action_bar:I = 0x7f0d002b

.field public static final src_atop:I = 0x7f0d004d

.field public static final src_in:I = 0x7f0d004e

.field public static final src_over:I = 0x7f0d004f

.field public static final submenuarrow:I = 0x7f0d008d

.field public static final submit_area:I = 0x7f0d00a1

.field public static final tabMode:I = 0x7f0d0042

.field public static final tag_transition_group:I = 0x7f0d0032

.field public static final text:I = 0x7f0d0036

.field public static final text2:I = 0x7f0d0037

.field public static final textSpacerNoButtons:I = 0x7f0d0082

.field public static final textSpacerNoTitle:I = 0x7f0d0081

.field public static final time:I = 0x7f0d018f

.field public static final title:I = 0x7f0d0038

.field public static final titleDividerNoCustom:I = 0x7f0d0089

.field public static final title_template:I = 0x7f0d0087

.field public static final topPanel:I = 0x7f0d0086

.field public static final uniform:I = 0x7f0d0050

.field public static final up:I = 0x7f0d003f

.field public static final wrap_content:I = 0x7f0d0051


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
