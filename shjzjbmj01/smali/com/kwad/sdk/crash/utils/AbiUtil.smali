.class public final Lcom/kwad/sdk/crash/utils/AbiUtil;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;
    }
.end annotation


# static fields
.field private static a:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;


# direct methods
.method public static a()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/kwad/sdk/crash/utils/AbiUtil;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "arm64-v8a"

    goto :goto_0

    :cond_0
    const-string v0, "armeabi-v7a"

    :goto_0
    return-object v0
.end method

.method public static b()Z
    .locals 2

    invoke-static {}, Lcom/kwad/sdk/crash/utils/AbiUtil;->c()Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    move-result-object v0

    sget-object v1, Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;->ARM64_V8A:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static c()Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;
    .locals 4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    sget-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;->ARMEABI_V7A:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    return-object v0

    :cond_0
    sget-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil;->a:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil;->a:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    :try_start_0
    const-string v1, "dalvik.system.VMRuntime"

    const-string v2, "getRuntime"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/kwad/sdk/utils/l;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "is64Bit"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/kwad/sdk/utils/l;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;->ARM64_V8A:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;->ARMEABI_V7A:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    :goto_0
    sput-object v1, Lcom/kwad/sdk/crash/utils/AbiUtil;->a:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    invoke-static {v1}, Lcom/kwad/sdk/core/d/a;->b(Ljava/lang/Throwable;)V

    :try_start_1
    const-string v1, "sun.misc.Unsafe"

    const-string v2, "getUnsafe"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/kwad/sdk/utils/l;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "addressSize"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/kwad/sdk/utils/l;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;->ARM64_V8A:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;->ARMEABI_V7A:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    :goto_1
    sput-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil;->a:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    return-object v0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/kwad/sdk/core/d/a;->b(Ljava/lang/Throwable;)V

    :try_start_2
    invoke-static {}, Lcom/kwad/sdk/crash/d;->a()Lcom/kwad/sdk/crash/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kwad/sdk/crash/d;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    const-string v1, "arm64"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;->ARM64_V8A:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    sput-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil;->a:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    return-object v0

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/kwad/sdk/core/d/a;->b(Ljava/lang/Throwable;)V

    :cond_4
    sget-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;->UNKNOWN:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    sput-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil;->a:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    sget-object v0, Lcom/kwad/sdk/crash/utils/AbiUtil;->a:Lcom/kwad/sdk/crash/utils/AbiUtil$Abi;

    return-object v0
.end method
