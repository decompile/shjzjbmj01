.class public final Lcom/kwad/sdk/crash/c/c;
.super Lcom/kwad/sdk/crash/c/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kwad/sdk/crash/c/c$a;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/kwad/sdk/crash/c/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/kwad/sdk/crash/c/c$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/kwad/sdk/crash/c/c;-><init>()V

    return-void
.end method

.method public static d()Lcom/kwad/sdk/crash/c/c;
    .locals 1

    invoke-static {}, Lcom/kwad/sdk/crash/c/c$a;->a()Lcom/kwad/sdk/crash/c/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/File;Lcom/kwad/sdk/crash/e;Lcom/kwad/sdk/crash/report/c;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/kwad/sdk/crash/c/b;->a(Ljava/io/File;Lcom/kwad/sdk/crash/e;Lcom/kwad/sdk/crash/report/c;)V

    invoke-static {}, Lcom/kwad/sdk/crash/d;->a()Lcom/kwad/sdk/crash/d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/kwad/sdk/crash/d;->i()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Ljava/io/File;

    const-string p2, "sdcard/kwad_ex/java_crash/dump"

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/kwad/sdk/crash/c/c;->a(Ljava/io/File;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Throwable;Lcom/kwad/sdk/crash/model/message/ExceptionMessage;Landroid/content/Context;)V
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/kwad/sdk/crash/model/message/ExceptionMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/kwad/sdk/crash/c/c;->a(Ljava/lang/Throwable;Lcom/kwad/sdk/crash/model/message/ExceptionMessage;Landroid/content/Context;Z)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;Lcom/kwad/sdk/crash/model/message/ExceptionMessage;Landroid/content/Context;Z)V
    .locals 17
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/kwad/sdk/crash/model/message/ExceptionMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    iget-object v0, v1, Lcom/kwad/sdk/crash/c/c;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iget-object v4, v1, Lcom/kwad/sdk/crash/c/c;->f:Ljava/io/File;

    iget-object v5, v1, Lcom/kwad/sdk/crash/c/c;->g:Ljava/io/File;

    iget-object v6, v1, Lcom/kwad/sdk/crash/c/c;->h:Ljava/io/File;

    iget-object v7, v1, Lcom/kwad/sdk/crash/c/c;->i:Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Lcom/kwad/sdk/crash/c/c;->a()Lcom/kwad/sdk/crash/report/c;

    move-result-object v8

    const/4 v10, 0x0

    const/4 v11, 0x1

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v2, Lcom/kwad/sdk/crash/model/message/ExceptionMessage;->mCrashDetail:Ljava/lang/String;

    iget-object v12, v1, Lcom/kwad/sdk/crash/c/c;->e:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_0

    iget-object v12, v1, Lcom/kwad/sdk/crash/c/c;->e:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->mkdirs()Z

    move-result v12

    if-nez v12, :cond_0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v2, Lcom/kwad/sdk/crash/model/message/ExceptionMessage;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, "create "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/kwad/sdk/crash/c/c;->d()Lcom/kwad/sdk/crash/c/c;

    move-result-object v13

    iget-object v13, v13, Lcom/kwad/sdk/crash/c/c;->e:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, " failed!\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v2, Lcom/kwad/sdk/crash/model/message/ExceptionMessage;->mErrorMessage:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v12, 0x0

    goto :goto_0

    :cond_0
    const/4 v12, 0x1

    :goto_0
    if-eqz v4, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    :try_start_1
    new-instance v13, Ljava/io/File;

    iget-object v14, v1, Lcom/kwad/sdk/crash/c/c;->e:Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/kwad/sdk/crash/c/c;->a:Ljava/lang/String;

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "-"

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, ".dump"

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v13, v14, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v4, v13

    :cond_2
    if-eqz v5, :cond_3

    if-eqz v0, :cond_4

    :cond_3
    new-instance v9, Ljava/io/File;

    iget-object v13, v1, Lcom/kwad/sdk/crash/c/c;->e:Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/kwad/sdk/crash/c/c;->a:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v15, ".log"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v9, v13, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v5, v9

    :cond_4
    if-eqz v6, :cond_5

    if-eqz v0, :cond_6

    :cond_5
    new-instance v9, Ljava/io/File;

    iget-object v13, v1, Lcom/kwad/sdk/crash/c/c;->e:Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/kwad/sdk/crash/c/c;->a:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ".jtrace"

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v13, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v6, v9

    :cond_6
    invoke-static/range {p1 .. p3}, Lcom/kwad/sdk/crash/utils/f;->a(Ljava/lang/Throwable;Lcom/kwad/sdk/crash/model/message/ExceptionMessage;Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/kwad/sdk/crash/c/c;->c()I

    move-result v0

    invoke-static {v2, v0}, Lcom/kwad/sdk/crash/utils/f;->a(Lcom/kwad/sdk/crash/model/message/ExceptionMessage;I)V

    iget-object v0, v1, Lcom/kwad/sdk/crash/c/c;->c:Lcom/kwad/sdk/crash/e;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/kwad/sdk/crash/c/c;->c:Lcom/kwad/sdk/crash/e;

    invoke-virtual/range {p0 .. p0}, Lcom/kwad/sdk/crash/c/c;->c()I

    move-result v9

    invoke-interface {v0, v9, v2}, Lcom/kwad/sdk/crash/e;->a(ILcom/kwad/sdk/crash/model/message/ExceptionMessage;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_7
    :try_start_2
    invoke-virtual/range {p2 .. p2}, Lcom/kwad/sdk/crash/model/message/ExceptionMessage;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v12, :cond_b

    invoke-static {v4, v0}, Lcom/kwad/sdk/crash/utils/f;->a(Ljava/io/File;Ljava/lang/CharSequence;)V

    invoke-static {v6}, Lcom/kwad/sdk/crash/utils/f;->c(Ljava/io/File;)V

    invoke-static {v5}, Lcom/kwad/sdk/crash/utils/f;->a(Ljava/io/File;)V

    invoke-static {}, Lcom/kwad/sdk/crash/d;->a()Lcom/kwad/sdk/crash/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kwad/sdk/crash/d;->i()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, v1, Lcom/kwad/sdk/crash/c/c;->e:Ljava/io/File;

    invoke-virtual {v1, v0}, Lcom/kwad/sdk/crash/c/c;->b(Ljava/io/File;)V

    :cond_8
    if-eqz v8, :cond_a

    const-string v0, "ExceptionCollector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "------  Java Crash Happened Begin ------\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v0, v2}, Lcom/kwad/sdk/crash/report/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_9

    new-array v0, v11, [Ljava/io/File;

    aput-object v4, v0, v10

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/kwad/sdk/crash/c/c;->a([Ljava/io/File;Ljava/util/concurrent/CountDownLatch;)V

    goto :goto_1

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/kwad/sdk/crash/c/c;->b()V

    :cond_a
    :goto_1
    invoke-static {v7}, Lcom/kwad/sdk/crash/utils/f;->d(Ljava/io/File;)V

    goto/16 :goto_6

    :cond_b
    if-eqz v8, :cond_10

    const-string v3, "ExceptionCollector"

    const-string v4, "uploader.uploadEvent(message);"

    invoke-static {v3, v4}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v2}, Lcom/kwad/sdk/crash/report/c;->a(Lcom/kwad/sdk/crash/model/message/ExceptionMessage;)V

    const-string v3, "java_crash_mkdir_fail"

    invoke-interface {v8, v3, v0}, Lcom/kwad/sdk/crash/report/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ExceptionCollector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "------  Java Crash Happened Begin ------\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v0, v2}, Lcom/kwad/sdk/crash/report/c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_6

    :catch_0
    move-exception v0

    if-eqz v8, :cond_10

    :try_start_3
    const-string v2, "java_crash_dump_error"

    :goto_2
    invoke-static {v0}, Lcom/kwad/sdk/crash/utils/f;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v2, v0}, Lcom/kwad/sdk/crash/report/c;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_6

    :catch_1
    move-exception v0

    goto :goto_4

    :catchall_0
    move-exception v0

    const/4 v12, 0x1

    :goto_3
    move-object/from16 v16, v4

    move-object v4, v0

    move-object/from16 v0, v16

    goto/16 :goto_7

    :catch_2
    move-exception v0

    const/4 v12, 0x1

    :goto_4
    :try_start_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v13, v2, Lcom/kwad/sdk/crash/model/message/ExceptionMessage;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/kwad/sdk/crash/model/message/ExceptionMessage;->mErrorMessage:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-virtual/range {p2 .. p2}, Lcom/kwad/sdk/crash/model/message/ExceptionMessage;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v12, :cond_f

    invoke-static {v4, v0}, Lcom/kwad/sdk/crash/utils/f;->a(Ljava/io/File;Ljava/lang/CharSequence;)V

    invoke-static {v6}, Lcom/kwad/sdk/crash/utils/f;->c(Ljava/io/File;)V

    invoke-static {v5}, Lcom/kwad/sdk/crash/utils/f;->a(Ljava/io/File;)V

    invoke-static {}, Lcom/kwad/sdk/crash/d;->a()Lcom/kwad/sdk/crash/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kwad/sdk/crash/d;->i()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, v1, Lcom/kwad/sdk/crash/c/c;->e:Ljava/io/File;

    invoke-virtual {v1, v0}, Lcom/kwad/sdk/crash/c/c;->b(Ljava/io/File;)V

    :cond_c
    if-eqz v8, :cond_e

    const-string v0, "ExceptionCollector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "------  Java Crash Happened Begin ------\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v0, v2}, Lcom/kwad/sdk/crash/report/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_d

    new-array v0, v11, [Ljava/io/File;

    aput-object v4, v0, v10

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/kwad/sdk/crash/c/c;->a([Ljava/io/File;Ljava/util/concurrent/CountDownLatch;)V

    goto :goto_5

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/kwad/sdk/crash/c/c;->b()V

    :cond_e
    :goto_5
    invoke-static {v7}, Lcom/kwad/sdk/crash/utils/f;->d(Ljava/io/File;)V

    goto :goto_6

    :cond_f
    if-eqz v8, :cond_10

    const-string v3, "ExceptionCollector"

    const-string v4, "uploader.uploadEvent(message);"

    invoke-static {v3, v4}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v2}, Lcom/kwad/sdk/crash/report/c;->a(Lcom/kwad/sdk/crash/model/message/ExceptionMessage;)V

    const-string v3, "java_crash_mkdir_fail"

    invoke-interface {v8, v3, v0}, Lcom/kwad/sdk/crash/report/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ExceptionCollector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "------  Java Crash Happened Begin ------\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v0, v2}, Lcom/kwad/sdk/crash/report/c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_6

    :catch_3
    move-exception v0

    if-eqz v8, :cond_10

    :try_start_6
    const-string v2, "java_crash_dump_error"
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_2

    :catch_4
    :cond_10
    :goto_6
    return-void

    :catchall_1
    move-exception v0

    goto/16 :goto_3

    :goto_7
    :try_start_7
    invoke-virtual/range {p2 .. p2}, Lcom/kwad/sdk/crash/model/message/ExceptionMessage;->toJson()Lorg/json/JSONObject;

    move-result-object v9

    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_14

    invoke-static {v0, v9}, Lcom/kwad/sdk/crash/utils/f;->a(Ljava/io/File;Ljava/lang/CharSequence;)V

    invoke-static {v6}, Lcom/kwad/sdk/crash/utils/f;->c(Ljava/io/File;)V

    invoke-static {v5}, Lcom/kwad/sdk/crash/utils/f;->a(Ljava/io/File;)V

    invoke-static {}, Lcom/kwad/sdk/crash/d;->a()Lcom/kwad/sdk/crash/d;

    move-result-object v5

    invoke-virtual {v5}, Lcom/kwad/sdk/crash/d;->i()Z

    move-result v5

    if-eqz v5, :cond_11

    iget-object v5, v1, Lcom/kwad/sdk/crash/c/c;->e:Ljava/io/File;

    invoke-virtual {v1, v5}, Lcom/kwad/sdk/crash/c/c;->b(Ljava/io/File;)V

    :cond_11
    if-eqz v8, :cond_13

    const-string v5, "ExceptionCollector"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "------  Java Crash Happened Begin ------\n"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v5, v2}, Lcom/kwad/sdk/crash/report/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_12

    new-array v2, v11, [Ljava/io/File;

    aput-object v0, v2, v10

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/kwad/sdk/crash/c/c;->a([Ljava/io/File;Ljava/util/concurrent/CountDownLatch;)V

    goto :goto_8

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/kwad/sdk/crash/c/c;->b()V

    :cond_13
    :goto_8
    invoke-static {v7}, Lcom/kwad/sdk/crash/utils/f;->d(Ljava/io/File;)V

    goto :goto_9

    :cond_14
    if-eqz v8, :cond_15

    const-string v0, "ExceptionCollector"

    const-string v3, "uploader.uploadEvent(message);"

    invoke-static {v0, v3}, Lcom/kwad/sdk/core/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v2}, Lcom/kwad/sdk/crash/report/c;->a(Lcom/kwad/sdk/crash/model/message/ExceptionMessage;)V

    const-string v0, "java_crash_mkdir_fail"

    invoke-interface {v8, v0, v9}, Lcom/kwad/sdk/crash/report/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ExceptionCollector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "------  Java Crash Happened Begin ------\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v0, v2}, Lcom/kwad/sdk/crash/report/c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_9

    :catch_5
    move-exception v0

    if-eqz v8, :cond_15

    :try_start_8
    const-string v2, "java_crash_dump_error"

    invoke-static {v0}, Lcom/kwad/sdk/crash/utils/f;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v2, v0}, Lcom/kwad/sdk/crash/report/c;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    :catch_6
    :cond_15
    :goto_9
    throw v4
.end method

.method protected a([Ljava/io/File;Ljava/util/concurrent/CountDownLatch;)V
    .locals 3
    .param p1    # [Ljava/io/File;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/CountDownLatch;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance p2, Lcom/kwad/sdk/crash/report/d;

    invoke-direct {p2}, Lcom/kwad/sdk/crash/report/d;-><init>()V

    invoke-virtual {p0}, Lcom/kwad/sdk/crash/c/c;->a()Lcom/kwad/sdk/crash/report/c;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/kwad/sdk/crash/report/d;->a(Lcom/kwad/sdk/crash/report/c;)V

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    invoke-virtual {p2, v2}, Lcom/kwad/sdk/crash/report/d;->b(Ljava/io/File;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected c()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
