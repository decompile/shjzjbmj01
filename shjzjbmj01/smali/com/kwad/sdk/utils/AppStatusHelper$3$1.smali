.class Lcom/kwad/sdk/utils/AppStatusHelper$3$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/kwad/sdk/collector/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kwad/sdk/utils/AppStatusHelper$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/kwad/sdk/utils/AppStatusHelper$3;


# direct methods
.method constructor <init>(Lcom/kwad/sdk/utils/AppStatusHelper$3;)V
    .locals 0

    iput-object p1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$3$1;->a:Lcom/kwad/sdk/utils/AppStatusHelper$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 3

    const-string v0, "AppStatusHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fetchAppStatusConfig onFetchError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", code: "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/kwad/sdk/core/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/kwad/sdk/collector/AppStatusRules;)V
    .locals 4

    invoke-static {p1}, Lcom/kwad/sdk/utils/AppStatusHelper;->a(Lcom/kwad/sdk/collector/AppStatusRules;)Lcom/kwad/sdk/collector/AppStatusRules;

    iget-object p1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$3$1;->a:Lcom/kwad/sdk/utils/AppStatusHelper$3;

    iget-object p1, p1, Lcom/kwad/sdk/utils/AppStatusHelper$3;->a:Landroid/content/Context;

    invoke-static {}, Lcom/kwad/sdk/utils/AppStatusHelper;->b()Lcom/kwad/sdk/collector/AppStatusRules;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/kwad/sdk/utils/AppStatusHelper;->a(Landroid/content/Context;Lcom/kwad/sdk/collector/AppStatusRules;)V

    iget-object p1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$3$1;->a:Lcom/kwad/sdk/utils/AppStatusHelper$3;

    iget-object p1, p1, Lcom/kwad/sdk/utils/AppStatusHelper$3;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/kwad/sdk/utils/AppStatusHelper;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/kwad/sdk/utils/AppStatusHelper;->b()Lcom/kwad/sdk/collector/AppStatusRules;

    move-result-object p1

    invoke-virtual {p1}, Lcom/kwad/sdk/collector/AppStatusRules;->obtainScanInterval()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    iget-object p1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$3$1;->a:Lcom/kwad/sdk/utils/AppStatusHelper$3;

    iget-object p1, p1, Lcom/kwad/sdk/utils/AppStatusHelper$3;->a:Landroid/content/Context;

    invoke-static {p1, v0, v1}, Lcom/kwad/sdk/utils/AppStatusHelper;->b(Landroid/content/Context;J)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$3$1;->a:Lcom/kwad/sdk/utils/AppStatusHelper$3;

    iget-object p1, p1, Lcom/kwad/sdk/utils/AppStatusHelper$3;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/kwad/sdk/utils/AppStatusHelper;->c(Landroid/content/Context;)V

    :goto_0
    return-void
.end method
