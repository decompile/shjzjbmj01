.class public Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kwad/sdk/utils/AppStatusHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppRunningInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable<",
        "Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static format:Ljava/text/SimpleDateFormat; = null

.field public static granularity:J = 0xea60L

.field private static final serialVersionUID:J = 0x615a474ccdc5b72eL


# instance fields
.field private lastRunningTime:J

.field private name:Ljava/lang/String;

.field private packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd:HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->format:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->lastRunningTime:J

    return-void
.end method

.method public static createInstance(Lcom/kwad/sdk/utils/InstalledAppInfoManager$AppPackageInfo;)Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;
    .locals 2

    new-instance v0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;

    invoke-direct {v0}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;-><init>()V

    iget-object v1, p0, Lcom/kwad/sdk/utils/InstalledAppInfoManager$AppPackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->setPackageName(Ljava/lang/String;)V

    iget-object p0, p0, Lcom/kwad/sdk/utils/InstalledAppInfoManager$AppPackageInfo;->appName:Ljava/lang/String;

    invoke-virtual {v0, p0}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->setName(Ljava/lang/String;)V

    return-object v0
.end method

.method public static createInstance(Ljava/lang/String;)Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;
    .locals 1

    new-instance v0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;

    invoke-direct {v0}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;-><init>()V

    invoke-virtual {v0, p0}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->setPackageName(Ljava/lang/String;)V

    return-object v0
.end method

.method public static samePackageTimeTo(Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_3

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->getLastRunningTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->getLastRunningTime()J

    move-result-wide p0

    cmp-long v4, v2, p0

    if-nez v4, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method


# virtual methods
.method public cloneNewOne()Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;
    .locals 3

    new-instance v0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;

    invoke-direct {v0}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;-><init>()V

    iget-object v1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->setName(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->lastRunningTime:J

    invoke-virtual {v0, v1, v2}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->setLastRunningTime(J)V

    iget-object v1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->setPackageName(Ljava/lang/String;)V

    return-object v0
.end method

.method public compareTo(Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;)I
    .locals 5

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-wide v1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->lastRunningTime:J

    invoke-virtual {p1}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->getLastRunningTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x0

    cmp-long p1, v1, v3

    if-nez p1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    cmp-long p1, v1, v3

    if-lez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;

    invoke-virtual {p0, p1}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->compareTo(Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;

    sget-wide v1, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->granularity:J

    iget-wide v3, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->lastRunningTime:J

    div-long/2addr v3, v1

    invoke-virtual {p1}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->getLastRunningTime()J

    move-result-wide v5

    div-long/2addr v5, v1

    cmp-long v1, v3, v5

    if-eqz v1, :cond_2

    return v0

    :cond_2
    iget-object v1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    :cond_3
    iget-object v0, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->packageName:Ljava/lang/String;

    iget-object p1, p1, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_4
    :goto_0
    return v0
.end method

.method public formatTime(J)Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->format:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getFormattedLastRunningTime()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->getLastRunningTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->formatTime(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastRunningTime()J
    .locals 2

    iget-wide v0, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->lastRunningTime:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    sget-wide v0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->granularity:J

    iget-wide v2, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->lastRunningTime:J

    div-long/2addr v2, v0

    iget-object v0, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    const/16 v1, 0x20

    ushr-long v4, v2, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public setLastRunningTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->lastRunningTime:J

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->name:Ljava/lang/String;

    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->packageName:Ljava/lang/String;

    return-void
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 4

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "name"

    iget-object v2, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->name:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "packageName"

    iget-object v2, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "lastRunningTime"

    iget-wide v2, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->lastRunningTime:J

    invoke-static {v0, v1, v2, v3}, Lcom/kwad/sdk/utils/m;->a(Lorg/json/JSONObject;Ljava/lang/String;J)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppRunningInfo{packageName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", lastRunningTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->lastRunningTime:J

    invoke-virtual {p0, v1, v2}, Lcom/kwad/sdk/utils/AppStatusHelper$AppRunningInfo;->formatTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
