.class public final Lcom/qmo/game/mpsdk/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qmo/game/mpsdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0c00b7

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0c00b8

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0c00b9

.field public static final abc_btn_colored_text_material:I = 0x7f0c00ba

.field public static final abc_color_highlight_material:I = 0x7f0c00bb

.field public static final abc_hint_foreground_material_dark:I = 0x7f0c00bc

.field public static final abc_hint_foreground_material_light:I = 0x7f0c00bd

.field public static final abc_input_method_navigation_guard:I = 0x7f0c0001

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0c00be

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0c00bf

.field public static final abc_primary_text_material_dark:I = 0x7f0c00c0

.field public static final abc_primary_text_material_light:I = 0x7f0c00c1

.field public static final abc_search_url_text:I = 0x7f0c00c2

.field public static final abc_search_url_text_normal:I = 0x7f0c0002

.field public static final abc_search_url_text_pressed:I = 0x7f0c0003

.field public static final abc_search_url_text_selected:I = 0x7f0c0004

.field public static final abc_secondary_text_material_dark:I = 0x7f0c00c3

.field public static final abc_secondary_text_material_light:I = 0x7f0c00c4

.field public static final abc_tint_btn_checkable:I = 0x7f0c00c5

.field public static final abc_tint_default:I = 0x7f0c00c6

.field public static final abc_tint_edittext:I = 0x7f0c00c7

.field public static final abc_tint_seek_thumb:I = 0x7f0c00c8

.field public static final abc_tint_spinner:I = 0x7f0c00c9

.field public static final abc_tint_switch_track:I = 0x7f0c00ca

.field public static final accent_material_dark:I = 0x7f0c0005

.field public static final accent_material_light:I = 0x7f0c0006

.field public static final background_floating_material_dark:I = 0x7f0c0007

.field public static final background_floating_material_light:I = 0x7f0c0008

.field public static final background_material_dark:I = 0x7f0c000a

.field public static final background_material_light:I = 0x7f0c000b

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0c000d

.field public static final bright_foreground_disabled_material_light:I = 0x7f0c000e

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0c000f

.field public static final bright_foreground_inverse_material_light:I = 0x7f0c0010

.field public static final bright_foreground_material_dark:I = 0x7f0c0011

.field public static final bright_foreground_material_light:I = 0x7f0c0012

.field public static final button_material_dark:I = 0x7f0c0017

.field public static final button_material_light:I = 0x7f0c0018

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0c0023

.field public static final dim_foreground_disabled_material_light:I = 0x7f0c0024

.field public static final dim_foreground_material_dark:I = 0x7f0c0025

.field public static final dim_foreground_material_light:I = 0x7f0c0026

.field public static final error_color_material_dark:I = 0x7f0c0027

.field public static final error_color_material_light:I = 0x7f0c0028

.field public static final foreground_material_dark:I = 0x7f0c0029

.field public static final foreground_material_light:I = 0x7f0c002a

.field public static final highlighted_text_material_dark:I = 0x7f0c002b

.field public static final highlighted_text_material_light:I = 0x7f0c002c

.field public static final material_blue_grey_800:I = 0x7f0c004c

.field public static final material_blue_grey_900:I = 0x7f0c004d

.field public static final material_blue_grey_950:I = 0x7f0c004e

.field public static final material_deep_teal_200:I = 0x7f0c004f

.field public static final material_deep_teal_500:I = 0x7f0c0050

.field public static final material_grey_100:I = 0x7f0c0051

.field public static final material_grey_300:I = 0x7f0c0052

.field public static final material_grey_50:I = 0x7f0c0053

.field public static final material_grey_600:I = 0x7f0c0054

.field public static final material_grey_800:I = 0x7f0c0055

.field public static final material_grey_850:I = 0x7f0c0056

.field public static final material_grey_900:I = 0x7f0c0057

.field public static final notification_action_color_filter:I = 0x7f0c0000

.field public static final notification_icon_bg_color:I = 0x7f0c0058

.field public static final primary_dark_material_dark:I = 0x7f0c0059

.field public static final primary_dark_material_light:I = 0x7f0c005a

.field public static final primary_material_dark:I = 0x7f0c005b

.field public static final primary_material_light:I = 0x7f0c005c

.field public static final primary_text_default_material_dark:I = 0x7f0c005d

.field public static final primary_text_default_material_light:I = 0x7f0c005e

.field public static final primary_text_disabled_material_dark:I = 0x7f0c005f

.field public static final primary_text_disabled_material_light:I = 0x7f0c0060

.field public static final ripple_material_dark:I = 0x7f0c0061

.field public static final ripple_material_light:I = 0x7f0c0062

.field public static final secondary_text_default_material_dark:I = 0x7f0c0063

.field public static final secondary_text_default_material_light:I = 0x7f0c0064

.field public static final secondary_text_disabled_material_dark:I = 0x7f0c0065

.field public static final secondary_text_disabled_material_light:I = 0x7f0c0066

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0c0068

.field public static final switch_thumb_disabled_material_light:I = 0x7f0c0069

.field public static final switch_thumb_material_dark:I = 0x7f0c00cb

.field public static final switch_thumb_material_light:I = 0x7f0c00cc

.field public static final switch_thumb_normal_material_dark:I = 0x7f0c006a

.field public static final switch_thumb_normal_material_light:I = 0x7f0c006b

.field public static final tooltip_background_dark:I = 0x7f0c006c

.field public static final tooltip_background_light:I = 0x7f0c006d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
