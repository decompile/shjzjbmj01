.class final enum Lcom/qmo/game/mpsdk/utils/Report$Singleton;
.super Ljava/lang/Enum;
.source "Report.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qmo/game/mpsdk/utils/Report;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Singleton"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/qmo/game/mpsdk/utils/Report$Singleton;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qmo/game/mpsdk/utils/Report$Singleton;

.field public static final enum INSTANCE:Lcom/qmo/game/mpsdk/utils/Report$Singleton;


# instance fields
.field private instance:Lcom/qmo/game/mpsdk/utils/Report;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 43
    new-instance v0, Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/qmo/game/mpsdk/utils/Report$Singleton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qmo/game/mpsdk/utils/Report$Singleton;->INSTANCE:Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    const/4 v0, 0x1

    .line 42
    new-array v0, v0, [Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    sget-object v1, Lcom/qmo/game/mpsdk/utils/Report$Singleton;->INSTANCE:Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    aput-object v1, v0, v2

    sput-object v0, Lcom/qmo/game/mpsdk/utils/Report$Singleton;->$VALUES:[Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    new-instance p1, Lcom/qmo/game/mpsdk/utils/Report;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lcom/qmo/game/mpsdk/utils/Report;-><init>(Lcom/qmo/game/mpsdk/utils/Report$1;)V

    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$Singleton;->instance:Lcom/qmo/game/mpsdk/utils/Report;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qmo/game/mpsdk/utils/Report$Singleton;
    .locals 1

    .line 42
    const-class v0, Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    return-object p0
.end method

.method public static values()[Lcom/qmo/game/mpsdk/utils/Report$Singleton;
    .locals 1

    .line 42
    sget-object v0, Lcom/qmo/game/mpsdk/utils/Report$Singleton;->$VALUES:[Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    invoke-virtual {v0}, [Lcom/qmo/game/mpsdk/utils/Report$Singleton;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    return-object v0
.end method


# virtual methods
.method public getInstance()Lcom/qmo/game/mpsdk/utils/Report;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$Singleton;->instance:Lcom/qmo/game/mpsdk/utils/Report;

    return-object v0
.end method
