.class final Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4;
.super Ljava/lang/Object;
.source "MpsdkNativeUtils.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->initLifecycle(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    const/4 p1, 0x0

    .line 953
    invoke-static {p1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1200(Z)V

    .line 955
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->getInstance()Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->unregisteScreenShotFileObserver()V

    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    const/4 p1, 0x1

    .line 930
    invoke-static {p1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1200(Z)V

    .line 932
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->getInstance()Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;

    move-result-object p1

    new-instance v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4$1;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4$1;-><init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4;)V

    invoke-virtual {p1, v0}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->registerScreenShotFileObserver(Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;)V

    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method
