.class Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetMetaDataByKeyHandler;
.super Ljava/lang/Object;
.source "MpsdkNativeUtils.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetMetaDataByKeyHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V
    .locals 0

    .line 724
    invoke-direct {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetMetaDataByKeyHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-string p1, "MpsdkNativeUtils"

    const-string p2, "doing GetMetaDataByKeyHandler invoke..."

    .line 727
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    if-eqz p3, :cond_1

    .line 728
    array-length p2, p3

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    aget-object v0, p3, p2

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    aget-object p2, p3, p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_1
    :goto_0
    move-object p2, p1

    :goto_1
    const-string p3, "MpsdkNativeUtils"

    .line 729
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doing GetMetaDataByKeyHandler invoke...msg = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string p3, "MPSDK_CHANNEL"

    .line 730
    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_2

    const-string p3, "getMetaDataByKey"

    .line 731
    invoke-static {p2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getMetaDataByKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1000(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    const-string p3, "MPSDK_IMEI"

    .line 732
    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_3

    const-string p3, "getDeviceId"

    .line 733
    invoke-static {p2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getMetaDataByKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1000(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const-string p3, "MPSDK_ANDROID_ID"

    .line 734
    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    const-string p2, "getAndroidId"

    .line 735
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getAndroidId()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1000(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_2
    return-object p1
.end method
