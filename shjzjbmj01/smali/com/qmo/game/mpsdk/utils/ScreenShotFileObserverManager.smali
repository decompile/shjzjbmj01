.class public Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;
.super Ljava/lang/Object;
.source "ScreenShotFileObserverManager.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager$Singleton;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ObserverManager"


# instance fields
.field private KEYWORDS:[Ljava/lang/String;

.field private SCREENSHOT_ROOT_PATH:Ljava/lang/String;

.field private SCREENSHOT_ROOT_PATH_DCIM:Ljava/lang/String;

.field private SCREENSHOT_ROOT_PATH_PICTURES:Ljava/lang/String;

.field private screenShotFileObserver:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;

.field private screenshotPath:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 3

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenShotFileObserver:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;

    const/16 v0, 0xe

    .line 36
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "screenshots"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "screenshot"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "screen_shot"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "screen-shot"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "screen shot"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "screencapture"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "screen_capture"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "screen-capture"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "screen capture"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "screencap"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string v1, "screen_cap"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string v1, "screen-cap"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string v1, "screen cap"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string v1, "\u622a\u5c4f"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->KEYWORDS:[Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->SCREENSHOT_ROOT_PATH:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->SCREENSHOT_ROOT_PATH_DCIM:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->SCREENSHOT_ROOT_PATH_PICTURES:Ljava/lang/String;

    const-string v0, ""

    .line 55
    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenshotPath:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager$1;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;
    .locals 1

    .line 28
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager$Singleton;->INSTANCE:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager$Singleton;

    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager$Singleton;->getInstance()Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public registerScreenShotFileObserver(Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;)V
    .locals 8

    const-string v0, "ObserverManager"

    const-string v1, "registerScreenShotFileObserver"

    .line 61
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "ObserverManager"

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "screenshot_root_path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->SCREENSHOT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "ObserverManager"

    .line 63
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "screenshot_path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenshotPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->KEYWORDS:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, v0, v2

    .line 75
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->SCREENSHOT_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ObserverManager"

    .line 76
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\u641c\u67e5 keyword = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "ObserverManager"

    .line 77
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenshotPath:Ljava/lang/String;

    const-string v0, "ObserverManager"

    .line 80
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u627e\u5230\u4e86\u622a\u56fe\u6587\u4ef6\u5939 path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenshotPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 83
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->SCREENSHOT_ROOT_PATH_DCIM:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ObserverManager"

    .line 84
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dcimPath = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenshotPath:Ljava/lang/String;

    const-string v0, "ObserverManager"

    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u627e\u5230\u4e86\u622a\u56fe\u6587\u4ef6\u5939 path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenshotPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 90
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->SCREENSHOT_ROOT_PATH_PICTURES:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ObserverManager"

    .line 91
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "picPath = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenshotPath:Ljava/lang/String;

    const-string v0, "ObserverManager"

    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u627e\u5230\u4e86\u622a\u56fe\u6587\u4ef6\u5939 path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenshotPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 98
    :cond_3
    :goto_1
    new-instance v0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenshotPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenShotFileObserver:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;

    .line 99
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenShotFileObserver:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;

    invoke-virtual {v0, p1}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->setLister(Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;)V

    .line 100
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenShotFileObserver:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;

    invoke-virtual {p1}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->startWatching()V

    return-void
.end method

.method public unregisteScreenShotFileObserver()V
    .locals 2

    const-string v0, "ObserverManager"

    const-string v1, "unregisteScreenShotFileObserver"

    .line 107
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenShotFileObserver:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenShotFileObserver:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;

    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->removeLister()V

    .line 110
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserverManager;->screenShotFileObserver:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;

    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->stopWatching()V

    :cond_0
    return-void
.end method
