.class public final enum Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;
.super Ljava/lang/Enum;
.source "SPManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qmo/game/mpsdk/utils/SPManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Singleton"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

.field public static final enum INSTANCE:Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;


# instance fields
.field private instance:Lcom/qmo/game/mpsdk/utils/SPManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 34
    new-instance v0, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;->INSTANCE:Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

    const/4 v0, 0x1

    .line 33
    new-array v0, v0, [Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

    sget-object v1, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;->INSTANCE:Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

    aput-object v1, v0, v2

    sput-object v0, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;->$VALUES:[Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    new-instance p1, Lcom/qmo/game/mpsdk/utils/SPManager;

    invoke-direct {p1}, Lcom/qmo/game/mpsdk/utils/SPManager;-><init>()V

    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;->instance:Lcom/qmo/game/mpsdk/utils/SPManager;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;
    .locals 1

    .line 33
    const-class v0, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

    return-object p0
.end method

.method public static values()[Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;
    .locals 1

    .line 33
    sget-object v0, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;->$VALUES:[Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

    invoke-virtual {v0}, [Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;

    return-object v0
.end method


# virtual methods
.method public getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/SPManager$Singleton;->instance:Lcom/qmo/game/mpsdk/utils/SPManager;

    return-object v0
.end method
