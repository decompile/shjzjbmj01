.class Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetNativeAppOpenIdHandler;
.super Ljava/lang/Object;
.source "MpsdkNativeUtils.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetNativeAppOpenIdHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V
    .locals 0

    .line 678
    invoke-direct {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetNativeAppOpenIdHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-string p1, "MpsdkNativeUtils"

    const-string p2, "doing GetNativeAppOpenIdHandler invoke..."

    .line 681
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    if-eqz p3, :cond_1

    .line 683
    array-length p2, p3

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    aget-object v0, p3, p2

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    aget-object p2, p3, p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_1
    :goto_0
    move-object p2, p1

    .line 684
    :goto_1
    invoke-static {p2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getNativeAppOpenId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string v0, "MpsdkNativeUtils"

    .line 685
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doing GetNativeAppOpenIdHandler invoke...msg = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , openid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_2

    .line 686
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_2

    return-object p1

    :cond_2
    const-string p2, "MpsdkNativeUtils"

    const-string v0, "doing GetNativeAppOpenIdHandler invoke, msg is null, send openid to egret "

    .line 689
    invoke-static {p2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string p2, "getNativeAppOpenId"

    .line 690
    invoke-static {p2, p3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1000(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method
