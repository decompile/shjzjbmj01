.class final Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;
.super Ljava/lang/Object;
.source "MpsdkNativeUtils.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/utils/PermissionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->initMPSDK(Landroid/app/Activity;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$onInitCallbackListener:Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;)V
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;->val$onInitCallbackListener:Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public complete()V
    .locals 2

    const-string v0, "MpsdkNativeUtils"

    const-string v1, "\u6743\u9650\u7533\u8bf7\u5b8c\u6210"

    .line 149
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 153
    invoke-static {v0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$002(Z)Z

    .line 154
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;->val$onInitCallbackListener:Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;

    invoke-interface {v0}, Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;->complete()V

    return-void
.end method

.method public denied(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 140
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "MpsdkNativeUtils"

    .line 141
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MPSDK\u6743\u9650\u7533\u8bf7\u672a\u901a\u8fc7:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    return-void
.end method

.method public granted()V
    .locals 0

    return-void
.end method
