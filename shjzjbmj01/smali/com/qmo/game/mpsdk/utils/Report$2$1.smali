.class Lcom/qmo/game/mpsdk/utils/Report$2$1;
.super Ljava/lang/Object;
.source "Report.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/utils/Report$2;->complete(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

.field final synthetic val$plt:Ljava/lang/String;

.field final synthetic val$sbParams:Ljava/lang/StringBuffer;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/utils/Report$2;Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 429
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$sbParams:Ljava/lang/StringBuffer;

    iput-object p3, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$url:Ljava/lang/String;

    iput-object p4, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$plt:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;)V
    .locals 7

    const-string v0, "Report"

    .line 447
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "report transform wacth video error="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    .line 450
    :try_start_0
    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object v1, v1, Lcom/qmo/game/mpsdk/utils/Report$2;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/Report;->access$200(Lcom/qmo/game/mpsdk/utils/Report;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$url:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 451
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object v0, v0, Lcom/qmo/game/mpsdk/utils/Report$2;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    invoke-static {v0}, Lcom/qmo/game/mpsdk/utils/Report;->access$200(Lcom/qmo/game/mpsdk/utils/Report;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$url:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_0
    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    .line 454
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object v1, v0, Lcom/qmo/game/mpsdk/utils/Report$2;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object v2, v0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$openId:Ljava/lang/String;

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object v3, v0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$gameId:Ljava/lang/String;

    const v4, 0xf3fec

    iget-object v5, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$url:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "platform="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$plt:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 457
    :cond_1
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object p1, p1, Lcom/qmo/game/mpsdk/utils/Report$2;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    invoke-static {p1}, Lcom/qmo/game/mpsdk/utils/Report;->access$200(Lcom/qmo/game/mpsdk/utils/Report;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object p1

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$url:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object v0, p1, Lcom/qmo/game/mpsdk/utils/Report$2;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object v1, p1, Lcom/qmo/game/mpsdk/utils/Report$2;->val$gameId:Ljava/lang/String;

    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object v2, p1, Lcom/qmo/game/mpsdk/utils/Report$2;->val$openId:Ljava/lang/String;

    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-wide v3, p1, Lcom/qmo/game/mpsdk/utils/Report$2;->val$accountCreateTime:J

    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget v5, p1, Lcom/qmo/game/mpsdk/utils/Report$2;->val$platfrom:I

    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget v6, p1, Lcom/qmo/game/mpsdk/utils/Report$2;->val$type:I

    invoke-static/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->access$300(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;JII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 460
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public onSuccess(Ljava/lang/String;)V
    .locals 3

    const-string v0, "Report"

    .line 432
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "report transform watch video response = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object v0, v0, Lcom/qmo/game/mpsdk/utils/Report$2;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$sbParams:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/qmo/game/mpsdk/utils/Report;->access$100(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    :try_start_0
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object p1, p1, Lcom/qmo/game/mpsdk/utils/Report$2;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    invoke-static {p1}, Lcom/qmo/game/mpsdk/utils/Report;->access$200(Lcom/qmo/game/mpsdk/utils/Report;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object p1

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$url:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 437
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->this$1:Lcom/qmo/game/mpsdk/utils/Report$2;

    iget-object p1, p1, Lcom/qmo/game/mpsdk/utils/Report$2;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    invoke-static {p1}, Lcom/qmo/game/mpsdk/utils/Report;->access$200(Lcom/qmo/game/mpsdk/utils/Report;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object p1

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$2$1;->val$url:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 440
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method
