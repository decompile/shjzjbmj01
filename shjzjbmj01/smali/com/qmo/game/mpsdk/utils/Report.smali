.class public Lcom/qmo/game/mpsdk/utils/Report;
.super Ljava/lang/Object;
.source "Report.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qmo/game/mpsdk/utils/Report$Singleton;
    }
.end annotation


# static fields
.field private static final EVENT_ID_ACTIVE_NO_999401:I = 0xf3fe9

.field private static final EVENT_ID_ACTIVE_YES_999400:I = 0xf3fe8

.field private static final EVENT_ID_ERROR_999404:I = 0xf3fec

.field private static final EVENT_ID_NEXT_DAY_RETENTION_NO_999403:I = 0xf3feb

.field private static final EVENT_ID_NEXT_DAY_RETENTION_YES_999402:I = 0xf3fea

.field private static final EVENT_ID_WATCH_VIDEO_NO_999406:I = 0xf3fee

.field private static final EVENT_ID_WATCH_VIDEO_YES_999405:I = 0xf3fed

.field private static final HTTP_STATE_CODE_FAIL_2001:I = 0x7d1

.field private static final HTTP_STATE_CODE_FAIL_3003:I = 0xbbb

.field private static final HTTP_STATE_CODE_SUCCESS_2000:I = 0x7d0

.field private static final REPORT_AD_COMTENT_CENSUS_SERVER:Ljava/lang/String; = "https://xyxcck-log-ad.raink.com.cn/MiniGameLog/log/adContentCensus.action?"

.field private static final REPORT_EVENT_SERVER:Ljava/lang/String; = "https://xyxcck-log.raink.com.cn"

.field private static final REPORT_GDT_TRANS_SERVER:Ljava/lang/String; = "https://xyx-app-ad.raink.com.cn/tx/up?"

.field private static final REPORT_KS_TRANS_SERVER:Ljava/lang/String; = "https://xyx-app-ad.raink.com.cn/active/index?"

.field private static final REPORT_TT_TRANS_SERVER:Ljava/lang/String; = "https://xyx-app-ad.raink.com.cn/up/index?"

.field private static final TAG:Ljava/lang/String; = "Report"

.field public static final TRANSFORM_ACTIVE_SCENE_LOGIN_SUCCESS:I = 0x2

.field public static final TRANSFORM_ACTIVE_SCENE_START:I = 0x1

.field public static final TRANSFORM_ACTIVE_SCENE_VIDEO_END:I = 0x3

.field private static final TRANSFORM_NEXT_DAY_RETENTION:I = 0x6

.field private static final TRANSFORM_PLATFORM_GDT:I = 0x3

.field private static final TRANSFORM_PLATFORM_KS:I = 0x2

.field private static final TRANSFORM_PLATFORM_TT:I = 0x1

.field public static final TRANSFORM_WATCH_VIDEO_SCENE:I = 0x55


# instance fields
.field private GameOpenid:Ljava/lang/String;

.field private mActiveUserId:Ljava/lang/String;

.field private mCreateTime:J

.field private mGameId:Ljava/lang/String;

.field private mNextDayRetentionUserId:Ljava/lang/String;

.field private reTryMap:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 68
    iput-wide v0, p0, Lcom/qmo/game/mpsdk/utils/Report;->mCreateTime:J

    const-string v0, ""

    .line 159
    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    const-string v0, ""

    .line 160
    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report;->mNextDayRetentionUserId:Ljava/lang/String;

    const-string v0, ""

    .line 161
    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    .line 472
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report;->reTryMap:Ljava/util/concurrent/ConcurrentMap;

    return-void
.end method

.method synthetic constructor <init>(Lcom/qmo/game/mpsdk/utils/Report$1;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/qmo/game/mpsdk/utils/Report;-><init>()V

    return-void
.end method

.method private _getOpenid()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report;->GameOpenid:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report;->GameOpenid:Ljava/lang/String;

    return-object v0

    .line 167
    :cond_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getLocalOpenId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/qmo/game/mpsdk/utils/Report;->doHandleReportTransformWatchVideoResponse(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/qmo/game/mpsdk/utils/Report;)Ljava/util/concurrent/ConcurrentMap;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/qmo/game/mpsdk/utils/Report;->reTryMap:Ljava/util/concurrent/ConcurrentMap;

    return-object p0
.end method

.method static synthetic access$300(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 0

    .line 37
    invoke-direct/range {p0 .. p6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void
.end method

.method static synthetic access$400(Lcom/qmo/game/mpsdk/utils/Report;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/qmo/game/mpsdk/utils/Report;->doHandleReportTransformResponse(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 0

    .line 37
    invoke-direct/range {p0 .. p6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransform(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void
.end method

.method private checkNeedActive(IIJ)Z
    .locals 4

    const/4 p1, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p2, v0, :cond_0

    const/4 v2, 0x2

    if-eq p2, v2, :cond_0

    if-eq p2, p1, :cond_0

    return v1

    :cond_0
    if-lt p2, v0, :cond_6

    if-le p2, p1, :cond_1

    goto :goto_0

    .line 582
    :cond_1
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/qmo/game/mpsdk/utils/SPManager;->getTransformActiveState()I

    move-result v2

    .line 584
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/qmo/game/mpsdk/utils/SPManager;->isTransformProductDownline()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string p1, "Report"

    const-string p2, "report transform active state is close, stop report"

    .line 585
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_2
    const/16 v3, 0xc8

    if-ne v2, v3, :cond_3

    const-string p1, "Report"

    const-string p2, "report transform active state is success, stop report"

    .line 590
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_3
    if-eqz v2, :cond_4

    if-eq p2, v2, :cond_4

    const-string p1, "Report"

    const-string p2, "report transform active scene is not match, stop report"

    .line 596
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_4
    if-ne p2, p1, :cond_5

    .line 600
    invoke-direct {p0, p3, p4}, Lcom/qmo/game/mpsdk/utils/Report;->isToday(J)Z

    move-result p1

    if-nez p1, :cond_5

    const-string p1, "Report"

    const-string p2, "report transform active scene is 3,isToday = false, stop report"

    .line 601
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_5
    const-string p1, "Report"

    const-string p2, "report transform active begin..."

    .line 605
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_6
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform active type is not exist"

    .line 579
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method private checkNeedNextDayRetention(IIJ)Z
    .locals 1

    const/4 p1, 0x0

    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    return p1

    .line 613
    :cond_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object p2

    invoke-virtual {p2}, Lcom/qmo/game/mpsdk/utils/SPManager;->getTransformNextDayRetentionState()I

    move-result p2

    .line 615
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/SPManager;->isTransformProductDownline()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p2, "Report"

    const-string p3, "report transform next day retention state is close, stop report"

    .line 616
    invoke-static {p2, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return p1

    :cond_1
    const/16 v0, 0xc8

    if-ne p2, v0, :cond_2

    const-string p2, "Report"

    const-string p3, "report transform next day retention state is success, stop report"

    .line 622
    invoke-static {p2, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return p1

    .line 626
    :cond_2
    invoke-direct {p0, p3, p4}, Lcom/qmo/game/mpsdk/utils/Report;->isYeaterday(J)Z

    move-result p2

    if-nez p2, :cond_3

    const-string p2, "Report"

    const-string p3, "report transform next day retention is not next day retention user, stop report"

    .line 627
    invoke-static {p2, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return p1

    :cond_3
    const-string p1, "Report"

    const-string p2, "report transform next day retention begin..."

    .line 630
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x1

    return p1
.end method

.method private doHandleReportTransformActiveResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    if-eqz p1, :cond_7

    .line 672
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_5

    :cond_0
    const/4 v0, 0x0

    .line 676
    :try_start_0
    invoke-static {p1, v0}, Lcom/qmo/game/mpsdk/utils/JsonObjectCoder;->decode(Ljava/lang/String;Ljava/lang/Void;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "code"

    .line 677
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x7d0

    if-eq v2, v1, :cond_5

    const/16 v3, 0xbbb

    if-ne v3, v1, :cond_1

    goto/16 :goto_3

    :cond_1
    const-string v2, "message"

    .line 690
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string v2, ""

    const-string v3, "message"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    const-string v2, "message"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    :goto_0
    const-string v2, ""

    :goto_1
    const-string v3, "data"

    .line 691
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v3, "state"

    .line 692
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const-string v4, "active_type"

    .line 693
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const-string v5, "platform"

    .line 694
    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    if-nez v3, :cond_4

    .line 696
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    const/16 v3, 0x190

    invoke-virtual {v0, v3}, Lcom/qmo/game/mpsdk/utils/SPManager;->putTransformActiveState(I)V

    goto :goto_2

    .line 698
    :cond_4
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/qmo/game/mpsdk/utils/SPManager;->putTransformActiveState(I)V

    .line 700
    :goto_2
    iget-object v6, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    iget-object v7, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const v8, 0xf3fe9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "code="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", msg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v5, p0

    move-object v9, p2

    invoke-virtual/range {v5 .. v10}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string v0, "Report"

    const-string v1, "report transform active fail."

    .line 701
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_5
    :goto_3
    const-string v0, "Report"

    const-string v3, "report transform active tt success."

    .line 679
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    const/16 v3, 0xc8

    invoke-virtual {v0, v3}, Lcom/qmo/game/mpsdk/utils/SPManager;->putTransformActiveState(I)V

    if-ne v2, v1, :cond_6

    .line 683
    iget-object v5, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    iget-object v6, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const v7, 0xf3fe8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v4, p0

    move-object v8, p2

    invoke-virtual/range {v4 .. v9}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    return-void

    :catch_0
    move-exception v0

    .line 704
    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    iget-object v3, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const v4, 0xf3fe9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "res="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 705
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_4
    return-void

    :cond_7
    :goto_5
    return-void
.end method

.method private doHandleReportTransformNextDayRetentionResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    if-eqz p1, :cond_7

    .line 712
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_5

    :cond_0
    const/4 v0, 0x0

    .line 716
    :try_start_0
    invoke-static {p1, v0}, Lcom/qmo/game/mpsdk/utils/JsonObjectCoder;->decode(Ljava/lang/String;Ljava/lang/Void;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "code"

    .line 717
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x7d0

    if-eq v2, v1, :cond_5

    const/16 v3, 0xbbb

    if-ne v3, v1, :cond_1

    goto/16 :goto_3

    :cond_1
    const-string v2, "message"

    .line 729
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string v2, ""

    const-string v3, "message"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    const-string v2, "message"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    :goto_0
    const-string v2, ""

    :goto_1
    const-string v3, "data"

    .line 730
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v3, "state"

    .line 731
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const-string v4, "platform"

    .line 733
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    if-nez v3, :cond_4

    .line 735
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    const/16 v3, 0x190

    invoke-virtual {v0, v3}, Lcom/qmo/game/mpsdk/utils/SPManager;->putTransformNextDayRetentionState(I)V

    goto :goto_2

    .line 737
    :cond_4
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/qmo/game/mpsdk/utils/SPManager;->putTransformNextDayRetentionState(I)V

    .line 739
    :goto_2
    iget-object v5, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    iget-object v6, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const v7, 0xf3feb

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "code="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", msg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v4, p0

    move-object v8, p2

    invoke-virtual/range {v4 .. v9}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string v0, "Report"

    const-string v1, "report transform next day retention fail."

    .line 740
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_5
    :goto_3
    const-string v0, "Report"

    const-string v3, "report transform next day retention success."

    .line 719
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    const/16 v3, 0xc8

    invoke-virtual {v0, v3}, Lcom/qmo/game/mpsdk/utils/SPManager;->putTransformNextDayRetentionState(I)V

    if-ne v2, v1, :cond_6

    .line 723
    iget-object v5, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    iget-object v6, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const v7, 0xf3fea

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v4, p0

    move-object v8, p2

    invoke-virtual/range {v4 .. v9}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    return-void

    :catch_0
    move-exception v0

    .line 743
    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    iget-object v3, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const v4, 0xf3feb

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "res="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 744
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_4
    return-void

    :cond_7
    :goto_5
    return-void
.end method

.method private doHandleReportTransformResponse(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 566
    invoke-direct {p0, p2, p3}, Lcom/qmo/game/mpsdk/utils/Report;->doHandleReportTransformNextDayRetentionResponse(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 568
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/qmo/game/mpsdk/utils/Report;->doHandleReportTransformActiveResponse(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private doHandleReportTransformWatchVideoResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    if-eqz p1, :cond_6

    .line 635
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    :cond_0
    const/4 v0, 0x0

    .line 639
    :try_start_0
    invoke-static {p1, v0}, Lcom/qmo/game/mpsdk/utils/JsonObjectCoder;->decode(Ljava/lang/String;Ljava/lang/Void;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "code"

    .line 640
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x7d0

    if-eq v2, v1, :cond_5

    const/16 v2, 0xbbb

    if-ne v2, v1, :cond_1

    goto/16 :goto_2

    :cond_1
    const-string v2, "message"

    .line 654
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string v2, ""

    const-string v3, "message"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    const-string v2, "message"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    :goto_0
    const-string v2, ""

    :goto_1
    const-string v3, "data"

    .line 655
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v3, "state"

    .line 656
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    .line 658
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    const/16 v3, 0x190

    invoke-virtual {v0, v3}, Lcom/qmo/game/mpsdk/utils/SPManager;->putTransformWatchVideoState(I)V

    .line 660
    :cond_4
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    iget-object v3, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const v4, 0xf3fee

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "code="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", msg="

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", res="

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, v0

    move-object v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string v0, "Report"

    const-string v1, "report transform watch video fail."

    .line 661
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    :goto_2
    const-string v0, "Report"

    const-string v1, "report transform watch video success."

    .line 642
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    iget-object v3, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    iget-object v4, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const v5, 0xf3fed

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v2, p0

    move-object v6, p2

    invoke-virtual/range {v2 .. v7}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 664
    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    iget-object v3, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const v4, 0xf3fee

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "res="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 665
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_3
    return-void

    :cond_6
    :goto_4
    return-void
.end method

.method public static getInstance()Lcom/qmo/game/mpsdk/utils/Report;
    .locals 1

    .line 56
    sget-object v0, Lcom/qmo/game/mpsdk/utils/Report$Singleton;->INSTANCE:Lcom/qmo/game/mpsdk/utils/Report$Singleton;

    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/Report$Singleton;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    return-object v0
.end method

.method private isToday(J)Z
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    const-string p1, "Report"

    const-string p2, "\u4e0d\u4f1a\u6fc0\u6d3b\u573a\u666f3\u4e0a\u62a5--\u300b\u539f\u56e0\uff1aaccountCreateTime\u4e0d\u5b58\u5728\uff0c\u8bf7\u5728mpsdk.init\u6216\u8005mpsdk.initWithAccount\u6210\u529f\u540e\uff0c\u5e76\u4e14\u5728\u89c6\u9891\u770b\u5b8c\u540e\u4e0a\u62a5."

    .line 764
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    return p1

    .line 768
    :cond_0
    invoke-static {p1, p2}, Lcom/qmo/game/mpsdk/utils/DateTimeUtil;->isToday(J)Z

    move-result p1

    return p1
.end method

.method private isYeaterday(J)Z
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    const-string p1, "Report"

    const-string p2, "\u4e0d\u4f1a\u6b21\u7559\u4e0a\u62a5--\u300b\u539f\u56e0\uff1acreateTime\u4e0d\u5b58\u5728\uff0c\u8bf7\u5728mpsdk.init\u6216\u8005mpsdk.initWithAccount\u6210\u529f\u540e\u5728\u4e0a\u62a5."

    .line 754
    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    return p1

    .line 758
    :cond_0
    invoke-static {p1, p2}, Lcom/qmo/game/mpsdk/utils/DateTimeUtil;->isYeaterday(J)Z

    move-result p1

    return p1
.end method

.method private static reportAdContentCensusEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 7

    if-eqz p0, :cond_a

    .line 939
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    if-eqz p1, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_d

    :cond_0
    if-nez p4, :cond_1

    const-string p0, "Report"

    const-string p1, "mpsdk--reportAdContentCensusEvent---\u5e94\u7528\u4fe1\u606f\u4e0d\u5168,\u65e0\u6cd5\u63d0\u4ea4"

    .line 944
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    if-nez p4, :cond_2

    :try_start_0
    const-string p0, "Report"

    const-string p1, "mpsdk--reportAdContentCensusEvent---adObj is null. "

    .line 950
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception p0

    goto/16 :goto_6

    :catch_1
    move-exception p0

    goto/16 :goto_7

    :catch_2
    move-exception p0

    goto/16 :goto_8

    :catch_3
    move-exception p0

    goto/16 :goto_9

    :catch_4
    move-exception p0

    goto/16 :goto_a

    :catch_5
    move-exception p0

    goto/16 :goto_b

    .line 953
    :cond_2
    invoke-static {p4}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p4

    if-eqz p4, :cond_9

    const-string v0, "packagename"

    .line 954
    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "packagename"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto/16 :goto_5

    :cond_3
    const-string v0, "platform"

    .line 959
    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "platform"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string v0, ""

    :goto_0
    const-string v1, "appname"

    .line 960
    invoke-virtual {p4, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "appname"

    invoke-virtual {p4, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_5
    const-string v1, ""

    :goto_1
    const-string v2, "packagename"

    .line 961
    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "packagename"

    invoke-virtual {p4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_6
    const-string v2, ""

    :goto_2
    const-string v3, "desc"

    .line 962
    invoke-virtual {p4, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "desc"

    invoke-virtual {p4, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    goto :goto_3

    :cond_7
    const-string p4, ""

    :goto_3
    if-eqz p5, :cond_8

    const-string v3, "1"

    goto :goto_4

    :cond_8
    const-string v3, "0"

    .line 964
    :goto_4
    new-instance v4, Ljava/lang/StringBuffer;

    const-string v5, "https://xyxcck-log-ad.raink.com.cn/MiniGameLog/log/adContentCensus.action?"

    invoke-direct {v4, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v5, "gameid="

    .line 965
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "userid="

    .line 966
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "platform="

    .line 967
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "appname="

    .line 968
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "mainid="

    .line 969
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "subid="

    .line 970
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "packagename="

    .line 971
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "desc="

    .line 972
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "companyname="

    .line 973
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "isclickad="

    .line 974
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "Report"

    .line 976
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mpsdk--reportAdContentCensusEvent---gameid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ", userid = "

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ", platform = "

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ", appname = "

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ", mainid = "

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " , subid="

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ",packagename="

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ",desc="

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ",isclickad="

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Lcom/qmo/game/mpsdk/utils/Report$6;

    invoke-direct {p1}, Lcom/qmo/game/mpsdk/utils/Report$6;-><init>()V

    invoke-static {p0, p1}, Lcom/qmo/game/mpsdk/utils/HttpUtil;->get(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V

    goto :goto_c

    :cond_9
    :goto_5
    const-string p0, "Report"

    const-string p1, "mpsdk--reportAdContentCensusEvent---pkginfo is null. "

    .line 955
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 1000
    :goto_6
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_c

    .line 998
    :goto_7
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_c

    .line 996
    :goto_8
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_c

    .line 994
    :goto_9
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_c

    .line 992
    :goto_a
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_c

    .line 990
    :goto_b
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :goto_c
    return-void

    :cond_a
    :goto_d
    const-string p0, "Report"

    const-string p1, "mpsdk--reportAdContentCensusEvent---\u8bf7\u5148\u8d4b\u503c\u53d8\u91cf\u540e\u518d\u4e0a\u62a5"

    .line 940
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private reportEvent999006(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 855
    new-instance v0, Lcom/qmo/game/mpsdk/utils/Report$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/qmo/game/mpsdk/utils/Report$5;-><init>(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/Report$5;->start()V

    return-void
.end method

.method private reportTransform(Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 12

    move-object v9, p0

    move-object v4, p1

    move-object v5, p2

    move-wide v7, p3

    move/from16 v2, p5

    move/from16 v3, p6

    const-string v0, "Report"

    .line 475
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "report transform ==> gameId "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " , openId="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " , accountCreateTime="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " , platform="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " , type="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_2

    if-nez v5, :cond_0

    goto :goto_0

    .line 480
    :cond_0
    invoke-direct {p0, v2, v3, v7, v8}, Lcom/qmo/game/mpsdk/utils/Report;->checkNeedActive(IIJ)Z

    move-result v0

    .line 481
    invoke-direct {p0, v2, v3, v7, v8}, Lcom/qmo/game/mpsdk/utils/Report;->checkNeedNextDayRetention(IIJ)Z

    move-result v1

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    return-void

    .line 487
    :cond_1
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/AppUtil;->getCurContext()Landroid/content/Context;

    move-result-object v10

    .line 488
    new-instance v11, Lcom/qmo/game/mpsdk/utils/Report$3;

    move-object v0, v11

    move-object v1, p0

    move/from16 v2, p5

    move/from16 v3, p6

    move-object v4, p1

    move-object v5, p2

    move-object v6, v10

    move-wide v7, p3

    invoke-direct/range {v0 .. v8}, Lcom/qmo/game/mpsdk/utils/Report$3;-><init>(Lcom/qmo/game/mpsdk/utils/Report;IILjava/lang/String;Ljava/lang/String;Landroid/content/Context;J)V

    invoke-static {v10, v11}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getOAID(Landroid/content/Context;Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;)V

    return-void

    :cond_2
    :goto_0
    const-string v0, "Report"

    const-string v1, "report transform params is error"

    .line 477
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private reportTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 11

    move-object v4, p1

    move-object v5, p2

    const-string v0, "Report"

    .line 383
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "report transform watch video ==> gameId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , openId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , accountCreateTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide v7, p3

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " , platform="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v2, p5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " , type="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v3, p6

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_2

    if-nez v5, :cond_0

    goto :goto_0

    .line 389
    :cond_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/SPManager;->isTransformProductWatchVideoDownline()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Report"

    const-string v1, "report transform watch video state is close, stop report"

    .line 390
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 394
    :cond_1
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/AppUtil;->getCurContext()Landroid/content/Context;

    move-result-object v9

    .line 395
    new-instance v10, Lcom/qmo/game/mpsdk/utils/Report$2;

    move-object v0, v10

    move-object v1, p0

    move/from16 v2, p5

    move/from16 v3, p6

    move-object v4, p1

    move-object v5, p2

    move-object v6, v9

    move-wide v7, p3

    invoke-direct/range {v0 .. v8}, Lcom/qmo/game/mpsdk/utils/Report$2;-><init>(Lcom/qmo/game/mpsdk/utils/Report;IILjava/lang/String;Ljava/lang/String;Landroid/content/Context;J)V

    invoke-static {v9, v10}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getOAID(Landroid/content/Context;Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;)V

    return-void

    :cond_2
    :goto_0
    const-string v0, "Report"

    const-string v1, "report transform watch video params is error"

    .line 385
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public reportAdColseEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 6

    .line 924
    invoke-direct {p0, p1, p2}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent999006(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    move-object v1, p2

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move v5, p7

    .line 925
    invoke-static/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportAdContentCensusEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V

    return-void
.end method

.method public reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 9

    if-eqz p1, :cond_5

    .line 181
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 186
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "https://xyxcck-log.raink.com.cn"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "/MiniGameLog/log/event.action?"

    .line 187
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "gameid="

    .line 188
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "userid="

    .line 189
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "eventid="

    .line 190
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "param1="

    .line 191
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "utf-8"

    invoke-static {p4, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    :goto_0
    const-string v1, ""

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "param2="

    .line 192
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz p5, :cond_4

    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    goto :goto_2

    :cond_3
    const-string v1, "utf-8"

    invoke-static {p5, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_4
    :goto_2
    const-string v1, ""

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 193
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v8, Lcom/qmo/game/mpsdk/utils/Report$1;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/qmo/game/mpsdk/utils/Report$1;-><init>(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v8}, Lcom/qmo/game/mpsdk/utils/HttpUtil;->get(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    :cond_5
    :goto_4
    const-string p1, "Report"

    const-string p2, "reportEvent-->\u8bf7\u4f20\u5165userid"

    .line 182
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportEvent999005(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8

    .line 814
    new-instance v7, Lcom/qmo/game/mpsdk/utils/Report$4;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p5

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report$4;-><init>(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    invoke-virtual {v7}, Lcom/qmo/game/mpsdk/utils/Report$4;->start()V

    return-void
.end method

.method public reportEvent999007(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    :try_start_0
    const-string v0, "Report"

    const-string v1, "mpsdk--reportEvent999007---\u5728\u70b9\u51fb\u89c6\u9891\u5165\u53e3\u6309\u94ae\u65f6\u8c03\u7528\u4e86"

    .line 785
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_1

    .line 786
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 790
    :cond_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v1

    const v4, 0xf3e5f    # 1.399907E-39f

    const-string v6, ""

    move-object v2, p2

    move-object v3, p1

    move-object v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :goto_0
    const-string p3, "Report"

    .line 787
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mpsdk--reportEvent999007---gameid|openid is null. gameId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ",openId="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    :goto_1
    return-void
.end method

.method public reportGDTTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 7

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 341
    :cond_0
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    .line 342
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p4

    move v6, p3

    .line 343
    invoke-direct/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransform(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform active gdt params is error"

    .line 338
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportGDTTransformNextDayRetention(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 359
    :cond_0
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mNextDayRetentionUserId:Ljava/lang/String;

    .line 360
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const/4 v5, 0x3

    const/4 v6, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    .line 361
    invoke-direct/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransform(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform next day retention gdt params is error"

    .line 356
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportGDTTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 377
    :cond_0
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    .line 378
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const/4 v5, 0x3

    const/16 v6, 0x55

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    .line 379
    invoke-direct/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform watch video ks params is error"

    .line 374
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportKSTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 7

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 286
    :cond_0
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    .line 287
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p4

    move v6, p3

    .line 288
    invoke-direct/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransform(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform active ks params is error"

    .line 283
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportKSTransformNextDayRetention(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 322
    :cond_0
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mNextDayRetentionUserId:Ljava/lang/String;

    .line 323
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const/4 v5, 0x2

    const/4 v6, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    .line 324
    invoke-direct/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransform(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform next day retention ks params is error"

    .line 319
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportKSTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 304
    :cond_0
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    .line 305
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const/4 v5, 0x2

    const/16 v6, 0x55

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    .line 306
    invoke-direct/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform watch video ks params is error"

    .line 301
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportTTTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 8

    const-string v0, "Report"

    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "report transform active tt scene = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-lt p3, v0, :cond_3

    const/4 v0, 0x3

    if-le p3, v0, :cond_0

    goto :goto_1

    :cond_0
    if-eqz p1, :cond_2

    if-nez p2, :cond_1

    goto :goto_0

    .line 230
    :cond_1
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    .line 231
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p4

    move v7, p3

    .line 232
    invoke-direct/range {v1 .. v7}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransform(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void

    :cond_2
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform active tt params is error"

    .line 226
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    :goto_1
    const-string p1, "Report"

    const-string p2, "report transform active tt scene is not exist"

    .line 222
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportTTTransformNextDayRetention(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 266
    :cond_0
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mNextDayRetentionUserId:Ljava/lang/String;

    .line 267
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    .line 268
    invoke-direct/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransform(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform next day retention tt params is error"

    .line 263
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportTTTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 248
    :cond_0
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report;->mActiveUserId:Ljava/lang/String;

    .line 249
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report;->mGameId:Ljava/lang/String;

    const/4 v5, 0x1

    const/16 v6, 0x55

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    .line 250
    invoke-direct/range {v0 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;JII)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "Report"

    const-string p2, "report transform watch video ks params is error"

    .line 245
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public reportUserNameAndUnionID(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "10"

    .line 902
    invoke-static {v0, p3, p4, p2, p1}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportAppUserAttr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
