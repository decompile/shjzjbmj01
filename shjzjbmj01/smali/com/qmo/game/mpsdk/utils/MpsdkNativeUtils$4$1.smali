.class Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4$1;
.super Ljava/lang/Object;
.source "MpsdkNativeUtils.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4;->onActivityResumed(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4;)V
    .locals 0

    .line 932
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4$1;->this$0:Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public beganScreenShot(Ljava/lang/String;)V
    .locals 3

    const-string v0, "MpsdkNativeUtils"

    .line 940
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "beganScreenShot path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string p1, "5"

    const-string v0, ""

    const-string v1, ""

    .line 942
    invoke-static {p1, v0, v1}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportAppUserAttr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public finshScreenShot(Ljava/lang/String;)V
    .locals 3

    const-string v0, "MpsdkNativeUtils"

    .line 935
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finshScreenShot path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
