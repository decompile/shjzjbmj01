.class public Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;
.super Ljava/lang/Object;
.source "ADInfoGetUtils.java"


# static fields
.field private static final BaseUrl:Ljava/lang/String; = "https://xyxcck-log-ad.raink.com.cn/MiniGameLog/log/adContentCensus.action"

.field public static GAMEID:Ljava/lang/String; = null

.field private static TAG:Ljava/lang/String; = "QMOSDK::ADInfoGetUtils"

.field public static USERID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetAdInfo(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 19
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u89e3\u6790\u5bf9\u8c61\u4e0d\u80fd\u4e3anull"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    .line 22
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.bytedance.msdk.api.reward.TTRewardAd"

    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 24
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_MOBRAIN(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_1
    const-string v2, "com.anythink.rewardvideo.api.ATRewardVideoAd"

    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 26
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_TOPON(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v2, "com.bytedance.sdk.openadsdk.TTRewardVideoAd"

    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 28
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_3
    const-string v2, "com.qq.e.ads.rewardvideo.RewardVideoAD"

    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 30
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_4
    const-string v2, "com.kwad.sdk.api.KsRewardVideoAd"

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 32
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    .line 34
    :cond_5
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u89e3\u6790\u5bf9\u8c61\u6ca1\u6709\u9002\u914d:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public static GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 105
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_CSJ_3300(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 107
    :cond_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u7a7f\u5c71\u75323300\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_CSJ_3251(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_1

    return-object p0

    .line 111
    :cond_1
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u7a7f\u5c71\u75323251\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method private static GetAdInfo_CSJ_3251(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 8

    .line 168
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u7a7f\u5c71\u75323251\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "b"

    .line 171
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "V"

    .line 173
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    return-object v0

    :cond_2
    const-string v2, "k"

    .line 175
    invoke-static {p0, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 176
    instance-of v3, v2, Ljava/lang/String;

    if-nez v3, :cond_3

    return-object v0

    .line 177
    :cond_3
    check-cast v2, Ljava/lang/String;

    const-string v3, "b"

    .line 178
    invoke-static {v1, v3}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 179
    instance-of v4, v3, Ljava/lang/String;

    if-nez v4, :cond_4

    return-object v0

    .line 180
    :cond_4
    check-cast v3, Ljava/lang/String;

    const-string v4, "e"

    .line 181
    invoke-static {v1, v4}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 182
    instance-of v5, v4, Ljava/lang/String;

    if-nez v5, :cond_5

    return-object v0

    .line 183
    :cond_5
    check-cast v4, Ljava/lang/String;

    const-string v5, "f"

    .line 184
    invoke-static {v1, v5}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 185
    instance-of v5, v1, Ljava/lang/String;

    if-nez v5, :cond_6

    return-object v0

    .line 186
    :cond_6
    check-cast v1, Ljava/lang/String;

    const-string v5, ""

    if-eqz v1, :cond_7

    const-string v6, ""

    .line 188
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_7
    const-string v1, "l"

    .line 189
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 190
    instance-of v6, v1, Ljava/lang/String;

    if-nez v6, :cond_8

    return-object v0

    .line 191
    :cond_8
    check-cast v1, Ljava/lang/String;

    :cond_9
    if-eqz v1, :cond_a

    const-string v6, ""

    .line 193
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    :cond_a
    const-string v1, "r"

    .line 194
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 195
    instance-of v1, p0, Ljava/lang/String;

    if-nez v1, :cond_b

    return-object v0

    .line 196
    :cond_b
    move-object v1, p0

    check-cast v1, Ljava/lang/String;

    .line 198
    :cond_c
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "type = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->CSJ:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " desc = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " packagename:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " appname:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " companyname:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->CSJ:Ljava/lang/String;

    invoke-static {p0, v4, v2, v1, v5}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 215
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 213
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 211
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 209
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 207
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 205
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 203
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 201
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private static GetAdInfo_CSJ_3300(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 8

    .line 227
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u7a7f\u5c71\u75323300\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "b"

    .line 230
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "c"

    .line 232
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "l"

    .line 233
    invoke-static {p0, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    const-string v6, ""

    const-string v7, "ah"

    .line 238
    invoke-static {p0, v7}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    if-eqz p0, :cond_5

    .line 240
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "developer_name"

    .line 242
    invoke-virtual {v4, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    const-string p0, "developer_name"

    invoke-virtual {v4, p0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_2
    const-string p0, ""

    :goto_0
    const-string v5, "package_name"

    .line 243
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "package_name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_3
    const-string v5, ""

    :goto_1
    const-string v6, "app_name"

    .line 244
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "app_name"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_4
    const-string v4, ""

    :goto_2
    move-object v6, v4

    goto :goto_3

    :cond_5
    move-object p0, v4

    :goto_3
    if-eqz v1, :cond_6

    const-string v4, "a"

    .line 248
    invoke-static {v1, v4}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v4, ""

    .line 249
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    goto :goto_4

    :cond_6
    move-object v1, v3

    .line 253
    :goto_4
    sget-object v3, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "type = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->CSJ:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " desc = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " packagename:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " appname:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " companyname:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->CSJ:Ljava/lang/String;

    invoke-static {p0, v5, v2, v6, v1}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 270
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_5

    :catch_1
    move-exception p0

    .line 268
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_5

    :catch_2
    move-exception p0

    .line 266
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_5

    :catch_3
    move-exception p0

    .line 264
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_5

    :catch_4
    move-exception p0

    .line 262
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_5

    :catch_5
    move-exception p0

    .line 260
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_5

    :catch_6
    move-exception p0

    .line 258
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_5

    :catch_7
    move-exception p0

    .line 256
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_5
    return-object v0
.end method

.method public static GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 126
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT_43321202(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 128
    :cond_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a43321202\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT_42941164(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    .line 132
    :cond_1
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42941164\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT_42511121(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_2

    return-object v0

    .line 136
    :cond_2
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42511121\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT_42701140(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_3

    return-object p0

    .line 140
    :cond_3
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42701140\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method private static GetAdInfo_GDT_42511121(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 6

    .line 278
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42511121\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "c"

    .line 281
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "a"

    .line 283
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "u"

    .line 285
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 287
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 289
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "r"

    .line 291
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_6

    return-object v0

    :cond_6
    const-string v3, "b"

    .line 293
    invoke-static {p0, v1, v3}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    const-string v1, "a"

    .line 294
    invoke-static {v2, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "h"

    .line 295
    invoke-static {v2, v3}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 297
    sget-object v3, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v5, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " desc = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " packagename:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " appname:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    sget-object v3, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->GDT:Ljava/lang/String;

    const-string v4, ""

    invoke-static {v3, v1, p0, v2, v4}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 314
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 312
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 310
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 308
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 306
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 304
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 302
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 300
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private static GetAdInfo_GDT_42701140(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 7

    .line 451
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42701140\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "c"

    .line 454
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "a"

    .line 456
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "u"

    .line 458
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 460
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 462
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "q"

    .line 464
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_6

    return-object v0

    :cond_6
    const-string v3, "c"

    .line 466
    invoke-static {p0, v1, v3}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    const-string v1, "a"

    .line 467
    invoke-static {v2, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "h"

    .line 468
    invoke-static {v2, v3}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, ""

    .line 470
    sget-object v4, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " desc = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " packagename:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " appname:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    sget-object v4, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-static {v4, v1, p0, v2, v3}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 487
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 485
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 483
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 481
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 479
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 477
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 475
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 473
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private static GetAdInfo_GDT_42941164(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 9

    .line 387
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42941164\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "c"

    .line 390
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "a"

    .line 392
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "h"

    .line 394
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 396
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-nez v1, :cond_4

    return-object v0

    .line 397
    :cond_4
    invoke-static {p0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_5

    return-object v0

    :cond_5
    const/4 v1, 0x0

    .line 398
    invoke-static {p0, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_18

    const-string v1, ""

    .line 399
    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    goto/16 :goto_b

    .line 400
    :cond_6
    new-instance v1, Lorg/json/JSONObject;

    check-cast p0, Ljava/lang/String;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "desc"

    .line 402
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_7

    const-string p0, "desc"

    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_7
    move-object p0, v0

    :goto_0
    const-string v2, "txt"

    .line 403
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "txt"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_8
    move-object v2, v0

    :goto_1
    const-string v3, ""

    const-string v4, "corporate_logo"

    .line 405
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "corporate_logo"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_9
    move-object v4, v0

    :goto_2
    const-string v5, "endcard_info"

    .line 406
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v5, "endcard_info"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    goto :goto_3

    :cond_a
    move-object v5, v0

    :goto_3
    if-eqz v5, :cond_10

    const-string v6, "appname"

    .line 408
    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    const-string v6, "appname"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    :cond_b
    move-object v6, v0

    :goto_4
    const-string v7, "desc"

    .line 409
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    const-string v7, "desc"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :cond_c
    move-object v7, v0

    :goto_5
    const-string v8, "img2"

    .line 410
    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    const-string v8, "img2"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_d
    move-object v5, v0

    :goto_6
    if-eqz v6, :cond_e

    const-string v8, ""

    .line 411
    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_e

    move-object v2, v6

    :cond_e
    if-eqz v7, :cond_f

    const-string v6, ""

    .line 412
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_f

    move-object p0, v7

    :cond_f
    if-eqz v5, :cond_10

    const-string v6, ""

    .line 413
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_10

    move-object v4, v5

    :cond_10
    const-string v5, "ext"

    .line 415
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_11

    const-string v5, "ext"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    goto :goto_7

    :cond_11
    move-object v1, v0

    :goto_7
    if-eqz v1, :cond_17

    const-string v5, "appname"

    .line 417
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    const-string v5, "appname"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_8

    :cond_12
    move-object v5, v0

    :goto_8
    const-string v6, "packagename"

    .line 418
    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_13

    const-string v6, "packagename"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_9

    :cond_13
    move-object v6, v0

    :goto_9
    const-string v7, "pkg_name"

    .line 419
    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_14

    const-string v7, "pkg_name"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_a

    :cond_14
    move-object v1, v0

    :goto_a
    if-eqz v5, :cond_15

    const-string v7, ""

    .line 420
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_15

    move-object v2, v5

    :cond_15
    if-eqz v6, :cond_16

    const-string v5, ""

    .line 421
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_16

    move-object v3, v6

    :cond_16
    if-nez v3, :cond_17

    if-eqz v1, :cond_17

    const-string v5, ""

    .line 423
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_17

    move-object v3, v1

    .line 426
    :cond_17
    sget-object v1, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " desc = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " packagename:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " appname:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    sget-object v1, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-static {v1, v3, p0, v2, v4}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :cond_18
    :goto_b
    return-object v0

    :catch_0
    move-exception p0

    .line 443
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_c

    :catch_1
    move-exception p0

    .line 441
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_c

    :catch_2
    move-exception p0

    .line 439
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_c

    :catch_3
    move-exception p0

    .line 437
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_c

    :catch_4
    move-exception p0

    .line 435
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_c

    :catch_5
    move-exception p0

    .line 433
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_c

    :catch_6
    move-exception p0

    .line 431
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_c

    :catch_7
    move-exception p0

    .line 429
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_c
    return-object v0
.end method

.method private static GetAdInfo_GDT_43321202(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 10

    .line 322
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a43321202\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "c"

    .line 325
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "a"

    .line 327
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "g"

    .line 329
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 331
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 333
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "E"

    .line 335
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/json/JSONObject;

    if-nez p0, :cond_6

    return-object v0

    :cond_6
    const-string v1, "desc"

    .line 337
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "desc"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_7
    const-string v1, ""

    :goto_0
    const-string v2, "txt"

    .line 338
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "txt"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_8
    const-string v2, ""

    :goto_1
    const-string v3, "corporation_name"

    .line 339
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "corporation_name"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_9
    const-string v3, ""

    :goto_2
    const-string v4, ""

    const-string v5, "corporate_logo"

    .line 341
    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v5, "corporate_logo"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_a
    move-object v5, v0

    :goto_3
    const-string v6, "endcard_info"

    .line 342
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    const-string v6, "endcard_info"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    goto :goto_4

    :cond_b
    move-object v6, v0

    :goto_4
    if-eqz v6, :cond_11

    const-string v7, "appname"

    .line 344
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    const-string v7, "appname"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :cond_c
    move-object v7, v0

    :goto_5
    const-string v8, "desc"

    .line 345
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    const-string v8, "desc"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_6

    :cond_d
    move-object v8, v0

    :goto_6
    const-string v9, "img2"

    .line 346
    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    const-string v9, "img2"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_7

    :cond_e
    move-object v6, v0

    :goto_7
    if-eqz v7, :cond_f

    const-string v9, ""

    .line 347
    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_f

    move-object v2, v7

    :cond_f
    if-eqz v8, :cond_10

    const-string v7, ""

    .line 348
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_10

    move-object v1, v8

    :cond_10
    if-eqz v6, :cond_11

    const-string v7, ""

    .line 349
    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    move-object v5, v6

    :cond_11
    const-string v6, "ext"

    .line 351
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_12

    const-string v6, "ext"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    goto :goto_8

    :cond_12
    move-object p0, v0

    :goto_8
    if-eqz p0, :cond_18

    const-string v6, "appname"

    .line 353
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_13

    const-string v6, "appname"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_9

    :cond_13
    move-object v6, v0

    :goto_9
    const-string v7, "packagename"

    .line 354
    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_14

    const-string v7, "packagename"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_a

    :cond_14
    move-object v7, v0

    :goto_a
    const-string v8, "pkg_name"

    .line 355
    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_15

    const-string v8, "pkg_name"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_b

    :cond_15
    move-object p0, v0

    :goto_b
    if-eqz v6, :cond_16

    const-string v8, ""

    .line 356
    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_16

    move-object v2, v6

    :cond_16
    if-eqz v7, :cond_17

    const-string v6, ""

    .line 357
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_17

    move-object v4, v7

    :cond_17
    if-nez v4, :cond_18

    if-eqz p0, :cond_18

    const-string v6, ""

    .line 359
    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_18

    move-object v4, p0

    .line 362
    :cond_18
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "type = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " desc = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " packagename:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " appname:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " companyname:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-static {p0, v4, v1, v2, v5}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 379
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_c

    :catch_1
    move-exception p0

    .line 377
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_c

    :catch_2
    move-exception p0

    .line 375
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_c

    :catch_3
    move-exception p0

    .line 373
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_c

    :catch_4
    move-exception p0

    .line 371
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_c

    :catch_5
    move-exception p0

    .line 369
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_c

    :catch_6
    move-exception p0

    .line 367
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_c

    :catch_7
    move-exception p0

    .line 365
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_c
    return-object v0
.end method

.method public static GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 155
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_KS_330(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 157
    :cond_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5feb\u624b330\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_KS_336(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_1

    return-object p0

    .line 161
    :cond_1
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5feb\u624b336\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method private static GetAdInfo_KS_330(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 7

    .line 499
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5feb\u624b330\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "b"

    .line 502
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "adBaseInfo"

    .line 504
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "adDescription"

    .line 506
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "appPackageName"

    .line 507
    invoke-static {p0, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "appName"

    .line 508
    invoke-static {p0, v3}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "appIconUrl"

    .line 509
    invoke-static {p0, v4}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v3, :cond_3

    const-string v5, ""

    .line 510
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    const-string v3, "productName"

    .line 511
    invoke-static {p0, v3}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    move-object v3, p0

    check-cast v3, Ljava/lang/String;

    .line 513
    :cond_4
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->KS:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " desc = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " packagename:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " appname:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->KS:Ljava/lang/String;

    invoke-static {p0, v2, v1, v3, v4}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 530
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 528
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 526
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 524
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 522
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 520
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 518
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 516
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private static GetAdInfo_KS_336(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 7

    .line 538
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5feb\u624b336\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "mAdInfo"

    .line 541
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "adBaseInfo"

    .line 543
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "adDescription"

    .line 545
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "appPackageName"

    .line 546
    invoke-static {p0, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "appName"

    .line 547
    invoke-static {p0, v3}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "appIconUrl"

    .line 548
    invoke-static {p0, v4}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v3, :cond_3

    const-string v5, ""

    .line 549
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    const-string v3, "productName"

    .line 550
    invoke-static {p0, v3}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    move-object v3, p0

    check-cast v3, Ljava/lang/String;

    .line 552
    :cond_4
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->KS:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " desc = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " packagename:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " appname:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADChannelDef;->KS:Ljava/lang/String;

    invoke-static {p0, v2, v1, v3, v4}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 569
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 567
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 565
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 563
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 561
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 559
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 557
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 555
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static GetAdInfo_MOBRAIN(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 59
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_MOBRAIN_2600(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 61
    :cond_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Mobrain2600\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_MOBRAIN_2500(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    .line 65
    :cond_1
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Mobrain2500\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_MOBRAIN_2411(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_2

    return-object p0

    .line 69
    :cond_2
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Mobrain2411\u5e7f\u544a\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method public static GetAdInfo_MOBRAIN_2411(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getAdNetworkPlatformId"

    const/4 v2, 0x0

    .line 708
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_d

    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    goto/16 :goto_0

    :cond_1
    const-string v1, "a"

    .line 742
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    .line 744
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_3

    return-object v0

    .line 746
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 748
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "B"

    .line 750
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    :cond_6
    const-string v1, "b"

    .line 752
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 753
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "a"

    .line 712
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    .line 714
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_9

    return-object v0

    .line 716
    :cond_9
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_a

    return-object v0

    .line 718
    :cond_a
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_b

    return-object v0

    :cond_b
    const-string v2, "B"

    .line 720
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_c

    return-object v0

    :cond_c
    const-string v1, "b"

    .line 722
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 723
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_d
    const-string v1, "a"

    .line 727
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_e

    return-object v0

    .line 729
    :cond_e
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_f

    return-object v0

    .line 731
    :cond_f
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_10

    return-object v0

    .line 733
    :cond_10
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_11

    return-object v0

    :cond_11
    const-string v2, "B"

    .line 735
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_12

    return-object v0

    :cond_12
    const-string v1, "b"

    .line 737
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 738
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 775
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 773
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 771
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 769
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 767
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 765
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 763
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 761
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_8
    move-exception p0

    .line 759
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static GetAdInfo_MOBRAIN_2500(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getAdNetworkPlatformId"

    const/4 v2, 0x0

    .line 800
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_d

    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    goto/16 :goto_0

    :cond_1
    const-string v1, "a"

    .line 834
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    .line 836
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_3

    return-object v0

    .line 838
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 840
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "C"

    .line 842
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    :cond_6
    const-string v1, "b"

    .line 844
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 845
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "a"

    .line 804
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    .line 806
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_9

    return-object v0

    .line 808
    :cond_9
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_a

    return-object v0

    .line 810
    :cond_a
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_b

    return-object v0

    :cond_b
    const-string v2, "C"

    .line 812
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_c

    return-object v0

    :cond_c
    const-string v1, "b"

    .line 814
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 815
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_d
    const-string v1, "a"

    .line 819
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_e

    return-object v0

    .line 821
    :cond_e
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_f

    return-object v0

    .line 823
    :cond_f
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_10

    return-object v0

    .line 825
    :cond_10
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_11

    return-object v0

    :cond_11
    const-string v2, "C"

    .line 827
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_12

    return-object v0

    :cond_12
    const-string v1, "a"

    .line 829
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 830
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 867
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 865
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 863
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 861
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 859
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 857
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 855
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 853
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_8
    move-exception p0

    .line 851
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static GetAdInfo_MOBRAIN_2600(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getAdNetworkPlatformId"

    const/4 v2, 0x0

    .line 892
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_d

    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    goto/16 :goto_0

    :cond_1
    const-string v1, "a"

    .line 926
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    .line 928
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_3

    return-object v0

    .line 930
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 932
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "C"

    .line 934
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    :cond_6
    const-string v1, "a"

    .line 936
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 937
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "a"

    .line 896
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    .line 898
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_9

    return-object v0

    .line 900
    :cond_9
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_a

    return-object v0

    .line 902
    :cond_a
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_b

    return-object v0

    :cond_b
    const-string v2, "C"

    .line 904
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_c

    return-object v0

    :cond_c
    const-string v1, "a"

    .line 906
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 907
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_d
    const-string v1, "a"

    .line 911
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_e

    return-object v0

    .line 913
    :cond_e
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_f

    return-object v0

    .line 915
    :cond_f
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_10

    return-object v0

    .line 917
    :cond_10
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_11

    return-object v0

    :cond_11
    const-string v2, "C"

    .line 919
    invoke-static {p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_12

    return-object v0

    :cond_12
    const-string v1, "a"

    .line 921
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 922
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 959
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 957
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 955
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 953
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 951
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 949
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 947
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 945
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_8
    move-exception p0

    .line 943
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static GetAdInfo_TOPON(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 84
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_TOPON_5711(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 86
    :cond_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Topon5711\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_TOPON_571(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_1

    return-object p0

    .line 90
    :cond_1
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Topon571\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method private static GetAdInfo_TOPON_571(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getNetworkFirmId"

    const/4 v2, 0x0

    .line 637
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_7

    const/16 v2, 0xf

    if-eq v1, v2, :cond_4

    const/16 v2, 0x1c

    if-eq v1, v2, :cond_1

    goto/16 :goto_0

    :cond_1
    const-string v1, "mBaseAdapter"

    .line 657
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "b"

    .line 659
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 661
    :cond_3
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_4
    const-string v1, "mBaseAdapter"

    .line 649
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_5

    return-object v0

    :cond_5
    const-string v1, "e"

    .line 651
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    .line 653
    :cond_6
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "mBaseAdapter"

    .line 641
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    :cond_8
    const-string v1, "a"

    .line 643
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_9

    return-object v0

    .line 645
    :cond_9
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 683
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 681
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 679
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 677
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 675
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 673
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 671
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 669
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_8
    move-exception p0

    .line 667
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private static GetAdInfo_TOPON_5711(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getNetworkFirmId"

    const/4 v2, 0x0

    .line 580
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_7

    const/16 v2, 0xf

    if-eq v1, v2, :cond_4

    const/16 v2, 0x1c

    if-eq v1, v2, :cond_1

    goto/16 :goto_0

    :cond_1
    const-string v1, "mBaseAdapter"

    .line 600
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "b"

    .line 602
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 604
    :cond_3
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_4
    const-string v1, "mBaseAdapter"

    .line 592
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_5

    return-object v0

    :cond_5
    const-string v1, "f"

    .line 594
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    .line 596
    :cond_6
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "mBaseAdapter"

    .line 584
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    :cond_8
    const-string v1, "a"

    .line 586
    invoke-static {p0, v1}, Lcom/qmo/game/mpsdk/utils/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_9

    return-object v0

    .line 588
    :cond_9
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 626
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 624
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 622
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 620
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 618
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception p0

    .line 616
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception p0

    .line 614
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_7
    move-exception p0

    .line 612
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_8
    move-exception p0

    .line 610
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static Report(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Z)V
    .locals 7

    .line 975
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GAMEID:Ljava/lang/String;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->USERID:Ljava/lang/String;

    if-nez v0, :cond_0

    goto/16 :goto_7

    :cond_0
    if-eqz p2, :cond_7

    const-string v0, "packagename"

    .line 979
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "packagename"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_6

    :cond_1
    :try_start_0
    const-string v0, "platform"

    .line 984
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "platform"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    :goto_0
    const-string v1, "appname"

    .line 985
    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "appname"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string v1, ""

    :goto_1
    const-string v2, "packagename"

    .line 986
    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "packagename"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_4
    const-string v2, ""

    :goto_2
    const-string v3, "desc"

    .line 987
    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "desc"

    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_3

    :cond_5
    const-string p2, ""

    :goto_3
    if-eqz p3, :cond_6

    const-string v3, "1"

    goto :goto_4

    :cond_6
    const-string v3, "0"

    .line 989
    :goto_4
    new-instance v4, Ljava/lang/StringBuffer;

    const-string v5, "https://xyxcck-log-ad.raink.com.cn/MiniGameLog/log/adContentCensus.action"

    invoke-direct {v4, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v5, "gameid="

    .line 990
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v5, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GAMEID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "userid="

    .line 991
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v5, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->USERID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "platform="

    .line 992
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "appname="

    .line 993
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "mainid="

    .line 994
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "subid="

    .line 995
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "packagename="

    .line 996
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "desc="

    .line 997
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "companyname="

    .line 998
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "isclickad="

    .line 999
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1001
    sget-object v3, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mpsdk--report---gameid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GAMEID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", userid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->USERID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", platform = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", appname = "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", mainid = "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " , subid="

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ",packagename="

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ",desc="

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ",isclickad="

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils$1;

    invoke-direct {p1}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils$1;-><init>()V

    invoke-static {p0, p1}, Lcom/qmo/game/mpsdk/utils/HttpUtil;->get(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception p0

    .line 1026
    invoke-virtual {p0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_5

    :catch_1
    move-exception p0

    .line 1024
    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_5

    :catch_2
    move-exception p0

    .line 1022
    invoke-virtual {p0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_5

    :catch_3
    move-exception p0

    .line 1020
    invoke-virtual {p0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_5

    :catch_4
    move-exception p0

    .line 1018
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_5

    :catch_5
    move-exception p0

    .line 1016
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :goto_5
    return-void

    .line 980
    :cond_7
    :goto_6
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string p1, "\u5e94\u7528\u4fe1\u606f\u4e0d\u5168,\u65e0\u6cd5\u63d0\u4ea4"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 976
    :cond_8
    :goto_7
    sget-object p0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string p1, "\u8bf7\u5148\u8d4b\u503c\u53d8\u91cf\u540e\u518d\u4e0a\u62a5"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 11
    sget-object v0, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 40
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "packagename"

    .line 41
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "desc"

    .line 42
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "appname"

    .line 43
    invoke-virtual {v0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "platform"

    .line 44
    invoke-virtual {v0, p1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "appicon"

    .line 45
    invoke-virtual {v0, p0, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0
.end method
