.class final Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$5;
.super Ljava/lang/Object;
.source "MpsdkNativeUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 987
    :try_start_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1300()Z

    move-result v0

    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1400()Z

    move-result v1

    const-wide/16 v2, 0x3e8

    if-eq v0, v1, :cond_1

    .line 989
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1300()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 991
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1502(J)J

    const-string v0, "MpsdkNativeUtils"

    .line 992
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mpsdk--online: start time is "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1500()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 996
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    div-long/2addr v0, v2

    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1500()J

    move-result-wide v4

    const/4 v6, 0x0

    sub-long/2addr v0, v4

    const-string v4, "MpsdkNativeUtils"

    .line 998
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mpsdk--online: stay time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, "s , start time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1500()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    invoke-static {v0, v1}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportOnlineTime(J)V

    .line 1000
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1600()V

    .line 1004
    :goto_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1300()Z

    move-result v0

    invoke-static {v0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1402(Z)Z

    .line 1006
    :cond_1
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1300()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1007
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    div-long/2addr v0, v2

    .line 1008
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1500()J

    move-result-wide v4

    cmp-long v6, v4, v0

    if-lez v6, :cond_2

    .line 1009
    invoke-static {v0, v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1502(J)J

    .line 1011
    :cond_2
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1500()J

    move-result-wide v4

    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1700()I

    move-result v6

    int-to-long v6, v6

    sub-long v6, v0, v6

    cmp-long v8, v4, v6

    if-gtz v8, :cond_3

    const-string v4, "MpsdkNativeUtils"

    .line 1013
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mpsdk--online: stay time is 60s , start time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1500()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " , currTime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1502(J)J

    const-wide/16 v0, 0x3c

    .line 1016
    invoke-static {v0, v1}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportOnlineTime(J)V

    .line 1017
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1600()V

    .line 1020
    :cond_3
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1800()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    return-void
.end method
