.class Lcom/qmo/game/mpsdk/utils/Report$5;
.super Ljava/lang/Thread;
.source "Report.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/utils/Report;->reportEvent999006(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qmo/game/mpsdk/utils/Report;

.field final synthetic val$gameId:Ljava/lang/String;

.field final synthetic val$openId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 855
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$gameId:Ljava/lang/String;

    iput-object p3, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$openId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    :try_start_0
    const-string v0, "Report"

    const-string v1, "mpsdk--reportEvent999006---\u5728\u70b9\u51fb\u89c6\u9891\u5173\u95ed\u7684\u65f6\u5019\u8c03\u7528\u4e86"

    .line 859
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$gameId:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$gameId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$openId:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$openId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_7

    .line 865
    :cond_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/SPManager;->getAdInfosMap()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 866
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto/16 :goto_6

    .line 869
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 870
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    const-string v4, ""

    const-string v5, ""

    const-wide/16 v6, 0x0

    if-eqz v3, :cond_8

    .line 874
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_8

    const-string v4, "param1"

    .line 875
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_4

    const-string v4, "param1"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_1

    :cond_3
    const-string v4, "param1"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_2

    :cond_4
    :goto_1
    const-string v4, ""

    :goto_2
    const-string v5, "param2"

    .line 876
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_6

    const-string v5, "param2"

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_3

    :cond_5
    const-string v5, "param2"

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    goto :goto_4

    :cond_6
    :goto_3
    const-string v5, ""

    :goto_4
    const-string v8, "clickTime"

    .line 877
    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_8

    const-string v8, "clickTime"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_7

    goto :goto_5

    :cond_7
    const-string v6, "clickTime"

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    :cond_8
    :goto_5
    move-object v12, v4

    move-object v13, v5

    .line 879
    invoke-static {v2, v6, v7}, Lcom/qmo/game/mpsdk/utils/AppUtil;->findAppByPkg(Ljava/lang/String;J)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 880
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v8

    iget-object v9, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$openId:Ljava/lang/String;

    iget-object v10, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$gameId:Ljava/lang/String;

    const v11, 0xf3e5e    # 1.399906E-39f

    invoke-virtual/range {v8 .. v13}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 881
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/qmo/game/mpsdk/utils/SPManager;->delAdInfo(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    :goto_6
    return-void

    :cond_a
    :goto_7
    const-string v0, "Report"

    .line 861
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mpsdk--reportEvent999006---gameid|openid|adObj is null. gameId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$gameId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " ,openId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/Report$5;->val$openId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    :cond_b
    return-void
.end method
