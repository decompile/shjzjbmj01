.class final Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$7;
.super Ljava/lang/Object;
.source "MpsdkNativeUtils.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getOAID(Landroid/content/Context;Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$onGetOAIDLinstener:Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;)V
    .locals 0

    .line 1144
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$7;->val$onGetOAIDLinstener:Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOAIDGetComplete(Ljava/lang/String;)V
    .locals 3

    const-string v0, "MpsdkNativeUtils"

    .line 1147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOAIDGetComplete====>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$7;->val$onGetOAIDLinstener:Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;

    invoke-interface {v0, p1}, Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;->complete(Ljava/lang/String;)V

    return-void
.end method

.method public onOAIDGetError(Ljava/lang/Exception;)V
    .locals 3

    const-string v0, "MpsdkNativeUtils"

    .line 1154
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOAIDGetError====>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$7;->val$onGetOAIDLinstener:Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;

    const-string v0, ""

    invoke-interface {p1, v0}, Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;->complete(Ljava/lang/String;)V

    return-void
.end method
