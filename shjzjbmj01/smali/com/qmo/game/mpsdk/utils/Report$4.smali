.class Lcom/qmo/game/mpsdk/utils/Report$4;
.super Ljava/lang/Thread;
.source "Report.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/utils/Report;->reportEvent999005(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qmo/game/mpsdk/utils/Report;

.field final synthetic val$adObj:Ljava/lang/Object;

.field final synthetic val$gameId:Ljava/lang/String;

.field final synthetic val$openId:Ljava/lang/String;

.field final synthetic val$placementId:Ljava/lang/String;

.field final synthetic val$scene:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/utils/Report;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 814
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$gameId:Ljava/lang/String;

    iput-object p3, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$openId:Ljava/lang/String;

    iput-object p4, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$adObj:Ljava/lang/Object;

    iput-object p5, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$scene:Ljava/lang/String;

    iput-object p6, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$placementId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    :try_start_0
    const-string v0, "Report"

    const-string v1, "mpsdk--reportEvent999005---\u5728\u70b9\u51fb\u89c6\u9891\u5185\u5bb9\u65f6\u8c03\u7528\u4e86"

    .line 820
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$gameId:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$gameId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$openId:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$openId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    .line 826
    :cond_0
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$adObj:Ljava/lang/Object;

    if-nez v0, :cond_1

    const-string v0, "Report"

    const-string v1, "mpsdk--reportEvent999005---adObj is null. "

    .line 827
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v2

    iget-object v3, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$openId:Ljava/lang/String;

    iget-object v4, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$gameId:Ljava/lang/String;

    const v5, 0xf3e5d    # 1.399904E-39f

    const-string v6, ""

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$scene:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void

    .line 831
    :cond_1
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$adObj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/qmo/game/mpsdk/utils/ADInfoGetUtils;->GetAdInfo(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "Report"

    const-string v1, "mpsdk--reportEvent999005---adJsonObject is null. "

    .line 833
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v2

    iget-object v3, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$openId:Ljava/lang/String;

    iget-object v4, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$gameId:Ljava/lang/String;

    const v5, 0xf3e5d    # 1.399904E-39f

    const-string v6, ""

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$scene:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v1, "platform"

    .line 837
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "platform"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    const-string v1, ""

    :goto_0
    const-string v2, "packagename"

    .line 838
    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "packagename"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, ""

    :goto_1
    move-object v3, v0

    .line 839
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v4

    iget-object v5, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$openId:Ljava/lang/String;

    iget-object v6, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$gameId:Ljava/lang/String;

    const v7, 0xf3e5d    # 1.399904E-39f

    iget-object v8, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$placementId:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$scene:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 840
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/SPManager;->getInstance()Lcom/qmo/game/mpsdk/utils/SPManager;

    move-result-object v2

    iget-object v4, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$placementId:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "_"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$scene:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "_"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Lcom/qmo/game/mpsdk/utils/SPManager;->putAdInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_3

    :cond_5
    :goto_2
    const-string v0, "Report"

    .line 822
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mpsdk--reportEvent999005---gameid|openid|adObj is null. gameId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$gameId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " ,openId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$openId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 842
    :catch_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v2

    iget-object v3, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$openId:Ljava/lang/String;

    iget-object v4, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$gameId:Ljava/lang/String;

    const v5, 0xf3e5d    # 1.399904E-39f

    const-string v6, ""

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$4;->val$scene:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    :goto_3
    return-void
.end method
