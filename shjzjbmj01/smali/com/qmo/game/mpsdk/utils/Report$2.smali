.class Lcom/qmo/game/mpsdk/utils/Report$2;
.super Ljava/lang/Object;
.source "Report.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/utils/Report;->reportTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;JII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qmo/game/mpsdk/utils/Report;

.field final synthetic val$accountCreateTime:J

.field final synthetic val$gameId:Ljava/lang/String;

.field final synthetic val$mContext:Landroid/content/Context;

.field final synthetic val$openId:Ljava/lang/String;

.field final synthetic val$platfrom:I

.field final synthetic val$type:I


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/utils/Report;IILjava/lang/String;Ljava/lang/String;Landroid/content/Context;J)V
    .locals 0

    .line 395
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->this$0:Lcom/qmo/game/mpsdk/utils/Report;

    iput p2, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$platfrom:I

    iput p3, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$type:I

    iput-object p4, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$gameId:Ljava/lang/String;

    iput-object p5, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$openId:Ljava/lang/String;

    iput-object p6, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$mContext:Landroid/content/Context;

    iput-wide p7, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$accountCreateTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public complete(Ljava/lang/String;)V
    .locals 5

    const-string v0, "Report"

    .line 398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "report transform watch video ==> oaid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , platform="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$platfrom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " , type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "v="

    .line 400
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {}, Lcom/qmo/game/mpsdk/utils/AppUtil;->getVersionCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "gameid="

    .line 401
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$gameId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "openid="

    .line 402
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$openId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "imei="

    .line 403
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "meid="

    .line 404
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getMEID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "mac="

    .line 405
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getMac(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "androidid="

    .line 406
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "type="

    .line 407
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "oaid="

    .line 408
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p1, "&"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p1, "device_id="

    .line 409
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getLocalDeviceUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p1, ""

    const-string v1, ""

    .line 414
    iget v2, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$platfrom:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const-string p1, "https://xyx-app-ad.raink.com.cn/up/index?"

    const-string v1, "TT"

    goto :goto_0

    .line 417
    :cond_0
    iget v2, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$platfrom:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    const-string p1, "https://xyx-app-ad.raink.com.cn/active/index?"

    const-string v1, "KS"

    goto :goto_0

    .line 420
    :cond_1
    iget v2, p0, Lcom/qmo/game/mpsdk/utils/Report$2;->val$platfrom:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    const-string p1, "https://xyx-app-ad.raink.com.cn/tx/up?"

    const-string v1, "GDT"

    .line 427
    :cond_2
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "Report"

    .line 428
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "report transform watch video request => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    new-instance v2, Lcom/qmo/game/mpsdk/utils/Report$2$1;

    invoke-direct {v2, p0, v0, p1, v1}, Lcom/qmo/game/mpsdk/utils/Report$2$1;-><init>(Lcom/qmo/game/mpsdk/utils/Report$2;Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, v2}, Lcom/qmo/game/mpsdk/utils/HttpUtil;->get(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V

    return-void
.end method
