.class public Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;
.super Ljava/lang/Object;
.source "MpsdkNativeUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$SetSkipParamsHandler;,
        Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$ClickAppHandler;,
        Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetMetaDataByKeyHandler;,
        Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetAppLaunchOptionsHandler;,
        Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetPhoneModelHandler;,
        Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetNativeAppOpenId2Handler;,
        Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetNativeAppOpenIdHandler;
    }
.end annotation


# static fields
.field private static final DIRECTORY_NAME:Ljava/lang/String; = ".mpsdk"

.field private static final FILE_NAME:Ljava/lang/String; = "mp_game_uid_"

.field private static GAME_ID:Ljava/lang/String; = "1"

.field private static INTERVALS_TIME:I = 0x0

.field public static final LOCATION_CODE:I = 0x12d

.field private static PERMISSIONS_NO_READ_PHONE_STATE:[Ljava/lang/String; = null

.field private static PERMISSIONS_STORAGE:[Ljava/lang/String; = null

.field private static final REQUEST_PERMISSIONS_STATE:I = 0x3e7

.field private static final TAG:Ljava/lang/String; = "MpsdkNativeUtils"

.field private static appOnForegroundCheckRunnable:Ljava/lang/Runnable; = null

.field private static isInitComplete:Z = false

.field private static lastLaunchOptions:Ljava/lang/String; = ""

.field private static launchOptions:Ljava/lang/String; = ""

.field private static locationListener:Landroid/location/LocationListener; = null

.field private static locationListener1:Landroid/location/LocationListener; = null

.field private static locationManager:Landroid/location/LocationManager; = null

.field private static locationProvider:Ljava/lang/String; = null

.field private static mContext:Landroid/app/Activity; = null

.field private static mEngineObj:Ljava/lang/Object; = null

.field private static mHandler:Landroid/os/Handler; = null

.field private static mIsAppOnForegroundB:Z = false

.field private static mIsAppRunningB:Z = false

.field private static mLastAppOnForeground:Z = false

.field private static mListener:Lcom/qmo/game/mpsdk/utils/PermissionListener; = null

.field private static mOaid:Ljava/lang/String; = ""

.field private static mOpenId:Ljava/lang/String; = ""

.field private static mStartTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 898
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    .line 976
    sput-boolean v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mLastAppOnForeground:Z

    const/16 v1, 0x3c

    .line 977
    sput v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->INTERVALS_TIME:I

    .line 978
    new-instance v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$5;

    invoke-direct {v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$5;-><init>()V

    sput-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->appOnForegroundCheckRunnable:Ljava/lang/Runnable;

    const/4 v1, 0x5

    .line 1050
    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v1, v0

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string v2, "android.permission.READ_PHONE_STATE"

    const/4 v4, 0x2

    aput-object v2, v1, v4

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    const/4 v5, 0x3

    aput-object v2, v1, v5

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    const/4 v5, 0x4

    aput-object v2, v1, v5

    sput-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->PERMISSIONS_STORAGE:[Ljava/lang/String;

    .line 1052
    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v1, v0

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v0, v1, v3

    sput-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->PERMISSIONS_NO_READ_PHONE_STATE:[Ljava/lang/String;

    const/4 v0, 0x0

    .line 1181
    sput-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationProvider:Ljava/lang/String;

    .line 1263
    new-instance v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$8;

    invoke-direct {v0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$8;-><init>()V

    sput-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationListener:Landroid/location/LocationListener;

    .line 1308
    new-instance v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$9;

    invoke-direct {v0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$9;-><init>()V

    sput-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationListener1:Landroid/location/LocationListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .line 77
    sget-boolean v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->isInitComplete:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0

    .line 77
    sput-boolean p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->isInitComplete:Z

    return p0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .line 77
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mOaid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 77
    invoke-static {p0, p1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->invokeCallExternalInterface(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 77
    sput-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mOaid:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    .line 77
    invoke-static {p0, p1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->clickApp(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1200(Z)V
    .locals 0

    .line 77
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->setAppOnForeground(Z)V

    return-void
.end method

.method static synthetic access$1300()Z
    .locals 1

    .line 77
    sget-boolean v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mIsAppOnForegroundB:Z

    return v0
.end method

.method static synthetic access$1400()Z
    .locals 1

    .line 77
    sget-boolean v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mLastAppOnForeground:Z

    return v0
.end method

.method static synthetic access$1402(Z)Z
    .locals 0

    .line 77
    sput-boolean p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mLastAppOnForeground:Z

    return p0
.end method

.method static synthetic access$1500()J
    .locals 2

    .line 77
    sget-wide v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$1502(J)J
    .locals 0

    .line 77
    sput-wide p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mStartTime:J

    return-wide p0
.end method

.method static synthetic access$1600()V
    .locals 0

    .line 77
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->reportVpnUser()V

    return-void
.end method

.method static synthetic access$1700()I
    .locals 1

    .line 77
    sget v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->INTERVALS_TIME:I

    return v0
.end method

.method static synthetic access$1800()Landroid/os/Handler;
    .locals 1

    .line 77
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1900()V
    .locals 0

    .line 77
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getLocation()V

    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .line 77
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->GAME_ID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Landroid/location/Location;)Ljava/util/List;
    .locals 0

    .line 77
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getAddress(Landroid/location/Location;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$2100(Landroid/location/Location;)Ljava/util/List;
    .locals 0

    .line 77
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getAddress1(Landroid/location/Location;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static afterLogin(Ljava/lang/String;)V
    .locals 6

    .line 250
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->reportX86User()V

    .line 251
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->reportVpnUser()V

    .line 252
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->reportEvent999006()V

    .line 253
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    sget-object v2, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->GAME_ID:Ljava/lang/String;

    sget-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    sget-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getLocalDeviceUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const v3, 0xbdb8d

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 254
    new-instance p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$3;

    invoke-direct {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$3;-><init>()V

    .line 259
    invoke-virtual {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$3;->start()V

    return-void
.end method

.method private static clickApp(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    const-string v0, "MpsdkNativeUtils"

    .line 833
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clickApp -- packagename="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , landingPageUrl = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    .line 834
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 837
    :cond_0
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->isAppInstalled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string p1, "MpsdkNativeUtils"

    const-string v0, "clickApp -- app\u5df2\u5b58\u5728\uff0c\u542f\u52a8app"

    .line 838
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->launchAppWithPackage(Ljava/lang/String;)Z

    move-result p0

    return p0

    :cond_1
    if-eqz p1, :cond_3

    .line 843
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_2

    goto :goto_0

    :cond_2
    const-string p0, "MpsdkNativeUtils"

    const-string v0, "clickApp -- app\u4e0d\u5b58\u5728\uff0c\u6253\u5f00\u843d\u5730\u9875"

    .line 846
    invoke-static {p0, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    invoke-static {p1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->jump2LandingPage(Ljava/lang/String;)Z

    move-result p0

    return p0

    :cond_3
    :goto_0
    return v0

    :cond_4
    :goto_1
    return v0
.end method

.method private static getAddress(Landroid/location/Location;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            ")",
            "Ljava/util/List<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 1249
    :try_start_0
    new-instance v1, Landroid/location/Geocoder;

    sget-object v2, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 1250
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    .line 1251
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    const/4 v6, 0x1

    .line 1250
    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string v0, "MpsdkNativeUtils"

    .line 1252
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u83b7\u53d6\u5730\u5740\u4fe1\u606f\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->parseLaction(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "9.2"

    .line 1254
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, v0, p0}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportAppUserAttr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1255
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationManager:Landroid/location/LocationManager;

    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationListener:Landroid/location/LocationListener;

    invoke-virtual {p0, v0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    move-object v1, v0

    .line 1258
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_0
    move-object v1, v0

    :goto_1
    return-object v1
.end method

.method private static getAddress1(Landroid/location/Location;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            ")",
            "Ljava/util/List<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 1294
    :try_start_0
    new-instance v1, Landroid/location/Geocoder;

    sget-object v2, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 1295
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    .line 1296
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    const/4 v6, 0x1

    .line 1295
    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string v0, "MpsdkNativeUtils"

    .line 1297
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u83b7\u53d6\u5730\u5740\u4fe1\u606f1\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->parseLaction(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "9.2"

    .line 1299
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, v0, p0}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportAppUserAttr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationManager:Landroid/location/LocationManager;

    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationListener1:Landroid/location/LocationListener;

    invoke-virtual {p0, v0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    move-object v1, v0

    .line 1303
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_0
    move-object v1, v0

    :goto_1
    return-object v1
.end method

.method public static getAndroidId()Ljava/lang/String;
    .locals 3

    const-string v0, ""

    .line 819
    :try_start_0
    sget-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "android_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :catch_0
    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method public static getAppLaunchOptions()Ljava/lang/String;
    .locals 1

    .line 344
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getMpsdkPasteContent()V

    .line 349
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->launchOptions:Ljava/lang/String;

    return-object v0
.end method

.method public static getDeviceId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .line 413
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {v0}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getGameId()Ljava/lang/String;
    .locals 1

    .line 175
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->GAME_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static getLocalDeviceUUID(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .line 1108
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/UUIDUtil;->getInstance()Lcom/qmo/game/mpsdk/utils/UUIDUtil;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/qmo/game/mpsdk/utils/UUIDUtil;->getLocalUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "MpsdkNativeUtils"

    .line 1109
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uuid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method public static getLocalOpenId()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    :try_start_0
    const-string v1, ""

    .line 181
    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getNativeAppOpenId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :catch_0
    return-object v0
.end method

.method private static getLocation()V
    .locals 14

    .line 1184
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 1186
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    .line 1188
    invoke-static {v0, v1}, Landroidx/core/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "MpsdkNativeUtils"

    const-string v1, "\u83b7\u53d6\u4f4d\u7f6e\u5931\u8d25-\u7528\u6237\u672a\u6388\u6743"

    .line 1190
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "9.1"

    const-string v1, ""

    const-string v2, ""

    .line 1191
    invoke-static {v0, v1, v2}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportAppUserAttr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 1196
    :cond_1
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    sput-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationManager:Landroid/location/LocationManager;

    .line 1199
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationManager:Landroid/location/LocationManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v0

    const-string v1, "gps"

    .line 1201
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "gps"

    .line 1203
    sput-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationProvider:Ljava/lang/String;

    const-string v0, "MpsdkNativeUtils"

    const-string v1, "\u5b9a\u4f4d\u65b9\u5f0fGPS"

    .line 1204
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v1, "network"

    .line 1205
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "network"

    .line 1207
    sput-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationProvider:Ljava/lang/String;

    const-string v0, "MpsdkNativeUtils"

    const-string v1, "\u5b9a\u4f4d\u65b9\u5f0fNetwork"

    .line 1208
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1216
    :goto_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationManager:Landroid/location/LocationManager;

    sget-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "MpsdkNativeUtils"

    .line 1218
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u83b7\u53d6\u4e0a\u6b21\u7684\u4f4d\u7f6e-\u7ecf\u7eac\u5ea6\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    invoke-static {v0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getAddress(Landroid/location/Location;)Ljava/util/List;

    goto :goto_1

    :cond_3
    const-string v0, "MpsdkNativeUtils"

    const-string v1, "\u83b7\u53d6\u4e0a\u6b21\u7684\u4f4d\u7f6e\u4e3a\u7a7a"

    .line 1222
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1230
    :goto_1
    :try_start_0
    sget-object v2, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationManager:Landroid/location/LocationManager;

    const-string v3, "gps"

    const-wide/16 v4, 0xbb8

    const/4 v6, 0x0

    sget-object v7, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v2 .. v7}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1236
    :catch_0
    :try_start_1
    sget-object v8, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationManager:Landroid/location/LocationManager;

    const-string v9, "network"

    const-wide/16 v10, 0xbb8

    const/4 v12, 0x0

    sget-object v13, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->locationListener1:Landroid/location/LocationListener;

    invoke-virtual/range {v8 .. v13}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void

    :cond_4
    const-string v0, "MpsdkNativeUtils"

    const-string v1, "\u6ca1\u6709\u53ef\u7528\u7684\u4f4d\u7f6e\u63d0\u4f9b\u5668"

    .line 1210
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "9.1"

    const-string v1, ""

    const-string v2, ""

    .line 1211
    invoke-static {v0, v1, v2}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportAppUserAttr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static getMetaDataByKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    if-eqz p0, :cond_4

    .line 274
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    const-string v1, "MPSDK_CHANNEL"

    .line 278
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 279
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getMpsdkChannelCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v1, "MPSDK_IMEI"

    .line 280
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 281
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v1, "MPSDK_ANDROID_ID"

    .line 282
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 283
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getAndroidId()Ljava/lang/String;

    move-result-object v0

    :cond_3
    :goto_0
    return-object v0

    :cond_4
    :goto_1
    return-object v0
.end method

.method private static getMpsdkChannelCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, ""

    if-eqz p0, :cond_3

    .line 291
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2

    .line 296
    :cond_0
    :try_start_0
    sget-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 298
    iget-object v2, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-nez v2, :cond_1

    const-string p0, ""

    :goto_0
    move-object v0, p0

    goto :goto_1

    :cond_1
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 302
    invoke-virtual {p0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception p0

    .line 300
    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    :goto_1
    if-eqz v0, :cond_2

    .line 305
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string p0, "MpsdkNativeUtils"

    const-string v0, "MPSDK_CHANNEL\u672a\u914d\u7f6e,\u8bf7\u5728AndroidManifest.xml\u4e2d\u914d\u7f6e"

    .line 306
    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    new-instance p0, Ljava/lang/Error;

    const-string v0, "MPSDK_CHANNEL\u672a\u914d\u7f6e,\u8bf7\u5728AndroidManifest.xml\u4e2d\u914d\u7f6e"

    invoke-direct {p0, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_3
    :goto_2
    return-object v0
.end method

.method private static getMpsdkPasteContent()V
    .locals 8

    .line 356
    :try_start_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 357
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 358
    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v2

    if-lez v2, :cond_9

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 359
    :goto_0
    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v4

    if-ge v3, v4, :cond_8

    .line 360
    invoke-virtual {v1, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 361
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_0

    goto/16 :goto_2

    .line 365
    :cond_0
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 366
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    goto/16 :goto_2

    :cond_1
    const-string v5, "MpsdkNativeUtils"

    .line 370
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getMpsdkPasteContent text="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "qmkey"

    .line 371
    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    goto :goto_2

    :cond_2
    const-string v5, "qmkey"

    const-string v6, ""

    .line 374
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 375
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_1

    .line 378
    :cond_3
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v4, v2}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v4

    const-string v6, "UTF-8"

    invoke-direct {v5, v4, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 379
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    return-void

    :cond_4
    const-string v4, "type=link"

    .line 384
    invoke-virtual {v5, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "gameid="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getGameId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    return-void

    .line 388
    :cond_5
    sput-object v5, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->launchOptions:Ljava/lang/String;

    const/4 v4, 0x0

    .line 390
    invoke-static {v4, v4}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 391
    invoke-virtual {v0, v4}, Landroid/content/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_6
    :goto_1
    return-void

    :cond_7
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_8
    const-string v0, "MpsdkNativeUtils"

    const-string v1, "getMpsdkPasteContent over."

    .line 395
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    const-string v1, "MpsdkNativeUtils"

    .line 402
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMpsdkPasteContent error=="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_1
    move-exception v0

    const-string v1, "MpsdkNativeUtils"

    .line 400
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMpsdkPasteContent excep=="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    :goto_3
    return-void
.end method

.method public static getMyOpenId()Ljava/lang/String;
    .locals 1

    .line 263
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mOpenId:Ljava/lang/String;

    return-object v0
.end method

.method public static getNativeAppOpenId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-eqz p0, :cond_0

    .line 232
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    sput-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mOpenId:Ljava/lang/String;

    .line 234
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->afterLogin(Ljava/lang/String;)V

    .line 235
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->writeUUIDToSDCard(Ljava/lang/String;)Z

    .line 239
    :cond_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->readUUIDFromSDCard()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getNativeAppOpenId2(Ljava/lang/String;)V
    .locals 1

    if-eqz p0, :cond_0

    .line 243
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    sput-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mOpenId:Ljava/lang/String;

    .line 245
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->afterLogin(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static getOAID(Landroid/content/Context;Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;)V
    .locals 1

    if-nez p1, :cond_0

    const-string p0, "MpsdkNativeUtils"

    const-string p1, "OnGetOAIDListener\u4e0d\u80fd\u4e3a\u7a7a"

    .line 1121
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 1124
    :cond_0
    invoke-static {p0}, Lcom/qmo/game/mpsdk/oaid/DeviceID;->with(Landroid/content/Context;)Lcom/qmo/game/mpsdk/oaid/IDeviceId;

    move-result-object p0

    .line 1125
    invoke-interface {p0}, Lcom/qmo/game/mpsdk/oaid/IDeviceId;->supportOAID()Z

    move-result v0

    if-nez v0, :cond_1

    const-string p0, "MpsdkNativeUtils"

    const-string v0, "\u4e0d\u652f\u6301OAID\uff0c\u987b\u81ea\u884c\u751f\u6210GUID"

    .line 1126
    invoke-static {p0, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string p0, ""

    .line 1127
    invoke-interface {p1, p0}, Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;->complete(Ljava/lang/String;)V

    return-void

    .line 1144
    :cond_1
    new-instance v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$7;

    invoke-direct {v0, p1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$7;-><init>(Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;)V

    invoke-interface {p0, v0}, Lcom/qmo/game/mpsdk/oaid/IDeviceId;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    return-void
.end method

.method public static getPhoneModel()Ljava/lang/String;
    .locals 4

    const-string v0, ""

    .line 424
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "oaid"

    .line 425
    sget-object v3, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mOaid:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "androidId"

    .line 426
    sget-object v3, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {v3}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "imei"

    .line 427
    sget-object v3, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {v3}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "mode"

    .line 428
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "mac"

    .line 429
    sget-object v3, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {v3}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getMac(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    .line 430
    invoke-static {v1, v2}, Lcom/qmo/game/mpsdk/utils/JsonObjectCoder;->encode(Ljava/util/Map;Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 433
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    const-string v1, "MpsdkNativeUtils"

    .line 435
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPhoneModel deviceInfo==>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private static handleEngineInit(Ljava/lang/Object;)V
    .locals 4

    if-nez p0, :cond_0

    return-void

    .line 587
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "org.egret.egretnativeandroid.EgretNativeAndroid"

    .line 590
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const-string v0, "org.egret.egretnativeandroid.EgretNativeAndroid"

    .line 591
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v1, "org.egret.launcher.egret_android_launcher.NativeLauncher"

    .line 592
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "org.egret.launcher.egret_android_launcher.NativeLauncher"

    .line 593
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_3

    return-void

    .line 613
    :cond_3
    new-instance v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetNativeAppOpenIdHandler;

    invoke-direct {v1, v2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetNativeAppOpenIdHandler;-><init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V

    const-string v3, "getNativeAppOpenId"

    invoke-static {v0, p0, v1, v3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->invokeSetExternalInterface(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/reflect/InvocationHandler;Ljava/lang/String;)V

    .line 614
    new-instance v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetNativeAppOpenId2Handler;

    invoke-direct {v1, v2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetNativeAppOpenId2Handler;-><init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V

    const-string v3, "getNativeAppOpenId2"

    invoke-static {v0, p0, v1, v3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->invokeSetExternalInterface(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/reflect/InvocationHandler;Ljava/lang/String;)V

    .line 615
    new-instance v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetPhoneModelHandler;

    invoke-direct {v1, v2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetPhoneModelHandler;-><init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V

    const-string v3, "getPhoneModel"

    invoke-static {v0, p0, v1, v3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->invokeSetExternalInterface(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/reflect/InvocationHandler;Ljava/lang/String;)V

    .line 616
    new-instance v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetAppLaunchOptionsHandler;

    invoke-direct {v1, v2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetAppLaunchOptionsHandler;-><init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V

    const-string v3, "getAppLaunchOptions"

    invoke-static {v0, p0, v1, v3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->invokeSetExternalInterface(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/reflect/InvocationHandler;Ljava/lang/String;)V

    .line 619
    new-instance v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetMetaDataByKeyHandler;

    invoke-direct {v1, v2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$GetMetaDataByKeyHandler;-><init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V

    const-string v3, "getMetaDataByKey"

    invoke-static {v0, p0, v1, v3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->invokeSetExternalInterface(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/reflect/InvocationHandler;Ljava/lang/String;)V

    .line 620
    new-instance v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$ClickAppHandler;

    invoke-direct {v1, v2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$ClickAppHandler;-><init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V

    const-string v3, "clickApp"

    invoke-static {v0, p0, v1, v3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->invokeSetExternalInterface(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/reflect/InvocationHandler;Ljava/lang/String;)V

    .line 621
    new-instance v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$SetSkipParamsHandler;

    invoke-direct {v1, v2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$SetSkipParamsHandler;-><init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V

    const-string v2, "setSkipParams"

    invoke-static {v0, p0, v1, v2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->invokeSetExternalInterface(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/reflect/InvocationHandler;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 624
    invoke-virtual {p0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public static initCrashReport(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0

    return-void
.end method

.method private static initLifecycle(Landroid/content/Context;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 907
    instance-of v1, p0, Landroid/app/Application;

    if-eqz v1, :cond_1

    .line 908
    move-object v0, p0

    check-cast v0, Landroid/app/Application;

    goto :goto_0

    .line 909
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Landroid/app/Application;

    if-eqz v1, :cond_2

    .line 910
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Landroid/app/Application;

    :cond_2
    :goto_0
    if-nez v0, :cond_3

    return-void

    .line 917
    :cond_3
    new-instance p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4;

    invoke-direct {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$4;-><init>()V

    invoke-virtual {v0, p0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void
.end method

.method public static initMPSDK(Landroid/app/Activity;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 100
    invoke-static {p0, p1, p2, v0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->initMPSDK(Landroid/app/Activity;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;Ljava/lang/Object;)V

    return-void
.end method

.method public static initMPSDK(Landroid/app/Activity;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;Ljava/lang/Object;)V
    .locals 6

    .line 114
    sput-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    .line 115
    sput-object p1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->GAME_ID:Ljava/lang/String;

    .line 116
    sput-object p3, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mEngineObj:Ljava/lang/Object;

    .line 118
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    sget-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getLocalDeviceUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const v3, 0xbdb8c

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getMpsdkPasteContent()V

    .line 120
    invoke-static {p3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->handleEngineInit(Ljava/lang/Object;)V

    .line 121
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->initLifecycle(Landroid/content/Context;)V

    .line 124
    sget-object p1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->PERMISSIONS_STORAGE:[Ljava/lang/String;

    .line 129
    new-instance p3, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;

    invoke-direct {p3, p2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;-><init>(Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;)V

    const/16 p2, 0x3e7

    invoke-static {p1, p3, p2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->requestRuntimePermissions([Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/PermissionListener;I)V

    .line 160
    new-instance p1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$2;

    invoke-direct {p1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$2;-><init>()V

    invoke-static {p0, p1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getOAID(Landroid/content/Context;Lcom/qmo/game/mpsdk/utils/OnGetOAIDListener;)V

    return-void
.end method

.method private static invokeCallExternalInterface(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const-string v0, "MpsdkNativeUtils"

    .line 659
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "methodName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " , param:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    :try_start_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mEngineObj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "callExternalInterface"

    const/4 v2, 0x2

    .line 665
    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-class v4, Ljava/lang/String;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 667
    sget-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mEngineObj:Ljava/lang/Object;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v5

    aput-object p1, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const-string p0, "MpsdkNativeUtils"

    .line 668
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "callExternalInterface over: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 674
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 672
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 670
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private static invokeSetExternalInterface(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/reflect/InvocationHandler;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Object;",
            "Ljava/lang/reflect/InvocationHandler;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    :try_start_0
    const-string v0, "org.egret.runtime.launcherInterface.INativePlayer$INativeInterface"

    .line 632
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 633
    const-class v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v3, p2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object p2

    const-string v1, "setExternalInterface"

    const/4 v3, 0x2

    .line 636
    new-array v5, v3, [Ljava/lang/Class;

    const-class v6, Ljava/lang/String;

    aput-object v6, v5, v4

    aput-object v0, v5, v2

    invoke-virtual {p0, v1, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p0

    .line 638
    new-array v0, v3, [Ljava/lang/Object;

    aput-object p3, v0, v4

    aput-object p2, v0, v2

    invoke-virtual {p0, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 653
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 650
    invoke-virtual {p0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 647
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 644
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception p0

    .line 641
    invoke-virtual {p0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public static isAppInstalled(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    .line 873
    :try_start_0
    sget-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 874
    invoke-virtual {v1, p0, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static isNewUser()Z
    .locals 2

    const-string v0, ""

    :try_start_0
    const-string v1, ""

    .line 191
    invoke-static {v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getNativeAppOpenId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :catch_0
    if-eqz v0, :cond_1

    .line 195
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private static jump2LandingPage(Ljava/lang/String;)Z
    .locals 3

    .line 854
    :try_start_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 855
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 856
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x1

    goto :goto_0

    :catch_0
    move-exception p0

    const-string v0, "MpsdkNativeUtils"

    .line 859
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "jump2LandingPage -- error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static launchAppWithPackage(Ljava/lang/String;)Z
    .locals 3

    .line 886
    :try_start_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 887
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x1

    goto :goto_0

    :catch_0
    move-exception p0

    const-string v0, "MpsdkNativeUtils"

    .line 890
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "launchAppWithPackage -- error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 4

    const/16 v0, 0x3e7

    if-eq p0, v0, :cond_0

    return-void

    .line 553
    :cond_0
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x17

    if-ge p0, v0, :cond_1

    return-void

    .line 556
    :cond_1
    array-length p0, p2

    if-lez p0, :cond_5

    .line 557
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    .line 559
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_3

    .line 560
    aget v1, p2, v0

    if-nez v1, :cond_2

    const-string v1, "MpsdkNativeUtils"

    .line 562
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MPSDK\u6743\u9650\u7533\u8bf7\u901a\u8fc7:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    sget-object v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mListener:Lcom/qmo/game/mpsdk/utils/PermissionListener;

    invoke-interface {v1}, Lcom/qmo/game/mpsdk/utils/PermissionListener;->granted()V

    goto :goto_1

    .line 565
    :cond_2
    aget-object v1, p1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 568
    :cond_3
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    .line 569
    sget-object p1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mListener:Lcom/qmo/game/mpsdk/utils/PermissionListener;

    invoke-interface {p1, p0}, Lcom/qmo/game/mpsdk/utils/PermissionListener;->denied(Ljava/util/List;)V

    .line 572
    :cond_4
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mListener:Lcom/qmo/game/mpsdk/utils/PermissionListener;

    invoke-interface {p0}, Lcom/qmo/game/mpsdk/utils/PermissionListener;->complete()V

    :cond_5
    return-void
.end method

.method private static parseLaction(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/location/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_5

    .line 1336
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    .line 1340
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Address;

    invoke-virtual {v2}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v2

    .line 1341
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/Address;

    invoke-virtual {v3}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v3

    .line 1342
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/location/Address;

    invoke-virtual {p0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object p0

    if-eqz v2, :cond_1

    .line 1343
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    if-eqz p0, :cond_4

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 1347
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_4
    :goto_0
    return-object v0

    :cond_5
    :goto_1
    return-object v0
.end method

.method private static readUUIDFromSDCard()Ljava/lang/String;
    .locals 4

    .line 441
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 442
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 443
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".mpsdk"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mp_game_uid_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->GAME_ID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 445
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "UUID file is not exists................"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v0, ""

    return-object v0

    :cond_0
    const/4 v0, 0x0

    .line 450
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 451
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v0

    new-array v0, v0, [B

    .line 452
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 453
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 462
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :goto_0
    return-object v1

    :catchall_0
    move-exception v0

    goto :goto_1

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_1
    if-eqz v2, :cond_1

    .line 459
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    .line 462
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 465
    :cond_1
    :goto_2
    throw v0

    :catch_2
    move-object v2, v0

    :catch_3
    if-eqz v2, :cond_2

    .line 459
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_3

    :catch_4
    move-exception v0

    .line 462
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_2
    :goto_3
    const-string v0, ""

    return-object v0
.end method

.method private static reportEvent999006()V
    .locals 1

    .line 1354
    new-instance v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$10;

    invoke-direct {v0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$10;-><init>()V

    .line 1390
    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$10;->start()V

    return-void
.end method

.method private static reportVpnUser()V
    .locals 3

    .line 1057
    :try_start_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->isVpnUsed()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "6"

    const-string v1, ""

    const-string v2, ""

    .line 1058
    invoke-static {v0, v1, v2}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportAppUserAttr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private static reportX86User()V
    .locals 3

    .line 1068
    :try_start_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->isX86ABI()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "7"

    const-string v1, ""

    const-string v2, ""

    .line 1069
    invoke-static {v0, v1, v2}, Lcom/qmo/game/mpsdk/utils/Statistic;->reportAppUserAttr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077
    :catch_0
    :cond_0
    :try_start_1
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    new-instance v1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$6;

    invoke-direct {v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$6;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method

.method private static requestRuntimePermissions([Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/PermissionListener;I)V
    .locals 4

    .line 521
    sput-object p1, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mListener:Lcom/qmo/game/mpsdk/utils/PermissionListener;

    .line 522
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x17

    if-ge p1, v0, :cond_0

    .line 523
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mListener:Lcom/qmo/game/mpsdk/utils/PermissionListener;

    invoke-interface {p0}, Lcom/qmo/game/mpsdk/utils/PermissionListener;->granted()V

    .line 524
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mListener:Lcom/qmo/game/mpsdk/utils/PermissionListener;

    invoke-interface {p0}, Lcom/qmo/game/mpsdk/utils/PermissionListener;->complete()V

    return-void

    .line 527
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 529
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v2, p0, v1

    .line 530
    sget-object v3, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    invoke-static {v3, v2}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    .line 531
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 533
    :cond_1
    sget-object v2, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mListener:Lcom/qmo/game/mpsdk/utils/PermissionListener;

    invoke-interface {v2}, Lcom/qmo/game/mpsdk/utils/PermissionListener;->granted()V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 537
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_3

    .line 538
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    .line 539
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    .line 538
    invoke-static {p0, p1, p2}, Landroidx/core/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_2

    .line 541
    :cond_3
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mListener:Lcom/qmo/game/mpsdk/utils/PermissionListener;

    invoke-interface {p0}, Lcom/qmo/game/mpsdk/utils/PermissionListener;->complete()V

    :goto_2
    return-void
.end method

.method private static setAppOnForeground(Z)V
    .locals 3

    .line 1036
    sput-boolean p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mIsAppOnForegroundB:Z

    .line 1037
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mHandler:Landroid/os/Handler;

    if-eqz p0, :cond_0

    .line 1040
    :try_start_0
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mHandler:Landroid/os/Handler;

    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->appOnForegroundCheckRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1041
    sget-object p0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mHandler:Landroid/os/Handler;

    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->appOnForegroundCheckRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public static setSkipParams(Ljava/lang/String;)V
    .locals 3

    if-eqz p0, :cond_2

    .line 317
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    const-string v0, "MpsdkNativeUtils"

    .line 322
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSkipParams params is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :try_start_0
    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->mContext:Landroid/app/Activity;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v1, 0x0

    .line 329
    invoke-static {v1, p0}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    const-string p0, "MpsdkNativeUtils"

    const-string v0, "setSkipParams over."

    .line 330
    invoke-static {p0, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string v0, "MpsdkNativeUtils"

    .line 335
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSkipParams error=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception p0

    const-string v0, "MpsdkNativeUtils"

    .line 333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSkipParams excep=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_2
    :goto_1
    const-string p0, "MpsdkNativeUtils"

    const-string v0, "setSkipParams params is null."

    .line 318
    invoke-static {p0, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static writeUUIDToSDCard(Ljava/lang/String;)Z
    .locals 4

    .line 473
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 475
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 477
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".mpsdk"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 478
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".mpsdk"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "mp_game_uid_"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->GAME_ID:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 479
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    .line 480
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    return v3

    .line 484
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    :cond_1
    const/4 v0, 0x0

    .line 490
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 502
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 505
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    :goto_0
    return v3

    :catchall_0
    move-exception p0

    move-object v0, v1

    goto :goto_3

    :catch_1
    move-exception p0

    move-object v0, v1

    goto :goto_1

    :catch_2
    move-exception p0

    move-object v0, v1

    goto :goto_2

    :catchall_1
    move-exception p0

    goto :goto_3

    :catch_3
    move-exception p0

    .line 498
    :goto_1
    :try_start_3
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v0, :cond_3

    .line 502
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    goto :goto_5

    :catch_4
    move-exception p0

    .line 495
    :goto_2
    :try_start_5
    invoke-virtual {p0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v0, :cond_3

    .line 502
    :try_start_6
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_5

    :catch_5
    move-exception p0

    .line 505
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    :goto_3
    if-eqz v0, :cond_2

    .line 502
    :try_start_7
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto :goto_4

    :catch_6
    move-exception v0

    .line 505
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 508
    :cond_2
    :goto_4
    throw p0

    :cond_3
    :goto_5
    const/4 p0, 0x0

    return p0
.end method
