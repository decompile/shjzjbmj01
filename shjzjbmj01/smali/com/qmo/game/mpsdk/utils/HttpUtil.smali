.class public Lcom/qmo/game/mpsdk/utils/HttpUtil;
.super Ljava/lang/Object;
.source "HttpUtil.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HttpUtil"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V
    .locals 0

    .line 19
    invoke-static {p0, p1}, Lcom/qmo/game/mpsdk/utils/HttpUtil;->getRequest(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V

    return-void
.end method

.method static synthetic access$100(Ljava/lang/String;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V
    .locals 0

    .line 19
    invoke-static {p0, p1, p2}, Lcom/qmo/game/mpsdk/utils/HttpUtil;->postRequest(Ljava/lang/String;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V

    return-void
.end method

.method public static get(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V
    .locals 1

    .line 25
    new-instance v0, Lcom/qmo/game/mpsdk/utils/HttpUtil$1;

    invoke-direct {v0, p0, p1}, Lcom/qmo/game/mpsdk/utils/HttpUtil$1;-><init>(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V

    .line 29
    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/HttpUtil$1;->start()V

    return-void
.end method

.method private static getRequest(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 47
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p0

    check-cast p0, Ljava/net/HttpURLConnection;

    const-string v2, "GET"

    .line 50
    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const v2, 0xc350

    .line 51
    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 52
    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const-string v2, "User-Agent"

    const-string v3, "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;"

    .line 54
    invoke-virtual {p0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Accept-Language"

    const-string v3, "zh-CN"

    .line 55
    invoke-virtual {p0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Connection"

    const-string v3, "Keep-Alive"

    .line 56
    invoke-virtual {p0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Charset"

    const-string v3, "UTF-8"

    .line 57
    invoke-virtual {p0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    .line 64
    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 67
    invoke-virtual {p0, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 68
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->connect()V

    .line 69
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getContentLength()I

    .line 70
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_1

    .line 71
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p0
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 72
    :try_start_1
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/16 v0, 0x400

    .line 74
    :try_start_2
    new-array v0, v0, [B

    .line 75
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 76
    invoke-virtual {v3, v0, v1, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v2, v0

    move-object v0, v3

    const/4 v1, 0x1

    goto :goto_1

    :catchall_0
    move-exception p1

    move-object v0, v3

    goto/16 :goto_a

    :catch_0
    move-exception v0

    move-object v2, p0

    move-object p0, v0

    move-object v0, v3

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v2, p0

    move-object p0, v0

    move-object v0, v3

    goto/16 :goto_3

    :catch_2
    move-exception v0

    move-object v2, p0

    move-object p0, v0

    move-object v0, v3

    goto/16 :goto_4

    :catchall_1
    move-exception p1

    goto/16 :goto_a

    :catch_3
    move-exception v2

    move-object v6, v2

    move-object v2, p0

    move-object p0, v6

    goto :goto_2

    :catch_4
    move-exception v2

    move-object v6, v2

    move-object v2, p0

    move-object p0, v6

    goto :goto_3

    :catch_5
    move-exception v2

    move-object v6, v2

    move-object v2, p0

    move-object p0, v6

    goto :goto_4

    .line 83
    :cond_1
    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "code:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v2, p0

    move-object p0, v0

    :goto_1
    if-eqz v0, :cond_2

    .line 98
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    :cond_2
    if-eqz p0, :cond_7

    .line 101
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    goto :goto_8

    :catchall_2
    move-exception p1

    move-object p0, v0

    goto :goto_a

    :catch_6
    move-exception p0

    move-object v2, v0

    .line 93
    :goto_2
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 94
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    if-eqz v0, :cond_3

    .line 98
    :try_start_6
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    :cond_3
    if-eqz v2, :cond_6

    .line 101
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9

    goto :goto_7

    :catch_7
    move-exception p0

    move-object v2, v0

    .line 90
    :goto_3
    :try_start_7
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 91
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v0, :cond_4

    .line 98
    :try_start_8
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    :cond_4
    if-eqz v2, :cond_6

    .line 101
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9

    goto :goto_7

    :catch_8
    move-exception p0

    move-object v2, v0

    .line 87
    :goto_4
    :try_start_9
    invoke-virtual {p0}, Ljava/net/MalformedURLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 88
    invoke-virtual {p0}, Ljava/net/MalformedURLException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    if-eqz v0, :cond_5

    .line 98
    :try_start_a
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    goto :goto_5

    :catch_9
    move-exception p0

    goto :goto_6

    :cond_5
    :goto_5
    if-eqz v2, :cond_6

    .line 101
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    goto :goto_7

    .line 104
    :goto_6
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    :cond_6
    :goto_7
    move-object v2, v3

    :cond_7
    :goto_8
    if-eqz v1, :cond_8

    .line 109
    invoke-interface {p1, v2}, Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;->onSuccess(Ljava/lang/String;)V

    goto :goto_9

    .line 111
    :cond_8
    invoke-interface {p1, v2}, Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;->onError(Ljava/lang/String;)V

    :goto_9
    return-void

    :catchall_3
    move-exception p1

    move-object p0, v2

    :goto_a
    if-eqz v0, :cond_9

    .line 98
    :try_start_b
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    goto :goto_b

    :catch_a
    move-exception p0

    goto :goto_c

    :cond_9
    :goto_b
    if-eqz p0, :cond_a

    .line 101
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    goto :goto_d

    .line 104
    :goto_c
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    .line 105
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    .line 107
    :cond_a
    :goto_d
    throw p1
.end method

.method public static post(Ljava/lang/String;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V
    .locals 1

    .line 33
    new-instance v0, Lcom/qmo/game/mpsdk/utils/HttpUtil$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/qmo/game/mpsdk/utils/HttpUtil$2;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V

    .line 37
    invoke-virtual {v0}, Lcom/qmo/game/mpsdk/utils/HttpUtil$2;->start()V

    return-void
.end method

.method private static postRequest(Ljava/lang/String;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;)V
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 121
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p0

    check-cast p0, Ljava/net/HttpURLConnection;

    const-string v2, "POST"

    .line 124
    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const v2, 0xc350

    .line 125
    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 126
    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const-string v2, "User-Agent"

    const-string v3, "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;"

    .line 128
    invoke-virtual {p0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Accept-Language"

    const-string v3, "zh-CN"

    .line 129
    invoke-virtual {p0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Connection"

    const-string v3, "Keep-Alive"

    .line 130
    invoke-virtual {p0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Charset"

    const-string v3, "UTF-8"

    .line 131
    invoke-virtual {p0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    .line 138
    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 140
    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 141
    invoke-virtual {p0, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 144
    new-instance v3, Ljava/io/OutputStreamWriter;

    .line 145
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-direct {v3, v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 147
    invoke-virtual {v3, p1}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->flush()V

    .line 149
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->connect()V

    .line 151
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getContentLength()I

    .line 152
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result p1

    const/16 v3, 0xc8

    if-ne p1, v3, :cond_1

    .line 154
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p0
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 155
    :try_start_1
    new-instance p1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/16 v0, 0x400

    .line 157
    :try_start_2
    new-array v0, v0, [B

    .line 158
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 159
    invoke-virtual {p1, v0, v1, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 161
    :cond_0
    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v1, 0x1

    goto :goto_1

    :catchall_0
    move-exception p2

    move-object v0, p1

    goto/16 :goto_9

    :catch_0
    move-exception v0

    move-object v6, p1

    move-object p1, p0

    move-object p0, v0

    move-object v0, v6

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v6, p1

    move-object p1, p0

    move-object p0, v0

    move-object v0, v6

    goto :goto_3

    :catchall_1
    move-exception p2

    goto/16 :goto_9

    :catch_2
    move-exception p1

    move-object v6, p1

    move-object p1, p0

    move-object p0, v6

    goto :goto_2

    :catch_3
    move-exception p1

    move-object v6, p1

    move-object p1, p0

    move-object p0, v6

    goto :goto_3

    .line 166
    :cond_1
    :try_start_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u8bf7\u6c42\u5931\u8d25 code:"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object p1, v0

    move-object v0, p0

    move-object p0, p1

    :goto_1
    if-eqz p1, :cond_2

    .line 178
    :try_start_4
    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->close()V

    :cond_2
    if-eqz p0, :cond_6

    .line 181
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    goto :goto_7

    :catchall_2
    move-exception p2

    move-object p0, v0

    goto :goto_9

    :catch_4
    move-exception p0

    move-object p1, v0

    .line 173
    :goto_2
    :try_start_5
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 174
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    if-eqz v0, :cond_3

    .line 178
    :try_start_6
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    :cond_3
    if-eqz p1, :cond_5

    .line 181
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_6

    :catch_5
    move-exception p0

    move-object p1, v0

    .line 170
    :goto_3
    :try_start_7
    invoke-virtual {p0}, Ljava/net/MalformedURLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 171
    invoke-virtual {p0}, Ljava/net/MalformedURLException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v0, :cond_4

    .line 178
    :try_start_8
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    goto :goto_4

    :catch_6
    move-exception p0

    goto :goto_5

    :cond_4
    :goto_4
    if-eqz p1, :cond_5

    .line 181
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_6

    .line 184
    :goto_5
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 185
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    :cond_5
    :goto_6
    move-object v0, v2

    :cond_6
    :goto_7
    if-eqz v1, :cond_7

    .line 189
    invoke-interface {p2, v0}, Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;->onSuccess(Ljava/lang/String;)V

    goto :goto_8

    .line 191
    :cond_7
    invoke-interface {p2, v0}, Lcom/qmo/game/mpsdk/utils/HttpUtil$OnRequestCallBack;->onError(Ljava/lang/String;)V

    :goto_8
    return-void

    :catchall_3
    move-exception p2

    move-object p0, p1

    :goto_9
    if-eqz v0, :cond_8

    .line 178
    :try_start_9
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    goto :goto_a

    :catch_7
    move-exception p0

    goto :goto_b

    :cond_8
    :goto_a
    if-eqz p0, :cond_9

    .line 181
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_c

    .line 184
    :goto_b
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    .line 185
    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    .line 187
    :cond_9
    :goto_c
    throw p2
.end method
