.class public Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;
.super Landroid/os/FileObserver;
.source "ScreenShotFileObserver.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ScreenShotFileObserver"

.field private static lastEndScreenShotFileName:Ljava/lang/String; = ""

.field private static lastStartScreenShotFileName:Ljava/lang/String; = ""


# instance fields
.field private beganScreenShoted:Z

.field private handler:Landroid/os/Handler;

.field private run:Ljava/lang/Runnable;

.field private screenShotDir:Ljava/lang/String;

.field private screenShotFileName:Ljava/lang/String;

.field private screenShotLister:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/16 v0, 0xfff

    .line 28
    invoke-direct {p0, p1, v0}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    const/4 v0, 0x0

    .line 20
    iput-boolean v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->beganScreenShoted:Z

    const-string v0, ""

    .line 21
    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotFileName:Ljava/lang/String;

    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotDir:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotLister:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->handler:Landroid/os/Handler;

    .line 36
    new-instance v0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$1;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$1;-><init>(Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;)V

    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->run:Ljava/lang/Runnable;

    .line 29
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotDir:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->dealData()V

    return-void
.end method

.method private dealData()V
    .locals 3

    const/4 v0, 0x0

    .line 78
    iput-boolean v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->beganScreenShoted:Z

    const-string v0, "ScreenShotFileObserver"

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "screenShotFileName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotLister:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;->finshScreenShot(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p2, :cond_0

    .line 52
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x100

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->lastStartScreenShotFileName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ScreenShotFileObserver"

    .line 53
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " , path = "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotFileName:Ljava/lang/String;

    .line 55
    sput-object p2, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->lastStartScreenShotFileName:Ljava/lang/String;

    const/4 p1, 0x1

    .line 56
    iput-boolean p1, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->beganScreenShoted:Z

    .line 57
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->handler:Landroid/os/Handler;

    iget-object p2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->run:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 58
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->handler:Landroid/os/Handler;

    iget-object p2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->run:Ljava/lang/Runnable;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 59
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotLister:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotDir:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotFileName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;->beganScreenShot(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 60
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->lastEndScreenShotFileName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ScreenShotFileObserver"

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CLOSE_WRITE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " , path = "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotFileName:Ljava/lang/String;

    .line 63
    sput-object p2, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->lastEndScreenShotFileName:Ljava/lang/String;

    .line 64
    iget-object p1, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->handler:Landroid/os/Handler;

    iget-object p2, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->run:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 65
    invoke-direct {p0}, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->dealData()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    :goto_0
    return-void
.end method

.method public removeLister()V
    .locals 1

    const/4 v0, 0x0

    .line 88
    iput-object v0, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotLister:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;

    return-void
.end method

.method public setLister(Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver;->screenShotLister:Lcom/qmo/game/mpsdk/utils/ScreenShotFileObserver$ScreenShotLister;

    return-void
.end method
