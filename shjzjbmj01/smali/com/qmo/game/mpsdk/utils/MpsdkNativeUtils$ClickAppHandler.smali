.class Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$ClickAppHandler;
.super Ljava/lang/Object;
.source "MpsdkNativeUtils.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ClickAppHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$1;)V
    .locals 0

    .line 742
    invoke-direct {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils$ClickAppHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-string p1, "MpsdkNativeUtils"

    const-string p2, "doing ClickAppHandler invoke..."

    .line 745
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    if-eqz p3, :cond_1

    .line 746
    array-length p2, p3

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    aget-object v0, p3, p2

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    aget-object p2, p3, p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_1
    :goto_0
    move-object p2, p1

    :goto_1
    const-string p3, "MpsdkNativeUtils"

    .line 747
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doing ClickAppHandler invoke...msg = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_8

    .line 748
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_2

    goto :goto_7

    .line 752
    :cond_2
    invoke-static {p2, p1}, Lcom/qmo/game/mpsdk/utils/JsonObjectCoder;->decode(Ljava/lang/String;Ljava/lang/Void;)Ljava/util/Map;

    move-result-object p2

    if-eqz p2, :cond_4

    const-string p3, "appid"

    .line 753
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    if-nez p3, :cond_3

    goto :goto_2

    :cond_3
    const-string p3, "appid"

    .line 754
    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    goto :goto_3

    :cond_4
    :goto_2
    const-string p3, ""

    :goto_3
    if-eqz p2, :cond_6

    const-string v0, "page"

    .line 755
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_5

    goto :goto_4

    :cond_5
    const-string v0, "page"

    .line 756
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    goto :goto_5

    :cond_6
    :goto_4
    const-string p2, ""

    :goto_5
    const-string v0, "clickApp"

    .line 758
    invoke-static {p3, p2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1100(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_7

    const-string p2, "1"

    goto :goto_6

    :cond_7
    const-string p2, "0"

    :goto_6
    invoke-static {v0, p2}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->access$1000(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    :cond_8
    :goto_7
    return-object p1
.end method
