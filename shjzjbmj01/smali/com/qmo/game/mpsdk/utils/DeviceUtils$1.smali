.class final Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;
.super Ljava/lang/Object;
.source "DeviceUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/utils/DeviceUtils;->sendByGet(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$path:Ljava/lang/String;

.field final synthetic val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;)V
    .locals 0

    .line 435
    iput-object p1, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$path:Ljava/lang/String;

    iput-object p2, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v0, 0x0

    .line 441
    :try_start_0
    new-instance v1, Ljava/net/URL;

    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$path:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 442
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v2, "GET"

    .line 444
    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const/16 v2, 0x1388

    .line 446
    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 448
    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 451
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    .line 455
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 457
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/net/ProtocolException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 458
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 460
    :goto_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 461
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "KSActive send back: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    const-string v2, "success"

    invoke-interface {v0, v2}, Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;->callBack(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/ProtocolException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v3

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v2, v0

    move-object v0, v3

    goto/16 :goto_a

    :catch_0
    move-exception v0

    move-object v2, v0

    move-object v0, v3

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v2, v0

    move-object v0, v3

    goto/16 :goto_5

    :catch_2
    move-exception v0

    move-object v2, v0

    move-object v0, v3

    goto/16 :goto_7

    .line 467
    :cond_1
    :try_start_3
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/DeviceUtils;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KSActive send fail "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/net/ProtocolException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_1
    if-eqz v0, :cond_2

    .line 481
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    :catch_3
    move-exception v0

    .line 483
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 484
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    const-string v2, "fail"

    invoke-interface {v0, v2}, Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;->callBack(Ljava/lang/String;)V

    :cond_2
    :goto_2
    if-eqz v1, :cond_6

    goto/16 :goto_9

    :catch_4
    move-exception v2

    goto :goto_3

    :catch_5
    move-exception v2

    goto :goto_5

    :catch_6
    move-exception v2

    goto :goto_7

    :catchall_1
    move-exception v1

    move-object v2, v1

    move-object v1, v0

    goto/16 :goto_a

    :catch_7
    move-exception v1

    move-object v2, v1

    move-object v1, v0

    .line 476
    :goto_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 477
    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    const-string v3, "fail"

    invoke-interface {v2, v3}, Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;->callBack(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v0, :cond_3

    .line 481
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8

    goto :goto_4

    :catch_8
    move-exception v0

    .line 483
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 484
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    const-string v2, "fail"

    invoke-interface {v0, v2}, Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;->callBack(Ljava/lang/String;)V

    :cond_3
    :goto_4
    if-eqz v1, :cond_6

    goto :goto_9

    :catch_9
    move-exception v1

    move-object v2, v1

    move-object v1, v0

    .line 473
    :goto_5
    :try_start_7
    invoke-virtual {v2}, Ljava/net/ProtocolException;->printStackTrace()V

    .line 474
    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    const-string v3, "fail"

    invoke-interface {v2, v3}, Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;->callBack(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v0, :cond_4

    .line 481
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_a

    goto :goto_6

    :catch_a
    move-exception v0

    .line 483
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 484
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    const-string v2, "fail"

    invoke-interface {v0, v2}, Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;->callBack(Ljava/lang/String;)V

    :cond_4
    :goto_6
    if-eqz v1, :cond_6

    goto :goto_9

    :catch_b
    move-exception v1

    move-object v2, v1

    move-object v1, v0

    .line 470
    :goto_7
    :try_start_9
    invoke-virtual {v2}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 471
    iget-object v2, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    const-string v3, "fail"

    invoke-interface {v2, v3}, Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;->callBack(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    if-eqz v0, :cond_5

    .line 481
    :try_start_a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_c

    goto :goto_8

    :catch_c
    move-exception v0

    .line 483
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 484
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    const-string v2, "fail"

    invoke-interface {v0, v2}, Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;->callBack(Ljava/lang/String;)V

    :cond_5
    :goto_8
    if-eqz v1, :cond_6

    .line 488
    :goto_9
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_6
    return-void

    :catchall_2
    move-exception v2

    :goto_a
    if-eqz v0, :cond_7

    .line 481
    :try_start_b
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_d

    goto :goto_b

    :catch_d
    move-exception v0

    .line 483
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 484
    iget-object v0, p0, Lcom/qmo/game/mpsdk/utils/DeviceUtils$1;->val$sendMsgInterface:Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;

    const-string v3, "fail"

    invoke-interface {v0, v3}, Lcom/qmo/game/mpsdk/utils/DeviceUtils$ISendMsgInterface;->callBack(Ljava/lang/String;)V

    :cond_7
    :goto_b
    if-eqz v1, :cond_8

    .line 488
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 490
    :cond_8
    throw v2
.end method
