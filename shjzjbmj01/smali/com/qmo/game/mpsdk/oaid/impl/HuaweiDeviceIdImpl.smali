.class public Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;
.super Ljava/lang/Object;
.source "HuaweiDeviceIdImpl.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/oaid/IDeviceId;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;)Landroid/content/Context;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;->context:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public doGet(Lcom/qmo/game/mpsdk/oaid/IGetter;)V
    .locals 1
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 80
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$2;

    invoke-direct {v0, p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$2;-><init>(Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IGetter;)V

    invoke-virtual {p0, v0}, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    return-void
.end method

.method public doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
    .locals 4
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 39
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.uodis.opendevice.OPENIDS_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.huawei.hwid"

    .line 40
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    :try_start_0
    iget-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;->context:Landroid/content/Context;

    new-instance v2, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;

    invoke-direct {v2, p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;-><init>(Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Huawei OPENIDS_SERVICE bind failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 73
    invoke-interface {p1, v0}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method public supportOAID()Z
    .locals 3

    const/4 v0, 0x0

    .line 29
    :try_start_0
    iget-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.huawei.hwid"

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    return v0
.end method
