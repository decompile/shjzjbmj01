.class public Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;
.super Ljava/lang/Object;
.source "XiaomiDeviceIdImpl.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/oaid/IDeviceId;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private idProvider:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->context:Landroid/content/Context;

    return-void
.end method

.method private invokeMethod(Ljava/lang/reflect/Method;)Ljava/lang/String;
    .locals 4

    if-eqz p1, :cond_0

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->idProvider:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->context:Landroid/content/Context;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method


# virtual methods
.method public doGet(Lcom/qmo/game/mpsdk/oaid/IGetter;)V
    .locals 1
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 86
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl$1;

    invoke-direct {v0, p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl$1;-><init>(Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IGetter;)V

    invoke-virtual {p0, v0}, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    return-void
.end method

.method public doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
    .locals 7
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PrivateApi"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->idProvider:Ljava/lang/Class;

    if-nez v0, :cond_0

    :try_start_0
    const-string v0, "com.android.id.impl.IdProviderImpl"

    .line 41
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->idProvider:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 48
    :try_start_1
    iget-object v3, p0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->idProvider:Ljava/lang/Class;

    const-string v4, "getDefaultUDID"

    new-array v5, v2, [Ljava/lang/Class;

    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 49
    invoke-direct {p0, v3}, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->invokeMethod(Ljava/lang/reflect/Method;)Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v3

    :catch_1
    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 54
    invoke-interface {p1, v0}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetComplete(Ljava/lang/String;)V

    return-void

    .line 58
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->idProvider:Ljava/lang/Class;

    const-string v3, "getOAID"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v1

    invoke-virtual {v0, v3, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 59
    invoke-direct {p0, v0}, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->invokeMethod(Ljava/lang/reflect/Method;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 60
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 61
    invoke-interface {p1, v0}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetComplete(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Xiaomi OAID get failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    .line 67
    invoke-interface {p1, v0}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method public supportOAID()Z
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PrivateApi"
        }
    .end annotation

    :try_start_0
    const-string v0, "com.android.id.impl.IdProviderImpl"

    .line 28
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;->idProvider:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method
