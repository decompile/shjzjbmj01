.class public Lcom/qmo/game/mpsdk/oaid/impl/DefaultDeviceIdImpl;
.super Ljava/lang/Object;
.source "DefaultDeviceIdImpl.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/oaid/IDeviceId;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doGet(Lcom/qmo/game/mpsdk/oaid/IGetter;)V
    .locals 1
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 31
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/DefaultDeviceIdImpl$1;

    invoke-direct {v0, p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/DefaultDeviceIdImpl$1;-><init>(Lcom/qmo/game/mpsdk/oaid/impl/DefaultDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IGetter;)V

    invoke-virtual {p0, v0}, Lcom/qmo/game/mpsdk/oaid/impl/DefaultDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    return-void
.end method

.method public doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
    .locals 2
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 25
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "OAID unsupported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V

    return-void
.end method

.method public supportOAID()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
