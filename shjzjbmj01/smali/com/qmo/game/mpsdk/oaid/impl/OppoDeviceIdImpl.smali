.class public Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;
.super Ljava/lang/Object;
.source "OppoDeviceIdImpl.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/oaid/IDeviceId;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private sign:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;Landroid/os/IBinder;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->realGetOUID(Landroid/os/IBinder;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;)Landroid/content/Context;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->context:Landroid/content/Context;

    return-object p0
.end method

.method private realGetOUID(Landroid/os/IBinder;)Ljava/lang/String;
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PackageManagerGetSignatures"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->sign:Ljava/lang/String;

    const/4 v2, 0x0

    if-nez v1, :cond_2

    .line 84
    iget-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v3, 0x40

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v3, 0x0

    .line 85
    aget-object v1, v1, v3

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v1

    const-string v4, "SHA1"

    .line 86
    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4

    .line 87
    invoke-virtual {v4, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 88
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    array-length v5, v1

    const/4 v6, 0x0

    :goto_0
    const/4 v7, 0x1

    if-ge v6, v5, :cond_0

    aget-byte v8, v1, v6

    and-int/lit16 v8, v8, 0xff

    or-int/lit16 v8, v8, 0x100

    .line 90
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {v8, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 92
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->sign:Ljava/lang/String;

    .line 94
    const-class v1, Lcom/heytap/openid/IOpenID$Stub;

    const-string v4, "asInterface"

    new-array v5, v7, [Ljava/lang/Class;

    const-class v6, Landroid/os/IBinder;

    aput-object v6, v5, v3

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 95
    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v3

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/heytap/openid/IOpenID;

    if-eqz p1, :cond_1

    .line 99
    iget-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->sign:Ljava/lang/String;

    const-string v2, "OUID"

    invoke-interface {p1, v0, v1, v2}, Lcom/heytap/openid/IOpenID;->getSerID(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 97
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "IOpenID is null"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return-object v2
.end method


# virtual methods
.method public doGet(Lcom/qmo/game/mpsdk/oaid/IGetter;)V
    .locals 1
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 107
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$2;

    invoke-direct {v0, p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$2;-><init>(Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IGetter;)V

    invoke-virtual {p0, v0}, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    return-void
.end method

.method public doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
    .locals 4
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 45
    new-instance v0, Landroid/content/Intent;

    const-string v1, "action.com.heytap.openid.OPEN_ID_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 46
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.heytap.openid"

    const-string v3, "com.heytap.openid.IdentifyService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->context:Landroid/content/Context;

    new-instance v2, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;

    invoke-direct {v2, p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;-><init>(Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 73
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "HeyTap IdentifyService bind failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 76
    invoke-interface {p1, v0}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method public supportOAID()Z
    .locals 3

    const/4 v0, 0x0

    .line 35
    :try_start_0
    iget-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.heytap.openid"

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    return v0
.end method
