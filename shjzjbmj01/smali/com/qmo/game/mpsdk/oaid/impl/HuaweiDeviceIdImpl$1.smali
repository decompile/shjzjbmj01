.class Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;
.super Ljava/lang/Object;
.source "HuaweiDeviceIdImpl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;

.field final synthetic val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;

    iput-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    .line 47
    :try_start_0
    invoke-static {p2}, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;

    move-result-object p1

    .line 48
    invoke-interface {p1}, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;->getIDs()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 49
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    if-eqz p2, :cond_0

    .line 52
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    invoke-interface {p2, p1}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetComplete(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Huawei IDs get failed"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 58
    :try_start_1
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    new-instance v0, Ljava/lang/Exception;

    invoke-virtual {p1}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception p1

    .line 55
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    invoke-interface {p2, p1}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    :goto_0
    iget-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;

    invoke-static {p1}, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;->access$000(Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    return-void

    :goto_1
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;

    invoke-static {p2}, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;->access$000(Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;)Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 61
    throw p1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    return-void
.end method
