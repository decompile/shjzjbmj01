.class Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl$2;
.super Ljava/lang/Object;
.source "AsusDeviceIdImpl.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IGetter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl;

.field final synthetic val$getter:Lcom/qmo/game/mpsdk/oaid/IGetter;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IGetter;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl$2;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl;

    iput-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl$2;->val$getter:Lcom/qmo/game/mpsdk/oaid/IGetter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOAIDGetComplete(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 89
    iget-object v0, p0, Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl$2;->val$getter:Lcom/qmo/game/mpsdk/oaid/IGetter;

    invoke-interface {v0, p1}, Lcom/qmo/game/mpsdk/oaid/IGetter;->onDeviceIdGetComplete(Ljava/lang/String;)V

    return-void
.end method

.method public onOAIDGetError(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 94
    iget-object v0, p0, Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl$2;->val$getter:Lcom/qmo/game/mpsdk/oaid/IGetter;

    invoke-interface {v0, p1}, Lcom/qmo/game/mpsdk/oaid/IGetter;->onDeviceIdGetError(Ljava/lang/Exception;)V

    return-void
.end method
