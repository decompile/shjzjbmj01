.class public Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl;
.super Ljava/lang/Object;
.source "MeizuDeviceIdImpl.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/oaid/IDeviceId;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public doGet(Lcom/qmo/game/mpsdk/oaid/IGetter;)V
    .locals 1
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 60
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl$1;

    invoke-direct {v0, p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl$1;-><init>(Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IGetter;)V

    invoke-virtual {p0, v0}, Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    return-void
.end method

.method public doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
    .locals 7
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "content://com.meizu.flyme.openidsdk/"

    .line 39
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v6, "oaid"

    aput-object v6, v5, v0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v1, 0x0

    .line 41
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v2, v0

    check-cast v2, Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v2, "value"

    .line 43
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_0

    .line 45
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_1

    .line 47
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 50
    invoke-interface {p1, v2}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetComplete(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_4

    .line 51
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    .line 48
    :cond_1
    :try_start_3
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "OAID query failed"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v2

    goto :goto_1

    :catch_0
    move-exception v1

    .line 40
    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    .line 51
    :try_start_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    :cond_2
    :try_start_6
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :catch_1
    :cond_3
    :goto_2
    throw v2
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception v0

    .line 53
    invoke-interface {p1, v0}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V

    :cond_4
    :goto_3
    return-void
.end method

.method public supportOAID()Z
    .locals 3

    const/4 v0, 0x0

    .line 27
    :try_start_0
    iget-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.meizu.flyme.openidsdk"

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :catch_0
    :cond_0
    return v0
.end method
