.class Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;
.super Ljava/lang/Object;
.source "OppoDeviceIdImpl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;

.field final synthetic val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;

    iput-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    .line 53
    :try_start_0
    iget-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;

    invoke-static {p1, p2}, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->access$000(Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;Landroid/os/IBinder;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    if-eqz p2, :cond_0

    .line 57
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    invoke-interface {p2, p1}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetComplete(Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "HeyTap OUID get failed"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 61
    :try_start_1
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    invoke-interface {p2, p1}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    :goto_0
    iget-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;

    invoke-static {p1}, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->access$100(Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    return-void

    :goto_1
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;

    invoke-static {p2}, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;->access$100(Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;)Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 64
    throw p1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    return-void
.end method
