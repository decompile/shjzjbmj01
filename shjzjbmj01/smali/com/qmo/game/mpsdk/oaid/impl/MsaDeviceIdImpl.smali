.class public Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;
.super Ljava/lang/Object;
.source "MsaDeviceIdImpl.java"

# interfaces
.implements Lcom/qmo/game/mpsdk/oaid/IDeviceId;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;)Landroid/content/Context;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;->context:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public doGet(Lcom/qmo/game/mpsdk/oaid/IGetter;)V
    .locals 1
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 94
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl$2;

    invoke-direct {v0, p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl$2;-><init>(Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IGetter;)V

    invoke-virtual {p0, v0}, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    return-void
.end method

.method public doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
    .locals 4
    .param p1    # Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x1

    .line 42
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.bun.msa.action.start.service"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.mdid.msa"

    const-string v3, "com.mdid.msa.service.MsaKlService"

    .line 43
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.bun.msa.param.pkgname"

    .line 44
    iget-object v3, p0, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.bun.msa.param.runinset"

    .line 45
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 46
    iget-object v2, p0, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :catch_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.bun.msa.action.bindto.service"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.mdid.msa"

    const-string v3, "com.mdid.msa.service.MsaIdService"

    .line 51
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.bun.msa.param.pkgname"

    .line 52
    iget-object v3, p0, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    :try_start_1
    iget-object v2, p0, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;->context:Landroid/content/Context;

    new-instance v3, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl$1;

    invoke-direct {v3, p0, p1}, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl$1;-><init>(Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V

    invoke-virtual {v2, v1, v3, v0}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 84
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MsaIdService bind failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    .line 87
    invoke-interface {p1, v0}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method public supportOAID()Z
    .locals 3

    const/4 v0, 0x0

    .line 31
    :try_start_0
    iget-object v1, p0, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.mdid.msa"

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    return v0
.end method
