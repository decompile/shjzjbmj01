.class Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl$1;
.super Ljava/lang/Object;
.source "SamsungDeviceIdImpl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;->doGet(Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;

.field final synthetic val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;


# direct methods
.method constructor <init>(Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;

    iput-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .line 50
    :try_start_0
    const-class p1, Lcom/samsung/android/deviceidservice/IDeviceIdService$Stub;

    const-string v0, "asInterface"

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Landroid/os/IBinder;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p1, v0, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p1

    const/4 v0, 0x0

    .line 51
    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p1, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/samsung/android/deviceidservice/IDeviceIdService;

    if-eqz p1, :cond_1

    .line 55
    invoke-interface {p1}, Lcom/samsung/android/deviceidservice/IDeviceIdService;->getID()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 56
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    if-eqz p2, :cond_0

    .line 59
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    invoke-interface {p2, p1}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetComplete(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Samsung DeviceId get failed"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 53
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "IDeviceIdService is null"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 62
    :try_start_1
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl$1;->val$getter:Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;

    invoke-interface {p2, p1}, Lcom/qmo/game/mpsdk/oaid/IOAIDGetter;->onOAIDGetError(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    :goto_0
    iget-object p1, p0, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;

    invoke-static {p1}, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;->access$000(Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    return-void

    :goto_1
    iget-object p2, p0, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl$1;->this$0:Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;

    invoke-static {p2}, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;->access$000(Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;)Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 65
    throw p1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    return-void
.end method
