.class public final Lcom/qmo/game/mpsdk/oaid/DeviceID;
.super Ljava/lang/Object;
.source "DeviceID.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deviceInfo()Ljava/lang/String;
    .locals 2

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BrandModel\uff1a"

    .line 56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    .line 60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Manufacturer\uff1a"

    .line 61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "SystemVersion\uff1a"

    .line 64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " (API "

    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static with(Landroid/content/Context;)Lcom/qmo/game/mpsdk/oaid/IDeviceId;
    .locals 1

    .line 26
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isLenovo()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isMotolora()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_3

    .line 28
    :cond_0
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isMeizu()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/MeizuDeviceIdImpl;-><init>(Landroid/content/Context;)V

    goto/16 :goto_4

    .line 30
    :cond_1
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isNubia()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 31
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/NubiaDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/NubiaDeviceIdImpl;-><init>(Landroid/content/Context;)V

    goto/16 :goto_4

    .line 32
    :cond_2
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isXiaomi()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isBlackShark()Z

    move-result v0

    if-eqz v0, :cond_3

    goto/16 :goto_2

    .line 34
    :cond_3
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isSamsung()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 35
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/SamsungDeviceIdImpl;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 36
    :cond_4
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isVivo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 37
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/VivoDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/VivoDeviceIdImpl;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 38
    :cond_5
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isASUS()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 39
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/AsusDeviceIdImpl;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 40
    :cond_6
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isHuawei()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 41
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/HuaweiDeviceIdImpl;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 42
    :cond_7
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isOppo()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isOnePlus()Z

    move-result v0

    if-eqz v0, :cond_8

    goto :goto_1

    .line 44
    :cond_8
    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isZTE()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isFreeme()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-static {}, Lcom/qmo/game/mpsdk/oaid/SystemUtils;->isSSUI()Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_0

    .line 47
    :cond_9
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/DefaultDeviceIdImpl;

    invoke-direct {v0}, Lcom/qmo/game/mpsdk/oaid/impl/DefaultDeviceIdImpl;-><init>()V

    goto :goto_4

    .line 45
    :cond_a
    :goto_0
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/MsaDeviceIdImpl;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 43
    :cond_b
    :goto_1
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/OppoDeviceIdImpl;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 33
    :cond_c
    :goto_2
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/XiaomiDeviceIdImpl;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 27
    :cond_d
    :goto_3
    new-instance v0, Lcom/qmo/game/mpsdk/oaid/impl/LenovoDeviceIdImpl;

    invoke-direct {v0, p0}, Lcom/qmo/game/mpsdk/oaid/impl/LenovoDeviceIdImpl;-><init>(Landroid/content/Context;)V

    :goto_4
    return-object v0
.end method
