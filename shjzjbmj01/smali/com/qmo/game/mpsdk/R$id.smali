.class public final Lcom/qmo/game/mpsdk/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qmo/game/mpsdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f0d0096

.field public static final action_bar_activity_content:I = 0x7f0d0021

.field public static final action_bar_container:I = 0x7f0d0095

.field public static final action_bar_root:I = 0x7f0d0091

.field public static final action_bar_spinner:I = 0x7f0d0022

.field public static final action_bar_subtitle:I = 0x7f0d0073

.field public static final action_bar_title:I = 0x7f0d0072

.field public static final action_container:I = 0x7f0d0188

.field public static final action_context_bar:I = 0x7f0d0097

.field public static final action_divider:I = 0x7f0d0193

.field public static final action_image:I = 0x7f0d0189

.field public static final action_menu_divider:I = 0x7f0d0023

.field public static final action_menu_presenter:I = 0x7f0d0024

.field public static final action_mode_bar:I = 0x7f0d0093

.field public static final action_mode_bar_stub:I = 0x7f0d0092

.field public static final action_mode_close_button:I = 0x7f0d0074

.field public static final action_text:I = 0x7f0d018a

.field public static final actions:I = 0x7f0d0194

.field public static final activity_chooser_view_content:I = 0x7f0d0075

.field public static final add:I = 0x7f0d004a

.field public static final alertTitle:I = 0x7f0d0088

.field public static final async:I = 0x7f0d0061

.field public static final blocking:I = 0x7f0d0062

.field public static final bottom:I = 0x7f0d0052

.field public static final buttonPanel:I = 0x7f0d007b

.field public static final checkbox:I = 0x7f0d008f

.field public static final chronometer:I = 0x7f0d0190

.field public static final content:I = 0x7f0d008b

.field public static final contentPanel:I = 0x7f0d007e

.field public static final custom:I = 0x7f0d0085

.field public static final customPanel:I = 0x7f0d0084

.field public static final decor_content_parent:I = 0x7f0d0094

.field public static final default_activity_button:I = 0x7f0d0078

.field public static final edit_query:I = 0x7f0d0098

.field public static final end:I = 0x7f0d0058

.field public static final expand_activities_button:I = 0x7f0d0076

.field public static final expanded_menu:I = 0x7f0d008e

.field public static final forever:I = 0x7f0d0063

.field public static final group_divider:I = 0x7f0d008a

.field public static final home:I = 0x7f0d0025

.field public static final icon:I = 0x7f0d007a

.field public static final icon_group:I = 0x7f0d0195

.field public static final image:I = 0x7f0d0077

.field public static final info:I = 0x7f0d0191

.field public static final italic:I = 0x7f0d0064

.field public static final left:I = 0x7f0d005c

.field public static final line1:I = 0x7f0d0027

.field public static final line3:I = 0x7f0d0028

.field public static final listMode:I = 0x7f0d0040

.field public static final list_item:I = 0x7f0d0079

.field public static final message:I = 0x7f0d00a5

.field public static final multiply:I = 0x7f0d004b

.field public static final none:I = 0x7f0d0045

.field public static final normal:I = 0x7f0d0041

.field public static final notification_background:I = 0x7f0d018b

.field public static final notification_main_column:I = 0x7f0d018d

.field public static final notification_main_column_container:I = 0x7f0d018c

.field public static final parentPanel:I = 0x7f0d007d

.field public static final progress_circular:I = 0x7f0d0029

.field public static final progress_horizontal:I = 0x7f0d002a

.field public static final radio:I = 0x7f0d0090

.field public static final right:I = 0x7f0d005d

.field public static final right_icon:I = 0x7f0d0192

.field public static final right_side:I = 0x7f0d018e

.field public static final screen:I = 0x7f0d004c

.field public static final scrollIndicatorDown:I = 0x7f0d0083

.field public static final scrollIndicatorUp:I = 0x7f0d007f

.field public static final scrollView:I = 0x7f0d0080

.field public static final search_badge:I = 0x7f0d009a

.field public static final search_bar:I = 0x7f0d0099

.field public static final search_button:I = 0x7f0d009b

.field public static final search_close_btn:I = 0x7f0d00a0

.field public static final search_edit_frame:I = 0x7f0d009c

.field public static final search_go_btn:I = 0x7f0d00a2

.field public static final search_mag_icon:I = 0x7f0d009d

.field public static final search_plate:I = 0x7f0d009e

.field public static final search_src_text:I = 0x7f0d009f

.field public static final search_voice_btn:I = 0x7f0d00a3

.field public static final select_dialog_listview:I = 0x7f0d00a4

.field public static final shortcut:I = 0x7f0d008c

.field public static final spacer:I = 0x7f0d007c

.field public static final split_action_bar:I = 0x7f0d002b

.field public static final src_atop:I = 0x7f0d004d

.field public static final src_in:I = 0x7f0d004e

.field public static final src_over:I = 0x7f0d004f

.field public static final start:I = 0x7f0d005e

.field public static final submenuarrow:I = 0x7f0d008d

.field public static final submit_area:I = 0x7f0d00a1

.field public static final tabMode:I = 0x7f0d0042

.field public static final tag_transition_group:I = 0x7f0d0032

.field public static final tag_unhandled_key_event_manager:I = 0x7f0d0033

.field public static final tag_unhandled_key_listeners:I = 0x7f0d0034

.field public static final text:I = 0x7f0d0036

.field public static final text2:I = 0x7f0d0037

.field public static final textSpacerNoButtons:I = 0x7f0d0082

.field public static final textSpacerNoTitle:I = 0x7f0d0081

.field public static final time:I = 0x7f0d018f

.field public static final title:I = 0x7f0d0038

.field public static final titleDividerNoCustom:I = 0x7f0d0089

.field public static final title_template:I = 0x7f0d0087

.field public static final top:I = 0x7f0d005f

.field public static final topPanel:I = 0x7f0d0086

.field public static final uniform:I = 0x7f0d0050

.field public static final up:I = 0x7f0d003f

.field public static final wrap_content:I = 0x7f0d0051


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
