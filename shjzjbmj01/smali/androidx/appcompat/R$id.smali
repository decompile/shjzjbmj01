.class public final Landroidx/appcompat/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accessibility_action_clickable_span:I = 0x7f0d0000

.field public static final accessibility_custom_action_0:I = 0x7f0d0001

.field public static final accessibility_custom_action_1:I = 0x7f0d0002

.field public static final accessibility_custom_action_10:I = 0x7f0d0003

.field public static final accessibility_custom_action_11:I = 0x7f0d0004

.field public static final accessibility_custom_action_12:I = 0x7f0d0005

.field public static final accessibility_custom_action_13:I = 0x7f0d0006

.field public static final accessibility_custom_action_14:I = 0x7f0d0007

.field public static final accessibility_custom_action_15:I = 0x7f0d0008

.field public static final accessibility_custom_action_16:I = 0x7f0d0009

.field public static final accessibility_custom_action_17:I = 0x7f0d000a

.field public static final accessibility_custom_action_18:I = 0x7f0d000b

.field public static final accessibility_custom_action_19:I = 0x7f0d000c

.field public static final accessibility_custom_action_2:I = 0x7f0d000d

.field public static final accessibility_custom_action_20:I = 0x7f0d000e

.field public static final accessibility_custom_action_21:I = 0x7f0d000f

.field public static final accessibility_custom_action_22:I = 0x7f0d0010

.field public static final accessibility_custom_action_23:I = 0x7f0d0011

.field public static final accessibility_custom_action_24:I = 0x7f0d0012

.field public static final accessibility_custom_action_25:I = 0x7f0d0013

.field public static final accessibility_custom_action_26:I = 0x7f0d0014

.field public static final accessibility_custom_action_27:I = 0x7f0d0015

.field public static final accessibility_custom_action_28:I = 0x7f0d0016

.field public static final accessibility_custom_action_29:I = 0x7f0d0017

.field public static final accessibility_custom_action_3:I = 0x7f0d0018

.field public static final accessibility_custom_action_30:I = 0x7f0d0019

.field public static final accessibility_custom_action_31:I = 0x7f0d001a

.field public static final accessibility_custom_action_4:I = 0x7f0d001b

.field public static final accessibility_custom_action_5:I = 0x7f0d001c

.field public static final accessibility_custom_action_6:I = 0x7f0d001d

.field public static final accessibility_custom_action_7:I = 0x7f0d001e

.field public static final accessibility_custom_action_8:I = 0x7f0d001f

.field public static final accessibility_custom_action_9:I = 0x7f0d0020

.field public static final action_bar:I = 0x7f0d0096

.field public static final action_bar_activity_content:I = 0x7f0d0021

.field public static final action_bar_container:I = 0x7f0d0095

.field public static final action_bar_root:I = 0x7f0d0091

.field public static final action_bar_spinner:I = 0x7f0d0022

.field public static final action_bar_subtitle:I = 0x7f0d0073

.field public static final action_bar_title:I = 0x7f0d0072

.field public static final action_container:I = 0x7f0d0188

.field public static final action_context_bar:I = 0x7f0d0097

.field public static final action_divider:I = 0x7f0d0193

.field public static final action_image:I = 0x7f0d0189

.field public static final action_menu_divider:I = 0x7f0d0023

.field public static final action_menu_presenter:I = 0x7f0d0024

.field public static final action_mode_bar:I = 0x7f0d0093

.field public static final action_mode_bar_stub:I = 0x7f0d0092

.field public static final action_mode_close_button:I = 0x7f0d0074

.field public static final action_text:I = 0x7f0d018a

.field public static final actions:I = 0x7f0d0194

.field public static final activity_chooser_view_content:I = 0x7f0d0075

.field public static final add:I = 0x7f0d004a

.field public static final alertTitle:I = 0x7f0d0088

.field public static final async:I = 0x7f0d0061

.field public static final blocking:I = 0x7f0d0062

.field public static final buttonPanel:I = 0x7f0d007b

.field public static final checkbox:I = 0x7f0d008f

.field public static final checked:I = 0x7f0d027a

.field public static final chronometer:I = 0x7f0d0190

.field public static final content:I = 0x7f0d008b

.field public static final contentPanel:I = 0x7f0d007e

.field public static final custom:I = 0x7f0d0085

.field public static final customPanel:I = 0x7f0d0084

.field public static final decor_content_parent:I = 0x7f0d0094

.field public static final default_activity_button:I = 0x7f0d0078

.field public static final dialog_button:I = 0x7f0d00b4

.field public static final edit_query:I = 0x7f0d0098

.field public static final expand_activities_button:I = 0x7f0d0076

.field public static final expanded_menu:I = 0x7f0d008e

.field public static final forever:I = 0x7f0d0063

.field public static final group_divider:I = 0x7f0d008a

.field public static final home:I = 0x7f0d0025

.field public static final icon:I = 0x7f0d007a

.field public static final icon_group:I = 0x7f0d0195

.field public static final image:I = 0x7f0d0077

.field public static final info:I = 0x7f0d0191

.field public static final italic:I = 0x7f0d0064

.field public static final line1:I = 0x7f0d0027

.field public static final line3:I = 0x7f0d0028

.field public static final listMode:I = 0x7f0d0040

.field public static final list_item:I = 0x7f0d0079

.field public static final message:I = 0x7f0d00a5

.field public static final multiply:I = 0x7f0d004b

.field public static final none:I = 0x7f0d0045

.field public static final normal:I = 0x7f0d0041

.field public static final notification_background:I = 0x7f0d018b

.field public static final notification_main_column:I = 0x7f0d018d

.field public static final notification_main_column_container:I = 0x7f0d018c

.field public static final off:I = 0x7f0d027d

.field public static final on:I = 0x7f0d027c

.field public static final parentPanel:I = 0x7f0d007d

.field public static final progress_circular:I = 0x7f0d0029

.field public static final progress_horizontal:I = 0x7f0d002a

.field public static final radio:I = 0x7f0d0090

.field public static final right_icon:I = 0x7f0d0192

.field public static final right_side:I = 0x7f0d018e

.field public static final screen:I = 0x7f0d004c

.field public static final scrollIndicatorDown:I = 0x7f0d0083

.field public static final scrollIndicatorUp:I = 0x7f0d007f

.field public static final scrollView:I = 0x7f0d0080

.field public static final search_badge:I = 0x7f0d009a

.field public static final search_bar:I = 0x7f0d0099

.field public static final search_button:I = 0x7f0d009b

.field public static final search_close_btn:I = 0x7f0d00a0

.field public static final search_edit_frame:I = 0x7f0d009c

.field public static final search_go_btn:I = 0x7f0d00a2

.field public static final search_mag_icon:I = 0x7f0d009d

.field public static final search_plate:I = 0x7f0d009e

.field public static final search_src_text:I = 0x7f0d009f

.field public static final search_voice_btn:I = 0x7f0d00a3

.field public static final select_dialog_listview:I = 0x7f0d00a4

.field public static final shortcut:I = 0x7f0d008c

.field public static final spacer:I = 0x7f0d007c

.field public static final split_action_bar:I = 0x7f0d002b

.field public static final src_atop:I = 0x7f0d004d

.field public static final src_in:I = 0x7f0d004e

.field public static final src_over:I = 0x7f0d004f

.field public static final submenuarrow:I = 0x7f0d008d

.field public static final submit_area:I = 0x7f0d00a1

.field public static final tabMode:I = 0x7f0d0042

.field public static final tag_accessibility_actions:I = 0x7f0d002c

.field public static final tag_accessibility_clickable_spans:I = 0x7f0d002d

.field public static final tag_accessibility_heading:I = 0x7f0d002e

.field public static final tag_accessibility_pane_title:I = 0x7f0d002f

.field public static final tag_screen_reader_focusable:I = 0x7f0d0031

.field public static final tag_transition_group:I = 0x7f0d0032

.field public static final tag_unhandled_key_event_manager:I = 0x7f0d0033

.field public static final tag_unhandled_key_listeners:I = 0x7f0d0034

.field public static final text:I = 0x7f0d0036

.field public static final text2:I = 0x7f0d0037

.field public static final textSpacerNoButtons:I = 0x7f0d0082

.field public static final textSpacerNoTitle:I = 0x7f0d0081

.field public static final time:I = 0x7f0d018f

.field public static final title:I = 0x7f0d0038

.field public static final titleDividerNoCustom:I = 0x7f0d0089

.field public static final title_template:I = 0x7f0d0087

.field public static final topPanel:I = 0x7f0d0086

.field public static final unchecked:I = 0x7f0d027b

.field public static final uniform:I = 0x7f0d0050

.field public static final up:I = 0x7f0d003f

.field public static final wrap_content:I = 0x7f0d0051


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
