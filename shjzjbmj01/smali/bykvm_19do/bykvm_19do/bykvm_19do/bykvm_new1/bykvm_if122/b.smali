.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b;
.super Ljava/lang/Object;
.source "ReportNetApiImpl.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a<",
        "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b;->a:Landroid/content/Context;

    return-void
.end method

.method public static a(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 6

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    .line 7
    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/a;->a()Ljava/lang/String;

    move-result-object v2

    .line 8
    invoke-static {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 9
    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 10
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 11
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "message"

    .line 12
    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "cypher"

    .line 13
    invoke-virtual {v0, v2, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_1
    const-string v2, "message"

    .line 15
    :try_start_1
    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "cypher"

    .line 16
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    const-string v2, "message"

    .line 20
    :try_start_2
    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, v2, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "cypher"

    .line 21
    invoke-virtual {v0, p0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    :goto_0
    return-object v0
.end method

.method private b(Lorg/json/JSONObject;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    const-string v1, "code"

    .line 1
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v1, 0x4e20

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    :catchall_0
    :cond_0
    return v0
.end method


# virtual methods
.method public a(Ljava/util/List;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;",
            ">;)",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 22
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 23
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 24
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;

    .line 25
    iget-object v3, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->b:Lorg/json/JSONObject;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :cond_0
    const-string p1, "stats_list"

    .line 27
    invoke-virtual {v1, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 30
    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1

    const-string v1, "ad_sdk_version"

    const-string v2, "2.6.0.0"

    .line 31
    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz p1, :cond_1

    .line 34
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    .line 35
    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b$a;

    const/4 v5, 0x1

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/c;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v3, v2

    move-object v4, p0

    move-object v8, v1

    invoke-direct/range {v3 .. v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b;ILjava/lang/String;Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p$a;)V

    .line 46
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object p1

    const/16 v3, 0x2710

    invoke-virtual {p1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object p1

    invoke-virtual {v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/e;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    move-result-object p1

    sget-object v2, Lcom/bytedance/msdk/base/b;->a:Ljava/lang/String;

    .line 47
    invoke-virtual {p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    move-result-object p1

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b;->a:Landroid/content/Context;

    .line 48
    invoke-static {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    move-result-object v2

    invoke-virtual {p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :try_start_1
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->get()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    :try_start_2
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->a:Ljava/lang/Object;

    check-cast v1, Lorg/json/JSONObject;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v0, v1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "uploadEvent"

    invoke-static {v1, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :catchall_0
    :cond_1
    move-object p1, v0

    :catchall_1
    :goto_1
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    .line 62
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b;->b(Lorg/json/JSONObject;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    const-string v3, "error unknown"

    if-eqz p1, :cond_3

    .line 66
    iget-wide v4, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->f:J

    long-to-int v4, v4

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    :goto_3
    if-nez v0, :cond_4

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_4

    const-string v3, "server say not success"

    const/4 v2, 0x1

    goto :goto_4

    :cond_4
    if-eqz p1, :cond_5

    .line 72
    iget-object p1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;

    if-eqz p1, :cond_5

    .line 73
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 76
    :cond_5
    :goto_4
    new-instance p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;

    invoke-direct {p1, v0, v4, v3, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;-><init>(ZILjava/lang/String;Z)V

    return-object p1
.end method
