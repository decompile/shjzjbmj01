.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;
.super Ljava/lang/Object;
.source "AdEvent.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->a:Ljava/lang/String;

    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->b:Lorg/json/JSONObject;

    return-void
.end method

.method static a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Lorg/json/JSONObject;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;
    .locals 6

    :cond_0
    :goto_0
    const/16 v0, 0x3b

    const/16 v1, 0x5e

    const/4 v2, 0x1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    :cond_1
    :pswitch_1
    const/16 v0, 0x48

    mul-int v1, v0, v0

    const/16 v3, 0x31

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    const/4 v4, 0x4

    mul-int v5, v4, v4

    add-int/2addr v1, v5

    const/16 v5, 0x48

    mul-int/lit8 v5, v5, 0x31

    mul-int/lit8 v3, v3, 0x4

    add-int/2addr v5, v3

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v5, v0

    if-ge v1, v5, :cond_2

    :pswitch_2
    const/16 v0, 0x39

    rsub-int/lit8 v1, v2, 0x39

    mul-int/lit8 v1, v1, 0x39

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v2

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    if-eqz v1, :cond_1

    :pswitch_3
    const/16 v0, 0x34

    mul-int v1, v0, v0

    const/16 v3, 0x1f

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    const/16 v4, 0xf

    mul-int v5, v4, v4

    add-int/2addr v1, v5

    const/16 v5, 0x34

    mul-int/lit8 v5, v5, 0x1f

    mul-int/lit8 v3, v3, 0xf

    add-int/2addr v5, v3

    mul-int/lit8 v0, v0, 0xf

    add-int/2addr v5, v0

    if-ge v1, v5, :cond_2

    goto :goto_2

    :goto_1
    :pswitch_4
    packed-switch v0, :pswitch_data_2

    goto :goto_3

    :goto_2
    :pswitch_5
    const/16 v0, 0x1e

    rsub-int/lit8 v1, v2, 0x1e

    mul-int/lit8 v1, v1, 0x1e

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v2

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    if-eqz v1, :cond_0

    :pswitch_6
    const/16 v0, 0x5f

    mul-int v1, v0, v0

    const/16 v2, 0xa

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    mul-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v1, v0

    :cond_2
    :pswitch_7
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->b(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;-><init>(Ljava/lang/String;Lorg/json/JSONObject;)V

    return-object v0

    :goto_3
    const/16 v0, 0x5d

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_4
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static b(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 8

    :cond_0
    :goto_0
    const/16 v0, 0x41

    const/4 v1, 0x4

    const/4 v2, -0x1

    const/4 v3, 0x1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x4b

    mul-int v0, v0, v0

    const/16 v1, 0x21

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    if-ne v0, v2, :cond_0

    goto :goto_2

    :goto_1
    :pswitch_2
    const/16 v1, 0x47

    mul-int v4, v1, v1

    const/16 v5, 0x25

    mul-int v6, v5, v5

    add-int/2addr v4, v6

    const/16 v6, 0x1b

    mul-int v7, v6, v6

    add-int/2addr v4, v7

    const/16 v7, 0x47

    mul-int/lit8 v7, v7, 0x25

    mul-int/lit8 v5, v5, 0x1b

    add-int/2addr v7, v5

    mul-int/lit8 v1, v1, 0x1b

    add-int/2addr v7, v1

    if-ge v4, v7, :cond_0

    :pswitch_3
    packed-switch v0, :pswitch_data_2

    goto :goto_3

    :goto_2
    :pswitch_4
    const/16 v0, 0x44

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x44

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    :cond_1
    :pswitch_5
    const/4 v0, 0x0

    mul-int v0, v0, v0

    const/16 v1, 0x14

    mul-int/lit8 v1, v1, 0x14

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    if-ne v0, v2, :cond_2

    :pswitch_6
    const/16 v0, 0x4d

    mul-int v1, v0, v0

    const/16 v4, 0x28

    mul-int v5, v4, v4

    add-int/2addr v1, v5

    const/16 v5, 0x19

    mul-int v6, v5, v5

    add-int/2addr v1, v6

    const/16 v6, 0x4d

    mul-int/lit8 v6, v6, 0x28

    mul-int/lit8 v4, v4, 0x19

    add-int/2addr v6, v4

    mul-int/lit8 v0, v0, 0x19

    add-int/2addr v6, v0

    if-ge v1, v6, :cond_0

    const/16 v0, 0x20

    mul-int v1, v0, v0

    const/16 v4, 0x15

    mul-int v5, v4, v4

    add-int/2addr v1, v5

    mul-int v5, v3, v3

    add-int/2addr v1, v5

    const/16 v5, 0x20

    mul-int/lit8 v5, v5, 0x15

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v5, v4

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v5, v0

    if-ge v1, v5, :cond_1

    :cond_2
    :goto_3
    :pswitch_7
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    if-eqz p1, :cond_11

    const-string v1, "type"

    :try_start_0
    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "link_id"

    :try_start_1
    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v1, "adn_name"

    :try_start_2
    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const-string v1, "ad_sdk_version"

    :try_start_3
    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const-string v1, "rit_cpm"

    :try_start_4
    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    const-string v1, "mediation_rit"

    :try_start_5
    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const-string v1, "adtype"

    :try_start_6
    iget v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->q:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    const-string v1, "error_msg"

    :try_start_7
    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    const-string v1, "error_code"

    :try_start_8
    iget v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->n:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    const-string v1, "creative_id"

    :try_start_9
    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    const-string v1, "exchange_rate"

    :try_start_a
    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->e()Ljava/lang/String;

    move-result-object v1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    if-eqz v1, :cond_3

    const-string v1, "app_abtest"

    :try_start_b
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v3

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_3
    iget v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->s:I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    if-eq v1, v2, :cond_4

    const-string v1, "result"

    :try_start_c
    iget v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->s:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_4
    iget v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->t:I
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    if-eq v1, v2, :cond_5

    const-string v1, "status_code"

    :try_start_d
    iget v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->t:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_5
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->p:Ljava/lang/String;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0

    if-eqz v1, :cond_6

    const-string v1, "show_sort"

    :try_start_e
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_6
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->o:Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0

    if-eqz v1, :cond_7

    const-string v1, "load_sort"

    :try_start_f
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_7
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->m:Ljava/lang/String;
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_0

    if-eqz v1, :cond_8

    const-string v1, "req_biding_type"

    :try_start_10
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    :cond_8
    const-string v1, "prime_rit"

    :try_start_11
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0

    const-string v1, "media_fill_fail"

    :try_start_12
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_0

    if-nez v1, :cond_9

    const-string v1, "media_fill"

    :try_start_13
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_0

    if-nez v1, :cond_9

    const-string v1, "get_config_final"

    :try_start_14
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_0

    if-nez v1, :cond_9

    const-string v1, "sdk_init_end"

    :try_start_15
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_0

    if-nez v1, :cond_9

    const-string v1, "return_bidding_result"

    :try_start_16
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_0

    if-eqz v1, :cond_a

    :cond_9
    const-string v1, "duration"

    :try_start_17
    iget-wide v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->r:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_0

    :cond_a
    const-string v1, "total_load_fail"

    :try_start_18
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_0

    if-nez v1, :cond_c

    const-string v1, "adapter_request_fail"

    :try_start_19
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_b
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e:Ljava/lang/String;

    :goto_4
    const-string v2, "req_id"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_0

    :cond_c
    const-string v1, "country"

    :try_start_1a
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v1

    const-string v2, "pangle"

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v1
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_0

    if-eqz v1, :cond_d

    const-string v1, "app_id"

    :try_start_1b
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v2

    const-string v3, "pangle"

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v2
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_0

    goto :goto_5

    :cond_d
    const-string v1, "app_id"

    :try_start_1c
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c()Ljava/lang/String;

    move-result-object v2

    :goto_5
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-wide v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i:J
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_0

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_e

    const-string v1, "waterfall_id"

    :try_start_1d
    iget-wide v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_e
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_0

    if-nez v1, :cond_f

    const-string v1, "version"

    :try_start_1e
    iget-object v2, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_f
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->v:Ljava/util/Map;

    if-eqz v1, :cond_11

    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->v:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_11

    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->v:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_10
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->v:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    if-eqz v3, :cond_10

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_6

    :cond_11
    if-eqz p2, :cond_12

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_7

    :cond_12
    const/4 p2, 0x0

    :goto_7
    const-string v1, "event_extra"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_0

    const-string p2, "app_version"

    :try_start_1f
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/x;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_0

    const-string p2, "conn_type"

    :try_start_20
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/m;->c(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_0

    const-string p2, "timestamp"

    :try_start_21
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p2, "mediation_sdk_version"

    const-string v1, "2.6.0.0"

    invoke-virtual {v0, p2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_0

    const-string p2, "device_info"

    :try_start_22
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->b(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-virtual {v0, p2, p0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_0

    if-eqz p1, :cond_13

    const-string p0, "get_config_start"

    :try_start_23
    iget-object p1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_0

    if-eqz p0, :cond_13

    const-string p0, "timestamp"

    :try_start_24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    const-wide/16 v1, 0xbb8

    sub-long/2addr p1, v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_0

    :catch_0
    :cond_13
    return-object v0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x41
        :pswitch_7
        :pswitch_7
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x57
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->a:Ljava/lang/String;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method b()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->b:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "localId"

    :try_start_0
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "event"

    :try_start_1
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->b:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
