.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;
.super Landroid/os/HandlerThread;
.source "AdEventThread.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;,
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;",
        ">",
        "Landroid/os/HandlerThread;",
        "Landroid/os/Handler$Callback;"
    }
.end annotation


# static fields
.field public static j:Ljava/lang/String; = "AdEventThread"

.field public static k:Ljava/lang/String; = "ttad_bk"


# instance fields
.field public final a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a<",
            "TT;>;"
        }
    .end annotation
.end field

.field public b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field public d:J

.field public e:Z

.field public f:I

.field public g:Landroid/os/Handler;

.field public final h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;

.field public final i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;


# direct methods
.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a<",
            "TT;>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a<",
            "TT;>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;",
            ")V"
        }
    .end annotation

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->k:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    .line 3
    iput-object p4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;

    .line 4
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    .line 5
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;

    .line 6
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    return-void
.end method

.method private a()V
    .locals 2

    .line 28
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a(Ljava/util/List;)V

    .line 29
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method private a(IJ)V
    .locals 1

    .line 30
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->g:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 31
    iput p1, v0, Landroid/os/Message;->what:I

    .line 32
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->g:Landroid/os/Handler;

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/util/List;)V

    .line 17
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    invoke-interface {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a(Ljava/lang/Object;)V

    .line 18
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    if-eqz v0, :cond_0

    .line 19
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\u5982\u679c\u5728\u5bb9\u707e\u72b6\u6001\uff0c\u76f4\u63a5\u8fd4\u56de.......mIsServerBusy="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/util/Logger;->w(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "onHandleReceivedAdEvent"

    .line 23
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "onHandleReceivedAdEvent upload"

    .line 26
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f()V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .line 34
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->j:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x118

    if-gt v0, v1, :cond_1

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start and return, checkAndDeleteEvent local size:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "\u5c0f\u4e8e:"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    return-void

    .line 6
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x43600000    # 224.0f

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start checkAndDeleteEvent local size,deleteCnt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 8
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 10
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;

    .line 11
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 13
    :cond_2
    invoke-interface {p1, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 14
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    invoke-interface {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a(Ljava/util/List;)V

    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "end checkAndDeleteEvent local size:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    return-void
.end method

.method private static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;)Z
    .locals 0

    .line 33
    iget-boolean p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;->d:Z

    return p0
.end method

.method private b()V
    .locals 2

    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    .line 15
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    invoke-interface {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a(Z)V

    .line 16
    iput v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    .line 17
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    invoke-interface {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a(I)V

    .line 18
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->g:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    .line 1
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 5
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 6
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;

    .line 7
    invoke-interface {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 9
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;

    .line 10
    invoke-interface {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 11
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    :goto_2
    const-string p1, "reloadCacheList adEventList is empty======"

    .line 12
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    return-void
.end method

.method private static b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;)Z
    .locals 1

    .line 13
    iget p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;->b:I

    const/16 v0, 0x1fd

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private c(Ljava/util/List;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;"
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;

    if-nez v0, :cond_0

    .line 2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;

    move-result-object v0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7
    :catch_0
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    .line 11
    :cond_1
    invoke-interface {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;->a(Ljava/util/List;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;

    move-result-object p1

    return-object p1
.end method

.method private c()V
    .locals 3

    .line 12
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget-wide v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->b:J

    const/4 v2, 0x2

    invoke-direct {p0, v2, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(IJ)V

    return-void
.end method

.method private d()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->g()J

    move-result-wide v0

    const/4 v2, 0x4

    invoke-direct {p0, v2, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(IJ)V

    return-void
.end method

.method private e()V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u666e\u901a\u5931\u8d25 \uff0c\u89e6\u53d1\u91cd\u8bd5\u673a\u5236\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget-wide v1, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\u6beb\u79d2\u540e \u91cd\u8bd5....."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget-wide v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->c:J

    const/4 v2, 0x3

    invoke-direct {p0, v2, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(IJ)V

    return-void
.end method

.method private f()V
    .locals 2

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->g:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->g:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->j:Ljava/lang/String;

    const-string v1, "\u5982\u679c\u6709\uff0c\u79fb\u9664\u666e\u901a\u5931\u8d25\u91cd\u8bd5\u4e8b\u4ef6-MSG_ROUTINE_FAIL_RETRY\uff0c\u89e6\u53d1\u65b0\u7684\u4e0a\u62a5"

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/k;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->d:J

    .line 9
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c()V

    return-void

    .line 12
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "doRoutineUpload no net, wait retry"

    .line 13
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e()V

    return-void

    .line 17
    :cond_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c(Ljava/util/List;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 20
    iget-boolean v1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;->a:Z

    if-eqz v1, :cond_2

    const-string v0, "doRoutineUpload success"

    .line 21
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a()V

    .line 25
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->n()V

    goto :goto_0

    .line 28
    :cond_2
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "doRoutineUpload serverbusy"

    .line 29
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->h()V

    goto :goto_0

    .line 32
    :cond_3
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "\u670d\u52a1\u7aef\u8fd4\u56dedata error \u629b\u5f03\u6570\u636e , \u6e05\u7a7a\u672c\u6b21\u65e5\u5fd7\uff0c\u91cd\u7f6e\u4e0a\u4f20\u72b6\u6001...."

    .line 33
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a()V

    .line 37
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->n()V

    goto :goto_0

    .line 38
    :cond_4
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    if-nez v0, :cond_5

    .line 40
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e()V

    const-string v0, "doRoutineUpload net fail retry"

    .line 41
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    :cond_5
    :goto_0
    return-void
.end method

.method private g()J
    .locals 4

    .line 1
    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    rem-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget-wide v2, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->f:J

    mul-long v0, v0, v2

    .line 2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u670d\u52a1\u5668\u7e41\u5fd9\uff0c"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "\u6beb\u79d2\u540e\u8fdb\u884c\u91cd\u8bd5,\u5f53\u524d\u91cd\u8bd5\u6b21\u6570\uff1amServerBusyRetryCount="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "\uff0cmServerBusyRetryBaseInternal="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget-wide v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->f:J

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 4
    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    rem-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget-wide v2, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->f:J

    mul-long v0, v0, v2

    return-wide v0
.end method

.method private h()V
    .locals 2

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    .line 2
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    invoke-interface {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a(Z)V

    .line 3
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->g:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 5
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->g:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 6
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->d()V

    return-void
.end method

.method private i()Z
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCacheList.size():"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",mPolicy.mMaxCacheCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget v1, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",System.currentTimeMillis() - mLastSuccessUploadTime ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->d:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ",mPolicy.mMaxCacheTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget-wide v1, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 2
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget v1, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->a:I

    if-ge v0, v1, :cond_0

    .line 3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->d:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget-wide v2, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->b:J

    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private j()V
    .locals 5

    const-string v0, "onHandleInitEvent \u521d\u59cb\u5316\u65e5\u5fd7\u7ec4\u4ef6....."

    .line 1
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget v2, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->d:I

    iget-wide v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->e:J

    invoke-interface {v0, v2, v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a(IJ)V

    .line 4
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->c()Z

    move-result v0

    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    .line 5
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->b()I

    move-result v0

    iput v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    .line 6
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    if-eqz v0, :cond_0

    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onHandleInitEvent serverBusy, retryCount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->d()V

    goto :goto_0

    .line 10
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a()Ljava/util/List;

    move-result-object v0

    .line 11
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->b(Ljava/util/List;)V

    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onHandleInitEvent cacheData count = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f()V

    :goto_0
    return-void
.end method

.method private k()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    if-eqz v0, :cond_0

    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u5982\u679c\u5728\u5bb9\u707e\u72b6\u6001\uff0c\u76f4\u63a5\u8fd4\u56de, \u5b89\u5168\u8d77\u89c1\u8fd9\u91cc\u5224\u65ad\u4e00\u4e0b,mIsServerBusy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "onHandleRoutineRetryEvent"

    .line 6
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f()V

    return-void
.end method

.method private l()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    if-eqz v0, :cond_0

    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u5982\u679c\u5728\u5bb9\u707e\u72b6\u6001\uff0c\u76f4\u63a5\u8fd4\u56de, \u5b89\u5168\u8d77\u89c1\u8fd9\u91cc\u5224\u65ad\u4e00\u4e0b,mIsServerBusy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "onHandleRoutineUploadEvent"

    .line 6
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f()V

    return-void
.end method

.method private m()V
    .locals 6

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget-wide v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->c:J

    const/4 v2, 0x4

    invoke-direct {p0, v2, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(IJ)V

    const-string v0, "onHandleServerBusyRetryEvent, no net"

    .line 4
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    return-void

    .line 7
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a()Ljava/util/List;

    move-result-object v0

    .line 8
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/util/List;)V

    .line 9
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/k;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "onHandleServerBusyRetryEvent, empty list start routine"

    .line 10
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->b()V

    .line 15
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c()V

    return-void

    .line 18
    :cond_1
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c(Ljava/util/List;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 20
    iget-boolean v2, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;->a:Z

    if-eqz v2, :cond_2

    const-string v0, "onHandleServerBusyRetryEvent, success"

    .line 21
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a()V

    .line 25
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->n()V

    goto :goto_0

    .line 27
    :cond_2
    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 28
    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    .line 29
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    iget v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    invoke-interface {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a(I)V

    .line 30
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    iget v3, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->d:I

    iget-wide v4, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->e:J

    invoke-interface {v1, v0, v3, v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;->a(Ljava/util/List;IJ)V

    .line 33
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->d()V

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onHandleServerBusyRetryEvent, serverbusy, count = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_3
    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/f;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "onHandleServerBusyRetryEvent, ---\u300b data Error\u76f4\u63a5\u629b\u5f03\u6570\u636e "

    .line 37
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a()V

    .line 41
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->n()V

    goto :goto_0

    .line 44
    :cond_4
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->e()V

    const-string v0, "onHandleServerBusyRetryEvent, net fail"

    .line 45
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    :cond_5
    :goto_0
    return-void
.end method

.method private n()V
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->d:J

    .line 3
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->b()V

    .line 5
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->c()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 1

    .line 1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 17
    :pswitch_0
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->j()V

    goto :goto_0

    :pswitch_1
    const-string p1, "\u89e6\u53d1\u670d\u52a1\u5668\u7e41\u5fd9\u91cd\u8bd5\u673a\u5236....."

    .line 18
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->m()V

    goto :goto_0

    .line 20
    :pswitch_2
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->k()V

    goto :goto_0

    .line 21
    :pswitch_3
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->l()V

    goto :goto_0

    .line 22
    :pswitch_4
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;

    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/h;)V

    :goto_0
    const/4 p1, 0x1

    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onLooperPrepared()V
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->d:J

    .line 2
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e;->g:Landroid/os/Handler;

    return-void
.end method
