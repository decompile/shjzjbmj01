.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;
.super Ljava/lang/Object;
.source "AdEventUtil.java"


# direct methods
.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 7

    :goto_0
    const/16 v0, 0x8

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x5a

    mul-int v1, v0, v0

    const/16 v2, 0x24

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x10

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    const/16 v4, 0x5a

    mul-int/lit8 v4, v4, 0x24

    mul-int/lit8 v2, v2, 0x10

    add-int/2addr v4, v2

    mul-int/lit8 v0, v0, 0x10

    add-int/2addr v4, v0

    :pswitch_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const-string v1, "adapter_request_fail"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_0
    const-string v2, "unknown"

    :goto_1
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_1
    const-string v2, "unknown"

    :goto_2
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_2
    const-string v2, "unknown"

    :goto_3
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_3
    move-object v3, v2

    :goto_4
    invoke-virtual {v1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    const-wide/16 v3, 0x0

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v5

    goto :goto_5

    :cond_4
    move-wide v5, v3

    :goto_5
    invoke-virtual {v1, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_5

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v5

    goto :goto_6

    :cond_5
    const/4 v5, 0x0

    :goto_6
    invoke-virtual {v1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v5

    goto :goto_7

    :cond_6
    const/16 v5, -0x64

    :goto_7
    invoke-virtual {v1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    const/4 v5, -0x2

    invoke-virtual {v1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    const-string v6, "adapter create fail !"

    invoke-virtual {v1, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_7

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f()I

    move-result v6

    goto :goto_8

    :cond_7
    const/4 v6, -0x2

    :goto_8
    invoke-virtual {v1, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_8

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->n()I

    move-result v5

    :cond_8
    invoke-virtual {v1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_9

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->o()Ljava/lang/String;

    move-result-object v5

    goto :goto_9

    :cond_9
    move-object v5, v2

    :goto_9
    const-string v6, "waterfall_abtest"

    invoke-virtual {v1, v6, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_a

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->l()Ljava/lang/String;

    move-result-object v5

    goto :goto_a

    :cond_a
    move-object v5, v2

    :goto_a
    const-string v6, "server_bidding_extra"

    invoke-virtual {v1, v6, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_b

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->g()Ljava/lang/String;

    move-result-object v2

    :cond_b
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const-string v1, "0"

    invoke-virtual {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->j(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p0, v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    if-eqz p1, :cond_c

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object p1

    goto :goto_b

    :cond_c
    const-string p1, "unknown"

    :goto_b
    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 7
    .param p1    # Lcom/bytedance/msdk/api/AdSlot;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    :goto_0
    :pswitch_0
    const/16 v0, 0x13

    const/16 v1, 0x2d

    const/4 v2, 0x6

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :cond_0
    :pswitch_1
    const/16 v0, 0x22

    mul-int v2, v0, v0

    const/16 v3, 0x27

    mul-int v3, v3, v3

    mul-int/lit8 v3, v3, 0x22

    sub-int/2addr v2, v3

    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    goto :goto_3

    :pswitch_2
    const/4 v2, 0x5

    mul-int v3, v2, v2

    const/16 v4, 0xc

    mul-int v5, v4, v4

    add-int/2addr v3, v5

    const/16 v5, 0x14

    mul-int v6, v5, v5

    add-int/2addr v3, v6

    const/4 v6, 0x5

    mul-int/lit8 v6, v6, 0xc

    mul-int/lit8 v4, v4, 0x14

    add-int/2addr v6, v4

    mul-int/lit8 v2, v2, 0x14

    add-int/2addr v6, v2

    if-ge v3, v6, :cond_1

    goto :goto_2

    :cond_1
    :goto_1
    :pswitch_3
    const/16 v0, 0x12

    const/16 v1, 0x8

    :goto_2
    :pswitch_4
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    :goto_3
    :pswitch_5
    packed-switch v1, :pswitch_data_2

    :pswitch_6
    packed-switch v1, :pswitch_data_3

    :pswitch_7
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_2
    move-object v2, v1

    :goto_4
    invoke-virtual {v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    :cond_3
    move-object v3, v1

    :goto_5
    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    const/4 v3, -0x3

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f()I

    move-result v4

    goto :goto_6

    :cond_4
    const/4 v4, -0x3

    :goto_6
    invoke-virtual {v2, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p0, :cond_5

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->n()I

    move-result v3

    :cond_5
    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p0, :cond_6

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v3

    goto :goto_7

    :cond_6
    const/4 v3, 0x0

    :goto_7
    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p0, :cond_7

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e()D

    move-result-wide v3

    goto :goto_8

    :cond_7
    const-wide/16 v3, 0x0

    :goto_8
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->j(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p0, :cond_8

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->g()Ljava/lang/String;

    move-result-object v3

    goto :goto_9

    :cond_8
    move-object v3, v1

    :goto_9
    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v3

    goto :goto_a

    :cond_9
    const-string v3, "unknown"

    :goto_a
    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v3

    goto :goto_b

    :cond_a
    move-object v3, v1

    :goto_b
    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v3

    goto :goto_c

    :cond_b
    const-wide/16 v3, 0x0

    :goto_c
    invoke-virtual {v2, v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p1, :cond_c

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v3

    goto :goto_d

    :cond_c
    const/16 v3, -0x64

    :goto_d
    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    if-eqz p1, :cond_d

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object p1

    goto :goto_e

    :cond_d
    const-string p1, "unknown"

    :goto_e
    invoke-virtual {v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    if-eqz p0, :cond_e

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->o()Ljava/lang/String;

    move-result-object p2

    goto :goto_f

    :cond_e
    move-object p2, v1

    :goto_f
    const-string v2, "waterfall_abtest"

    invoke-virtual {p1, v2, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    if-eqz p0, :cond_f

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->l()Ljava/lang/String;

    move-result-object v1

    :cond_f
    const-string p0, "server_bidding_extra"

    invoke-virtual {p1, p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x12
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x8
        :pswitch_7
        :pswitch_7
        :pswitch_0
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x2d
        :pswitch_3
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 9

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    const-string v3, "total_load_fail"

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move v7, p3

    move-object v8, p4

    invoke-static/range {v3 .. v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Ljava/lang/String;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 4
    .param p0    # Lcom/bytedance/msdk/api/AdSlot;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    :cond_0
    :pswitch_0
    const/16 v0, 0x10

    :goto_0
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    :cond_1
    :pswitch_1
    const/16 v0, 0x1e

    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x8

    mul-int v1, v0, v0

    const/16 v2, 0x1b

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    mul-int/lit8 v0, v0, 0x1b

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v1, v0

    if-gez v1, :cond_0

    :pswitch_2
    const/4 v0, 0x1

    const/16 v1, 0x2d

    rsub-int/lit8 v2, v0, 0x2d

    mul-int/lit8 v2, v2, 0x2d

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    goto/16 :goto_2

    :pswitch_3
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->j(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p1, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    const-string p1, " "

    :goto_1
    invoke-virtual {v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, ""

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "primerit_req_type"

    invoke-virtual {p1, p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    return-object v0

    :goto_2
    :pswitch_4
    const/16 v0, 0xe

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1d
        :pswitch_2
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 4
    .param p0    # Lcom/bytedance/msdk/api/AdSlot;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    :cond_0
    :goto_0
    :pswitch_0
    const/16 v0, 0x2a

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x25

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x25

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    :pswitch_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->j(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string v2, "unknown"

    :goto_1
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v2

    goto :goto_3

    :cond_3
    const-wide/16 v2, 0x0

    :goto_3
    invoke-virtual {v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    if-eqz p2, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " ["

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "] "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_4

    :cond_4
    const-string p2, " "

    :goto_4
    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x28
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/bytedance/msdk/base/TTBaseAd;ILjava/lang/String;Ljava/lang/String;J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v1

    invoke-static {v1}, Lcom/bytedance/msdk/base/a;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getRit()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getSdkVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotType()I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getExchangeRate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdType()I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getReqId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->h(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getEventLoadSort()I

    move-result p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->j(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p1, p4, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getEventMap()Ljava/util/Map;

    move-result-object p0

    invoke-virtual {p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/util/Map;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p0, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    return-object v0
.end method

.method private static a(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 4

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto/16 :goto_0

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v1

    invoke-static {v1}, Lcom/bytedance/msdk/base/a;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getExchangeRate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotType()I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getReqId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->h(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getSdkVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getEventLoadSort()I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->f(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->j(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getEventMap()Ljava/util/Map;

    move-result-object p0

    invoke-virtual {v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/util/Map;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    return-object v0

    :cond_1
    :goto_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/lang/String;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;
    .locals 4

    if-nez p1, :cond_0

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v1

    invoke-virtual {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const-string p1, "waterfall_abtest"

    invoke-virtual {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const-string p1, "server_bidding_extra"

    invoke-virtual {p0, p1, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p0, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p0, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    return-object v0
.end method

.method public static a()V
    .locals 6

    :cond_0
    :goto_0
    const/16 v0, 0x25

    const/16 v1, 0x36

    const/4 v2, 0x1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x12

    mul-int v0, v0, v0

    const/16 v1, 0x23

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :pswitch_2
    const/16 v0, 0x1a

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x1a

    const/16 v3, 0x9

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0x9

    add-int/2addr v1, v4

    const/16 v4, 0xf

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0xf

    add-int/2addr v1, v5

    mul-int/lit8 v0, v0, 0x9

    mul-int/lit8 v0, v0, 0xf

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_1

    :pswitch_3
    const/16 v0, 0x28

    rsub-int/lit8 v1, v2, 0x28

    mul-int/lit8 v1, v1, 0x28

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v2

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    if-eqz v1, :cond_1

    goto :goto_2

    :goto_1
    :pswitch_4
    const/16 v0, 0x1e

    rsub-int/lit8 v1, v2, 0x1e

    mul-int/lit8 v1, v1, 0x1e

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v2

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    if-eqz v1, :cond_0

    :goto_2
    :pswitch_5
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const-string v1, "get_config_start"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(IIJ)V
    .locals 5

    :cond_0
    :goto_0
    const/16 v0, 0x5f

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x54

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x54

    const/16 v2, 0x2a

    mul-int v3, v2, v2

    mul-int/lit8 v3, v3, 0x2a

    add-int/2addr v1, v3

    const/16 v3, 0x16

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0x16

    add-int/2addr v1, v4

    mul-int/lit8 v0, v0, 0x2a

    mul-int/lit8 v0, v0, 0x16

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_0

    const/16 v0, 0x46

    mul-int v1, v0, v0

    const/16 v2, 0x2c

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    mul-int/lit8 v0, v0, 0x2c

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v1, v0

    if-gez v1, :cond_1

    const/16 v0, 0x12

    mul-int v0, v0, v0

    const/16 v1, 0x23

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    const/16 v1, 0x1e

    rsub-int/lit8 v2, v0, 0x1e

    mul-int/lit8 v2, v2, 0x1e

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    :pswitch_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p0, p2, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const-string p2, "get_config_final"

    invoke-virtual {p0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static a(ILjava/lang/String;ZLjava/lang/String;)V
    .locals 2

    if-eqz p2, :cond_0

    const-string p2, "  mSdk\u5df2\u7ecf\u521d\u59cb\u5316"

    goto :goto_0

    :cond_0
    const-string p2, "  mSdk\u672a\u521d\u59cb\u5316"

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p2

    const-string v0, "adn_init_fail"

    invoke-virtual {p2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->d(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    invoke-virtual {p0, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, p2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_1
    const/16 p0, 0x8c

    const/16 p1, 0x49

    const/16 p2, 0x22

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    packed-switch p0, :pswitch_data_1

    goto :goto_2

    :pswitch_1
    const/16 p0, 0x9

    add-int/lit8 p0, p0, 0x1

    mul-int/lit8 p0, p0, 0x9

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_1

    :pswitch_2
    const/16 p0, 0x52

    mul-int p0, p0, p0

    const/16 p1, 0x28

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr p0, p1

    const/4 p1, -0x1

    if-ne p0, p1, :cond_3

    :pswitch_3
    const/16 p0, 0x5a

    add-int/lit8 p0, p0, 0x1

    mul-int/lit8 p0, p0, 0x5a

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_3

    goto :goto_1

    :cond_2
    :pswitch_4
    const/16 p0, 0x39

    mul-int p1, p0, p0

    mul-int p3, p2, p2

    add-int/2addr p1, p3

    const/16 p3, 0xc

    mul-int v0, p3, p3

    add-int/2addr p1, v0

    const/16 v0, 0x39

    mul-int/lit8 v0, v0, 0x22

    mul-int/lit8 v1, p2, 0xc

    add-int/2addr v0, v1

    mul-int/lit8 p0, p0, 0xc

    add-int/2addr v0, p0

    if-ge p1, v0, :cond_2

    :cond_3
    :goto_2
    :pswitch_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x34
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(JII)V
    .locals 6

    :cond_0
    :goto_0
    const/4 v0, -0x1

    const/16 v1, 0x35

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v1, 0x3d

    mul-int v1, v1, v1

    const/16 v2, 0xc

    mul-int v2, v2, v2

    mul-int/lit8 v2, v2, 0x22

    sub-int/2addr v1, v2

    if-ne v1, v0, :cond_0

    goto :goto_5

    :cond_1
    :pswitch_1
    const/16 v2, 0xd

    mul-int v2, v2, v2

    const/16 v3, 0x13

    mul-int v3, v3, v3

    mul-int/lit8 v3, v3, 0x22

    sub-int/2addr v2, v3

    if-ne v2, v0, :cond_1

    :goto_1
    :pswitch_2
    const/16 v0, 0x31

    const/16 v2, 0x3c

    const/4 v3, 0x1

    packed-switch v2, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch v0, :pswitch_data_2

    goto :goto_4

    :pswitch_4
    const/16 v0, 0x57

    rsub-int/lit8 v2, v3, 0x57

    mul-int/lit8 v2, v2, 0x57

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v3

    mul-int v2, v2, v0

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    const/16 v0, 0x35

    :goto_2
    packed-switch v0, :pswitch_data_3

    goto :goto_3

    :pswitch_5
    const/16 v0, 0x5e

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x5e

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    goto :goto_1

    :goto_3
    const/16 v0, 0x38

    goto :goto_2

    :goto_4
    :pswitch_6
    const/16 v0, 0x1a

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x1a

    const/16 v2, 0x9

    mul-int v4, v2, v2

    mul-int/lit8 v4, v4, 0x9

    add-int/2addr v1, v4

    const/16 v4, 0xf

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0xf

    add-int/2addr v1, v5

    mul-int/lit8 v0, v0, 0x9

    mul-int/lit8 v0, v0, 0xf

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_4

    :pswitch_7
    const/16 v0, 0x28

    rsub-int/lit8 v1, v3, 0x28

    mul-int/lit8 v1, v1, 0x28

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v3

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    :cond_3
    :goto_5
    :pswitch_8
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const-string v1, "sdk_init_end"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {v0, p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const-string p1, "adn_count"

    invoke-virtual {v0, p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string p2, "is_from_local_config"

    invoke-interface {p0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_4
    return-void

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3c
        :pswitch_3
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x31
        :pswitch_8
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x37
        :pswitch_5
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;I)V
    .locals 4

    invoke-static {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    if-eqz p0, :cond_3

    const-string p2, "media_request"

    invoke-virtual {p0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const-string v0, "adn_count"

    invoke-virtual {p2, v0, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p1, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, ""

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "primerit_req_type"

    invoke-virtual {p0, p3, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string p2, "mediationrit_req_type"

    invoke-virtual {p0, p2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    :pswitch_0
    const/16 p0, 0x53

    const/16 p1, 0x51

    const/16 p2, 0x22

    const/16 p3, 0x11

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    const/16 p0, 0x1b

    mul-int p1, p0, p0

    mul-int v0, p2, p2

    add-int/2addr p1, v0

    mul-int v0, p3, p3

    add-int/2addr p1, v0

    const/16 v0, 0x1b

    mul-int/lit8 v0, v0, 0x22

    const/16 p2, 0x22

    mul-int/lit8 p2, p2, 0x11

    add-int/2addr v0, p2

    mul-int/lit8 p0, p0, 0x11

    add-int/2addr v0, p0

    if-ge p1, v0, :cond_3

    goto :goto_0

    :goto_1
    :pswitch_2
    const/16 v0, 0xa

    const/4 v1, 0x1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0xa

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    goto :goto_4

    :cond_2
    :goto_2
    packed-switch p1, :pswitch_data_1

    goto :goto_3

    :pswitch_3
    const/4 v0, 0x0

    rsub-int/lit8 v2, v1, 0x0

    mul-int/lit8 v2, v2, 0x0

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v1

    mul-int v2, v2, v0

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_3

    :pswitch_4
    packed-switch p0, :pswitch_data_2

    goto :goto_0

    :pswitch_5
    const/16 v0, 0xf

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0xf

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    :pswitch_6
    const/16 v0, 0x44

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x44

    const/16 v2, 0xe

    mul-int v3, v2, v2

    mul-int/lit8 v3, v3, 0xe

    add-int/2addr v1, v3

    mul-int v3, p3, p3

    mul-int/lit8 v3, v3, 0x11

    add-int/2addr v1, v3

    mul-int/lit8 v0, v0, 0xe

    mul-int/lit8 v0, v0, 0x11

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_1

    :pswitch_7
    const/16 v0, 0xd

    mul-int v0, v0, v0

    const/16 v1, 0x13

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    goto :goto_1

    :goto_3
    const/16 p1, 0x28

    const/16 p0, 0x39

    goto :goto_2

    :cond_3
    :goto_4
    :pswitch_8
    return-void

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_2
        :pswitch_8
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x27
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x39
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 5

    if-eqz p0, :cond_4

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "primerit_req_type"

    invoke-virtual {p0, v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v0, "mediationrit_req_type"

    invoke-virtual {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    :pswitch_0
    const/16 p0, 0x5e

    const/4 p1, 0x1

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :cond_2
    :pswitch_1
    const/16 p0, 0x2d

    add-int/2addr p0, p1

    mul-int/lit8 p0, p0, 0x2d

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_2

    :cond_3
    :goto_1
    :pswitch_2
    const/16 p0, 0x53

    const/16 v0, 0x29

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p0, :pswitch_data_2

    goto :goto_0

    :pswitch_4
    const/16 p0, 0x11

    add-int/2addr p0, p1

    mul-int/lit8 p0, p0, 0x11

    rem-int/lit8 p0, p0, 0x2

    goto :goto_1

    :pswitch_5
    const/16 v0, 0xe

    mul-int v1, v0, v0

    const/16 v2, 0x25

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x9

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    const/16 v4, 0xe

    mul-int/lit8 v4, v4, 0x25

    mul-int/lit8 v2, v2, 0x9

    add-int/2addr v4, v2

    mul-int/lit8 v0, v0, 0x9

    add-int/2addr v4, v0

    if-ge v1, v4, :cond_1

    :goto_2
    :pswitch_6
    const/4 v0, -0x1

    packed-switch p0, :pswitch_data_3

    goto :goto_3

    :pswitch_7
    const/16 p0, 0x16

    mul-int p0, p0, p0

    const/16 v1, 0xa

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr p0, v1

    if-ne p0, v0, :cond_3

    :pswitch_8
    const/16 p0, 0x3d

    mul-int p0, p0, p0

    const/16 v1, 0xc

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr p0, v1

    if-ne p0, v0, :cond_4

    :pswitch_9
    const/16 p0, 0x26

    rsub-int/lit8 v0, p1, 0x26

    mul-int/lit8 v0, v0, 0x26

    mul-int/lit8 p0, p0, 0x2

    sub-int/2addr p0, p1

    mul-int v0, v0, p0

    rem-int/lit8 v0, v0, 0x6

    goto :goto_4

    :goto_3
    const/16 p0, 0x3a

    goto :goto_2

    :cond_4
    :goto_4
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x27
        :pswitch_5
        :pswitch_6
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x52
        :pswitch_9
        :pswitch_a
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x39
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Lcom/bytedance/msdk/api/AdSlot;I)V
    .locals 5

    if-eqz p0, :cond_3

    const-string v0, "media_fill_fail"

    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string v1, "adn_count"

    invoke-virtual {v0, v1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p1, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "primerit_req_type"

    invoke-virtual {p0, v0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string p2, "mediationrit_req_type"

    invoke-virtual {p0, p2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1, p0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    :pswitch_0
    const/16 p0, 0x53

    const/16 p1, 0x51

    const/16 p2, 0x22

    const/16 v0, 0x11

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    const/16 p0, 0x1b

    mul-int p1, p0, p0

    mul-int v1, p2, p2

    add-int/2addr p1, v1

    mul-int v1, v0, v0

    add-int/2addr p1, v1

    const/16 v1, 0x1b

    mul-int/lit8 v1, v1, 0x22

    const/16 p2, 0x22

    mul-int/lit8 p2, p2, 0x11

    add-int/2addr v1, p2

    mul-int/lit8 p0, p0, 0x11

    add-int/2addr v1, p0

    if-ge p1, v1, :cond_3

    goto :goto_0

    :goto_1
    :pswitch_2
    const/16 v1, 0xa

    const/4 v2, 0x1

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0xa

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    goto :goto_4

    :cond_2
    :goto_2
    packed-switch p1, :pswitch_data_1

    goto :goto_3

    :pswitch_3
    const/4 v1, 0x0

    rsub-int/lit8 v3, v2, 0x0

    mul-int/lit8 v3, v3, 0x0

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v2

    mul-int v3, v3, v1

    rem-int/lit8 v3, v3, 0x6

    if-eqz v3, :cond_3

    :pswitch_4
    packed-switch p0, :pswitch_data_2

    goto :goto_0

    :pswitch_5
    const/16 v1, 0xf

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0xf

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    :pswitch_6
    const/16 v1, 0x44

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0x44

    const/16 v3, 0xe

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0xe

    add-int/2addr v2, v4

    mul-int v4, v0, v0

    mul-int/lit8 v4, v4, 0x11

    add-int/2addr v2, v4

    mul-int/lit8 v1, v1, 0xe

    mul-int/lit8 v1, v1, 0x11

    mul-int/lit8 v1, v1, 0x3

    if-ge v2, v1, :cond_1

    :pswitch_7
    const/16 v1, 0xd

    mul-int v1, v1, v1

    const/16 v2, 0x13

    mul-int v2, v2, v2

    mul-int/lit8 v2, v2, 0x22

    sub-int/2addr v1, v2

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    goto :goto_1

    :goto_3
    const/16 p1, 0x28

    const/16 p0, 0x39

    goto :goto_2

    :cond_3
    :goto_4
    :pswitch_8
    return-void

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_2
        :pswitch_8
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x27
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x39
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;I)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->c(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    const-string v1, "get_config_error"

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v1, "primerit_req_type"

    invoke-virtual {p1, v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    return-void
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;IJLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const-string v1, "return_bidding_result"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p2

    invoke-virtual {p2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    const-string p2, "waterfall_abtest"

    invoke-virtual {p1, p2, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    const-string p2, "server_bidding_extra"

    invoke-virtual {p1, p2, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide p2

    invoke-virtual {p1, p2, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, ""

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "primerit_req_type"

    invoke-virtual {p1, p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    return-void
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;Ljava/lang/String;)V
    .locals 7

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->s()Ljava/lang/String;

    move-result-object p1

    move-object v3, p1

    goto :goto_0

    :cond_0
    move-object v3, v0

    :goto_0
    const-string v1, "mediation_fill"

    const/4 v5, 0x0

    const-string v6, ""

    move-object v2, p0

    move-object v4, p2

    invoke-static/range {v1 .. v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Ljava/lang/String;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    if-eqz p0, :cond_1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "primerit_req_type"

    invoke-virtual {p1, p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_2
    :goto_1
    const/16 p0, 0x28

    const/16 p1, 0x1d

    const/16 p2, 0xd

    const/16 v0, 0x11

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    packed-switch p0, :pswitch_data_1

    goto :goto_2

    :pswitch_1
    const/16 v1, 0x44

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0x44

    const/16 v3, 0xe

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0xe

    add-int/2addr v2, v4

    mul-int v4, v0, v0

    mul-int/lit8 v4, v4, 0x11

    add-int/2addr v2, v4

    mul-int/lit8 v1, v1, 0xe

    mul-int/lit8 v1, v1, 0x11

    mul-int/lit8 v1, v1, 0x3

    if-ge v2, v1, :cond_6

    :pswitch_2
    mul-int v1, p2, p2

    const/16 v2, 0x13

    mul-int v2, v2, v2

    mul-int/lit8 v2, v2, 0x22

    sub-int/2addr v1, v2

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    const/16 v1, 0x28

    goto :goto_6

    :goto_2
    :pswitch_3
    const/16 v1, 0x28

    goto :goto_5

    :cond_3
    :goto_3
    :pswitch_4
    const/16 v1, 0x53

    packed-switch v1, :pswitch_data_2

    goto :goto_3

    :pswitch_5
    const/16 v1, 0x28

    :goto_4
    const/16 v2, 0x5e

    mul-int v3, v2, v2

    mul-int/lit8 v3, v3, 0x5e

    mul-int v4, v0, v0

    mul-int/lit8 v4, v4, 0x11

    add-int/2addr v3, v4

    const/4 v4, 0x7

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x7

    add-int/2addr v3, v5

    mul-int/lit8 v2, v2, 0x11

    mul-int/lit8 v2, v2, 0x7

    mul-int/lit8 v2, v2, 0x3

    if-ge v3, v2, :cond_4

    goto :goto_7

    :cond_4
    :goto_5
    packed-switch v1, :pswitch_data_3

    goto :goto_3

    :goto_6
    :pswitch_6
    const/16 v2, 0xa

    add-int/lit8 v2, v2, 0x1

    mul-int/lit8 v2, v2, 0xa

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_6

    :pswitch_7
    const/16 v2, 0x3d

    mul-int/lit8 v2, v2, 0x3d

    const/16 v3, 0xc

    mul-int/lit8 v3, v3, 0xc

    mul-int/lit8 v3, v3, 0x22

    sub-int/2addr v2, v3

    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    :pswitch_8
    const/16 v2, 0x36

    mul-int v3, v2, v2

    const/16 v4, 0x1b

    mul-int v5, v4, v4

    add-int/2addr v3, v5

    const/16 v5, 0x18

    mul-int v6, v5, v5

    add-int/2addr v3, v6

    const/16 v6, 0x36

    mul-int/lit8 v6, v6, 0x1b

    mul-int/lit8 v4, v4, 0x18

    add-int/2addr v6, v4

    mul-int/lit8 v2, v2, 0x18

    add-int/2addr v6, v2

    if-ge v3, v6, :cond_2

    goto :goto_4

    :cond_5
    :pswitch_9
    const/16 v1, 0x19

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0x19

    mul-int v3, p2, p2

    mul-int/lit8 v3, v3, 0xd

    add-int/2addr v2, v3

    mul-int v3, p1, p1

    mul-int/lit8 v3, v3, 0x1d

    add-int/2addr v2, v3

    mul-int/lit8 v1, v1, 0xd

    mul-int/lit8 v1, v1, 0x1d

    mul-int/lit8 v1, v1, 0x3

    if-ge v2, v1, :cond_3

    :cond_6
    :goto_7
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x1d
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x27
        :pswitch_4
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x52
        :pswitch_5
        :pswitch_a
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x39
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/AdError;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget p1, p1, Lcom/bytedance/msdk/api/AdError;->code:I

    const/16 v0, 0x2713

    if-ne p1, v0, :cond_5

    const/16 p1, 0x271a

    const-string v0, "Ad load timeout!"

    invoke-static {p0, p2, p3, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    if-eqz p0, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, ""

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "primerit_req_type"

    invoke-virtual {p1, p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    invoke-static {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    const/16 p0, 0x50

    const/16 p1, 0x52

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p2, 0x37

    mul-int p3, p2, p2

    mul-int/lit8 p3, p3, 0x37

    const/16 v0, 0xb

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0xb

    add-int/2addr p3, v1

    add-int/2addr p3, v1

    mul-int/lit8 p2, p2, 0xb

    mul-int/lit8 p2, p2, 0xb

    mul-int/lit8 p2, p2, 0x3

    if-ge p3, p2, :cond_3

    :pswitch_1
    const/16 p2, 0xd

    mul-int p3, p2, p2

    mul-int/lit8 p3, p3, 0xd

    const/16 v0, 0x9

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x9

    add-int/2addr p3, v1

    const/16 v1, 0x8

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0x8

    add-int/2addr p3, v2

    mul-int/lit8 p2, p2, 0x9

    mul-int/lit8 p2, p2, 0x8

    mul-int/lit8 p2, p2, 0x3

    if-ge p3, p2, :cond_2

    goto/16 :goto_3

    :cond_2
    :goto_1
    packed-switch p1, :pswitch_data_1

    const/16 p1, 0x47

    goto :goto_1

    :cond_3
    :pswitch_2
    const/16 p1, 0x40

    mul-int p2, p1, p1

    const/16 p3, 0x26

    mul-int v0, p3, p3

    add-int/2addr p2, v0

    const/4 v0, 0x4

    mul-int v1, v0, v0

    add-int/2addr p2, v1

    const/16 v1, 0x40

    mul-int/lit8 v1, v1, 0x26

    mul-int/lit8 p3, p3, 0x4

    add-int/2addr v1, p3

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr v1, p1

    if-ge p2, v1, :cond_4

    :pswitch_3
    const/16 p1, 0x54

    mul-int p2, p1, p1

    const/4 p3, 0x7

    mul-int v0, p3, p3

    add-int/2addr p2, v0

    mul-int/lit8 p1, p1, 0x7

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p2, p1

    if-gez p2, :cond_4

    const/16 p1, 0x11

    add-int/lit8 p1, p1, 0x1

    mul-int/lit8 p1, p1, 0x11

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_4

    :pswitch_4
    const/16 p0, 0x2d

    add-int/lit8 p0, p0, 0x1

    mul-int/lit8 p0, p0, 0x2d

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_1

    goto :goto_3

    :goto_2
    :pswitch_5
    if-eq p1, p0, :cond_7

    :cond_4
    const/16 p1, 0x50

    goto :goto_2

    :cond_5
    const/16 p1, 0x2766

    const-string v0, "Ad load fail all loadsorts! "

    invoke-static {p0, p2, p3, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    if-eqz p0, :cond_6

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, ""

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "primerit_req_type"

    invoke-virtual {p1, p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_6
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    invoke-static {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_7
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_5
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x46
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/base/TTBaseAd;I)V
    .locals 2

    invoke-static {p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p2

    const-string v0, "media_cache_success"

    invoke-virtual {p2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->adnHasAdVideoCachedApi()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "is_mock_video_cache_api"

    invoke-virtual {v0, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p0, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "primerit_req_type"

    invoke-virtual {p2, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const-string p1, "mediationrit_req_type"

    invoke-virtual {p2, p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, p2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :goto_0
    :pswitch_0
    const/16 p0, 0x12

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    const/16 p0, 0xc

    mul-int p1, p0, p0

    const/16 p2, 0xe

    mul-int v0, p2, p2

    add-int/2addr p1, v0

    mul-int/lit8 p0, p0, 0xe

    mul-int/lit8 p0, p0, 0x2

    sub-int/2addr p1, p0

    if-gez p1, :cond_1

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_2
    const/16 p0, 0x28

    const/16 p1, 0x55

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p0, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    const/16 p0, 0x47

    mul-int p0, p0, p0

    const/16 p1, 0x19

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr p0, p1

    const/4 p1, -0x1

    goto :goto_1

    :goto_2
    :pswitch_5
    const/16 p0, 0x5e

    mul-int p1, p0, p0

    mul-int/lit8 p1, p1, 0x5e

    const/16 p2, 0x11

    mul-int v0, p2, p2

    mul-int/lit8 v0, v0, 0x11

    add-int/2addr p1, v0

    const/4 v0, 0x7

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x7

    add-int/2addr p1, v1

    mul-int/lit8 p0, p0, 0x11

    mul-int/lit8 p0, p0, 0x7

    mul-int/lit8 p0, p0, 0x3

    if-ge p1, p0, :cond_1

    :pswitch_6
    const/16 p0, 0x25

    add-int/lit8 p0, p0, 0x1

    mul-int/lit8 p0, p0, 0x25

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_1

    :pswitch_7
    return-void

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x55
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x28
        :pswitch_7
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/base/TTBaseAd;III)V
    .locals 4

    invoke-static {p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    const-string v0, "mediation_request_end"

    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "primerit_req_type"

    invoke-virtual {p1, v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    const/4 p0, 0x0

    const/4 v0, 0x1

    if-ne p4, v0, :cond_1

    const/4 p4, 0x0

    goto :goto_0

    :cond_1
    const/4 p4, 0x1

    :goto_0
    iput p4, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->n:I

    new-instance p4, Ljava/util/HashMap;

    invoke-direct {p4}, Ljava/util/HashMap;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string v1, "requested_adn_count"

    invoke-interface {p4, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string p3, "requested_level_count"

    invoke-interface {p4, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, p1, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_2
    :goto_1
    :pswitch_0
    const/16 p1, 0x53

    const/16 p2, 0x51

    const/16 p3, 0x11

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    :pswitch_1
    const/16 p1, 0x1b

    mul-int p2, p1, p1

    add-int/lit16 p2, p2, 0x484

    mul-int p4, p3, p3

    add-int/2addr p2, p4

    const/16 p4, 0x1b

    mul-int/lit8 p4, p4, 0x22

    const/16 v1, 0x22

    mul-int/lit8 v1, v1, 0x11

    add-int/2addr p4, v1

    mul-int/lit8 p1, p1, 0x11

    add-int/2addr p4, p1

    if-ge p2, p4, :cond_3

    goto :goto_1

    :goto_2
    :pswitch_2
    const/16 p4, 0xa

    add-int/2addr p4, v0

    mul-int/lit8 p4, p4, 0xa

    rem-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_4

    :cond_3
    :pswitch_3
    return-void

    :cond_4
    :goto_3
    packed-switch p2, :pswitch_data_1

    goto :goto_4

    :pswitch_4
    rsub-int/lit8 p4, v0, 0x0

    mul-int/lit8 p4, p4, 0x0

    mul-int/lit8 v1, p0, 0x2

    sub-int/2addr v1, v0

    mul-int p4, p4, v1

    rem-int/lit8 p4, p4, 0x6

    if-eqz p4, :cond_2

    :pswitch_5
    packed-switch p1, :pswitch_data_2

    goto :goto_1

    :pswitch_6
    const/16 p4, 0xf

    add-int/2addr p4, v0

    mul-int/lit8 p4, p4, 0xf

    rem-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_2

    :pswitch_7
    const/16 p4, 0x44

    mul-int v1, p4, p4

    mul-int/lit8 v1, v1, 0x44

    const/16 v2, 0xe

    mul-int v3, v2, v2

    mul-int/lit8 v3, v3, 0xe

    add-int/2addr v1, v3

    mul-int v3, p3, p3

    mul-int/lit8 v3, v3, 0x11

    add-int/2addr v1, v3

    mul-int/lit8 p4, p4, 0xe

    mul-int/lit8 p4, p4, 0x11

    mul-int/lit8 p4, p4, 0x3

    if-ge v1, p4, :cond_2

    :pswitch_8
    const/16 p4, 0xd

    mul-int p4, p4, p4

    const/16 v1, 0x13

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr p4, v1

    const/4 v1, -0x1

    if-ne p4, v1, :cond_2

    goto :goto_2

    :goto_4
    const/16 p2, 0x28

    const/16 p1, 0x39

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x27
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x39
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)V
    .locals 6

    if-nez p0, :cond_0

    goto/16 :goto_3

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const-string v1, "cache_cannot_use"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "primerit_req_type"

    invoke-virtual {v1, v3, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    const-string v2, "cache_invalid_info"

    invoke-virtual {v1, v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const-string v1, "mediationrit_req_type"

    invoke-virtual {p1, v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    const/16 p0, 0x5a

    const/16 p1, 0x59

    const/16 v0, 0x1f

    const/4 v1, -0x1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :goto_1
    :pswitch_0
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    :cond_2
    :pswitch_1
    const/16 p1, 0x3d

    mul-int p1, p1, p1

    const/16 v2, 0x15

    mul-int v2, v2, v2

    mul-int/lit8 v2, v2, 0x22

    sub-int/2addr p1, v2

    if-ne p1, v1, :cond_1

    :pswitch_2
    const/16 p1, 0x16

    const/16 v2, 0x8

    if-le v0, p1, :cond_3

    goto :goto_2

    :cond_3
    packed-switch p0, :pswitch_data_2

    goto :goto_2

    :pswitch_3
    const/16 p0, 0xb

    mul-int/lit8 p0, p0, 0xb

    mul-int v2, v2, v2

    mul-int/lit8 v2, v2, 0x22

    sub-int/2addr p0, v2

    if-ne p0, v1, :cond_1

    :pswitch_4
    const/16 p0, 0x2a

    mul-int p1, p0, p0

    mul-int/lit8 p1, p1, 0x2a

    const/16 v0, 0x2d

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x2d

    add-int/2addr p1, v1

    const/16 v1, 0x18

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0x18

    add-int/2addr p1, v2

    mul-int/lit8 p0, p0, 0x2d

    mul-int/lit8 p0, p0, 0x18

    mul-int/lit8 p0, p0, 0x3

    if-ge p1, p0, :cond_1

    :pswitch_5
    const/4 p0, 0x4

    mul-int p1, p0, p0

    const/16 v0, 0x20

    mul-int v1, v0, v0

    add-int/2addr p1, v1

    const/16 v1, 0x19

    mul-int v2, v1, v1

    add-int/2addr p1, v2

    const/4 v2, 0x4

    mul-int/lit8 v2, v2, 0x20

    mul-int/lit8 v0, v0, 0x19

    add-int/2addr v2, v0

    mul-int/lit8 p0, p0, 0x19

    add-int/2addr v2, p0

    if-ge p1, v2, :cond_4

    goto :goto_0

    :goto_2
    const/16 p1, 0xd

    mul-int v3, p1, p1

    mul-int/lit8 v3, v3, 0xd

    const/16 v4, 0x9

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x9

    add-int/2addr v3, v5

    mul-int v5, v2, v2

    mul-int/lit8 v5, v5, 0x8

    add-int/2addr v3, v5

    mul-int/lit8 p1, p1, 0x9

    mul-int/lit8 p1, p1, 0x8

    mul-int/lit8 p1, p1, 0x3

    if-ge v3, p1, :cond_2

    :pswitch_6
    const/4 p0, 0x1

    const/16 p1, 0x35

    rsub-int/lit8 p0, p0, 0x35

    mul-int/lit8 p0, p0, 0x35

    mul-int/lit8 p1, p1, 0x2

    add-int/lit8 p1, p1, -0x1

    mul-int p0, p0, p1

    rem-int/lit8 p0, p0, 0x6

    if-eqz p0, :cond_1

    :pswitch_7
    const/4 p0, 0x5

    mul-int/lit8 p0, p0, 0x5

    const/16 p1, 0x1c

    mul-int/lit8 p1, p1, 0x1c

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr p0, p1

    if-ne p0, v1, :cond_4

    :cond_4
    :goto_3
    :pswitch_8
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x57
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1d
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x40
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const-string v0, "mediation_request"

    const-string v5, ""

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Ljava/lang/String;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    if-eqz p0, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "primerit_req_type"

    invoke-virtual {p1, p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p2, 0x0

    invoke-static {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    const/16 p0, 0x5f

    const/4 p1, -0x1

    const/16 p2, 0x3c

    const/4 v0, 0x1

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :cond_2
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 p0, 0x45

    mul-int v1, p0, p0

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/4 v3, 0x4

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    const/16 v4, 0x45

    mul-int/lit8 v4, v4, 0x14

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr v4, v2

    mul-int/lit8 p0, p0, 0x4

    add-int/2addr v4, p0

    if-ge v1, v4, :cond_2

    :pswitch_2
    const/16 p0, 0x12

    mul-int p0, p0, p0

    const/16 p2, 0x23

    mul-int p2, p2, p2

    mul-int/lit8 p2, p2, 0x22

    sub-int/2addr p0, p2

    if-ne p0, p1, :cond_3

    goto :goto_2

    :goto_1
    :pswitch_3
    packed-switch p2, :pswitch_data_2

    goto :goto_3

    :goto_2
    :pswitch_4
    const/16 p0, 0x9

    add-int/2addr p0, v0

    mul-int/lit8 p0, p0, 0x9

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_1

    :pswitch_5
    const/16 p0, 0x52

    mul-int p0, p0, p0

    const/16 p2, 0x28

    mul-int p2, p2, p2

    mul-int/lit8 p2, p2, 0x22

    sub-int/2addr p0, p2

    if-ne p0, p1, :cond_3

    :pswitch_6
    const/4 p0, 0x0

    rsub-int/lit8 p1, v0, 0x0

    mul-int/lit8 p1, p1, 0x0

    mul-int/lit8 p0, p0, 0x2

    sub-int/2addr p0, v0

    mul-int p1, p1, p0

    rem-int/lit8 p1, p1, 0x6

    goto :goto_4

    :goto_3
    :pswitch_7
    const/16 p0, 0x27

    add-int/2addr p0, v0

    mul-int/lit8 p0, p0, 0x27

    rem-int/lit8 p0, p0, 0x2

    :cond_3
    :goto_4
    :pswitch_8
    return-void

    :pswitch_data_0
    .packed-switch 0x5f
        :pswitch_0
        :pswitch_3
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3c
        :pswitch_8
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x31
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/msdk/api/AdSlot;",
            "Ljava/util/List<",
            "*>;",
            "Ljava/util/List<",
            "*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p2

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_1
    const-string p2, ""

    :goto_1
    const-string v0, "done sort"

    invoke-static {p0, p1, p2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    const-string p2, "request_load_sort_list"

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p2

    const-string v0, "server_bidding_extra"

    invoke-virtual {p2, v0, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p0, :cond_2

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, ""

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "primerit_req_type"

    invoke-virtual {p1, p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p2, 0x0

    invoke-static {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    const/16 p0, 0xe

    :goto_2
    packed-switch p0, :pswitch_data_0

    goto :goto_3

    :pswitch_0
    return-void

    :goto_3
    :pswitch_1
    const/16 p0, 0xd

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/msdk/api/AdSlot;",
            "Ljava/util/List<",
            "*>;",
            "Ljava/util/List<",
            "*>;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p3

    if-eqz p3, :cond_2

    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_2
    const-string p1, ""

    :goto_2
    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p3

    if-eqz p3, :cond_3

    invoke-interface {p2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p2

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_3

    :cond_3
    const-string p2, ""

    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v0, "loadSort:size"

    invoke-static {p0, p1, p2, p3, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    const-string p2, "request_used_load_sort_list"

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p2

    const-string p3, "server_bidding_extra"

    invoke-virtual {p2, p3, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p0, :cond_4

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, ""

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "primerit_req_type"

    invoke-virtual {p1, p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_4
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p2, 0x0

    invoke-static {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :goto_4
    const/16 p0, 0x47

    const/4 p1, 0x1

    packed-switch p0, :pswitch_data_0

    goto :goto_4

    :pswitch_0
    const/16 p0, 0x28

    rsub-int/lit8 p2, p1, 0x28

    mul-int/lit8 p2, p2, 0x28

    mul-int/lit8 p0, p0, 0x2

    sub-int/2addr p0, p1

    mul-int p2, p2, p0

    rem-int/lit8 p2, p2, 0x6

    if-eqz p2, :cond_5

    :pswitch_1
    const/16 p0, 0x15

    rsub-int/lit8 p2, p1, 0x15

    mul-int/lit8 p2, p2, 0x15

    mul-int/lit8 p0, p0, 0x2

    sub-int/2addr p0, p1

    mul-int p2, p2, p0

    rem-int/lit8 p2, p2, 0x6

    :cond_5
    :goto_5
    :pswitch_2
    const/16 p0, 0x34

    packed-switch p0, :pswitch_data_1

    goto :goto_5

    :cond_6
    :pswitch_3
    const/16 p0, 0x12

    mul-int p1, p0, p0

    const/16 p2, 0xf

    mul-int p3, p2, p2

    add-int/2addr p1, p3

    const/16 p3, 0x1c

    mul-int p4, p3, p3

    add-int/2addr p1, p4

    const/16 p4, 0x12

    mul-int/lit8 p4, p4, 0xf

    mul-int/lit8 p2, p2, 0x1c

    add-int/2addr p4, p2

    mul-int/lit8 p0, p0, 0x1c

    add-int/2addr p4, p0

    if-ge p1, p4, :cond_6

    :pswitch_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x47
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x33
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/bytedance/msdk/base/TTBaseAd;ILjava/lang/String;Ljava/lang/String;JLcom/bytedance/msdk/api/AdSlot;I)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;ILjava/lang/String;Ljava/lang/String;J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const/4 p1, -0x1

    const/4 p2, 0x1

    const/16 p3, 0x22

    if-eqz p0, :cond_1

    const-string p4, "media_fill"

    invoke-virtual {p0, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p4

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    const-string p7, "adn_count"

    invoke-virtual {p4, p7, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p6, :cond_0

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, ""

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p5

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    const-string p5, "primerit_req_type"

    invoke-virtual {p0, p5, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    const-string p5, "mediationrit_req_type"

    invoke-virtual {p0, p5, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p6}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide p4

    invoke-virtual {p0, p4, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p6}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p4

    new-instance p5, Ljava/util/HashMap;

    invoke-direct {p5}, Ljava/util/HashMap;-><init>()V

    invoke-static {p4, p0, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    goto :goto_1

    :cond_1
    :goto_0
    :pswitch_0
    const/16 p0, 0x5f

    packed-switch p0, :pswitch_data_0

    goto :goto_2

    :pswitch_1
    const/16 p0, 0x3d

    mul-int p0, p0, p0

    const/16 p4, 0x15

    mul-int p4, p4, p4

    mul-int/lit8 p4, p4, 0x22

    sub-int/2addr p0, p4

    if-ne p0, p1, :cond_2

    goto :goto_0

    :cond_2
    :goto_1
    const/16 p0, 0x46

    packed-switch p0, :pswitch_data_1

    goto :goto_1

    :goto_2
    const/16 p0, 0x26

    rsub-int/lit8 p4, p2, 0x26

    mul-int/lit8 p4, p4, 0x26

    mul-int/lit8 p0, p0, 0x2

    sub-int/2addr p0, p2

    mul-int p4, p4, p0

    rem-int/lit8 p4, p4, 0x6

    if-eqz p4, :cond_1

    :pswitch_2
    const/16 p0, 0x1b

    mul-int p4, p0, p0

    mul-int p5, p3, p3

    add-int/2addr p4, p5

    const/16 p5, 0x11

    mul-int p6, p5, p5

    add-int/2addr p4, p6

    const/16 p6, 0x1b

    mul-int/lit8 p6, p6, 0x22

    const/16 p7, 0x22

    mul-int/lit8 p7, p7, 0x11

    add-int/2addr p6, p7

    mul-int/lit8 p0, p0, 0x11

    add-int/2addr p6, p0

    if-ge p4, p6, :cond_3

    goto :goto_3

    :cond_3
    :pswitch_3
    const/16 p0, 0xa

    add-int/2addr p0, p2

    mul-int/lit8 p0, p0, 0xa

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_4

    goto :goto_0

    :cond_4
    const/4 p0, 0x5

    mul-int p0, p0, p0

    const/16 p4, 0x1c

    mul-int p4, p4, p4

    mul-int/lit8 p4, p4, 0x22

    sub-int/2addr p0, p4

    if-ne p0, p1, :cond_5

    goto :goto_0

    :cond_5
    :goto_3
    :pswitch_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x46
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static b()V
    .locals 6

    :goto_0
    const/16 v0, 0x25

    const/16 v1, 0x36

    const/4 v2, 0x1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x12

    mul-int v0, v0, v0

    const/16 v1, 0x23

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    :pswitch_2
    const/16 v0, 0x1a

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x1a

    const/16 v3, 0x9

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0x9

    add-int/2addr v1, v4

    const/16 v4, 0xf

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0xf

    add-int/2addr v1, v5

    mul-int/lit8 v0, v0, 0x9

    mul-int/lit8 v0, v0, 0xf

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_0

    :pswitch_3
    const/16 v0, 0x28

    rsub-int/lit8 v1, v2, 0x28

    mul-int/lit8 v1, v1, 0x28

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v2

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    if-eqz v1, :cond_0

    goto :goto_2

    :goto_1
    :pswitch_4
    const/16 v0, 0x1e

    rsub-int/lit8 v1, v2, 0x1e

    mul-int/lit8 v1, v1, 0x1e

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v2

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    :goto_2
    :pswitch_5
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const-string v1, "sdk_init"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 2

    const-string v0, ""

    invoke-static {p0, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const-string v0, "adapter_request"

    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "primerit_req_type"

    invoke-virtual {p0, v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v0, "mediationrit_req_type"

    invoke-virtual {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    const/16 p0, 0x37

    const/4 p1, 0x0

    :cond_1
    :goto_0
    :pswitch_0
    const/16 v0, 0x48

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    :goto_2
    :pswitch_2
    packed-switch p0, :pswitch_data_2

    const/16 p0, 0x1e

    goto :goto_2

    :pswitch_3
    const/16 v0, 0x39

    if-gt p1, v0, :cond_1

    :pswitch_4
    return-void

    :pswitch_5
    const/16 v0, 0x49

    const/16 p1, 0x10

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_5
        :pswitch_3
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x34
        :pswitch_0
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1d
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)V
    .locals 4

    if-eqz p0, :cond_3

    if-nez p1, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-static {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const-string p2, "get_bidding_adm_to_adn"

    invoke-virtual {p0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "primerit_req_type"

    invoke-virtual {p2, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    const/16 p0, 0x36

    const/16 p1, 0x4a

    const/4 p2, -0x1

    const/16 v0, 0x22

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch p0, :pswitch_data_1

    const/16 p0, 0x5e

    add-int/lit8 p0, p0, 0x1

    mul-int/lit8 p0, p0, 0x5e

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_1

    goto :goto_1

    :cond_2
    :pswitch_1
    const/16 p0, 0x39

    mul-int p1, p0, p0

    mul-int v1, v0, v0

    add-int/2addr p1, v1

    const/16 v1, 0xc

    mul-int v2, v1, v1

    add-int/2addr p1, v2

    const/16 v2, 0x39

    mul-int/lit8 v2, v2, 0x22

    const/16 v3, 0x22

    mul-int/lit8 v3, v3, 0xc

    add-int/2addr v2, v3

    mul-int/lit8 p0, p0, 0xc

    add-int/2addr v2, p0

    if-ge p1, v2, :cond_2

    :goto_1
    :pswitch_2
    const/16 p0, 0x12

    mul-int p0, p0, p0

    const/16 p1, 0x23

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr p0, p1

    if-ne p0, p2, :cond_3

    :pswitch_3
    const/16 p0, 0x9

    add-int/lit8 p0, p0, 0x1

    mul-int/lit8 p0, p0, 0x9

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_3

    :pswitch_4
    const/16 p0, 0x52

    mul-int p0, p0, p0

    const/16 p1, 0x28

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr p0, p1

    if-ne p0, p2, :cond_3

    :cond_3
    :goto_2
    :pswitch_5
    return-void

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x34
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static b(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)V
    .locals 4

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const-string v1, "start_bidding_request"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->i(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->k(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getWaterfallId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    const-string v2, "waterfall_abtest"

    invoke-virtual {v1, v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->g(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v1, "primerit_req_type"

    invoke-virtual {p1, v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    return-void
.end method

.method public static b(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 3

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const-string v0, "bidding_adm_cache"

    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "primerit_req_type"

    invoke-virtual {v0, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static c(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 3

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const-string v0, "bidding_adm_load"

    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "primerit_req_type"

    invoke-virtual {v0, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static d(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 3

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    const-string v0, "bidding_win_event"

    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "primerit_req_type"

    invoke-virtual {v0, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static e(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 4

    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p0

    if-eqz p0, :cond_3

    const-string v0, "media_click_listen"

    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "primerit_req_type"

    invoke-virtual {p0, v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v0, "mediationrit_req_type"

    invoke-virtual {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    :goto_0
    :pswitch_0
    const/16 p0, 0x46

    const/4 p1, 0x1

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_1
    const/16 p0, 0x5f

    packed-switch p0, :pswitch_data_1

    const/16 p0, 0x26

    rsub-int/lit8 v0, p1, 0x26

    mul-int/lit8 v0, v0, 0x26

    mul-int/lit8 p0, p0, 0x2

    sub-int/2addr p0, p1

    mul-int v0, v0, p0

    rem-int/lit8 v0, v0, 0x6

    if-eqz v0, :cond_1

    :pswitch_2
    const/16 p0, 0x1b

    mul-int v0, p0, p0

    const/16 v1, 0x22

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x11

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1b

    mul-int/lit8 v3, v3, 0x22

    mul-int/lit8 v1, v1, 0x11

    add-int/2addr v3, v1

    mul-int/lit8 p0, p0, 0x11

    add-int/2addr v3, p0

    if-ge v0, v3, :cond_2

    goto :goto_2

    :cond_2
    :pswitch_3
    const/16 p0, 0xa

    add-int/2addr p0, p1

    mul-int/lit8 p0, p0, 0xa

    rem-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_3

    goto :goto_1

    :cond_3
    :goto_2
    :pswitch_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x46
        :pswitch_1
        :pswitch_1
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_2
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public static f(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 6

    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const/16 v1, 0x4e

    const/4 v2, 0x5

    const/4 v3, 0x3

    if-eqz v0, :cond_3

    const-string v4, "media_show"

    invoke-virtual {v0, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p1, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "primerit_req_type"

    invoke-virtual {v0, v5, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v4, "mediationrit_req_type"

    invoke-virtual {v0, v4, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdType()I

    move-result p1

    const/16 v4, 0x8

    if-eq p1, v4, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdType()I

    move-result p1

    const/4 v4, 0x7

    if-ne p1, v4, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->isCacheSuccess()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v4, "is_video_cache_success"

    invoke-virtual {v0, v4, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object p1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->adnHasAdVideoCachedApi()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const-string v4, "is_mock_video_cache_api"

    invoke-virtual {p1, v4, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    goto :goto_4

    :cond_3
    const/16 p0, 0x11

    const/16 p1, 0x5e

    :goto_0
    move v0, p0

    const/4 p0, 0x3

    :goto_1
    if-eq p0, v3, :cond_6

    const/4 v4, 0x4

    if-eq p0, v4, :cond_5

    if-eq p0, v2, :cond_4

    move p0, v0

    goto :goto_0

    :cond_4
    if-eq p1, v1, :cond_6

    packed-switch v0, :pswitch_data_0

    goto :goto_3

    :cond_5
    :goto_2
    packed-switch p1, :pswitch_data_1

    goto :goto_4

    :goto_3
    :pswitch_0
    const/4 p0, 0x5

    const/16 p1, 0x4e

    goto :goto_1

    :goto_4
    :pswitch_1
    const/16 p1, 0x48

    const/16 v0, -0x31

    goto :goto_2

    :cond_6
    return-void

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x47
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static g(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 4

    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v0

    const-string v1, "media_show_listen"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    if-eqz p1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "primerit_req_type"

    invoke-virtual {v0, v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "mediationrit_req_type"

    invoke-virtual {v0, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_0
    const/16 p1, 0x8

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdType()I

    move-result v1

    if-eq v1, p1, :cond_1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdType()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->isCacheSuccess()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "is_video_cache_success"

    invoke-virtual {v0, v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/bytedance/msdk/base/TTBaseAd;->adnHasAdVideoCachedApi()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const-string v2, "is_mock_video_cache_api"

    invoke-virtual {v1, v2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;->a(Ljava/lang/String;Ljava/lang/Object;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;

    :cond_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/c;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/d;Ljava/util/Map;)V

    const/16 p0, 0x3d

    const/16 v0, 0xc

    const/16 v0, 0x3d

    const/16 v1, 0xc

    :goto_0
    :pswitch_0
    const/16 v2, 0x9

    :goto_1
    const/16 v3, 0x1b

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :goto_2
    :pswitch_1
    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x27

    goto :goto_2

    :pswitch_2
    if-ge v0, v3, :cond_3

    :goto_3
    :pswitch_3
    packed-switch v1, :pswitch_data_2

    goto :goto_4

    :pswitch_4
    const/16 v0, 0x25

    const/16 v2, 0x8

    goto :goto_1

    :goto_4
    const/16 v1, 0x1b

    goto :goto_3

    :cond_3
    if-le v1, p0, :cond_4

    :goto_5
    nop

    goto :goto_5

    :cond_4
    :pswitch_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x25
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x19
        :pswitch_4
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method
