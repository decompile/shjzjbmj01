.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;
.super Ljava/lang/Object;
.source "TTNetClient.java"


# static fields
.field private static volatile d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

.field private c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    .line 2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->a:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    if-nez v0, :cond_1

    .line 2
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    monitor-enter v0

    .line 3
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    if-nez v1, :cond_0

    .line 4
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    invoke-direct {v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;-><init>(Landroid/content/Context;)V

    sput-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    .line 6
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 8
    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    return-object p0
.end method

.method private c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    move-result-object v0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    :cond_0
    return-void
.end method

.method public static d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;
    .locals 1

    .line 1
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;
    .locals 1

    .line 9
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    if-nez v0, :cond_0

    .line 10
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    move-result-object v0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    .line 12
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    return-object v0
.end method

.method public b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;
    .locals 1

    .line 1
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->c()V

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    return-object v0
.end method
