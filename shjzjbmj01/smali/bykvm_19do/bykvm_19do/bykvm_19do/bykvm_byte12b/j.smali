.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;
.super Ljava/lang/Object;
.source "IdUtils.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "MissingPermission"
    }
.end annotation


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static e:Ljava/lang/String;

.field private static f:Ljava/lang/String;

.field private static g:Ljava/lang/String;

.field private static h:Ljava/lang/String;

.field private static volatile i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Ljava/lang/String;
    .locals 3

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/p;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MIUI-"

    .line 154
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 155
    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/p;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "FLYME-"

    .line 156
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 158
    :cond_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/p;->f()Ljava/lang/String;

    move-result-object v1

    .line 159
    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/p;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "EMUI-"

    .line 160
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 163
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_3
    :goto_0
    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :catch_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v0, :cond_1

    .line 3
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-boolean v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v1, :cond_0

    .line 5
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->j(Landroid/content/Context;)V

    .line 7
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 10
    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->b:Ljava/lang/String;

    return-object p0
.end method

.method private static declared-synchronized a(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "TrulyRandom",
            "HardwareIds"
        }
    .end annotation

    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;

    monitor-enter v0

    .line 22
    :try_start_0
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->c(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-nez p0, :cond_0

    monitor-exit v0

    return-object v1

    .line 29
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-object v2, v1

    :goto_0
    const/16 v3, 0xd

    if-eqz v2, :cond_1

    :try_start_2
    const-string v4, "9774d56d682e549c"

    .line 35
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 36
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v3, :cond_7

    :cond_1
    const-string v4, "tt_device_info"

    .line 38
    invoke-static {v4, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object p0

    const-string v4, "openudid"

    .line 39
    invoke-virtual {p0, v4, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 41
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    .line 42
    new-instance v4, Ljava/math/BigInteger;

    const/16 v5, 0x40

    invoke-direct {v4, v5, v1}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/16 v1, 0x10

    invoke-virtual {v4, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    .line 43
    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2d

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    .line 44
    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 45
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    if-lez v3, :cond_4

    .line 47
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    if-lez v3, :cond_3

    const/16 v5, 0x46

    .line 49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 52
    :cond_3
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_4
    if-eqz p1, :cond_5

    const-string p1, "openudid.dat"

    .line 56
    invoke-static {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 57
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    goto :goto_2

    :cond_5
    move-object p1, v1

    :goto_2
    const-string v1, "openudid"

    .line 61
    invoke-virtual {p0, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_6
    move-object p1, v1

    goto :goto_3

    :catch_1
    :cond_7
    move-object p1, v2

    :goto_3
    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p0

    .line 21
    monitor-exit v0

    throw p0
.end method

.method public static a(Landroid/net/wifi/WifiManager;)Ljava/lang/String;
    .locals 6

    const-string v0, "02:00:00:00:00:00"

    if-nez p0, :cond_0

    return-object v0

    .line 15
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v1

    .line 16
    invoke-virtual {p0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object p0

    if-eqz v1, :cond_2

    if-eqz p0, :cond_2

    .line 17
    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    .line 18
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 19
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/ScanResult;

    .line 20
    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 21
    iget-object v3, v3, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v3

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    :cond_2
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 62
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    .line 63
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    .line 65
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/Android/data/com.snssdk.api/cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    .line 72
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    if-nez v0, :cond_1

    .line 74
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    return-object p1

    :catchall_0
    move-exception p0

    goto :goto_0

    .line 77
    :cond_1
    :try_start_2
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    new-instance p0, Ljava/io/RandomAccessFile;

    const-string v2, "rwd"

    invoke-direct {p0, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 79
    :try_start_3
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 80
    :try_start_4
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x81

    .line 82
    new-array v1, v0, [B

    const/4 v3, 0x0

    .line 83
    invoke-virtual {p0, v1, v3, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v4

    if-lez v4, :cond_3

    if-ge v4, v0, :cond_3

    .line 85
    new-instance v0, Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-direct {v0, v1, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 86
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_3

    if-eqz v2, :cond_2

    .line 101
    :try_start_5
    invoke-virtual {v2}, Ljava/nio/channels/FileLock;->release()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 107
    :catch_0
    :cond_2
    :try_start_6
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    return-object v0

    :cond_3
    :try_start_7
    const-string v0, "UTF-8"

    .line 108
    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const-wide/16 v3, 0x0

    .line 109
    invoke-virtual {p0, v3, v4}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 110
    invoke-virtual {p0, v0}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v2, :cond_4

    .line 118
    :try_start_8
    invoke-virtual {v2}, Ljava/nio/channels/FileLock;->release()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 124
    :catch_2
    :cond_4
    :try_start_9
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    :catch_3
    return-object p1

    :catchall_1
    move-exception p1

    move-object v1, v2

    goto :goto_1

    :catch_4
    move-object v1, v2

    goto :goto_2

    :catchall_2
    move-exception p1

    goto :goto_1

    :catchall_3
    move-exception p0

    :goto_0
    move-object p1, p0

    move-object p0, v1

    :goto_1
    if-eqz v1, :cond_5

    .line 125
    :try_start_a
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    :catch_5
    :cond_5
    if-eqz p0, :cond_6

    .line 131
    :try_start_b
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    .line 135
    :catch_6
    :cond_6
    throw p1

    :catch_7
    move-object p0, v1

    :catch_8
    :goto_2
    if-eqz v1, :cond_7

    .line 136
    :try_start_c
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_9

    :catch_9
    :cond_7
    if-eqz p0, :cond_8

    .line 142
    :try_start_d
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a

    :catch_a
    :cond_8
    return-object p1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .line 11
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "tt_device_info"

    .line 12
    invoke-static {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object p0

    const-string v0, "did"

    .line 13
    invoke-virtual {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    sput-object p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 143
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xd

    if-lt v1, v2, :cond_7

    const/16 v2, 0x80

    if-le v1, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_6

    .line 147
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_2

    const/16 v4, 0x39

    if-le v3, v4, :cond_5

    :cond_2
    const/16 v4, 0x61

    if-lt v3, v4, :cond_3

    const/16 v4, 0x66

    if-le v3, v4, :cond_5

    :cond_3
    const/16 v4, 0x41

    if-lt v3, v4, :cond_4

    const/16 v4, 0x46

    if-le v3, v4, :cond_5

    :cond_4
    const/16 v4, 0x2d

    if-eq v3, v4, :cond_5

    return v0

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_6
    const/4 p0, 0x1

    return p0

    :cond_7
    :goto_1
    return v0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v0, :cond_1

    .line 3
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-boolean v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v1, :cond_0

    .line 5
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->j(Landroid/content/Context;)V

    .line 7
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 10
    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->g:Ljava/lang/String;

    return-object p0
.end method

.method private static c(Landroid/content/Context;)Landroid/content/Context;
    .locals 0

    if-nez p0, :cond_0

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v0, :cond_1

    .line 3
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-boolean v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v1, :cond_0

    .line 5
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->j(Landroid/content/Context;)V

    .line 7
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 10
    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a:Ljava/lang/String;

    return-object p0
.end method

.method public static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v0, :cond_1

    .line 3
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-boolean v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v1, :cond_0

    .line 5
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->j(Landroid/content/Context;)V

    .line 7
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 10
    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->c:Ljava/lang/String;

    return-object p0
.end method

.method public static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v0, :cond_1

    .line 3
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-boolean v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v1, :cond_0

    .line 5
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->j(Landroid/content/Context;)V

    .line 7
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 10
    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->f:Ljava/lang/String;

    return-object p0
.end method

.method public static g(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v0, :cond_1

    .line 3
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-boolean v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v1, :cond_0

    .line 5
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->j(Landroid/content/Context;)V

    .line 7
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 10
    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->d:Ljava/lang/String;

    return-object p0
.end method

.method public static h(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v0, :cond_1

    .line 3
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-boolean v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v1, :cond_0

    .line 5
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->j(Landroid/content/Context;)V

    .line 7
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 10
    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->h:Ljava/lang/String;

    return-object p0
.end method

.method public static i(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v0, :cond_1

    .line 3
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-boolean v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-nez v1, :cond_0

    .line 5
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->j(Landroid/content/Context;)V

    .line 7
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 10
    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->e:Ljava/lang/String;

    return-object p0
.end method

.method private static j(Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HardwareIds",
            "MissingPermission"
        }
    .end annotation

    .annotation build Landroidx/annotation/RequiresPermission;
        anyOf = {
            "android.permission.ACCESS_WIFI_STATE"
        }
    .end annotation

    .line 1
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->c(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p0

    if-nez p0, :cond_1

    return-void

    .line 10
    :cond_1
    :try_start_0
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->k(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    :try_start_1
    const-string v1, "wifi"

    .line 17
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    if-eqz v1, :cond_2

    .line 18
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    goto :goto_0

    :cond_2
    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 20
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->d:Ljava/lang/String;

    .line 22
    :cond_3
    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a(Landroid/net/wifi/WifiManager;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->e:Ljava/lang/String;

    .line 23
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1c

    if-lt v1, v2, :cond_4

    .line 24
    invoke-static {}, Landroid/os/Build;->getSerial()Ljava/lang/String;

    goto :goto_1

    .line 26
    :cond_4
    sget-object v1, Landroid/os/Build;->SERIAL:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :goto_1
    const/4 v1, 0x1

    .line 31
    invoke-static {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->b:Ljava/lang/String;

    const-string v2, "tt_device_info"

    .line 32
    invoke-static {v2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object v2

    const-string v3, "did"

    invoke-virtual {v2, v3, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a:Ljava/lang/String;

    .line 33
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a()Ljava/lang/String;

    .line 35
    sget-wide v2, Landroid/os/Build;->TIME:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->g:Ljava/lang/String;

    const-string v2, "tt_device_info"

    .line 36
    invoke-static {v2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object p0

    const-string v2, "uuid"

    invoke-virtual {p0, v2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->h:Ljava/lang/String;

    .line 37
    sput-boolean v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->i:Z

    return-void
.end method

.method private static k(Landroid/content/Context;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HardwareIds",
            "MissingPermission"
        }
    .end annotation

    const-string v0, "phone"

    .line 1
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/telephony/TelephonyManager;

    if-eqz p0, :cond_0

    .line 3
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->c:Ljava/lang/String;

    .line 4
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->f:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public static l(Landroid/content/Context;)V
    .locals 1

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 5
    :cond_0
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->c(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p0

    if-nez p0, :cond_1

    return-void

    .line 10
    :cond_1
    :try_start_0
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->k(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method
