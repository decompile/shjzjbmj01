.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;
.super Ljava/lang/Object;
.source "AdLocationUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e$d;
    }
.end annotation


# static fields
.field private static a:J = 0x1b7740L

.field private static b:Landroid/os/Handler;

.field private static c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b:Landroid/os/Handler;

    .line 2
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->c:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private static a(Landroid/location/LocationManager;)Landroid/location/Location;
    .locals 1

    const-string v0, "gps"

    .line 12
    invoke-static {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->a(Landroid/location/LocationManager;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "network"

    .line 14
    invoke-static {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->a(Landroid/location/LocationManager;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    const-string v0, "passive"

    .line 17
    invoke-static {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->a(Landroid/location/LocationManager;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private static a(Landroid/location/LocationManager;Ljava/lang/String;)Landroid/location/Location;
    .locals 2

    .line 18
    :try_start_0
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e$d;

    invoke-direct {v0, p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e$d;-><init>(Landroid/location/LocationManager;Ljava/lang/String;)V

    .line 19
    new-instance p0, Ljava/util/concurrent/FutureTask;

    invoke-direct {p0, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 20
    sget-object p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const-wide/16 v0, 0x1

    .line 21
    sget-object p1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0, v1, p1}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/location/Location;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string p1, "AdLocationUtils"

    .line 22
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "location:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object p0

    :catchall_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    const/4 v0, 0x0

    .line 5
    invoke-static {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object p0

    const-string v1, "latitude"

    const/high16 v2, -0x40800000    # -1.0f

    .line 6
    invoke-virtual {p0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;F)F

    move-result v1

    const-string v3, "longitude"

    .line 7
    invoke-virtual {p0, v3, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;F)F

    move-result p0

    cmpl-float v3, v1, v2

    if-eqz v3, :cond_1

    cmpl-float v2, p0, v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 11
    :cond_0
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;

    invoke-direct {v0, v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;-><init>(FF)V

    :cond_1
    :goto_0
    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;Landroid/location/Location;)V
    .locals 0

    .line 3
    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/content/Context;Landroid/location/Location;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Landroid/location/LocationManager;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/content/Context;Landroid/location/LocationManager;)V

    return-void
.end method

.method static synthetic a(Landroid/location/LocationManager;Landroid/location/LocationListener;)V
    .locals 0

    .line 4
    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/location/LocationManager;Landroid/location/LocationListener;)V

    return-void
.end method

.method static synthetic a(Landroid/location/Location;)Z
    .locals 0

    .line 2
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/location/Location;)Z

    move-result p0

    return p0
.end method

.method public static b(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    if-nez p0, :cond_0

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    .line 2
    :goto_0
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;

    move-result-object v0

    .line 4
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    return-object v0

    .line 7
    :cond_1
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->d(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;

    move-result-object p0

    return-object p0
.end method

.method private static b(Landroid/location/LocationManager;)Ljava/lang/String;
    .locals 1

    const-string v0, "gps"

    .line 8
    invoke-virtual {p0, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "gps"

    return-object p0

    :cond_0
    const-string v0, "network"

    .line 10
    invoke-virtual {p0, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p0, "network"

    return-object p0

    :cond_1
    const-string v0, "passive"

    .line 12
    invoke-virtual {p0, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    const-string p0, "passive"

    return-object p0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method private static b(Landroid/content/Context;Landroid/location/Location;)V
    .locals 2

    .line 58
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 61
    invoke-static {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object p0

    .line 62
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    double-to-float v0, v0

    const-string v1, "latitude"

    invoke-virtual {p0, v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;F)V

    .line 63
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    double-to-float p1, v0

    const-string v0, "longitude"

    invoke-virtual {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;F)V

    .line 64
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string p1, "lbstime"

    invoke-virtual {p0, p1, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;J)V

    return-void
.end method

.method private static b(Landroid/content/Context;Landroid/location/LocationManager;)V
    .locals 4

    if-eqz p0, :cond_3

    if-nez p1, :cond_0

    goto :goto_0

    .line 13
    :cond_0
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e$b;

    invoke-direct {v0, p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e$b;-><init>(Landroid/content/Context;Landroid/location/LocationManager;)V

    .line 36
    :try_start_0
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/location/LocationManager;)Ljava/lang/String;

    move-result-object p0

    .line 37
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    .line 41
    :cond_1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {p1, p0, v0, v1}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 43
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b:Landroid/os/Handler;

    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e$c;

    invoke-direct {v1, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e$c;-><init>(Landroid/location/LocationManager;Landroid/location/LocationListener;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {p0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 50
    invoke-static {}, Lcom/bytedance/msdk/adapter/util/Logger;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 51
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 53
    :cond_2
    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/location/LocationManager;Landroid/location/LocationListener;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private static b(Landroid/location/LocationManager;Landroid/location/LocationListener;)V
    .locals 0

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 56
    invoke-static {}, Lcom/bytedance/msdk/adapter/util/Logger;->isDebug()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 57
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method

.method private static b(Landroid/location/Location;)Z
    .locals 5

    .line 65
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double p0, v0, v2

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static c(Landroid/content/Context;)Z
    .locals 5

    const/4 v0, 0x0

    .line 1
    invoke-static {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object p0

    const-string v0, "lbstime"

    const-wide/16 v1, -0x1

    .line 2
    invoke-virtual {p0, v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;J)J

    move-result-wide v3

    cmp-long p0, v3, v1

    if-eqz p0, :cond_1

    .line 4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v3

    sget-wide v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->a:J

    cmp-long p0, v0, v2

    if-lez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static d(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;
    .locals 7

    const-string v0, "location"

    .line 1
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 5
    :try_start_0
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->a(Landroid/location/LocationManager;)Landroid/location/Location;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 6
    invoke-static {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/location/Location;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 7
    invoke-static {p0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/content/Context;Landroid/location/Location;)V

    .line 8
    new-instance v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    double-to-float v4, v4

    .line 9
    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    double-to-float v2, v5

    invoke-direct {v3, v4, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;-><init>(FF)V

    move-object v1, v3

    .line 12
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v2, v3, :cond_1

    .line 13
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e$a;

    invoke-direct {v3, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e$a;-><init>(Landroid/content/Context;Landroid/location/LocationManager;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 20
    :cond_1
    invoke-static {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/content/Context;Landroid/location/LocationManager;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 24
    invoke-static {}, Lcom/bytedance/msdk/adapter/util/Logger;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 25
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_2
    :goto_0
    return-object v1
.end method
