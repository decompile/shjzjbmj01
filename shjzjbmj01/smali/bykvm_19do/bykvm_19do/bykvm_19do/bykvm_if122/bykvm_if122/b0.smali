.class public abstract Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;
.super Ljava/lang/Object;
.source "ResponseBody.java"

# interfaces
.implements Ljava/io/Closeable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;JLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;
    .locals 1

    if-eqz p3, :cond_0

    .line 4
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0$a;

    invoke-direct {v0, p0, p1, p2, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;JLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;)V

    return-object v0

    .line 5
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "source == null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;[B)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;
    .locals 3

    .line 2
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/c;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/c;-><init>()V

    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/c;->write([B)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/c;

    move-result-object v0

    .line 3
    array-length p1, p1

    int-to-long v1, p1

    invoke-static {p0, v1, v2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;JLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a()Ljava/io/InputStream;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    move-result-object v0

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;->z()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()J
.end method

.method public abstract c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;
.end method

.method public close()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    move-result-object v0

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Ljava/io/Closeable;)V

    return-void
.end method
