.class public final Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;
.super Ljava/lang/Object;
.source "CipherSuite.java"


# static fields
.field static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final l:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final m:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final n:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final o:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final p:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final q:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

.field public static final r:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;


# instance fields
.field final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h$a;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h$a;-><init>()V

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->b:Ljava/util/Comparator;

    .line 20
    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->c:Ljava/util/Map;

    const-string v0, "SSL_RSA_WITH_NULL_MD5"

    const/4 v1, 0x1

    .line 25
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_RSA_WITH_NULL_SHA"

    const/4 v1, 0x2

    .line 26
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_RSA_EXPORT_WITH_RC4_40_MD5"

    const/4 v1, 0x3

    .line 27
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_RSA_WITH_RC4_128_MD5"

    const/4 v1, 0x4

    .line 28
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_RSA_WITH_RC4_128_SHA"

    const/4 v1, 0x5

    .line 29
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v1, 0x8

    .line 32
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_RSA_WITH_DES_CBC_SHA"

    const/16 v1, 0x9

    .line 33
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_RSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v1, 0xa

    .line 34
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v1, 0x11

    .line 41
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DHE_DSS_WITH_DES_CBC_SHA"

    const/16 v1, 0x12

    .line 42
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA"

    const/16 v1, 0x13

    .line 43
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v1, 0x14

    .line 44
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DHE_RSA_WITH_DES_CBC_SHA"

    const/16 v1, 0x15

    .line 45
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v1, 0x16

    .line 46
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DH_anon_EXPORT_WITH_RC4_40_MD5"

    const/16 v1, 0x17

    .line 47
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DH_anon_WITH_RC4_128_MD5"

    const/16 v1, 0x18

    .line 48
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v1, 0x19

    .line 49
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DH_anon_WITH_DES_CBC_SHA"

    const/16 v1, 0x1a

    .line 50
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "SSL_DH_anon_WITH_3DES_EDE_CBC_SHA"

    const/16 v1, 0x1b

    .line 51
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_WITH_DES_CBC_SHA"

    const/16 v1, 0x1e

    .line 52
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_WITH_3DES_EDE_CBC_SHA"

    const/16 v1, 0x1f

    .line 53
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_WITH_RC4_128_SHA"

    const/16 v1, 0x20

    .line 54
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_WITH_DES_CBC_MD5"

    const/16 v1, 0x22

    .line 56
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_WITH_3DES_EDE_CBC_MD5"

    const/16 v1, 0x23

    .line 57
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_WITH_RC4_128_MD5"

    const/16 v1, 0x24

    .line 58
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA"

    const/16 v1, 0x26

    .line 60
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_EXPORT_WITH_RC4_40_SHA"

    const/16 v1, 0x28

    .line 62
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5"

    const/16 v1, 0x29

    .line 63
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_KRB5_EXPORT_WITH_RC4_40_MD5"

    const/16 v1, 0x2b

    .line 65
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_AES_128_CBC_SHA"

    const/16 v1, 0x2f

    .line 69
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA"

    const/16 v1, 0x32

    .line 72
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA"

    const/16 v1, 0x33

    .line 73
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DH_anon_WITH_AES_128_CBC_SHA"

    const/16 v1, 0x34

    .line 74
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_AES_256_CBC_SHA"

    const/16 v1, 0x35

    .line 75
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA"

    const/16 v1, 0x38

    .line 78
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA"

    const/16 v1, 0x39

    .line 79
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DH_anon_WITH_AES_256_CBC_SHA"

    const/16 v1, 0x3a

    .line 80
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_NULL_SHA256"

    const/16 v1, 0x3b

    .line 81
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v1, 0x3c

    .line 82
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_AES_256_CBC_SHA256"

    const/16 v1, 0x3d

    .line 83
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"

    const/16 v1, 0x40

    .line 86
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_CAMELLIA_128_CBC_SHA"

    const/16 v1, 0x41

    .line 87
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA"

    const/16 v1, 0x44

    .line 90
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA"

    const/16 v1, 0x45

    .line 91
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v1, 0x67

    .line 93
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256"

    const/16 v1, 0x6a

    .line 96
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"

    const/16 v1, 0x6b

    .line 97
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DH_anon_WITH_AES_128_CBC_SHA256"

    const/16 v1, 0x6c

    .line 98
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DH_anon_WITH_AES_256_CBC_SHA256"

    const/16 v1, 0x6d

    .line 99
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_CAMELLIA_256_CBC_SHA"

    const/16 v1, 0x84

    .line 100
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA"

    const/16 v1, 0x87

    .line 103
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA"

    const/16 v1, 0x88

    .line 104
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_PSK_WITH_RC4_128_SHA"

    const/16 v1, 0x8a

    .line 106
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_PSK_WITH_3DES_EDE_CBC_SHA"

    const/16 v1, 0x8b

    .line 107
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_PSK_WITH_AES_128_CBC_SHA"

    const/16 v1, 0x8c

    .line 108
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_PSK_WITH_AES_256_CBC_SHA"

    const/16 v1, 0x8d

    .line 109
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_SEED_CBC_SHA"

    const/16 v1, 0x96

    .line 118
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v1, 0x9c

    .line 124
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v1, 0x9d

    .line 125
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v1, 0x9e

    .line 126
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v1, 0x9f

    .line 127
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256"

    const/16 v1, 0xa2

    .line 130
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384"

    const/16 v1, 0xa3

    .line 131
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DH_anon_WITH_AES_128_GCM_SHA256"

    const/16 v1, 0xa6

    .line 134
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_DH_anon_WITH_AES_256_GCM_SHA384"

    const/16 v1, 0xa7

    .line 135
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"

    const/16 v1, 0xff

    .line 166
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_FALLBACK_SCSV"

    const/16 v1, 0x5600

    .line 167
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_ECDSA_WITH_NULL_SHA"

    const v1, 0xc001

    .line 168
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_ECDSA_WITH_RC4_128_SHA"

    const v1, 0xc002

    .line 169
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"

    const v1, 0xc003

    .line 170
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA"

    const v1, 0xc004

    .line 171
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA"

    const v1, 0xc005

    .line 172
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_NULL_SHA"

    const v1, 0xc006

    .line 173
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA"

    const v1, 0xc007

    .line 174
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA"

    const v1, 0xc008

    .line 175
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"

    const v1, 0xc009

    .line 176
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"

    const v1, 0xc00a

    .line 177
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_RSA_WITH_NULL_SHA"

    const v1, 0xc00b

    .line 178
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_RSA_WITH_RC4_128_SHA"

    const v1, 0xc00c

    .line 179
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA"

    const v1, 0xc00d

    .line 180
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA"

    const v1, 0xc00e

    .line 181
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA"

    const v1, 0xc00f

    .line 182
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_NULL_SHA"

    const v1, 0xc010

    .line 183
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_RC4_128_SHA"

    const v1, 0xc011

    .line 184
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"

    const v1, 0xc012

    .line 185
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"

    const v1, 0xc013

    .line 186
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA"

    const v1, 0xc014

    .line 187
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->l:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_anon_WITH_NULL_SHA"

    const v1, 0xc015

    .line 188
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_anon_WITH_RC4_128_SHA"

    const v1, 0xc016

    .line 189
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA"

    const v1, 0xc017

    .line 190
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_anon_WITH_AES_128_CBC_SHA"

    const v1, 0xc018

    .line 191
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_anon_WITH_AES_256_CBC_SHA"

    const v1, 0xc019

    .line 192
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"

    const v1, 0xc023

    .line 202
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"

    const v1, 0xc024

    .line 203
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256"

    const v1, 0xc025

    .line 204
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384"

    const v1, 0xc026

    .line 205
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"

    const v1, 0xc027

    .line 206
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"

    const v1, 0xc028

    .line 207
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256"

    const v1, 0xc029

    .line 208
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384"

    const v1, 0xc02a

    .line 209
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"

    const v1, 0xc02b

    .line 210
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->m:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"

    const v1, 0xc02c

    .line 211
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->n:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256"

    const v1, 0xc02d

    .line 212
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384"

    const v1, 0xc02e

    .line 213
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"

    const v1, 0xc02f

    .line 214
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->o:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"

    const v1, 0xc030

    .line 215
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->p:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256"

    const v1, 0xc031

    .line 216
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384"

    const v1, 0xc032

    .line 217
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA"

    const v1, 0xc035

    .line 220
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA"

    const v1, 0xc036

    .line 221
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256"

    const v1, 0xcca8

    .line 343
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->q:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    const-string v0, "TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256"

    const v1, 0xcca9

    .line 344
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v0

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->r:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 5
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a:Ljava/lang/String;

    return-void

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
.end method

.method public static declared-synchronized a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;
    .locals 3

    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->c:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    if-nez v1, :cond_0

    .line 3
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    invoke-direct {v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;-><init>(Ljava/lang/String;)V

    .line 4
    sget-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->c:Ljava/util/Map;

    invoke-interface {v2, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    .line 0
    monitor-exit v0

    throw p0
.end method

.method private static a(Ljava/lang/String;I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;
    .locals 0

    .line 10
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object p0

    return-object p0
.end method

.method static varargs a([Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;",
            ">;"
        }
    .end annotation

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 6
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    .line 7
    invoke-static {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 9
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;->a:Ljava/lang/String;

    return-object v0
.end method
