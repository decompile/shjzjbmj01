.class public final Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;
.super Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$i;
.source "RealConnection.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/i;


# instance fields
.field private final b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/j;

.field private final c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

.field private d:Ljava/net/Socket;

.field private e:Ljava/net/Socket;

.field private f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

.field private g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

.field private h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

.field private i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

.field private j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

.field public k:Z

.field public l:I

.field public m:I

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/ref/Reference<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;",
            ">;>;"
        }
    .end annotation
.end field

.field public o:J


# direct methods
.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/j;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$i;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->m:I

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->n:Ljava/util/List;

    const-wide v0, 0x7fffffffffffffffL

    .line 12
    iput-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->o:J

    .line 15
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/j;

    .line 16
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    return-void
.end method

.method private a(IILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CONNECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    invoke-static {p4, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;Z)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, " HTTP/1.1"

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 250
    :goto_0
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_int108/a;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    const/4 v3, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_int108/a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;)V

    .line 251
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    invoke-interface {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;->t()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;

    move-result-object v1

    int-to-long v4, p1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;->a(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;

    .line 252
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    invoke-interface {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;->t()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;

    move-result-object v1

    int-to-long v4, p2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4, v5, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;->a(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;

    .line 253
    invoke-virtual {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_int108/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;Ljava/lang/String;)V

    .line 254
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_int108/a;->a()V

    const/4 v1, 0x0

    .line 255
    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_int108/a;->a(Z)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    move-result-object v1

    .line 256
    invoke-virtual {v1, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    move-result-object p3

    .line 257
    invoke-virtual {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    move-result-object p3

    .line 260
    invoke-static {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/e;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)J

    move-result-wide v1

    const-wide/16 v4, -0x1

    cmp-long v6, v1, v4

    if-nez v6, :cond_0

    const-wide/16 v1, 0x0

    .line 264
    :cond_0
    invoke-virtual {v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_int108/a;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;

    move-result-object v0

    .line 265
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v2, 0x7fffffff

    invoke-static {v0, v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;ILjava/util/concurrent/TimeUnit;)Z

    .line 266
    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;->close()V

    .line 268
    invoke-virtual {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->c()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x197

    if-ne v0, v1, :cond_3

    .line 280
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b;

    move-result-object v0

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-interface {v0, v1, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "Connection"

    .line 284
    invoke-virtual {p3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "close"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_1

    return-object v0

    :cond_1
    move-object p3, v0

    goto/16 :goto_0

    .line 285
    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Failed to authenticate with proxy"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 293
    :cond_3
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Unexpected response code for CONNECT: "

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    invoke-virtual {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->c()I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 295
    :cond_4
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;->o()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/c;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/c;->x()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;->o()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/c;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/c;->x()Z

    move-result p1

    if-eqz p1, :cond_5

    return-object v3

    .line 296
    :cond_5
    new-instance p1, Ljava/io/IOException;

    const-string p2, "TLS tunnel buffered too many bytes!"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(IIILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 95
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x15

    if-ge v2, v3, :cond_1

    .line 98
    invoke-direct {p0, p1, p2, p4, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->a(IILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;)V

    .line 99
    invoke-direct {p0, p2, p3, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->a(IILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 105
    :cond_0
    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    invoke-static {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Ljava/net/Socket;)V

    const/4 v3, 0x0

    .line 106
    iput-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    .line 107
    iput-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    .line 108
    iput-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    .line 109
    iget-object v4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->d()Ljava/net/InetSocketAddress;

    move-result-object v4

    iget-object v5, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->b()Ljava/net/Proxy;

    move-result-object v5

    invoke-virtual {p5, p4, v4, v5, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private a(IILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->b()Ljava/net/Proxy;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    .line 113
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    sget-object v3, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v2, v3, :cond_1

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    sget-object v3, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v2, v3, :cond_0

    goto :goto_0

    .line 114
    :cond_0
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1, v0}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->i()Ljavax/net/SocketFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    .line 117
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->d()Ljava/net/InetSocketAddress;

    move-result-object v1

    invoke-virtual {p4, p3, v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;)V

    .line 118
    iget-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    invoke-virtual {p3, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 120
    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;

    move-result-object p2

    iget-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    iget-object p4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->d()Ljava/net/InetSocketAddress;

    move-result-object p4

    invoke-virtual {p2, p3, p4, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->a(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_1

    .line 132
    :try_start_1
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/l;->b(Ljava/net/Socket;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;

    move-result-object p1

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    move-result-object p1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    .line 133
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/l;->a(Ljava/net/Socket;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;

    move-result-object p1

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    move-result-object p1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 135
    invoke-virtual {p1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object p2

    const-string p3, "throw with null exception"

    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    :goto_2
    return-void

    .line 136
    :cond_2
    new-instance p2, Ljava/io/IOException;

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_1
    move-exception p1

    .line 137
    new-instance p2, Ljava/net/ConnectException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Failed to connect to "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->d()Ljava/net/InetSocketAddress;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2, p1}, Ljava/net/ConnectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 139
    throw p2
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/b;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 157
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->j()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    const/4 v2, 0x0

    .line 163
    :try_start_0
    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    .line 164
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v4

    invoke-virtual {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v5

    invoke-virtual {v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->j()I

    move-result v5

    const/4 v6, 0x1

    .line 165
    invoke-virtual {v1, v3, v4, v5, v6}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v1

    check-cast v1, Ljavax/net/ssl/SSLSocket;
    :try_end_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 169
    :try_start_1
    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/b;->a(Ljavax/net/ssl/SSLSocket;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/k;

    move-result-object p1

    .line 170
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/k;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 171
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;

    move-result-object v3

    .line 172
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v4

    invoke-virtual {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->e()Ljava/util/List;

    move-result-object v5

    .line 173
    invoke-virtual {v3, v1, v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V

    .line 178
    :cond_0
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    .line 179
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v3

    invoke-static {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;->a(Ljavax/net/ssl/SSLSession;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

    move-result-object v3

    .line 182
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->d()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v4

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v5

    invoke-virtual {v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 191
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/g;

    move-result-object v4

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v0

    .line 192
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;->b()Ljava/util/List;

    move-result-object v5

    .line 193
    invoke-virtual {v4, v0, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/g;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 197
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/k;->c()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 198
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;

    move-result-object p1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->b(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;

    move-result-object v2

    .line 200
    :cond_1
    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    .line 201
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/l;->b(Ljava/net/Socket;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;

    move-result-object p1

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    move-result-object p1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    .line 202
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/l;->a(Ljava/net/Socket;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;

    move-result-object p1

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    move-result-object p1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    .line 203
    iput-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

    if-eqz v2, :cond_2

    .line 205
    invoke-static {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    move-result-object p1

    goto :goto_0

    :cond_2
    sget-object p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    :goto_0
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;
    :try_end_1
    .catch Ljava/lang/AssertionError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_3

    .line 213
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;

    move-result-object p1

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->a(Ljavax/net/ssl/SSLSocket;)V

    :cond_3
    return-void

    .line 214
    :cond_4
    :try_start_2
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;->b()Ljava/util/List;

    move-result-object p1

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/security/cert/X509Certificate;

    .line 215
    new-instance v2, Ljavax/net/ssl/SSLPeerUnverifiedException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Hostname "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not verified:\n    certificate: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/g;->a(Ljava/security/cert/Certificate;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n    DN: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/Principal;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n    subjectAltNames: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_case1/e;->a(Ljava/security/cert/X509Certificate;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/AssertionError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    move-object v2, v1

    goto :goto_1

    :catchall_1
    move-exception p1

    move-object v1, v2

    goto :goto_2

    :catch_1
    move-exception p1

    .line 238
    :goto_1
    :try_start_3
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Ljava/lang/AssertionError;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 239
    :cond_5
    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_2
    if-eqz v1, :cond_6

    .line 242
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->a(Ljavax/net/ssl/SSLSocket;)V

    .line 245
    :cond_6
    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Ljava/net/Socket;)V

    .line 247
    throw p1
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->j()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    if-nez v0, :cond_0

    .line 141
    sget-object p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    .line 142
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    return-void

    .line 146
    :cond_0
    invoke-virtual {p3, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;->g(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;)V

    .line 147
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/b;)V

    .line 148
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

    invoke-virtual {p3, p2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;)V

    .line 150
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    sget-object p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    if-ne p1, p2, :cond_1

    .line 151
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 152
    new-instance p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$h;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$h;-><init>(Z)V

    iget-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    iget-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    .line 153
    invoke-virtual {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object p3

    invoke-virtual {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object p3

    invoke-virtual {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object p3

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    invoke-virtual {p1, p2, p3, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$h;->a(Ljava/net/Socket;Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$h;

    move-result-object p1

    .line 154
    invoke-virtual {p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$h;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$i;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$h;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g$h;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    move-result-object p1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    .line 156
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;->c()V

    :cond_1
    return-void
.end method

.method private e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;
    .locals 3

    .line 1
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;-><init>()V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    .line 2
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object v0

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    .line 3
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Host"

    invoke-virtual {v0, v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object v0

    const-string v1, "Proxy-Connection"

    const-string v2, "Keep-Alive"

    .line 4
    invoke-virtual {v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object v0

    .line 5
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/d;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "User-Agent"

    invoke-virtual {v0, v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .line 339
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    if-eqz v0, :cond_0

    .line 340
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/f;

    invoke-direct {v1, p1, p2, p3, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/f;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;)V

    return-object v1

    .line 342
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-interface {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 343
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;->t()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;

    move-result-object v0

    invoke-interface {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;->d()I

    move-result v1

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;->a(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;

    .line 344
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;->t()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;

    move-result-object v0

    invoke-interface {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;->a()I

    move-result p2

    int-to-long v1, p2

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;->a(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;

    .line 345
    new-instance p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_int108/a;

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    invoke-direct {p2, p1, p3, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_int108/a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;)V

    return-object p2
.end method

.method public a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;
    .locals 1

    .line 346
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    return-object v0
.end method

.method public a(IIIZLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;)V
    .locals 15

    move-object v7, p0

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    .line 1
    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    if-nez v0, :cond_a

    .line 4
    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->b()Ljava/util/List;

    move-result-object v0

    .line 5
    new-instance v10, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/b;

    invoke-direct {v10, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/b;-><init>(Ljava/util/List;)V

    .line 7
    iget-object v1, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->j()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    if-nez v1, :cond_2

    .line 8
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/k;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/k;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12
    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v0

    .line 13
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_try19/e;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 14
    :cond_0
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;

    new-instance v2, Ljava/net/UnknownServiceException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CLEARTEXT communication to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not permitted by network security policy"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 15
    :cond_1
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;

    new-instance v1, Ljava/net/UnknownServiceException;

    const-string v2, "CLEARTEXT communication not enabled for client"

    invoke-direct {v1, v2}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_2
    :goto_0
    const/4 v11, 0x0

    move-object v12, v11

    .line 27
    :goto_1
    :try_start_0
    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    .line 28
    invoke-direct/range {v1 .. v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->a(IIILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;)V

    .line 29
    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_3

    goto :goto_3

    :cond_3
    move/from16 v13, p1

    move/from16 v14, p2

    goto :goto_2

    :cond_4
    move/from16 v13, p1

    move/from16 v14, p2

    .line 34
    :try_start_1
    invoke-direct {p0, v13, v14, v8, v9}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->a(IILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;)V

    .line 36
    :goto_2
    invoke-direct {p0, v10, v8, v9}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;)V

    .line 37
    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->d()Ljava/net/InetSocketAddress;

    move-result-object v0

    iget-object v1, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->b()Ljava/net/Proxy;

    move-result-object v1

    iget-object v2, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    invoke-virtual {v9, v8, v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 64
    :goto_3
    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    if-eqz v0, :cond_5

    goto :goto_4

    .line 65
    :cond_5
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Too many tunnel connections attempted: 21"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    .line 67
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;

    invoke-direct {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 70
    :cond_6
    :goto_4
    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    if-eqz v0, :cond_7

    .line 71
    iget-object v1, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/j;

    monitor-enter v1

    .line 72
    :try_start_2
    iget-object v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;->b()I

    move-result v0

    iput v0, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->m:I

    .line 73
    monitor-exit v1

    goto :goto_5

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_7
    :goto_5
    return-void

    :catch_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    move/from16 v13, p1

    move/from16 v14, p2

    .line 74
    :goto_6
    iget-object v1, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Ljava/net/Socket;)V

    .line 75
    iget-object v1, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Ljava/net/Socket;)V

    .line 76
    iput-object v11, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    .line 77
    iput-object v11, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->d:Ljava/net/Socket;

    .line 78
    iput-object v11, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    .line 79
    iput-object v11, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;

    .line 80
    iput-object v11, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

    .line 81
    iput-object v11, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    .line 82
    iput-object v11, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    .line 84
    iget-object v1, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->d()Ljava/net/InetSocketAddress;

    move-result-object v3

    iget-object v1, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->b()Ljava/net/Proxy;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v1, p6

    move-object/from16 v2, p5

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;Ljava/io/IOException;)V

    if-nez v12, :cond_8

    .line 87
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;

    invoke-direct {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;-><init>(Ljava/io/IOException;)V

    move-object v12, v1

    goto :goto_7

    .line 89
    :cond_8
    invoke-virtual {v12, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;->a(Ljava/io/IOException;)V

    :goto_7
    if-eqz p4, :cond_9

    .line 92
    invoke-virtual {v10, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/b;->a(Ljava/io/IOException;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_1

    .line 93
    :cond_9
    throw v12

    .line 94
    :cond_a
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;)V
    .locals 1

    .line 368
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/j;

    monitor-enter v0

    .line 369
    :try_start_0
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;->b()I

    move-result p1

    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->m:I

    .line 370
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 367
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/b;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/b;

    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/i;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/b;)V

    return-void
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;)Z
    .locals 4

    .line 297
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->m:I

    const/4 v2, 0x0

    if-ge v0, v1, :cond_a

    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->k:Z

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 300
    :cond_0
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/a;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/a;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;)Z

    move-result v0

    if-nez v0, :cond_1

    return v2

    .line 303
    :cond_1
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    return v1

    .line 313
    :cond_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    if-nez v0, :cond_3

    return v2

    :cond_3
    if-nez p2, :cond_4

    return v2

    .line 319
    :cond_4
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->b()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v3, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v3, :cond_5

    return v2

    .line 320
    :cond_5
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->b()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v3, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v3, :cond_6

    return v2

    .line 321
    :cond_6
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->d()Ljava/net/InetSocketAddress;

    move-result-object v0

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->d()Ljava/net/InetSocketAddress;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/net/InetSocketAddress;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    return v2

    .line 324
    :cond_7
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object p2

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->d()Ljavax/net/ssl/HostnameVerifier;

    move-result-object p2

    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_case1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_case1/e;

    if-eq p2, v0, :cond_8

    return v2

    .line 325
    :cond_8
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object p2

    invoke-virtual {p0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Z

    move-result p2

    if-nez p2, :cond_9

    return v2

    .line 329
    :cond_9
    :try_start_0
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/g;

    move-result-object p2

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/g;->a(Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    :cond_a
    :goto_0
    return v2
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Z
    .locals 4

    .line 330
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->j()I

    move-result v0

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->j()I

    move-result v1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    .line 334
    :cond_0
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_2

    .line 336
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

    if-eqz v0, :cond_1

    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_case1/e;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_case1/e;

    .line 337
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object p1

    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/security/cert/X509Certificate;

    .line 338
    invoke-virtual {v0, p1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_case1/e;->a(Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method public a(Z)Z
    .locals 4

    .line 347
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_4

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 351
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 352
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;->a()Z

    move-result p1

    xor-int/2addr p1, v2

    return p1

    :cond_1
    if-eqz p1, :cond_3

    .line 357
    :try_start_0
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-virtual {p1}, Ljava/net/Socket;->getSoTimeout()I

    move-result p1
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :try_start_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-virtual {v0, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 360
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;->x()Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    .line 365
    :try_start_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    return v1

    :cond_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    return v2

    :catchall_0
    move-exception v0

    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    invoke-virtual {v3, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 366
    throw v0
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    return v1

    :catch_1
    :cond_3
    return v2

    :cond_4
    :goto_0
    return v1
.end method

.method public b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->h:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/g;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d()Ljava/net/Socket;
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->e:Ljava/net/Socket;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Connection{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    .line 2
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", proxy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    .line 4
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->b()Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " hostAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    .line 6
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->d()Ljava/net/InetSocketAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " cipherSuite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;

    if-eqz v1, :cond_0

    .line 8
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/q;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/h;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, "none"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " protocol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
