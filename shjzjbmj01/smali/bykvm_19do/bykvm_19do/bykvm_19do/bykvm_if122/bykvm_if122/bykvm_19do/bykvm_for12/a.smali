.class public final Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/a;
.super Ljava/lang/Object;
.source "BridgeInterceptor.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t;


# instance fields
.field private final a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/m;


# direct methods
.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/m;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/a;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/m;

    return-void
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/l;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    if-lez v2, :cond_0

    const-string v3, "; "

    .line 70
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    :cond_0
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/l;

    .line 73
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/l;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x3d

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/l;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->f()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object v1

    .line 4
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    move-result-object v2

    const-wide/16 v3, -0x1

    if-eqz v2, :cond_2

    .line 6
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 8
    invoke-virtual {v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Content-Type"

    invoke-virtual {v1, v6, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    .line 11
    :cond_0
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;->a()J

    move-result-wide v5

    cmp-long v2, v5, v3

    if-eqz v2, :cond_1

    .line 13
    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v5, "Content-Length"

    invoke-virtual {v1, v5, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    const-string v2, "Transfer-Encoding"

    .line 14
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    :cond_1
    const-string v2, "Transfer-Encoding"

    const-string v5, "chunked"

    .line 16
    invoke-virtual {v1, v2, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    const-string v2, "Content-Length"

    .line 17
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    :cond_2
    :goto_0
    const-string v2, "Host"

    .line 21
    invoke-virtual {v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    if-nez v2, :cond_3

    .line 22
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v2

    invoke-static {v2, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;Z)Ljava/lang/String;

    move-result-object v2

    const-string v6, "Host"

    invoke-virtual {v1, v6, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    :cond_3
    const-string v2, "Connection"

    .line 25
    invoke-virtual {v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    const-string v2, "Connection"

    const-string v6, "Keep-Alive"

    .line 26
    invoke-virtual {v1, v2, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    :cond_4
    const-string v2, "Accept-Encoding"

    .line 32
    invoke-virtual {v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    const-string v2, "Range"

    invoke-virtual {v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    const/4 v5, 0x1

    const-string v2, "Accept-Encoding"

    const-string v6, "gzip"

    .line 34
    invoke-virtual {v1, v2, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    .line 37
    :cond_5
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/a;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/m;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v6

    invoke-interface {v2, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/m;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Ljava/util/List;

    move-result-object v2

    .line 38
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_6

    .line 39
    invoke-direct {p0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "Cookie"

    invoke-virtual {v1, v6, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    :cond_6
    const-string v2, "User-Agent"

    .line 42
    invoke-virtual {v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7

    .line 43
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/d;->a()Ljava/lang/String;

    move-result-object v2

    const-string v6, "User-Agent"

    invoke-virtual {v1, v6, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    .line 46
    :cond_7
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v1

    invoke-interface {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    move-result-object p1

    .line 48
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/a;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/m;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v2

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;

    move-result-object v6

    invoke-static {v1, v2, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/e;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/m;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;)V

    .line 50
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    move-result-object v1

    .line 51
    invoke-virtual {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    move-result-object v0

    if-eqz v5, :cond_8

    const-string v1, "Content-Encoding"

    .line 54
    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "gzip"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 55
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/e;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 56
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/j;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    move-result-object v2

    invoke-direct {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/j;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;)V

    .line 57
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r$a;

    move-result-object v2

    const-string v5, "Content-Encoding"

    .line 58
    invoke-virtual {v2, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r$a;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r$a;

    move-result-object v2

    const-string v5, "Content-Length"

    .line 59
    invoke-virtual {v2, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r$a;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r$a;

    move-result-object v2

    .line 60
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r$a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;

    move-result-object v2

    .line 61
    invoke-virtual {v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    const-string v2, "Content-Type"

    .line 62
    invoke-virtual {p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 63
    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/h;

    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;

    move-result-object v1

    invoke-direct {v2, p1, v3, v4, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/h;-><init>(Ljava/lang/String;JLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/e;)V

    invoke-virtual {v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    .line 66
    :cond_8
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    move-result-object p1

    return-object p1
.end method
