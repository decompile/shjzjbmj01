.class public final Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;
.super Ljava/lang/Object;
.source "RetryAndFollowUpInterceptor.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t;


# instance fields
.field private final a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

.field private final b:Z

.field private c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

.field private d:Ljava/lang/Object;

.field private volatile e:Z


# direct methods
.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    .line 3
    iput-boolean p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->b:Z

    return-void
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;
    .locals 17

    move-object/from16 v0, p0

    .line 108
    invoke-virtual/range {p1 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->h()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->x()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    .line 110
    iget-object v1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->l()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v1

    .line 111
    iget-object v3, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/g;

    move-result-object v3

    move-object v10, v1

    move-object v9, v2

    move-object v11, v3

    goto :goto_0

    :cond_0
    move-object v9, v2

    move-object v10, v9

    move-object v11, v10

    .line 114
    :goto_0
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    invoke-virtual/range {p1 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->j()I

    move-result v6

    iget-object v2, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->h()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/o;

    move-result-object v7

    iget-object v2, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->w()Ljavax/net/SocketFactory;

    move-result-object v8

    iget-object v2, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    .line 115
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->s()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b;

    move-result-object v12

    iget-object v2, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    .line 116
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->r()Ljava/net/Proxy;

    move-result-object v13

    iget-object v2, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->q()Ljava/util/List;

    move-result-object v14

    iget-object v2, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->e()Ljava/util/List;

    move-result-object v15

    iget-object v2, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->t()Ljava/net/ProxySelector;

    move-result-object v16

    move-object v4, v1

    invoke-direct/range {v4 .. v16}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;-><init>(Ljava/lang/String;ILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/o;Ljavax/net/SocketFactory;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/g;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b;Ljava/net/Proxy;Ljava/util/List;Ljava/util/List;Ljava/net/ProxySelector;)V

    return-object v1
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_e

    .line 151
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 153
    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/i;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 155
    :goto_0
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->c()I

    move-result v2

    .line 157
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v3

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->e()Ljava/lang/String;

    move-result-object v3

    sparse-switch v2, :sswitch_data_0

    return-object v1

    .line 227
    :sswitch_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->v()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v1

    .line 232
    :cond_1
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    .line 236
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->h()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 237
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->h()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->c()I

    move-result v0

    const/16 v2, 0x198

    if-ne v0, v2, :cond_2

    return-object v1

    .line 242
    :cond_2
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object p1

    return-object p1

    :sswitch_1
    if-eqz v0, :cond_3

    .line 243
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;->b()Ljava/net/Proxy;

    move-result-object v1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    .line 244
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->r()Ljava/net/Proxy;

    move-result-object v1

    .line 245
    :goto_1
    invoke-virtual {v1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v1, v2, :cond_4

    .line 248
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->s()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object p1

    return-object p1

    .line 249
    :cond_4
    new-instance p1, Ljava/net/ProtocolException;

    const-string v0, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {p1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 254
    :sswitch_2
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/c0;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object p1

    return-object p1

    :sswitch_3
    const-string v0, "GET"

    .line 260
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "HEAD"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    return-object v1

    .line 269
    :cond_5
    :sswitch_4
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->j()Z

    move-result v0

    if-nez v0, :cond_6

    return-object v1

    :cond_6
    const-string v0, "Location"

    .line 271
    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    return-object v1

    .line 273
    :cond_7
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v2

    invoke-virtual {v2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v0

    if-nez v0, :cond_8

    return-object v1

    .line 279
    :cond_8
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v4

    invoke-virtual {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v4

    invoke-virtual {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 280
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->k()Z

    move-result v2

    if-nez v2, :cond_9

    return-object v1

    .line 283
    :cond_9
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->f()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object v2

    .line 284
    invoke-static {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/f;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 285
    invoke-static {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/f;->d(Ljava/lang/String;)Z

    move-result v4

    .line 286
    invoke-static {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/f;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v3, "GET"

    .line 287
    invoke-virtual {v2, v3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_2

    :cond_a
    if-eqz v4, :cond_b

    .line 289
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    move-result-object v1

    .line 290
    :cond_b
    invoke-virtual {v2, v3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    :goto_2
    if-nez v4, :cond_c

    const-string v1, "Transfer-Encoding"

    .line 293
    invoke-virtual {v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    const-string v1, "Content-Length"

    .line 294
    invoke-virtual {v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    const-string v1, "Content-Type"

    .line 295
    invoke-virtual {v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    .line 302
    :cond_c
    invoke-direct {p0, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Z

    move-result p1

    if-nez p1, :cond_d

    const-string p1, "Authorization"

    .line 303
    invoke-virtual {v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    .line 306
    :cond_d
    invoke-virtual {v2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object p1

    return-object p1

    .line 307
    :cond_e
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    nop

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_4
        0x12d -> :sswitch_4
        0x12e -> :sswitch_4
        0x12f -> :sswitch_4
        0x133 -> :sswitch_3
        0x134 -> :sswitch_3
        0x191 -> :sswitch_2
        0x197 -> :sswitch_1
        0x198 -> :sswitch_0
    .end sparse-switch
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Z
    .locals 2

    .line 308
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->k()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object p1

    .line 309
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->j()I

    move-result v0

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->j()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 311
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->m()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;->m()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private a(Ljava/io/IOException;Z)Z
    .locals 3

    .line 131
    instance-of v0, p1, Ljava/net/ProtocolException;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 137
    :cond_0
    instance-of v0, p1, Ljava/io/InterruptedIOException;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 138
    instance-of p1, p1, Ljava/net/SocketTimeoutException;

    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 143
    :cond_2
    instance-of p2, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz p2, :cond_3

    .line 146
    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    instance-of p2, p2, Ljava/security/cert/CertificateException;

    if-eqz p2, :cond_3

    return v1

    .line 150
    :cond_3
    instance-of p1, p1, Ljavax/net/ssl/SSLPeerUnverifiedException;

    if-eqz p1, :cond_4

    return v1

    :cond_4
    return v2
.end method

.method private a(Ljava/io/IOException;ZLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;)Z
    .locals 2

    .line 117
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->a(Ljava/io/IOException;)V

    .line 120
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->v()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    if-eqz p2, :cond_1

    .line 123
    invoke-virtual {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    .line 127
    :cond_1
    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a(Ljava/io/IOException;Z)Z

    move-result p1

    if-nez p1, :cond_2

    return v1

    .line 130
    :cond_2
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->c()Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    :cond_3
    const/4 p1, 0x1

    return p1
.end method


# virtual methods
.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3
    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/t$a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v0

    .line 4
    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/g;

    .line 5
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/g;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;

    move-result-object v7

    .line 6
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/g;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;

    move-result-object v8

    .line 8
    new-instance v9, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/j;

    move-result-object v2

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    invoke-direct {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v3

    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->d:Ljava/lang/Object;

    move-object v1, v9

    move-object v4, v7

    move-object v5, v8

    invoke-direct/range {v1 .. v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/j;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;Ljava/lang/Object;)V

    iput-object v9, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, v0

    move-object v0, v10

    const/4 v2, 0x0

    .line 14
    :goto_0
    iget-boolean v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->e:Z

    if-nez v3, :cond_9

    .line 22
    :try_start_0
    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {p1, v1, v3, v10, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/g;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/c;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/c;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    move-result-object v3
    :try_end_0
    .catch Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    move-result-object v1

    .line 48
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    move-result-object v0

    .line 49
    invoke-virtual {v0, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    move-result-object v0

    .line 51
    invoke-virtual {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->c(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0$a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    move-result-object v0

    goto :goto_1

    :cond_0
    move-object v0, v3

    .line 57
    :goto_1
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object v11

    if-nez v11, :cond_2

    .line 60
    iget-boolean p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->b:Z

    if-nez p1, :cond_1

    .line 61
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->e()V

    :cond_1
    return-object v0

    .line 66
    :cond_2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;

    move-result-object v1

    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/c;->a(Ljava/io/Closeable;)V

    add-int/lit8 v12, v2, 0x1

    const/16 v1, 0x14

    if-gt v12, v1, :cond_5

    .line 73
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    .line 78
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 79
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->e()V

    .line 80
    new-instance v13, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/j;

    move-result-object v2

    .line 81
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->g()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;

    move-result-object v1

    invoke-direct {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;

    move-result-object v3

    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->d:Ljava/lang/Object;

    move-object v1, v13

    move-object v4, v7

    move-object v5, v8

    invoke-direct/range {v1 .. v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/j;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/p;Ljava/lang/Object;)V

    iput-object v13, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    goto :goto_2

    .line 82
    :cond_3
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/c;

    move-result-object v1

    if-nez v1, :cond_4

    :goto_2
    move-object v1, v11

    move v2, v12

    goto :goto_0

    .line 83
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Closing the body of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " didn\'t close its backing stream. Bad interceptor?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 84
    :cond_5
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->e()V

    .line 85
    new-instance p1, Ljava/net/ProtocolException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Too many follow-up requests: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception v3

    .line 86
    :try_start_1
    instance-of v4, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_new1/a;

    if-nez v4, :cond_6

    const/4 v4, 0x1

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    .line 87
    :goto_3
    invoke-direct {p0, v3, v4, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a(Ljava/io/IOException;ZLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;)Z

    move-result v4

    if-eqz v4, :cond_7

    goto/16 :goto_0

    :cond_7
    throw v3

    :catch_1
    move-exception v3

    .line 88
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;->a()Ljava/io/IOException;

    move-result-object v4

    invoke-direct {p0, v4, v9, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->a(Ljava/io/IOException;ZLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;)Z

    move-result v4

    if-eqz v4, :cond_8

    goto/16 :goto_0

    .line 89
    :cond_8
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/e;->a()Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :goto_4
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {v0, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->a(Ljava/io/IOException;)V

    .line 103
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->e()V

    .line 105
    throw p1

    .line 106
    :cond_9
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_if122/g;->e()V

    .line 107
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Canceled"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->d:Ljava/lang/Object;

    return-void
.end method

.method public a()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/j;->e:Z

    return v0
.end method
