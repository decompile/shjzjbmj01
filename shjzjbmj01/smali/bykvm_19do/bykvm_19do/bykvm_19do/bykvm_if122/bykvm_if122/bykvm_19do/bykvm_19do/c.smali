.class public final Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_19do/c;
.super Ljava/lang/Object;
.source "CacheStrategy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_19do/c$a;
    }
.end annotation


# instance fields
.field public final a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

.field public final b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;


# direct methods
.method constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_19do/c;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    .line 3
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_19do/c;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    return-void
.end method

.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;)Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->c()I

    move-result v0

    const/4 v1, 0x0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "Expires"

    .line 21
    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 22
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;->d()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 23
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    :cond_0
    :sswitch_1
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;

    move-result-object p0

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;->i()Z

    move-result p0

    if-nez p0, :cond_1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;

    move-result-object p0

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/d;->i()Z

    move-result p0

    if-nez p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0xcb -> :sswitch_1
        0xcc -> :sswitch_1
        0x12c -> :sswitch_1
        0x12d -> :sswitch_1
        0x12e -> :sswitch_0
        0x133 -> :sswitch_0
        0x134 -> :sswitch_1
        0x194 -> :sswitch_1
        0x195 -> :sswitch_1
        0x19a -> :sswitch_1
        0x19e -> :sswitch_1
        0x1f5 -> :sswitch_1
    .end sparse-switch
.end method
