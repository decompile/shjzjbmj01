.class final Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;
.super Ljava/lang/Object;
.source "SegmentPool.java"


# static fields
.field static a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

.field static b:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;
    .locals 6

    .line 1
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;

    monitor-enter v0

    .line 2
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    if-eqz v1, :cond_0

    .line 3
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    .line 4
    iget-object v2, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    sput-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    const/4 v2, 0x0

    .line 5
    iput-object v2, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    .line 6
    sget-wide v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->b:J

    const-wide/16 v4, 0x2000

    sub-long/2addr v2, v4

    sput-wide v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->b:J

    .line 7
    monitor-exit v0

    return-object v1

    .line 9
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;-><init>()V

    return-object v0

    :catchall_0
    move-exception v1

    .line 11
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;)V
    .locals 8

    .line 12
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    if-nez v0, :cond_2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    if-nez v0, :cond_2

    .line 13
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;->d:Z

    if-eqz v0, :cond_0

    return-void

    .line 14
    :cond_0
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;

    monitor-enter v0

    .line 15
    :try_start_0
    sget-wide v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->b:J

    const-wide/16 v3, 0x2000

    add-long/2addr v1, v3

    const-wide/32 v5, 0x10000

    cmp-long v7, v1, v5

    if-lez v7, :cond_1

    monitor-exit v0

    return-void

    .line 16
    :cond_1
    sget-wide v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->b:J

    const/4 v5, 0x0

    add-long/2addr v1, v3

    sput-wide v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->b:J

    .line 17
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    const/4 v1, 0x0

    .line 18
    iput v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;->c:I

    iput v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;->b:I

    .line 19
    sput-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/p;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/o;

    .line 20
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 21
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method
