.class public interface abstract Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
.super Ljava/lang/Object;
.source "BufferedSink.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;
.implements Ljava/nio/channels/WritableByteChannel;


# virtual methods
.method public abstract A()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract d(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract flush()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract g(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract o()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/c;
.end method

.method public abstract write([B)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract write([BII)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract writeByte(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract writeInt(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract writeShort(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
