.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;
.super Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;
.source "AsyncTimeout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a$c;
    }
.end annotation


# static fields
.field private static final h:J

.field private static final i:J

.field static j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;


# instance fields
.field private e:Z

.field private f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3c

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->h:J

    .line 2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    sput-wide v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->i:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;-><init>()V

    return-void
.end method

.method private static declared-synchronized a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;JZ)V
    .locals 6

    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    if-nez v1, :cond_0

    .line 2
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    invoke-direct {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;-><init>()V

    sput-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    .line 3
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a$c;

    invoke-direct {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a$c;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 6
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, p1, v3

    if-eqz v5, :cond_1

    if-eqz p3, :cond_1

    .line 10
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;->c()J

    move-result-wide v3

    const/4 p3, 0x0

    sub-long/2addr v3, v1

    invoke-static {p1, p2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    const/4 p3, 0x0

    add-long/2addr p1, v1

    iput-wide p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->g:J

    goto :goto_0

    :cond_1
    if-eqz v5, :cond_2

    add-long/2addr p1, v1

    .line 12
    iput-wide p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->g:J

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_6

    .line 14
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;->c()J

    move-result-wide p1

    iput-wide p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->g:J

    .line 20
    :goto_0
    invoke-direct {p0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->b(J)J

    move-result-wide p1

    .line 21
    sget-object p3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    .line 22
    :goto_1
    iget-object v3, p3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    if-eqz v3, :cond_4

    invoke-direct {v3, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->b(J)J

    move-result-wide v3

    cmp-long v5, p1, v3

    if-gez v5, :cond_3

    goto :goto_2

    .line 23
    :cond_3
    iget-object p3, p3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    goto :goto_1

    .line 25
    :cond_4
    :goto_2
    iget-object p1, p3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    .line 26
    iput-object p0, p3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    .line 27
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    if-ne p3, p0, :cond_5

    .line 28
    const-class p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    monitor-exit v0

    return-void

    .line 29
    :cond_6
    :try_start_1
    new-instance p0, Ljava/lang/AssertionError;

    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p0

    .line 0
    monitor-exit v0

    throw p0
.end method

.method private static declared-synchronized a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;)Z
    .locals 3

    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    monitor-enter v0

    .line 30
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    :goto_0
    if-eqz v1, :cond_1

    .line 31
    iget-object v2, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    if-ne v2, p0, :cond_0

    .line 32
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    iput-object v2, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    const/4 v1, 0x0

    .line 33
    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p0, 0x0

    :goto_1
    monitor-exit v0

    return p0

    :cond_0
    move-object v1, v2

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    goto :goto_1

    :catchall_0
    move-exception p0

    .line 29
    monitor-exit v0

    throw p0
.end method

.method private b(J)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->g:J

    sub-long/2addr v0, p1

    return-wide v0
.end method

.method static j()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    iget-object v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 5
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 6
    sget-wide v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->h:J

    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    invoke-virtual {v0, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 7
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    iget-object v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v2

    sget-wide v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->i:J

    cmp-long v0, v4, v2

    if-ltz v0, :cond_0

    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    :cond_0
    return-object v1

    .line 12
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->b(J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-lez v6, :cond_2

    const-wide/32 v4, 0xf4240

    .line 18
    div-long v6, v2, v4

    mul-long v4, v4, v6

    sub-long/2addr v2, v4

    long-to-int v0, v2

    .line 20
    const-class v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    invoke-virtual {v2, v6, v7, v0}, Ljava/lang/Object;->wait(JI)V

    return-object v1

    .line 25
    :cond_2
    sget-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    iget-object v3, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    iput-object v3, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    .line 26
    iput-object v1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;
    .locals 1

    .line 34
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a$a;

    invoke-direct {v0, p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/r;)V

    return-object v0
.end method

.method public final a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;
    .locals 1

    .line 35
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a$b;

    invoke-direct {v0, p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a$b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/s;)V

    return-object v0
.end method

.method final a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 38
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->h()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    .line 39
    :cond_0
    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    return-object p1
.end method

.method final a(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 36
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 37
    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method protected b(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2

    .line 2
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 4
    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_0
    return-object v0
.end method

.method public final g()V
    .locals 6

    .line 1
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->e:Z

    if-nez v0, :cond_1

    .line 2
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;->f()J

    move-result-wide v0

    .line 3
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/t;->d()Z

    move-result v2

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-nez v5, :cond_0

    if-nez v2, :cond_0

    return-void

    :cond_0
    const/4 v3, 0x1

    .line 7
    iput-boolean v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->e:Z

    .line 8
    invoke-static {p0, v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;JZ)V

    return-void

    .line 9
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unbalanced enter/exit"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final h()Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->e:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    iput-boolean v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->e:Z

    .line 3
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_19do/a;)Z

    move-result v0

    return v0
.end method

.method protected i()V
    .locals 0

    return-void
.end method
