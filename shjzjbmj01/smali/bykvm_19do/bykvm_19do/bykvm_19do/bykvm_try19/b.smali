.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;
.super Ljava/lang/Object;
.source "InitHelper.java"


# static fields
.field private static volatile a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a; = null

.field private static b:J = 0x0L

.field private static c:Z = false


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;
    .locals 1

    .line 2
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 0

    .line 3
    invoke-static {p0}, Lcom/bytedance/msdk/adapter/ThirdSdkInit;->initUnitySDK(Landroid/app/Activity;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x1

    .line 4
    sput-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->c:Z

    .line 5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->b:J

    .line 6
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->b(Landroid/content/Context;)V

    .line 7
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->f()V

    .line 8
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->e()V

    .line 9
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/t;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/t;

    .line 10
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;

    move-result-object p0

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->b()Ljava/lang/String;

    .line 11
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;

    move-result-object p0

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;->a()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;)V
    .locals 1

    .line 12
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b$a;

    invoke-direct {v0, p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b$a;-><init>(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;)V

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/util/ThreadHelper;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Ljava/util/Map;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->c(Ljava/util/Map;)V

    return-void
.end method

.method public static b(Ljava/util/Map;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 7
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->b()V

    .line 8
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a(Landroid/content/Context;Ljava/util/Map;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static b()V
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;

    if-nez v0, :cond_1

    .line 2
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    monitor-enter v0

    .line 3
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;

    if-nez v1, :cond_0

    .line 4
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;

    invoke-direct {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;-><init>()V

    sput-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;

    .line 6
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    return-void
.end method

.method private static b(Landroid/content/Context;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 9
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->b(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public static c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->b()V

    .line 2
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 3
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    const-string v1, "pangle"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 10
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c()Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-string v1, "pangle"

    .line 13
    invoke-static {v1}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 14
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 15
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "app_id"

    .line 16
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    :cond_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    const-string v1, "baidu"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 23
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v1, "baidu"

    .line 25
    invoke-static {v1}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 27
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "app_id"

    .line 28
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "baidu_is_read_device_id"

    .line 29
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v3

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "true"

    goto :goto_1

    :cond_3
    const-string v3, "false"

    :goto_1
    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    :cond_4
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    const-string v1, "admob"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 37
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v1, "admob"

    .line 39
    invoke-static {v1}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 41
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "app_id"

    .line 42
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    :cond_5
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    const-string v1, "gdt"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 51
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v1, "gdt"

    .line 53
    invoke-static {v1}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 55
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "app_id"

    .line 56
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :cond_6
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    const-string v1, "sigmob"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 65
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_7

    if-eqz v0, :cond_7

    const-string v2, "sigmob"

    .line 68
    invoke-static {v2}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 70
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v4, "app_id"

    .line 71
    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "app_key"

    .line 72
    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    :cond_7
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    const-string v1, "ks"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 80
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v1, "ks"

    .line 82
    invoke-static {v1}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 84
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "app_id"

    .line 85
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->f()Ljava/lang/String;

    move-result-object v0

    const-string v3, "app_name"

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    const-string v0, "mintegral"

    .line 92
    invoke-static {v0}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 94
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v1

    const-string v2, "mintegral"

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 96
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v2

    .line 97
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->b()Ljava/lang/String;

    move-result-object v1

    .line 98
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v4, "app_id"

    .line 99
    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "app_key"

    .line 100
    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    return-void
.end method

.method public static d()J
    .locals 2

    .line 1
    sget-wide v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->b:J

    return-wide v0
.end method

.method private static e()V
    .locals 3

    .line 1
    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InitHelper-->initSetting->loadData Exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    :goto_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->a()V

    return-void
.end method

.method private static f()V
    .locals 3

    .line 1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 6
    :cond_0
    :try_start_0
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

    invoke-direct {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;-><init>(Landroid/content/Context;)V

    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;)V

    const/4 v1, 0x1

    .line 7
    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(Z)V

    .line 8
    move-object v2, v0

    check-cast v2, Landroid/app/Application;

    invoke-static {v0, v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(Landroid/content/Context;Landroid/app/Application;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public static g()Z
    .locals 1

    .line 1
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->c:Z

    return v0
.end method
