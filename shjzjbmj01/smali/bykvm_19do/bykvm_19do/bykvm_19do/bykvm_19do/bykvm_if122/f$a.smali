.class Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;
.super Ljava/lang/Object;
.source "DiskBasedCache.java"


# annotations
.annotation build Landroidx/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field a:J

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:J

.field final e:J

.field final f:J

.field final g:J

.field final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)V
    .locals 12

    .line 9
    iget-object v2, p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->c:Ljava/lang/String;

    iget-wide v3, p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->d:J

    iget-wide v5, p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->e:J

    iget-wide v7, p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->f:J

    iget-wide v9, p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->g:J

    .line 16
    invoke-static {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)Ljava/util/List;

    move-result-object v11

    move-object v0, p0

    move-object v1, p1

    .line 17
    invoke-direct/range {v0 .. v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJLjava/util/List;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JJJJLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJJJ",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->b:Ljava/lang/String;

    const-string p1, ""

    .line 3
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p2, 0x0

    :cond_0
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->c:Ljava/lang/String;

    .line 4
    iput-wide p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->d:J

    .line 5
    iput-wide p5, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->e:J

    .line 6
    iput-wide p7, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->f:J

    .line 7
    iput-wide p9, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->g:J

    .line 8
    iput-object p11, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->h:Ljava/util/List;

    return-void
.end method

.method static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$b;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 7
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->b(Ljava/io/InputStream;)I

    move-result v0

    const v1, 0x20150306

    if-ne v0, v1, :cond_0

    .line 12
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$b;)Ljava/lang/String;

    move-result-object v3

    .line 13
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$b;)Ljava/lang/String;

    move-result-object v4

    .line 14
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->c(Ljava/io/InputStream;)J

    move-result-wide v5

    .line 15
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->c(Ljava/io/InputStream;)J

    move-result-wide v7

    .line 16
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->c(Ljava/io/InputStream;)J

    move-result-wide v9

    .line 17
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->c(Ljava/io/InputStream;)J

    move-result-wide v11

    .line 18
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$b;)Ljava/util/List;

    move-result-object v13

    .line 19
    new-instance p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;

    move-object v2, p0

    invoke-direct/range {v2 .. v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJLjava/util/List;)V

    return-object p0

    .line 20
    :cond_0
    new-instance p0, Ljava/io/IOException;

    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    throw p0
.end method

.method private static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;",
            ")",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    return-object v0

    .line 6
    :cond_0
    iget-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->h:Ljava/util/Map;

    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/b;->a(Ljava/util/Map;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method a([B)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;
    .locals 3

    .line 21
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;-><init>()V

    .line 22
    iput-object p1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->b:[B

    .line 23
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->c:Ljava/lang/String;

    iput-object p1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->c:Ljava/lang/String;

    .line 24
    iget-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->d:J

    iput-wide v1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->d:J

    .line 25
    iget-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->e:J

    iput-wide v1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->e:J

    .line 26
    iget-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->f:J

    iput-wide v1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->f:J

    .line 27
    iget-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->g:J

    iput-wide v1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->g:J

    .line 28
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->h:Ljava/util/List;

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/b;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->h:Ljava/util/Map;

    .line 29
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->h:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->i:Ljava/util/List;

    return-object v0
.end method

.method a(Ljava/io/OutputStream;)Z
    .locals 4

    const v0, 0x20150306

    const/4 v1, 0x1

    .line 30
    :try_start_0
    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->a(Ljava/io/OutputStream;I)V

    .line 31
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->c:Ljava/lang/String;

    :goto_0
    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 33
    iget-wide v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->d:J

    invoke-static {p1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->a(Ljava/io/OutputStream;J)V

    .line 34
    iget-wide v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->e:J

    invoke-static {p1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->a(Ljava/io/OutputStream;J)V

    .line 35
    iget-wide v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->f:J

    invoke-static {p1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->a(Ljava/io/OutputStream;J)V

    .line 36
    iget-wide v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->g:J

    invoke-static {p1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->a(Ljava/io/OutputStream;J)V

    .line 37
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f$a;->h:Ljava/util/List;

    invoke-static {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/f;->a(Ljava/util/List;Ljava/io/OutputStream;)V

    .line 38
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v1

    :catchall_0
    move-exception p1

    .line 41
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "%s"

    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1
.end method
