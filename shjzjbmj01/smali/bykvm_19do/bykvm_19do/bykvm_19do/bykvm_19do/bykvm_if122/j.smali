.class Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;
.super Ljava/lang/Thread;
.source "NetworkDispatcher.java"


# instance fields
.field private final a:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;

.field private final c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

.field private final d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

.field private volatile e:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/BlockingQueue;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/BlockingQueue<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->e:Z

    .line 18
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->a:Ljava/util/concurrent/BlockingQueue;

    .line 19
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;

    .line 20
    iput-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

    .line 21
    iput-object p4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    return-void
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;",
            ")V"
        }
    .end annotation

    .line 168
    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;

    move-result-object p2

    .line 169
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    invoke-interface {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V

    return-void
.end method

.method private b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    .line 4
    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V

    return-void
.end method

.method private b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;)V"
        }
    .end annotation

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 2
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->t()I

    move-result p1

    invoke-static {p1}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    .line 1
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->e:Z

    .line 2
    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    return-void
.end method

.method a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V
    .locals 9
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;)V"
        }
    .end annotation

    .line 3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const/4 v2, 0x3

    .line 4
    invoke-virtual {p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x4

    :try_start_0
    const-string v5, "network-queue-take"

    .line 6
    invoke-virtual {p1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 10
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->x()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "network-discard-cancelled"

    .line 11
    invoke-virtual {p1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->b(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->A()V
    :try_end_0
    .catch Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    invoke-virtual {p1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    return-void

    .line 65
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V

    .line 68
    iget-object v5, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;

    invoke-interface {v5, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;

    move-result-object v5

    .line 69
    iget-wide v6, v5, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;->f:J

    invoke-virtual {p1, v6, v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(J)V

    const-string v6, "network-http-complete"

    .line 70
    invoke-virtual {p1, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 74
    iget-boolean v6, v5, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;->e:Z

    if-eqz v6, :cond_1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->w()Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v5, "not-modified"

    .line 75
    invoke-virtual {p1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->b(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->A()V
    :try_end_1
    .catch Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    invoke-virtual {p1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    return-void

    .line 114
    :cond_1
    :try_start_2
    invoke-virtual {p1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    move-result-object v6

    .line 115
    iget-wide v7, v5, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;->f:J

    invoke-virtual {p1, v7, v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(J)V

    const-string v5, "network-parse-complete"

    .line 116
    invoke-virtual {p1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->C()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, v6, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;

    if-eqz v5, :cond_2

    .line 121
    iget-object v5, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->e()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;

    invoke-interface {v5, v7, v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)V

    const-string v5, "network-cache-written"

    .line 122
    invoke-virtual {p1, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 126
    :cond_2
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->z()V

    .line 127
    iget-object v5, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    invoke-interface {v5, p1, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;)V

    .line 128
    invoke-virtual {p1, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->c(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;)V
    :try_end_2
    .catch Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    const-string v6, "NetworkDispatcher Unhandled throwable %s"

    .line 140
    :try_start_3
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v2

    invoke-static {v5, v6, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;

    invoke-direct {v2, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;-><init>(Ljava/lang/Throwable;)V

    .line 142
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    const/4 v3, 0x0

    sub-long/2addr v5, v0

    invoke-virtual {v2, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;->a(J)V

    .line 143
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    invoke-interface {v0, p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V

    .line 144
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->A()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v5

    const-string v6, "Unhandled exception %s"

    .line 145
    :try_start_4
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v2

    invoke-static {v5, v6, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;

    invoke-direct {v2, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;-><init>(Ljava/lang/Throwable;)V

    .line 147
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    const/4 v3, 0x0

    sub-long/2addr v5, v0

    invoke-virtual {v2, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;->a(J)V

    .line 148
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    invoke-interface {v0, p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V

    .line 149
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->A()V

    goto :goto_0

    :catch_1
    move-exception v2

    .line 150
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    const/4 v3, 0x0

    sub-long/2addr v5, v0

    invoke-virtual {v2, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;->a(J)V

    .line 151
    invoke-direct {p0, p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V

    .line 152
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->A()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 166
    :goto_0
    invoke-virtual {p1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    return-void

    :catchall_1
    move-exception v0

    invoke-virtual {p1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    .line 167
    throw v0
.end method

.method public run()V
    .locals 2

    const/16 v0, 0xa

    .line 1
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 4
    :goto_0
    :try_start_0
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->b()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 7
    :catch_0
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->e:Z

    if-eqz v0, :cond_0

    .line 8
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 11
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Ignoring spurious interrupt of NetworkDispatcher thread; use quit() to terminate it"

    invoke-static {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
