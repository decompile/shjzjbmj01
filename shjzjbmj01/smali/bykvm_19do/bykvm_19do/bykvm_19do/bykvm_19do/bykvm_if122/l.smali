.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;
.super Ljava/lang/Object;
.source "OkStack.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l$a;
    }
.end annotation


# instance fields
.field private final a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;-><init>()V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2710

    .line 3
    invoke-virtual {v0, v2, v3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->a(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 4
    invoke-virtual {v0, v2, v3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->b(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 5
    invoke-virtual {v0, v2, v3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->c(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    move-result-object v0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    return-void
.end method

.method private static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;
        }
    .end annotation

    .line 157
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->b()[B

    move-result-object v0

    if-nez v0, :cond_1

    .line 160
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->i()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, ""

    .line 161
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    return-object p0

    .line 167
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->c()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;

    move-result-object p0

    invoke-static {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;[B)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;)Ljava/io/InputStream;
    .locals 0

    .line 1
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;)Ljava/io/InputStream;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;",
            ")",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    .line 97
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-object p0

    .line 99
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;->b()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    .line 100
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;->b()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_2

    .line 101
    invoke-virtual {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 102
    invoke-virtual {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;->b(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_1

    .line 104
    new-instance v5, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;

    invoke-direct {v5, v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;
        }
    .end annotation

    .line 105
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->i()I

    move-result v0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    .line 138
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Unknown method type."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 139
    :pswitch_0
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    move-result-object p1

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    :pswitch_1
    const-string p1, "TRACE"

    .line 140
    invoke-virtual {p0, p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    :pswitch_2
    const-string p1, "OPTIONS"

    .line 141
    invoke-virtual {p0, p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    .line 142
    :pswitch_3
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    .line 143
    :pswitch_4
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    .line 149
    :pswitch_5
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    move-result-object p1

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->d(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    .line 150
    :pswitch_6
    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    move-result-object p1

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->c(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    .line 151
    :pswitch_7
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    .line 152
    :pswitch_8
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->m()[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->c()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;

    move-result-object p1

    .line 156
    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/u;[B)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;

    move-result-object p1

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->c(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/z;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    :cond_0
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(II)Z
    .locals 1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/16 p0, 0x64

    if-gt p0, p1, :cond_0

    const/16 p0, 0xc8

    if-lt p1, p0, :cond_1

    :cond_0
    const/16 p0, 0xcc

    if-eq p1, p0, :cond_1

    const/16 p0, 0x130

    if-eq p1, p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 1
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->u()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    invoke-direct {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;-><init>()V

    .line 7
    new-instance v2, Ljava/net/URL;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->u()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 9
    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object p1

    .line 11
    sget-object v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/q;

    if-eqz v3, :cond_1

    .line 12
    invoke-interface {v3, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    const/4 v3, 0x0

    .line 16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 18
    :try_start_0
    new-instance v4, Ljava/net/URL;

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1, v0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 19
    invoke-virtual {v1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/net/URL;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object v0

    const-string v4, "Host"

    invoke-virtual {v0, v4, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    :catch_0
    :cond_2
    if-nez v3, :cond_3

    .line 27
    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/net/URL;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    :cond_3
    return-object v1

    :cond_4
    :goto_0
    return-object v0
.end method

.method private static b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;)Ljava/io/InputStream;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 28
    :cond_0
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;->a()Ljava/io/InputStream;

    move-result-object p0

    return-object p0
.end method

.method private c(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 1
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->u()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 6
    :cond_0
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->u()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 7
    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object p1

    .line 8
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object p1

    .line 9
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, ""

    :goto_0
    return-object p1

    :cond_1
    :goto_1
    const-string p1, ""

    return-object p1
.end method

.method private d(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->c(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->c(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Ljava/util/Map;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;
        }
    .end annotation

    .line 2
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->s()I

    move-result v0

    .line 4
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->p()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    move-result-object v1

    int-to-long v2, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 5
    invoke-virtual {v1, v2, v3, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->a(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 6
    invoke-virtual {v0, v2, v3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->b(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 7
    invoke-virtual {v0, v2, v3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->c(JLjava/util/concurrent/TimeUnit;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    move-result-object v0

    const/4 v1, 0x1

    .line 8
    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->a(Z)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    move-result-object v0

    .line 9
    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->b(Z)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v$b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;

    move-result-object v0

    .line 12
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 18
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->d(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V

    .line 21
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->v()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 22
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->v()Ljava/lang/String;

    move-result-object v3

    const-string v4, "User-Agent"

    .line 23
    invoke-virtual {v2, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    move-result-object v4

    const-string v5, "User-Agent"

    .line 24
    invoke-virtual {v4, v5, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    .line 28
    :cond_0
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->g()Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 30
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 31
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 35
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 37
    invoke-interface {p2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->b(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;

    goto :goto_1

    .line 42
    :cond_2
    invoke-static {v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V

    .line 44
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y$a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;

    move-result-object p2

    .line 45
    invoke-virtual {v0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/v;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/y;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;

    move-result-object p2

    .line 46
    invoke-interface {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/e;->B()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;

    move-result-object p2

    .line 48
    invoke-static {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/k;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/k;

    move-result-object v0

    .line 49
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;

    move-result-object v2

    const/4 v3, 0x0

    .line 54
    :try_start_0
    iget v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/bykvm_19do/bykvm_for12/k;->b:I

    const/4 v4, -0x1

    if-eq v0, v4, :cond_4

    .line 59
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->i()I

    move-result p1

    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->a(II)Z

    move-result p1

    if-nez p1, :cond_3

    .line 60
    new-instance p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;

    move-result-object p2

    invoke-static {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p1, v0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;-><init>(ILjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 72
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;->close()V

    return-object p1

    .line 73
    :cond_3
    :try_start_1
    new-instance p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;

    .line 75
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/a0;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;

    move-result-object p2

    invoke-static {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/r;)Ljava/util/List;

    move-result-object p2

    .line 76
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;->b()J

    move-result-wide v3

    long-to-int v3, v3

    new-instance v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l$a;

    invoke-direct {v4, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/l$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;)V

    invoke-direct {p1, v0, p2, v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;-><init>(ILjava/util/List;ILjava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object p1

    :catchall_0
    move-exception p1

    const/4 v3, 0x1

    goto :goto_2

    .line 77
    :cond_4
    :try_start_2
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Could not retrieve response code from HttpUrlConnection."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p1

    :goto_2
    if-nez v3, :cond_5

    .line 93
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/bykvm_if122/b0;->close()V

    .line 95
    :cond_5
    throw p1

    .line 96
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "request params maybe null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
