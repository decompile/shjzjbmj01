.class Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;
.super Ljava/lang/Thread;
.source "CacheDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;
    }
.end annotation


# static fields
.field private static final g:Z


# instance fields
.field private final a:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

.field private final d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

.field private volatile e:Z

.field private final f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->b:Z

    sput-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->g:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/BlockingQueue<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;>;",
            "Ljava/util/concurrent/BlockingQueue<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->e:Z

    .line 23
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->a:Ljava/util/concurrent/BlockingQueue;

    .line 24
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->b:Ljava/util/concurrent/BlockingQueue;

    .line 25
    iput-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

    .line 26
    iput-object p4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    .line 27
    new-instance p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;

    invoke-direct {p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;)V

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;

    return-void
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;)Ljava/util/concurrent/BlockingQueue;
    .locals 0

    .line 1
    iget-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->b:Ljava/util/concurrent/BlockingQueue;

    return-object p0
.end method

.method static synthetic b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;
    .locals 0

    .line 1
    iget-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    return-object p0
.end method

.method private b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    .line 3
    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->e:Z

    .line 3
    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    return-void
.end method

.method a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V
    .locals 6
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const-string v0, "cache-queue-take"

    .line 4
    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 5
    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    const/4 v1, 0x2

    .line 9
    :try_start_0
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->x()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "cache-discard-canceled"

    .line 10
    invoke-virtual {p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    return-void

    .line 83
    :cond_0
    :try_start_1
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, "cache-miss"

    .line 85
    invoke-virtual {p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 87
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;

    invoke-static {v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 88
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2, p1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    :cond_1
    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    return-void

    .line 151
    :cond_2
    :try_start_2
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "cache-hit-expired"

    .line 152
    invoke-virtual {p1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 153
    invoke-virtual {p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    .line 154
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;

    invoke-static {v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 155
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2, p1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 207
    :cond_3
    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    return-void

    :cond_4
    :try_start_3
    const-string v3, "cache-hit"

    .line 208
    invoke-virtual {p1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 209
    new-instance v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;

    iget-object v4, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->b:[B

    iget-object v5, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->h:Ljava/util/Map;

    invoke-direct {v3, v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;-><init>([BLjava/util/Map;)V

    .line 210
    invoke-virtual {p1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    move-result-object v3

    const-string v4, "cache-hit-parsed"

    .line 212
    invoke-virtual {p1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 214
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->b()Z

    move-result v4

    if-nez v4, :cond_5

    .line 216
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    invoke-interface {v2, p1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;)V

    goto :goto_0

    :cond_5
    const-string v4, "cache-hit-refresh-needed"

    .line 221
    invoke-virtual {p1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 222
    invoke-virtual {p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    .line 224
    iput-boolean v0, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->d:Z

    .line 226
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;

    invoke-static {v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 229
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    new-instance v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$a;

    invoke-direct {v4, p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V

    invoke-interface {v2, p1, v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 246
    :cond_6
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    invoke-interface {v2, p1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 254
    :goto_0
    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v2

    const-string v3, "CacheDispatcher Unhandled Throwable %s"

    .line 255
    :try_start_4
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    invoke-static {v2, v3, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;

    invoke-direct {v0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;-><init>(Ljava/lang/Throwable;)V

    .line 257
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    invoke-interface {v2, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 259
    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    :goto_1
    return-void

    :catchall_1
    move-exception v0

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(I)V

    .line 260
    throw v0
.end method

.method public run()V
    .locals 3

    .line 1
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->g:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "start new dispatcher"

    invoke-static {v2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const/16 v0, 0xa

    .line 2
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 5
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;->a()V

    .line 9
    :goto_0
    :try_start_0
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->b()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 12
    :catch_0
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->e:Z

    if-eqz v0, :cond_1

    .line 13
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    return-void

    .line 16
    :cond_1
    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "Ignoring spurious interrupt of CacheDispatcher thread; use quit() to terminate it"

    invoke-static {v2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
