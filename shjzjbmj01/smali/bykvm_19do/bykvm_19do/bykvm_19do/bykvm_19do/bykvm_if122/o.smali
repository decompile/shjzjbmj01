.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;
.super Ljava/lang/Object;
.source "RequestQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o$a;,
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

.field private final f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;

.field private final g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

.field private final h:[Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;

.field private i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o$b;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;)V
    .locals 1

    const/4 v0, 0x4

    .line 77
    invoke-direct {p0, p1, p2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;I)V

    return-void
.end method

.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;I)V
    .locals 3

    .line 71
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;

    new-instance v1, Landroid/os/Handler;

    .line 75
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;-><init>(Landroid/os/Handler;)V

    .line 76
    invoke-direct {p0, p1, p2, p3, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;ILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;)V

    return-void
.end method

.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;ILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 8
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->b:Ljava/util/Set;

    .line 13
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 18
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->j:Ljava/util/List;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->k:Ljava/util/List;

    .line 67
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

    .line 68
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;

    .line 69
    new-array p1, p3, [Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->h:[Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;

    .line 70
    iput-object p4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    return v0
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "TT;>;)",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "TT;>;"
        }
    .end annotation

    .line 2
    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->c(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V

    .line 3
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->B()V

    .line 4
    invoke-virtual {p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    .line 5
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->b:Ljava/util/Set;

    monitor-enter v0

    .line 6
    :try_start_0
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 7
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    const-string v0, "add-to-queue"

    .line 11
    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 12
    invoke-virtual {p0, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;I)V

    .line 15
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->C()Z

    move-result v0

    if-nez v0, :cond_0

    .line 16
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    return-object p1

    .line 19
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    return-object p1

    :catchall_0
    move-exception p1

    .line 20
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;I)V"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->k:Ljava/util/List;

    monitor-enter v0

    .line 22
    :try_start_0
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o$a;

    .line 23
    invoke-interface {v2, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;I)V

    goto :goto_0

    .line 25
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public b()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->c()V

    .line 3
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

    iget-object v4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    invoke-direct {v0, v1, v2, v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;-><init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;)V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;

    .line 4
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x0

    .line 7
    :goto_0
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->h:[Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 8
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->f:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;

    iget-object v4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;

    iget-object v5, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->g:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;

    invoke-direct {v1, v2, v3, v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;-><init>(Ljava/util/concurrent/BlockingQueue;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;)V

    .line 10
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->h:[Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;

    aput-object v1, v2, v0

    .line 11
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "TT;>;)V"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->b:Ljava/util/Set;

    monitor-enter v0

    .line 13
    :try_start_0
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 14
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 15
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->j:Ljava/util/List;

    monitor-enter v1

    .line 16
    :try_start_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o$b;

    .line 17
    invoke-interface {v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o$b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V

    goto :goto_0

    .line 19
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x5

    .line 20
    invoke-virtual {p0, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;I)V

    return-void

    :catchall_0
    move-exception p1

    .line 21
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    .line 22
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public c()V
    .locals 4

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/d;->a()V

    .line 4
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;->h:[Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    if-eqz v3, :cond_1

    .line 6
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/j;->a()V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public c(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "TT;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 7
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->u()Ljava/lang/String;

    move-result-object v0

    .line 11
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 12
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 13
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 14
    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->d(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method
