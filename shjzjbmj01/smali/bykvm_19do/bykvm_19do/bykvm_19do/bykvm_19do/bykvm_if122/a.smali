.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;
.super Ljava/lang/Object;
.source "BasicNetwork.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/c;


# static fields
.field protected static final c:Z


# instance fields
.field protected final a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

.field protected final b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->b:Z

    sput-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->c:Z

    return-void
.end method

.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;)V
    .locals 2

    .line 1
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;)V

    return-void
.end method

.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    .line 4
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;

    return-void
.end method

.method private static a(Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;",
            ">;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;",
            ")",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;",
            ">;"
        }
    .end annotation

    .line 453
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 457
    new-instance v1, Ljava/util/TreeSet;

    sget-object v2, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    if-eqz p0, :cond_0

    .line 458
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 459
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;

    .line 460
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 461
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_4

    .line 468
    iget-object p0, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->i:Ljava/util/List;

    if-eqz p0, :cond_2

    .line 469
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_4

    .line 470
    iget-object p0, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->i:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;

    .line 471
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 472
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 478
    :cond_2
    iget-object p0, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->h:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_4

    .line 479
    iget-object p0, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->h:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_3
    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map$Entry;

    .line 480
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 481
    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {v2, v3, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    return-object v0
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 380
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 383
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 385
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v2, "If-None-Match"

    .line 386
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    :cond_1
    iget-wide v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->e:J

    const-wide/16 v3, 0x0

    cmp-long p1, v1, v3

    if-lez p1, :cond_2

    .line 391
    invoke-static {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/b;->a(J)Ljava/lang/String;

    move-result-object p1

    const-string v1, "If-Modified-Since"

    .line 392
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v0
.end method

.method private a(JLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;[BI)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;[BI)V"
        }
    .end annotation

    .line 354
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->c:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0xbb8

    cmp-long v2, p1, v0

    if-lez v2, :cond_2

    :cond_0
    const-string v0, "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]"

    const/4 v1, 0x5

    .line 355
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    .line 359
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v1, p2

    const/4 p1, 0x2

    if-eqz p4, :cond_1

    array-length p2, p4

    .line 360
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    goto :goto_0

    :cond_1
    const-string p2, "null"

    :goto_0
    aput-object p2, v1, p1

    .line 361
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, v1, p2

    .line 362
    invoke-virtual {p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->q()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/e;

    move-result-object p1

    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/e;->b()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 p2, 0x4

    aput-object p1, v1, p2

    .line 363
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method private static a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;
        }
    .end annotation

    .line 364
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->q()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/e;

    move-result-object v0

    .line 365
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->s()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    .line 368
    :try_start_0
    invoke-interface {v0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/e;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V
    :try_end_0
    .catch Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h; {:try_start_0 .. :try_end_0} :catch_0

    .line 374
    new-array p2, v4, [Ljava/lang/Object;

    aput-object p0, p2, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, p2, v2

    const-string p0, "%s-retry [timeout=%s]"

    invoke-static {p0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception p2

    .line 375
    new-array v0, v4, [Ljava/lang/Object;

    aput-object p0, v0, v3

    .line 376
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v2

    const-string p0, "%s-timeout-giveup [timeout=%s]"

    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 377
    invoke-virtual {p1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 379
    throw p2
.end method

.method private a(Ljava/io/InputStream;I)[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;
        }
    .end annotation

    .line 406
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/c;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;

    invoke-direct {v0, v1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/c;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;I)V

    const/4 p2, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    .line 412
    :try_start_0
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;

    const/16 v3, 0x400

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;->a(I)[B

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 414
    :goto_0
    :try_start_1
    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 415
    invoke-virtual {v0, v2, p2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/c;->write([BII)V

    goto :goto_0

    .line 417
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_1

    .line 422
    :try_start_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 427
    :catch_0
    new-array p1, p2, [Ljava/lang/Object;

    const-string p2, "Error occurred when closing InputStream"

    invoke-static {p2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 429
    :cond_1
    :goto_1
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;

    invoke-virtual {p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;->a([B)V

    .line 430
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/c;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    move-object v4, v2

    move-object v2, v1

    move-object v1, v4

    goto :goto_2

    :catchall_1
    move-exception v2

    goto :goto_2

    .line 431
    :cond_2
    :try_start_3
    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;

    invoke-direct {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;-><init>()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_2
    if-eqz p1, :cond_3

    .line 443
    :try_start_4
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_3

    .line 448
    :catch_1
    new-array p1, p2, [Ljava/lang/Object;

    const-string p2, "Error occurred when closing InputStream"

    invoke-static {p2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    :cond_3
    :goto_3
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;

    invoke-virtual {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/b;->a([B)V

    .line 451
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/c;->close()V

    .line 452
    throw v2
.end method


# virtual methods
.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;)",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    .line 1
    :catchall_0
    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    .line 4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    .line 8
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;

    move-result-object v0

    invoke-direct {v7, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)Ljava/util/Map;

    move-result-object v0

    .line 9
    iget-object v3, v7, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    invoke-interface {v3, v8, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Ljava/util/Map;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;

    move-result-object v11
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    .line 10
    :try_start_1
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->d()I

    move-result v13

    .line 12
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->c()Ljava/util/List;

    move-result-object v12
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    const/16 v0, 0x130

    if-ne v13, v0, :cond_4

    .line 15
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;

    move-result-object v0

    if-nez v0, :cond_2

    .line 17
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;

    const/16 v15, 0x130

    const/16 v16, 0x0

    const/16 v17, 0x1

    .line 21
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const/4 v1, 0x0

    sub-long v18, v3, v9

    move-object v14, v0

    move-object/from16 v20, v12

    invoke-direct/range {v14 .. v20}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;-><init>(I[BZJLjava/util/List;)V
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    if-eqz v11, :cond_1

    .line 94
    :try_start_3
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 95
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    :cond_1
    return-object v0

    .line 96
    :cond_2
    :try_start_4
    invoke-static {v12, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a(Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;)Ljava/util/List;

    move-result-object v19

    .line 97
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;

    const/16 v14, 0x130

    iget-object v15, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/a$a;->b:[B

    const/16 v16, 0x1

    .line 101
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const/4 v0, 0x0

    sub-long v17, v3, v9

    move-object v13, v1

    invoke-direct/range {v13 .. v19}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;-><init>(I[BZJLjava/util/List;)V
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    if-eqz v11, :cond_3

    .line 165
    :try_start_5
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 166
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    :cond_3
    return-object v1

    :catch_0
    move-exception v0

    move-object v13, v2

    move-object v2, v11

    move-object/from16 v17, v12

    goto/16 :goto_3

    .line 167
    :cond_4
    :try_start_6
    invoke-virtual {v7, v8, v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;)[B

    move-result-object v19
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/net/MalformedURLException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 170
    :try_start_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const/4 v2, 0x0

    sub-long v2, v0, v9

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-object/from16 v5, v19

    move v6, v13

    .line 171
    invoke-direct/range {v1 .. v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a(JLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;[BI)V

    const/16 v0, 0xc8

    if-lt v13, v0, :cond_6

    const/16 v0, 0x12b

    if-gt v13, v0, :cond_6

    .line 176
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;

    const/4 v15, 0x0

    .line 180
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1
    :try_end_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/net/MalformedURLException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    const/4 v3, 0x0

    sub-long v16, v1, v9

    move-object v1, v12

    move-object v12, v0

    move-object/from16 v14, v19

    move-object/from16 v18, v1

    :try_start_8
    invoke-direct/range {v12 .. v18}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;-><init>(I[BZJLjava/util/List;)V
    :try_end_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/net/MalformedURLException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    if-eqz v11, :cond_5

    .line 227
    :try_start_9
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 228
    invoke-virtual {v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :catchall_3
    :cond_5
    return-object v0

    :cond_6
    move-object v1, v12

    .line 229
    :try_start_a
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_a
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/net/MalformedURLException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v1, v12

    :goto_1
    move-object/from16 v17, v1

    move-object v2, v11

    move-object/from16 v13, v19

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v1, v12

    goto :goto_2

    :catchall_4
    move-exception v0

    move-object v2, v11

    goto/16 :goto_a

    :catch_4
    move-exception v0

    :goto_2
    move-object/from16 v17, v1

    move-object v13, v2

    move-object v2, v11

    goto :goto_3

    :catch_5
    move-exception v0

    move-object v2, v11

    goto/16 :goto_7

    :catch_6
    move-object v2, v11

    goto/16 :goto_8

    :catchall_5
    move-exception v0

    goto/16 :goto_a

    :catch_7
    move-exception v0

    move-object/from16 v17, v1

    move-object v13, v2

    :goto_3
    if-eqz v2, :cond_e

    .line 244
    :try_start_b
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->d()I

    move-result v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    const-string v1, "Unexpected response code %d for %s"

    const/4 v3, 0x2

    .line 248
    :try_start_c
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual/range {p1 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->u()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v13, :cond_d

    .line 251
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;

    const/4 v14, 0x0

    .line 256
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const/4 v5, 0x0

    sub-long v15, v3, v9

    move-object v11, v1

    move v12, v0

    invoke-direct/range {v11 .. v17}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;-><init>(I[BZJLjava/util/List;)V

    const/16 v3, 0x191

    if-eq v0, v3, :cond_c

    const/16 v3, 0x193

    if-ne v0, v3, :cond_7

    goto :goto_5

    :cond_7
    const/16 v3, 0x190

    if-lt v0, v3, :cond_9

    const/16 v3, 0x1f3

    if-le v0, v3, :cond_8

    goto :goto_4

    .line 264
    :cond_8
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    invoke-direct {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;)V

    throw v0

    :cond_9
    :goto_4
    const/16 v3, 0x1f4

    if-lt v0, v3, :cond_b

    const/16 v3, 0x257

    if-gt v0, v3, :cond_b

    .line 266
    invoke-virtual/range {p1 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->D()Z

    move-result v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    if-eqz v0, :cond_a

    const-string v0, "server"

    .line 267
    :try_start_d
    new-instance v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;

    invoke-direct {v3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;)V

    invoke-static {v0, v8, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V

    goto :goto_6

    .line 270
    :cond_a
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;

    invoke-direct {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;)V

    throw v0

    .line 274
    :cond_b
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;

    invoke-direct {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;)V

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    :cond_c
    :goto_5
    const-string v0, "auth"

    .line 275
    :try_start_e
    new-instance v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;

    invoke-direct {v3, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/k;)V

    invoke-static {v0, v8, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    goto :goto_6

    :cond_d
    const-string v0, "network"

    .line 292
    :try_start_f
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/c;

    invoke-direct {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/c;-><init>()V

    invoke-static {v0, v8, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    :goto_6
    if-eqz v2, :cond_0

    .line 297
    :try_start_10
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_9

    .line 299
    :cond_e
    :try_start_11
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/d;

    invoke-direct {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/d;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_8
    move-exception v0

    .line 300
    :goto_7
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bad URL "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    :catch_9
    :goto_8
    const-string v0, "socket"

    .line 301
    :try_start_12
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/g;

    invoke-direct {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/g;-><init>()V

    invoke-static {v0, v8, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    if-eqz v2, :cond_0

    .line 345
    :try_start_13
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 346
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 298
    :goto_9
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_0

    :goto_a
    if-eqz v2, :cond_f

    .line 347
    :try_start_14
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 348
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    .line 353
    :catchall_6
    :cond_f
    throw v0
.end method

.method protected a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;",
            ")[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/f;
        }
    .end annotation

    .line 393
    instance-of v0, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;

    if-eqz v0, :cond_0

    .line 394
    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;)[B

    move-result-object p1

    return-object p1

    .line 399
    :cond_0
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->a()Ljava/io/InputStream;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 401
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/i;->b()I

    move-result p2

    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/a;->a(Ljava/io/InputStream;I)[B

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 405
    new-array p1, p1, [B

    :goto_0
    return-object p1
.end method
