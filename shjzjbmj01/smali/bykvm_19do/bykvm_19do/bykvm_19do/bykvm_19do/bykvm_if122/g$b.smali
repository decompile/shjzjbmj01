.class Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;
.super Ljava/lang/Object;
.source "ExecutorDelivery.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

.field private final b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

.field private final c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    .line 3
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    .line 4
    iput-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->c:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    const-string v1, "canceled-at-delivery"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->b(Ljava/lang/String;)V

    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->f()Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->e:Ljava/util/Map;

    .line 7
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->r()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->a(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    .line 8
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->j()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->b(J)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    .line 11
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13
    :try_start_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;)V

    goto :goto_0

    .line 18
    :cond_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :catchall_0
    :goto_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    iget-boolean v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->d:Z

    if-eqz v0, :cond_2

    .line 26
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    const-string v1, "intermediate-response"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 28
    :cond_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    const-string v1, "done"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->b(Ljava/lang/String;)V

    .line 32
    :goto_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 34
    :try_start_1
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    :cond_3
    return-void
.end method
