.class Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;
.super Ljava/lang/Object;
.source "ExecutorDelivery.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Ljava/util/concurrent/Executor;

.field private c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/c;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$a;

    invoke-direct {v0, p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;Landroid/os/Handler;)V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->a:Ljava/util/concurrent/Executor;

    .line 10
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->b:Ljava/util/concurrent/Executor;

    .line 11
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object p1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/c;

    return-void
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Ljava/util/concurrent/Executor;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;)",
            "Ljava/util/concurrent/Executor;"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 1
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->y()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->b:Ljava/util/concurrent/Executor;

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->a:Ljava/util/concurrent/Executor;

    :goto_1
    return-object p1
.end method


# virtual methods
.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;",
            ")V"
        }
    .end annotation

    const-string v0, "post-error"

    .line 10
    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 11
    invoke-static {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/h;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;

    move-result-object v0

    .line 12
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 13
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/c;

    if-eqz v0, :cond_0

    .line 14
    invoke-interface {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p<",
            "*>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, p2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;Ljava/lang/Runnable;)V

    .line 3
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/c;

    if-eqz v0, :cond_0

    .line 4
    invoke-interface {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;)V

    :cond_0
    return-void
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;Ljava/lang/Runnable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n<",
            "*>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p<",
            "*>;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .line 5
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->z()V

    const-string v0, "post-response"

    .line 6
    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Ljava/lang/String;)V

    .line 7
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;

    invoke-direct {v1, p1, p2, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g$b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 8
    iget-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/g;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/c;

    if-eqz p3, :cond_0

    .line 9
    invoke-interface {p3, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/c;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p;)V

    :cond_0
    return-void
.end method
