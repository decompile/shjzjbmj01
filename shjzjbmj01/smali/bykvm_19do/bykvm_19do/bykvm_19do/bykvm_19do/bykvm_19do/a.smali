.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;
.super Ljava/lang/Object;
.source "AppConfig.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/h$a;


# static fields
.field private static k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;


# instance fields
.field private final a:Z

.field private volatile b:Z

.field private c:Z

.field private d:Z

.field private e:J

.field private f:J

.field private g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final h:Landroid/content/Context;

.field private volatile i:Z

.field final j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b:Z

    const/4 v1, 0x1

    .line 3
    iput-boolean v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c:Z

    .line 4
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d:Z

    const-wide/16 v1, 0x0

    .line 5
    iput-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:J

    .line 6
    iput-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->f:J

    .line 8
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 11
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->i:Z

    .line 14
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/h;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/h;-><init>(Landroid/os/Looper;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/h$a;)V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/h;

    .line 36
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->h:Landroid/content/Context;

    .line 37
    iput-boolean p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a:Z

    return-void
.end method

.method public static a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;
    .locals 3

    .line 3
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    monitor-enter v0

    .line 4
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    if-nez v1, :cond_0

    .line 5
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->b(Landroid/content/Context;)Z

    move-result v1

    .line 6
    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v2, p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;-><init>(Landroid/content/Context;Z)V

    sput-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    .line 7
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;)V

    .line 9
    :cond_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    .line 10
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method private a(I)V
    .locals 4

    .line 76
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a()[Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x66

    if-eqz v0, :cond_3

    .line 77
    array-length v2, v0

    if-gt v2, p1, :cond_0

    goto :goto_1

    .line 81
    :cond_0
    aget-object v0, v0, p1

    .line 82
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    invoke-direct {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(I)V

    return-void

    .line 87
    :cond_1
    :try_start_0
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89
    invoke-direct {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(I)V

    return-void

    .line 92
    :cond_2
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 93
    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;

    new-instance v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a$c;

    invoke-direct {v3, p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a$c;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;I)V

    const/4 p1, 0x0

    invoke-direct {v2, p1, v0, v1, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;-><init>(ILjava/lang/String;Lorg/json/JSONObject;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p$a;)V

    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;-><init>()V

    const/16 v1, 0x2710

    .line 125
    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object p1

    invoke-virtual {v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/e;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    move-result-object p1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->h:Landroid/content/Context;

    .line 126
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "try app config exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "AppConfig"

    invoke-static {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    .line 129
    :cond_3
    :goto_1
    invoke-direct {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(I)V

    return-void
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(I)V

    return-void
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;Ljava/lang/Object;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method private a(Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 45
    instance-of v0, p1, Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 46
    check-cast p1, Ljava/lang/String;

    .line 47
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    .line 50
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "message"

    .line 51
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "success"

    .line 52
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    .line 55
    :cond_1
    instance-of v0, p1, Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 56
    move-object v0, p1

    check-cast v0, Lorg/json/JSONObject;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :cond_3
    :goto_0
    if-nez v0, :cond_4

    return v1

    :cond_4
    const-string p1, "data"

    .line 61
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    .line 62
    monitor-enter p0

    .line 63
    :try_start_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->h:Landroid/content/Context;

    const-string v2, "m_ss_app_config"

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, "m_last_refresh_time"

    .line 67
    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 69
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 70
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/e;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 74
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/e;->a(Lorg/json/JSONObject;)V

    :cond_5
    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p1

    .line 75
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 27
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/get_domains/v4/"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    move-result-object p1

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->h:Landroid/content/Context;

    invoke-interface {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a(Landroid/content/Context;)Landroid/location/Address;

    move-result-object p1

    .line 33
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    .line 34
    invoke-virtual {p1}, Landroid/location/Address;->hasLatitude()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/location/Address;->hasLongitude()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    invoke-virtual {p1}, Landroid/location/Address;->getLatitude()D

    move-result-wide v2

    const-string v0, "latitude"

    invoke-virtual {v1, v0, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;D)V

    .line 36
    invoke-virtual {p1}, Landroid/location/Address;->getLongitude()D

    move-result-wide v2

    const-string v0, "longitude"

    invoke-virtual {v1, v0, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;D)V

    .line 37
    invoke-virtual {p1}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object p1

    .line 38
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 39
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "city"

    invoke-virtual {v1, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    :cond_1
    iget-boolean p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b:Z

    if-eqz p1, :cond_2

    const-string p1, "force"

    const/4 v0, 0x1

    .line 42
    invoke-virtual {v1, p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;I)V

    .line 46
    :cond_2
    :try_start_0
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-ge p1, v0, :cond_3

    .line 47
    sget-object p1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    goto :goto_0

    .line 49
    :cond_3
    sget-object p1, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object p1, p1, v0

    :goto_0
    const-string v0, "abi"

    .line 51
    invoke-virtual {v1, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 54
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 56
    :goto_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    move-result-object p1

    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->c()I

    move-result p1

    const-string v0, "aid"

    invoke-virtual {v1, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;I)V

    .line 57
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    move-result-object p1

    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->b()Ljava/lang/String;

    move-result-object p1

    const-string v0, "device_platform"

    invoke-virtual {v1, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    move-result-object p1

    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->d()Ljava/lang/String;

    move-result-object p1

    const-string v0, "channel"

    invoke-virtual {v1, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    move-result-object p1

    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->e()I

    move-result p1

    const-string v0, "version_code"

    invoke-virtual {v1, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;I)V

    .line 62
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    move-result-object p1

    invoke-interface {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->f()Ljava/lang/String;

    move-result-object p1

    const-string v0, "custom_info_1"

    invoke-virtual {v1, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b(I)V
    .locals 1

    .line 25
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/h;

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .line 13
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    if-eqz v0, :cond_1

    .line 15
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->b(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    .line 16
    invoke-virtual {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(Z)V

    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d()V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(I)V

    return-void
.end method

.method private d(Z)V
    .locals 6

    .line 2
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d:Z

    if-eqz v0, :cond_0

    return-void

    .line 4
    :cond_0
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c:Z

    const-wide/16 v0, 0x0

    .line 6
    iput-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:J

    .line 7
    iput-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->f:J

    :cond_1
    if-eqz p1, :cond_2

    const-wide/32 v0, 0xa4cb80

    goto :goto_0

    :cond_2
    const-wide/32 v0, 0x2932e00

    .line 10
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 11
    iget-wide v4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:J

    sub-long v4, v2, v4

    cmp-long p1, v4, v0

    if-lez p1, :cond_4

    .line 12
    iget-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->f:J

    sub-long/2addr v2, v0

    const-wide/32 v0, 0x1d4c0

    cmp-long p1, v2, v0

    if-lez p1, :cond_4

    .line 13
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->h:Landroid/content/Context;

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->a(Landroid/content/Context;)Z

    move-result p1

    .line 14
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->i:Z

    if-eqz v0, :cond_3

    if-eqz p1, :cond_4

    .line 15
    :cond_3
    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(Z)Z

    :cond_4
    return-void
.end method

.method private e()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 2
    array-length v0, v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    invoke-direct {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(I)V

    :cond_1
    :goto_0
    return v1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 25
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p1

    .line 27
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 29
    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object p1, v0

    :catchall_0
    return-object p1

    .line 36
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a:Z

    if-eqz v0, :cond_2

    .line 37
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c()V

    goto :goto_0

    .line 39
    :cond_2
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b()V

    .line 41
    :goto_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object p1, v0

    :catchall_1
    return-object p1
.end method

.method public a(Z)Z
    .locals 4

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doRefresh: updating state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TNCManager"

    invoke-static {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "TNCManager"

    const-string v0, "doRefresh, already running"

    .line 13
    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :cond_0
    if-eqz p1, :cond_1

    .line 17
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->f:J

    .line 19
    :cond_1
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a$b;

    const-string v2, "AppConfigThread"

    invoke-direct {v0, p0, v2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a$b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;Ljava/lang/String;Z)V

    .line 24
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return v1
.end method

.method public a()[Ljava/lang/String;
    .locals 2

    .line 42
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    move-result-object v0

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 43
    array-length v1, v0

    if-gtz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 44
    new-array v0, v0, [Ljava/lang/String;

    :cond_1
    return-object v0
.end method

.method declared-synchronized b()V
    .locals 5

    monitor-enter p0

    .line 19
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 20
    iget-wide v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:J

    const/4 v4, 0x0

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 21
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :try_start_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 24
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/e;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/e;->c()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 18
    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Z)V
    .locals 4

    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a:Z

    if-eqz v0, :cond_0

    .line 3
    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d(Z)V

    goto :goto_0

    .line 5
    :cond_0
    iget-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-gtz p1, :cond_1

    .line 7
    :try_start_1
    new-instance p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a$a;

    const-string v0, "LoadDomainConfig4Other-Thread"

    invoke-direct {p1, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;Ljava/lang/String;)V

    .line 12
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    .line 1
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized c()V
    .locals 5

    monitor-enter p0

    .line 1
    :try_start_0
    iget-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 3
    :try_start_1
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->i:Z

    .line 4
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->h:Landroid/content/Context;

    const-string v1, "m_ss_app_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "m_last_refresh_time"

    const-wide/16 v2, 0x0

    .line 6
    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    move-wide v0, v2

    .line 11
    :cond_1
    iput-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:J

    .line 13
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/e;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 14
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/e;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/e;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 0
    monitor-exit p0

    throw v0
.end method

.method c(Z)V
    .locals 2

    const-string v0, "TNCManager"

    const-string v1, "doRefresh, actual request"

    .line 15
    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c()V

    const/4 v0, 0x1

    .line 18
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d:Z

    if-nez p1, :cond_0

    .line 20
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->j:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/h;

    const/16 v0, 0x66

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    .line 24
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 26
    :catch_0
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    return-void
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->b(Z)V

    return-void
.end method

.method public handleMsg(Landroid/os/Message;)V
    .locals 3

    .line 1
    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x0

    const/16 v1, 0x65

    if-eq p1, v1, :cond_2

    const/16 v1, 0x66

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 11
    :cond_0
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d:Z

    .line 12
    iget-boolean p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c:Z

    if-eqz p1, :cond_1

    .line 13
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d()V

    :cond_1
    const-string p1, "TNCManager"

    const-string v1, "doRefresh, error"

    .line 15
    invoke-static {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 17
    :cond_2
    iput-boolean v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d:Z

    .line 18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:J

    const-string p1, "TNCManager"

    const-string v1, "doRefresh, succ"

    .line 19
    invoke-static {p1, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    iget-boolean p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c:Z

    if-eqz p1, :cond_3

    .line 21
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d()V

    .line 22
    :cond_3
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    return-void
.end method
