.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;
.super Ljava/lang/Object;
.source "AdNetSdk.java"


# static fields
.field private static a:Ljava/lang/String; = null

.field public static b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/q; = null

.field private static c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b; = null

.field private static d:Z = true

.field private static e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;


# direct methods
.method public static a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;
    .locals 1

    .line 14
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    :try_start_0
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p0

    const-string v1, "VAdNetSdk"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 4
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    const/4 v0, 0x0

    .line 7
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "init adnetsdk default directory error "

    invoke-static {p0, v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/r;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    :cond_0
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a:Ljava/lang/String;

    return-object p0
.end method

.method public static a(Landroid/content/Context;Landroid/app/Application;Z)V
    .locals 1

    if-eqz p0, :cond_3

    .line 15
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;

    move-result-object p1

    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->b(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p1, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/f;->a(Landroid/content/Context;Z)V

    .line 16
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->c(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->b(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    if-eqz p2, :cond_1

    .line 17
    :cond_0
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c()V

    .line 18
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d()V

    .line 20
    :cond_1
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->b(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_2

    return-void

    .line 23
    :cond_2
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;

    .line 24
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c(Landroid/content/Context;)V

    return-void

    .line 25
    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "tryInitAdTTNet context is null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;)V
    .locals 0

    .line 13
    sput-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/a;

    return-void
.end method

.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;)V
    .locals 0

    .line 11
    sput-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    return-void
.end method

.method public static a(Z)V
    .locals 0

    .line 12
    sput-boolean p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d:Z

    return-void
.end method

.method public static b(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;
    .locals 0

    .line 1
    invoke-static {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/m;->a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    move-result-object p0

    return-object p0
.end method

.method public static b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;
    .locals 2

    .line 2
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;

    if-eqz v0, :cond_0

    return-object v0

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sITTNetDepend is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    .line 3
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a$a;

    const-string v1, "load_config"

    invoke-direct {v0, v1, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a$a;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 9
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public static c()Z
    .locals 1

    .line 1
    sget-boolean v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/a;->d:Z

    return v0
.end method
