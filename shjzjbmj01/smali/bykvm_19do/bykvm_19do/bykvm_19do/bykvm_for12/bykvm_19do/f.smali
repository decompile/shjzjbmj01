.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;
.super Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;
.source "TTInterSplashAdManager.java"

# interfaces
.implements Lcom/bytedance/msdk/adapter/listener/ITTAdapterSplashAdListener;


# instance fields
.field private J:Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;

.field private K:Lcom/bytedance/msdk/api/splash/TTSplashAdListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private x()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/base/TTBaseAd;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->p:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->p:Ljava/util/List;

    return-object v0

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    :goto_0
    const/16 v0, 0x39

    const/16 v1, 0x2c

    const/16 v2, 0x9

    const/4 v3, 0x1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v1, 0x3e

    mul-int v4, v1, v1

    const/16 v5, 0xa

    mul-int v6, v5, v5

    add-int/2addr v4, v6

    mul-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v4, v1

    if-gez v4, :cond_6

    :goto_1
    :pswitch_1
    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x14

    goto :goto_1

    :cond_2
    :goto_2
    :pswitch_2
    packed-switch v2, :pswitch_data_2

    :pswitch_3
    packed-switch v2, :pswitch_data_3

    :pswitch_4
    rsub-int/lit8 v0, v3, 0x44

    mul-int/lit8 v0, v0, 0x44

    const/16 v1, 0x44

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v3

    mul-int v0, v0, v1

    rem-int/lit8 v0, v0, 0x6

    if-eqz v0, :cond_3

    :pswitch_5
    const/16 v0, 0x24

    rsub-int/lit8 v1, v3, 0x24

    mul-int/lit8 v1, v1, 0x24

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v3

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    if-eqz v1, :cond_2

    :pswitch_6
    const/16 v0, 0x47

    mul-int v1, v0, v0

    const/16 v4, 0x25

    mul-int v5, v4, v4

    add-int/2addr v1, v5

    const/16 v5, 0x1b

    mul-int v6, v5, v5

    add-int/2addr v1, v6

    const/16 v6, 0x47

    mul-int/lit8 v6, v6, 0x25

    mul-int/lit8 v4, v4, 0x1b

    add-int/2addr v6, v4

    mul-int/lit8 v0, v0, 0x1b

    add-int/2addr v6, v0

    if-ge v1, v6, :cond_4

    goto :goto_3

    :cond_3
    :pswitch_7
    const/16 v0, 0x44

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x44

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    :cond_4
    :pswitch_8
    const/16 v0, 0x46

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x46

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    :cond_5
    :pswitch_9
    const/16 v0, 0x28

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x28

    const/4 v4, 0x5

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x5

    add-int/2addr v1, v5

    const/16 v5, 0x16

    mul-int v6, v5, v5

    mul-int/lit8 v6, v6, 0x16

    add-int/2addr v1, v6

    mul-int/lit8 v0, v0, 0x5

    mul-int/lit8 v0, v0, 0x16

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_6

    :pswitch_a
    const/16 v0, 0x15

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x15

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    goto :goto_2

    :cond_6
    :goto_3
    :pswitch_b
    const/4 v0, 0x0

    return-object v0

    :cond_7
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    return-object v0

    :cond_8
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    return-object v0

    :cond_9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->e()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/f;->c()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_a
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2c
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x12
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x8
        :pswitch_6
        :pswitch_b
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x2d
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)V
    .locals 11

    if-eqz p1, :cond_8

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->x()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v1, :cond_0

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getRit()Ljava/lang/String;

    move-result-object v1

    const-string v2, "show"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u5c55\u793a\u7684\u5e7f\u544a\u7c7b\u578b\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v1

    invoke-static {v1}, Lcom/bytedance/msdk/base/a;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",slotId\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",slotType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0, p1}, Lcom/bytedance/msdk/base/TTBaseAd;->showSplashAd(Landroid/view/ViewGroup;)V

    :cond_1
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->isServerBiddingAd()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->d(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    :cond_2
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {p1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->f(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->g:Landroid/os/Handler;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    const/4 v0, 0x3

    const/16 v1, 0x1c

    const/16 v2, 0xc

    const/16 v3, 0x12

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->isServerBiddingAd()Z

    move-result p1

    if-nez p1, :cond_8

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;

    move-result-object p1

    iget-object v4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v4}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->e(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_8

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;

    move-result-object v4

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->r:Ljava/util/Map;

    iget-object v8, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->H:Lcom/bytedance/msdk/api/TTNetworkRequestInfo;

    iget-object v9, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->v:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;

    iget-object v10, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->A:Landroid/content/Context;

    const/4 v7, 0x1

    invoke-virtual/range {v4 .. v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a(Ljava/lang/String;Ljava/util/Map;ZLcom/bytedance/msdk/api/TTNetworkRequestInfo;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;Landroid/content/Context;)V

    goto/16 :goto_7

    :cond_3
    :goto_0
    const/16 p1, 0x53

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :cond_4
    :pswitch_0
    const/16 p1, 0x20

    mul-int v4, p1, p1

    mul-int/lit8 v4, v4, 0x20

    mul-int v5, v3, v3

    mul-int/lit8 v5, v5, 0x12

    add-int/2addr v4, v5

    const/16 v5, 0xa

    mul-int v6, v5, v5

    mul-int/lit8 v6, v6, 0xa

    add-int/2addr v4, v6

    mul-int/lit8 p1, p1, 0x12

    mul-int/lit8 p1, p1, 0xa

    mul-int/lit8 p1, p1, 0x3

    if-ge v4, p1, :cond_4

    goto/16 :goto_7

    :pswitch_1
    const/16 p1, 0x9

    mul-int p1, p1, p1

    mul-int v4, v1, v1

    mul-int/lit8 v4, v4, 0x22

    sub-int/2addr p1, v4

    const/4 v4, -0x1

    if-ne p1, v4, :cond_5

    goto/16 :goto_8

    :cond_5
    :pswitch_2
    const/4 p1, 0x7

    mul-int v4, p1, p1

    mul-int/lit8 v4, v4, 0x7

    const/16 v5, 0x1a

    mul-int v6, v5, v5

    mul-int/lit8 v6, v6, 0x1a

    add-int/2addr v4, v6

    const/16 v6, 0x11

    mul-int v7, v6, v6

    mul-int/lit8 v7, v7, 0x11

    add-int/2addr v4, v7

    mul-int/lit8 p1, p1, 0x1a

    mul-int/lit8 p1, p1, 0x11

    mul-int/lit8 p1, p1, 0x3

    if-ge v4, p1, :cond_6

    goto :goto_5

    :cond_6
    :goto_1
    :pswitch_3
    const/16 p1, 0x24

    :goto_2
    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :goto_3
    :pswitch_4
    const/16 p1, 0x59

    packed-switch p1, :pswitch_data_2

    goto :goto_3

    :pswitch_5
    mul-int p1, v3, v3

    const/16 v4, 0xf

    mul-int v5, v4, v4

    add-int/2addr p1, v5

    mul-int v5, v1, v1

    add-int/2addr p1, v5

    const/16 v5, 0x12

    mul-int/lit8 v5, v5, 0xf

    mul-int/lit8 v4, v4, 0x1c

    add-int/2addr v5, v4

    mul-int/lit8 v4, v3, 0x1c

    add-int/2addr v5, v4

    if-ge p1, v5, :cond_7

    :pswitch_6
    const/4 p1, 0x3

    :goto_4
    packed-switch p1, :pswitch_data_3

    goto :goto_6

    :goto_5
    :pswitch_7
    mul-int p1, v2, v2

    const/16 v4, 0xd

    mul-int v5, v4, v4

    add-int/2addr p1, v5

    const/16 v5, 0x14

    mul-int v6, v5, v5

    add-int/2addr p1, v6

    const/16 v6, 0xc

    mul-int/lit8 v6, v6, 0xd

    mul-int/lit8 v4, v4, 0x14

    add-int/2addr v6, v4

    const/16 v4, 0xc

    mul-int/lit8 v4, v4, 0x14

    add-int/2addr v6, v4

    if-ge p1, v6, :cond_6

    :pswitch_8
    const/4 p1, 0x2

    mul-int v4, p1, p1

    const/16 v5, 0x1f

    mul-int v6, v5, v5

    add-int/2addr v4, v6

    mul-int v6, v2, v2

    add-int/2addr v4, v6

    const/4 v6, 0x2

    mul-int/lit8 v6, v6, 0x1f

    mul-int/lit8 v5, v5, 0xc

    add-int/2addr v6, v5

    mul-int/lit8 p1, p1, 0xc

    add-int/2addr v6, p1

    if-ge v4, v6, :cond_6

    goto :goto_8

    :goto_6
    const/16 p1, 0x4f

    goto :goto_4

    :cond_7
    :goto_7
    const/16 p1, 0x26

    goto :goto_2

    :cond_8
    :goto_8
    :pswitch_9
    return-void

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_1
        :pswitch_9
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x24
        :pswitch_9
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x57
        :pswitch_5
        :pswitch_6
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x4f
        :pswitch_2
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected a(Lcom/bytedance/msdk/api/AdError;)V
    .locals 3

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->J:Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;

    if-eqz v0, :cond_1

    iget v1, p1, Lcom/bytedance/msdk/api/AdError;->code:I

    const/16 v2, 0xfab

    if-ne v1, v2, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;->onAdLoadTimeout()V

    goto :goto_0

    :cond_0
    invoke-interface {v0, p1}, Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;->onSplashAdLoadFail(Lcom/bytedance/msdk/api/AdError;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/TTNetworkRequestInfo;Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;I)V
    .locals 1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    if-eqz p1, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/api/AdSlot;->setAdType(I)V

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/api/AdSlot;->setAdCount(I)V

    :cond_0
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->H:Lcom/bytedance/msdk/api/TTNetworkRequestInfo;

    iput-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->J:Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->r:Ljava/util/Map;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string p3, "ad_load_timeout"

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->B:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->t()V

    return-void
.end method

.method public a(Lcom/bytedance/msdk/api/splash/TTSplashAdListener;)V
    .locals 0

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->K:Lcom/bytedance/msdk/api/splash/TTSplashAdListener;

    return-void
.end method

.method public m()V
    .locals 1

    invoke-super {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->m()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->K:Lcom/bytedance/msdk/api/splash/TTSplashAdListener;

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->J:Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;

    return-void
.end method

.method public n()I
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getLoadSort()I

    move-result v0

    const/4 v1, -0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v0

    return v0

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->h()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, -0x3

    return v0

    :cond_1
    invoke-super {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->n()I

    move-result v0

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getLoadSort()I

    move-result v0

    const/4 v1, -0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->h()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "-3"

    return-object v0

    :cond_1
    invoke-super {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onAdClicked()V
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->K:Lcom/bytedance/msdk/api/splash/TTSplashAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/splash/TTSplashAdListener;->onAdClicked()V

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->e(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    return-void
.end method

.method public onAdDismiss()V
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->K:Lcom/bytedance/msdk/api/splash/TTSplashAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/splash/TTSplashAdListener;->onAdDismiss()V

    :cond_0
    return-void
.end method

.method public onAdShow()V
    .locals 3

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->K:Lcom/bytedance/msdk/api/splash/TTSplashAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/splash/TTSplashAdListener;->onAdShow()V

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    const-string v2, "show_listen"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "adSlotId\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\uff0c\u5e7f\u544a\u7c7b\u578b\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v1

    invoke-static {v1}, Lcom/bytedance/msdk/base/a;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->g(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/f;->b(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void
.end method

.method public onAdSkip()V
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->K:Lcom/bytedance/msdk/api/splash/TTSplashAdListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/splash/TTSplashAdListener;->onAdSkip()V

    :cond_0
    return-void
.end method

.method public p()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getLoadSort()I

    move-result v0

    const/4 v1, -0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getNetWorkPlatFormCpm()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->h()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "-3"

    return-object v0

    :cond_1
    invoke-super {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected q()V
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/f;->J:Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/splash/TTSplashAdLoadCallback;->onSplashAdLoadSuccess()V

    :cond_0
    return-void
.end method

.method protected r()V
    .locals 0

    return-void
.end method
