.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;
.super Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;
.source "TTInterBannerAdManager.java"

# interfaces
.implements Lcom/bytedance/msdk/adapter/listener/ITTAdapterBannerAdListener;


# instance fields
.field private J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

.field private K:Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private y()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/base/TTBaseAd;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->p:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->p:Ljava/util/List;

    return-object v0

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    :goto_0
    const/16 v0, 0x39

    const/16 v1, 0x2c

    const/16 v2, 0x9

    const/4 v3, 0x1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v1, 0x3e

    mul-int v4, v1, v1

    const/16 v5, 0xa

    mul-int v6, v5, v5

    add-int/2addr v4, v6

    mul-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v4, v1

    if-gez v4, :cond_6

    :goto_1
    :pswitch_1
    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x14

    goto :goto_1

    :cond_2
    :goto_2
    :pswitch_2
    packed-switch v2, :pswitch_data_2

    :pswitch_3
    packed-switch v2, :pswitch_data_3

    :pswitch_4
    rsub-int/lit8 v0, v3, 0x44

    mul-int/lit8 v0, v0, 0x44

    const/16 v1, 0x44

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v3

    mul-int v0, v0, v1

    rem-int/lit8 v0, v0, 0x6

    if-eqz v0, :cond_3

    :pswitch_5
    const/16 v0, 0x24

    rsub-int/lit8 v1, v3, 0x24

    mul-int/lit8 v1, v1, 0x24

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v3

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    if-eqz v1, :cond_2

    :pswitch_6
    const/16 v0, 0x47

    mul-int v1, v0, v0

    const/16 v4, 0x25

    mul-int v5, v4, v4

    add-int/2addr v1, v5

    const/16 v5, 0x1b

    mul-int v6, v5, v5

    add-int/2addr v1, v6

    const/16 v6, 0x47

    mul-int/lit8 v6, v6, 0x25

    mul-int/lit8 v4, v4, 0x1b

    add-int/2addr v6, v4

    mul-int/lit8 v0, v0, 0x1b

    add-int/2addr v6, v0

    if-ge v1, v6, :cond_4

    goto :goto_3

    :cond_3
    :pswitch_7
    const/16 v0, 0x44

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x44

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    :cond_4
    :pswitch_8
    const/16 v0, 0x46

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x46

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    :cond_5
    :pswitch_9
    const/16 v0, 0x28

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x28

    const/4 v4, 0x5

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x5

    add-int/2addr v1, v5

    const/16 v5, 0x16

    mul-int v6, v5, v5

    mul-int/lit8 v6, v6, 0x16

    add-int/2addr v1, v6

    mul-int/lit8 v0, v0, 0x5

    mul-int/lit8 v0, v0, 0x16

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_6

    :pswitch_a
    const/16 v0, 0x15

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x15

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    goto :goto_2

    :cond_6
    :goto_3
    :pswitch_b
    const/4 v0, 0x0

    return-object v0

    :cond_7
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    return-object v0

    :cond_8
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    return-object v0

    :cond_9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->e()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/f;->c()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_a
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2c
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x12
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x8
        :pswitch_6
        :pswitch_b
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x2d
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method protected a(Lcom/bytedance/msdk/api/AdError;)V
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->K:Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;->onAdFailedToLoad(Lcom/bytedance/msdk/api/AdError;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;)V
    .locals 1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/api/AdSlot;->setAdType(I)V

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p1, v0}, Lcom/bytedance/msdk/api/AdSlot;->setAdCount(I)V

    :cond_0
    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->K:Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;

    iput-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->B:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->t()V

    return-void
.end method

.method public a(Lcom/bytedance/msdk/api/banner/TTAdBannerListener;)V
    .locals 0

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

    return-void
.end method

.method public b(I)V
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->r:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "refresh_time"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b(Z)V
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->r:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string v1, "allow_show_close_btn"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public m()V
    .locals 1

    invoke-super {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->m()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->K:Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;

    return-void
.end method

.method public onAdClicked()V
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/banner/TTAdBannerListener;->onAdClicked()V

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->e(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    return-void
.end method

.method public onAdClosed()V
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/banner/TTAdBannerListener;->onAdClosed()V

    :cond_0
    return-void
.end method

.method public onAdLeftApplication()V
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/banner/TTAdBannerListener;->onAdLeftApplication()V

    :cond_0
    return-void
.end method

.method public onAdOpened()V
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/banner/TTAdBannerListener;->onAdOpened()V

    :cond_0
    return-void
.end method

.method public onAdShow()V
    .locals 3

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/banner/TTAdBannerListener;->onAdShow()V

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    const-string v2, "show_listen"

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "adSlotId\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\uff0c\u5e7f\u544a\u7c7b\u578b\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v1

    invoke-static {v1}, Lcom/bytedance/msdk/base/a;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->g(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/f;->b(Lcom/bytedance/msdk/base/TTBaseAd;)V

    return-void
.end method

.method protected q()V
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->K:Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/bytedance/msdk/api/banner/TTAdBannerLoadCallBack;->onAdLoaded()V

    :cond_0
    return-void
.end method

.method protected r()V
    .locals 0

    return-void
.end method

.method public x()Landroid/view/View;
    .locals 12

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdView()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->y()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v2, :cond_1

    iput-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->getRit()Ljava/lang/String;

    move-result-object v2

    const-string v3, "show"

    invoke-static {v2, v3}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\u5c55\u793a\u7684\u5e7f\u544a\u7c7b\u578b\uff1a"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v2

    invoke-static {v2}, Lcom/bytedance/msdk/base/a;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",slotId\uff1a"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",slotType:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotType()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "TTMediationSDK"

    invoke-static {v2, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdView()Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->hasDislike()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->A:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    new-instance v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b$a;

    invoke-direct {v4, p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;Landroid/view/View;)V

    invoke-virtual {v2, v3, v4}, Lcom/bytedance/msdk/base/TTBaseAd;->setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/msdk/api/TTDislikeCallback;)V

    :cond_2
    if-eqz v0, :cond_1

    :cond_3
    const/4 v1, 0x2

    if-eqz v0, :cond_8

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v2

    if-ne v2, v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->getRit()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v3}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "mTTAdBannerListener-->Admob--->onAdShow......"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ADMOB_EVENT"

    invoke-static {v2, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_19do/b;->J:Lcom/bytedance/msdk/api/banner/TTAdBannerListener;

    invoke-interface {v1}, Lcom/bytedance/msdk/api/banner/TTAdBannerListener;->onAdShow()V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    const-string v3, "show_listen"

    invoke-static {v2, v3}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagThirdLevelByEvent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "adSlotId\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\uff0c\u5e7f\u544a\u7c7b\u578b\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v2

    invoke-static {v2}, Lcom/bytedance/msdk/base/a;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TTMediationSDK"

    invoke-static {v2, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->g(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/f;->b(Lcom/bytedance/msdk/base/TTBaseAd;)V

    :cond_5
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->isServerBiddingAd()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->d(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    :cond_6
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->f(Lcom/bytedance/msdk/base/TTBaseAd;Lcom/bytedance/msdk/api/AdSlot;)V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->g:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->isServerBiddingAd()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;

    move-result-object v1

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->e(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;

    move-result-object v2

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->C:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->r:Ljava/util/Map;

    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->H:Lcom/bytedance/msdk/api/TTNetworkRequestInfo;

    iget-object v7, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->v:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;

    iget-object v8, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->A:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-virtual/range {v2 .. v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a(Ljava/lang/String;Ljava/util/Map;ZLcom/bytedance/msdk/api/TTNetworkRequestInfo;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/g;Landroid/content/Context;)V

    :cond_7
    return-object v0

    :cond_8
    :goto_0
    const/4 v0, 0x6

    const/16 v2, 0x30

    const/16 v3, 0x1e

    const/16 v4, 0x22

    const/16 v5, 0x1b

    const/16 v6, 0x2c

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v7, 0x2c

    goto/16 :goto_4

    :cond_9
    :goto_1
    :pswitch_1
    const/16 v7, 0x9

    packed-switch v6, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    packed-switch v7, :pswitch_data_2

    goto/16 :goto_5

    :goto_2
    :pswitch_3
    packed-switch v7, :pswitch_data_3

    goto/16 :goto_7

    :cond_a
    :goto_3
    :pswitch_4
    mul-int v7, v0, v0

    mul-int v8, v3, v3

    add-int/2addr v7, v8

    const/4 v8, 0x6

    mul-int/lit8 v8, v8, 0x1e

    mul-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    if-gez v7, :cond_c

    :cond_b
    :pswitch_5
    const/16 v7, 0x52

    mul-int v8, v7, v7

    mul-int/lit8 v8, v8, 0x52

    mul-int v9, v5, v5

    mul-int/lit8 v9, v9, 0x1b

    add-int/2addr v8, v9

    const/16 v9, 0xe

    mul-int v10, v9, v9

    mul-int/lit8 v10, v10, 0xe

    add-int/2addr v8, v10

    mul-int/lit8 v7, v7, 0x1b

    mul-int/lit8 v7, v7, 0xe

    mul-int/lit8 v7, v7, 0x3

    if-ge v8, v7, :cond_9

    :pswitch_6
    const/16 v7, 0x31

    mul-int v8, v7, v7

    mul-int/lit8 v8, v8, 0x31

    mul-int v9, v6, v6

    mul-int/lit8 v9, v9, 0x2c

    add-int/2addr v8, v9

    const/16 v9, 0x14

    mul-int v10, v9, v9

    mul-int/lit8 v10, v10, 0x14

    add-int/2addr v8, v10

    mul-int/lit8 v7, v7, 0x2c

    mul-int/lit8 v7, v7, 0x14

    mul-int/lit8 v7, v7, 0x3

    if-ge v8, v7, :cond_b

    :pswitch_7
    mul-int v7, v3, v3

    const/16 v8, 0x12

    mul-int/lit8 v8, v8, 0x12

    mul-int/lit8 v8, v8, 0x22

    sub-int/2addr v7, v8

    const/4 v8, -0x1

    if-ne v7, v8, :cond_c

    :pswitch_8
    const/4 v7, 0x1

    const/16 v8, 0x55

    rsub-int/lit8 v7, v7, 0x55

    mul-int/lit8 v7, v7, 0x55

    mul-int/lit8 v8, v8, 0x2

    add-int/lit8 v8, v8, -0x1

    mul-int v7, v7, v8

    rem-int/2addr v7, v0

    if-eqz v7, :cond_c

    :pswitch_9
    const/16 v7, 0x38

    mul-int/lit8 v7, v7, 0x38

    mul-int v8, v5, v5

    mul-int/lit8 v8, v8, 0x22

    sub-int/2addr v7, v8

    const/4 v8, -0x1

    if-ne v7, v8, :cond_a

    :cond_c
    const/16 v7, 0x57

    :goto_4
    const/16 v8, 0x57

    if-eq v7, v8, :cond_d

    goto :goto_6

    :cond_d
    :goto_5
    mul-int v7, v2, v2

    mul-int v8, v4, v4

    add-int/2addr v7, v8

    mul-int/lit8 v8, v2, 0x22

    mul-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    if-gez v7, :cond_e

    :goto_6
    const/16 v7, 0x48

    mul-int v8, v7, v7

    const/16 v9, 0x1c

    mul-int v10, v9, v9

    add-int/2addr v8, v10

    const/16 v10, 0xd

    mul-int v11, v10, v10

    add-int/2addr v8, v11

    const/16 v11, 0x48

    mul-int/lit8 v11, v11, 0x1c

    mul-int/lit8 v9, v9, 0xd

    add-int/2addr v11, v9

    mul-int/lit8 v7, v7, 0xd

    add-int/2addr v11, v7

    if-ge v8, v11, :cond_c

    goto/16 :goto_3

    :goto_7
    const/16 v7, 0x41

    goto/16 :goto_2

    :cond_e
    :pswitch_a
    const/4 v0, 0x0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x2f
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2c
        :pswitch_a
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x16
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x40
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
