.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;
.super Ljava/lang/Object;
.source "InternalContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a$b;
    }
.end annotation


# static fields
.field private static volatile a:Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field

.field private static volatile b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

.field private static volatile c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;

    if-nez v0, :cond_1

    .line 2
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;

    monitor-enter v0

    .line 3
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;

    if-nez v1, :cond_0

    .line 4
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;

    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/b;

    sget-object v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/b;-><init>(Landroid/content/Context;)V

    .line 6
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;

    move-result-object v3

    .line 7
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    move-result-object v4

    sget-object v5, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a:Landroid/content/Context;

    .line 8
    invoke-static {v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_19do/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;)V

    sput-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;

    .line 11
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 13
    :cond_1
    :goto_0
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/b;

    return-object v0
.end method

.method private static a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$a;
    .locals 1

    .line 14
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a$a;

    invoke-direct {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a$a;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static b()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;
    .locals 1

    .line 17
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/e$b;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;)V
    .locals 3

    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;

    monitor-enter v0

    .line 1
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a:Landroid/content/Context;

    if-nez v1, :cond_2

    .line 2
    const-class v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 3
    :try_start_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a$b;->a()Landroid/app/Application;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_0

    .line 5
    :try_start_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a$b;->a()Landroid/app/Application;

    move-result-object v2

    sput-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a:Landroid/content/Context;

    .line 6
    sget-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a:Landroid/content/Context;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_0

    .line 7
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit v0

    return-void

    :catchall_0
    :cond_0
    if-eqz p0, :cond_1

    .line 14
    :try_start_4
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sput-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a:Landroid/content/Context;

    .line 16
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception p0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_2
    :goto_0
    monitor-exit v0

    return-void

    :catchall_2
    move-exception p0

    .line 0
    monitor-exit v0

    throw p0
.end method

.method public static c()Landroid/content/Context;
    .locals 1

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->b(Landroid/content/Context;)V

    .line 4
    :cond_0
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method public static d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/a;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;

    if-nez v0, :cond_1

    .line 2
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;

    monitor-enter v0

    .line 3
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;

    if-nez v1, :cond_0

    .line 4
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b;

    sget-object v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/b;-><init>(Landroid/content/Context;)V

    sput-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;

    .line 6
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 8
    :cond_1
    :goto_0
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/bykvm_if122/a;

    return-object v0
.end method

.method public static e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    if-nez v0, :cond_1

    .line 2
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    monitor-enter v0

    .line 3
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    if-nez v1, :cond_0

    .line 4
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    invoke-direct {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;-><init>()V

    sput-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    .line 6
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 8
    :cond_1
    :goto_0
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    return-object v0
.end method
