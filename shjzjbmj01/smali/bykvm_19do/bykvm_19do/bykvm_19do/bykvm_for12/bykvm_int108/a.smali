.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;
.super Ljava/lang/Object;
.source "TTAdapterConfigurationManager.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a:Ljava/util/Map;

    .line 5
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->b:Ljava/util/List;

    return-void
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 18
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->b:Ljava/util/List;

    monitor-enter v0

    .line 19
    :try_start_0
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 20
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private b(Landroid/content/Context;Ljava/util/Map;)Lorg/json/JSONObject;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/bytedance/msdk/adapter/util/Preconditions;->checkNotNull(Ljava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a:Ljava/util/Map;

    .line 4
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    .line 9
    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;

    .line 11
    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;->getBiddingToken(Landroid/content/Context;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    .line 12
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    .line 18
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    move-object v2, v4

    .line 20
    :cond_2
    invoke-interface {v1}, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;->getAdNetworkName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 22
    :catch_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "JSON parsing failed for MSDK network name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    invoke-interface {v1}, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;->getAdNetworkName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "TTMediationSDK"

    .line 24
    invoke-static {v3, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    return-object v2
.end method

.method private b()V
    .locals 3

    .line 25
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->b:Ljava/util/List;

    monitor-enter v0

    .line 26
    :try_start_0
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;

    if-eqz v2, :cond_0

    .line 28
    invoke-interface {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;->a()V

    goto :goto_0

    .line 31
    :cond_1
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 32
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/util/Map;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 10
    invoke-static {p1}, Lcom/bytedance/msdk/adapter/util/Preconditions;->checkNotNull(Ljava/lang/Object;)V

    .line 12
    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->b(Landroid/content/Context;Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 16
    :cond_0
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;",
            ">;"
        }
    .end annotation

    .line 23
    invoke-static {}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClassNamesSet()Ljava/util/Set;

    move-result-object v0

    .line 24
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :catch_0
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    .line 27
    :try_start_0
    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 28
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;

    :cond_1
    if-nez v2, :cond_0

    .line 32
    const-class v2, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;

    invoke-static {v1, v2}, Lcom/bytedance/msdk/adapter/util/Reflection;->instantiateClassWithEmptyConstructor(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;

    if-eqz v2, :cond_0

    .line 36
    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    :cond_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a:Ljava/util/Map;

    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/bytedance/msdk/adapter/util/Preconditions;->checkNotNull(Ljava/lang/Object;)V

    .line 2
    invoke-static {p2}, Lcom/bytedance/msdk/adapter/util/Preconditions;->checkNotNull(Ljava/lang/Object;)V

    .line 3
    invoke-static {p3}, Lcom/bytedance/msdk/adapter/util/Preconditions;->checkNotNull(Ljava/lang/Object;)V

    .line 4
    invoke-static {p4}, Lcom/bytedance/msdk/adapter/util/Preconditions;->checkNotNull(Ljava/lang/Object;)V

    .line 5
    invoke-direct {p0, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;)V

    .line 7
    new-instance v8, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a$a;

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->a:Ljava/util/Map;

    const/4 p1, 0x0

    if-eqz p5, :cond_0

    const/4 p5, 0x1

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    move-object v0, v8

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p0

    invoke-direct/range {v0 .. v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a$a;-><init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/b;Ljava/util/Map;Z)V

    new-array p1, p1, [Ljava/lang/Void;

    .line 9
    invoke-static {v8, p1}, Lcom/bytedance/msdk/adapter/util/AsyncTasks;->safeExecuteOnExecutor(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/a;->b()V

    return-void
.end method

.method public onNetworkInitializationFinished(Ljava/lang/Class;Lcom/bytedance/msdk/api/AdError;)V
    .locals 2
    .param p1    # Ljava/lang/Class;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/bytedance/msdk/api/AdError;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;",
            ">;",
            "Lcom/bytedance/msdk/api/AdError;",
            ")V"
        }
    .end annotation

    const-string v0, "TTMediationSDK"

    .line 1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " initialized status: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of p1, p2, Lcom/bytedance/msdk/api/error/InitSdkError;

    if-eqz p1, :cond_0

    check-cast p2, Lcom/bytedance/msdk/api/error/InitSdkError;

    invoke-virtual {p2}, Lcom/bytedance/msdk/api/error/InitSdkError;->printMsg()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
