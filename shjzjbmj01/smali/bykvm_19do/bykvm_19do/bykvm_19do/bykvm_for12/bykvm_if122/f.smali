.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/f;
.super Ljava/lang/Object;
.source "TTClientBiddingCore.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$a;)V
    .locals 9

    .line 1
    iget-object v0, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->b:Ljava/util/Map;

    .line 2
    iget-object p1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 4
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 5
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 6
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_0

    .line 7
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-gtz v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    .line 11
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v3

    const/16 v4, 0x64

    if-ne v3, v4, :cond_2

    goto :goto_0

    .line 14
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 15
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 16
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    .line 18
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    goto :goto_1

    .line 20
    :cond_4
    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkPlatformId()I

    move-result v4

    if-ne v4, v5, :cond_5

    .line 21
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v6

    cmpg-double v8, v4, v6

    if-gtz v8, :cond_3

    .line 22
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 23
    instance-of v4, p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$b;

    if-eqz v4, :cond_3

    .line 24
    move-object v4, p2

    check-cast v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$b;

    invoke-interface {v4, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;)V

    goto :goto_1

    .line 28
    :cond_5
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v6

    cmpg-double v8, v4, v6

    if-gtz v8, :cond_3

    .line 30
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v6

    cmpl-double v8, v4, v6

    if-nez v8, :cond_6

    .line 31
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, "pangle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    goto :goto_1

    .line 34
    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 35
    instance-of v4, p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$b;

    if-eqz v4, :cond_3

    .line 36
    move-object v4, p2

    check-cast v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$b;

    invoke-interface {v4, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;)V

    goto :goto_1

    :cond_7
    if-eqz p2, :cond_8

    .line 43
    new-instance p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/a;

    invoke-direct {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/a;-><init>()V

    .line 44
    iput-object v0, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/a;->a:Ljava/util/Map;

    .line 45
    invoke-interface {p2, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/a;)V

    :cond_8
    return-void
.end method
