.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/g;
.super Ljava/lang/Object;
.source "TTServerBiddingCore.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/g;Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;)Ljava/util/List;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/g;->a(Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;",
            ")",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 16
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    if-eqz p2, :cond_4

    .line 20
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->r()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->r()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1

    .line 24
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 29
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->r()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    .line 30
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    if-eqz v1, :cond_3

    if-eqz v3, :cond_3

    .line 31
    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 32
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    :goto_1
    return-object v0
.end method


# virtual methods
.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$a;)V
    .locals 14
    .param p2    # Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object v4, p1

    if-eqz v4, :cond_1

    .line 2
    iget-object v0, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->c:Lcom/bytedance/msdk/api/AdSlot;

    if-eqz v0, :cond_1

    iget-object v0, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;

    if-eqz v0, :cond_1

    iget-object v0, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->d:Ljava/util/List;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/s;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 6
    :cond_0
    iget-object v0, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->s()Ljava/lang/String;

    move-result-object v6

    .line 8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->c:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\u5f00\u542fserver bidding\u7f51\u7edc\u8bf7\u6c42......\uff1a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iget-object v0, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->c:Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v0, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->b(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)V

    .line 11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v0, 0x1

    .line 12
    new-array v5, v0, [I

    const/4 v0, 0x0

    const/4 v1, 0x4

    aput v1, v5, v0

    .line 14
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;

    move-result-object v8

    iget-object v9, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->c:Lcom/bytedance/msdk/api/AdSlot;

    iget-object v10, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->d:Ljava/util/List;

    iget-object v11, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->e:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;

    iget v12, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;->f:I

    new-instance v13, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/g$a;

    move-object v0, v13

    move-object v1, p0

    move-object v4, p1

    move-object/from16 v7, p2

    invoke-direct/range {v0 .. v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/g$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/g;JLbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/b;[ILjava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$a;)V

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    invoke-virtual/range {v7 .. v12}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;ILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;)V

    return-void

    :cond_1
    :goto_0
    const/4 v0, 0x0

    move-object/from16 v1, p2

    .line 15
    invoke-interface {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/d$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_if122/a;)V

    return-void
.end method
