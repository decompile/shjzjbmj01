.class Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;
.super Ljava/lang/Object;
.source "TTAdHeaderBidingRequestCore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->D()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;


# direct methods
.method constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;

    iget-object v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;

    iget-object v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;

    iget-object v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 3
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;

    iget-object v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;

    iget-object v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 4
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;

    iget-object v1, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    invoke-static {v1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "m-sdk----\u8bbe\u7f6e cacheTimeout \u5df2\u5230\u65f6\u95f4\u3010"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v1

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;

    iget-object v2, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->c(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "\u3011\uff0c\u7ed9\u5916\u90e8invokeAdVideoCache ..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    :cond_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e$h;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->r()V

    :cond_3
    return-void
.end method
