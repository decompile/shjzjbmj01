.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/b;
.super Ljava/lang/Object;
.source "SortAlgorithm.java"


# direct methods
.method public static a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    if-eqz p0, :cond_4

    .line 1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    goto :goto_4

    .line 3
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 5
    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_1

    .line 6
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 10
    :cond_1
    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_3

    .line 11
    aget v3, v0, v1

    move v4, v1

    :goto_2
    if-lez v4, :cond_2

    add-int/lit8 v5, v4, -0x1

    .line 14
    aget v6, v0, v5

    if-le v6, v3, :cond_2

    .line 15
    aget v5, v0, v5

    aput v5, v0, v4

    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 17
    :cond_2
    aput v3, v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 21
    :cond_3
    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 23
    array-length v1, v0

    :goto_3
    if-ge v2, v1, :cond_4

    aget v3, v0, v2

    .line 24
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    :goto_4
    return-void
.end method
