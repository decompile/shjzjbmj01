.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;
.super Ljava/lang/Object;
.source "CacheBase.java"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/AdSlot;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_e

    if-nez p1, :cond_0

    goto/16 :goto_1

    .line 18
    :cond_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getOrientation()I

    move-result v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getOrientation()I

    move-result v2

    if-eq v1, v2, :cond_1

    return v0

    .line 22
    :cond_1
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v1

    .line 23
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v2

    if-nez v1, :cond_2

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_e

    if-eqz v2, :cond_e

    .line 27
    invoke-virtual {v1}, Lcom/bytedance/msdk/api/TTVideoOption;->isMuted()Z

    move-result v1

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/TTVideoOption;->isMuted()Z

    move-result v2

    if-eq v1, v2, :cond_3

    return v0

    .line 34
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getBannerSize()I

    move-result v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getBannerSize()I

    move-result v2

    if-eq v1, v2, :cond_4

    return v0

    .line 38
    :cond_4
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdCount()I

    move-result v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdCount()I

    move-result v2

    if-eq v1, v2, :cond_5

    return v0

    .line 42
    :cond_5
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v2

    if-eq v1, v2, :cond_6

    return v0

    .line 45
    :cond_6
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v2

    if-eq v1, v2, :cond_7

    return v0

    .line 49
    :cond_7
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v2

    if-eq v1, v2, :cond_8

    return v0

    .line 54
    :cond_8
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 55
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    return v0

    .line 58
    :cond_9
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_e

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_e

    .line 63
    :cond_a
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getRewardAmount()I

    move-result v1

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getRewardAmount()I

    move-result v2

    if-eq v1, v2, :cond_b

    return v0

    .line 66
    :cond_b
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getCustomData()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getCustomData()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 67
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getCustomData()Ljava/util/Map;

    move-result-object p0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getCustomData()Ljava/util/Map;

    move-result-object p1

    invoke-static {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a(Ljava/util/Map;Ljava/util/Map;)Z

    move-result p0

    if-nez p0, :cond_d

    return v0

    .line 70
    :cond_c
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getCustomData()Ljava/util/Map;

    move-result-object p0

    if-nez p0, :cond_e

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getCustomData()Ljava/util/Map;

    move-result-object p0

    if-nez p0, :cond_e

    :cond_d
    const/4 p0, 0x1

    return p0

    :cond_e
    :goto_1
    return v0
.end method

.method public static a(Ljava/util/Map;Ljava/util/Map;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_6

    if-eqz p1, :cond_6

    .line 71
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v3

    if-eq v2, v3, :cond_1

    return v1

    .line 74
    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 76
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-nez v3, :cond_3

    goto :goto_0

    .line 80
    :cond_3
    invoke-interface {p0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 81
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-nez v4, :cond_4

    if-nez v3, :cond_4

    goto :goto_0

    .line 86
    :cond_4
    invoke-static {v4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    return v1

    :cond_5
    return v0

    :cond_6
    return v1
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 1

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const p1, 0x1b7740

    :goto_0
    return p1
.end method

.method public a(Ljava/lang/String;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;)Z
    .locals 5

    const/4 v0, 0x1

    if-eqz p1, :cond_3

    .line 4
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->hasDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2

    .line 10
    :cond_0
    iget-object v1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1}, Lcom/bytedance/msdk/base/TTBaseAd;->adnHasAdExpiredApi()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    iget-object p1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->isAdExpired()Z

    move-result p1

    const-string v0, "TTMediationSDK"

    const-string v1, "--==-- \u4f7f\u7528\u4e86adn\u7684\u8fc7\u671f\u63a5\u53e3 ------"

    .line 12
    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 14
    :cond_1
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a()J

    move-result-wide v1

    iget-object p1, p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a(Ljava/lang/String;)I

    move-result p1

    int-to-long v3, p1

    add-long/2addr v1, v3

    .line 15
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    cmp-long p1, v3, v1

    if-lez p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    const/4 p1, 0x0

    :goto_0
    const-string v0, "TTMediationSDK"

    const-string v1, "--==-- \u4f7f\u7528\u4e86\u914d\u7f6e\u7684\u8fc7\u671f\u65f6\u95f4\u8fdb\u884c\u5224\u65ad ------"

    .line 16
    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return p1

    :cond_3
    :goto_2
    const-string p1, "TTMediationSDK"

    const-string v1, "--==-- \u5e7f\u544a\u5df2\u7ecfdestroy\u4e86\uff0c\u89c6\u4e3a\u8fc7\u671f ------"

    .line 17
    invoke-static {p1, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method
