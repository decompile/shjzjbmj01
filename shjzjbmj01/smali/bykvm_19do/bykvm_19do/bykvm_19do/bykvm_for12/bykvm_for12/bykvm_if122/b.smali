.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;
.super Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;
.source "PreLoadManager.java"


# static fields
.field private static volatile k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/msdk/api/AdSlot;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/msdk/api/AdError;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->b:Ljava/util/Map;

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->c:Ljava/util/Map;

    .line 4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->d:Ljava/util/Map;

    .line 6
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->e:Ljava/util/Map;

    .line 7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->f:Ljava/util/Map;

    .line 8
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g:Ljava/util/Map;

    .line 9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->h:Ljava/util/Map;

    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->i:Ljava/util/Map;

    .line 12
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->j:Ljava/util/Map;

    return-void
.end method

.method public static a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;
    .locals 2

    .line 1
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    if-nez v0, :cond_1

    .line 2
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    monitor-enter v0

    .line 3
    :try_start_0
    sget-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    if-nez v1, :cond_0

    .line 4
    new-instance v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    invoke-direct {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;-><init>()V

    sput-object v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    .line 6
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 8
    :cond_1
    :goto_0
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->k:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    return-object v0
.end method

.method private a(Ljava/util/List;Ljava/lang/String;Ljava/util/Map;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;",
            ">;)",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;",
            ">;"
        }
    .end annotation

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 101
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_5

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->c:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 102
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;

    .line 104
    new-instance v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;

    invoke-direct {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;-><init>()V

    .line 105
    invoke-virtual {v4, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;->b(Ljava/lang/String;)V

    .line 106
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->n()I

    move-result v5

    invoke-virtual {v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;->d(I)V

    .line 107
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->i()I

    move-result v5

    invoke-virtual {v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;->b(I)V

    if-nez v3, :cond_2

    const/4 v3, 0x0

    .line 110
    iget-object v5, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->h:Ljava/util/Map;

    invoke-interface {v5, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    if-eqz v5, :cond_0

    .line 112
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/bytedance/msdk/api/AdError;

    :cond_0
    if-eqz v3, :cond_1

    const/4 v2, 0x3

    .line 115
    invoke-virtual {v4, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;->c(I)V

    .line 116
    iget v2, v3, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorCode:I

    invoke-virtual {v4, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;->a(I)V

    .line 117
    iget-object v2, v3, Lcom/bytedance/msdk/api/AdError;->thirdSdkErrorMessage:Ljava/lang/String;

    invoke-virtual {v4, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x4

    .line 119
    invoke-virtual {v4, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;->c(I)V

    .line 121
    :goto_1
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 123
    :cond_2
    invoke-virtual {p0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 124
    iget-object v2, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v2}, Lcom/bytedance/msdk/base/TTBaseAd;->adnHasAdExpiredApi()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x2

    :goto_2
    invoke-virtual {v4, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;->c(I)V

    .line 126
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_5
    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2

    .line 131
    invoke-virtual {p0, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->e(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 132
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "--==-- \u914d\u7f6e\u672a\u5f00\u542f\u9884\u52a0\u8f7d\u7f13\u5b58 ----"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "TTMediationSDK"

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->i:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->i:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 141
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "--==-- preloat start ----"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 145
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 147
    :cond_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->h:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 149
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 152
    :cond_3
    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;

    invoke-direct {v0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 153
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->j:Ljava/util/Map;

    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->i:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->f:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/msdk/api/AdSlot;

    if-nez p1, :cond_4

    .line 158
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "--==-- preload fail\uff0cadslot\u4e3anull\uff0crit\uff1a"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "TTMediationSDK"

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 161
    :cond_4
    invoke-virtual {p1, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->setPrimeRitReqType(I)V

    .line 162
    invoke-virtual {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->a(Lcom/bytedance/msdk/api/AdSlot;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 64
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    .line 69
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;

    if-eqz v2, :cond_1

    .line 70
    invoke-virtual {p0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 72
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "--==-- \u547d\u4e2d\u6700\u4f18\u5e7f\u544a\uff1a "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 73
    invoke-virtual {p2}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetWorkName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", loadSort: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 74
    invoke-virtual {p2}, Lcom/bytedance/msdk/base/TTBaseAd;->getLoadSort()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ", showSort: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {p2}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "TTMediationSDK"

    .line 75
    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 81
    :cond_1
    invoke-direct {p0, p1, p2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a(Ljava/util/List;Ljava/lang/String;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    .line 82
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 84
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 85
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;

    .line 86
    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_2

    const-string v2, "]"

    .line 88
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const-string v2, ","

    .line 90
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 94
    :cond_3
    :try_start_0
    new-instance p1, Lorg/json/JSONArray;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v0, "TTMediationSDK"

    .line 95
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "adCannotUseInfo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->f:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/msdk/api/AdSlot;

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "adCannotUseInfo json err: "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "TTMediationSDK"

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_2
    return-void
.end method

.method private c(Ljava/lang/String;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 6

    .line 3
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 5
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 6
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;

    if-eqz v3, :cond_0

    .line 8
    invoke-virtual {p0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    .line 10
    invoke-interface {v0, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "--==-- \u5e7f\u544a\u8fc7\u671f\u4e86 -------: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 12
    invoke-virtual {v4}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", loadSort: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 13
    invoke-virtual {v4}, Lcom/bytedance/msdk/base/TTBaseAd;->getLoadSort()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", showSort: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 14
    invoke-virtual {v3}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TTMediationSDK"

    .line 15
    invoke-static {v3, v2}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 19
    :cond_1
    iget-object v4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->f:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/bytedance/msdk/api/AdSlot;

    invoke-static {v4, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/AdSlot;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 20
    invoke-interface {v0, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "--==-- \u5e7f\u544aAdSlot\u4e0d\u7b26\u5408 -------: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 22
    invoke-virtual {v4}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ", loadSort: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 23
    invoke-virtual {v4}, Lcom/bytedance/msdk/base/TTBaseAd;->getLoadSort()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", showSort: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    .line 24
    invoke-virtual {v3}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TTMediationSDK"

    .line 25
    invoke-static {v3, v2}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;
    .locals 1

    .line 127
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_0

    .line 129
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x2

    .line 130
    invoke-direct {p0, p1, p2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method public a(Ljava/lang/String;J)V
    .locals 1

    .line 9
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->e:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;)V
    .locals 5

    .line 21
    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->b()J

    move-result-wide v0

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->e:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 25
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_1

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 28
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    :cond_1
    iget-object p1, p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {p1}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetworkSlotId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;JLcom/bytedance/msdk/api/AdError;)V
    .locals 3

    .line 11
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v2, p3, v0

    if-eqz v2, :cond_0

    return-void

    .line 15
    :cond_0
    iget-object p3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->h:Ljava/util/Map;

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Map;

    if-nez p3, :cond_1

    .line 17
    new-instance p3, Ljava/util/HashMap;

    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    .line 18
    iget-object p4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->h:Ljava/util/Map;

    invoke-interface {p4, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    :cond_1
    invoke-interface {p3, p2, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/bytedance/msdk/api/AdSlot;)Z
    .locals 7

    .line 31
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->i:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;

    if-eqz v0, :cond_3

    .line 35
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->x()Ljava/util/List;

    move-result-object v0

    .line 36
    invoke-static {}, Lcom/bytedance/msdk/adapter/util/Logger;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    .line 38
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/bytedance/msdk/adapter/util/TTLogUtil;->getTagSecondLevel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "waterfall: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ", loadSort: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->i()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ", showSort: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->n()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ", eCpm: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "TTMediationSDK"

    .line 42
    invoke-static {v4, v3}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_0
    invoke-direct {p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->c(Ljava/lang/String;Lcom/bytedance/msdk/api/AdSlot;)V

    .line 53
    iget-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map;

    if-eqz p2, :cond_3

    .line 55
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    .line 56
    :goto_1
    iget-object v4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->c:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ge v3, v4, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 57
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v4

    .line 58
    invoke-interface {p2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;

    if-eqz v4, :cond_1

    .line 60
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 63
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method public b(Ljava/lang/String;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 1

    .line 4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 7
    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/msdk/api/AdSlot;

    return-object p1
.end method

.method public b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x4

    .line 8
    invoke-direct {p0, p1, p2, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method public b(Ljava/lang/String;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b(Ljava/lang/String;Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 1

    if-eqz p2, :cond_0

    .line 2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->f:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;
    .locals 1

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;

    return-object p1
.end method

.method public c(Ljava/lang/String;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->c:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public d(Ljava/lang/String;)I
    .locals 1

    .line 2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_0

    .line 3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.method public d(Ljava/lang/String;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public e(Ljava/lang/String;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public f(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    return-object p1
.end method

.method public g(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_1

    .line 4
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 5
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;

    if-eqz v2, :cond_0

    .line 7
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method
