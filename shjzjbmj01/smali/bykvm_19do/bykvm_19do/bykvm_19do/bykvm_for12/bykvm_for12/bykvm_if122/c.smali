.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;
.super Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;
.source "PreLoadRequestCore.java"


# instance fields
.field private J:J

.field private K:I

.field private final L:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private final M:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;"
        }
    .end annotation
.end field

.field private final N:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 p1, 0x0

    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->K:I

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->L:Ljava/util/Map;

    new-instance p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c$a;

    invoke-direct {p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;)V

    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->M:Ljava/util/Comparator;

    new-instance p2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c$b;

    invoke-direct {p2, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c$b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;)V

    iput-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->N:Ljava/util/Comparator;

    iget-object p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->d:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->q()Ljava/util/Map;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v1

    const/16 v2, 0x64

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->L:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;)Ljava/lang/String;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;Lcom/bytedance/msdk/api/AdError;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->onAdFailed(Lcom/bytedance/msdk/api/AdError;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;)V

    :goto_0
    const/16 p0, 0x5d

    const/16 p1, 0x5d

    :goto_1
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 p0, 0x4

    if-gt p1, p0, :cond_0

    goto :goto_0

    :cond_0
    return-void

    :pswitch_1
    packed-switch p1, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch p1, :pswitch_data_2

    goto :goto_3

    :pswitch_3
    const/16 p0, 0x5e

    const/16 p1, 0x4b

    goto :goto_1

    :goto_3
    const/16 p1, 0x5b

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;)J
    .locals 4

    iget-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->J:J

    :pswitch_0
    const/16 p0, 0x5e

    const/16 v2, 0x4b

    const/16 v3, 0x5d

    :goto_0
    packed-switch p0, :pswitch_data_0

    goto :goto_3

    :pswitch_1
    packed-switch v2, :pswitch_data_1

    :goto_1
    :pswitch_2
    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_3
    return-wide v0

    :goto_2
    const/16 v2, 0x5b

    goto :goto_1

    :goto_3
    :pswitch_4
    const/16 p0, 0x5d

    const/16 v2, 0x5d

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private b(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-direct {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;-><init>()V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b(I)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a(I)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->i()I

    move-result v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d(I)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->n()I

    move-result v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f(I)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->k()I

    move-result v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e(I)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->g(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f(Ljava/lang/String;)V

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->m()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    move-result-object v1

    invoke-virtual {v2, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;)V

    invoke-interface {p1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_1
    return-void
.end method

.method private c(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;)",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;"
        }
    .end annotation

    :cond_0
    :goto_0
    const/16 v0, 0x50

    const/16 v1, 0x52

    :goto_1
    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_2
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    goto :goto_2

    :pswitch_1
    const/16 v0, 0x37

    mul-int v3, v0, v0

    mul-int/lit8 v3, v3, 0x37

    const/16 v4, 0xb

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0xb

    add-int/2addr v3, v5

    add-int/2addr v3, v5

    mul-int/lit8 v0, v0, 0xb

    mul-int/lit8 v0, v0, 0xb

    mul-int/lit8 v0, v0, 0x3

    if-ge v3, v0, :cond_3

    :pswitch_2
    const/16 v0, 0xd

    mul-int v3, v0, v0

    mul-int/lit8 v3, v3, 0xd

    const/16 v4, 0x9

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x9

    add-int/2addr v3, v5

    const/16 v5, 0x8

    mul-int v6, v5, v5

    mul-int/lit8 v6, v6, 0x8

    add-int/2addr v3, v6

    mul-int/lit8 v0, v0, 0x9

    mul-int/lit8 v0, v0, 0x8

    mul-int/lit8 v0, v0, 0x3

    if-ge v3, v0, :cond_2

    goto :goto_2

    :cond_2
    const/16 v0, 0x52

    :goto_3
    packed-switch v0, :pswitch_data_2

    const/16 v0, 0x47

    goto :goto_3

    :cond_3
    :pswitch_3
    const/16 v0, 0x40

    mul-int v3, v0, v0

    const/16 v4, 0x26

    mul-int v5, v4, v4

    add-int/2addr v3, v5

    const/4 v5, 0x4

    mul-int v6, v5, v5

    add-int/2addr v3, v6

    const/16 v6, 0x40

    mul-int/lit8 v6, v6, 0x26

    mul-int/lit8 v4, v4, 0x4

    add-int/2addr v6, v4

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v6, v0

    if-ge v3, v6, :cond_0

    :pswitch_4
    const/16 v0, 0x54

    mul-int v3, v0, v0

    const/4 v4, 0x7

    mul-int v5, v4, v4

    add-int/2addr v3, v5

    mul-int/lit8 v0, v0, 0x7

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v3, v0

    if-gez v3, :cond_4

    const/16 v0, 0x11

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x11

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    :pswitch_5
    const/16 v0, 0x2d

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x2d

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    goto/16 :goto_0

    :cond_4
    :pswitch_6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v4

    const/16 v5, 0x64

    if-ne v4, v5, :cond_6

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    :cond_6
    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v4

    if-ne v4, v2, :cond_5

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v4

    iget-object v5, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a(Ljava/lang/String;Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;

    move-result-object v4

    if-eqz v4, :cond_5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v4}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c(Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->M:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->N:Ljava/util/Comparator;

    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0

    :pswitch_7
    const/16 v0, 0x52

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x50
        :pswitch_0
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x52
        :pswitch_7
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x46
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private d(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;)",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x40

    :goto_0
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->p()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->M:Ljava/util/Comparator;

    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_2

    return-object p1

    :cond_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->L:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->L:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->M:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0

    :cond_4
    return-object p1

    :pswitch_1
    const/16 v0, 0x42

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x40
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private y()Lcom/bytedance/msdk/base/TTBaseAd;
    .locals 9

    :cond_0
    :goto_0
    const/16 v0, 0x52

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v1, 0x37

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0x37

    const/16 v3, 0xb

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0xb

    add-int/2addr v2, v4

    add-int/2addr v2, v4

    mul-int/lit8 v1, v1, 0xb

    mul-int/lit8 v1, v1, 0xb

    mul-int/lit8 v1, v1, 0x3

    if-ge v2, v1, :cond_2

    :pswitch_1
    const/16 v1, 0xd

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0xd

    const/16 v3, 0x9

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0x9

    add-int/2addr v2, v4

    const/16 v4, 0x8

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x8

    add-int/2addr v2, v5

    mul-int/lit8 v1, v1, 0x9

    mul-int/lit8 v1, v1, 0x8

    mul-int/lit8 v1, v1, 0x3

    if-ge v2, v1, :cond_1

    goto :goto_2

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x47

    goto :goto_1

    :cond_2
    :pswitch_2
    const/16 v0, 0x40

    mul-int v1, v0, v0

    const/16 v2, 0x26

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/4 v3, 0x4

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    const/16 v4, 0x40

    mul-int/lit8 v4, v4, 0x26

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr v4, v2

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    if-ge v1, v4, :cond_4

    :pswitch_3
    const/16 v0, 0x54

    mul-int v1, v0, v0

    const/4 v2, 0x7

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    mul-int/lit8 v0, v0, 0x7

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v1, v0

    if-gez v1, :cond_0

    const/16 v0, 0x11

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x11

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    :pswitch_4
    const/16 v0, 0x2d

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x2d

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    :cond_3
    :goto_2
    move-object v1, p0

    goto :goto_4

    :cond_4
    move-object v1, p0

    goto :goto_7

    :pswitch_5
    move-object v1, p0

    :goto_3
    packed-switch v0, :pswitch_data_2

    goto :goto_7

    :goto_4
    :pswitch_6
    const/4 v0, 0x0

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v2

    iget-object v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->g(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_7

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;

    iget-object v4, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v4}, Lcom/bytedance/msdk/base/TTBaseAd;->isBidingAd()Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v1, v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->a(Ljava/util/List;Lcom/bytedance/msdk/base/TTBaseAd;)V

    if-nez v0, :cond_6

    goto :goto_6

    :cond_6
    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v4

    iget-object v6, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    invoke-virtual {v6}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v6

    cmpl-double v8, v4, v6

    if-lez v8, :cond_5

    :goto_6
    iget-object v0, v3, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;->a:Lcom/bytedance/msdk/base/TTBaseAd;

    goto :goto_5

    :cond_7
    return-object v0

    :goto_7
    :pswitch_7
    const/16 v0, 0x50

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_5
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x46
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x50
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method protected a(IZ)V
    .locals 7

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v0

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->d(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x2

    :cond_1
    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->K:I

    const/16 v2, 0x19

    const/16 v3, 0x1b

    if-lt v1, v0, :cond_2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "--==--loadAdByLoadSort("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->K:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ")\u8bf7\u6c42\u5c42\u6570\u8d85\u8fc7pre_load_sort_control("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ")"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "TTMediationSDK"

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, 0x54

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->m:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_5

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->s:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/g;

    invoke-virtual {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/g;->c(I)Z

    move-result v0

    if-nez v0, :cond_5

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->K:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->K:I

    invoke-super {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->a(IZ)V

    const/16 p1, 0x1a

    goto :goto_0

    :cond_3
    const/16 p1, 0x1b

    :goto_0
    :pswitch_0
    packed-switch p1, :pswitch_data_0

    goto :goto_3

    :goto_1
    :pswitch_1
    const/16 p2, 0x29

    packed-switch p2, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 v0, 0x47

    mul-int v1, v0, v0

    const/16 v4, 0x25

    mul-int v5, v4, v4

    add-int/2addr v1, v5

    mul-int v5, v3, v3

    add-int/2addr v1, v5

    const/16 v6, 0x47

    mul-int/lit8 v6, v6, 0x25

    mul-int/lit8 v4, v4, 0x1b

    add-int/2addr v6, v4

    mul-int/lit8 v0, v0, 0x1b

    add-int/2addr v6, v0

    if-ge v1, v6, :cond_4

    goto :goto_2

    :cond_4
    packed-switch p2, :pswitch_data_2

    const/4 p2, 0x4

    mul-int v0, p2, p2

    const/16 v1, 0x20

    mul-int v4, v1, v1

    add-int/2addr v0, v4

    mul-int v4, v2, v2

    add-int/2addr v0, v4

    const/4 v4, 0x4

    mul-int/lit8 v4, v4, 0x20

    mul-int/lit8 v1, v1, 0x19

    add-int/2addr v4, v1

    mul-int/lit8 p2, p2, 0x19

    add-int/2addr v4, p2

    if-ge v0, v4, :cond_5

    :pswitch_3
    const/16 p2, 0x2e

    mul-int v0, p2, p2

    const/16 v1, 0xc

    mul-int v4, v1, v1

    add-int/2addr v0, v4

    add-int/2addr v0, v5

    const/16 v4, 0x2e

    mul-int/lit8 v4, v4, 0xc

    mul-int/lit8 v1, v1, 0x1b

    add-int/2addr v4, v1

    mul-int/lit8 p2, p2, 0x1b

    add-int/2addr v4, p2

    if-ge v0, v4, :cond_5

    goto :goto_0

    :pswitch_4
    const/4 p1, 0x7

    mul-int p2, p1, p1

    const/4 v0, 0x0

    mul-int v1, v0, v0

    add-int/2addr p2, v1

    mul-int/lit8 p1, p1, 0x0

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p2, p1

    if-gez p2, :cond_5

    :cond_5
    :goto_2
    :pswitch_5
    return-void

    :goto_3
    const/16 p1, 0x19

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x19
        :pswitch_1
        :pswitch_5
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x27
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x26
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/os/Message;)V
    .locals 6

    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x2711

    if-ne v0, v1, :cond_1

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->y()Lcom/bytedance/msdk/base/TTBaseAd;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->i:D

    const-wide/16 v3, 0x0

    cmpl-double v5, v1, v3

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v1

    cmpl-double v5, v1, v3

    if-nez v5, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->a(Lcom/bytedance/msdk/base/TTBaseAd;)Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--==-- \u9884\u8bf7\u6c42 \u6267\u884c\u4e86client bidding\u8fc7\u6ee4,lowestCpmAd: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getAdNetWorkName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", loadSort: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getLoadSort()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", showSort: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getShowSort()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", cpm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/bytedance/msdk/base/TTBaseAd;->getCpm()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TTMediationSDK"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-super {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->a(Landroid/os/Message;)V

    return-void
.end method

.method protected a(Lcom/bytedance/msdk/api/AdError;)V
    .locals 2

    :pswitch_0
    const/16 p1, 0x5e

    const/16 v0, 0x4b

    const/16 v1, 0x5d

    :goto_0
    packed-switch p1, :pswitch_data_0

    goto :goto_3

    :pswitch_1
    packed-switch v0, :pswitch_data_1

    :goto_1
    :pswitch_2
    packed-switch v0, :pswitch_data_2

    goto :goto_2

    :pswitch_3
    return-void

    :goto_2
    const/16 v0, 0x5b

    goto :goto_1

    :goto_3
    :pswitch_4
    const/16 p1, 0x5d

    const/16 v0, 0x5d

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/bytedance/msdk/api/AdSlot;)V
    .locals 9

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->J:J

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v0

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    iget-wide v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->J:J

    invoke-virtual {v0, v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a(Ljava/lang/String;J)V

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->h:Lcom/bytedance/msdk/api/AdSlot;

    const/4 p1, 0x0

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->B:Lcom/bytedance/msdk/adapter/listener/ITTAdatperCallback;

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/e;->t()V

    :cond_0
    :goto_0
    const/16 p1, 0x34

    const/16 v0, 0x60

    const/4 v1, -0x1

    const/16 v2, 0xd

    const/4 v3, 0x1

    const/16 v4, 0x22

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x39

    mul-int v5, v0, v0

    mul-int v6, v4, v4

    add-int/2addr v5, v6

    const/16 v6, 0xc

    mul-int v7, v6, v6

    add-int/2addr v5, v7

    const/16 v7, 0x39

    mul-int/lit8 v7, v7, 0x22

    const/16 v8, 0x22

    mul-int/lit8 v8, v8, 0xc

    add-int/2addr v7, v8

    mul-int/lit8 v0, v0, 0xc

    add-int/2addr v7, v0

    if-ge v5, v7, :cond_2

    :pswitch_1
    const/16 v0, 0xd

    :goto_1
    const/16 v5, 0x12

    packed-switch p1, :pswitch_data_1

    goto :goto_3

    :pswitch_2
    mul-int p1, v5, v5

    const/16 v0, 0x23

    mul-int v0, v0, v0

    mul-int/lit8 v0, v0, 0x22

    sub-int/2addr p1, v0

    if-ne p1, v1, :cond_3

    goto :goto_2

    :pswitch_3
    packed-switch v0, :pswitch_data_2

    goto :goto_4

    :goto_2
    :pswitch_4
    const/16 p1, 0x1a

    mul-int v0, p1, p1

    mul-int/lit8 v0, v0, 0x1a

    const/16 v1, 0x9

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0x9

    add-int/2addr v0, v2

    const/16 v2, 0xf

    mul-int v4, v2, v2

    mul-int/lit8 v4, v4, 0xf

    add-int/2addr v0, v4

    mul-int/lit8 p1, p1, 0x9

    mul-int/lit8 p1, p1, 0xf

    mul-int/lit8 p1, p1, 0x3

    if-ge v0, p1, :cond_3

    :pswitch_5
    const/4 p1, 0x0

    rsub-int/lit8 v0, v3, 0x0

    mul-int/lit8 v0, v0, 0x0

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p1, v3

    mul-int v0, v0, p1

    rem-int/lit8 v0, v0, 0x6

    if-eqz v0, :cond_0

    :cond_1
    :pswitch_6
    rsub-int/lit8 p1, v3, 0x12

    mul-int/lit8 p1, p1, 0x12

    mul-int/lit8 v0, v5, 0x2

    sub-int/2addr v0, v3

    mul-int p1, p1, v0

    rem-int/lit8 p1, p1, 0x6

    if-eqz p1, :cond_1

    goto :goto_5

    :goto_3
    const/16 p1, 0x37

    const/16 v0, 0x3d

    goto :goto_1

    :cond_2
    :goto_4
    :pswitch_7
    const/16 p1, 0x31

    packed-switch p1, :pswitch_data_3

    goto :goto_4

    :pswitch_8
    mul-int p1, v2, v2

    const/16 v0, 0x13

    mul-int v0, v0, v0

    mul-int/lit8 v0, v0, 0x22

    sub-int/2addr p1, v0

    if-ne p1, v1, :cond_3

    :pswitch_9
    const/16 p1, 0xa

    add-int/2addr p1, v3

    mul-int/lit8 p1, p1, 0xa

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_0

    goto :goto_4

    :cond_3
    :goto_5
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_0
        :pswitch_1
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x37
        :pswitch_3
        :pswitch_7
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3c
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x31
        :pswitch_a
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public a(Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/base/TTBaseAd;",
            ">;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;",
            ")V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/bytedance/msdk/base/TTBaseAd;

    if-eqz v2, :cond_0

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v0

    iget-object v8, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->f:Ljava/lang/String;

    new-instance v9, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;

    iget-wide v4, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->J:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    move-object v1, v9

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;-><init>(Lcom/bytedance/msdk/base/TTBaseAd;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;JJ)V

    invoke-virtual {v0, v8, v9}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a(Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/d;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected f()Z
    .locals 2

    :goto_0
    const/16 v0, 0x5d

    const/16 v1, 0x5d

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x4

    if-gt v1, v0, :cond_0

    goto :goto_0

    :pswitch_1
    packed-switch v1, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch v1, :pswitch_data_2

    goto :goto_3

    :cond_0
    :pswitch_3
    const/4 v0, 0x1

    return v0

    :pswitch_4
    const/16 v0, 0x5e

    const/16 v1, 0x4b

    goto :goto_1

    :goto_3
    const/16 v1, 0x5b

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onAdFailed(Lcom/bytedance/msdk/api/AdError;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;)V
    .locals 3

    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c$c;

    invoke-direct {v0, p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c$c;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;Lcom/bytedance/msdk/api/AdError;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/d;)V

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/util/ThreadHelper;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    :goto_0
    const/16 p1, 0x25

    const/16 p2, 0x36

    const/4 v0, 0x1

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 p1, 0x12

    mul-int p1, p1, p1

    const/16 p2, 0x23

    mul-int p2, p2, p2

    mul-int/lit8 p2, p2, 0x22

    sub-int/2addr p1, p2

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    :pswitch_2
    const/16 p1, 0x1a

    mul-int p2, p1, p1

    mul-int/lit8 p2, p2, 0x1a

    const/16 v1, 0x51

    mul-int/lit8 v1, v1, 0x9

    add-int/2addr p2, v1

    const/16 v1, 0xf

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0xf

    add-int/2addr p2, v2

    mul-int/lit8 p1, p1, 0x9

    mul-int/lit8 p1, p1, 0xf

    mul-int/lit8 p1, p1, 0x3

    if-ge p2, p1, :cond_1

    :pswitch_3
    const/16 p1, 0x28

    rsub-int/lit8 p2, v0, 0x28

    mul-int/lit8 p2, p2, 0x28

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p1, v0

    mul-int p2, p2, p1

    rem-int/lit8 p2, p2, 0x6

    goto :goto_0

    :goto_1
    :pswitch_4
    const/16 p1, 0x1e

    rsub-int/lit8 p2, v0, 0x1e

    mul-int/lit8 p2, p2, 0x1e

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p1, v0

    mul-int p2, p2, p1

    rem-int/lit8 p2, p2, 0x6

    if-eqz p2, :cond_0

    :cond_1
    :pswitch_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected q()V
    .locals 2

    :goto_0
    const/16 v0, 0x5d

    const/16 v1, 0x5d

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x4

    if-gt v1, v0, :cond_0

    goto :goto_0

    :pswitch_1
    packed-switch v1, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch v1, :pswitch_data_2

    goto :goto_3

    :cond_0
    :pswitch_3
    return-void

    :pswitch_4
    const/16 v0, 0x5e

    const/16 v1, 0x4b

    goto :goto_1

    :goto_3
    const/16 v1, 0x5b

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected r()V
    .locals 3

    :pswitch_0
    const/16 v0, 0x5e

    const/16 v1, 0x4b

    const/16 v2, 0x5d

    :goto_0
    packed-switch v0, :pswitch_data_0

    goto :goto_3

    :pswitch_1
    packed-switch v1, :pswitch_data_1

    :goto_1
    :pswitch_2
    packed-switch v1, :pswitch_data_2

    goto :goto_2

    :pswitch_3
    return-void

    :goto_2
    const/16 v1, 0x5b

    goto :goto_1

    :goto_3
    :pswitch_4
    const/16 v0, 0x5d

    const/16 v1, 0x5d

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public x()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;"
        }
    .end annotation

    const/16 v0, 0xe

    :goto_0
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_4

    :pswitch_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->m:Ljava/util/List;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->m:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->e:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->e:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    if-nez v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->b(Ljava/util/List;)V

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_new1/c;->g()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->d(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_3

    :cond_5
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/c;->M:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :goto_3
    return-object v0

    :goto_4
    :pswitch_1
    const/16 v0, 0xd

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
