.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;
.super Ljava/lang/Object;
.source "TTSdkSettings.java"

# interfaces
.implements Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:J

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:I

.field private j:Ljava/lang/String;

.field private k:I

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/api/TTSettingConfigCallback;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->j:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->k:I

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->l:Ljava/util/Map;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private a(I)Ljava/lang/Long;
    .locals 6

    :cond_0
    :goto_0
    const/16 v0, 0x5c

    const/16 v1, 0xe

    const/4 v2, 0x4

    const/16 v3, 0x14

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :goto_1
    :pswitch_0
    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x5f

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x42

    mul-int v0, v0, v0

    const/16 v1, 0x10

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :pswitch_2
    const/16 v0, 0x1d

    mul-int v1, v0, v0

    const/16 v4, 0x2d

    mul-int v5, v4, v4

    add-int/2addr v1, v5

    mul-int v5, v3, v3

    add-int/2addr v1, v5

    const/16 v5, 0x1d

    mul-int/lit8 v5, v5, 0x2d

    mul-int/lit8 v4, v4, 0x14

    add-int/2addr v5, v4

    mul-int/lit8 v0, v0, 0x14

    add-int/2addr v5, v0

    if-ge v1, v5, :cond_2

    :cond_1
    :pswitch_3
    const/16 v0, 0x46

    mul-int v1, v0, v0

    const/4 v4, 0x7

    mul-int v5, v4, v4

    add-int/2addr v1, v5

    mul-int v5, v2, v2

    add-int/2addr v1, v5

    const/16 v5, 0x46

    mul-int/lit8 v5, v5, 0x7

    mul-int/lit8 v4, v4, 0x4

    add-int/2addr v5, v4

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v5, v0

    if-ge v1, v5, :cond_0

    :cond_2
    :pswitch_4
    const/16 v0, 0x45

    mul-int v1, v0, v0

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    mul-int v4, v2, v2

    add-int/2addr v1, v4

    const/16 v4, 0x45

    mul-int/lit8 v4, v4, 0x14

    mul-int/lit8 v3, v3, 0x4

    add-int/2addr v4, v3

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    if-ge v1, v4, :cond_3

    goto :goto_2

    :cond_3
    :pswitch_5
    const-wide/16 v0, 0x0

    packed-switch p1, :pswitch_data_2

    :pswitch_6
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :pswitch_7
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :goto_2
    :pswitch_8
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_1
        :pswitch_5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_6
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method private a(Lorg/json/JSONArray;)Ljava/lang/String;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eqz v1, :cond_a

    iget-object v6, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->clear()V

    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONArray;->length()I

    move-result v6

    const/4 v7, 0x0

    move-object v8, v2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v6, :cond_9

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    if-eqz v9, :cond_7

    new-instance v8, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;

    invoke-direct {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;-><init>()V

    const-string v10, "rit_id"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->b(Ljava/lang/String;)V

    const-string v10, "version"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->c(Ljava/lang/String;)V

    const-string v10, "waterfall_id"

    const-wide/16 v11, -0x1

    invoke-virtual {v9, v10, v11, v12}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->e(J)V

    const-string v10, "rit_type"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->f(I)V

    const-string v10, "look_type"

    invoke-virtual {v9, v10, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v10

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->b(I)V

    const-string v10, "look_time"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->g(I)V

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->l()I

    move-result v10

    invoke-direct {v0, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(I)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-string v12, "time_min"

    invoke-virtual {v9, v12, v10, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->c(J)V

    const-string v10, "layer_time_out"

    const-wide/16 v11, 0x7d0

    invoke-virtual {v9, v10, v11, v12}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->b(J)V

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->l()I

    move-result v10

    invoke-direct {v0, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->b(I)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-string v12, "total_time_out"

    invoke-virtual {v9, v12, v10, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->d(J)V

    const-string v10, "cache_time_out"

    const-wide/16 v11, 0xbb8

    invoke-virtual {v9, v10, v11, v12}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->a(J)V

    const-string v10, "pre_load_sort_control"

    invoke-virtual {v9, v10, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v10

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->c(I)V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v10

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->h()I

    move-result v12

    invoke-virtual {v10, v11, v12}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->b(Ljava/lang/String;I)V

    const-string v10, "pre_show_sort_control"

    invoke-virtual {v9, v10, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v10

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->d(I)V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v10

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->i()I

    move-result v12

    invoke-virtual {v10, v11, v12}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->c(Ljava/lang/String;I)V

    const-string v10, "req_type"

    invoke-virtual {v9, v10, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v10

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->e(I)V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v10

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->j()I

    move-result v12

    invoke-virtual {v10, v11, v12}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->d(Ljava/lang/String;I)V

    const-string v10, "waterfall_abtest"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->d(Ljava/lang/String;)V

    :cond_0
    const-string v10, "adn_rit_conf"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    if-eqz v9, :cond_6

    const/4 v11, 0x0

    :goto_1
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v11, v12, :cond_6

    invoke-virtual {v9, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    new-instance v13, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-direct {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;-><init>()V

    const-string v14, "adn_name"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b(Ljava/lang/String;)V

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->l()I

    move-result v14

    if-ne v14, v3, :cond_1

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c()Ljava/lang/String;

    move-result-object v14

    const-string v15, "mintegral"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    goto/16 :goto_4

    :cond_1
    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->s()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->g(Ljava/lang/String;)V

    const-string v14, "adn_slot_id"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a(Ljava/lang/String;)V

    const-string v14, "req_biding_type"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v13, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b(I)V

    const-string v14, "slot_cpm"

    const-string v15, "0"

    invoke-virtual {v12, v14, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c(Ljava/lang/String;)V

    const-string v14, "exchange_rate"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d(Ljava/lang/String;)V

    const-string v14, "load_sort"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v13, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d(I)V

    const-string v14, "show_sort"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v13, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f(I)V

    const-string v14, "ad_expired_time"

    const v15, 0x1b7740

    invoke-virtual {v12, v14, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v14

    invoke-virtual {v13, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a(I)V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_if122/b;

    move-result-object v14

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a()I

    move-result v3

    invoke-virtual {v14, v15, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a(Ljava/lang/String;I)V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;

    move-result-object v3

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a()I

    move-result v15

    invoke-virtual {v3, v14, v15}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/a;->a(Ljava/lang/String;I)V

    const-string v3, "if_reuse_ads"

    invoke-virtual {v12, v3, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v13, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c(I)V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;

    move-result-object v3

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->h()I

    move-result v14

    invoke-virtual {v3, v12, v14}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_for12/bykvm_19do/a;->b(Ljava/lang/String;I)V

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->l()I

    move-result v3

    invoke-virtual {v13, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e(I)V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v12, Lcom/bytedance/ad/sdk/ad_mediation_sdk/R$string;->tt_mediation_format_adapter_name:I

    invoke-virtual {v3, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e(Ljava/lang/String;)V

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v3

    if-ne v3, v5, :cond_2

    invoke-virtual {v8, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->a(Z)V

    invoke-virtual {v8, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->a(I)V

    :goto_2
    invoke-virtual {v13, v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d(I)V

    invoke-virtual {v13, v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f(I)V

    goto :goto_3

    :cond_2
    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v3

    if-ne v3, v4, :cond_3

    invoke-virtual {v8, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->a(Z)V

    invoke-virtual {v8, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->a(I)V

    goto :goto_2

    :cond_3
    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d()I

    move-result v3

    const/16 v12, 0x64

    if-ne v3, v12, :cond_4

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->i()I

    move-result v3

    add-int/lit16 v3, v3, -0x12c

    invoke-virtual {v13, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d(I)V

    goto :goto_3

    :cond_4
    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->c()D

    move-result-wide v14

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e()D

    move-result-wide v16

    cmpg-double v3, v14, v16

    if-gez v3, :cond_5

    invoke-virtual {v13}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e()D

    move-result-wide v14

    invoke-virtual {v8, v14, v15}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->a(D)V

    :cond_5
    :goto_3
    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/lit8 v11, v11, 0x1

    const/4 v3, 0x3

    goto/16 :goto_1

    :cond_6
    invoke-static {v10}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v8, v10}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->a(Ljava/util/List;)V

    :cond_7
    if-eqz v8, :cond_8

    iget-object v3, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->k()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x3

    goto/16 :goto_0

    :cond_9
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_a
    :goto_5
    :pswitch_0
    const/16 v1, 0x62

    packed-switch v1, :pswitch_data_0

    goto :goto_5

    :goto_6
    :pswitch_1
    const/16 v1, 0x14

    mul-int v1, v1, v1

    const/16 v3, 0xd

    mul-int v3, v3, v3

    mul-int/lit8 v3, v3, 0x22

    sub-int/2addr v1, v3

    const/4 v3, -0x1

    if-ne v1, v3, :cond_a

    :pswitch_2
    const/16 v1, 0xf

    mul-int v3, v1, v1

    mul-int/lit8 v3, v3, 0xf

    const/16 v6, 0x20

    mul-int v7, v6, v6

    mul-int/lit8 v7, v7, 0x20

    add-int/2addr v3, v7

    const/16 v7, 0x18

    mul-int v8, v7, v7

    mul-int/lit8 v8, v8, 0x18

    add-int/2addr v3, v8

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x18

    const/4 v6, 0x3

    mul-int/lit8 v1, v1, 0x3

    if-ge v3, v1, :cond_b

    goto :goto_8

    :cond_b
    :goto_7
    const/16 v1, 0x5e

    packed-switch v1, :pswitch_data_1

    goto :goto_7

    :pswitch_3
    const/16 v1, 0x30

    add-int/2addr v1, v5

    mul-int/lit8 v1, v1, 0x30

    rem-int/2addr v1, v4

    if-eqz v1, :cond_a

    goto :goto_6

    :pswitch_4
    const/4 v1, 0x4

    mul-int v3, v1, v1

    const/16 v7, 0x2d

    mul-int v8, v7, v7

    add-int/2addr v3, v8

    mul-int/lit8 v1, v1, 0x2d

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v3, v1

    if-gez v3, :cond_c

    goto :goto_5

    :cond_c
    :goto_8
    :pswitch_5
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x60
        :pswitch_1
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    iget-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private a(Z)V
    .locals 3

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->k()Z

    move-result v0

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c$a;

    invoke-direct {v2, p0, v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;ZZ)V

    invoke-static {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->a(Landroid/content/Context;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_int108/c;)V

    return-void
.end method

.method private b(I)Ljava/lang/Long;
    .locals 5

    :pswitch_0
    const/16 v0, 0x5f

    const-wide/16 v1, 0x2710

    packed-switch p1, :pswitch_data_0

    :pswitch_1
    goto :goto_1

    :pswitch_2
    const-wide/32 v0, 0x927c0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :pswitch_3
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :goto_1
    :pswitch_4
    const/16 v3, 0x5e

    const/16 v4, 0x7d

    :goto_2
    packed-switch v3, :pswitch_data_1

    goto :goto_1

    :pswitch_5
    packed-switch v4, :pswitch_data_2

    goto :goto_3

    :pswitch_6
    const-wide/16 v0, 0x1388

    goto :goto_0

    :goto_3
    :pswitch_7
    packed-switch v4, :pswitch_data_3

    goto :goto_4

    :pswitch_8
    const/16 v3, 0x27

    if-ne v4, v3, :cond_0

    :pswitch_9
    const/16 v3, 0x5f

    const/16 v4, 0x5f

    goto :goto_2

    :cond_0
    :goto_4
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_9
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_8
        :pswitch_5
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5e
        :pswitch_9
        :pswitch_6
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x37
        :pswitch_0
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;)V
    .locals 0

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->w()V

    return-void
.end method

.method private c(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 5

    if-eqz p1, :cond_2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "app_id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "app_key"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    invoke-direct {v4, v3, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->l:Ljava/util/Map;

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method private d(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    if-eqz p1, :cond_8

    :try_start_0
    const-string v0, "ad_event_control"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "ad_event_control"

    :try_start_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v0, "type_banner_control"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v2, "type_banner_control"

    :try_start_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v0, "type_interaction_control"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const-string v2, "type_interaction_control"

    :try_start_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    const-string v0, "type_splash_control"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const-string v2, "type_splash_control"

    :try_start_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    const-string v0, "type_feed_control"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    const-string v2, "type_feed_control"

    :try_start_5
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    const-string v0, "type_reward_control"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const-string v2, "type_reward_control"

    :try_start_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    const-string v0, "type_full_control"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    const-string v2, "type_full_control"

    :try_start_7
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    const-string v0, "type_native_control"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    const-string v2, "type_native_control"

    :try_start_8
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    return-object p1

    :catch_0
    :cond_8
    const/4 p1, 0x0

    return-object p1
.end method

.method private e(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, "fetch_ad_type"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->i:I

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private v()Z
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    const-string v1, "active_control"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private w()V
    .locals 3

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->l()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bytedance/msdk/api/TTSettingConfigCallback;

    invoke-interface {v2}, Lcom/bytedance/msdk/api/TTSettingConfigCallback;->configLoad()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    return-object p1
.end method

.method public a()V
    .locals 7

    move-object v0, p0

    :cond_0
    :goto_0
    const/16 v1, 0x5f

    const/4 v2, 0x1

    const/16 v3, 0x22

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    const/16 v1, 0x3d

    mul-int v1, v1, v1

    const/16 v4, 0x15

    mul-int v4, v4, v4

    mul-int/lit8 v4, v4, 0x22

    sub-int/2addr v1, v4

    const/4 v3, -0x1

    if-ne v1, v3, :cond_1

    goto :goto_0

    :goto_1
    const/16 v1, 0x26

    rsub-int/lit8 v4, v2, 0x26

    mul-int/lit8 v4, v4, 0x26

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v2

    mul-int v4, v4, v1

    rem-int/lit8 v4, v4, 0x6

    if-eqz v4, :cond_0

    :pswitch_1
    const/16 v1, 0x1b

    mul-int v4, v1, v1

    mul-int v5, v3, v3

    add-int/2addr v4, v5

    const/16 v5, 0x11

    mul-int v6, v5, v5

    add-int/2addr v4, v6

    const/16 v6, 0x1b

    mul-int/lit8 v6, v6, 0x22

    const/16 v3, 0x22

    mul-int/lit8 v3, v3, 0x11

    add-int/2addr v6, v3

    mul-int/lit8 v1, v1, 0x11

    add-int/2addr v6, v1

    if-ge v4, v6, :cond_2

    :cond_1
    :pswitch_2
    move-object v1, v0

    goto :goto_3

    :cond_2
    :goto_2
    const/16 v1, 0xa

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0xa

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    goto :goto_0

    :goto_3
    const/16 v0, 0x46

    packed-switch v0, :pswitch_data_1

    goto :goto_3

    :pswitch_3
    move-object v0, v1

    goto :goto_2

    :pswitch_4
    move-object v0, v1

    goto :goto_0

    :pswitch_5
    const-string v0, "tt_sdk_settings"

    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object v0

    const-string v3, "etag"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a:Ljava/lang/String;

    const-string v3, "max_age"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->c(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->b:J

    const-string v3, "max_expire_time"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->c(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->c:J

    const-string v3, "ab_version"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->d:Ljava/lang/String;

    const-string v3, "ab_params"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->e:Ljava/lang/String;

    const-string v3, "country"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->f:Ljava/lang/String;

    const-string v3, "if_test"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;)I

    move-result v3

    iput v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->g:I

    const-string v3, "network_permission"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;)I

    move-result v3

    iput v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->h:I

    const-string v3, "app_abtest"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "app_abtest"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->j:Ljava/lang/String;

    :cond_3
    const-string v3, "network_conf"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    :cond_4
    const-string v3, "adn_init_conf"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "{"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->c(Lorg/json/JSONObject;)Ljava/lang/String;

    :cond_6
    const-string v3, "rit_conf"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "{"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_7
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Lorg/json/JSONArray;)Ljava/lang/String;

    :cond_8
    const-string v3, "module_control"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->d(Lorg/json/JSONObject;)Ljava/lang/String;

    :cond_9
    const-string v3, "all_active_control"

    invoke-virtual {v0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v3, v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v4, "active_control"

    :try_start_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    :cond_a
    invoke-direct {v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Z)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x46
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/bytedance/msdk/api/TTSettingConfigCallback;->configLoad()V

    return-void

    :cond_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object p1

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    move-result-object p1

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b()V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 5

    if-eqz p1, :cond_1

    const-string v0, "state_code"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x7534

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_sdk_settings"

    invoke-static {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object v0

    const-string v1, "max_age"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->b:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->b:J

    add-long/2addr v1, v3

    iput-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->c:J

    const-string p1, "max_age"

    invoke-virtual {v0, p1, v3, v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;J)V

    iget-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->c:J

    const-string p1, "max_expire_time"

    invoke-virtual {v0, p1, v1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;J)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->l()I

    move-result p1

    if-eq p1, p2, :cond_2

    return v1

    :cond_2
    :goto_0
    return v2
.end method

.method public b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public b()V
    .locals 2

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_sdk_settings"

    invoke-static {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a:Ljava/lang/String;

    return-void
.end method

.method public b(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->n:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public b(Lorg/json/JSONObject;)V
    .locals 12

    if-nez p1, :cond_8

    :goto_0
    :pswitch_0
    const/4 p1, -0x1

    const/16 v0, 0x54

    const/16 v1, 0xd

    const/16 v2, 0x27

    const/16 v3, 0x22

    const/4 v4, 0x1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    const/16 v5, 0x27

    const/16 v6, 0xd

    :goto_1
    const/16 v7, 0x1b

    if-ge v5, v7, :cond_0

    goto :goto_3

    :cond_0
    :goto_2
    mul-int v7, v0, v0

    const/16 v8, 0x1a

    mul-int/lit8 v8, v8, 0x1a

    mul-int/lit8 v8, v8, 0x22

    sub-int/2addr v7, v8

    if-ne v7, p1, :cond_1

    :goto_3
    const/16 v7, 0xa

    add-int/2addr v7, v4

    mul-int/lit8 v7, v7, 0xa

    rem-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_3

    goto :goto_4

    :cond_1
    :pswitch_2
    packed-switch v2, :pswitch_data_1

    goto :goto_6

    :pswitch_3
    const/16 v5, 0x27

    const/16 v6, 0xd

    goto :goto_5

    :pswitch_4
    const/16 v5, 0x27

    const/16 v6, 0xd

    :goto_4
    mul-int v7, v3, v3

    mul-int v8, v2, v2

    mul-int/lit8 v8, v8, 0x22

    sub-int/2addr v7, v8

    if-ne v7, p1, :cond_7

    :goto_5
    const/16 v7, 0x21

    mul-int/lit8 v7, v7, 0x21

    const/16 v8, 0x8

    mul-int/lit8 v8, v8, 0x8

    mul-int/lit8 v8, v8, 0x22

    sub-int/2addr v7, v8

    if-ne v7, p1, :cond_4

    goto/16 :goto_a

    :goto_6
    :pswitch_5
    const/16 v5, 0x1e

    add-int/2addr v5, v4

    mul-int/lit8 v5, v5, 0x1e

    rem-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_2

    const/16 v5, 0x27

    const/16 v6, 0xd

    goto :goto_8

    :cond_2
    const/16 v5, 0x27

    const/16 v6, 0xd

    :cond_3
    :goto_7
    packed-switch v1, :pswitch_data_2

    goto :goto_7

    :pswitch_6
    const/16 v7, 0x31

    mul-int v8, v7, v7

    const/16 v9, 0xb

    mul-int v10, v9, v9

    add-int/2addr v8, v10

    const/16 v10, 0x12

    mul-int v11, v10, v10

    add-int/2addr v8, v11

    const/16 v11, 0x31

    mul-int/lit8 v11, v11, 0xb

    mul-int/lit8 v9, v9, 0x12

    add-int/2addr v11, v9

    mul-int/lit8 v7, v7, 0x12

    add-int/2addr v11, v7

    if-ge v8, v11, :cond_7

    :cond_4
    :pswitch_7
    packed-switch v6, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_8
    rsub-int/lit8 v7, v4, 0x41

    mul-int/lit8 v7, v7, 0x41

    const/16 v8, 0x41

    mul-int/lit8 v8, v8, 0x2

    sub-int/2addr v8, v4

    mul-int v7, v7, v8

    rem-int/lit8 v7, v7, 0x6

    if-eqz v7, :cond_3

    goto/16 :goto_1

    :pswitch_9
    const/16 v7, 0x2d

    rsub-int/lit8 v8, v4, 0x2d

    mul-int/lit8 v8, v8, 0x2d

    mul-int/lit8 v7, v7, 0x2

    sub-int/2addr v7, v4

    mul-int v8, v8, v7

    rem-int/lit8 v8, v8, 0x6

    if-eqz v8, :cond_6

    :pswitch_a
    if-ge v5, v0, :cond_5

    goto :goto_9

    :cond_5
    :goto_8
    const/16 v7, 0x52

    mul-int v8, v7, v7

    const/16 v9, 0xf

    mul-int v10, v9, v9

    add-int/2addr v8, v10

    const/4 v10, 0x4

    mul-int v11, v10, v10

    add-int/2addr v8, v11

    const/16 v11, 0x52

    mul-int/lit8 v11, v11, 0xf

    mul-int/lit8 v9, v9, 0x4

    add-int/2addr v11, v9

    mul-int/lit8 v7, v7, 0x4

    add-int/2addr v11, v7

    if-ge v8, v11, :cond_3

    :goto_9
    const/16 v7, 0x3e

    add-int/2addr v7, v4

    mul-int/lit8 v7, v7, 0x3e

    rem-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_7

    goto/16 :goto_2

    :cond_6
    const/16 v5, -0x3e

    goto/16 :goto_1

    :cond_7
    :goto_a
    :pswitch_b
    return-void

    :cond_8
    const/4 v0, 0x0

    :try_start_0
    const-string v1, "etag"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a:Ljava/lang/String;

    const-string v1, "max_age"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->b:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->b:J

    const/4 v5, 0x0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->c:J

    const-string v1, "ab_version"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->d:Ljava/lang/String;

    const-string v1, "ab_params"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->e:Ljava/lang/String;

    const-string v1, "country"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->f:Ljava/lang/String;

    const-string v1, "if_test"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->g:I

    const-string v1, "if_get_detail_return"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->h:I

    const-string v1, "app_abtest"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->j:Ljava/lang/String;

    const-string v1, "innerLog"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->k:I

    const-string v1, "adn_init_conf"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->c(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "poor_network_config"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {p0, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "rit_conf"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-direct {p0, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Lorg/json/JSONArray;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "module_disable_control"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->d(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v4, "tt_sdk_settings"

    :try_start_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v5, "etag"

    :try_start_2
    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v5, "max_age"

    :try_start_3
    iget-wide v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->b:J

    invoke-virtual {v4, v5, v6, v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v5, "max_expire_time"

    :try_start_4
    iget-wide v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->c:J

    invoke-virtual {v4, v5, v6, v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string v5, "ab_version"

    :try_start_5
    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->d:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v5, "ab_params"

    :try_start_6
    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->e:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v5, "country"

    :try_start_7
    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->f:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-string v5, "if_test"

    :try_start_8
    iget v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->g:I

    invoke-virtual {v4, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const-string v5, "network_permission"

    :try_start_9
    iget v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->h:I

    invoke-virtual {v4, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;I)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const-string v5, "app_abtest"

    :try_start_a
    iget-object v6, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->j:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "module_control"

    invoke-virtual {v4, v5, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_9

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/b;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_9

    const-string v1, "adn_init_conf"

    invoke-virtual {v4, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    if-eqz v3, :cond_a

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/b;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_a

    const-string v1, "rit_conf"

    invoke-virtual {v4, v1, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    if-eqz v2, :cond_b

    const-string p1, "network_conf"

    invoke-virtual {v4, p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_b

    :catchall_0
    move-exception p1

    :try_start_b
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :cond_b
    :goto_b
    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Z)V

    return-void

    :catchall_1
    move-exception p1

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Z)V

    throw p1

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x25
        :pswitch_4
        :pswitch_3
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0xd
        :pswitch_6
        :pswitch_7
        :pswitch_0
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0xa
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public c(Ljava/lang/String;)J
    .locals 2

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->b(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->b()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0xbb8

    return-wide v0
.end method

.method public c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->l:Ljava/util/Map;

    return-object v0
.end method

.method public d()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 3

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    const/4 v1, 0x1

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;

    if-nez p1, :cond_2

    return v2

    :cond_2
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->q()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    if-nez p1, :cond_3

    goto :goto_0

    :cond_3
    return v1

    :cond_4
    :goto_0
    return v2

    :cond_5
    :goto_1
    return v1
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->j:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->l:Ljava/util/Map;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->f:Ljava/lang/String;

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 4

    const-string v0, "tt_sdk_settings"

    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "active_control"

    const/4 v2, 0x1

    :try_start_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {p1, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "all_active_control"

    :goto_0
    invoke-virtual {v0, p1, v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->b(Ljava/lang/String;Z)V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v1, "active_control"

    const/4 v2, 0x0

    :try_start_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {p1, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "all_active_control"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tt_sdk_settings"

    invoke-static {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object v0

    const-string v1, "etag"

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .locals 2

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public i()Z
    .locals 3

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->v()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    const-string v2, "ad_event_control"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public j()Z
    .locals 3

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->v()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    const-string v2, "type_banner_control"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public k()Z
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->m:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public m()Z
    .locals 3

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->v()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    const-string v2, "type_feed_control"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public n()Z
    .locals 3

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->v()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    const-string v2, "type_full_control"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public o()Z
    .locals 3

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->v()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    const-string v2, "type_interaction_control"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public p()Z
    .locals 3

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->v()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    const-string v2, "type_native_control"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public q()Z
    .locals 2

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public r()Z
    .locals 3

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->v()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    const-string v2, "type_reward_control"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public s()Z
    .locals 3

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->v()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->o:Ljava/util/Map;

    const-string v2, "type_splash_control"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public t()Z
    .locals 2

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public u()Z
    .locals 2

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
