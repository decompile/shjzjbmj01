.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;
.super Ljava/lang/Object;
.source "SdkSettingsHelper.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Lcom/bytedance/msdk/adapter/util/WeakHandler$IHandler;


# static fields
.field private static final h:Ljava/util/concurrent/atomic/AtomicLong;

.field private static volatile i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final g:Lcom/bytedance/msdk/adapter/util/WeakHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->h:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method private constructor <init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-nez p1, :cond_0

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b:Landroid/content/Context;

    new-instance p1, Lcom/bytedance/msdk/adapter/util/WeakHandler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Lcom/bytedance/msdk/adapter/util/WeakHandler;-><init>(Landroid/os/Looper;Lcom/bytedance/msdk/adapter/util/WeakHandler$IHandler;)V

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->g:Lcom/bytedance/msdk/adapter/util/WeakHandler;

    return-void
.end method

.method public static a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;
    .locals 1

    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    if-nez v0, :cond_1

    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    if-nez v0, :cond_0

    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    invoke-direct {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;)V

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    :cond_0
    const-class p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p0

    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->i:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    return-object p0
.end method

.method private a(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 0

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/x;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v0

    :cond_3
    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    iget-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;Lorg/json/JSONObject;)V
    .locals 0

    invoke-direct {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b(Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;)Lcom/bytedance/msdk/adapter/util/WeakHandler;
    .locals 0

    iget-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->g:Lcom/bytedance/msdk/adapter/util/WeakHandler;

    return-object p0
.end method

.method private b(Lorg/json/JSONObject;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    const-string v0, "remote_log_enabled"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/bytedance/msdk/adapter/util/Logger;->openDebugMode()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :cond_1
    return-void
.end method

.method static synthetic c(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;
    .locals 0

    iget-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;

    return-object p0
.end method

.method static synthetic d(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    iget-object p0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method private d()Z
    .locals 6

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b:Landroid/content/Context;

    const-string v1, "tt_sdk_settings"

    invoke-static {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;

    move-result-object v0

    const-string v1, "max_expire_time"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/q;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v4, v0

    if-lez v2, :cond_0

    const-string v2, "SdkSettingsHelper"

    const-string v4, "setting \u7f13\u5b58\u8fc7\u671f\uff0c\u518d\u6b21\u53d1\u8d77\u8bf7\u6c42..."

    invoke-static {v2, v4}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v4, v0

    if-lez v2, :cond_1

    const/4 v3, 0x1

    :cond_1
    return v3
.end method

.method private static e()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "pangle"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "gdt"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "admob"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "unity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "baidu"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "sigmob"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "mintegral"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "ks"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method static synthetic e(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->h()V

    return-void
.end method

.method private f()Lorg/json/JSONObject;
    .locals 5

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->c()Ljava/util/Map;

    move-result-object v1

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->e()Ljava/util/List;

    move-result-object v2

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/bytedance/msdk/adapter/config/DefaultAdapterClasses;->getClassNameByAdnName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;

    if-eqz v4, :cond_0

    :try_start_0
    invoke-interface {v4}, Lcom/bytedance/msdk/adapter/config/ITTAdapterConfiguration;->getNetworkSdkVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private g()Lorg/json/JSONObject;
    .locals 11

    move-object v0, p0

    :goto_0
    :pswitch_0
    const/16 v1, 0x53

    const/16 v2, 0x39

    const/4 v3, 0x1

    const/16 v4, 0x11

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    move-object v6, v0

    const/16 v0, 0x39

    const/16 v5, 0x53

    :goto_1
    const/16 v7, 0x11

    add-int/2addr v7, v3

    mul-int/lit8 v7, v7, 0x11

    rem-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_1

    :cond_0
    move-object v0, v6

    goto :goto_4

    :cond_1
    :goto_2
    const/4 v7, -0x1

    const/16 v8, 0x22

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_7

    :pswitch_2
    const/16 v9, 0xd

    mul-int v9, v9, v9

    const/16 v10, 0x13

    mul-int v10, v10, v10

    mul-int/lit8 v10, v10, 0x22

    sub-int/2addr v9, v10

    if-ne v9, v7, :cond_5

    goto :goto_3

    :pswitch_3
    packed-switch v5, :pswitch_data_2

    goto/16 :goto_6

    :goto_3
    :pswitch_4
    const/16 v9, 0xa

    add-int/2addr v9, v3

    mul-int/lit8 v9, v9, 0xa

    rem-int/lit8 v9, v9, 0x2

    if-eqz v9, :cond_0

    :pswitch_5
    const/16 v9, 0x3d

    mul-int v9, v9, v9

    const/16 v10, 0xc

    mul-int v10, v10, v10

    mul-int/lit8 v10, v10, 0x22

    sub-int/2addr v9, v10

    if-ne v9, v7, :cond_5

    :pswitch_6
    const/16 v7, 0x1b

    mul-int v9, v7, v7

    mul-int v10, v8, v8

    add-int/2addr v9, v10

    mul-int v10, v4, v4

    add-int/2addr v9, v10

    const/16 v10, 0x1b

    mul-int/lit8 v10, v10, 0x22

    const/16 v8, 0x22

    mul-int/lit8 v8, v8, 0x11

    add-int/2addr v10, v8

    mul-int/lit8 v7, v7, 0x11

    add-int/2addr v10, v7

    if-ge v9, v10, :cond_0

    goto :goto_1

    :goto_4
    :pswitch_7
    const/16 v5, 0x1e

    packed-switch v5, :pswitch_data_3

    goto :goto_4

    :pswitch_8
    const/16 v5, 0x44

    mul-int v6, v5, v5

    mul-int/lit8 v6, v6, 0x44

    const/16 v7, 0xe

    mul-int v8, v7, v7

    mul-int/lit8 v8, v8, 0xe

    add-int/2addr v6, v8

    mul-int v8, v4, v4

    mul-int/lit8 v8, v8, 0x11

    add-int/2addr v6, v8

    mul-int/lit8 v5, v5, 0xe

    mul-int/lit8 v5, v5, 0x11

    mul-int/lit8 v5, v5, 0x3

    if-ge v6, v5, :cond_2

    :pswitch_9
    move-object v6, v0

    const/16 v0, 0x39

    const/16 v5, 0x53

    goto :goto_2

    :cond_2
    :pswitch_a
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "ts"

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "log_did"

    :try_start_1
    iget-object v4, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b:Landroid/content/Context;

    invoke-static {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "media_sdk_version"

    const-string v4, "2.6.0.0"

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v2, "app_id"

    :try_start_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v4

    invoke-virtual {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const-string v2, "package_name"

    :try_start_3
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/x;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const-string v2, "app_version"

    :try_start_4
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/x;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    const-string v2, "imei_md5"

    :try_start_5
    iget-object v4, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b:Landroid/content/Context;

    invoke-static {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const-string v2, "oaid"

    :try_start_6
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/n;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    const-string v2, "gaid"

    :try_start_7
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;

    move-result-object v4

    invoke-virtual {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    const-string v2, "conn_type"

    :try_start_8
    iget-object v4, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b:Landroid/content/Context;

    invoke-static {v4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/m;->b(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v2, "os"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    const-string v2, "os_version"

    :try_start_9
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    const-string v2, "vendor"

    :try_start_a
    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    const-string v2, "model"

    :try_start_b
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    const-string v2, "mcc"

    :try_start_c
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/r;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    const-string v2, "mnc"

    :try_start_d
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/r;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->r()Z

    move-result v2
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0

    if-nez v2, :cond_3

    const-string v2, "etag"

    :try_start_e
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v3

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0

    :cond_3
    const-string v2, "publisher_did"

    :try_start_f
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v3

    invoke-virtual {v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v2, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b:Landroid/content/Context;

    invoke-static {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;

    move-result-object v2
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_0

    if-eqz v2, :cond_4

    const-string v3, "latitude"

    :try_start_10
    iget v4, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;->a:F

    float-to-double v4, v4

    invoke-virtual {v1, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    const-string v3, "longitude"

    :try_start_11
    iget v2, v2, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;->b:F

    float-to-double v4, v2

    invoke-virtual {v1, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0

    :cond_4
    const-string v2, "adn_version_list"

    :try_start_12
    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->f()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_5
    return-object v1

    :cond_5
    :goto_6
    :pswitch_b
    move-object v0, v6

    goto/16 :goto_0

    :goto_7
    const/16 v0, 0x27

    const/16 v5, 0x3b

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_1
        :pswitch_7
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x27
        :pswitch_3
        :pswitch_b
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x39
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1e
        :pswitch_a
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private h()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->g:Lcom/bytedance/msdk/adapter/util/WeakHandler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->g:Lcom/bytedance/msdk/adapter/util/WeakHandler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->g:Lcom/bytedance/msdk/adapter/util/WeakHandler;

    const-wide/32 v2, 0x927c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const-string v0, "SdkSettingsHelper"

    const-string v1, "setting \u91cd\u8bd5\u6b21\u6570\u8d85\u8fc73\u6b21\u5c31\u95f4\u9694\u65f6\u95f4:600000\u6beb\u79d2\u540e\u518d\u6267\u884c...."

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SdkSettingsHelper"

    const-string v1, "setting \u6b63\u5728\u52a0\u8f7d\uff0c\u65e0\u9700\u518d\u6b21\u53d1\u8d77\u8bf7\u6c42..."

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v1, "SdkSettingsHelper"

    const-string v2, "load sdk settings error: "

    invoke-static {v1, v2, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public b()V
    .locals 10

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, -0x1

    const/16 v1, 0x31

    const/4 v2, 0x1

    const/16 v3, 0x22

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    const/16 v4, 0xd

    mul-int v4, v4, v4

    const/16 v5, 0x13

    mul-int v5, v5, v5

    mul-int/lit8 v5, v5, 0x22

    sub-int/2addr v4, v5

    if-ne v4, v0, :cond_1

    :pswitch_2
    const/16 v0, 0xa

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0xa

    rem-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_3
    const/16 v4, 0x34

    :goto_2
    const/16 v5, 0x60

    packed-switch v5, :pswitch_data_1

    goto :goto_2

    :pswitch_4
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b:Landroid/content/Context;

    invoke-static {}, Lcom/bytedance/embedapplog/AppLog;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/j;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    :try_start_0
    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->d()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "SdkSettingsHelper"

    const-string v1, "setting \u7f13\u5b58\u672a\u8fc7\u671f\uff0c\u65e0\u9700\u518d\u6b21\u53d1\u8d77\u8bf7\u6c42..."

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_3
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "SdkSettingsHelper"

    const-string v1, "setting \u6b63\u5728\u52a0\u8f7d\uff0c\u65e0\u9700\u518d\u6b21\u53d1\u8d77\u8bf7\u6c42..."

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_4
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    const-string v1, "SdkSettingsHelper"

    const-string v2, "load sdk settings error: "

    invoke-static {v1, v2, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_3
    return-void

    :pswitch_5
    const/16 v5, 0x39

    mul-int v6, v5, v5

    mul-int v7, v3, v3

    add-int/2addr v6, v7

    const/16 v7, 0xc

    mul-int v8, v7, v7

    add-int/2addr v6, v8

    const/16 v8, 0x39

    mul-int/lit8 v8, v8, 0x22

    const/16 v9, 0x22

    mul-int/lit8 v9, v9, 0xc

    add-int/2addr v8, v9

    mul-int/lit8 v5, v5, 0xc

    add-int/2addr v8, v5

    if-ge v6, v8, :cond_0

    :pswitch_6
    const/16 v5, 0x31

    :goto_4
    const/16 v6, 0x12

    packed-switch v4, :pswitch_data_2

    goto :goto_6

    :pswitch_7
    mul-int v4, v6, v6

    const/16 v5, 0x23

    mul-int/lit8 v5, v5, 0x23

    mul-int/lit8 v5, v5, 0x22

    sub-int/2addr v4, v5

    if-ne v4, v0, :cond_1

    goto :goto_5

    :pswitch_8
    packed-switch v5, :pswitch_data_3

    goto/16 :goto_0

    :goto_5
    :pswitch_9
    const/16 v4, 0x1a

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x1a

    const/16 v7, 0x9

    mul-int v8, v7, v7

    mul-int/lit8 v8, v8, 0x9

    add-int/2addr v5, v8

    const/16 v8, 0xf

    mul-int v9, v8, v8

    mul-int/lit8 v9, v9, 0xf

    add-int/2addr v5, v9

    mul-int/lit8 v4, v4, 0x9

    mul-int/lit8 v4, v4, 0xf

    mul-int/lit8 v4, v4, 0x3

    if-ge v5, v4, :cond_0

    :pswitch_a
    const/4 v4, 0x0

    rsub-int/lit8 v5, v2, 0x0

    mul-int/lit8 v5, v5, 0x0

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v4, v2

    mul-int v5, v5, v4

    rem-int/lit8 v5, v5, 0x6

    if-eqz v5, :cond_1

    :cond_5
    :pswitch_b
    rsub-int/lit8 v4, v2, 0x12

    mul-int/lit8 v4, v4, 0x12

    const/16 v5, 0x12

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v5, v2

    mul-int v4, v4, v5

    rem-int/lit8 v4, v4, 0x6

    if-eqz v4, :cond_5

    goto/16 :goto_1

    :goto_6
    const/16 v4, 0x37

    const/16 v5, 0x3d

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_5
        :pswitch_6
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x37
        :pswitch_8
        :pswitch_0
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3c
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;
    .locals 2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-object p0
.end method

.method public handleMsg(Landroid/os/Message;)V
    .locals 1

    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_new1/g;->a()V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    :try_start_0
    iget-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    :goto_0
    return-void
.end method

.method public run()V
    .locals 13

    :cond_0
    :goto_0
    :pswitch_0
    const/16 v0, 0x2a

    const/4 v1, 0x2

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x25

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    rem-int/2addr v0, v1

    if-eqz v0, :cond_0

    :pswitch_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/m;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->a:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;

    invoke-interface {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    :cond_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v0

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->k()Z

    move-result v3

    new-array v4, v2, [I

    const/4 v7, 0x0

    aput v7, v4, v7

    new-array v5, v2, [I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    if-nez v3, :cond_2

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->g:Lcom/bytedance/msdk/adapter/util/WeakHandler;

    const-wide/16 v10, 0xbb8

    invoke-virtual {v0, v1, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    invoke-direct {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->g()Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/c;->c()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;->a(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b$a;

    move-object v0, v12

    move-object v1, p0

    move-object v2, v5

    move-wide v5, v8

    invoke-direct/range {v0 .. v6}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;[IZ[IJ)V

    new-instance v6, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b$b;

    const/4 v2, 0x1

    move-object v0, v6

    move-object v3, v10

    move-object v4, v11

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b$b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/b;ILjava/lang/String;Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p$a;)V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object v0

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object v0

    invoke-virtual {v0, v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object v0

    invoke-virtual {v6, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/e;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    move-result-object v0

    sget-object v1, Lcom/bytedance/msdk/base/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    move-result-object v0

    invoke-virtual {v0, v7}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Z)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    move-result-object v0

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;)V

    :catchall_0
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x28
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
