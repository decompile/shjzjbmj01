.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;
.super Ljava/lang/Object;
.source "ServerBiddingHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "a"

.field private static volatile b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 2

    :goto_0
    const/16 v0, 0x5d

    const/16 v1, 0x5d

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x4

    if-gt v1, v0, :cond_0

    goto :goto_0

    :pswitch_1
    packed-switch v1, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch v1, :pswitch_data_2

    goto :goto_3

    :cond_0
    :pswitch_3
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a:Ljava/lang/String;

    return-object v0

    :pswitch_4
    const/16 v0, 0x5e

    const/16 v1, 0x4b

    goto :goto_1

    :goto_3
    const/16 v1, 0x5b

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lcom/bytedance/msdk/api/AdSlot;Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;I)Ljava/lang/String;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/msdk/api/AdSlot;",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;",
            "I)",
            "Ljava/lang/String;"
        }
    .end annotation

    move-object/from16 v1, p0

    const/16 v0, 0x1a

    const/16 v2, 0x9

    :goto_0
    const/16 v3, 0xe

    :goto_1
    packed-switch v3, :pswitch_data_0

    goto/16 :goto_a

    :pswitch_0
    const/16 v3, 0x16

    if-le v0, v3, :cond_0

    goto/16 :goto_a

    :cond_0
    :goto_2
    packed-switch v2, :pswitch_data_1

    goto/16 :goto_a

    :pswitch_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "request_id"

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const-string v0, "sdk_version"

    :try_start_1
    invoke-static {}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->getSdkVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const-string v0, "waterfall_id"

    :try_start_2
    invoke-virtual/range {p3 .. p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->t()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    const-string v0, "waterfall_version"

    :try_start_3
    invoke-virtual/range {p3 .. p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    const-string v0, "req_bidding_type"

    const/4 v4, 0x2

    :try_start_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-direct/range {p0 .. p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->b()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v4, "app_abtest"

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    invoke-virtual/range {p3 .. p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->s()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v4, "waterfall_abtest"

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_2
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sget-object v5, Lcom/bytedance/msdk/adapter/config/TTBaseAdapterConfiguration;->TT_MSDK_ADSLOT_INFO:Ljava/lang/String;

    move-object/from16 v6, p1

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p3 .. p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;->s()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/bytedance/msdk/adapter/pangle/PangleAdapterUtils;->updateData(Ljava/lang/String;)V

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_try19/b;->b(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    const/4 v7, 0x0

    :goto_3
    :try_start_5
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    if-ge v3, v0, :cond_6

    const/4 v8, 0x0

    move-object/from16 v9, p2

    :try_start_6
    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->p()Z

    move-result v10

    if-nez v10, :cond_3

    goto/16 :goto_6

    :cond_3
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c()Ljava/lang/String;

    move-result-object v11
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v8

    invoke-virtual {v8, v11}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->a(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;

    move-result-object v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-string v12, "tt_server_bidding"

    :try_start_8
    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "adnName:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "token="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "name"

    invoke-virtual {v10, v12, v11}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v12, "token"

    invoke-virtual {v10, v12, v13}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const-string v12, "slot_id"

    :try_start_9
    invoke-virtual {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v12, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v8, :cond_4

    const-string v0, "app_id"

    :try_start_a
    invoke-virtual {v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/a;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v0, v8}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_4
    invoke-virtual {v4, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v0

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v11, v8

    :goto_4
    :try_start_b
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "serverBiddingRequest-buildBiddingRequestBody:[adnName="

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "]-error:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    const-string v10, "TTMediationSDK"

    :try_start_c
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_5
    const-string v0, "pangle"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    if-eqz v0, :cond_5

    const/4 v7, 0x1

    :cond_5
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_6
    move v3, v7

    goto :goto_7

    :catch_0
    move-exception v0

    move v3, v7

    goto :goto_8

    :cond_7
    :goto_7
    :try_start_d
    const-string v0, "bidders"

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    if-nez v3, :cond_8

    const-string v0, "bid_request"

    :try_start_e
    invoke-direct/range {p0 .. p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->b(Lcom/bytedance/msdk/api/AdSlot;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_8
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "primerit_req_type"

    move/from16 v5, p4

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v4, "bid_request_ext"

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_1

    const-string v0, "serverBiddingRequest"

    :try_start_f
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serverBiddingRequest-requestBody="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1

    goto :goto_9

    :catch_1
    move-exception v0

    :goto_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ServerBiddingHelper#serverBiddingRequest getParam() error:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "tt_server_bidding"

    invoke-static {v4, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_9
    invoke-direct {v1, v2, v3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a(Lorg/json/JSONObject;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_2
    move-object/from16 v6, p1

    move-object/from16 v9, p2

    move/from16 v5, p4

    goto/16 :goto_2

    :goto_a
    :pswitch_3
    move-object/from16 v6, p1

    move-object/from16 v9, p2

    move/from16 v5, p4

    goto/16 :goto_0

    :pswitch_4
    move-object/from16 v6, p1

    move-object/from16 v9, p2

    move/from16 v5, p4

    const/16 v3, 0x10

    const/16 v0, -0x2c

    const/16 v2, 0x1e

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_4
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1d
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lorg/json/JSONObject;Z)Ljava/lang/String;
    .locals 4

    invoke-static {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/x;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "token_type"

    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    :goto_0
    const/16 p1, 0xc

    const/16 p2, 0x5f

    const/4 v0, -0x1

    const/16 v1, 0xf

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v2, 0x42

    mul-int v2, v2, v2

    const/16 v3, 0x10

    mul-int v3, v3, v3

    mul-int/lit8 v3, v3, 0x22

    sub-int/2addr v2, v3

    if-ne v2, v0, :cond_0

    :pswitch_1
    packed-switch p2, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 p1, 0x12

    mul-int p1, p1, p1

    const/16 p2, 0x23

    mul-int p2, p2, p2

    mul-int/lit8 p2, p2, 0x22

    sub-int/2addr p1, p2

    if-ne p1, v0, :cond_3

    goto :goto_2

    :goto_1
    packed-switch p1, :pswitch_data_2

    goto :goto_3

    :cond_1
    :goto_2
    :pswitch_3
    const/16 p1, 0x1a

    mul-int p2, p1, p1

    mul-int/lit8 p2, p2, 0x1a

    const/16 v0, 0x9

    mul-int v2, v0, v0

    mul-int/lit8 v2, v2, 0x9

    add-int/2addr p2, v2

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0xf

    add-int/2addr p2, v2

    mul-int/lit8 p1, p1, 0x9

    mul-int/lit8 p1, p1, 0xf

    mul-int/lit8 p1, p1, 0x3

    if-ge p2, p1, :cond_0

    :pswitch_4
    const/16 p1, 0x5a

    add-int/lit8 p1, p1, 0x1

    mul-int/lit8 p1, p1, 0x5a

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_0

    :pswitch_5
    const/16 p1, 0xf

    add-int/lit8 p1, p1, 0x1

    mul-int/lit8 p1, p1, 0xf

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_2
    :goto_3
    :pswitch_6
    const/16 p1, 0x27

    add-int/lit8 p1, p1, 0x1

    mul-int/lit8 p1, p1, 0x27

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_0

    :cond_3
    :pswitch_7
    const/16 p1, 0x46

    mul-int p2, p1, p1

    const/4 v0, 0x7

    mul-int v1, v0, v0

    add-int/2addr p2, v1

    const/4 v1, 0x4

    mul-int v2, v1, v1

    add-int/2addr p2, v2

    const/16 v2, 0x46

    mul-int/lit8 v2, v2, 0x7

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v2, v0

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr v2, p1

    if-ge p2, v2, :cond_2

    :pswitch_8
    const/4 p1, 0x0

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_6
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_7
        :pswitch_8
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x37
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Lcom/bytedance/msdk/api/AdSlot;)Lorg/json/JSONObject;
    .locals 9

    move-object v0, p1

    move-object p1, p0

    :goto_0
    const/16 v1, 0xf

    move-object v2, v0

    move-object v0, p1

    const/16 p1, 0xf

    :goto_1
    const/16 v3, 0xc

    const/16 v4, 0x5f

    const/4 v5, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x7

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_5

    :pswitch_0
    packed-switch v4, :pswitch_data_1

    goto :goto_2

    :pswitch_1
    const/16 p1, 0x12

    mul-int p1, p1, p1

    const/16 v3, 0x23

    mul-int v3, v3, v3

    mul-int/lit8 v3, v3, 0x22

    sub-int/2addr p1, v3

    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    goto :goto_3

    :cond_0
    :pswitch_2
    const/16 p1, 0x46

    mul-int v3, p1, p1

    mul-int v4, v7, v7

    add-int/2addr v3, v4

    const/4 v4, 0x4

    mul-int v5, v4, v4

    add-int/2addr v3, v5

    const/16 v5, 0x46

    mul-int/lit8 v5, v5, 0x7

    const/4 v6, 0x7

    mul-int/lit8 v6, v6, 0x4

    add-int/2addr v5, v6

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr v5, p1

    if-ge v3, v5, :cond_7

    goto/16 :goto_5

    :goto_2
    packed-switch v3, :pswitch_data_2

    goto/16 :goto_4

    :cond_1
    :goto_3
    :pswitch_3
    const/16 p1, 0x1a

    mul-int v3, p1, p1

    mul-int/lit8 v3, v3, 0x1a

    const/16 v4, 0x9

    mul-int v8, v4, v4

    mul-int/lit8 v8, v8, 0x9

    add-int/2addr v3, v8

    mul-int v8, v1, v1

    mul-int/lit8 v8, v8, 0xf

    add-int/2addr v3, v8

    mul-int/lit8 p1, p1, 0x9

    mul-int/lit8 p1, p1, 0xf

    mul-int/lit8 p1, p1, 0x3

    if-ge v3, p1, :cond_2

    :pswitch_4
    const/16 p1, 0x5a

    add-int/2addr p1, v6

    mul-int/lit8 p1, p1, 0x5a

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_8

    :pswitch_5
    const/16 p1, 0xf

    add-int/2addr p1, v6

    mul-int/lit8 p1, p1, 0xf

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_1

    :cond_2
    :pswitch_6
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "id"

    :try_start_1
    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v3, "adtype"

    invoke-virtual {p1, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v3, "pos"

    :try_start_2
    invoke-static {v1}, Lcom/bytedance/sdk/openadsdk/AdSlot;->getPosition(I)I

    move-result v4

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const-string v3, "accepted_size"

    :try_start_3
    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v4

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v8

    invoke-direct {v0, p1, v3, v4, v8}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a(Lorg/json/JSONObject;Ljava/lang/String;II)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const-string v0, "is_support_dpl"

    :try_start_4
    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->isSupportDeepLink()Z

    move-result v3

    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/AdSlot;->getAdCount()I

    move-result v0

    if-ge v0, v6, :cond_3

    const/4 v0, 0x1

    :cond_3
    if-le v0, v5, :cond_4

    const/4 v0, 0x3

    :cond_4
    if-eq v1, v7, :cond_5

    const/16 v2, 0x8

    if-ne v1, v2, :cond_6

    :cond_5
    const/4 v0, 0x1

    :cond_6
    const-string v1, "ad_count"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    return-object p1

    :cond_7
    :goto_4
    :pswitch_7
    move-object p1, v0

    move-object v0, v2

    goto/16 :goto_0

    :cond_8
    :goto_5
    :pswitch_8
    const/16 p1, 0xe

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_7
        :pswitch_6
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_2
        :pswitch_8
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x37
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    :cond_0
    :goto_0
    const/16 p1, 0x5f

    const/4 v0, 0x1

    const/16 v2, 0xf

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 p1, 0x12

    mul-int p1, p1, p1

    const/16 v3, 0x23

    mul-int/lit8 v3, v3, 0x23

    mul-int/lit8 v3, v3, 0x22

    sub-int/2addr p1, v3

    const/4 v3, -0x1

    if-ne p1, v3, :cond_3

    goto :goto_2

    :goto_1
    :pswitch_2
    packed-switch p1, :pswitch_data_2

    goto :goto_3

    :cond_1
    :goto_2
    :pswitch_3
    const/16 p1, 0x1a

    mul-int v3, p1, p1

    mul-int/lit8 v3, v3, 0x1a

    const/16 v4, 0x9

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x9

    add-int/2addr v3, v5

    mul-int v5, v2, v2

    mul-int/lit8 v5, v5, 0xf

    add-int/2addr v3, v5

    mul-int/lit8 p1, p1, 0x9

    mul-int/lit8 p1, p1, 0xf

    mul-int/lit8 p1, p1, 0x3

    if-ge v3, p1, :cond_0

    :pswitch_4
    const/16 p1, 0x50

    mul-int v3, p1, p1

    const/4 v4, 0x0

    mul-int v5, v4, v4

    add-int/2addr v3, v5

    add-int/2addr v3, v5

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v5, p1

    add-int/2addr v5, p1

    if-ge v3, v5, :cond_0

    :pswitch_5
    const/16 p1, 0xf

    add-int/2addr p1, v0

    mul-int/lit8 p1, p1, 0xf

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_1

    goto :goto_0

    :goto_3
    :pswitch_6
    const/16 p1, 0x1e

    rsub-int/lit8 v2, v0, 0x1e

    mul-int/lit8 v2, v2, 0x1e

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p1, v0

    mul-int v2, v2, p1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_0

    :cond_2
    const/16 p1, 0x27

    add-int/2addr p1, v0

    mul-int/lit8 p1, p1, 0x27

    rem-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_0

    :cond_3
    :pswitch_7
    const/16 p1, 0x46

    mul-int v2, p1, p1

    const/4 v3, 0x7

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    const/4 v4, 0x4

    mul-int v5, v4, v4

    add-int/2addr v2, v5

    const/16 v5, 0x46

    mul-int/lit8 v5, v5, 0x7

    mul-int/lit8 v3, v3, 0x4

    add-int/2addr v5, v3

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr v5, p1

    if-ge v2, v5, :cond_2

    :pswitch_8
    return-object v1

    :cond_4
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    :goto_4
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_6
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_7
        :pswitch_8
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x37
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;)V
    .locals 3

    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$d;

    invoke-direct {v0, p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$d;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;)V

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/util/ThreadHelper;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    :goto_0
    const/16 p1, 0x25

    const/16 p2, 0x36

    const/4 v0, 0x1

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 p1, 0x12

    mul-int p1, p1, p1

    const/16 p2, 0x23

    mul-int p2, p2, p2

    mul-int/lit8 p2, p2, 0x22

    sub-int/2addr p1, p2

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    :pswitch_2
    const/16 p1, 0x1a

    mul-int p2, p1, p1

    mul-int/lit8 p2, p2, 0x1a

    const/16 v1, 0x51

    mul-int/lit8 v1, v1, 0x9

    add-int/2addr p2, v1

    const/16 v1, 0xf

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0xf

    add-int/2addr p2, v2

    mul-int/lit8 p1, p1, 0x9

    mul-int/lit8 p1, p1, 0xf

    mul-int/lit8 p1, p1, 0x3

    if-ge p2, p1, :cond_1

    :pswitch_3
    const/16 p1, 0x28

    rsub-int/lit8 p2, v0, 0x28

    mul-int/lit8 p2, p2, 0x28

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p1, v0

    mul-int p2, p2, p1

    rem-int/lit8 p2, p2, 0x6

    goto :goto_0

    :goto_1
    :pswitch_4
    const/16 p1, 0x1e

    rsub-int/lit8 p2, v0, 0x1e

    mul-int/lit8 p2, p2, 0x1e

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p1, v0

    mul-int p2, p2, p1

    rem-int/lit8 p2, p2, 0x6

    if-eqz p2, :cond_0

    :cond_1
    :pswitch_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;Lcom/bytedance/msdk/api/AdError;)V
    .locals 3

    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$c;

    invoke-direct {v0, p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$c;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;Lcom/bytedance/msdk/api/AdError;)V

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/util/ThreadHelper;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    :goto_0
    const/16 p1, 0x25

    const/16 p2, 0x36

    const/4 v0, 0x1

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 p1, 0x12

    mul-int p1, p1, p1

    const/16 p2, 0x23

    mul-int p2, p2, p2

    mul-int/lit8 p2, p2, 0x22

    sub-int/2addr p1, p2

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    :pswitch_2
    const/16 p1, 0x1a

    mul-int p2, p1, p1

    mul-int/lit8 p2, p2, 0x1a

    const/16 v1, 0x51

    mul-int/lit8 v1, v1, 0x9

    add-int/2addr p2, v1

    const/16 v1, 0xf

    mul-int v2, v1, v1

    mul-int/lit8 v2, v2, 0xf

    add-int/2addr p2, v2

    mul-int/lit8 p1, p1, 0x9

    mul-int/lit8 p1, p1, 0xf

    mul-int/lit8 p1, p1, 0x3

    if-ge p2, p1, :cond_1

    :pswitch_3
    const/16 p1, 0x28

    rsub-int/lit8 p2, v0, 0x28

    mul-int/lit8 p2, p2, 0x28

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p1, v0

    mul-int p2, p2, p1

    rem-int/lit8 p2, p2, 0x6

    goto :goto_0

    :goto_1
    :pswitch_4
    const/16 p1, 0x1e

    rsub-int/lit8 p2, v0, 0x1e

    mul-int/lit8 p2, p2, 0x1e

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr p1, v0

    mul-int p2, p2, p1

    rem-int/lit8 p2, p2, 0x6

    if-eqz p2, :cond_0

    :cond_1
    :pswitch_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;)V

    :pswitch_0
    const/16 p0, 0x5e

    const/16 p1, 0x4b

    const/16 p2, 0x5d

    :goto_0
    packed-switch p0, :pswitch_data_0

    goto :goto_3

    :pswitch_1
    packed-switch p1, :pswitch_data_1

    :goto_1
    :pswitch_2
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_3
    return-void

    :goto_2
    const/16 p1, 0x5b

    goto :goto_1

    :goto_3
    :pswitch_4
    const/16 p0, 0x5d

    const/16 p1, 0x5d

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;Lcom/bytedance/msdk/api/AdError;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;Lcom/bytedance/msdk/api/AdError;)V

    :goto_0
    const/16 p0, 0x5d

    const/16 p1, 0x5d

    :goto_1
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 p0, 0x4

    if-gt p1, p0, :cond_0

    goto :goto_0

    :cond_0
    return-void

    :pswitch_1
    packed-switch p1, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch p1, :pswitch_data_2

    goto :goto_3

    :pswitch_3
    const/16 p0, 0x5e

    const/16 p1, 0x4b

    goto :goto_1

    :goto_3
    const/16 p1, 0x5b

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 5

    :cond_0
    :goto_0
    const/16 v0, 0x5c

    :goto_1
    const/16 v1, 0xe

    const/16 v2, 0xf

    :goto_2
    const/16 v3, 0xc

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    packed-switch v0, :pswitch_data_1

    goto :goto_3

    :pswitch_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/e;->b(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "latitude"

    :try_start_1
    iget v3, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;->a:F

    float-to-double v3, v3

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v2, "longitude"

    :try_start_2
    iget v0, v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/d;->b:F

    float-to-double v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v0, "geo"

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :cond_1
    return-void

    :pswitch_2
    const/16 v1, 0x48

    if-lt v0, v1, :cond_2

    goto :goto_4

    :cond_2
    :goto_3
    packed-switch v3, :pswitch_data_2

    goto :goto_4

    :cond_3
    :pswitch_3
    const/16 v0, 0x1a

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x1a

    const/16 v3, 0x9

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0x9

    add-int/2addr v1, v4

    mul-int v4, v2, v2

    mul-int/lit8 v4, v4, 0xf

    add-int/2addr v1, v4

    mul-int/lit8 v0, v0, 0x9

    mul-int/lit8 v0, v0, 0xf

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_0

    :pswitch_4
    const/16 v0, 0x5a

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x5a

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    :pswitch_5
    const/16 v0, 0xf

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0xf

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    goto :goto_0

    :goto_4
    :pswitch_6
    const/16 v0, 0x5f

    const/16 v1, 0xf

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_2
        :pswitch_6
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_6
        :pswitch_1
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x37
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;II)V
    .locals 3

    if-lez p3, :cond_0

    if-lez p4, :cond_0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    :try_start_0
    const-string v2, "width"

    invoke-virtual {v0, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p3, "height"

    invoke-virtual {v0, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    invoke-virtual {p1, p2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private b()Lorg/json/JSONObject;
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    :pswitch_0
    const/16 v2, 0x4a

    const/16 v3, 0x37

    :goto_0
    const/4 v4, 0x0

    packed-switch v2, :pswitch_data_0

    goto :goto_5

    :pswitch_1
    packed-switch v3, :pswitch_data_1

    :pswitch_2
    packed-switch v3, :pswitch_data_2

    goto :goto_4

    :pswitch_3
    const/16 v2, 0x12

    const/4 v3, 0x1

    packed-switch v3, :pswitch_data_3

    goto :goto_1

    :pswitch_4
    rsub-int/lit8 v5, v3, 0x0

    mul-int/lit8 v5, v5, 0x0

    const/4 v6, 0x0

    mul-int/lit8 v6, v6, 0x2

    sub-int/2addr v6, v3

    mul-int v5, v5, v6

    rem-int/lit8 v5, v5, 0x6

    if-eqz v5, :cond_3

    :pswitch_5
    rsub-int/lit8 v5, v3, 0x12

    mul-int/lit8 v5, v5, 0x12

    const/16 v6, 0x12

    mul-int/lit8 v6, v6, 0x2

    sub-int/2addr v6, v3

    mul-int v5, v5, v6

    rem-int/lit8 v5, v5, 0x6

    if-eqz v5, :cond_0

    :pswitch_6
    const/16 v1, 0x63

    mul-int v1, v1, v1

    mul-int v2, v2, v2

    mul-int/lit8 v2, v2, 0x22

    sub-int/2addr v1, v2

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    goto :goto_3

    :cond_0
    :goto_1
    :pswitch_7
    if-eqz v1, :cond_2

    :cond_1
    return-object v4

    :cond_2
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v4

    :goto_2
    return-object v0

    :cond_3
    :goto_3
    :pswitch_8
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->e()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;

    move-result-object v1

    invoke-virtual {v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_else10/c;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    :goto_4
    :pswitch_9
    const/16 v2, 0x49

    const/16 v3, 0x60

    goto :goto_0

    :goto_5
    const/16 v2, 0x48

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_9
        :pswitch_3
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x37
        :pswitch_8
        :pswitch_7
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3c
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private b(Lcom/bytedance/msdk/api/AdSlot;)Lorg/json/JSONObject;
    .locals 8

    move-object v0, p0

    :cond_0
    :goto_0
    const/4 v1, -0x1

    const/16 v2, 0x3d

    const/16 v3, 0xf

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :goto_1
    :pswitch_0
    const/16 v4, 0x12

    mul-int v4, v4, v4

    const/16 v5, 0x23

    mul-int v5, v5, v5

    mul-int/lit8 v5, v5, 0x22

    sub-int/2addr v4, v5

    if-ne v4, v1, :cond_2

    :goto_2
    :pswitch_1
    const/16 v4, 0x36

    packed-switch v4, :pswitch_data_1

    goto :goto_2

    :pswitch_2
    const/16 v4, 0x45

    mul-int v5, v4, v4

    const/16 v6, 0x16

    mul-int v7, v6, v6

    add-int/2addr v5, v7

    mul-int/lit8 v4, v4, 0x16

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v5, v4

    if-gez v5, :cond_1

    :pswitch_3
    mul-int v4, v2, v2

    const/16 v5, 0xc

    mul-int/lit8 v5, v5, 0xc

    mul-int/lit8 v5, v5, 0x22

    sub-int/2addr v4, v5

    goto :goto_4

    :cond_1
    :pswitch_4
    const/16 v4, 0x1a

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x1a

    const/16 v6, 0x9

    mul-int v7, v6, v6

    mul-int/lit8 v7, v7, 0x9

    add-int/2addr v5, v7

    mul-int v7, v3, v3

    mul-int/lit8 v7, v7, 0xf

    add-int/2addr v5, v7

    mul-int/lit8 v4, v4, 0x9

    mul-int/lit8 v4, v4, 0xf

    mul-int/lit8 v4, v4, 0x3

    if-ge v5, v4, :cond_4

    goto :goto_6

    :cond_2
    :goto_3
    const/16 v4, 0x46

    mul-int v5, v4, v4

    const/16 v6, 0x2c

    mul-int v7, v6, v6

    add-int/2addr v5, v7

    mul-int/lit8 v4, v4, 0x2c

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v5, v4

    if-gez v5, :cond_3

    goto :goto_1

    :cond_3
    :goto_4
    :pswitch_5
    move-object v4, p1

    move-object v5, v0

    :goto_5
    const/16 p1, 0x33

    const/16 v0, 0x60

    packed-switch v0, :pswitch_data_2

    goto :goto_5

    :pswitch_6
    packed-switch p1, :pswitch_data_3

    goto/16 :goto_9

    :pswitch_7
    move-object p1, v4

    move-object v0, v5

    goto :goto_8

    :pswitch_8
    move-object p1, v4

    move-object v0, v5

    goto :goto_7

    :pswitch_9
    move-object p1, v4

    move-object v0, v5

    :goto_6
    const/16 v4, 0x50

    mul-int v5, v4, v4

    const/4 v6, 0x0

    mul-int v7, v6, v6

    add-int/2addr v5, v7

    add-int/2addr v5, v7

    mul-int/lit8 v4, v4, 0x0

    add-int/2addr v7, v4

    add-int/2addr v7, v4

    if-ge v5, v7, :cond_0

    :goto_7
    const/16 v4, 0xf

    add-int/lit8 v4, v4, 0x1

    mul-int/lit8 v4, v4, 0xf

    rem-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_3

    :cond_4
    :goto_8
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "request_id"

    :try_start_0
    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "ad_sdk_version"

    const-string v3, "2.6.0.0"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "source_type"

    const-string v3, "app"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "app"

    :try_start_1
    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->c()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->c(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getOrientation()I

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-lez v3, :cond_5

    const-string v3, "orientation"

    :try_start_2
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getOrientation()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    :cond_5
    const-string v3, "device"

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const-string v2, "ua"

    :try_start_3
    sget-object v3, Lcom/bytedance/msdk/base/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const-string v2, "ip"

    :try_start_4
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    invoke-direct {v0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a(Lcom/bytedance/msdk/api/AdSlot;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    const-string v0, "adslots"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-string v0, "ts"

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    const-string v0, ""

    :try_start_5
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    :cond_6
    const-string p1, "req_sign"

    :try_start_6
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    :catch_0
    return-object v1

    :goto_9
    :pswitch_a
    move-object p1, v4

    move-object v0, v5

    goto/16 :goto_3

    :pswitch_b
    move-object p1, v4

    move-object v0, v5

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3c
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x34
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5f
        :pswitch_b
        :pswitch_6
        :pswitch_a
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x31
        :pswitch_9
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method private b(Lorg/json/JSONObject;)V
    .locals 2

    :goto_0
    const/16 v0, 0x5d

    const/16 v1, 0x5d

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x4

    if-gt v1, v0, :cond_0

    goto :goto_0

    :pswitch_1
    packed-switch v1, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch v1, :pswitch_data_2

    goto :goto_3

    :cond_0
    :pswitch_3
    const-string v0, "package_name"

    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/x;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v0, "version_code"

    :try_start_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/x;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v0, "version"

    :try_start_2
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_byte12b/x;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    return-void

    :pswitch_4
    const/16 v0, 0x5e

    const/16 v1, 0x4b

    goto :goto_1

    :goto_3
    const/16 v1, 0x5b

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private c()Lorg/json/JSONObject;
    .locals 4

    :cond_0
    :goto_0
    const/16 v0, -0x37

    const/16 v1, 0x49

    const/4 v2, 0x1

    const/16 v3, 0x55

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    packed-switch v0, :pswitch_data_1

    goto :goto_2

    :pswitch_1
    const/16 v0, 0x5e

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x5e

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    :pswitch_2
    rsub-int/lit8 v0, v2, 0x55

    mul-int/lit8 v0, v0, 0x55

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v2

    mul-int v0, v0, v1

    rem-int/lit8 v0, v0, 0x6

    if-eqz v0, :cond_1

    goto :goto_2

    :cond_1
    :goto_1
    :pswitch_3
    const/16 v0, 0x5b

    packed-switch v0, :pswitch_data_2

    goto :goto_1

    :pswitch_4
    mul-int v0, v3, v3

    const/16 v1, 0x10

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :pswitch_5
    const/16 v0, 0x1e

    rsub-int/lit8 v1, v2, 0x1e

    mul-int/lit8 v1, v1, 0x1e

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v2

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    if-eqz v1, :cond_0

    goto :goto_1

    :goto_2
    :pswitch_6
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "appid"

    :try_start_0
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "name"

    :try_start_1
    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->x()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;

    move-result-object v2

    invoke-virtual {v2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/b;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->b(Lorg/json/JSONObject;)V

    invoke-direct {p0, v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_3
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x27
        :pswitch_1
        :pswitch_2
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_6
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;
    .locals 3

    :cond_0
    :goto_0
    :pswitch_0
    const/16 v0, 0x49

    const/16 v1, 0x60

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_4

    :pswitch_1
    packed-switch v1, :pswitch_data_1

    :pswitch_2
    packed-switch v1, :pswitch_data_2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_3

    goto :goto_3

    :pswitch_4
    const/4 v1, 0x0

    rsub-int/lit8 v2, v0, 0x0

    mul-int/lit8 v2, v2, 0x0

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_0

    :pswitch_5
    const/16 v1, 0x12

    rsub-int/lit8 v2, v0, 0x12

    mul-int/lit8 v2, v2, 0x12

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_3

    goto :goto_0

    :pswitch_6
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;

    if-nez v0, :cond_2

    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;

    if-nez v0, :cond_1

    new-instance v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;

    invoke-direct {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;-><init>()V

    sput-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;

    :cond_1
    const-class v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;

    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception v0

    const-class v1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :goto_2
    sget-object v0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;

    return-object v0

    :cond_3
    :goto_3
    :pswitch_7
    const/16 v0, 0x4a

    const/16 v1, 0x37

    goto :goto_1

    :goto_4
    const/16 v0, 0x48

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_7
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_0
        :pswitch_3
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x37
        :pswitch_6
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3c
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a(Lcom/bytedance/msdk/api/AdSlot;Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;ILbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bytedance/msdk/api/AdSlot;",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;",
            "I",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;->a(Lcom/bytedance/msdk/api/AdSlot;Ljava/util/List;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/c;I)Ljava/lang/String;

    move-result-object v4

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "serverBiddingRequest-encryptBody="

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "serverBiddingRequest"

    invoke-static {p2, p1}, Lcom/bytedance/msdk/adapter/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$b;

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/c;->a()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$a;

    invoke-direct {v5, p0, p5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$a;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$e;)V

    const/4 v2, 0x1

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a$b;-><init>(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_char12/a;ILjava/lang/String;Ljava/lang/String;Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/p$a;)V

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->d()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object p2

    const/16 p3, 0x2710

    invoke-virtual {p2, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;->b(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;->a(I)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/e;

    move-result-object p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/e;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    move-result-object p1

    sget-object p2, Lcom/bytedance/msdk/base/b;->a:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->e(Ljava/lang/String;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    move-result-object p1

    invoke-virtual {p1, p3}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Z)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;

    move-result-object p1

    invoke-static {}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/a;->c()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->a(Landroid/content/Context;)Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;

    move-result-object p2

    invoke-virtual {p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_int108/b;->a()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;

    move-result-object p2

    invoke-virtual {p1, p2}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/n;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_19do/bykvm_if122/o;)V

    :goto_0
    const/16 p1, 0x2a

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x25

    add-int/lit8 p1, p1, 0x1

    mul-int/lit8 p1, p1, 0x25

    rem-int/lit8 p1, p1, 0x2

    :pswitch_1
    return-void

    :pswitch_data_0
    .packed-switch 0x28
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
