.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;
.super Ljava/lang/Object;
.source "WaterFallConfig.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "1"

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c:Ljava/lang/String;

    const-string v0, "0"

    iput-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->l:I

    :pswitch_0
    const/16 v1, 0x5e

    const/16 v2, 0x4b

    const/16 v3, 0x5d

    :goto_0
    packed-switch v1, :pswitch_data_0

    goto :goto_3

    :pswitch_1
    packed-switch v2, :pswitch_data_1

    :goto_1
    :pswitch_2
    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_3
    return v0

    :goto_2
    const/16 v2, 0x5b

    goto :goto_1

    :goto_3
    :pswitch_4
    const/16 v1, 0x5d

    const/16 v2, 0x5d

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;)I
    .locals 4

    const/4 v0, 0x1

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f:I

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->i()I

    move-result v2

    if-le v1, v2, :cond_1

    goto :goto_1

    :cond_1
    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f:I

    invoke-virtual {p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->i()I

    move-result p1

    const/4 v2, -0x1

    if-ge v1, p1, :cond_2

    return v2

    :cond_2
    :goto_0
    const/16 p1, 0x3e

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x12

    mul-int p1, p1, p1

    const/16 v1, 0x23

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr p1, v1

    if-ne p1, v2, :cond_3

    :pswitch_1
    const/16 p1, 0x55

    mul-int v1, p1, p1

    const/16 v2, 0x20

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x20

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :pswitch_2
    const/4 p1, 0x0

    return p1

    :cond_3
    :goto_1
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x3c
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(I)V
    .locals 6

    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->l:I

    :cond_0
    :goto_0
    :pswitch_0
    const/16 p1, 0x49

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_1
    const/16 p1, 0x3d

    const/16 v1, 0x22

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 p1, 0x31

    mul-int v2, p1, p1

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    const/16 v3, 0x12

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    const/16 v4, 0x31

    mul-int/lit8 v4, v4, 0x22

    const/16 v5, 0x22

    mul-int/lit8 v5, v5, 0x12

    add-int/2addr v4, v5

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v4, p1

    if-ge v2, v4, :cond_3

    :pswitch_3
    const/16 p1, 0x28

    mul-int v2, p1, p1

    mul-int v3, v0, v0

    add-int/2addr v2, v3

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    if-gez v2, :cond_3

    :goto_2
    :pswitch_4
    const/16 p1, 0x34

    const/16 v2, 0x5d

    const/16 v3, 0x55

    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_5
    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :pswitch_6
    mul-int v3, v3, v3

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v3, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    :pswitch_7
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_1

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    goto :goto_4

    :goto_3
    :pswitch_9
    mul-int v3, v3, v3

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v3, p1

    const/4 p1, -0x1

    goto :goto_1

    :cond_3
    :goto_4
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3b
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x34
        :pswitch_a
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;)V
    .locals 6

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->n:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    :cond_0
    :goto_0
    :pswitch_0
    const/16 p1, 0x49

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_1
    const/16 p1, 0x3d

    const/16 v1, 0x22

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 p1, 0x31

    mul-int v2, p1, p1

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    const/16 v3, 0x12

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    const/16 v4, 0x31

    mul-int/lit8 v4, v4, 0x22

    const/16 v5, 0x22

    mul-int/lit8 v5, v5, 0x12

    add-int/2addr v4, v5

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v4, p1

    if-ge v2, v4, :cond_3

    :pswitch_3
    const/16 p1, 0x28

    mul-int v2, p1, p1

    mul-int v3, v0, v0

    add-int/2addr v2, v3

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    if-gez v2, :cond_3

    :goto_2
    :pswitch_4
    const/16 p1, 0x34

    const/16 v2, 0x5d

    const/16 v3, 0x55

    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_5
    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :pswitch_6
    mul-int v3, v3, v3

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v3, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    :pswitch_7
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_1

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    goto :goto_4

    :goto_3
    :pswitch_9
    mul-int v3, v3, v3

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v3, p1

    const/4 p1, -0x1

    goto :goto_1

    :cond_3
    :goto_4
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3b
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x34
        :pswitch_a
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b:Ljava/lang/String;

    :cond_0
    :goto_0
    :pswitch_0
    const/16 p1, 0x49

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_1
    const/16 p1, 0x3d

    const/16 v1, 0x22

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 p1, 0x31

    mul-int v2, p1, p1

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    const/16 v3, 0x12

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    const/16 v4, 0x31

    mul-int/lit8 v4, v4, 0x22

    const/16 v5, 0x22

    mul-int/lit8 v5, v5, 0x12

    add-int/2addr v4, v5

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v4, p1

    if-ge v2, v4, :cond_3

    :pswitch_3
    const/16 p1, 0x28

    mul-int v2, p1, p1

    mul-int v3, v0, v0

    add-int/2addr v2, v3

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    if-gez v2, :cond_3

    :goto_2
    :pswitch_4
    const/16 p1, 0x34

    const/16 v2, 0x5d

    const/16 v3, 0x55

    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_5
    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :pswitch_6
    mul-int v3, v3, v3

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v3, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    :pswitch_7
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_1

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    goto :goto_4

    :goto_3
    :pswitch_9
    mul-int v3, v3, v3

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v3, p1

    const/4 p1, -0x1

    goto :goto_1

    :cond_3
    :goto_4
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3b
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x34
        :pswitch_a
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b:Ljava/lang/String;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b(I)V
    .locals 6

    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e:I

    :cond_0
    :goto_0
    :pswitch_0
    const/16 p1, 0x49

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_1
    const/16 p1, 0x3d

    const/16 v1, 0x22

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 p1, 0x31

    mul-int v2, p1, p1

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    const/16 v3, 0x12

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    const/16 v4, 0x31

    mul-int/lit8 v4, v4, 0x22

    const/16 v5, 0x22

    mul-int/lit8 v5, v5, 0x12

    add-int/2addr v4, v5

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v4, p1

    if-ge v2, v4, :cond_3

    :pswitch_3
    const/16 p1, 0x28

    mul-int v2, p1, p1

    mul-int v3, v0, v0

    add-int/2addr v2, v3

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    if-gez v2, :cond_3

    :goto_2
    :pswitch_4
    const/16 p1, 0x34

    const/16 v2, 0x5d

    const/16 v3, 0x55

    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_5
    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :pswitch_6
    mul-int v3, v3, v3

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v3, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    :pswitch_7
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_1

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    goto :goto_4

    :goto_3
    :pswitch_9
    mul-int v3, v3, v3

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v3, p1

    const/4 p1, -0x1

    goto :goto_1

    :cond_3
    :goto_4
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3b
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x34
        :pswitch_a
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a:Ljava/lang/String;

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public c()Ljava/lang/String;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a:Ljava/lang/String;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public c(I)V
    .locals 6

    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->m:I

    :cond_0
    :goto_0
    :pswitch_0
    const/16 p1, 0x49

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_1
    const/16 p1, 0x3d

    const/16 v1, 0x22

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 p1, 0x31

    mul-int v2, p1, p1

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    const/16 v3, 0x12

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    const/16 v4, 0x31

    mul-int/lit8 v4, v4, 0x22

    const/16 v5, 0x22

    mul-int/lit8 v5, v5, 0x12

    add-int/2addr v4, v5

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v4, p1

    if-ge v2, v4, :cond_3

    :pswitch_3
    const/16 p1, 0x28

    mul-int v2, p1, p1

    mul-int v3, v0, v0

    add-int/2addr v2, v3

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    if-gez v2, :cond_3

    :goto_2
    :pswitch_4
    const/16 p1, 0x34

    const/16 v2, 0x5d

    const/16 v3, 0x55

    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_5
    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :pswitch_6
    mul-int v3, v3, v3

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v3, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    :pswitch_7
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_1

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    goto :goto_4

    :goto_3
    :pswitch_9
    mul-int v3, v3, v3

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v3, p1

    const/4 p1, -0x1

    goto :goto_1

    :cond_3
    :goto_4
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3b
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x34
        :pswitch_a
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public c(Ljava/lang/String;)V
    .locals 4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "0"

    :cond_0
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d:Ljava/lang/String;

    :cond_1
    :goto_0
    const/16 p1, 0xc

    const/16 v0, 0x60

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :goto_1
    :pswitch_0
    packed-switch p1, :pswitch_data_1

    const/16 p1, 0x5f

    goto :goto_1

    :pswitch_1
    const/4 p1, 0x1

    const/16 v0, 0x51

    rsub-int/lit8 v1, p1, 0x51

    mul-int/lit8 v1, v1, 0x51

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, p1

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    if-eqz v1, :cond_3

    const/16 p1, 0x42

    mul-int p1, p1, p1

    const/16 v0, 0x10

    mul-int v0, v0, v0

    mul-int/lit8 v0, v0, 0x22

    sub-int/2addr p1, v0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    :pswitch_2
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_3

    :cond_2
    :pswitch_3
    const/16 p1, 0x46

    mul-int v0, p1, p1

    const/4 v1, 0x7

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/4 v2, 0x4

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x46

    mul-int/lit8 v3, v3, 0x7

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x4

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_1

    :cond_3
    :pswitch_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 7

    check-cast p1, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;

    invoke-virtual {p0, p1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;)I

    move-result p1

    :cond_0
    :goto_0
    :pswitch_0
    const/16 v0, 0x49

    const/16 v1, 0xe

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :goto_1
    :pswitch_2
    const/16 v0, 0x22

    mul-int v3, v0, v0

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x26

    mul-int v5, v4, v4

    mul-int/lit8 v5, v5, 0x26

    add-int/2addr v3, v5

    const/4 v5, 0x7

    mul-int v6, v5, v5

    mul-int/lit8 v6, v6, 0x7

    add-int/2addr v3, v6

    mul-int/lit8 v0, v0, 0x26

    mul-int/lit8 v0, v0, 0x7

    mul-int/lit8 v0, v0, 0x3

    if-ge v3, v0, :cond_1

    :pswitch_3
    const/16 v0, 0x10

    rsub-int/lit8 v1, v2, 0x10

    mul-int/lit8 v1, v1, 0x10

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v2

    mul-int v1, v1, v0

    rem-int/lit8 v1, v1, 0x6

    goto :goto_0

    :cond_1
    :pswitch_4
    packed-switch v1, :pswitch_data_2

    goto :goto_1

    :pswitch_5
    const/16 v0, 0x9

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x9

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    goto :goto_2

    :pswitch_6
    const/16 v0, 0x5e

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x5e

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    :goto_2
    :pswitch_7
    return p1

    :pswitch_data_0
    .packed-switch 0x47
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xc
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0xd
        :pswitch_6
        :pswitch_7
        :pswitch_5
    .end packed-switch
.end method

.method public d()I
    .locals 3

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e:I

    :goto_0
    const/16 v1, 0x5d

    const/16 v2, 0x5d

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x4

    if-gt v2, v1, :cond_0

    goto :goto_0

    :pswitch_1
    packed-switch v2, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch v2, :pswitch_data_2

    goto :goto_3

    :cond_0
    :pswitch_3
    return v0

    :pswitch_4
    const/16 v1, 0x5e

    const/16 v2, 0x4b

    goto :goto_1

    :goto_3
    const/16 v2, 0x5b

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public d(I)V
    .locals 5

    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f:I

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public d(Ljava/lang/String;)V
    .locals 6

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c:Ljava/lang/String;

    :cond_0
    :goto_0
    :pswitch_0
    const/16 p1, 0x49

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_1
    const/16 p1, 0x3d

    const/16 v1, 0x22

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 p1, 0x31

    mul-int v2, p1, p1

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    const/16 v3, 0x12

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    const/16 v4, 0x31

    mul-int/lit8 v4, v4, 0x22

    const/16 v5, 0x22

    mul-int/lit8 v5, v5, 0x12

    add-int/2addr v4, v5

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v4, p1

    if-ge v2, v4, :cond_3

    :pswitch_3
    const/16 p1, 0x28

    mul-int v2, p1, p1

    mul-int v3, v0, v0

    add-int/2addr v2, v3

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    if-gez v2, :cond_3

    :goto_2
    :pswitch_4
    const/16 p1, 0x34

    const/16 v2, 0x5d

    const/16 v3, 0x55

    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_5
    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :pswitch_6
    mul-int v3, v3, v3

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v3, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    :pswitch_7
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_1

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    goto :goto_4

    :goto_3
    :pswitch_9
    mul-int v3, v3, v3

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v3, p1

    const/4 p1, -0x1

    goto :goto_1

    :cond_3
    :goto_4
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3b
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x34
        :pswitch_a
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public e()D
    .locals 5

    :cond_0
    :goto_0
    const/16 v0, 0x5f

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x54

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x54

    const/16 v2, 0x2a

    mul-int v3, v2, v2

    mul-int/lit8 v3, v3, 0x2a

    add-int/2addr v1, v3

    const/16 v3, 0x16

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0x16

    add-int/2addr v1, v4

    mul-int/lit8 v0, v0, 0x2a

    mul-int/lit8 v0, v0, 0x16

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_0

    const/16 v0, 0x46

    mul-int v1, v0, v0

    const/16 v2, 0x2c

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    mul-int/lit8 v0, v0, 0x2c

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v1, v0

    if-gez v1, :cond_1

    const/16 v0, 0x12

    mul-int v0, v0, v0

    const/16 v1, 0x23

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/16 v0, 0x1a

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x1a

    const/16 v2, 0x9

    mul-int v3, v2, v2

    mul-int/lit8 v3, v3, 0x9

    add-int/2addr v1, v3

    const/16 v3, 0xf

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0xf

    add-int/2addr v1, v4

    mul-int/lit8 v0, v0, 0x9

    mul-int/lit8 v0, v0, 0xf

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_0

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x1

    const/16 v1, 0x1e

    rsub-int/lit8 v2, v0, 0x1e

    mul-int/lit8 v2, v2, 0x1e

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :cond_1
    :goto_1
    :pswitch_2
    :try_start_0
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    mul-double v0, v0, v2

    return-wide v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getEcpm error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "WaterFallConfig"

    invoke-static {v1, v0}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    return-wide v0

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public e(I)V
    .locals 5

    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->h:I

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public e(Ljava/lang/String;)V
    .locals 6

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->i:Ljava/lang/String;

    :cond_0
    :goto_0
    :pswitch_0
    const/16 p1, 0x49

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_1
    const/16 p1, 0x3d

    const/16 v1, 0x22

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 p1, 0x31

    mul-int v2, p1, p1

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    const/16 v3, 0x12

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    const/16 v4, 0x31

    mul-int/lit8 v4, v4, 0x22

    const/16 v5, 0x22

    mul-int/lit8 v5, v5, 0x12

    add-int/2addr v4, v5

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v4, p1

    if-ge v2, v4, :cond_3

    :pswitch_3
    const/16 p1, 0x28

    mul-int v2, p1, p1

    mul-int v3, v0, v0

    add-int/2addr v2, v3

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    if-gez v2, :cond_3

    :goto_2
    :pswitch_4
    const/16 p1, 0x34

    const/16 v2, 0x5d

    const/16 v3, 0x55

    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_5
    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :pswitch_6
    mul-int v3, v3, v3

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v3, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    :pswitch_7
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_1

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    goto :goto_4

    :goto_3
    :pswitch_9
    mul-int v3, v3, v3

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v3, p1

    const/4 p1, -0x1

    goto :goto_1

    :cond_3
    :goto_4
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3b
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x34
        :pswitch_a
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public f()I
    .locals 7

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e:I

    const/16 v1, 0x14

    const/16 v2, 0x64

    if-ne v0, v2, :cond_3

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f:I

    :cond_0
    :goto_0
    const/16 v2, 0xc

    const/16 v3, 0x60

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :goto_1
    :pswitch_0
    packed-switch v2, :pswitch_data_1

    const/16 v2, 0x5f

    goto :goto_1

    :pswitch_1
    const/4 v2, 0x1

    const/16 v3, 0x51

    rsub-int/lit8 v4, v2, 0x51

    mul-int/lit8 v4, v4, 0x51

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v3, v2

    mul-int v4, v4, v3

    rem-int/lit8 v4, v4, 0x6

    if-eqz v4, :cond_4

    const/16 v2, 0x42

    mul-int v2, v2, v2

    const/16 v3, 0x10

    mul-int v3, v3, v3

    mul-int/lit8 v3, v3, 0x22

    sub-int/2addr v2, v3

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    :pswitch_2
    const/16 v2, 0x1d

    mul-int v3, v2, v2

    const/16 v4, 0x2d

    mul-int v5, v4, v4

    add-int/2addr v3, v5

    mul-int v5, v1, v1

    add-int/2addr v3, v5

    const/16 v5, 0x1d

    mul-int/lit8 v5, v5, 0x2d

    mul-int/lit8 v4, v4, 0x14

    add-int/2addr v5, v4

    mul-int/lit8 v2, v2, 0x14

    add-int/2addr v5, v2

    if-ge v3, v5, :cond_2

    :cond_1
    :pswitch_3
    const/16 v2, 0x46

    mul-int v3, v2, v2

    const/4 v4, 0x7

    mul-int v5, v4, v4

    add-int/2addr v3, v5

    const/4 v5, 0x4

    mul-int v6, v5, v5

    add-int/2addr v3, v6

    const/16 v6, 0x46

    mul-int/lit8 v6, v6, 0x7

    mul-int/lit8 v4, v4, 0x4

    add-int/2addr v6, v4

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr v6, v2

    if-ge v3, v6, :cond_0

    :cond_2
    :pswitch_4
    add-int/lit16 v0, v0, 0x12c

    return v0

    :cond_3
    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f:I

    :cond_4
    return v1

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public f(I)V
    .locals 5

    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->g:I

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public f(Ljava/lang/String;)V
    .locals 5

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->k:Ljava/lang/String;

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public g()Ljava/lang/String;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c:Ljava/lang/String;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public g(Ljava/lang/String;)V
    .locals 6

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->j:Ljava/lang/String;

    :cond_0
    :goto_0
    :pswitch_0
    const/16 p1, 0x49

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :cond_1
    :goto_1
    :pswitch_1
    const/16 p1, 0x3d

    const/16 v1, 0x22

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/16 p1, 0x31

    mul-int v2, p1, p1

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    const/16 v3, 0x12

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    const/16 v4, 0x31

    mul-int/lit8 v4, v4, 0x22

    const/16 v5, 0x22

    mul-int/lit8 v5, v5, 0x12

    add-int/2addr v4, v5

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v4, p1

    if-ge v2, v4, :cond_3

    :pswitch_3
    const/16 p1, 0x28

    mul-int v2, p1, p1

    mul-int v3, v0, v0

    add-int/2addr v2, v3

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v2, p1

    if-gez v2, :cond_3

    :goto_2
    :pswitch_4
    const/16 p1, 0x34

    const/16 v2, 0x5d

    const/16 v3, 0x55

    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_5
    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :pswitch_6
    mul-int v3, v3, v3

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v3, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v3, v1

    if-gez v3, :cond_0

    :pswitch_7
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_1

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_1

    goto :goto_4

    :goto_3
    :pswitch_9
    mul-int v3, v3, v3

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v3, p1

    const/4 p1, -0x1

    goto :goto_1

    :cond_3
    :goto_4
    :pswitch_a
    return-void

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3b
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x34
        :pswitch_a
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public h()I
    .locals 4

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->m:I

    :pswitch_0
    const/16 v1, 0x5e

    const/16 v2, 0x4b

    const/16 v3, 0x5d

    :goto_0
    packed-switch v1, :pswitch_data_0

    goto :goto_3

    :pswitch_1
    packed-switch v2, :pswitch_data_1

    :goto_1
    :pswitch_2
    packed-switch v2, :pswitch_data_2

    goto :goto_2

    :pswitch_3
    return v0

    :goto_2
    const/16 v2, 0x5b

    goto :goto_1

    :goto_3
    :pswitch_4
    const/16 v1, 0x5d

    const/16 v2, 0x5d

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public i()I
    .locals 3

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f:I

    :goto_0
    const/16 v1, 0x5d

    const/16 v2, 0x5d

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x4

    if-gt v2, v1, :cond_0

    goto :goto_0

    :pswitch_1
    packed-switch v2, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch v2, :pswitch_data_2

    goto :goto_3

    :cond_0
    :pswitch_3
    return v0

    :pswitch_4
    const/16 v1, 0x5e

    const/16 v2, 0x4b

    goto :goto_1

    :goto_3
    const/16 v2, 0x5b

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public j()Ljava/lang/String;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->i:Ljava/lang/String;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public k()I
    .locals 3

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->h:I

    :goto_0
    const/16 v1, 0x5d

    const/16 v2, 0x5d

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x4

    if-gt v2, v1, :cond_0

    goto :goto_0

    :pswitch_1
    packed-switch v2, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch v2, :pswitch_data_2

    goto :goto_3

    :cond_0
    :pswitch_3
    return v0

    :pswitch_4
    const/16 v1, 0x5e

    const/16 v2, 0x4b

    goto :goto_1

    :goto_3
    const/16 v2, 0x5b

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public l()Ljava/lang/String;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->k:Ljava/lang/String;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public m()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->n:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public n()I
    .locals 3

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->g:I

    :goto_0
    const/16 v1, 0x5d

    const/16 v2, 0x5d

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x4

    if-gt v2, v1, :cond_0

    goto :goto_0

    :pswitch_1
    packed-switch v2, :pswitch_data_1

    :goto_2
    :pswitch_2
    packed-switch v2, :pswitch_data_2

    goto :goto_3

    :cond_0
    :pswitch_3
    return v0

    :pswitch_4
    const/16 v1, 0x5e

    const/16 v2, 0x4b

    goto :goto_1

    :goto_3
    const/16 v2, 0x5b

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5b
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public o()Ljava/lang/String;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->j:Ljava/lang/String;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public p()Z
    .locals 8

    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e:I

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-ne v0, v1, :cond_3

    :cond_0
    :goto_0
    const/16 v0, 0xc

    const/16 v3, 0x60

    const/4 v4, 0x4

    const/16 v5, 0x14

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :goto_1
    :pswitch_0
    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x5f

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x51

    rsub-int/lit8 v3, v2, 0x51

    mul-int/lit8 v3, v3, 0x51

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v2

    mul-int v3, v3, v0

    rem-int/lit8 v3, v3, 0x6

    if-eqz v3, :cond_4

    const/16 v0, 0x42

    mul-int v0, v0, v0

    const/16 v3, 0x10

    mul-int v3, v3, v3

    mul-int/lit8 v3, v3, 0x22

    sub-int/2addr v0, v3

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    :pswitch_2
    const/16 v0, 0x1d

    mul-int v3, v0, v0

    const/16 v6, 0x2d

    mul-int v7, v6, v6

    add-int/2addr v3, v7

    mul-int v7, v5, v5

    add-int/2addr v3, v7

    const/16 v7, 0x1d

    mul-int/lit8 v7, v7, 0x2d

    mul-int/lit8 v6, v6, 0x14

    add-int/2addr v7, v6

    mul-int/lit8 v0, v0, 0x14

    add-int/2addr v7, v0

    if-ge v3, v7, :cond_2

    :cond_1
    :pswitch_3
    const/16 v0, 0x46

    mul-int v3, v0, v0

    const/4 v6, 0x7

    mul-int v7, v6, v6

    add-int/2addr v3, v7

    mul-int v7, v4, v4

    add-int/2addr v3, v7

    const/16 v7, 0x46

    mul-int/lit8 v7, v7, 0x7

    mul-int/lit8 v6, v6, 0x4

    add-int/2addr v7, v6

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v7, v0

    if-ge v3, v7, :cond_0

    :cond_2
    :pswitch_4
    const/16 v0, 0x45

    mul-int v3, v0, v0

    mul-int v6, v5, v5

    add-int/2addr v3, v6

    mul-int v6, v4, v4

    add-int/2addr v3, v6

    const/16 v6, 0x45

    mul-int/lit8 v6, v6, 0x14

    const/16 v5, 0x14

    mul-int/lit8 v5, v5, 0x4

    add-int/2addr v6, v5

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v6, v0

    if-ge v3, v6, :cond_0

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :cond_4
    :goto_2
    :pswitch_5
    return v2

    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_1
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    :cond_0
    :goto_0
    const/16 v0, 0x12

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0xc

    mul-int v1, v0, v0

    const/16 v2, 0xe

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    mul-int/lit8 v0, v0, 0xe

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v1, v0

    if-gez v1, :cond_2

    :pswitch_1
    const/16 v0, 0x36

    mul-int v1, v0, v0

    const/16 v2, 0x1b

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x18

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    const/16 v4, 0x36

    mul-int/lit8 v4, v4, 0x1b

    mul-int/lit8 v2, v2, 0x18

    add-int/2addr v4, v2

    mul-int/lit8 v0, v0, 0x18

    add-int/2addr v4, v0

    if-ge v1, v4, :cond_0

    :cond_1
    :goto_1
    :pswitch_2
    const/16 v0, 0x28

    const/16 v1, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch v0, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    const/16 v0, 0x47

    mul-int v0, v0, v0

    const/16 v1, 0x19

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    goto :goto_1

    :goto_2
    :pswitch_5
    const/16 v0, 0x5e

    mul-int v1, v0, v0

    mul-int/lit8 v1, v1, 0x5e

    const/16 v2, 0x11

    mul-int v3, v2, v2

    mul-int/lit8 v3, v3, 0x11

    add-int/2addr v1, v3

    const/4 v3, 0x7

    mul-int v4, v3, v3

    mul-int/lit8 v4, v4, 0x7

    add-int/2addr v1, v4

    mul-int/lit8 v0, v0, 0x11

    mul-int/lit8 v0, v0, 0x7

    mul-int/lit8 v0, v0, 0x3

    if-ge v1, v0, :cond_1

    :pswitch_6
    const/16 v0, 0x25

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x25

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    :cond_2
    :pswitch_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WaterFallConfig{mAdnetworkName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", mAdnetwokrSlotId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", mExchangeRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mSlotEcpm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mAdnetworkSlotType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mLoadSort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mShowSort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x55
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x28
        :pswitch_7
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method
