.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;
.super Ljava/lang/Object;
.source "AdSlotInner.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 9
    iput v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->c:I

    .line 12
    iput v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->d:I

    return-void
.end method

.method public static getShallowCopy(Lcom/bytedance/msdk/api/AdSlot;)Lcom/bytedance/msdk/api/AdSlot;
    .locals 4

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1
    :cond_0
    new-instance v0, Lcom/bytedance/msdk/api/AdSlot$Builder;

    invoke-direct {v0}, Lcom/bytedance/msdk/api/AdSlot$Builder;-><init>()V

    .line 2
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdStyleType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setAdStyleType(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 3
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setAdType(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 4
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setAdCount(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 5
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getImgAcceptedHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setImageAdSize(II)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 6
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getBannerSize()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setBannerSize(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 7
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getCustomData()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setCustomData(Ljava/util/Map;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 8
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getRewardName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setRewardName(Ljava/lang/String;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 9
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getRewardAmount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setRewardAmount(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 10
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getUserID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setUserID(Ljava/lang/String;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 11
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getReuestParam()Lcom/bytedance/msdk/api/TTRequestExtraParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setTTRequestExtraParams(Lcom/bytedance/msdk/api/TTRequestExtraParams;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 12
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setTTVideoOption(Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 13
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getOrientation()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setOrientation(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 14
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdmobNativeAdOptions()Lcom/bytedance/msdk/api/AdmobNativeAdOptions;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setAdmobNativeAdOptions(Lcom/bytedance/msdk/api/AdmobNativeAdOptions;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    .line 15
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot$Builder;->build()Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    .line 16
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->setAdUnitId(Ljava/lang/String;)V

    .line 17
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/AdSlot;->setVersion(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/AdSlot;->getAdCount()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/bytedance/msdk/api/AdSlot;->setWaterfallId(J)V

    .line 20
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getMediationRitReqType()I

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->setMediationRitReqType(I)V

    .line 21
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getPrimeRitReqType()I

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->setPrimeRitReqType(I)V

    .line 22
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getAdloadSeq()I

    move-result v1

    invoke-virtual {v0, v1}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->setAdloadSeq(I)V

    .line 23
    invoke-virtual {p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->getLinkedId()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->setLinkedId(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getAdloadSeq()I
    .locals 1

    .line 1
    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->b:I

    return v0
.end method

.method public getLinkedId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getMediationRitReqType()I
    .locals 1

    .line 1
    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->d:I

    return v0
.end method

.method public getPrimeRitReqType()I
    .locals 1

    .line 1
    iget v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->c:I

    return v0
.end method

.method public setAdloadSeq(I)V
    .locals 0

    .line 1
    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->b:I

    return-void
.end method

.method public setLinkedId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->a:Ljava/lang/String;

    return-void
.end method

.method public setMediationRitReqType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->d:I

    return-void
.end method

.method public setPrimeRitReqType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/b;->c:I

    return-void
.end method
