.class public Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;
.super Ljava/lang/Object;
.source "ServerBiddingModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

.field private c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;->a:Ljava/lang/String;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;)V
    .locals 5

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;)V
    .locals 5

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/h;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;

    if-nez v0, :cond_3

    :cond_0
    :goto_0
    const/16 v0, 0x5c

    const/16 v1, 0xe

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :goto_1
    :pswitch_0
    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x5f

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x42

    mul-int v0, v0, v0

    const/16 v1, 0x10

    mul-int v1, v1, v1

    mul-int/lit8 v1, v1, 0x22

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :pswitch_2
    const/16 v0, 0x1d

    mul-int v1, v0, v0

    const/16 v2, 0x2d

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x14

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    const/16 v4, 0x1d

    mul-int/lit8 v4, v4, 0x2d

    mul-int/lit8 v2, v2, 0x14

    add-int/2addr v4, v2

    mul-int/lit8 v0, v0, 0x14

    add-int/2addr v4, v0

    if-ge v1, v4, :cond_2

    :cond_1
    :pswitch_3
    const/16 v0, 0x46

    mul-int v1, v0, v0

    const/4 v2, 0x7

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/4 v3, 0x4

    mul-int v4, v3, v3

    add-int/2addr v1, v4

    const/16 v4, 0x46

    mul-int/lit8 v4, v4, 0x7

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr v4, v2

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    if-ge v1, v4, :cond_0

    :cond_2
    :pswitch_4
    const/4 v0, 0x0

    return-object v0

    :cond_3
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;->b(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;)Ljava/util/List;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_1
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5e
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    iput-object p1, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;->a:Ljava/lang/String;

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public c()Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;
    .locals 3

    :goto_0
    const/16 v0, 0x49

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, v0, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    :pswitch_1
    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;->b:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/f;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public c(Ljava/lang/String;)V
    .locals 5

    :cond_0
    :goto_0
    const/16 p1, 0x3d

    const/4 v0, 0x2

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x31

    mul-int v1, p1, p1

    add-int/lit16 v1, v1, 0x484

    const/16 v2, 0x12

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    const/16 v3, 0x31

    mul-int/lit8 v3, v3, 0x22

    const/16 v4, 0x22

    mul-int/lit8 v4, v4, 0x12

    add-int/2addr v3, v4

    mul-int/lit8 p1, p1, 0x12

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    :pswitch_1
    const/16 p1, 0x28

    mul-int v1, p1, p1

    mul-int v2, v0, v0

    add-int/2addr v1, v2

    mul-int/lit8 p1, p1, 0x2

    mul-int/lit8 p1, p1, 0x2

    sub-int/2addr v1, p1

    if-gez v1, :cond_3

    :goto_1
    :pswitch_2
    const/16 p1, 0x34

    const/16 v1, 0x5d

    const/16 v2, 0x55

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    :pswitch_3
    packed-switch p1, :pswitch_data_2

    goto :goto_2

    :pswitch_4
    mul-int v2, v2, v2

    const/16 p1, 0x20

    mul-int v1, p1, p1

    add-int/2addr v2, v1

    const/16 v1, 0x55

    mul-int/lit8 v1, v1, 0x20

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    if-gez v2, :cond_1

    :pswitch_5
    const/16 p1, 0x50

    mul-int v1, p1, p1

    const/4 v2, 0x0

    mul-int v3, v2, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v3

    mul-int/lit8 p1, p1, 0x0

    add-int/2addr v3, p1

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_2

    goto :goto_3

    :goto_2
    :pswitch_6
    mul-int v2, v2, v2

    const/16 p1, 0x10

    mul-int p1, p1, p1

    mul-int/lit8 p1, p1, 0x22

    sub-int/2addr v2, p1

    const/4 p1, -0x1

    if-ne v2, p1, :cond_1

    goto :goto_4

    :cond_1
    :goto_3
    :pswitch_7
    const/16 p1, 0x49

    packed-switch p1, :pswitch_data_3

    goto :goto_3

    :cond_2
    :pswitch_8
    const/4 p1, 0x1

    const/16 v1, 0x52

    rsub-int/lit8 v2, p1, 0x52

    mul-int/lit8 v2, v2, 0x52

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, p1

    mul-int v2, v2, v1

    rem-int/lit8 v2, v2, 0x6

    goto :goto_3

    :goto_4
    :pswitch_9
    const/16 p1, 0x1d

    mul-int v0, p1, p1

    const/16 v1, 0x2d

    mul-int v2, v1, v1

    add-int/2addr v0, v2

    const/16 v2, 0x14

    mul-int v3, v2, v2

    add-int/2addr v0, v3

    const/16 v3, 0x1d

    mul-int/lit8 v3, v3, 0x2d

    mul-int/lit8 v1, v1, 0x14

    add-int/2addr v3, v1

    mul-int/lit8 p1, p1, 0x14

    add-int/2addr v3, p1

    if-ge v0, v3, :cond_0

    :cond_3
    :pswitch_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5c
        :pswitch_7
        :pswitch_3
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_9
        :pswitch_a
        :pswitch_8
    .end packed-switch
.end method

.method public d()Z
    .locals 5

    iget-object v0, p0, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e;->c:Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {v0}, Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;->a(Lbykvm_19do/bykvm_19do/bykvm_19do/bykvm_for12/bykvm_try19/e$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/16 v0, 0x3e

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x12

    mul-int v0, v0, v0

    const/16 v2, 0x23

    mul-int v2, v2, v2

    mul-int/lit8 v2, v2, 0x22

    sub-int/2addr v0, v2

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    :pswitch_1
    const/16 v0, 0x55

    mul-int v2, v0, v0

    const/16 v3, 0x20

    mul-int v4, v3, v3

    add-int/2addr v2, v4

    mul-int/lit8 v0, v0, 0x20

    mul-int/lit8 v0, v0, 0x2

    sub-int/2addr v2, v0

    if-gez v2, :cond_2

    nop

    :cond_2
    :goto_1
    :pswitch_2
    return v1

    :pswitch_data_0
    .packed-switch 0x3c
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
