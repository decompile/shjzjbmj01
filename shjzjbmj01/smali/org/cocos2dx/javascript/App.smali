.class public Lorg/cocos2dx/javascript/App;
.super Landroid/app/Application;
.source "App.java"


# static fields
.field public static _ins:Landroid/app/Application;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static getPhoneInfo()Ljava/lang/String;
    .locals 1

    .line 31
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getPhoneModel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static initTTTransform()V
    .locals 3

    .line 196
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "=========TTApplication onCreate()==========="

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 197
    new-instance v0, Lcom/bytedance/applog/InitConfig;

    const-string v1, "216082"

    const-string v2, "shjzjb01"

    invoke-direct {v0, v1, v2}, Lcom/bytedance/applog/InitConfig;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 198
    invoke-virtual {v0, v1}, Lcom/bytedance/applog/InitConfig;->setUriConfig(I)Lcom/bytedance/applog/InitConfig;

    .line 199
    sget-object v1, Lorg/cocos2dx/javascript/-$$Lambda$App$UYUu2jqj2X5quuH6zW2WQbeU7Hc;->INSTANCE:Lorg/cocos2dx/javascript/-$$Lambda$App$UYUu2jqj2X5quuH6zW2WQbeU7Hc;

    invoke-virtual {v0, v1}, Lcom/bytedance/applog/InitConfig;->setLogger(Lcom/bytedance/applog/ILogger;)Lcom/bytedance/applog/InitConfig;

    const/4 v1, 0x1

    .line 200
    invoke-virtual {v0, v1}, Lcom/bytedance/applog/InitConfig;->setEnablePlay(Z)Lcom/bytedance/applog/InitConfig;

    .line 201
    invoke-virtual {v0, v1}, Lcom/bytedance/applog/InitConfig;->setAbEnable(Z)V

    .line 202
    invoke-virtual {v0, v1}, Lcom/bytedance/applog/InitConfig;->setAutoStart(Z)Lcom/bytedance/applog/InitConfig;

    .line 203
    sget-object v1, Lorg/cocos2dx/javascript/App;->_ins:Landroid/app/Application;

    invoke-static {v1, v0}, Lcom/bytedance/applog/AppLog;->init(Landroid/content/Context;Lcom/bytedance/applog/InitConfig;)V

    return-void
.end method

.method static synthetic lambda$initTTTransform$0(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    const-string v0, "active"

    .line 199
    invoke-static {v0, p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public static reportGDTTransformWatchVideo()V
    .locals 6

    .line 183
    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    .line 184
    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    .line 185
    sget-wide v3, Lorg/cocos2dx/javascript/GameDef;->REG_TIME:J

    .line 186
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/qmo/game/mpsdk/utils/Report;->reportGDTTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;J)V

    .line 189
    sget-wide v4, Lorg/cocos2dx/javascript/GameDef;->REG_TIME:J

    .line 190
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const/4 v3, 0x3

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportGDTTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V

    return-void
.end method

.method public static reportGDTWXLoginTransform(Ljava/lang/String;)V
    .locals 8

    .line 163
    sget-object v6, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    .line 164
    sget-object v7, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    .line 165
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const-string v4, "gdt"

    const-string v5, "before java report"

    const/16 v3, 0x3ef

    move-object v1, v7

    move-object v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 167
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    invoke-virtual {v0, v6, v7, v4, v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportGDTTransformNextDayRetention(Ljava/lang/String;Ljava/lang/String;J)V

    .line 169
    sput-wide v4, Lorg/cocos2dx/javascript/GameDef;->REG_TIME:J

    .line 171
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const/4 v3, 0x2

    move-object v1, v6

    move-object v2, v7

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportGDTTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V

    const-string v0, "cocos"

    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gameId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",openId\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",accountCreateTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ",scene\uff1a"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p0, 0x2

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const-string v4, "gdt"

    const-string v5, "after java report"

    const/16 v3, 0x3f0

    move-object v1, v7

    move-object v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string p0, "gdt"

    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loginActiveReportOver(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\')"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 179
    invoke-static {p0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    return-void
.end method

.method public static reportKSTransformWatchVideo()V
    .locals 6

    .line 120
    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    .line 121
    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    .line 122
    sget-wide v3, Lorg/cocos2dx/javascript/GameDef;->REG_TIME:J

    .line 123
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/qmo/game/mpsdk/utils/Report;->reportKSTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;J)V

    .line 126
    sget-wide v4, Lorg/cocos2dx/javascript/GameDef;->REG_TIME:J

    .line 127
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const/4 v3, 0x3

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportKSTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V

    return-void
.end method

.method public static reportKSWXLoginTransform(Ljava/lang/String;)V
    .locals 8

    .line 101
    sget-object v6, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    .line 102
    sget-object v7, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    .line 103
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const-string v4, "ks"

    const-string v5, "before java report"

    const/16 v3, 0x3ef

    move-object v1, v7

    move-object v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 105
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    invoke-virtual {v0, v6, v7, v4, v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportKSTransformNextDayRetention(Ljava/lang/String;Ljava/lang/String;J)V

    .line 107
    sput-wide v4, Lorg/cocos2dx/javascript/GameDef;->REG_TIME:J

    .line 109
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const/4 v3, 0x2

    move-object v1, v6

    move-object v2, v7

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportKSTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V

    const-string v0, "cocos"

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gameId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",openId\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",accountCreateTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ",scene\uff1a"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p0, 0x2

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const-string v4, "ks"

    const-string v5, "after java report"

    const/16 v3, 0x3f0

    move-object v1, v7

    move-object v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string p0, "ks"

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loginActiveReportOver(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\')"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 116
    invoke-static {p0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    return-void
.end method

.method public static reportStart(Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 7

    .line 54
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_KWAI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v1

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move-wide v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportKSTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V

    goto :goto_0

    .line 56
    :cond_0
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_BYTEDANCE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v1

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move-wide v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportTTTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V

    goto :goto_0

    .line 58
    :cond_1
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_GDT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v1

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move-wide v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportGDTTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V

    :cond_2
    :goto_0
    return-void
.end method

.method public static reportTTTransformWatchVideo()V
    .locals 6

    .line 151
    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    .line 152
    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    .line 153
    sget-wide v3, Lorg/cocos2dx/javascript/GameDef;->REG_TIME:J

    .line 154
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/qmo/game/mpsdk/utils/Report;->reportTTTransformWatchVideo(Ljava/lang/String;Ljava/lang/String;J)V

    .line 157
    sget-wide v4, Lorg/cocos2dx/javascript/GameDef;->REG_TIME:J

    .line 158
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const/4 v3, 0x3

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportTTTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V

    return-void
.end method

.method public static reportTTWXLoginTransform(Ljava/lang/String;)V
    .locals 10

    .line 132
    sget-object v6, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    .line 133
    sget-object v7, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    .line 134
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 135
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const-string v4, "csj"

    const-string v5, "before java report"

    const/16 v3, 0x3ef

    move-object v1, v7

    move-object v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/qmo/game/mpsdk/utils/Report;->reportTTTransformNextDayRetention(Ljava/lang/String;Ljava/lang/String;J)V

    .line 138
    sput-wide v8, Lorg/cocos2dx/javascript/GameDef;->REG_TIME:J

    .line 140
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const/4 v3, 0x2

    move-object v1, v6

    move-object v2, v7

    move-wide v4, v8

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportTTTransformActive(Ljava/lang/String;Ljava/lang/String;IJ)V

    const-string v0, "cocos"

    .line 142
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gameId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",openId\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",accountCreateTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ",scene\uff1a"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p0, 0x2

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    const-string v4, "csj"

    const-string v5, "after java report"

    const/16 v3, 0x3f0

    move-object v1, v7

    move-object v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string p0, "tt"

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loginActiveReportOver(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\')"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 147
    invoke-static {p0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    return-void
.end method

.method public static reportTransformWatchVideo()V
    .locals 2

    .line 90
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_KWAI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-static {}, Lorg/cocos2dx/javascript/App;->reportKSTransformWatchVideo()V

    goto :goto_0

    .line 92
    :cond_0
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_BYTEDANCE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    invoke-static {}, Lorg/cocos2dx/javascript/App;->reportTTTransformWatchVideo()V

    goto :goto_0

    .line 94
    :cond_1
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_GDT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    invoke-static {}, Lorg/cocos2dx/javascript/App;->reportGDTTransformWatchVideo()V

    :cond_2
    :goto_0
    return-void
.end method

.method public static reportWXLoginTransform(Ljava/lang/String;)V
    .locals 2

    .line 65
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->IS_USE_SDK_TRANS:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    sget-object p0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_KWAI:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    .line 68
    :cond_0
    sget-object p0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_BYTEDANCE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 69
    invoke-static {}, Lorg/cocos2dx/javascript/App;->initTTTransform()V

    goto :goto_0

    .line 70
    :cond_1
    sget-object p0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_GDT:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :goto_0
    return-void

    .line 76
    :cond_2
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_KWAI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    invoke-static {p0}, Lorg/cocos2dx/javascript/App;->reportKSWXLoginTransform(Ljava/lang/String;)V

    goto :goto_1

    .line 78
    :cond_3
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_BYTEDANCE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    invoke-static {p0}, Lorg/cocos2dx/javascript/App;->reportTTWXLoginTransform(Ljava/lang/String;)V

    goto :goto_1

    .line 80
    :cond_4
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_GDT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 81
    invoke-static {p0}, Lorg/cocos2dx/javascript/App;->reportGDTWXLoginTransform(Ljava/lang/String;)V

    :cond_5
    :goto_1
    return-void
.end method

.method public static reportWxInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const-string v0, "cocos"

    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nickname:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",unionid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v0

    sget-object v1, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p0, p1}, Lcom/qmo/game/mpsdk/utils/Report;->reportUserNameAndUnionID(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0
    invoke-static {p1}, Lcn/dxs/myapplication/hookpms/ServiceManagerWraper;->hookPMS(Landroid/content/Context;)V

    .line 45
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 46
    invoke-static {p1}, Landroidx/multidex/MultiDex;->install(Landroid/content/Context;)V

    return-void
.end method

.method public onCreate()V
    .locals 3

    .line 20
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 22
    sput-object p0, Lorg/cocos2dx/javascript/App;->_ins:Landroid/app/Application;

    .line 24
    invoke-static {p0}, Lorg/cocos2dx/javascript/TTAdManagerHolder;->init(Landroid/content/Context;)V

    const-string v0, "App"

    const-string v1, "App-->onCreate-0<TTAdManagerHolder.init"

    .line 25
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "31e988ea42"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/tencent/bugly/crashreport/CrashReport;->initCrashReport(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method
