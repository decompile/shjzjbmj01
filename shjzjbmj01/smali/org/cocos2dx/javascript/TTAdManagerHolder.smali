.class public Lorg/cocos2dx/javascript/TTAdManagerHolder;
.super Ljava/lang/Object;
.source "TTAdManagerHolder.java"


# static fields
.field private static sInit:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static buildConfig()Lcom/bytedance/msdk/api/TTAdConfig;
    .locals 3

    .line 37
    new-instance v0, Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    invoke-direct {v0}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;-><init>()V

    const-string v1, "5113347"

    .line 38
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->appId(Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    const-string v1, "APP\u6d4b\u8bd5\u5a92\u4f53"

    .line 39
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->appName(Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 40
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->openAdnTest(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    .line 41
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->isPanglePaid(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    const/4 v2, 0x1

    .line 42
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->openDebugLog(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    .line 43
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->usePangleTextureView(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->setPangleTitleBarTheme(I)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->allowPangleShowNotify(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    .line 46
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->allowPangleShowPageWhenScreenLock(Z)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 47
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->setPangleDirectDownloadNetworkType([I)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    .line 48
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->needPangleClearTaskReset([Ljava/lang/String;)Lcom/bytedance/msdk/api/TTAdConfig$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/TTAdConfig$Builder;->build()Lcom/bytedance/msdk/api/TTAdConfig;

    move-result-object v0

    return-object v0

    :array_0
    .array-data 4
        0x4
        0x3
    .end array-data
.end method

.method private static doInit(Landroid/content/Context;)V
    .locals 1

    .line 30
    sget-boolean v0, Lorg/cocos2dx/javascript/TTAdManagerHolder;->sInit:Z

    if-nez v0, :cond_0

    .line 31
    invoke-static {}, Lorg/cocos2dx/javascript/TTAdManagerHolder;->buildConfig()Lcom/bytedance/msdk/api/TTAdConfig;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->initialize(Landroid/content/Context;Lcom/bytedance/msdk/api/TTAdConfig;)V

    const/4 p0, 0x1

    .line 32
    sput-boolean p0, Lorg/cocos2dx/javascript/TTAdManagerHolder;->sInit:Z

    :cond_0
    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .locals 0

    .line 20
    invoke-static {p0}, Lorg/cocos2dx/javascript/TTAdManagerHolder;->doInit(Landroid/content/Context;)V

    return-void
.end method

.method public static initUnitySdkBanner(Landroid/app/Activity;)V
    .locals 0

    .line 24
    invoke-static {p0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->initUnityForBanner(Landroid/app/Activity;)V

    return-void
.end method
