.class public Lorg/cocos2dx/javascript/GameDef;
.super Ljava/lang/Object;
.source "GameDef.java"


# static fields
.field public static APP_NAME:Ljava/lang/String; = "csxtz"

.field public static BYTEFANCE_APPID:Ljava/lang/String; = "5057959"

.field public static GAME_ID:Ljava/lang/String; = "7393"

.field public static GAME_PATH:Ljava/lang/String; = "7393"

.field public static GDT_APPID:Ljava/lang/String; = "1110360331"

.field public static final IS_GM_OPEN:Z = false

.field public static IS_USE_SDK_TRANS:Ljava/lang/Boolean; = null

.field public static OPERATE_DOWNLOAD_FAIL:Ljava/lang/String; = "downloadFail"

.field public static OPERATE_DOWNLOAD_SUCC:Ljava/lang/String; = "downloadSucc"

.field public static OPERATE_JUMP:Ljava/lang/String; = "jump"

.field public static OPERATE_START_DOWNLOAD:Ljava/lang/String; = "startDownload"

.field public static REG_TIME:J = 0x0L

.field public static TRANSFORM_PLAT:Ljava/lang/String; = null

.field public static USER_ID:Ljava/lang/String; = ""

.field public static VIDEO_CHANNEL_BYTEDANCE:Ljava/lang/String; = "csj"

.field public static VIDEO_CHANNEL_GDT:Ljava/lang/String; = "gdt"

.field public static VIDEO_CHANNEL_KWAI:Ljava/lang/String; = "kwai"

.field public static videoConfigList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/cocos2dx/javascript/ConfigBean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->VIDEO_CHANNEL_KWAI:Ljava/lang/String;

    sput-object v0, Lorg/cocos2dx/javascript/GameDef;->TRANSFORM_PLAT:Ljava/lang/String;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lorg/cocos2dx/javascript/GameDef;->IS_USE_SDK_TRANS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
