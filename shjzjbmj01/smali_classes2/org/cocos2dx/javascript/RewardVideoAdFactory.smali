.class public Lorg/cocos2dx/javascript/RewardVideoAdFactory;
.super Ljava/lang/Object;
.source "RewardVideoAdFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "cocos video "

.field private static adids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static appActivity:Lorg/cocos2dx/javascript/AppActivity;

.field private static cashStatu:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static curShowingAdJsonInfo:Lorg/json/JSONObject;

.field private static handler:Landroid/os/Handler;

.field private static isClick:Z

.field private static loadingAdId:Ljava/lang/String;

.field private static loadingStatu:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;

.field private static mTTRewardedAdListener:Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

.field private static showingAdId:Ljava/lang/String;

.field private static videosEntity:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/bytedance/msdk/api/reward/TTRewardAd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->videosEntity:Ljava/util/Map;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadingStatu:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->cashStatu:Ljava/util/Map;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    const-string v0, ""

    .line 31
    sput-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadingAdId:Ljava/lang/String;

    const-string v0, ""

    .line 32
    sput-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->showingAdId:Ljava/lang/String;

    const/4 v0, 0x0

    .line 33
    sput-boolean v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->isClick:Z

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->handler:Landroid/os/Handler;

    .line 195
    new-instance v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory$3;

    invoke-direct {v0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory$3;-><init>()V

    sput-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;

    .line 265
    new-instance v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory$4;

    invoke-direct {v0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory$4;-><init>()V

    sput-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->mTTRewardedAdListener:Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/Map;
    .locals 1

    .line 24
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadingStatu:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    .line 24
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->cashStatu:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1000(Ljava/lang/String;I)V
    .locals 0

    .line 24
    invoke-static {p0, p1}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadAdWithCallback(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$200()Ljava/util/Map;
    .locals 1

    .line 24
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->videosEntity:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300()Lorg/cocos2dx/javascript/AppActivity;
    .locals 1

    .line 24
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    return-object v0
.end method

.method static synthetic access$400()Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;
    .locals 1

    .line 24
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->mTTRewardedAdListener:Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .line 24
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->showingAdId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 24
    sput-object p0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->showingAdId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600()Z
    .locals 1

    .line 24
    sget-boolean v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->isClick:Z

    return v0
.end method

.method static synthetic access$602(Z)Z
    .locals 0

    .line 24
    sput-boolean p0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->isClick:Z

    return p0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .line 24
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadingAdId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Ljava/lang/String;I)V
    .locals 0

    .line 24
    invoke-static {p0, p1}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadAd(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$900()Lorg/json/JSONObject;
    .locals 1

    .line 24
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->curShowingAdJsonInfo:Lorg/json/JSONObject;

    return-object v0
.end method

.method static synthetic access$902(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 0

    .line 24
    sput-object p0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->curShowingAdJsonInfo:Lorg/json/JSONObject;

    return-object p0
.end method

.method public static getAllAdids()Ljava/lang/String;
    .locals 4

    const-string v0, ""

    .line 158
    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    .line 159
    :goto_0
    sget-object v3, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    add-int/lit8 v3, v1, -0x1

    if-eq v2, v3, :cond_0

    .line 161
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 163
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static init(Lorg/cocos2dx/javascript/AppActivity;)V
    .locals 0

    .line 38
    sput-object p0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    return-void
.end method

.method public static isVideoAdCashSucc(Ljava/lang/String;)Z
    .locals 1

    .line 68
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->cashStatu:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    if-eqz p0, :cond_0

    .line 70
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static isVideoAdEntityExist(Ljava/lang/String;)Z
    .locals 1

    .line 42
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->videosEntity:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/bytedance/msdk/api/reward/TTRewardAd;

    if-nez p0, :cond_0

    const-string p0, "cocos video "

    const-string v0, "\u5b9e\u4f53\u4e0d\u5b58\u5728"

    .line 44
    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method public static isVideoAdLoading(Ljava/lang/String;)Z
    .locals 1

    .line 52
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadingStatu:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    if-eqz p0, :cond_0

    .line 54
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static isVideoAdReady(Ljava/lang/String;)Z
    .locals 1

    .line 60
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->videosEntity:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/bytedance/msdk/api/reward/TTRewardAd;

    if-eqz p0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/bytedance/msdk/api/reward/TTRewardAd;->isReady()Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method private static loadAd(Ljava/lang/String;I)V
    .locals 4

    const-string v0, "cocos video "

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u52a0\u8f7d\u5e7f\u544a\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    new-instance v0, Lcom/bytedance/msdk/api/reward/TTRewardAd;

    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    invoke-direct {v0, v1, p0}, Lcom/bytedance/msdk/api/reward/TTRewardAd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 95
    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->videosEntity:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/msdk/api/reward/TTRewardAd;

    if-eqz v1, :cond_0

    .line 97
    invoke-virtual {v1}, Lcom/bytedance/msdk/api/reward/TTRewardAd;->destroy()V

    .line 99
    :cond_0
    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->videosEntity:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-static {p0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->tryPutIntoAdids(Ljava/lang/String;)V

    .line 101
    new-instance v1, Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    invoke-direct {v1}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;-><init>()V

    const/4 v2, 0x1

    .line 102
    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setMuted(Z)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v1

    const/4 v3, 0x0

    .line 103
    invoke-virtual {v1, v3}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setAdmobAppVolume(F)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->build()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v1

    .line 107
    new-instance v3, Lcom/bytedance/msdk/api/AdSlot$Builder;

    invoke-direct {v3}, Lcom/bytedance/msdk/api/AdSlot$Builder;-><init>()V

    .line 108
    invoke-virtual {v3, v1}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setTTVideoOption(Lcom/bytedance/msdk/api/TTVideoOption;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 109
    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setSupportDeepLink(Z)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    const-string v2, "\u91d1\u5e01"

    .line 110
    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setRewardName(Ljava/lang/String;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    const/4 v2, 0x3

    .line 111
    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setRewardAmount(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    .line 112
    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setUserID(Ljava/lang/String;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    const-string v2, "media_extra"

    .line 113
    invoke-virtual {v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setMediaExtra(Ljava/lang/String;)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v1

    .line 114
    invoke-virtual {v1, p1}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setOrientation(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object p1

    .line 117
    invoke-virtual {p1}, Lcom/bytedance/msdk/api/AdSlot$Builder;->build()Lcom/bytedance/msdk/api/AdSlot;

    move-result-object p1

    new-instance v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory$1;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory$1;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lcom/bytedance/msdk/api/reward/TTRewardAd;->loadRewardAd(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/reward/TTRewardedAdLoadCallback;)V

    return-void
.end method

.method private static loadAdWithCallback(Ljava/lang/String;I)V
    .locals 2

    .line 204
    sput-object p0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadingAdId:Ljava/lang/String;

    .line 205
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadingStatu:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->cashStatu:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    invoke-static {}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->configLoadSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    invoke-static {p0, p1}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadAd(Ljava/lang/String;I)V

    goto :goto_0

    .line 210
    :cond_0
    sget-object p0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;

    invoke-static {p0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->registerConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V

    :goto_0
    return-void
.end method

.method public static loadOtherAd(Ljava/lang/String;)V
    .locals 5

    const-string v0, "cocos video "

    const-string v1, "\u51c6\u5907\u52a0\u8f7d\u5176\u4ed6\u5e7f\u544a"

    .line 219
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    .line 220
    :goto_0
    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 221
    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_1

    .line 225
    :cond_0
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 226
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "cocos video "

    .line 227
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadOtherAd\uff1a"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-static {v1}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadVideoAd(Ljava/lang/String;)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static loadVideoAd(Ljava/lang/String;)V
    .locals 3

    .line 80
    invoke-static {p0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->isVideoAdLoading(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cocos video "

    .line 81
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isLoading\u52a0\u8f7d\u53d6\u6d88\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 84
    :cond_0
    invoke-static {p0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->isVideoAdReady(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "cocos video "

    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ready \u5df2\u51c6\u5907\u597d \u52a0\u8f7d\u53d6\u6d88\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    const/4 v0, 0x1

    .line 88
    invoke-static {p0, v0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadAdWithCallback(Ljava/lang/String;I)V

    return-void
.end method

.method public static parseAdInfoToString()Ljava/lang/String;
    .locals 9

    const-string v0, ""

    .line 249
    :try_start_0
    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->curShowingAdJsonInfo:Lorg/json/JSONObject;

    if-eqz v1, :cond_0

    .line 250
    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->curShowingAdJsonInfo:Lorg/json/JSONObject;

    const-string v2, "packagename"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 251
    sget-object v2, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->curShowingAdJsonInfo:Lorg/json/JSONObject;

    const-string v3, "appname"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 252
    sget-object v3, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->curShowingAdJsonInfo:Lorg/json/JSONObject;

    const-string v4, "desc"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 253
    sget-object v4, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->curShowingAdJsonInfo:Lorg/json/JSONObject;

    const-string v5, "platform"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 254
    sget-object v5, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->curShowingAdJsonInfo:Lorg/json/JSONObject;

    const-string v6, "appicon"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 255
    sget-object v6, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->showingAdId:Ljava/lang/String;

    .line 256
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "packagename___"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",adName___"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",desc___"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",platform___"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",appicon___"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",showingId___"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 259
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    :cond_0
    :goto_0
    return-object v0
.end method

.method public static printInfo()V
    .locals 6

    const-string v0, ""

    const/4 v1, 0x0

    .line 234
    :goto_0
    sget-object v2, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 235
    sget-object v2, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 237
    invoke-static {v2}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->isVideoAdLoading(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 238
    invoke-static {v2}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->isVideoAdReady(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 239
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ":{ready:"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ",loading:"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "},"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const-string v1, "cocos video "

    .line 242
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "statuStr["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static showVideoAd(Ljava/lang/String;)V
    .locals 2

    .line 170
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->printInfo()V

    .line 171
    sget-object v0, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->handler:Landroid/os/Handler;

    new-instance v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory$2;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory$2;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static tryPutIntoAdids(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 144
    :goto_0
    sget-object v2, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 145
    sget-object v2, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 146
    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "cocos video "

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tryPutIntoAdids \u5df2\u5b58\u5728"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    :cond_1
    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->adids:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "cocos video "

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tryPutIntoAdids \u653e\u5165\u4e00\u4e2a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    sget-object v1, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadingStatu:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
