.class Lorg/cocos2dx/javascript/InterstitialAd$3;
.super Ljava/lang/Object;
.source "InterstitialAd.java"

# interfaces
.implements Lcom/bytedance/msdk/api/interstitial/TTInterstitialAdLoadCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/cocos2dx/javascript/InterstitialAd;->loadInteractionAd()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/cocos2dx/javascript/InterstitialAd;


# direct methods
.method constructor <init>(Lorg/cocos2dx/javascript/InterstitialAd;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lorg/cocos2dx/javascript/InterstitialAd$3;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInterstitialLoad()V
    .locals 2

    .line 93
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$3;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/cocos2dx/javascript/InterstitialAd;->access$302(Lorg/cocos2dx/javascript/InterstitialAd;Z)Z

    .line 94
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$3;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/cocos2dx/javascript/InterstitialAd;->access$402(Lorg/cocos2dx/javascript/InterstitialAd;I)I

    const-string v0, "cocos insert "

    const-string v1, "load interaction ad success ! "

    .line 95
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onInterstitialLoadFail(Lcom/bytedance/msdk/api/AdError;)V
    .locals 3

    .line 86
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$3;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/cocos2dx/javascript/InterstitialAd;->access$302(Lorg/cocos2dx/javascript/InterstitialAd;Z)Z

    .line 87
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$3;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/InterstitialAd;->continueLoadFailTimesPlus()V

    const-string v0, "cocos insert "

    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "load interaction ad error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Lcom/bytedance/msdk/api/AdError;->code:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/bytedance/msdk/api/AdError;->message:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
