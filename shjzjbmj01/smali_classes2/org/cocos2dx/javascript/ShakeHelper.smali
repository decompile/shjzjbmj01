.class public Lorg/cocos2dx/javascript/ShakeHelper;
.super Ljava/lang/Object;
.source "ShakeHelper.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private LastTime:J

.field private LastX:F

.field private LastY:F

.field private LastZ:F

.field private lastSendTime:J

.field private mContext:Landroid/content/Context;

.field private mInterval:I

.field private mSensor:Landroid/hardware/Sensor;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSpeed:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1770

    .line 18
    iput v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSpeed:I

    const/16 v0, 0x32

    .line 20
    iput v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mInterval:I

    const-wide/16 v0, 0x0

    .line 22
    iput-wide v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->lastSendTime:J

    .line 29
    iput-object p1, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mContext:Landroid/content/Context;

    .line 30
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/ShakeHelper;->Start()V

    return-void
.end method


# virtual methods
.method public Start()V
    .locals 3

    .line 35
    iget-object v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mContext:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSensorManager:Landroid/hardware/SensorManager;

    .line 36
    iget-object v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSensor:Landroid/hardware/Sensor;

    .line 40
    :cond_0
    iget-object v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v2, v1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_1
    return-void
.end method

.method public Stop()V
    .locals 1

    .line 48
    iget-object v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8

    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 61
    iget-wide v2, p0, Lorg/cocos2dx/javascript/ShakeHelper;->LastTime:J

    sub-long v2, v0, v2

    iget v4, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mInterval:I

    int-to-long v4, v4

    cmp-long v6, v2, v4

    if-gez v6, :cond_0

    return-void

    .line 64
    :cond_0
    iput-wide v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->LastTime:J

    .line 66
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    .line 67
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    .line 68
    iget-object p1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x2

    aget p1, p1, v5

    .line 70
    iget v5, p0, Lorg/cocos2dx/javascript/ShakeHelper;->LastX:F

    sub-float v5, v2, v5

    .line 71
    iget v6, p0, Lorg/cocos2dx/javascript/ShakeHelper;->LastY:F

    sub-float v6, v4, v6

    .line 72
    iget v7, p0, Lorg/cocos2dx/javascript/ShakeHelper;->LastZ:F

    sub-float v7, p1, v7

    .line 74
    iput v2, p0, Lorg/cocos2dx/javascript/ShakeHelper;->LastX:F

    .line 75
    iput v4, p0, Lorg/cocos2dx/javascript/ShakeHelper;->LastY:F

    .line 76
    iput p1, p0, Lorg/cocos2dx/javascript/ShakeHelper;->LastZ:F

    mul-float v5, v5, v5

    mul-float v6, v6, v6

    add-float/2addr v5, v6

    mul-float v7, v7, v7

    add-float/2addr v5, v7

    float-to-double v4, v5

    .line 78
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    iget p1, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mInterval:I

    int-to-double v6, p1

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v4, v6

    const-wide v6, 0x40c3880000000000L    # 10000.0

    mul-double v4, v4, v6

    .line 80
    iget p1, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mSpeed:I

    int-to-double v6, p1

    cmpl-double p1, v4, v6

    if-ltz p1, :cond_1

    .line 82
    iget-wide v4, p0, Lorg/cocos2dx/javascript/ShakeHelper;->lastSendTime:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0x3e8

    cmp-long p1, v4, v6

    if-lez p1, :cond_1

    .line 83
    iput-wide v0, p0, Lorg/cocos2dx/javascript/ShakeHelper;->lastSendTime:J

    .line 84
    iget-object p1, p0, Lorg/cocos2dx/javascript/ShakeHelper;->mContext:Landroid/content/Context;

    const-string v0, "\u4f60\u6447\u6643\u4e86\u624b\u673a\uff01"

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 85
    sget-object p1, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {}, Lorg/cocos2dx/javascript/AppActivity;->shakeOnce()V

    :cond_1
    return-void
.end method
