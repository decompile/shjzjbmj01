.class Lorg/cocos2dx/javascript/NativeExAd$4;
.super Ljava/lang/Object;
.source "NativeExAd.java"

# interfaces
.implements Lcom/bytedance/msdk/api/nativeAd/TTNativeAdLoadCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/cocos2dx/javascript/NativeExAd;->loadAd()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/cocos2dx/javascript/NativeExAd;


# direct methods
.method constructor <init>(Lorg/cocos2dx/javascript/NativeExAd;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lorg/cocos2dx/javascript/NativeExAd$4;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdLoaded(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 112
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 117
    :cond_0
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd$4;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/cocos2dx/javascript/NativeExAd;->access$102(Lorg/cocos2dx/javascript/NativeExAd;I)I

    const-string v0, "cocos neAd "

    .line 118
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "on FeedAdLoaded success \u5171\u52a0\u8f7d"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "\u6761\u4fe1\u606f\u6d41\u5e7f\u544a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd$4;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;

    invoke-static {v0, p1}, Lorg/cocos2dx/javascript/NativeExAd;->access$200(Lorg/cocos2dx/javascript/NativeExAd;Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;)V

    return-void

    :cond_1
    :goto_0
    const-string p1, "cocos neAd "

    const-string v0, "on FeedAdLoaded: ad is null!"

    .line 113
    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object p1, p0, Lorg/cocos2dx/javascript/NativeExAd$4;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-virtual {p1}, Lorg/cocos2dx/javascript/NativeExAd;->continueLoadFailTimesPlus()V

    return-void
.end method

.method public onAdLoadedFial(Lcom/bytedance/msdk/api/AdError;)V
    .locals 1

    const-string p1, "cocos neAd "

    const-string v0, "onAdLoadedFial"

    .line 127
    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
