.class public Lorg/cocos2dx/javascript/AppActivity;
.super Lorg/cocos2dx/lib/Cocos2dxActivity;
.source "AppActivity.java"


# static fields
.field static final GAME_ID:Ljava/lang/String;

.field static final GAME_PATH:Ljava/lang/String;

.field static final IS_GM_OPEN:Z = false

.field private static final REQUEST_PHONE_STATE:I = 0x3e9

.field private static TAG:Ljava/lang/String; = "cocos"

.field public static _ins:Lorg/cocos2dx/javascript/AppActivity;

.field private static api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

.field private static downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

.field public static imei:Ljava/lang/String;

.field static insertAdFaultClickTag:Ljava/lang/String;

.field public static isReportShake:Z

.field public static mInterstitialAd:Lorg/cocos2dx/javascript/InterstitialAd;

.field public static mNativeEXAd:Lorg/cocos2dx/javascript/NativeExAd;

.field static nativeAdFaultClickTag:Ljava/lang/String;


# instance fields
.field public adClick:Ljava/lang/String;

.field public adOnClose:Ljava/lang/String;

.field public adOnshow:Ljava/lang/String;

.field private isRegistered:Z

.field private netWorkChangReceiver:Lorg/cocos2dx/javascript/NetWorkChangReceiver;

.field public shakeHelper:Lorg/cocos2dx/javascript/ShakeHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 76
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    sput-object v0, Lorg/cocos2dx/javascript/AppActivity;->GAME_ID:Ljava/lang/String;

    .line 77
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->GAME_PATH:Ljava/lang/String;

    sput-object v0, Lorg/cocos2dx/javascript/AppActivity;->GAME_PATH:Ljava/lang/String;

    const-string v0, ""

    .line 80
    sput-object v0, Lorg/cocos2dx/javascript/AppActivity;->insertAdFaultClickTag:Ljava/lang/String;

    const-string v0, ""

    .line 81
    sput-object v0, Lorg/cocos2dx/javascript/AppActivity;->nativeAdFaultClickTag:Ljava/lang/String;

    const/4 v0, 0x1

    .line 86
    sput-boolean v0, Lorg/cocos2dx/javascript/AppActivity;->isReportShake:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 72
    invoke-direct {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;-><init>()V

    const/4 v0, 0x0

    .line 90
    iput-boolean v0, p0, Lorg/cocos2dx/javascript/AppActivity;->isRegistered:Z

    const-string v0, ""

    .line 96
    iput-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adClick:Ljava/lang/String;

    const-string v0, ""

    .line 97
    iput-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnshow:Ljava/lang/String;

    const-string v0, ""

    .line 98
    iput-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnClose:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 72
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static bmpToByteArray(Landroid/graphics/Bitmap;Z)[B
    .locals 3

    .line 438
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 439
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    if-eqz p1, :cond_0

    .line 441
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 443
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    .line 445
    :try_start_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 447
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-object p0
.end method

.method private static buildTransaction(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    .line 497
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static closeTTNativeAd()V
    .locals 2

    .line 541
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    const-string v1, "\u5173\u95ed\u4fe1\u606f\u6d41\u5e7f\u544a"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->mNativeEXAd:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/NativeExAd;->hide()V

    return-void
.end method

.method public static cocosBridge(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "window."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "(\""

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 300
    sget-object p1, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    new-instance v0, Lorg/cocos2dx/javascript/AppActivity$3;

    invoke-direct {v0, p0}, Lorg/cocos2dx/javascript/AppActivity$3;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lorg/cocos2dx/javascript/AppActivity;->runOnGLThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static copyStr2CM(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "Android"

    const-string v1, "java copy start"

    .line 473
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lorg/cocos2dx/javascript/AppActivity$4;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/AppActivity$4;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 491
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const-string p0, "Android"

    const-string v0, "java copy end"

    .line 492
    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x1

    return p0
.end method

.method public static downloadApk(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 718
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {v0, p2}, Lorg/cocos2dx/javascript/DownloadHelper;->checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    const-string p1, "====\u5c1d\u8bd5\u542f\u52a8===="

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {p2, p0}, Lorg/cocos2dx/javascript/DownloadHelper;->openApp(Ljava/lang/String;Landroid/content/Context;)V

    .line 721
    sget-object p0, Lorg/cocos2dx/javascript/GameDef;->OPERATE_JUMP:Ljava/lang/String;

    invoke-static {p0, p3}, Lorg/cocos2dx/javascript/AppActivity;->downloadApkReport(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 724
    :cond_0
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "downloadApk:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {v0, p1}, Lorg/cocos2dx/javascript/DownloadHelper;->isFileInRecordList(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    .line 726
    sget-object v1, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    sget-object v1, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {v1, p1}, Lorg/cocos2dx/javascript/DownloadHelper;->formatPath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 727
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 728
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    invoke-static {v1}, Lorg/cocos2dx/javascript/DownloadHelper;->isDownLoadFinish(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 729
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    const-string p1, "====\u5305\u5df2\u7ecf\u4e0b\u597d\u4e86\uff0c\u5c1d\u8bd5\u5b89\u88c5===="

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/DownloadHelper;->installNormal(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 732
    :cond_1
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    const-string p1, "====\u5305\u6b63\u5728\u4e0b\u8f7d\u4e2d===="

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string p0, "\u6b63\u5728\u4e0b\u8f7d\u4e2d"

    .line 733
    invoke-static {p0}, Lorg/cocos2dx/javascript/AppActivity;->toast(Ljava/lang/String;)V

    goto :goto_0

    .line 736
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 737
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    invoke-static {v1}, Lorg/cocos2dx/javascript/DownloadHelper;->deleteFile(Ljava/lang/String;)Z

    const-string v0, "\u5f00\u59cb\u4e0b\u8f7d"

    .line 738
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->toast(Ljava/lang/String;)V

    .line 739
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "====\u76f4\u63a5\u5f00\u59cb\u4e0b\u8f7d "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "===="

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    sget-object v1, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    sget-object v2, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    const/4 v5, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lorg/cocos2dx/javascript/DownloadHelper;->install(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public static downloadApkReport(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 757
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "customAdreport(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\",\""

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    return-void
.end method

.method public static getImei()Ljava/lang/String;
    .locals 1

    .line 518
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->imei:Ljava/lang/String;

    return-object v0
.end method

.method public static getMpsdkGameId()Ljava/lang/String;
    .locals 1

    .line 777
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->GAME_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static getMpsdkGamePath()Ljava/lang/String;
    .locals 1

    .line 781
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->GAME_PATH:Ljava/lang/String;

    return-object v0
.end method

.method public static getWechatType(Ljava/lang/String;)I
    .locals 4

    .line 428
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x7bd5b79f

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_1

    const v1, 0x76508296

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "session"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "timeLine"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p0, -0x1

    :goto_1
    packed-switch p0, :pswitch_data_0

    return v3

    :pswitch_0
    return v2

    :pswitch_1
    return v3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static hasNotchHw(Lorg/cocos2dx/javascript/AppActivity;)Z
    .locals 3

    const/4 v0, 0x0

    .line 700
    :try_start_0
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/AppActivity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p0

    const-string v1, "com.huawei.android.util.HwNotchSizeUtil"

    .line 701
    invoke-virtual {p0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p0

    const-string v1, "hasNotchInScreen"

    .line 702
    new-array v2, v0, [Ljava/lang/Class;

    invoke-virtual {p0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 703
    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    return v0
.end method

.method private static hasNotchOPPO(Lorg/cocos2dx/javascript/AppActivity;)Z
    .locals 3

    .line 667
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/AppActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const-string v0, "com.oppo.feature.screen.heteromorphism"

    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result p0

    const-string v0, "DDD"

    .line 668
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Android hasNotchInScreen 2 result = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return p0
.end method

.method private static hasNotchVIVO(Lorg/cocos2dx/javascript/AppActivity;)Z
    .locals 5

    const/4 p0, 0x0

    :try_start_0
    const-string v0, "android.util.FtFeature"

    .line 650
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "isFeatureSupport"

    const/4 v2, 0x1

    .line 651
    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, p0

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 652
    new-array v2, v2, [Ljava/lang/Object;

    const/16 v3, 0x20

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, p0

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    .line 654
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return p0
.end method

.method private static hasNotchXiaoMi(Lorg/cocos2dx/javascript/AppActivity;)Z
    .locals 6

    const/4 p0, 0x0

    :try_start_0
    const-string v0, "android.os.SystemProperties"

    .line 681
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "getInt"

    const/4 v2, 0x2

    .line 682
    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, p0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 683
    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "ro.miui.notch"

    aput-object v3, v2, p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v0, v5, :cond_0

    const/4 p0, 0x1

    :cond_0
    return p0

    :catch_0
    move-exception v0

    .line 685
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return p0
.end method

.method public static initNeInsertAdCode(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 798
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u8bbe\u7f6e\u4fe1\u606f\u6d41\u3001\u63d2\u5c4f\u5e7f\u544a\uff0cneAdStr:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "insertAdStr:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->mNativeEXAd:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-virtual {v0, p0}, Lorg/cocos2dx/javascript/NativeExAd;->setAdId(Ljava/lang/String;)V

    .line 800
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->mInterstitialAd:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-virtual {p0, p1}, Lorg/cocos2dx/javascript/InterstitialAd;->setAdId(Ljava/lang/String;)V

    return-void
.end method

.method public static invokeJs(Ljava/lang/String;)V
    .locals 2

    .line 577
    :try_start_0
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    new-instance v1, Lorg/cocos2dx/javascript/AppActivity$5;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/AppActivity$5;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/cocos2dx/javascript/AppActivity;->runOnGLThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 584
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public static isApkInstalled(Ljava/lang/String;)Z
    .locals 1

    .line 790
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {v0, p0}, Lorg/cocos2dx/javascript/DownloadHelper;->checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static isGMOpen()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static isTopHeadPhone()Z
    .locals 6

    .line 609
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    .line 611
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x0

    const/16 v3, 0x1c

    if-lt v1, v3, :cond_1

    .line 612
    invoke-virtual {v0}, Lorg/cocos2dx/javascript/AppActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getDisplayCutout()Landroid/view/DisplayCutout;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v2

    .line 619
    :cond_1
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "DDD"

    .line 620
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "manufacturer = : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_7

    .line 622
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_2
    const-string v3, "HUAWEI"

    .line 624
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 625
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->hasNotchHw(Lorg/cocos2dx/javascript/AppActivity;)Z

    move-result v0

    return v0

    :cond_3
    const-string v3, "xiaomi"

    .line 626
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 627
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->hasNotchXiaoMi(Lorg/cocos2dx/javascript/AppActivity;)Z

    move-result v0

    return v0

    :cond_4
    const-string v3, "oppo"

    .line 628
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v2, "DDD"

    .line 629
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "manufacturer 22= : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->hasNotchOPPO(Lorg/cocos2dx/javascript/AppActivity;)Z

    move-result v0

    return v0

    :cond_5
    const-string v3, "vivo"

    .line 631
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 632
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->hasNotchVIVO(Lorg/cocos2dx/javascript/AppActivity;)Z

    move-result v0

    return v0

    :cond_6
    const-string v0, "DDD"

    .line 634
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "manufacturer 11= : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_7
    :goto_0
    return v2
.end method

.method public static jsLog(Ljava/lang/String;)V
    .locals 1

    .line 294
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    const-string v0, "jsLog"

    invoke-static {v0, p0}, Lorg/cocos2dx/javascript/AppActivity;->cocosBridge(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static openApp(Ljava/lang/String;)Z
    .locals 3

    .line 746
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {v0, p0}, Lorg/cocos2dx/javascript/DownloadHelper;->checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 747
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "====\u5c1d\u8bd5\u542f\u52a8===="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {p0, v0}, Lorg/cocos2dx/javascript/DownloadHelper;->openApp(Ljava/lang/String;Landroid/content/Context;)V

    const/4 p0, 0x1

    return p0

    .line 751
    :cond_0
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "====\u5e94\u7528\u672a\u5b89\u88c5===="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    return p0
.end method

.method public static reprotNetBreak()V
    .locals 1

    const-string v0, "window.reportNetBreak()"

    .line 602
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->invokeJs(Ljava/lang/String;)V

    return-void
.end method

.method public static sendInsertAdFaultClick(Ljava/lang/String;)V
    .locals 3

    .line 554
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertAdFaultClick:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lorg/cocos2dx/javascript/AppActivity;->insertAdFaultClickTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertAdFaultClick(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\',\'"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->insertAdFaultClickTag:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\')"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 556
    invoke-static {p0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    return-void
.end method

.method public static sendInsertAdShow(Ljava/lang/String;)V
    .locals 3

    .line 548
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertAdShow:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lorg/cocos2dx/javascript/AppActivity;->insertAdFaultClickTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertAdShow(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\',\'"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->insertAdFaultClickTag:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\')"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 550
    invoke-static {p0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    return-void
.end method

.method public static sendNativeAdFaultClick(Ljava/lang/String;)V
    .locals 3

    .line 567
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nativeAdFaultClick"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lorg/cocos2dx/javascript/AppActivity;->nativeAdFaultClickTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nativeAdFaultClick(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\',\'"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->nativeAdFaultClickTag:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\')"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 569
    invoke-static {p0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    return-void
.end method

.method public static sendNativeAdShow(Ljava/lang/String;)V
    .locals 3

    .line 561
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nativeAdShow"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lorg/cocos2dx/javascript/AppActivity;->nativeAdFaultClickTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nativeAdShow(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\',\'"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->nativeAdFaultClickTag:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\')"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 563
    invoke-static {p0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    return-void
.end method

.method public static setOpenId(Ljava/lang/String;)V
    .locals 3

    .line 522
    sput-object p0, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    .line 523
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u8bbe\u7f6e\u7528\u6237userid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private setPhoneStateManifest()V
    .locals 4

    const-string v0, "android.permission.READ_PHONE_STATE"

    .line 453
    invoke-static {p0, v0}, Landroidx/core/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "android.permission.READ_PHONE_STATE"

    aput-object v3, v1, v2

    const/16 v2, 0x3e9

    invoke-static {v0, v1, v2}, Landroidx/core/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    const-string v0, "phone"

    .line 456
    invoke-virtual {p0, v0}, Lorg/cocos2dx/javascript/AppActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 457
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 458
    sget-object v1, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "imei:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    sput-object v0, Lorg/cocos2dx/javascript/AppActivity;->imei:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method public static setRoundMaxShowTimes(I)V
    .locals 3

    const-string v0, "cocos"

    .line 804
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "java\u8bbe\u7f6e\u64ad\u653e\u4e00\u5708\u6b21\u6570\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->mNativeEXAd:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-virtual {v0, p0}, Lorg/cocos2dx/javascript/NativeExAd;->setRoundMaxShowTimes(I)V

    return-void
.end method

.method public static setUserMsg(Ljava/lang/String;)V
    .locals 8

    const-string v0, "Android WXLogin"

    const-string v1, "wxLogin setUserMsg \u5b58\u50a8\u4fe1\u606f  "

    .line 280
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v2

    sget-object v3, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sget-object v4, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    const-string v6, "java_send_wx_info_to_js"

    const-string v7, ""

    const/16 v5, 0x3fc

    invoke-virtual/range {v2 .. v7}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 282
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    const-string v0, "<=============setUserMsg================>"

    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    .line 283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "window.saveUserMsg("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 284
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    new-instance v1, Lorg/cocos2dx/javascript/AppActivity$2;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/AppActivity$2;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/cocos2dx/javascript/AppActivity;->runOnGLThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static shakeOnce()V
    .locals 1

    .line 502
    sget-boolean v0, Lorg/cocos2dx/javascript/AppActivity;->isReportShake:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "window.shakeOnce()"

    .line 506
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->invokeJs(Ljava/lang/String;)V

    return-void
.end method

.method public static shareCaptureImageByThirdPlantform(Ljava/lang/String;)V
    .locals 3

    const-string v0, "jswrapper"

    .line 313
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 318
    new-instance v0, Lcom/tencent/mm/opensdk/modelmsg/WXImageObject;

    invoke-direct {v0, p0}, Lcom/tencent/mm/opensdk/modelmsg/WXImageObject;-><init>(Landroid/graphics/Bitmap;)V

    .line 319
    new-instance v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    invoke-direct {v1}, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;-><init>()V

    .line 320
    iput-object v0, v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->mediaObject:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage$IMediaObject;

    const/4 v0, 0x1

    const/16 v2, 0x48

    .line 323
    invoke-static {p0, v2, v2, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 324
    invoke-static {p0, v0}, Lorg/cocos2dx/javascript/AppActivity;->bmpToByteArray(Landroid/graphics/Bitmap;Z)[B

    move-result-object p0

    iput-object p0, v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->thumbData:[B

    .line 327
    new-instance p0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;

    invoke-direct {p0}, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;-><init>()V

    const-string v0, "img"

    .line 328
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->buildTransaction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->transaction:Ljava/lang/String;

    .line 329
    iput-object v1, p0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->message:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    const-string v0, "session"

    .line 330
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->getWechatType(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->scene:I

    .line 332
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {v0, p0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->sendReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)Z

    return-void
.end method

.method public static shareImageByThirdPlatform(Ljava/lang/String;I)V
    .locals 0

    const-string p0, "============="

    const-string p1, "shareImageByThirdPlatform"

    .line 339
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static shareImg(Ljava/lang/String;)V
    .locals 4

    .line 372
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 375
    new-instance v1, Lcom/tencent/mm/opensdk/modelmsg/WXImageObject;

    invoke-direct {v1, v0}, Lcom/tencent/mm/opensdk/modelmsg/WXImageObject;-><init>(Landroid/graphics/Bitmap;)V

    .line 376
    new-instance v2, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    invoke-direct {v2}, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;-><init>()V

    .line 377
    iput-object v1, v2, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->mediaObject:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage$IMediaObject;

    const/4 v1, 0x1

    const/16 v3, 0x48

    .line 380
    invoke-static {v0, v3, v3, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 381
    invoke-static {v0, v1}, Lorg/cocos2dx/javascript/AppActivity;->bmpToByteArray(Landroid/graphics/Bitmap;Z)[B

    move-result-object v0

    iput-object v0, v2, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->thumbData:[B

    .line 384
    new-instance v0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;

    invoke-direct {v0}, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;-><init>()V

    const-string v1, "img"

    .line 385
    invoke-static {v1}, Lorg/cocos2dx/javascript/AppActivity;->buildTransaction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->transaction:Ljava/lang/String;

    .line 386
    iput-object v2, v0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->message:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    .line 387
    invoke-static {p0}, Lorg/cocos2dx/javascript/AppActivity;->getWechatType(Ljava/lang/String;)I

    move-result p0

    iput p0, v0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->scene:I

    .line 389
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {p0, v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->sendReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)Z

    return-void
.end method

.method public static shareText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 356
    new-instance v0, Lcom/tencent/mm/opensdk/modelmsg/WXTextObject;

    invoke-direct {v0}, Lcom/tencent/mm/opensdk/modelmsg/WXTextObject;-><init>()V

    .line 357
    iput-object p0, v0, Lcom/tencent/mm/opensdk/modelmsg/WXTextObject;->text:Ljava/lang/String;

    .line 358
    new-instance v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    invoke-direct {v1}, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;-><init>()V

    .line 359
    iput-object v0, v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->mediaObject:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage$IMediaObject;

    .line 360
    iput-object p0, v1, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->description:Ljava/lang/String;

    .line 363
    new-instance p0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;

    invoke-direct {p0}, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;-><init>()V

    const-string v0, "text"

    .line 364
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->buildTransaction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->transaction:Ljava/lang/String;

    .line 365
    iput-object v1, p0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->message:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    .line 366
    invoke-static {p1}, Lorg/cocos2dx/javascript/AppActivity;->getWechatType(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->scene:I

    .line 367
    sget-object p1, Lorg/cocos2dx/javascript/AppActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {p1, p0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->sendReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)Z

    return-void
.end method

.method public static shareWebByThirdPlatform(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    const-string p0, "============="

    const-string p3, "shareWebByThirdPlatform"

    .line 347
    invoke-static {p0, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    const-string p0, "<==\u8c03\u7528wx\u5206\u4eab==>"

    invoke-static {p0}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    const-string p0, "session"

    .line 350
    invoke-static {p0, p1, p2}, Lorg/cocos2dx/javascript/AppActivity;->shareWebPage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static shareWebPage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 393
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "url:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "content:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    new-instance v0, Lcom/tencent/mm/opensdk/modelmsg/WXWebpageObject;

    invoke-direct {v0}, Lcom/tencent/mm/opensdk/modelmsg/WXWebpageObject;-><init>()V

    if-eqz p2, :cond_1

    .line 397
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 400
    :cond_0
    iput-object p2, v0, Lcom/tencent/mm/opensdk/modelmsg/WXWebpageObject;->webpageUrl:Ljava/lang/String;

    goto :goto_1

    :cond_1
    :goto_0
    const-string p2, "http://cdn-xyx.raink.com.cn/2048_ljxc/idiom_pp/money_king/index.html"

    .line 398
    iput-object p2, v0, Lcom/tencent/mm/opensdk/modelmsg/WXWebpageObject;->webpageUrl:Ljava/lang/String;

    .line 404
    :goto_1
    new-instance p2, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    invoke-direct {p2, v0}, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;-><init>(Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage$IMediaObject;)V

    const-string v0, "\u5c71\u6d77\u7ecf\u5f02\u53d8"

    .line 405
    iput-object v0, p2, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->title:Ljava/lang/String;

    if-eqz p1, :cond_3

    .line 406
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    goto :goto_2

    .line 409
    :cond_2
    iput-object p1, p2, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->description:Ljava/lang/String;

    goto :goto_3

    :cond_3
    :goto_2
    const-string p1, "0.3\u5143\u5373\u53ef\u63d0\u73b0\uff0c\u767e\u5143\u63d0\u73b0\u7acb\u5373\u5230\u8d26"

    .line 407
    iput-object p1, p2, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->description:Ljava/lang/String;

    .line 412
    :goto_3
    sget-object p1, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-virtual {p1}, Lorg/cocos2dx/javascript/AppActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 v0, 0x7f030000

    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 414
    invoke-virtual {p2, p1}, Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;->setThumbImage(Landroid/graphics/Bitmap;)V

    .line 416
    new-instance p1, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;

    invoke-direct {p1}, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;-><init>()V

    const-string v0, "webpage"

    .line 417
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->buildTransaction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->transaction:Ljava/lang/String;

    .line 418
    iput-object p2, p1, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->message:Lcom/tencent/mm/opensdk/modelmsg/WXMediaMessage;

    .line 419
    invoke-static {p0}, Lorg/cocos2dx/javascript/AppActivity;->getWechatType(Ljava/lang/String;)I

    move-result p0

    iput p0, p1, Lcom/tencent/mm/opensdk/modelmsg/SendMessageToWX$Req;->scene:I

    .line 423
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {p0, p1}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->sendReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)Z

    return-void
.end method

.method public static showTTInsertAd(Ljava/lang/String;)V
    .locals 1

    .line 528
    sput-object p0, Lorg/cocos2dx/javascript/AppActivity;->insertAdFaultClickTag:Ljava/lang/String;

    .line 529
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->mInterstitialAd:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-virtual {p0}, Lorg/cocos2dx/javascript/InterstitialAd;->show()V

    .line 530
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    const-string v0, "\u5c55\u793a\u7a7f\u5c71\u7532  \u63d2\u5c4f  \u5e7f\u544a"

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static showTTNativeAd(Ljava/lang/String;)V
    .locals 1

    .line 535
    sput-object p0, Lorg/cocos2dx/javascript/AppActivity;->nativeAdFaultClickTag:Ljava/lang/String;

    .line 536
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->mNativeEXAd:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-virtual {p0}, Lorg/cocos2dx/javascript/NativeExAd;->show()V

    .line 537
    sget-object p0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    const-string v0, "\u5c55\u793a\u4fe1\u606f\u6d41\u5e7f\u544a"

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static syncCardAd(II)V
    .locals 3

    .line 714
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "syncCardAd \u540c\u6b65\u5361\u7247\u5e7f\u544a\u5f00\u59cb:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ","

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static toast(Ljava/lang/String;)V
    .locals 2

    .line 761
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lorg/cocos2dx/javascript/AppActivity$6;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/AppActivity$6;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 772
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public static wxLogin()Z
    .locals 8

    .line 261
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "xxx wxLogin"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 262
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    const-string v0, "<==\u8c03\u7528wxLogin==>"

    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->jsLog(Ljava/lang/String;)V

    .line 263
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    if-eqz v0, :cond_0

    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->isWXAppInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    new-instance v0, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Req;

    invoke-direct {v0}, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Req;-><init>()V

    const-string v1, "snsapi_userinfo"

    .line 265
    iput-object v1, v0, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Req;->scope:Ljava/lang/String;

    const-string v1, "wechat_sdk_demo"

    .line 266
    iput-object v1, v0, Lcom/tencent/mm/opensdk/modelmsg/SendAuth$Req;->state:Ljava/lang/String;

    .line 267
    sget-object v1, Lorg/cocos2dx/javascript/AppActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    invoke-interface {v1, v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->sendReq(Lcom/tencent/mm/opensdk/modelbase/BaseReq;)Z

    const-string v0, "Android WXLogin"

    const-string v1, "\u6388\u6743\u5b8c\u6210"

    .line 268
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v2

    sget-object v3, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sget-object v4, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    const/16 v5, 0x3f4

    const-string v6, "start_wx_auth"

    const-string v7, ""

    invoke-virtual/range {v2 .. v7}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    .line 272
    :cond_0
    invoke-static {}, Lcom/qmo/game/mpsdk/utils/Report;->getInstance()Lcom/qmo/game/mpsdk/utils/Report;

    move-result-object v1

    sget-object v2, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sget-object v3, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    const/16 v4, 0x3f5

    const-string v5, "no_install_wx_app"

    const-string v6, ""

    invoke-virtual/range {v1 .. v6}, Lcom/qmo/game/mpsdk/utils/Report;->reportEvent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    const-string v0, "Android WXLogin"

    const-string v1, "\u7528\u6237\u672a\u5b89\u88c5\u5fae\u4fe1"

    .line 273
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "\u7528\u6237\u672a\u5b89\u88c5\u5fae\u4fe1"

    .line 274
    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->toast(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public cancelShake()V
    .locals 1

    const/4 v0, 0x0

    .line 510
    sput-boolean v0, Lorg/cocos2dx/javascript/AppActivity;->isReportShake:Z

    return-void
.end method

.method public initNetWorkLisner()V
    .locals 2

    .line 590
    new-instance v0, Lorg/cocos2dx/javascript/NetWorkChangReceiver;

    invoke-direct {v0}, Lorg/cocos2dx/javascript/NetWorkChangReceiver;-><init>()V

    iput-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->netWorkChangReceiver:Lorg/cocos2dx/javascript/NetWorkChangReceiver;

    .line 591
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    .line 592
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.STATE_CHANGE"

    .line 593
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    .line 594
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 595
    iget-object v1, p0, Lorg/cocos2dx/javascript/AppActivity;->netWorkChangReceiver:Lorg/cocos2dx/javascript/NetWorkChangReceiver;

    invoke-virtual {p0, v1, v0}, Lorg/cocos2dx/javascript/AppActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    .line 596
    iput-boolean v0, p0, Lorg/cocos2dx/javascript/AppActivity;->isRegistered:Z

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 182
    invoke-super {p0, p1, p2, p3}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 183
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/cocos2dx/javascript/SDKWrapper;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 206
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/SDKWrapper;->onBackPressed()V

    .line 207
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onBackPressed()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .line 212
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/cocos2dx/javascript/SDKWrapper;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 213
    invoke-super {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 105
    invoke-super {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/AppActivity;->isTaskRoot()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 109
    :cond_0
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object p1

    invoke-virtual {p1, p0}, Lorg/cocos2dx/javascript/SDKWrapper;->init(Landroid/content/Context;)V

    .line 111
    invoke-static {p0}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->getLocalDeviceUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 112
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->GAME_ID:Ljava/lang/String;

    new-instance v1, Lorg/cocos2dx/javascript/AppActivity$1;

    invoke-direct {v1, p0, p1}, Lorg/cocos2dx/javascript/AppActivity$1;-><init>(Lorg/cocos2dx/javascript/AppActivity;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->initMPSDK(Landroid/app/Activity;Ljava/lang/String;Lcom/qmo/game/mpsdk/utils/OnInitCallbackListener;)V

    .line 129
    sput-object p0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    .line 130
    sget-object p1, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    sget-object v0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->WX_APP_ID:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/tencent/mm/opensdk/openapi/WXAPIFactory;->createWXAPI(Landroid/content/Context;Ljava/lang/String;Z)Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    move-result-object p1

    sput-object p1, Lorg/cocos2dx/javascript/AppActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    .line 131
    sget-object p1, Lorg/cocos2dx/javascript/AppActivity;->api:Lcom/tencent/mm/opensdk/openapi/IWXAPI;

    sget-object v0, Lcom/qm/shjzjb/mj01/wxapi/WXEntryActivity;->WX_APP_ID:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/tencent/mm/opensdk/openapi/IWXAPI;->registerApp(Ljava/lang/String;)Z

    .line 133
    invoke-direct {p0}, Lorg/cocos2dx/javascript/AppActivity;->setPhoneStateManifest()V

    .line 135
    new-instance p1, Lorg/cocos2dx/javascript/ShakeHelper;

    invoke-direct {p1, p0}, Lorg/cocos2dx/javascript/ShakeHelper;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lorg/cocos2dx/javascript/AppActivity;->shakeHelper:Lorg/cocos2dx/javascript/ShakeHelper;

    .line 136
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/AppActivity;->initNetWorkLisner()V

    .line 138
    new-instance p1, Lorg/cocos2dx/javascript/DownloadHelper;

    invoke-direct {p1}, Lorg/cocos2dx/javascript/DownloadHelper;-><init>()V

    sput-object p1, Lorg/cocos2dx/javascript/AppActivity;->downloadhelper:Lorg/cocos2dx/javascript/DownloadHelper;

    .line 140
    new-instance p1, Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-direct {p1, p0}, Lorg/cocos2dx/javascript/InterstitialAd;-><init>(Lorg/cocos2dx/javascript/AppActivity;)V

    sput-object p1, Lorg/cocos2dx/javascript/AppActivity;->mInterstitialAd:Lorg/cocos2dx/javascript/InterstitialAd;

    .line 142
    new-instance p1, Lorg/cocos2dx/javascript/NativeExAd;

    invoke-direct {p1, p0}, Lorg/cocos2dx/javascript/NativeExAd;-><init>(Lorg/cocos2dx/javascript/AppActivity;)V

    sput-object p1, Lorg/cocos2dx/javascript/AppActivity;->mNativeEXAd:Lorg/cocos2dx/javascript/NativeExAd;

    .line 143
    invoke-static {p0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->init(Lorg/cocos2dx/javascript/AppActivity;)V

    return-void
.end method

.method public onCreateView()Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;
    .locals 8

    .line 148
    new-instance v7, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;

    invoke-direct {v7, p0}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x5

    const/4 v2, 0x6

    const/4 v3, 0x5

    const/4 v4, 0x0

    const/16 v5, 0x10

    const/16 v6, 0x8

    move-object v0, v7

    .line 149
    invoke-virtual/range {v0 .. v6}, Lorg/cocos2dx/lib/Cocos2dxGLSurfaceView;->setEGLConfigChooser(IIIIII)V

    .line 150
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0, v7, p0}, Lorg/cocos2dx/javascript/SDKWrapper;->setGLSurfaceView(Landroid/opengl/GLSurfaceView;Landroid/content/Context;)V

    return-object v7
.end method

.method protected onDestroy()V
    .locals 1

    .line 172
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onDestroy()V

    .line 173
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/SDKWrapper;->onDestroy()V

    .line 175
    iget-boolean v0, p0, Lorg/cocos2dx/javascript/AppActivity;->isRegistered:Z

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->netWorkChangReceiver:Lorg/cocos2dx/javascript/NetWorkChangReceiver;

    invoke-virtual {p0, v0}, Lorg/cocos2dx/javascript/AppActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 188
    invoke-super {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 189
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/cocos2dx/javascript/SDKWrapper;->onNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 164
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onPause()V

    .line 165
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/SDKWrapper;->onPause()V

    .line 166
    iget-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->shakeHelper:Lorg/cocos2dx/javascript/ShakeHelper;

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/ShakeHelper;->Stop()V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .line 464
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u8bf7\u6c42\u6743\u9650\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    invoke-super {p0, p1, p2, p3}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 466
    invoke-static {p1, p2, p3}, Lcom/qmo/game/mpsdk/utils/MpsdkNativeUtils;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    return-void
.end method

.method protected onRestart()V
    .locals 1

    .line 194
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onRestart()V

    .line 195
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/SDKWrapper;->onRestart()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 218
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/cocos2dx/javascript/SDKWrapper;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 219
    invoke-super {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 156
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onResume()V

    .line 157
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/SDKWrapper;->onResume()V

    .line 158
    iget-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->shakeHelper:Lorg/cocos2dx/javascript/ShakeHelper;

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/ShakeHelper;->Start()V

    .line 159
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/AppActivity;->report()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 224
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/cocos2dx/javascript/SDKWrapper;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 225
    invoke-super {p0, p1}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 230
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/SDKWrapper;->onStart()V

    .line 231
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    .line 200
    invoke-super {p0}, Lorg/cocos2dx/lib/Cocos2dxActivity;->onStop()V

    .line 201
    invoke-static {}, Lorg/cocos2dx/javascript/SDKWrapper;->getInstance()Lorg/cocos2dx/javascript/SDKWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/SDKWrapper;->onStop()V

    return-void
.end method

.method public openShake()V
    .locals 1

    const/4 v0, 0x1

    .line 514
    sput-boolean v0, Lorg/cocos2dx/javascript/AppActivity;->isReportShake:Z

    return-void
.end method

.method public report()V
    .locals 2

    .line 235
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->TAG:Ljava/lang/String;

    const-string v1, "\u5f00\u59cb\u4e0a\u62a5"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adClick:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adClick:Ljava/lang/String;

    invoke-static {v0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    const-string v0, ""

    .line 238
    iput-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adClick:Ljava/lang/String;

    .line 240
    :cond_0
    iget-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnshow:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 241
    iget-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnshow:Ljava/lang/String;

    invoke-static {v0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    const-string v0, ""

    .line 242
    iput-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnshow:Ljava/lang/String;

    .line 244
    :cond_1
    iget-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnClose:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 245
    iget-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnClose:Ljava/lang/String;

    invoke-static {v0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    const-string v0, ""

    .line 246
    iput-object v0, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnClose:Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public setAdOnClickInfo(Ljava/lang/String;)V
    .locals 0

    .line 250
    iput-object p1, p0, Lorg/cocos2dx/javascript/AppActivity;->adClick:Ljava/lang/String;

    return-void
.end method

.method public setAdOnCloseInfo(Ljava/lang/String;)V
    .locals 0

    .line 256
    iput-object p1, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnClose:Ljava/lang/String;

    return-void
.end method

.method public setAdOnShowInfo(Ljava/lang/String;)V
    .locals 0

    .line 253
    iput-object p1, p0, Lorg/cocos2dx/javascript/AppActivity;->adOnshow:Ljava/lang/String;

    return-void
.end method
