.class public Lorg/cocos2dx/javascript/ADInfoGetUtils;
.super Ljava/lang/Object;
.source "ADInfoGetUtils.java"


# static fields
.field private static final BaseUrl:Ljava/lang/String; = "https://xyxcck-log-ad.raink.com.cn/MiniGameLog/log/adContentCensus.action"

.field public static GAMEID:Ljava/lang/String; = null

.field private static TAG:Ljava/lang/String; = "QMOSDK::ADInfoGetUtils"

.field public static USERID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->GAME_ID:Ljava/lang/String;

    sput-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GAMEID:Ljava/lang/String;

    .line 20
    sget-object v0, Lorg/cocos2dx/javascript/GameDef;->USER_ID:Ljava/lang/String;

    sput-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->USERID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetAdInfo(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 24
    sget-object p0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u89e3\u6790\u5bf9\u8c61\u4e0d\u80fd\u4e3anull"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0

    .line 27
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.bytedance.msdk.api.reward.TTRewardAd"

    .line 28
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 29
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_MOBRAIN(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_1
    const-string v2, "com.anythink.rewardvideo.api.ATRewardVideoAd"

    .line 30
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 31
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_TOPON(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v2, "com.bytedance.sdk.openadsdk.TTRewardVideoAd"

    .line 32
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 33
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_3
    const-string v2, "com.qq.e.ads.rewardvideo.RewardVideoAD"

    .line 34
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 35
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_4
    const-string v2, "com.kwad.sdk.api.KsRewardVideoAd"

    .line 36
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 37
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    .line 39
    :cond_5
    sget-object p0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u89e3\u6790\u5bf9\u8c61\u6ca1\u6709\u9002\u914d:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public static GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 110
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_CSJ_3300(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 112
    :cond_0
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u7a7f\u5c71\u75323300\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_CSJ_3251(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_1

    return-object p0

    .line 116
    :cond_1
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u7a7f\u5c71\u75323251\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method private static GetAdInfo_CSJ_3251(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 8

    .line 167
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u7a7f\u5c71\u75323251\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "b"

    .line 170
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "V"

    .line 172
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    return-object v0

    :cond_2
    const-string v2, "k"

    .line 174
    invoke-static {p0, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 175
    instance-of v3, v2, Ljava/lang/String;

    if-nez v3, :cond_3

    return-object v0

    .line 176
    :cond_3
    check-cast v2, Ljava/lang/String;

    const-string v3, "b"

    .line 177
    invoke-static {v1, v3}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 178
    instance-of v4, v3, Ljava/lang/String;

    if-nez v4, :cond_4

    return-object v0

    .line 179
    :cond_4
    check-cast v3, Ljava/lang/String;

    const-string v4, "e"

    .line 180
    invoke-static {v1, v4}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 181
    instance-of v5, v4, Ljava/lang/String;

    if-nez v5, :cond_5

    return-object v0

    .line 182
    :cond_5
    check-cast v4, Ljava/lang/String;

    const-string v5, "f"

    .line 183
    invoke-static {v1, v5}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 184
    instance-of v5, v1, Ljava/lang/String;

    if-nez v5, :cond_6

    return-object v0

    .line 185
    :cond_6
    check-cast v1, Ljava/lang/String;

    const-string v5, ""

    if-eqz v1, :cond_7

    const-string v6, ""

    .line 187
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_7
    const-string v1, "l"

    .line 188
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 189
    instance-of v6, v1, Ljava/lang/String;

    if-nez v6, :cond_8

    return-object v0

    .line 190
    :cond_8
    check-cast v1, Ljava/lang/String;

    :cond_9
    if-eqz v1, :cond_a

    const-string v6, ""

    .line 192
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    :cond_a
    const-string v1, "r"

    .line 193
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 194
    instance-of v1, p0, Ljava/lang/String;

    if-nez v1, :cond_b

    return-object v0

    .line 195
    :cond_b
    move-object v1, p0

    check-cast v1, Ljava/lang/String;

    .line 197
    :cond_c
    sget-object p0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "type = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lorg/cocos2dx/javascript/ADChannelDef;->CSJ:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " desc = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " packagename:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " appname:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " companyname:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    sget-object p0, Lorg/cocos2dx/javascript/ADChannelDef;->CSJ:Ljava/lang/String;

    invoke-static {p0, v4, v2, v1, v5}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 204
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 202
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 200
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private static GetAdInfo_CSJ_3300(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 8

    .line 216
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u7a7f\u5c71\u75323300\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "b"

    .line 219
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "c"

    .line 221
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "l"

    .line 222
    invoke-static {p0, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    const-string v6, ""

    const-string v7, "ah"

    .line 227
    invoke-static {p0, v7}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    if-eqz p0, :cond_5

    .line 229
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "developer_name"

    .line 231
    invoke-virtual {v4, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    const-string p0, "developer_name"

    invoke-virtual {v4, p0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_2
    const-string p0, ""

    :goto_0
    const-string v5, "package_name"

    .line 232
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "package_name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_3
    const-string v5, ""

    :goto_1
    const-string v6, "app_name"

    .line 233
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "app_name"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_4
    const-string v4, ""

    :goto_2
    move-object v6, v4

    goto :goto_3

    :cond_5
    move-object p0, v4

    :goto_3
    if-eqz v1, :cond_6

    const-string v4, "a"

    .line 237
    invoke-static {v1, v4}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v4, ""

    .line 238
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    goto :goto_4

    :cond_6
    move-object v1, v3

    .line 242
    :goto_4
    sget-object v3, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "type = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lorg/cocos2dx/javascript/ADChannelDef;->CSJ:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " desc = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " packagename:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " appname:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " companyname:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    sget-object p0, Lorg/cocos2dx/javascript/ADChannelDef;->CSJ:Ljava/lang/String;

    invoke-static {p0, v5, v2, v6, v1}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 249
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_5

    :catch_1
    move-exception p0

    .line 247
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_5

    :catch_2
    move-exception p0

    .line 245
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_5
    return-object v0
.end method

.method public static GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 131
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT_43321202(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 133
    :cond_0
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a43321202\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT_42941164(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    .line 137
    :cond_1
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42941164\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT_42511121(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_2

    return-object v0

    .line 141
    :cond_2
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42511121\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT_42701140(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_3

    return-object p0

    .line 145
    :cond_3
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42701140\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method private static GetAdInfo_GDT_42511121(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 6

    .line 257
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42511121\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "c"

    .line 260
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "a"

    .line 262
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "u"

    .line 264
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 266
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 268
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "r"

    .line 270
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_6

    return-object v0

    :cond_6
    const-string v3, "b"

    .line 272
    invoke-static {p0, v1, v3}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    const-string v1, "a"

    .line 273
    invoke-static {v2, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "h"

    .line 274
    invoke-static {v2, v3}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 276
    sget-object v3, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v5, Lorg/cocos2dx/javascript/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " desc = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " packagename:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " appname:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    sget-object v3, Lorg/cocos2dx/javascript/ADChannelDef;->GDT:Ljava/lang/String;

    const-string v4, ""

    invoke-static {v3, v1, p0, v2, v4}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 283
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 281
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 279
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private static GetAdInfo_GDT_42701140(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 7

    .line 400
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42701140\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "c"

    .line 403
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "a"

    .line 405
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "u"

    .line 407
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 409
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 411
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "q"

    .line 413
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_6

    return-object v0

    :cond_6
    const-string v3, "c"

    .line 415
    invoke-static {p0, v1, v3}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    const-string v1, "a"

    .line 416
    invoke-static {v2, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "h"

    .line 417
    invoke-static {v2, v3}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, ""

    .line 419
    sget-object v4, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lorg/cocos2dx/javascript/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " desc = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " packagename:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " appname:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    sget-object v4, Lorg/cocos2dx/javascript/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-static {v4, v1, p0, v2, v3}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 426
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 424
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 422
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private static GetAdInfo_GDT_42941164(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 9

    .line 346
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a42941164\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "c"

    .line 349
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "a"

    .line 351
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "h"

    .line 353
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 355
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-nez v1, :cond_4

    return-object v0

    .line 356
    :cond_4
    invoke-static {p0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_5

    return-object v0

    :cond_5
    const/4 v1, 0x0

    .line 357
    invoke-static {p0, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_18

    const-string v1, ""

    .line 358
    invoke-virtual {p0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    goto/16 :goto_b

    .line 359
    :cond_6
    new-instance v1, Lorg/json/JSONObject;

    check-cast p0, Ljava/lang/String;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "desc"

    .line 361
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_7

    const-string p0, "desc"

    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_7
    move-object p0, v0

    :goto_0
    const-string v2, "txt"

    .line 362
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "txt"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_8
    move-object v2, v0

    :goto_1
    const-string v3, ""

    const-string v4, "corporate_logo"

    .line 364
    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "corporate_logo"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_9
    move-object v4, v0

    :goto_2
    const-string v5, "endcard_info"

    .line 365
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v5, "endcard_info"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    goto :goto_3

    :cond_a
    move-object v5, v0

    :goto_3
    if-eqz v5, :cond_10

    const-string v6, "appname"

    .line 367
    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    const-string v6, "appname"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    :cond_b
    move-object v6, v0

    :goto_4
    const-string v7, "desc"

    .line 368
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    const-string v7, "desc"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :cond_c
    move-object v7, v0

    :goto_5
    const-string v8, "img2"

    .line 369
    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    const-string v8, "img2"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_d
    move-object v5, v0

    :goto_6
    if-eqz v6, :cond_e

    const-string v8, ""

    .line 370
    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_e

    move-object v2, v6

    :cond_e
    if-eqz v7, :cond_f

    const-string v6, ""

    .line 371
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_f

    move-object p0, v7

    :cond_f
    if-eqz v5, :cond_10

    const-string v6, ""

    .line 372
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_10

    move-object v4, v5

    :cond_10
    const-string v5, "ext"

    .line 374
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_11

    const-string v5, "ext"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    goto :goto_7

    :cond_11
    move-object v1, v0

    :goto_7
    if-eqz v1, :cond_17

    const-string v5, "appname"

    .line 376
    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    const-string v5, "appname"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_8

    :cond_12
    move-object v5, v0

    :goto_8
    const-string v6, "packagename"

    .line 377
    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_13

    const-string v6, "packagename"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_9

    :cond_13
    move-object v6, v0

    :goto_9
    const-string v7, "pkg_name"

    .line 378
    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_14

    const-string v7, "pkg_name"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_a

    :cond_14
    move-object v1, v0

    :goto_a
    if-eqz v5, :cond_15

    const-string v7, ""

    .line 379
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_15

    move-object v2, v5

    :cond_15
    if-eqz v6, :cond_16

    const-string v5, ""

    .line 380
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_16

    move-object v3, v6

    :cond_16
    if-nez v3, :cond_17

    if-eqz v1, :cond_17

    const-string v5, ""

    .line 382
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_17

    move-object v3, v1

    .line 385
    :cond_17
    sget-object v1, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lorg/cocos2dx/javascript/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " desc = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " packagename:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " appname:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    sget-object v1, Lorg/cocos2dx/javascript/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-static {v1, v3, p0, v2, v4}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :cond_18
    :goto_b
    return-object v0

    :catch_0
    move-exception p0

    .line 392
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_c

    :catch_1
    move-exception p0

    .line 390
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_c

    :catch_2
    move-exception p0

    .line 388
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_c
    return-object v0
.end method

.method private static GetAdInfo_GDT_43321202(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 10

    .line 291
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5e7f\u70b9\u901a43321202\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "c"

    .line 294
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "a"

    .line 296
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "g"

    .line 298
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 300
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 302
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "E"

    .line 304
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/json/JSONObject;

    if-nez p0, :cond_6

    return-object v0

    :cond_6
    const-string v1, "desc"

    .line 306
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "desc"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_7
    const-string v1, ""

    :goto_0
    const-string v2, "txt"

    .line 307
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "txt"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_8
    const-string v2, ""

    :goto_1
    const-string v3, "corporation_name"

    .line 308
    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "corporation_name"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_9
    const-string v3, ""

    :goto_2
    const-string v4, ""

    const-string v5, "corporate_logo"

    .line 310
    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v5, "corporate_logo"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_a
    move-object v5, v0

    :goto_3
    const-string v6, "endcard_info"

    .line 311
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    const-string v6, "endcard_info"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    goto :goto_4

    :cond_b
    move-object v6, v0

    :goto_4
    if-eqz v6, :cond_11

    const-string v7, "appname"

    .line 313
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    const-string v7, "appname"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :cond_c
    move-object v7, v0

    :goto_5
    const-string v8, "desc"

    .line 314
    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    const-string v8, "desc"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_6

    :cond_d
    move-object v8, v0

    :goto_6
    const-string v9, "img2"

    .line 315
    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    const-string v9, "img2"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_7

    :cond_e
    move-object v6, v0

    :goto_7
    if-eqz v7, :cond_f

    const-string v9, ""

    .line 316
    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_f

    move-object v2, v7

    :cond_f
    if-eqz v8, :cond_10

    const-string v7, ""

    .line 317
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_10

    move-object v1, v8

    :cond_10
    if-eqz v6, :cond_11

    const-string v7, ""

    .line 318
    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    move-object v5, v6

    :cond_11
    const-string v6, "ext"

    .line 320
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_12

    const-string v6, "ext"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    goto :goto_8

    :cond_12
    move-object p0, v0

    :goto_8
    if-eqz p0, :cond_18

    const-string v6, "appname"

    .line 322
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_13

    const-string v6, "appname"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_9

    :cond_13
    move-object v6, v0

    :goto_9
    const-string v7, "packagename"

    .line 323
    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_14

    const-string v7, "packagename"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_a

    :cond_14
    move-object v7, v0

    :goto_a
    const-string v8, "pkg_name"

    .line 324
    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_15

    const-string v8, "pkg_name"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_b

    :cond_15
    move-object p0, v0

    :goto_b
    if-eqz v6, :cond_16

    const-string v8, ""

    .line 325
    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_16

    move-object v2, v6

    :cond_16
    if-eqz v7, :cond_17

    const-string v6, ""

    .line 326
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_17

    move-object v4, v7

    :cond_17
    if-nez v4, :cond_18

    if-eqz p0, :cond_18

    const-string v6, ""

    .line 328
    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_18

    move-object v4, p0

    .line 331
    :cond_18
    sget-object p0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "type = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lorg/cocos2dx/javascript/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " desc = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " packagename:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " appname:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " companyname:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    sget-object p0, Lorg/cocos2dx/javascript/ADChannelDef;->GDT:Ljava/lang/String;

    invoke-static {p0, v4, v1, v2, v5}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 338
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_c

    :catch_1
    move-exception p0

    .line 336
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_c

    :catch_2
    move-exception p0

    .line 334
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_c
    return-object v0
.end method

.method public static GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 159
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_KS_330(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_0

    return-object p0

    .line 161
    :cond_0
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5feb\u624b330\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method private static GetAdInfo_KS_330(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 7

    .line 438
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "\u5feb\u624b330\u7248\u672c\u5f00\u59cb\u89e3\u6790"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "b"

    .line 441
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    const-string v1, "adBaseInfo"

    .line 443
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "adDescription"

    .line 445
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "appPackageName"

    .line 446
    invoke-static {p0, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "appName"

    .line 447
    invoke-static {p0, v3}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "appIconUrl"

    .line 448
    invoke-static {p0, v4}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v3, :cond_3

    const-string v5, ""

    .line 449
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    const-string v3, "productName"

    .line 450
    invoke-static {p0, v3}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    move-object v3, p0

    check-cast v3, Ljava/lang/String;

    .line 452
    :cond_4
    sget-object p0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Lorg/cocos2dx/javascript/ADChannelDef;->KS:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " desc = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " packagename:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " appname:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    sget-object p0, Lorg/cocos2dx/javascript/ADChannelDef;->KS:Ljava/lang/String;

    invoke-static {p0, v2, v1, v3, v4}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 459
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 457
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 455
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static GetAdInfo_MOBRAIN(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 64
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_MOBRAIN_2600(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 66
    :cond_0
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Mobrain2600\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_MOBRAIN_2500(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    .line 70
    :cond_1
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Mobrain2500\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_MOBRAIN_2411(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_2

    return-object p0

    .line 74
    :cond_2
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Mobrain2411\u5e7f\u544a\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method public static GetAdInfo_MOBRAIN_2411(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getAdNetworkPlatformId"

    const/4 v2, 0x0

    .line 578
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_d

    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    goto/16 :goto_0

    :cond_1
    const-string v1, "a"

    .line 612
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    .line 614
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_3

    return-object v0

    .line 616
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 618
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "B"

    .line 620
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    :cond_6
    const-string v1, "b"

    .line 622
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 623
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "a"

    .line 582
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    .line 584
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_9

    return-object v0

    .line 586
    :cond_9
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_a

    return-object v0

    .line 588
    :cond_a
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_b

    return-object v0

    :cond_b
    const-string v2, "B"

    .line 590
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_c

    return-object v0

    :cond_c
    const-string v1, "b"

    .line 592
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 593
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_d
    const-string v1, "a"

    .line 597
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_e

    return-object v0

    .line 599
    :cond_e
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_f

    return-object v0

    .line 601
    :cond_f
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_10

    return-object v0

    .line 603
    :cond_10
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_11

    return-object v0

    :cond_11
    const-string v2, "B"

    .line 605
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_12

    return-object v0

    :cond_12
    const-string v1, "b"

    .line 607
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 608
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 635
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 633
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 631
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 629
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static GetAdInfo_MOBRAIN_2500(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getAdNetworkPlatformId"

    const/4 v2, 0x0

    .line 660
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_d

    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    goto/16 :goto_0

    :cond_1
    const-string v1, "a"

    .line 694
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    .line 696
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_3

    return-object v0

    .line 698
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 700
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "C"

    .line 702
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    :cond_6
    const-string v1, "b"

    .line 704
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 705
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "a"

    .line 664
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    .line 666
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_9

    return-object v0

    .line 668
    :cond_9
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_a

    return-object v0

    .line 670
    :cond_a
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_b

    return-object v0

    :cond_b
    const-string v2, "C"

    .line 672
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_c

    return-object v0

    :cond_c
    const-string v1, "b"

    .line 674
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 675
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_d
    const-string v1, "a"

    .line 679
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_e

    return-object v0

    .line 681
    :cond_e
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_f

    return-object v0

    .line 683
    :cond_f
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_10

    return-object v0

    .line 685
    :cond_10
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_11

    return-object v0

    :cond_11
    const-string v2, "C"

    .line 687
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_12

    return-object v0

    :cond_12
    const-string v1, "a"

    .line 689
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 690
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 717
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 715
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 713
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 711
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static GetAdInfo_MOBRAIN_2600(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getAdNetworkPlatformId"

    const/4 v2, 0x0

    .line 742
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_d

    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    goto/16 :goto_0

    :cond_1
    const-string v1, "a"

    .line 776
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    .line 778
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_3

    return-object v0

    .line 780
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v0

    .line 782
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_5

    return-object v0

    :cond_5
    const-string v2, "C"

    .line 784
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    :cond_6
    const-string v1, "a"

    .line 786
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 787
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "a"

    .line 746
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    .line 748
    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_9

    return-object v0

    .line 750
    :cond_9
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_a

    return-object v0

    .line 752
    :cond_a
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_b

    return-object v0

    :cond_b
    const-string v2, "C"

    .line 754
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_c

    return-object v0

    :cond_c
    const-string v1, "a"

    .line 756
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 757
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_d
    const-string v1, "a"

    .line 761
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_e

    return-object v0

    .line 763
    :cond_e
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_f

    return-object v0

    .line 765
    :cond_f
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_10

    return-object v0

    .line 767
    :cond_10
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_11

    return-object v0

    :cond_11
    const-string v2, "C"

    .line 769
    invoke-static {p0, v1, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_12

    return-object v0

    :cond_12
    const-string v1, "a"

    .line 771
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 772
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 799
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 797
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 795
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 793
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static GetAdInfo_TOPON(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 89
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_TOPON_5711(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 91
    :cond_0
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Topon5711\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_TOPON_571(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    if-eqz p0, :cond_1

    return-object p0

    .line 95
    :cond_1
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string v1, "Topon571\u7248\u672c\u89e3\u6790\u5931\u8d25"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method private static GetAdInfo_TOPON_571(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getNetworkFirmId"

    const/4 v2, 0x0

    .line 517
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_7

    const/16 v2, 0xf

    if-eq v1, v2, :cond_4

    const/16 v2, 0x1c

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "mBaseAdapter"

    .line 537
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "b"

    .line 539
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 541
    :cond_3
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_4
    const-string v1, "mBaseAdapter"

    .line 529
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_5

    return-object v0

    :cond_5
    const-string v1, "e"

    .line 531
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    .line 533
    :cond_6
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "mBaseAdapter"

    .line 521
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    :cond_8
    const-string v1, "a"

    .line 523
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_9

    return-object v0

    .line 525
    :cond_9
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 553
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 551
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 549
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 547
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method private static GetAdInfo_TOPON_5711(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "getNetworkFirmId"

    const/4 v2, 0x0

    .line 470
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v0, v2}, Lorg/cocos2dx/javascript/ReflectionUtils;->invokePrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_7

    const/16 v2, 0xf

    if-eq v1, v2, :cond_4

    const/16 v2, 0x1c

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "mBaseAdapter"

    .line 490
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v0

    :cond_2
    const-string v1, "b"

    .line 492
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_3

    return-object v0

    .line 494
    :cond_3
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_KS(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_4
    const-string v1, "mBaseAdapter"

    .line 482
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_5

    return-object v0

    :cond_5
    const-string v1, "f"

    .line 484
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_6

    return-object v0

    .line 486
    :cond_6
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_CSJ(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0

    return-object p0

    :cond_7
    const-string v1, "mBaseAdapter"

    .line 474
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_8

    return-object v0

    :cond_8
    const-string v1, "a"

    .line 476
    invoke-static {p0, v1}, Lorg/cocos2dx/javascript/ReflectionUtils;->getPrivateField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_9

    return-object v0

    .line 478
    :cond_9
    invoke-static {p0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_GDT(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 506
    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p0

    .line 504
    invoke-virtual {p0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p0

    .line 502
    invoke-virtual {p0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception p0

    .line 500
    invoke-virtual {p0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public static Report(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Z)V
    .locals 3

    .line 815
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GAMEID:Ljava/lang/String;

    if-eqz v0, :cond_8

    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->USERID:Ljava/lang/String;

    if-nez v0, :cond_0

    goto/16 :goto_7

    :cond_0
    if-eqz p2, :cond_7

    const-string v0, "packagename"

    .line 819
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "packagename"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_6

    .line 825
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/zhy/http/okhttp/OkHttpUtils;->get()Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object v0

    const-string v1, "https://xyxcck-log-ad.raink.com.cn/MiniGameLog/log/adContentCensus.action"

    .line 826
    invoke-virtual {v0, v1}, Lcom/zhy/http/okhttp/builder/GetBuilder;->url(Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/OkHttpRequestBuilder;

    move-result-object v0

    check-cast v0, Lcom/zhy/http/okhttp/builder/GetBuilder;

    const-string v1, "gameid"

    sget-object v2, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GAMEID:Ljava/lang/String;

    .line 827
    invoke-virtual {v0, v1, v2}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object v0

    const-string v1, "userid"

    sget-object v2, Lorg/cocos2dx/javascript/ADInfoGetUtils;->USERID:Ljava/lang/String;

    .line 828
    invoke-virtual {v0, v1, v2}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object v0

    const-string v1, "platform"

    const-string v2, "platform"

    .line 829
    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "platform"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    const-string v2, ""

    :goto_0
    invoke-virtual {v0, v1, v2}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object v0

    const-string v1, "appname"

    const-string v2, "appname"

    .line 830
    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "appname"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    const-string v2, ""

    :goto_1
    invoke-virtual {v0, v1, v2}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object v0

    const-string v1, "mainid"

    .line 831
    invoke-virtual {v0, v1, p0}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object p0

    const-string v0, "subid"

    .line 832
    invoke-virtual {p0, v0, p1}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object p0

    const-string p1, "packagename"

    const-string v0, "packagename"

    .line 833
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "packagename"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    const-string v0, ""

    :goto_2
    invoke-virtual {p0, p1, v0}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object p0

    const-string p1, "desc"

    const-string v0, "desc"

    .line 834
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "desc"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_3

    :cond_5
    const-string p2, ""

    :goto_3
    invoke-virtual {p0, p1, p2}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object p0

    const-string p1, "companyname"

    const-string p2, ""

    .line 835
    invoke-virtual {p0, p1, p2}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object p0

    const-string p1, "isclickad"

    if-eqz p3, :cond_6

    const-string p2, "1"

    goto :goto_4

    :cond_6
    const-string p2, "0"

    .line 836
    :goto_4
    invoke-virtual {p0, p1, p2}, Lcom/zhy/http/okhttp/builder/GetBuilder;->addParams(Ljava/lang/String;Ljava/lang/String;)Lcom/zhy/http/okhttp/builder/GetBuilder;

    move-result-object p0

    .line 837
    invoke-virtual {p0}, Lcom/zhy/http/okhttp/builder/GetBuilder;->build()Lcom/zhy/http/okhttp/request/RequestCall;

    move-result-object p0

    new-instance p1, Lorg/cocos2dx/javascript/ADInfoGetUtils$1;

    invoke-direct {p1}, Lorg/cocos2dx/javascript/ADInfoGetUtils$1;-><init>()V

    .line 838
    invoke-virtual {p0, p1}, Lcom/zhy/http/okhttp/request/RequestCall;->execute(Lcom/zhy/http/okhttp/callback/Callback;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception p0

    .line 850
    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    :goto_5
    return-void

    .line 820
    :cond_7
    :goto_6
    sget-object p0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string p1, "\u5e94\u7528\u4fe1\u606f\u4e0d\u5168,\u65e0\u6cd5\u63d0\u4ea4"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 816
    :cond_8
    :goto_7
    sget-object p0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    const-string p1, "\u8bf7\u5148\u8d4b\u503c\u53d8\u91cf\u540e\u518d\u4e0a\u62a5"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 16
    sget-object v0, Lorg/cocos2dx/javascript/ADInfoGetUtils;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static creatPkgInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 45
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "packagename"

    .line 46
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "desc"

    .line 47
    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "appname"

    .line 48
    invoke-virtual {v0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "platform"

    .line 49
    invoke-virtual {v0, p1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p0, "appicon"

    .line 50
    invoke-virtual {v0, p0, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0
.end method
