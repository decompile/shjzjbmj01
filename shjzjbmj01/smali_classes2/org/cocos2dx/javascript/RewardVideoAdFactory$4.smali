.class final Lorg/cocos2dx/javascript/RewardVideoAdFactory$4;
.super Ljava/lang/Object;
.source "RewardVideoAdFactory.java"

# interfaces
.implements Lcom/bytedance/msdk/api/reward/TTRewardedAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/cocos2dx/javascript/RewardVideoAdFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRewardClick()V
    .locals 2

    const/4 v0, 0x1

    .line 285
    invoke-static {v0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$602(Z)Z

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRewardClick(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 287
    invoke-static {v0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    const-string v0, "cocos video "

    const-string v1, "onRewardClick"

    .line 288
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRewardVerify(Lcom/bytedance/msdk/api/reward/RewardItem;)V
    .locals 1

    .line 349
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onRewardVerify(\'"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\')"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 350
    invoke-static {p1}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    const-string p1, "cocos video "

    const-string v0, "onRewardVerify"

    .line 351
    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRewardedAdClosed()V
    .locals 11

    const-string v0, "cocos video "

    const-string v1, "onRewardedAdClosed"

    .line 292
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$200()Ljava/util/Map;

    move-result-object v0

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/msdk/api/reward/TTRewardAd;

    .line 294
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/reward/TTRewardAd;->getAdNetworkRitId()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    .line 300
    :try_start_0
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$900()Lorg/json/JSONObject;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 301
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$900()Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "packagename"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    .line 302
    :try_start_1
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$900()Lorg/json/JSONObject;

    move-result-object v1

    const-string v5, "appname"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 303
    :try_start_2
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$900()Lorg/json/JSONObject;

    move-result-object v2

    const-string v5, "desc"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 304
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$900()Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "platform"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 305
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$900()Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "appicon"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 306
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v7

    .line 307
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "desc___"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",platform___"

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",appicon___"

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ",showingId___"

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v2

    move-object v2, v1

    move-object v1, v4

    goto :goto_1

    :catch_0
    move-exception v2

    move-object v10, v4

    move-object v4, v1

    move-object v1, v10

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v10, v2

    move-object v2, v1

    move-object v1, v4

    move-object v4, v10

    goto :goto_0

    :catch_2
    move-exception v4

    move-object v10, v4

    move-object v4, v2

    move-object v2, v10

    .line 310
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    move-object v2, v4

    .line 313
    :cond_0
    :goto_1
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$600()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 314
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendRewardVerify(true,true,\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\',\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\',\'"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\',\'"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\')"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 315
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$300()Lorg/cocos2dx/javascript/AppActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/cocos2dx/javascript/AppActivity;->setAdOnCloseInfo(Ljava/lang/String;)V

    goto :goto_2

    .line 317
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendRewardVerify(true,false,\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\',\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\',\'"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\',\'"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\')"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 318
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$300()Lorg/cocos2dx/javascript/AppActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/cocos2dx/javascript/AppActivity;->setAdOnCloseInfo(Ljava/lang/String;)V

    .line 320
    :goto_2
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$1000(Ljava/lang/String;I)V

    .line 322
    invoke-static {}, Lorg/cocos2dx/javascript/App;->reportTransformWatchVideo()V

    .line 324
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$900()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$600()Z

    move-result v3

    invoke-static {v1, v0, v2, v3}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->Report(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Z)V

    const/4 v0, 0x0

    .line 325
    invoke-static {v0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$902(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    return-void
.end method

.method public onRewardedAdShow()V
    .locals 4

    const-string v0, "cocos video "

    const-string v1, "onRewardedAdShow"

    .line 268
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->loadOtherAd(Ljava/lang/String;)V

    .line 271
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$200()Ljava/util/Map;

    move-result-object v0

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/msdk/api/reward/TTRewardAd;

    .line 272
    invoke-static {v0}, Lorg/cocos2dx/javascript/ADInfoGetUtils;->GetAdInfo_MOBRAIN(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$902(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 273
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/reward/TTRewardAd;->getAdNetworkRitId()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    .line 275
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$900()Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 276
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->parseAdInfoToString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cocos video "

    const-string v3, "parseSucc"

    .line 277
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "videoOnShow(\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\',\'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\')"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 280
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$300()Lorg/cocos2dx/javascript/AppActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/cocos2dx/javascript/AppActivity;->setAdOnShowInfo(Ljava/lang/String;)V

    return-void
.end method

.method public onSkippedVideo()V
    .locals 2

    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSkippedVideo(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 357
    invoke-static {v0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    const-string v0, "cocos video "

    const-string v1, "onSkippedVideo"

    .line 358
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoComplete()V
    .locals 2

    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onVideoComplete(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 330
    invoke-static {v0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    const-string v0, "cocos video "

    const-string v1, "onVideoComplete"

    .line 331
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onVideoError()V
    .locals 3

    const-string v0, "cocos video "

    const-string v1, "onVideoError"

    .line 336
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onVideoError(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 338
    invoke-static {v0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    .line 340
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$200()Ljava/util/Map;

    move-result-object v0

    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bytedance/msdk/api/reward/TTRewardAd;

    .line 341
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/reward/TTRewardAd;->getAdNetworkRitId()Ljava/lang/String;

    move-result-object v0

    .line 342
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendRewardVerify(true,false,\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\')"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 343
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$300()Lorg/cocos2dx/javascript/AppActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/cocos2dx/javascript/AppActivity;->setAdOnCloseInfo(Ljava/lang/String;)V

    .line 344
    invoke-static {}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$500()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/cocos2dx/javascript/RewardVideoAdFactory;->access$1000(Ljava/lang/String;I)V

    return-void
.end method
