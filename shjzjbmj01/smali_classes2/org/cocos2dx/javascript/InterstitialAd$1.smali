.class Lorg/cocos2dx/javascript/InterstitialAd$1;
.super Ljava/lang/Object;
.source "InterstitialAd.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/cocos2dx/javascript/InterstitialAd;->show()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/cocos2dx/javascript/InterstitialAd;


# direct methods
.method constructor <init>(Lorg/cocos2dx/javascript/InterstitialAd;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lorg/cocos2dx/javascript/InterstitialAd$1;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 56
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$1;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v0}, Lorg/cocos2dx/javascript/InterstitialAd;->access$000(Lorg/cocos2dx/javascript/InterstitialAd;)Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    move-result-object v0

    iget-object v1, p0, Lorg/cocos2dx/javascript/InterstitialAd$1;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    iget-object v1, v1, Lorg/cocos2dx/javascript/InterstitialAd;->interstitialListener:Lcom/bytedance/msdk/api/interstitial/TTInterstitialAdListener;

    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;->setTTAdInterstitialListener(Lcom/bytedance/msdk/api/interstitial/TTInterstitialAdListener;)V

    .line 57
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$1;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v0}, Lorg/cocos2dx/javascript/InterstitialAd;->access$000(Lorg/cocos2dx/javascript/InterstitialAd;)Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    move-result-object v0

    iget-object v1, p0, Lorg/cocos2dx/javascript/InterstitialAd$1;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v1}, Lorg/cocos2dx/javascript/InterstitialAd;->access$100(Lorg/cocos2dx/javascript/InterstitialAd;)Lorg/cocos2dx/javascript/AppActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;->showAd(Landroid/app/Activity;)V

    const-string v0, "cocos insert "

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "adNetworkPlatformId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/cocos2dx/javascript/InterstitialAd$1;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v2}, Lorg/cocos2dx/javascript/InterstitialAd;->access$000(Lorg/cocos2dx/javascript/InterstitialAd;)Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;->getAdNetworkPlatformId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "   adNetworkRitId\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/cocos2dx/javascript/InterstitialAd$1;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v2}, Lorg/cocos2dx/javascript/InterstitialAd;->access$000(Lorg/cocos2dx/javascript/InterstitialAd;)Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;->getAdNetworkRitId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "   preEcpm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/cocos2dx/javascript/InterstitialAd$1;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v2}, Lorg/cocos2dx/javascript/InterstitialAd;->access$000(Lorg/cocos2dx/javascript/InterstitialAd;)Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;->getPreEcpm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/bytedance/msdk/adapter/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
