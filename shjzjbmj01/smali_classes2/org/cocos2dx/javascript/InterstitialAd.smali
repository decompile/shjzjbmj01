.class public Lorg/cocos2dx/javascript/InterstitialAd;
.super Ljava/lang/Object;
.source "InterstitialAd.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "cocos insert "


# instance fields
.field private appActivity:Lorg/cocos2dx/javascript/AppActivity;

.field private continueLoadFailTimes:I

.field private continueShowFailTimes:I

.field private handler:Landroid/os/Handler;

.field interstitialListener:Lcom/bytedance/msdk/api/interstitial/TTInterstitialAdListener;

.field private isLoadSuccess:Z

.field private mAdUnitId:Ljava/lang/String;

.field private mInterstitialAd:Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

.field private mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;


# direct methods
.method constructor <init>(Lorg/cocos2dx/javascript/AppActivity;)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->handler:Landroid/os/Handler;

    const-string v0, "945705413"

    .line 23
    iput-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mAdUnitId:Ljava/lang/String;

    const/4 v0, 0x0

    .line 24
    iput v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueLoadFailTimes:I

    .line 25
    iput v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueShowFailTimes:I

    .line 66
    new-instance v0, Lorg/cocos2dx/javascript/InterstitialAd$2;

    invoke-direct {v0, p0}, Lorg/cocos2dx/javascript/InterstitialAd$2;-><init>(Lorg/cocos2dx/javascript/InterstitialAd;)V

    iput-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;

    .line 100
    new-instance v0, Lorg/cocos2dx/javascript/InterstitialAd$4;

    invoke-direct {v0, p0}, Lorg/cocos2dx/javascript/InterstitialAd$4;-><init>(Lorg/cocos2dx/javascript/InterstitialAd;)V

    iput-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->interstitialListener:Lcom/bytedance/msdk/api/interstitial/TTInterstitialAdListener;

    .line 28
    iput-object p1, p0, Lorg/cocos2dx/javascript/InterstitialAd;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    return-void
.end method

.method static synthetic access$000(Lorg/cocos2dx/javascript/InterstitialAd;)Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;
    .locals 0

    .line 17
    iget-object p0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mInterstitialAd:Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    return-object p0
.end method

.method static synthetic access$100(Lorg/cocos2dx/javascript/InterstitialAd;)Lorg/cocos2dx/javascript/AppActivity;
    .locals 0

    .line 17
    iget-object p0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    return-object p0
.end method

.method static synthetic access$200(Lorg/cocos2dx/javascript/InterstitialAd;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lorg/cocos2dx/javascript/InterstitialAd;->loadInteractionAd()V

    return-void
.end method

.method static synthetic access$302(Lorg/cocos2dx/javascript/InterstitialAd;Z)Z
    .locals 0

    .line 17
    iput-boolean p1, p0, Lorg/cocos2dx/javascript/InterstitialAd;->isLoadSuccess:Z

    return p1
.end method

.method static synthetic access$402(Lorg/cocos2dx/javascript/InterstitialAd;I)I
    .locals 0

    .line 17
    iput p1, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueLoadFailTimes:I

    return p1
.end method

.method private loadInteractionAd()V
    .locals 3

    .line 78
    new-instance v0, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    iget-object v1, p0, Lorg/cocos2dx/javascript/InterstitialAd;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    iget-object v2, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mAdUnitId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mInterstitialAd:Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    .line 79
    new-instance v0, Lcom/bytedance/msdk/api/AdSlot$Builder;

    invoke-direct {v0}, Lcom/bytedance/msdk/api/AdSlot$Builder;-><init>()V

    const/4 v1, 0x1

    .line 80
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setAdStyleType(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v0

    const/16 v1, 0x258

    const/16 v2, 0x190

    .line 81
    invoke-virtual {v0, v1, v2}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setImageAdSize(II)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot$Builder;->build()Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mInterstitialAd:Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    new-instance v2, Lorg/cocos2dx/javascript/InterstitialAd$3;

    invoke-direct {v2, p0}, Lorg/cocos2dx/javascript/InterstitialAd$3;-><init>(Lorg/cocos2dx/javascript/InterstitialAd;)V

    invoke-virtual {v1, v0, v2}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;->loadAd(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/interstitial/TTInterstitialAdLoadCallback;)V

    return-void
.end method


# virtual methods
.method public continueLoadFailTimesPlus()V
    .locals 2

    .line 136
    iget v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueLoadFailTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueLoadFailTimes:I

    .line 137
    iget v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueLoadFailTimes:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 138
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/InterstitialAd;->load()V

    :cond_0
    return-void
.end method

.method public continueShowFailTimesPlus()V
    .locals 2

    .line 144
    iget v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueShowFailTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueShowFailTimes:I

    .line 145
    iget v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueShowFailTimes:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 146
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/InterstitialAd;->load()V

    :cond_0
    return-void
.end method

.method public isReady()Z
    .locals 1

    .line 131
    iget-boolean v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->isLoadSuccess:Z

    return v0
.end method

.method public load()V
    .locals 2

    .line 39
    invoke-static {}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->configLoadSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cocos insert "

    const-string v1, "load ad \u5f53\u524dconfig\u914d\u7f6e\u5b58\u5728\uff0c\u76f4\u63a5\u52a0\u8f7d\u5e7f\u544a"

    .line 40
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-direct {p0}, Lorg/cocos2dx/javascript/InterstitialAd;->loadInteractionAd()V

    goto :goto_0

    :cond_0
    const-string v0, "cocos insert "

    const-string v1, "load ad \u5f53\u524dconfig\u914d\u7f6e\u4e0d\u5b58\u5728\uff0c\u6b63\u5728\u8bf7\u6c42config\u914d\u7f6e...."

    .line 43
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;

    invoke-static {v0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->registerConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V

    :goto_0
    return-void
.end method

.method public setAdId(Ljava/lang/String;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mAdUnitId:Ljava/lang/String;

    .line 34
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/InterstitialAd;->load()V

    return-void
.end method

.method public show()V
    .locals 2

    const/4 v0, 0x0

    .line 50
    iput v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->continueShowFailTimes:I

    .line 51
    iget-boolean v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->isLoadSuccess:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mInterstitialAd:Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->mInterstitialAd:Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd;->handler:Landroid/os/Handler;

    new-instance v1, Lorg/cocos2dx/javascript/InterstitialAd$1;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/InterstitialAd$1;-><init>(Lorg/cocos2dx/javascript/InterstitialAd;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/InterstitialAd;->continueShowFailTimesPlus()V

    :goto_0
    return-void
.end method
