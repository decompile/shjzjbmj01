.class Lorg/cocos2dx/javascript/NativeExAd$5;
.super Ljava/lang/Object;
.source "NativeExAd.java"

# interfaces
.implements Lcom/bytedance/msdk/api/TTDislikeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/cocos2dx/javascript/NativeExAd;->bindView(Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/cocos2dx/javascript/NativeExAd;


# direct methods
.method constructor <init>(Lorg/cocos2dx/javascript/NativeExAd;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lorg/cocos2dx/javascript/NativeExAd$5;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    const-string v0, "cocos neAd "

    const-string v1, "dislike \u70b9\u51fb\u4e86\u53d6\u6d88"

    .line 156
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRefuse()V
    .locals 2

    const-string v0, "cocos neAd "

    const-string v1, "onRefuse"

    .line 160
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onSelected(ILjava/lang/String;)V
    .locals 2

    const-string p1, "cocos neAd "

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u70b9\u51fb "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object p1, p0, Lorg/cocos2dx/javascript/NativeExAd$5;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-virtual {p1}, Lorg/cocos2dx/javascript/NativeExAd;->hide()V

    return-void
.end method
