.class Lorg/cocos2dx/javascript/NetUtils$TrustAllCerts;
.super Ljava/lang/Object;
.source "NetUtils.java"

# interfaces
.implements Ljavax/net/ssl/X509TrustManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/cocos2dx/javascript/NetUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TrustAllCerts"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/cocos2dx/javascript/NetUtils;


# direct methods
.method constructor <init>(Lorg/cocos2dx/javascript/NetUtils;)V
    .locals 0

    .line 213
    iput-object p1, p0, Lorg/cocos2dx/javascript/NetUtils$TrustAllCerts;->this$0:Lorg/cocos2dx/javascript/NetUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    return-void
.end method

.method public checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    return-void
.end method

.method public getAcceptedIssuers()[Ljava/security/cert/X509Certificate;
    .locals 1

    const/4 v0, 0x0

    .line 223
    new-array v0, v0, [Ljava/security/cert/X509Certificate;

    return-object v0
.end method
