.class public Lorg/cocos2dx/javascript/VideoOptionUtil;
.super Ljava/lang/Object;
.source "VideoOptionUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTTVideoOption()Lcom/bytedance/msdk/api/TTVideoOption;
    .locals 5

    .line 10
    new-instance v0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    invoke-direct {v0}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;-><init>()V

    const/4 v1, 0x1

    .line 11
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTAutoPlayMuted(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    const/4 v2, 0x0

    .line 12
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTDetailPageMuted(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 13
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTEnableDetailPage(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 14
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTEnableUserControl(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 15
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTMaxVideoDuration(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 16
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTMinVideoDuration(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 21
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setAutoPlayPolicy(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 22
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setDownAPPConfirmPolicy(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 23
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setBrowserType(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->build()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object v0

    .line 27
    new-instance v3, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;

    invoke-direct {v3}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;-><init>()V

    const/4 v4, 0x2

    .line 28
    invoke-virtual {v3, v4}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->setGDTExtraOption(I)Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;

    move-result-object v3

    .line 29
    invoke-virtual {v3, v1}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->setCacheVideoOnlyWifi(Z)Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->build()Lcom/bytedance/msdk/api/BaiduExtraOptions;

    move-result-object v1

    .line 32
    new-instance v3, Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    invoke-direct {v3}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;-><init>()V

    .line 33
    invoke-virtual {v3, v2}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setMuted(Z)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    .line 34
    invoke-virtual {v2, v3}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setAdmobAppVolume(F)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v2

    .line 35
    invoke-virtual {v2, v0}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setGDTExtraOption(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v0

    .line 36
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setBaiduExtraOption(Lcom/bytedance/msdk/api/BaiduExtraOptions;)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->build()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v0

    return-object v0
.end method

.method public static getTTVideoOption2()Lcom/bytedance/msdk/api/TTVideoOption;
    .locals 5

    .line 44
    new-instance v0, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    invoke-direct {v0}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;-><init>()V

    const/4 v1, 0x1

    .line 45
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTAutoPlayMuted(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    const/4 v2, 0x0

    .line 46
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTDetailPageMuted(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTEnableDetailPage(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTEnableUserControl(Z)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTMaxVideoDuration(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setGDTMinVideoDuration(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    const/4 v3, 0x2

    .line 51
    invoke-virtual {v0, v3}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setFeedExpressType(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setAutoPlayPolicy(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setDownAPPConfirmPolicy(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v2}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->setBrowserType(I)Lcom/bytedance/msdk/api/GDTExtraOption$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/GDTExtraOption$Builder;->build()Lcom/bytedance/msdk/api/GDTExtraOption;

    move-result-object v0

    .line 62
    new-instance v4, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;

    invoke-direct {v4}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;-><init>()V

    .line 63
    invoke-virtual {v4, v3}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->setGDTExtraOption(I)Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;

    move-result-object v3

    .line 64
    invoke-virtual {v3, v1}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->setCacheVideoOnlyWifi(Z)Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/bytedance/msdk/api/BaiduExtraOptions$Builder;->build()Lcom/bytedance/msdk/api/BaiduExtraOptions;

    move-result-object v1

    .line 67
    new-instance v3, Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    invoke-direct {v3}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;-><init>()V

    .line 68
    invoke-virtual {v3, v2}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setMuted(Z)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    .line 69
    invoke-virtual {v2, v3}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setAdmobAppVolume(F)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v2

    .line 70
    invoke-virtual {v2, v0}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setGDTExtraOption(Lcom/bytedance/msdk/api/GDTExtraOption;)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->setBaiduExtraOption(Lcom/bytedance/msdk/api/BaiduExtraOptions;)Lcom/bytedance/msdk/api/TTVideoOption$Builder;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/TTVideoOption$Builder;->build()Lcom/bytedance/msdk/api/TTVideoOption;

    move-result-object v0

    return-object v0
.end method
