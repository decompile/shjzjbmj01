.class Lorg/cocos2dx/javascript/NativeExAd$6;
.super Ljava/lang/Object;
.source "NativeExAd.java"

# interfaces
.implements Lcom/bytedance/msdk/api/nativeAd/TTNativeExpressAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/cocos2dx/javascript/NativeExAd;->bindView(Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/cocos2dx/javascript/NativeExAd;

.field final synthetic val$ad:Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;


# direct methods
.method constructor <init>(Lorg/cocos2dx/javascript/NativeExAd;Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;)V
    .locals 0

    .line 165
    iput-object p1, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    iput-object p2, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->val$ad:Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClick()V
    .locals 2

    const-string v0, "cocos neAd "

    const-string v1, "\u6a21\u677f\u5e7f\u544a\u88ab\u70b9\u51fb"

    .line 168
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-static {v0}, Lorg/cocos2dx/javascript/NativeExAd;->access$400(Lorg/cocos2dx/javascript/NativeExAd;)Lorg/cocos2dx/javascript/AppActivity;

    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-static {v0}, Lorg/cocos2dx/javascript/NativeExAd;->access$300(Lorg/cocos2dx/javascript/NativeExAd;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->sendNativeAdFaultClick(Ljava/lang/String;)V

    return-void
.end method

.method public onAdShow()V
    .locals 2

    const-string v0, "cocos neAd "

    const-string v1, "\u6a21\u677f\u5e7f\u544ashow"

    .line 173
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-static {v0}, Lorg/cocos2dx/javascript/NativeExAd;->access$300(Lorg/cocos2dx/javascript/NativeExAd;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->sendNativeAdShow(Ljava/lang/String;)V

    return-void
.end method

.method public onRenderFail(Landroid/view/View;Ljava/lang/String;I)V
    .locals 2

    const-string p1, "cocos neAd "

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u6a21\u677f\u5e7f\u544a\u6e32\u67d3\u5931\u8d25code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ",msg="

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRenderSuccess(Landroid/view/View;FF)V
    .locals 2

    const-string p1, "cocos neAd "

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u6a21\u677f\u5e7f\u544a\u6e32\u67d3\u6210\u529f:width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ",height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object p1, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    iget-object p1, p1, Lorg/cocos2dx/javascript/NativeExAd;->mFrameLayout:Landroid/widget/FrameLayout;

    if-eqz p1, :cond_1

    .line 189
    iget-object p1, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->val$ad:Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;

    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;->getExpressView()Landroid/view/View;

    move-result-object p1

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    const/high16 v0, -0x40000000    # -2.0f

    cmpl-float v0, p3, v0

    if-nez v0, :cond_0

    const/4 p2, -0x1

    const/4 p3, -0x2

    goto :goto_0

    .line 197
    :cond_0
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    invoke-static {v0}, Lorg/cocos2dx/javascript/NativeExAd;->access$400(Lorg/cocos2dx/javascript/NativeExAd;)Lorg/cocos2dx/javascript/AppActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/bytedance/msdk/adapter/util/UIUtils;->getScreenWidth(Landroid/content/Context;)I

    move-result v0

    int-to-float v1, v0

    mul-float v1, v1, p3

    div-float/2addr v1, p2

    float-to-int p3, v1

    move p2, v0

    :goto_0
    if-eqz p1, :cond_1

    .line 201
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 202
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, p2, p3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 203
    iget-object p2, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    iget-object p2, p2, Lorg/cocos2dx/javascript/NativeExAd;->mFrameLayoutContentView:Landroid/widget/FrameLayout;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 204
    iget-object p2, p0, Lorg/cocos2dx/javascript/NativeExAd$6;->this$0:Lorg/cocos2dx/javascript/NativeExAd;

    iget-object p2, p2, Lorg/cocos2dx/javascript/NativeExAd;->mFrameLayoutContentView:Landroid/widget/FrameLayout;

    invoke-virtual {p2, p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method
