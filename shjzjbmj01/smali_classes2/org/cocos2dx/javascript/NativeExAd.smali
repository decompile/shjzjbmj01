.class public Lorg/cocos2dx/javascript/NativeExAd;
.super Ljava/lang/Object;
.source "NativeExAd.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "cocos neAd "


# instance fields
.field private ALLOW_NE_SHOW_TIMES:I

.field private appActivity:Lorg/cocos2dx/javascript/AppActivity;

.field private continueLoadFailTimes:I

.field private handler:Landroid/os/Handler;

.field private mAdUnitId:Ljava/lang/String;

.field private mAds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;",
            ">;"
        }
    .end annotation
.end field

.field public mFrameLayout:Landroid/widget/FrameLayout;

.field public mFrameLayoutContentView:Landroid/widget/FrameLayout;

.field private mReamin:I

.field private mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;

.field mTTAdNative:Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;


# direct methods
.method constructor <init>(Lorg/cocos2dx/javascript/AppActivity;)V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mAds:Ljava/util/List;

    const/4 v0, 0x0

    .line 35
    iput v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->continueLoadFailTimes:I

    const-string v0, "945705412"

    .line 36
    iput-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mAdUnitId:Ljava/lang/String;

    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->handler:Landroid/os/Handler;

    const/4 v0, 0x3

    .line 40
    iput v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->ALLOW_NE_SHOW_TIMES:I

    .line 41
    iput v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mReamin:I

    .line 91
    new-instance v0, Lorg/cocos2dx/javascript/NativeExAd$3;

    invoke-direct {v0, p0}, Lorg/cocos2dx/javascript/NativeExAd$3;-><init>(Lorg/cocos2dx/javascript/NativeExAd;)V

    iput-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;

    .line 43
    iput-object p1, p0, Lorg/cocos2dx/javascript/NativeExAd;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    .line 44
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/NativeExAd;->creatrView()V

    return-void
.end method

.method static synthetic access$000(Lorg/cocos2dx/javascript/NativeExAd;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lorg/cocos2dx/javascript/NativeExAd;->loadAd()V

    return-void
.end method

.method static synthetic access$102(Lorg/cocos2dx/javascript/NativeExAd;I)I
    .locals 0

    .line 30
    iput p1, p0, Lorg/cocos2dx/javascript/NativeExAd;->continueLoadFailTimes:I

    return p1
.end method

.method static synthetic access$200(Lorg/cocos2dx/javascript/NativeExAd;Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lorg/cocos2dx/javascript/NativeExAd;->bindView(Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;)V

    return-void
.end method

.method static synthetic access$300(Lorg/cocos2dx/javascript/NativeExAd;)Ljava/lang/String;
    .locals 0

    .line 30
    iget-object p0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mAdUnitId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lorg/cocos2dx/javascript/NativeExAd;)Lorg/cocos2dx/javascript/AppActivity;
    .locals 0

    .line 30
    iget-object p0, p0, Lorg/cocos2dx/javascript/NativeExAd;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    return-object p0
.end method

.method private bindView(Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;)V
    .locals 2
    .param p1    # Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 145
    :try_start_0
    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;->hasDislike()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    new-instance v1, Lorg/cocos2dx/javascript/NativeExAd$5;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/NativeExAd$5;-><init>(Lorg/cocos2dx/javascript/NativeExAd;)V

    invoke-interface {p1, v0, v1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;->setDislikeCallback(Landroid/app/Activity;Lcom/bytedance/msdk/api/TTDislikeCallback;)V

    .line 165
    :cond_0
    new-instance v0, Lorg/cocos2dx/javascript/NativeExAd$6;

    invoke-direct {v0, p0, p1}, Lorg/cocos2dx/javascript/NativeExAd$6;-><init>(Lorg/cocos2dx/javascript/NativeExAd;Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;)V

    invoke-interface {p1, v0}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;->setTTNativeAdListener(Lcom/bytedance/msdk/api/nativeAd/TTNativeAdListener;)V

    .line 211
    invoke-interface {p1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;->render()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 213
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private loadAd()V
    .locals 4

    .line 100
    new-instance v0, Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;

    iget-object v1, p0, Lorg/cocos2dx/javascript/NativeExAd;->appActivity:Lorg/cocos2dx/javascript/AppActivity;

    iget-object v2, p0, Lorg/cocos2dx/javascript/NativeExAd;->mAdUnitId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mTTAdNative:Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;

    .line 102
    new-instance v0, Lcom/bytedance/msdk/api/AdSlot$Builder;

    invoke-direct {v0}, Lcom/bytedance/msdk/api/AdSlot$Builder;-><init>()V

    const/4 v1, 0x1

    .line 103
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setSupportDeepLink(Z)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v0

    .line 104
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setAdStyleType(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v0

    const/16 v2, 0x280

    const/16 v3, 0x15e

    .line 105
    invoke-virtual {v0, v2, v3}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setImageAdSize(II)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0, v1}, Lcom/bytedance/msdk/api/AdSlot$Builder;->setAdCount(I)Lcom/bytedance/msdk/api/AdSlot$Builder;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcom/bytedance/msdk/api/AdSlot$Builder;->build()Lcom/bytedance/msdk/api/AdSlot;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lorg/cocos2dx/javascript/NativeExAd;->mTTAdNative:Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;

    new-instance v2, Lorg/cocos2dx/javascript/NativeExAd$4;

    invoke-direct {v2, p0}, Lorg/cocos2dx/javascript/NativeExAd$4;-><init>(Lorg/cocos2dx/javascript/NativeExAd;)V

    invoke-virtual {v1, v0, v2}, Lcom/bytedance/msdk/api/nativeAd/TTUnifiedNativeAd;->loadAd(Lcom/bytedance/msdk/api/AdSlot;Lcom/bytedance/msdk/api/nativeAd/TTNativeAdLoadCallback;)V

    return-void
.end method


# virtual methods
.method public continueLoadFailTimesPlus()V
    .locals 2

    .line 230
    iget v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->continueLoadFailTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->continueLoadFailTimes:I

    .line 231
    iget v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->continueLoadFailTimes:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 232
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/NativeExAd;->load()V

    :cond_0
    return-void
.end method

.method public creatrView()V
    .locals 3

    .line 219
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040060

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 220
    new-instance v1, Landroid/widget/FrameLayout;

    sget-object v2, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lorg/cocos2dx/javascript/NativeExAd;->mFrameLayout:Landroid/widget/FrameLayout;

    .line 221
    iget-object v1, p0, Lorg/cocos2dx/javascript/NativeExAd;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 222
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x0

    .line 223
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 224
    sget-object v1, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    iget-object v2, p0, Lorg/cocos2dx/javascript/NativeExAd;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2, v0}, Lorg/cocos2dx/javascript/AppActivity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 225
    sget-object v0, Lorg/cocos2dx/javascript/AppActivity;->_ins:Lorg/cocos2dx/javascript/AppActivity;

    const v1, 0x7f0d0187

    invoke-virtual {v0, v1}, Lorg/cocos2dx/javascript/AppActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mFrameLayoutContentView:Landroid/widget/FrameLayout;

    return-void
.end method

.method protected destroy()V
    .locals 2

    .line 133
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;

    invoke-static {v0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->unregisterConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V

    .line 134
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mAds:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mAds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;

    .line 136
    invoke-interface {v1}, Lcom/bytedance/msdk/api/nativeAd/TTNativeAd;->destroy()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 139
    iput-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mAds:Ljava/util/List;

    return-void
.end method

.method public hide()V
    .locals 2

    .line 76
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->handler:Landroid/os/Handler;

    new-instance v1, Lorg/cocos2dx/javascript/NativeExAd$2;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/NativeExAd$2;-><init>(Lorg/cocos2dx/javascript/NativeExAd;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 82
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/NativeExAd;->load()V

    .line 83
    iget v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mReamin:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mReamin:I

    .line 84
    iget v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mReamin:I

    if-gtz v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/NativeExAd;->resetRemain()V

    const-string v0, "neAdRoundOver()"

    .line 86
    invoke-static {v0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    const-string v0, "cocos"

    const-string v1, "\u64ad\u653e\u5b8c\u4e00\u5708\uff0c\u4e0b\u6b21\u64ad\u653e\u5185\u90e8\u5e7f\u544a"

    .line 87
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public load()V
    .locals 2

    .line 56
    invoke-static {}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->configLoadSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cocos neAd "

    const-string v1, "load ad \u5f53\u524dconfig\u914d\u7f6e\u5b58\u5728\uff0c\u76f4\u63a5\u52a0\u8f7d\u5e7f\u544a"

    .line 57
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-direct {p0}, Lorg/cocos2dx/javascript/NativeExAd;->loadAd()V

    goto :goto_0

    :cond_0
    const-string v0, "cocos neAd "

    const-string v1, "load ad \u5f53\u524dconfig\u914d\u7f6e\u4e0d\u5b58\u5728\uff0c\u6b63\u5728\u8bf7\u6c42config\u914d\u7f6e...."

    .line 60
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mSettingConfigCallback:Lcom/bytedance/msdk/api/TTSettingConfigCallback;

    invoke-static {v0}, Lcom/bytedance/msdk/api/TTMediationAdSdk;->registerConfigCallback(Lcom/bytedance/msdk/api/TTSettingConfigCallback;)V

    :goto_0
    return-void
.end method

.method public resetRemain()V
    .locals 1

    .line 237
    iget v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->ALLOW_NE_SHOW_TIMES:I

    iput v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->mReamin:I

    return-void
.end method

.method public setAdId(Ljava/lang/String;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lorg/cocos2dx/javascript/NativeExAd;->mAdUnitId:Ljava/lang/String;

    .line 50
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/NativeExAd;->load()V

    .line 51
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/NativeExAd;->resetRemain()V

    return-void
.end method

.method public setRoundMaxShowTimes(I)V
    .locals 3

    const-string v0, "cocos"

    .line 240
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "java\u8bbe\u7f6e\u64ad\u653e\u4e00\u5708\u6b21\u6570\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iput p1, p0, Lorg/cocos2dx/javascript/NativeExAd;->ALLOW_NE_SHOW_TIMES:I

    .line 242
    invoke-virtual {p0}, Lorg/cocos2dx/javascript/NativeExAd;->resetRemain()V

    return-void
.end method

.method public show()V
    .locals 2

    .line 67
    iget-object v0, p0, Lorg/cocos2dx/javascript/NativeExAd;->handler:Landroid/os/Handler;

    new-instance v1, Lorg/cocos2dx/javascript/NativeExAd$1;

    invoke-direct {v1, p0}, Lorg/cocos2dx/javascript/NativeExAd$1;-><init>(Lorg/cocos2dx/javascript/NativeExAd;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
