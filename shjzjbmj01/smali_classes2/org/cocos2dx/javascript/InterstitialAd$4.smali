.class Lorg/cocos2dx/javascript/InterstitialAd$4;
.super Ljava/lang/Object;
.source "InterstitialAd.java"

# interfaces
.implements Lcom/bytedance/msdk/api/interstitial/TTInterstitialAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/cocos2dx/javascript/InterstitialAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/cocos2dx/javascript/InterstitialAd;


# direct methods
.method constructor <init>(Lorg/cocos2dx/javascript/InterstitialAd;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lorg/cocos2dx/javascript/InterstitialAd$4;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdLeftApplication()V
    .locals 2

    const-string v0, "cocos insert "

    const-string v1, "onAdLeftApplication"

    .line 126
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onAdOpened()V
    .locals 2

    const-string v0, "cocos insert "

    const-string v1, "onAdOpened"

    .line 122
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onInterstitialAdClick()V
    .locals 2

    const-string v0, "cocos insert "

    const-string v1, "onInterstitialAdClick"

    .line 109
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$4;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v0}, Lorg/cocos2dx/javascript/InterstitialAd;->access$000(Lorg/cocos2dx/javascript/InterstitialAd;)Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;->getAdNetworkRitId()Ljava/lang/String;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lorg/cocos2dx/javascript/InterstitialAd$4;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v1}, Lorg/cocos2dx/javascript/InterstitialAd;->access$100(Lorg/cocos2dx/javascript/InterstitialAd;)Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->sendInsertAdFaultClick(Ljava/lang/String;)V

    return-void
.end method

.method public onInterstitialClosed()V
    .locals 2

    const-string v0, "cocos insert "

    const-string v1, "onInterstitialClosed"

    .line 115
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$4;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/cocos2dx/javascript/InterstitialAd;->access$302(Lorg/cocos2dx/javascript/InterstitialAd;Z)Z

    const-string v0, "closeInsertAd()"

    .line 117
    invoke-static {v0}, Lorg/cocos2dx/javascript/ConchJNI;->RunJS(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$4;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-virtual {v0}, Lorg/cocos2dx/javascript/InterstitialAd;->load()V

    return-void
.end method

.method public onInterstitialShow()V
    .locals 2

    const-string v0, "cocos insert "

    const-string v1, "onInterstitialShow"

    .line 103
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v0, p0, Lorg/cocos2dx/javascript/InterstitialAd$4;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v0}, Lorg/cocos2dx/javascript/InterstitialAd;->access$000(Lorg/cocos2dx/javascript/InterstitialAd;)Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bytedance/msdk/api/interstitial/TTInterstitialAd;->getAdNetworkRitId()Ljava/lang/String;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lorg/cocos2dx/javascript/InterstitialAd$4;->this$0:Lorg/cocos2dx/javascript/InterstitialAd;

    invoke-static {v1}, Lorg/cocos2dx/javascript/InterstitialAd;->access$100(Lorg/cocos2dx/javascript/InterstitialAd;)Lorg/cocos2dx/javascript/AppActivity;

    invoke-static {v0}, Lorg/cocos2dx/javascript/AppActivity;->sendInsertAdShow(Ljava/lang/String;)V

    return-void
.end method
