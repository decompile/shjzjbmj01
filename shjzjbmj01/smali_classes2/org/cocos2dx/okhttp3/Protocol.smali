.class public final enum Lorg/cocos2dx/okhttp3/Protocol;
.super Ljava/lang/Enum;
.source "Protocol.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/cocos2dx/okhttp3/Protocol;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/cocos2dx/okhttp3/Protocol;

.field public static final enum H2_PRIOR_KNOWLEDGE:Lorg/cocos2dx/okhttp3/Protocol;

.field public static final enum HTTP_1_0:Lorg/cocos2dx/okhttp3/Protocol;

.field public static final enum HTTP_1_1:Lorg/cocos2dx/okhttp3/Protocol;

.field public static final enum HTTP_2:Lorg/cocos2dx/okhttp3/Protocol;

.field public static final enum QUIC:Lorg/cocos2dx/okhttp3/Protocol;

.field public static final enum SPDY_3:Lorg/cocos2dx/okhttp3/Protocol;


# instance fields
.field private final protocol:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 33
    new-instance v0, Lorg/cocos2dx/okhttp3/Protocol;

    const-string v1, "HTTP_1_0"

    const-string v2, "http/1.0"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lorg/cocos2dx/okhttp3/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_1_0:Lorg/cocos2dx/okhttp3/Protocol;

    .line 41
    new-instance v0, Lorg/cocos2dx/okhttp3/Protocol;

    const-string v1, "HTTP_1_1"

    const-string v2, "http/1.1"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lorg/cocos2dx/okhttp3/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_1_1:Lorg/cocos2dx/okhttp3/Protocol;

    .line 51
    new-instance v0, Lorg/cocos2dx/okhttp3/Protocol;

    const-string v1, "SPDY_3"

    const-string v2, "spdy/3.1"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lorg/cocos2dx/okhttp3/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/cocos2dx/okhttp3/Protocol;->SPDY_3:Lorg/cocos2dx/okhttp3/Protocol;

    .line 62
    new-instance v0, Lorg/cocos2dx/okhttp3/Protocol;

    const-string v1, "HTTP_2"

    const-string v2, "h2"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lorg/cocos2dx/okhttp3/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_2:Lorg/cocos2dx/okhttp3/Protocol;

    .line 71
    new-instance v0, Lorg/cocos2dx/okhttp3/Protocol;

    const-string v1, "H2_PRIOR_KNOWLEDGE"

    const-string v2, "h2_prior_knowledge"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lorg/cocos2dx/okhttp3/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/cocos2dx/okhttp3/Protocol;->H2_PRIOR_KNOWLEDGE:Lorg/cocos2dx/okhttp3/Protocol;

    .line 81
    new-instance v0, Lorg/cocos2dx/okhttp3/Protocol;

    const-string v1, "QUIC"

    const-string v2, "quic"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lorg/cocos2dx/okhttp3/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/cocos2dx/okhttp3/Protocol;->QUIC:Lorg/cocos2dx/okhttp3/Protocol;

    const/4 v0, 0x6

    .line 29
    new-array v0, v0, [Lorg/cocos2dx/okhttp3/Protocol;

    sget-object v1, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_1_0:Lorg/cocos2dx/okhttp3/Protocol;

    aput-object v1, v0, v3

    sget-object v1, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_1_1:Lorg/cocos2dx/okhttp3/Protocol;

    aput-object v1, v0, v4

    sget-object v1, Lorg/cocos2dx/okhttp3/Protocol;->SPDY_3:Lorg/cocos2dx/okhttp3/Protocol;

    aput-object v1, v0, v5

    sget-object v1, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_2:Lorg/cocos2dx/okhttp3/Protocol;

    aput-object v1, v0, v6

    sget-object v1, Lorg/cocos2dx/okhttp3/Protocol;->H2_PRIOR_KNOWLEDGE:Lorg/cocos2dx/okhttp3/Protocol;

    aput-object v1, v0, v7

    sget-object v1, Lorg/cocos2dx/okhttp3/Protocol;->QUIC:Lorg/cocos2dx/okhttp3/Protocol;

    aput-object v1, v0, v8

    sput-object v0, Lorg/cocos2dx/okhttp3/Protocol;->$VALUES:[Lorg/cocos2dx/okhttp3/Protocol;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 86
    iput-object p3, p0, Lorg/cocos2dx/okhttp3/Protocol;->protocol:Ljava/lang/String;

    return-void
.end method

.method public static get(Ljava/lang/String;)Lorg/cocos2dx/okhttp3/Protocol;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 96
    sget-object v0, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_1_0:Lorg/cocos2dx/okhttp3/Protocol;

    iget-object v0, v0, Lorg/cocos2dx/okhttp3/Protocol;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_1_0:Lorg/cocos2dx/okhttp3/Protocol;

    return-object p0

    .line 97
    :cond_0
    sget-object v0, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_1_1:Lorg/cocos2dx/okhttp3/Protocol;

    iget-object v0, v0, Lorg/cocos2dx/okhttp3/Protocol;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_1_1:Lorg/cocos2dx/okhttp3/Protocol;

    return-object p0

    .line 98
    :cond_1
    sget-object v0, Lorg/cocos2dx/okhttp3/Protocol;->H2_PRIOR_KNOWLEDGE:Lorg/cocos2dx/okhttp3/Protocol;

    iget-object v0, v0, Lorg/cocos2dx/okhttp3/Protocol;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p0, Lorg/cocos2dx/okhttp3/Protocol;->H2_PRIOR_KNOWLEDGE:Lorg/cocos2dx/okhttp3/Protocol;

    return-object p0

    .line 99
    :cond_2
    sget-object v0, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_2:Lorg/cocos2dx/okhttp3/Protocol;

    iget-object v0, v0, Lorg/cocos2dx/okhttp3/Protocol;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p0, Lorg/cocos2dx/okhttp3/Protocol;->HTTP_2:Lorg/cocos2dx/okhttp3/Protocol;

    return-object p0

    .line 100
    :cond_3
    sget-object v0, Lorg/cocos2dx/okhttp3/Protocol;->SPDY_3:Lorg/cocos2dx/okhttp3/Protocol;

    iget-object v0, v0, Lorg/cocos2dx/okhttp3/Protocol;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p0, Lorg/cocos2dx/okhttp3/Protocol;->SPDY_3:Lorg/cocos2dx/okhttp3/Protocol;

    return-object p0

    .line 101
    :cond_4
    sget-object v0, Lorg/cocos2dx/okhttp3/Protocol;->QUIC:Lorg/cocos2dx/okhttp3/Protocol;

    iget-object v0, v0, Lorg/cocos2dx/okhttp3/Protocol;->protocol:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object p0, Lorg/cocos2dx/okhttp3/Protocol;->QUIC:Lorg/cocos2dx/okhttp3/Protocol;

    return-object p0

    .line 102
    :cond_5
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected protocol: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/cocos2dx/okhttp3/Protocol;
    .locals 1

    .line 29
    const-class v0, Lorg/cocos2dx/okhttp3/Protocol;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/cocos2dx/okhttp3/Protocol;

    return-object p0
.end method

.method public static values()[Lorg/cocos2dx/okhttp3/Protocol;
    .locals 1

    .line 29
    sget-object v0, Lorg/cocos2dx/okhttp3/Protocol;->$VALUES:[Lorg/cocos2dx/okhttp3/Protocol;

    invoke-virtual {v0}, [Lorg/cocos2dx/okhttp3/Protocol;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/cocos2dx/okhttp3/Protocol;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 113
    iget-object v0, p0, Lorg/cocos2dx/okhttp3/Protocol;->protocol:Ljava/lang/String;

    return-object v0
.end method
