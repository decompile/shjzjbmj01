.class Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;
.super Ljava/lang/Object;
.source "Cocos2dxDownloader.java"

# interfaces
.implements Lorg/cocos2dx/okhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;


# direct methods
.method constructor <init>(Lorg/cocos2dx/lib/Cocos2dxDownloader$3;)V
    .locals 0

    .line 184
    iput-object p1, p0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lorg/cocos2dx/okhttp3/Call;Ljava/io/IOException;)V
    .locals 3

    .line 187
    iget-object p1, p0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object p1, p1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    iget-object v0, p0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$id:I

    invoke-virtual {p2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, p2, v2}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$400(Lorg/cocos2dx/lib/Cocos2dxDownloader;IILjava/lang/String;[B)V

    return-void
.end method

.method public onResponse(Lorg/cocos2dx/okhttp3/Call;Lorg/cocos2dx/okhttp3/Response;)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    const/16 v0, 0x1000

    .line 193
    new-array v2, v0, [B

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 198
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lorg/cocos2dx/okhttp3/Response;->code()I

    move-result v5

    const/16 v6, 0xc8

    if-lt v5, v6, :cond_c

    invoke-virtual/range {p2 .. p2}, Lorg/cocos2dx/okhttp3/Response;->code()I

    move-result v5

    const/16 v6, 0xce

    if-le v5, v6, :cond_0

    goto/16 :goto_7

    .line 203
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lorg/cocos2dx/okhttp3/Response;->body()Lorg/cocos2dx/okhttp3/ResponseBody;

    move-result-object v5

    invoke-virtual {v5}, Lorg/cocos2dx/okhttp3/ResponseBody;->contentLength()J

    move-result-wide v14

    .line 204
    iget-object v5, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v5, v5, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$path:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    const-wide/16 v7, 0x0

    if-lez v5, :cond_2

    invoke-static {}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$200()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    iget-object v9, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v9, v9, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->host:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    cmp-long v5, v14, v7

    if-lez v5, :cond_1

    .line 206
    invoke-static {}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$200()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    iget-object v9, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v9, v9, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->host:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 208
    :cond_1
    invoke-static {}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$200()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    iget-object v9, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v9, v9, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->host:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    :cond_2
    :goto_0
    iget-object v5, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-wide v9, v5, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->downloadStart:J

    .line 213
    invoke-virtual/range {p2 .. p2}, Lorg/cocos2dx/okhttp3/Response;->body()Lorg/cocos2dx/okhttp3/ResponseBody;

    move-result-object v5

    invoke-virtual {v5}, Lorg/cocos2dx/okhttp3/ResponseBody;->byteStream()Ljava/io/InputStream;

    move-result-object v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 215
    :try_start_1
    iget-object v11, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v11, v11, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$path:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_8

    .line 216
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-wide v12, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->downloadStart:J

    cmp-long v0, v12, v7

    if-lez v0, :cond_3

    .line 217
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v7, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v7, v7, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->tempFile:Ljava/io/File;

    invoke-direct {v0, v7, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    :goto_1
    move-object v12, v0

    goto :goto_2

    .line 219
    :cond_3
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v6, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v6, v6, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->tempFile:Ljava/io/File;

    invoke-direct {v0, v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 223
    :goto_2
    :try_start_2
    invoke-virtual {v5, v2}, Ljava/io/InputStream;->read([B)I

    move-result v0

    const/4 v13, -0x1

    if-eq v0, v13, :cond_4

    int-to-long v7, v0

    add-long v16, v9, v7

    .line 225
    invoke-virtual {v12, v2, v3, v0}, Ljava/io/FileOutputStream;->write([BII)V

    .line 226
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v6, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$id:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-wide v8, v7

    move v7, v0

    move-wide/from16 v10, v16

    move-object/from16 v18, v12

    const/4 v0, -0x1

    move-wide v12, v14

    :try_start_3
    invoke-static/range {v6 .. v13}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$500(Lorg/cocos2dx/lib/Cocos2dxDownloader;IJJJ)V

    move-wide/from16 v9, v16

    move-object/from16 v12, v18

    goto :goto_2

    :cond_4
    move-object/from16 v18, v12

    .line 228
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->flush()V

    .line 233
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->finalFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 234
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->finalFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_3

    .line 237
    :cond_5
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->finalFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_6

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t remove old file:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v2, v2, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->finalFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 242
    :cond_6
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->tempFile:Ljava/io/File;

    iget-object v2, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v2, v2, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->finalFile:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    :goto_3
    move-object v0, v4

    :goto_4
    if-nez v0, :cond_7

    .line 246
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    iget-object v2, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget v2, v2, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$id:I

    invoke-static {v0, v2, v3, v4, v4}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$400(Lorg/cocos2dx/lib/Cocos2dxDownloader;IILjava/lang/String;[B)V

    .line 247
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    invoke-static {v0}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$600(Lorg/cocos2dx/lib/Cocos2dxDownloader;)V

    goto/16 :goto_6

    .line 250
    :cond_7
    iget-object v2, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v2, v2, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    iget-object v6, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget v6, v6, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$id:I

    invoke-static {v2, v6, v3, v0, v4}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$400(Lorg/cocos2dx/lib/Cocos2dxDownloader;IILjava/lang/String;[B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_6

    :catch_0
    move-exception v0

    goto/16 :goto_8

    :catchall_0
    move-exception v0

    move-object/from16 v18, v12

    goto/16 :goto_c

    :catch_1
    move-exception v0

    move-object/from16 v18, v12

    goto/16 :goto_8

    :cond_8
    const/4 v12, -0x1

    cmp-long v6, v14, v7

    if-lez v6, :cond_9

    .line 255
    :try_start_4
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    long-to-int v6, v14

    invoke-direct {v0, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    goto :goto_5

    .line 257
    :cond_9
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    move-object v0, v6

    .line 261
    :goto_5
    invoke-virtual {v5, v2}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-eq v6, v12, :cond_a

    int-to-long v7, v6

    add-long v16, v9, v7

    .line 263
    invoke-virtual {v0, v2, v3, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 264
    iget-object v6, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v6, v6, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    iget-object v9, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget v9, v9, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$id:I

    move-wide v10, v7

    move v7, v9

    move-wide v8, v10

    move-wide/from16 v10, v16

    const/16 v18, -0x1

    move-wide v12, v14

    invoke-static/range {v6 .. v13}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$500(Lorg/cocos2dx/lib/Cocos2dxDownloader;IJJJ)V

    move-wide/from16 v9, v16

    const/4 v12, -0x1

    goto :goto_5

    .line 266
    :cond_a
    iget-object v2, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v2, v2, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    iget-object v6, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget v6, v6, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$id:I

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {v2, v6, v3, v4, v0}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$400(Lorg/cocos2dx/lib/Cocos2dxDownloader;IILjava/lang/String;[B)V

    .line 267
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    invoke-static {v0}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$600(Lorg/cocos2dx/lib/Cocos2dxDownloader;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object/from16 v18, v4

    :goto_6
    if-eqz v5, :cond_b

    .line 275
    :try_start_5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    :cond_b
    if-eqz v18, :cond_e

    .line 278
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_b

    :catchall_1
    move-exception v0

    move-object v2, v0

    goto :goto_d

    :catch_2
    move-exception v0

    move-object/from16 v18, v4

    goto :goto_8

    .line 199
    :cond_c
    :goto_7
    :try_start_6
    iget-object v0, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v0, v0, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    iget-object v2, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget v2, v2, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$id:I

    const/4 v5, -0x2

    invoke-virtual/range {p2 .. p2}, Lorg/cocos2dx/okhttp3/Response;->message()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v2, v5, v6, v4}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$400(Lorg/cocos2dx/lib/Cocos2dxDownloader;IILjava/lang/String;[B)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    return-void

    :catchall_2
    move-exception v0

    move-object v2, v0

    move-object v5, v4

    goto :goto_d

    :catch_3
    move-exception v0

    move-object v5, v4

    move-object/from16 v18, v5

    .line 270
    :goto_8
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 271
    iget-object v2, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget-object v2, v2, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$downloader:Lorg/cocos2dx/lib/Cocos2dxDownloader;

    iget-object v6, v1, Lorg/cocos2dx/lib/Cocos2dxDownloader$3$1;->this$0:Lorg/cocos2dx/lib/Cocos2dxDownloader$3;

    iget v6, v6, Lorg/cocos2dx/lib/Cocos2dxDownloader$3;->val$id:I

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v6, v3, v0, v4}, Lorg/cocos2dx/lib/Cocos2dxDownloader;->access$400(Lorg/cocos2dx/lib/Cocos2dxDownloader;IILjava/lang/String;[B)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v5, :cond_d

    .line 275
    :try_start_8
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_9

    :catch_4
    move-exception v0

    goto :goto_a

    :cond_d
    :goto_9
    if-eqz v18, :cond_e

    .line 278
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_b

    :goto_a
    const-string v2, "Cocos2dxDownloader"

    .line 281
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    :goto_b
    return-void

    :catchall_3
    move-exception v0

    :goto_c
    move-object v2, v0

    move-object/from16 v4, v18

    :goto_d
    if-eqz v5, :cond_f

    .line 275
    :try_start_9
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_e

    :catch_5
    move-exception v0

    goto :goto_f

    :cond_f
    :goto_e
    if-eqz v4, :cond_10

    .line 278
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_10

    .line 281
    :goto_f
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Cocos2dxDownloader"

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :cond_10
    :goto_10
    throw v2
.end method
