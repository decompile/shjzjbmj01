.class final Lcom/tencent/bugly/crashreport/crash/anr/b$4;
.super Landroid/os/FileObserver;
.source "BUGLY"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/tencent/bugly/crashreport/crash/anr/b;->i()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/tencent/bugly/crashreport/crash/anr/b;


# direct methods
.method constructor <init>(Lcom/tencent/bugly/crashreport/crash/anr/b;Ljava/lang/String;I)V
    .locals 0

    .line 833
    iput-object p1, p0, Lcom/tencent/bugly/crashreport/crash/anr/b$4;->a:Lcom/tencent/bugly/crashreport/crash/anr/b;

    const/16 p1, 0x100

    invoke-direct {p0, p2, p1}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final onEvent(ILjava/lang/String;)V
    .locals 3

    if-nez p2, :cond_0

    return-void

    :cond_0
    const-string p1, "startWatchingPrivateAnrDir %s"

    const/4 v0, 0x1

    .line 840
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {p1, v1}, Lcom/tencent/bugly/proguard/x;->d(Ljava/lang/String;[Ljava/lang/Object;)Z

    .line 841
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v1, "/data/anr/"

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "trace"

    .line 842
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    const-string p2, "not anr file %s"

    .line 843
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2, v0}, Lcom/tencent/bugly/proguard/x;->d(Ljava/lang/String;[Ljava/lang/Object;)Z

    return-void

    .line 847
    :cond_1
    iget-object p1, p0, Lcom/tencent/bugly/crashreport/crash/anr/b$4;->a:Lcom/tencent/bugly/crashreport/crash/anr/b;

    invoke-static {p1}, Lcom/tencent/bugly/crashreport/crash/anr/b;->a(Lcom/tencent/bugly/crashreport/crash/anr/b;)Lcom/tencent/bugly/proguard/ab;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 848
    iget-object p1, p0, Lcom/tencent/bugly/crashreport/crash/anr/b$4;->a:Lcom/tencent/bugly/crashreport/crash/anr/b;

    invoke-static {p1}, Lcom/tencent/bugly/crashreport/crash/anr/b;->a(Lcom/tencent/bugly/crashreport/crash/anr/b;)Lcom/tencent/bugly/proguard/ab;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/tencent/bugly/proguard/ab;->a(Z)V

    :cond_2
    return-void
.end method
