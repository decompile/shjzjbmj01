.class public final enum Lcom/sigmob/windad/WindAdError;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/windad/WindAdError;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ERROR_INVALID_ADSLOT_ID:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_OTHER:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_REQUEST_APP_IS_CLOSED:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_REQUEST_APP_NOT_SET_CHANNEL:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_REQUEST_APP_NOT_SET_STRATEGY:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_REQUEST_NEED_AD_SLOTS_INFO:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_REQUEST_NEED_DEVICE_ID_INFO:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_REQUEST_NEED_DEVICE_INFO:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_REQUEST_NO_DEVICE_ID:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_REQUEST_NO_SUCH_APP:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_REQUEST_SIGMOB_NOT_SUPPORT_REGION:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_AD_DB_INSERT:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_AD_PLAY:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_AD_PLAY_CHECK_FAIL:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_AD_PLAY_HAS_PLAYING:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_AD_TIME_OUT:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_FILE_DOWNLOAD:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_FILE_MD5:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_GDPR_DENIED:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_INFORMATION_LOSE:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_INIT_FAIL:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_INSTALL_FAIL:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_NETWORK:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_NOT_FOUD_ADAPTER:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_NOT_INIT:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_NOT_READY:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_PLACEMENTID_EMPTY:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_PLACEMNT_ID_IS_EMPTY:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_REQUEST:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_SPLASH_TIMEOUT:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_SPLASH_UNSUPPORT_ORIENTATION:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_SPLASH_UNSUPPORT_RESOURCE:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_STRATEGY:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SIGMOB_STRATEGY_EMPTY:Lcom/sigmob/windad/WindAdError;

.field public static final enum ERROR_SPLASH_ADBLOCK:Lcom/sigmob/windad/WindAdError;

.field public static final enum REQUEST_AD_SLOT_IS_CLOSED:Lcom/sigmob/windad/WindAdError;

.field public static final enum REQUEST_AD_SLOT_NOT_EXISTS:Lcom/sigmob/windad/WindAdError;

.field public static final enum REQUEST_AD_SLOT_NOT_MATCH__AD_TYPE:Lcom/sigmob/windad/WindAdError;

.field public static final enum REQUEST_OS_TYPE_NOT_MATCH_APP_TYPE:Lcom/sigmob/windad/WindAdError;

.field public static final enum RTB_SIG_DSP_NO_ADS_ERROR:Lcom/sigmob/windad/WindAdError;

.field private static final synthetic b:[Lcom/sigmob/windad/WindAdError;


# instance fields
.field private final a:I

.field private message:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_REQUEST_SIGMOB_NOT_SUPPORT_REGION"

    const-string v2, "\u8be5\u5730\u533a\u65e0sigmob\u5e7f\u544a\u6e20\u9053\u670d\u52a1"

    const/4 v3, 0x0

    const v4, 0x7a2b2

    invoke-direct {v0, v1, v3, v4, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_SIGMOB_NOT_SUPPORT_REGION:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_REQUEST_APP_IS_CLOSED"

    const-string v2, "\u8bf7\u6c42\u7684app\u5df2\u7ecf\u5173\u95ed\u5e7f\u544a\u670d\u52a1"

    const/4 v4, 0x1

    const v5, 0x7a2c4

    invoke-direct {v0, v1, v4, v5, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_APP_IS_CLOSED:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_REQUEST_NEED_DEVICE_INFO"

    const-string v2, "\u8bf7\u6c42\u53c2\u6570\u7f3a\u5c11\u8bbe\u5907\u4fe1\u606f"

    const/4 v5, 0x2

    const v6, 0x7a2c6

    invoke-direct {v0, v1, v5, v6, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NEED_DEVICE_INFO:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_REQUEST_NEED_DEVICE_ID_INFO"

    const-string v2, "\u7f3a\u5c11\u8bbe\u5907id\u76f8\u5173\u4fe1\u606f"

    const/4 v6, 0x3

    const v7, 0x7a2c8

    invoke-direct {v0, v1, v6, v7, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NEED_DEVICE_ID_INFO:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_REQUEST_NEED_AD_SLOTS_INFO"

    const-string v2, " \u7f3a\u5c11\u5e7f\u544a\u4f4d\u4fe1\u606f"

    const/4 v7, 0x4

    const v8, 0x7a2cc

    invoke-direct {v0, v1, v7, v8, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NEED_AD_SLOTS_INFO:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_INVALID_ADSLOT_ID"

    const-string v2, " \u9519\u8bef\u7684\u5e7f\u544a\u4f4d\u4fe1\u606f"

    const/4 v8, 0x5

    const v9, 0x7a2ce

    invoke-direct {v0, v1, v8, v9, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_INVALID_ADSLOT_ID:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "REQUEST_AD_SLOT_NOT_EXISTS"

    const-string v2, "\u5e7f\u544a\u4f4d\u4e0d\u5b58\u5728\uff0c\u6216\u8005appid\u4e0e\u5e7f\u544a\u4f4d\u4e0d\u5339\u914d"

    const/4 v9, 0x6

    const v10, 0x7a2d0

    invoke-direct {v0, v1, v9, v10, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->REQUEST_AD_SLOT_NOT_EXISTS:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "REQUEST_AD_SLOT_IS_CLOSED"

    const-string v2, "\u5e7f\u544a\u4f4d\u4e0d\u5b58\u5728\u6216\u662f\u5df2\u5173\u95ed"

    const/4 v10, 0x7

    const v11, 0x7a2d1

    invoke-direct {v0, v1, v10, v11, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->REQUEST_AD_SLOT_IS_CLOSED:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "REQUEST_OS_TYPE_NOT_MATCH_APP_TYPE"

    const-string v2, "\u8bbe\u5907\u7684\u64cd\u4f5c\u7cfb\u7edf\u7c7b\u578b\uff0c\u4e0e\u8bf7\u6c42\u7684app\u7684\u7cfb\u7edf\u7c7b\u578b\u4e0d\u5339\u914d"

    const/16 v11, 0x8

    const v12, 0x7a2d3

    invoke-direct {v0, v1, v11, v12, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->REQUEST_OS_TYPE_NOT_MATCH_APP_TYPE:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "REQUEST_AD_SLOT_NOT_MATCH__AD_TYPE"

    const-string v2, "\u5e7f\u544a\u5355\u5143id\u4e0e\u8bf7\u6c42\u7684\u5e7f\u544a\u7c7b\u578b\u4e0d\u5339\u914d"

    const/16 v12, 0x9

    const v13, 0x7a2d4

    invoke-direct {v0, v1, v12, v13, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->REQUEST_AD_SLOT_NOT_MATCH__AD_TYPE:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_REQUEST_NO_SUCH_APP"

    const-string v2, "\u8bf7\u6c42\u7684app\u4e0d\u5b58\u5728"

    const/16 v13, 0xa

    const v14, 0x7a2f9

    invoke-direct {v0, v1, v13, v14, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NO_SUCH_APP:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_PLACEMNT_ID_IS_EMPTY"

    const-string v2, "\u5e7f\u544a\u4f4did\u4e3a\u7a7a"

    const/16 v14, 0xb

    const v15, 0x7a2ff

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_PLACEMNT_ID_IS_EMPTY:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_REQUEST_NO_DEVICE_ID"

    const-string v2, "\u65e0\u6cd5\u83b7\u53d6\u8bbe\u5907ID"

    const/16 v15, 0xc

    const v14, 0x7a302

    invoke-direct {v0, v1, v15, v14, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NO_DEVICE_ID:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_REQUEST_APP_NOT_SET_STRATEGY"

    const-string v2, "app\u672a\u8bbe\u7f6e\u805a\u5408\u7b56\u7565"

    const/16 v14, 0xd

    const v15, 0x7a3dc

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_APP_NOT_SET_STRATEGY:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_REQUEST_APP_NOT_SET_CHANNEL"

    const-string v2, "app\u672a\u5f00\u901a\u4efb\u4f55\u5e7f\u544a\u6e20\u9053"

    const/16 v15, 0xe

    const v14, 0x7a3dd

    invoke-direct {v0, v1, v15, v14, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_APP_NOT_SET_CHANNEL:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "RTB_SIG_DSP_NO_ADS_ERROR"

    const-string v2, "\u5e7f\u544a\u65e0\u586b\u5145"

    const/16 v14, 0xf

    const v15, 0x30d40

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->RTB_SIG_DSP_NO_ADS_ERROR:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_NETWORK"

    const-string v2, "\u7f51\u7edc\u9519\u8bef"

    const/16 v14, 0x10

    const v15, 0x92824

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NETWORK:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_REQUEST"

    const-string v2, "\u5e7f\u544a\u8bf7\u6c42\u51fa\u9519"

    const/16 v14, 0x11

    const v15, 0x92825

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_REQUEST:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_NOT_FOUD_ADAPTER"

    const-string v2, "\u672a\u627e\u5230\u8be5\u6e20\u9053\u7684\u9002\u914d\u5668"

    const/16 v14, 0x12

    const v15, 0x92826

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_FOUD_ADAPTER:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_STRATEGY_EMPTY"

    const-string v2, "\u914d\u7f6e\u7684\u7b56\u7565\u4e3a\u7a7a"

    const/16 v14, 0x13

    const v15, 0x92827

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_STRATEGY_EMPTY:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_FILE_DOWNLOAD"

    const-string v2, "\u6587\u4ef6\u4e0b\u8f7d\u9519\u8bef"

    const/16 v14, 0x14

    const v15, 0x92828

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_FILE_DOWNLOAD:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_AD_TIME_OUT"

    const-string v2, "\u4e0b\u8f7d\u5e7f\u544a\u8d85\u65f6"

    const/16 v14, 0x15

    const v15, 0x92829

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_TIME_OUT:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_NOT_INIT"

    const-string v2, "SDK\u672a\u521d\u59cb\u5316"

    const/16 v14, 0x16

    const v15, 0x92b44

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_INIT:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_PLACEMENTID_EMPTY"

    const-string v2, "\u5e7f\u544a\u4f4d\u4e3a\u7a7a"

    const/16 v14, 0x17

    const v15, 0x92b45

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_PLACEMENTID_EMPTY:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_STRATEGY"

    const-string v2, "\u7b56\u7565\u8bf7\u6c42\u5931\u8d25"

    const/16 v14, 0x18

    const v15, 0x92b46

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_STRATEGY:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_INSTALL_FAIL"

    const-string v2, "\u5b89\u88c5\u5931\u8d25"

    const/16 v14, 0x19

    const v15, 0x92b47

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_INSTALL_FAIL:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_AD_DB_INSERT"

    const-string v2, "\u63d2\u5165\u6570\u636e\u5e93\u5931\u8d25"

    const/16 v14, 0x1a

    const v15, 0x92b48

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_DB_INSERT:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_GDPR_DENIED"

    const-string v2, "User GDPR Consent Status is denied"

    const/16 v14, 0x1b

    const v15, 0x92b49

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_GDPR_DENIED:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_INIT_FAIL"

    const-string v2, "SDK \u521d\u59cb\u5316\u5931\u8d25"

    const/16 v14, 0x1c

    const v15, 0x92b4a

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_INIT_FAIL:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_OTHER"

    const-string v2, ""

    const/16 v14, 0x1d

    const v15, 0x92ba7

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_OTHER:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_AD_PLAY"

    const-string v2, "\u6fc0\u52b1\u89c6\u9891\u64ad\u653e\u51fa\u9519"

    const/16 v14, 0x1e

    const v15, 0x94ed2

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_PLAY:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_NOT_READY"

    const-string v2, "\u6fc0\u52b1\u89c6\u9891\u672a\u51c6\u5907\u597d"

    const/16 v14, 0x1f

    const v15, 0x94ed3

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_READY:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_INFORMATION_LOSE"

    const-string v2, "server\u4e0b\u53d1\u7684\u5e7f\u544a\u4fe1\u606f\u7f3a\u5931\u5173\u952e\u4fe1\u606f"

    const/16 v14, 0x20

    const v15, 0x94ed4

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_INFORMATION_LOSE:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_FILE_MD5"

    const-string v2, "\u4e0b\u8f7d\u7684\u6587\u4ef6\u6821\u9a8cmd5\u51fa\u9519"

    const/16 v14, 0x21

    const v15, 0x94ed5

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_FILE_MD5:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_AD_PLAY_CHECK_FAIL"

    const-string v2, "\u6fc0\u52b1\u89c6\u9891\u64ad\u63a5\u53e3\u68c0\u67e5\u51fa\u9519\uff08\u5e7f\u544a\u8fc7\u671f\u6216\u8005\u672aready)"

    const/16 v14, 0x22

    const v15, 0x94ed6

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_PLAY_CHECK_FAIL:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_AD_PLAY_HAS_PLAYING"

    const-string v2, "\u6fc0\u52b1\u89c6\u9891\u64ad\u63a5\u53e3\u68c0\u67e5\u51fa\u9519\uff08\u6fc0\u52b1\u89c6\u9891\u5f53\u524d\u6709\u5e7f\u544a\u6b63\u5728\u64ad\u653e\u4e2d)"

    const/16 v14, 0x23

    const v15, 0x94ed7

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_PLAY_HAS_PLAYING:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_SPLASH_TIMEOUT"

    const-string v2, "\u5f00\u5c4f\u5e7f\u544a\u52a0\u8f7d\u8d85\u65f6"

    const/16 v14, 0x24

    const v15, 0x975e1

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_SPLASH_TIMEOUT:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_SPLASH_UNSUPPORT_ORIENTATION"

    const-string v2, "\u5f00\u5c4f\u5e7f\u544a\u4e0d\u652f\u6301\u5f53\u524d\u65b9\u5411"

    const/16 v14, 0x25

    const v15, 0x975e2

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_ORIENTATION:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SIGMOB_SPLASH_UNSUPPORT_RESOURCE"

    const-string v2, "\u5f00\u5c4f\u5e7f\u544a\u4e0d\u652f\u6301\u7684\u8d44\u6e90\u7c7b\u578b"

    const/16 v14, 0x26

    const v15, 0x975e3

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_RESOURCE:Lcom/sigmob/windad/WindAdError;

    new-instance v0, Lcom/sigmob/windad/WindAdError;

    const-string v1, "ERROR_SPLASH_ADBLOCK"

    const-string v2, "AD BLOCK"

    const/16 v14, 0x27

    const v15, 0x97964

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/sigmob/windad/WindAdError;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SPLASH_ADBLOCK:Lcom/sigmob/windad/WindAdError;

    const/16 v0, 0x28

    new-array v0, v0, [Lcom/sigmob/windad/WindAdError;

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_SIGMOB_NOT_SUPPORT_REGION:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_APP_IS_CLOSED:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NEED_DEVICE_INFO:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NEED_DEVICE_ID_INFO:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NEED_AD_SLOTS_INFO:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_INVALID_ADSLOT_ID:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sigmob/windad/WindAdError;->REQUEST_AD_SLOT_NOT_EXISTS:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v9

    sget-object v1, Lcom/sigmob/windad/WindAdError;->REQUEST_AD_SLOT_IS_CLOSED:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sigmob/windad/WindAdError;->REQUEST_OS_TYPE_NOT_MATCH_APP_TYPE:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sigmob/windad/WindAdError;->REQUEST_AD_SLOT_NOT_MATCH__AD_TYPE:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v12

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NO_SUCH_APP:Lcom/sigmob/windad/WindAdError;

    aput-object v1, v0, v13

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_PLACEMNT_ID_IS_EMPTY:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_NO_DEVICE_ID:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_APP_NOT_SET_STRATEGY:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_REQUEST_APP_NOT_SET_CHANNEL:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->RTB_SIG_DSP_NO_ADS_ERROR:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NETWORK:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_REQUEST:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_FOUD_ADAPTER:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_STRATEGY_EMPTY:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_FILE_DOWNLOAD:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_TIME_OUT:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_INIT:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_PLACEMENTID_EMPTY:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_STRATEGY:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_INSTALL_FAIL:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_DB_INSERT:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_GDPR_DENIED:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_INIT_FAIL:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_OTHER:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_PLAY:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_READY:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_INFORMATION_LOSE:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_FILE_MD5:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_PLAY_CHECK_FAIL:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_AD_PLAY_HAS_PLAYING:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_SPLASH_TIMEOUT:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_ORIENTATION:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_RESOURCE:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/windad/WindAdError;->ERROR_SPLASH_ADBLOCK:Lcom/sigmob/windad/WindAdError;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sput-object v0, Lcom/sigmob/windad/WindAdError;->b:[Lcom/sigmob/windad/WindAdError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sigmob/windad/WindAdError;->a:I

    iput-object p4, p0, Lcom/sigmob/windad/WindAdError;->message:Ljava/lang/String;

    return-void
.end method

.method public static getWindAdError(I)Lcom/sigmob/windad/WindAdError;
    .locals 5

    invoke-static {}, Lcom/sigmob/windad/WindAdError;->values()[Lcom/sigmob/windad/WindAdError;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lcom/sigmob/windad/WindAdError;->getErrorCode()I

    move-result v4

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/windad/WindAdError;
    .locals 1

    const-class v0, Lcom/sigmob/windad/WindAdError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/windad/WindAdError;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/windad/WindAdError;
    .locals 1

    sget-object v0, Lcom/sigmob/windad/WindAdError;->b:[Lcom/sigmob/windad/WindAdError;

    invoke-virtual {v0}, [Lcom/sigmob/windad/WindAdError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/windad/WindAdError;

    return-object v0
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    iget v0, p0, Lcom/sigmob/windad/WindAdError;->a:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sigmob/windad/WindAdError;->message:Ljava/lang/String;

    return-object v0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/windad/WindAdError;->message:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "{ \"error_code\":%d, \"message\": %s }"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/sigmob/windad/WindAdError;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/sigmob/windad/WindAdError;->message:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
