.class public Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;
.super Lcom/sigmob/windad/Splash/WindSplashAdAdapter;

# interfaces
.implements Lcom/sigmob/sdk/splash/b;


# instance fields
.field private a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

.field private b:Lcom/sigmob/sdk/splash/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sigmob/windad/Splash/WindSplashAdAdapter;-><init>()V

    return-void
.end method

.method private a(ILjava/lang/String;)Lcom/sigmob/windad/WindAdAdapterError;
    .locals 1

    new-instance v0, Lcom/sigmob/windad/WindAdAdapterError;

    invoke-direct {v0, p1, p2}, Lcom/sigmob/windad/WindAdAdapterError;-><init>(ILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public initWithWAdConnector(Lcom/sigmob/windad/Splash/WindSplashAdBridge;)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    return-void
.end method

.method public initializeSdk(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {}, Lcom/sigmob/sdk/a;->b()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-static {}, Lcom/sigmob/windad/WindAds;->sharedAds()Lcom/sigmob/windad/WindAds;

    move-result-object p1

    invoke-virtual {p1}, Lcom/sigmob/windad/WindAds;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/sdk/a;->a(Landroid/content/Context;)Lcom/sigmob/sdk/a;

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "initializeSdk Success"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->i(Ljava/lang/String;)V

    return-void
.end method

.method public isInited()Z
    .locals 1

    invoke-static {}, Lcom/sigmob/sdk/a;->b()Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onDestroy(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onPause(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->b:Lcom/sigmob/sdk/splash/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->b:Lcom/sigmob/sdk/splash/c;

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/splash/c;->a(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public onResume(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->b:Lcom/sigmob/sdk/splash/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->b:Lcom/sigmob/sdk/splash/c;

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/splash/c;->b(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public onSplashAdClicked()V
    .locals 1

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    invoke-interface {v0, p0}, Lcom/sigmob/windad/Splash/WindSplashAdConnector;->adapterDidAdClickSplashAd(Lcom/sigmob/windad/Splash/WindSplashAdAdapter;)V

    :cond_0
    return-void
.end method

.method public onSplashAdFailToPresent(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    invoke-direct {p0, p1, p2}, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a(ILjava/lang/String;)Lcom/sigmob/windad/WindAdAdapterError;

    move-result-object p1

    invoke-interface {v0, p0, p1}, Lcom/sigmob/windad/Splash/WindSplashAdConnector;->adapterDidFailPresentScreenSplashAd(Lcom/sigmob/windad/Splash/WindSplashAdAdapter;Lcom/sigmob/windad/WindAdAdapterError;)V

    :cond_0
    return-void
.end method

.method public onSplashAdSuccessPresentScreen()V
    .locals 1

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    invoke-interface {v0, p0}, Lcom/sigmob/windad/Splash/WindSplashAdConnector;->adapterDidSuccessPresentScreenSplashAd(Lcom/sigmob/windad/Splash/WindSplashAdAdapter;)V

    :cond_0
    return-void
.end method

.method public onSplashClosed()V
    .locals 1

    iget-object v0, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    invoke-interface {v0, p0}, Lcom/sigmob/windad/Splash/WindSplashAdConnector;->adapterDidCloseSplashAd(Lcom/sigmob/windad/Splash/WindSplashAdAdapter;)V

    return-void
.end method

.method public onStart(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public onStop(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public show(Landroid/app/Activity;Landroid/view/ViewGroup;ILcom/sigmob/windad/WindAdRequest;)V
    .locals 7

    if-eqz p1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/sdk/base/common/c;->a(Landroid/content/Context;)Lcom/sigmob/sdk/common/models/SigmobError;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check loadAd error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    const-string v1, "load"

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v2

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sigmob/sdk/common/mta/PointEntitySigmobError;->SigmobError(Ljava/lang/String;ILjava/lang/String;)Lcom/sigmob/sdk/common/mta/PointEntitySigmobError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/mta/PointEntitySigmobError;->commit()V

    iget-object v1, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a:Lcom/sigmob/windad/Splash/WindSplashAdConnector;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v2

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->a(ILjava/lang/String;)Lcom/sigmob/windad/WindAdAdapterError;

    move-result-object v0

    invoke-interface {v1, p0, v0}, Lcom/sigmob/windad/Splash/WindSplashAdConnector;->adapterDidFailPresentScreenSplashAd(Lcom/sigmob/windad/Splash/WindSplashAdAdapter;Lcom/sigmob/windad/WindAdAdapterError;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    new-instance v0, Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {p4}, Lcom/sigmob/windad/WindAdRequest;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4}, Lcom/sigmob/windad/WindAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4}, Lcom/sigmob/windad/WindAdRequest;->getAdType()I

    move-result v4

    invoke-virtual {p4}, Lcom/sigmob/windad/WindAdRequest;->getLoadId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p4}, Lcom/sigmob/windad/WindAdRequest;->getOptions()Ljava/util/Map;

    move-result-object v6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/sigmob/sdk/base/models/LoadAdRequest;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;)V

    new-instance p4, Lcom/sigmob/sdk/splash/c;

    move-object v1, p4

    move-object v2, p1

    move-object v3, p2

    move-object v4, v0

    move v5, p3

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/sigmob/sdk/splash/c;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/sigmob/sdk/base/models/LoadAdRequest;ILcom/sigmob/sdk/splash/b;)V

    iput-object p4, p0, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;->b:Lcom/sigmob/sdk/splash/c;

    return-void
.end method
