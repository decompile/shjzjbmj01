.class public final Lcom/sigmob/windad/Splash/WindSplashAD;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/windad/Splash/WindSplashAdBridge$WindSplashAdBridgeListener;


# instance fields
.field private a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

.field private b:Lcom/sigmob/windad/Splash/WindSplashADListener;

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/sigmob/sdk/common/models/ADStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/os/Handler;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/sigmob/windad/Splash/WindSplashAdAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/sigmob/windad/Splash/WindSplashAdRequest;Lcom/sigmob/windad/Splash/WindSplashADListener;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p3, :cond_0

    const-string p1, "WindAdRequest is null"

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    if-eqz p4, :cond_6

    sget-object p1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_REQUEST:Lcom/sigmob/windad/WindAdError;

    const/4 p2, 0x0

    :goto_0
    invoke-interface {p4, p1, p2}, Lcom/sigmob/windad/Splash/WindSplashADListener;->onSplashAdFailToPresent(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_0
    if-nez p1, :cond_1

    const-string p1, "activity is null"

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    if-eqz p4, :cond_6

    sget-object p1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_REQUEST:Lcom/sigmob/windad/WindAdError;

    :goto_1
    invoke-virtual {p3}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    sget-boolean v0, Lcom/sigmob/windad/WindAds;->isInited:Z

    if-nez v0, :cond_2

    const-string p1, "WindAds not initialize"

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    if-eqz p4, :cond_6

    sget-object p1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_INIT:Lcom/sigmob/windad/WindAdError;

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/sigmob/sdk/common/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object p1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_GDPR_DENIED:Lcom/sigmob/windad/WindAdError;

    invoke-virtual {p1}, Lcom/sigmob/windad/WindAdError;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    if-eqz p4, :cond_6

    sget-object p1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_GDPR_DENIED:Lcom/sigmob/windad/WindAdError;

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/sigmob/windad/WindAdLifecycleManager;->getInstance()Lcom/sigmob/windad/WindAdLifecycleManager;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/windad/WindAdLifecycleManager;->initalize(Landroid/app/Application;)V

    :cond_4
    invoke-virtual {p3}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string p1, "placementId with WindAdRequest is empty"

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    if-eqz p4, :cond_6

    sget-object p1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_PLACEMNT_ID_IS_EMPTY:Lcom/sigmob/windad/WindAdError;

    goto :goto_1

    :cond_5
    iput-object p4, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->b:Lcom/sigmob/windad/Splash/WindSplashADListener;

    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    iput-object p4, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->d:Ljava/util/List;

    new-instance p4, Ljava/util/HashMap;

    invoke-direct {p4}, Ljava/util/HashMap;-><init>()V

    iput-object p4, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->g:Ljava/util/Map;

    iput-object p3, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    new-instance p4, Ljava/util/HashMap;

    invoke-direct {p4}, Ljava/util/HashMap;-><init>()V

    iput-object p4, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->f:Ljava/util/Map;

    iget-object p4, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->f:Ljava/util/Map;

    invoke-virtual {p3}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p4, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p4, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->e:Landroid/os/Handler;

    const/4 p4, 0x2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/sigmob/windad/Splash/WindSplashAdRequest;I)V

    :cond_6
    :goto_2
    return-void
.end method

.method static synthetic a(Lcom/sigmob/windad/Splash/WindSplashAD;)Lcom/sigmob/windad/Splash/WindSplashADListener;
    .locals 0

    iget-object p0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->b:Lcom/sigmob/windad/Splash/WindSplashADListener;

    return-object p0
.end method

.method private a(Lcom/sigmob/sdk/common/models/ADStrategy;Ljava/lang/String;)Lcom/sigmob/windad/Splash/WindSplashAdAdapter;
    .locals 5

    const-string v0, "init"

    iget-object v1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    invoke-virtual {v1}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sigmob/sdk/common/mta/PointEntityWind;->WindTracking(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sigmob/sdk/common/mta/PointEntityWind;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sigmob/sdk/common/mta/PointEntityWind;->setPlatform(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/mta/PointEntityWind;->commit()V

    iget-object v0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sigmob/windad/Splash/WindSplashAdAdapter;

    if-eqz v0, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "find already initialize Adapter: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    return-object v0

    :cond_0
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/sigmob/windad/Splash/WindSplashAdAdapter;

    const/4 v3, 0x0

    if-ne v1, v2, :cond_5

    invoke-static {}, Lcom/sigmob/windad/Splash/WindSplashAdBridge;->Bridge()Lcom/sigmob/windad/Splash/WindSplashAdBridge;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sigmob/windad/Splash/WindSplashAdBridge;->setStrategy(Lcom/sigmob/sdk/common/models/ADStrategy;)V

    iget-object v2, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    invoke-virtual {v1, v2}, Lcom/sigmob/windad/Splash/WindSplashAdBridge;->setRequest(Lcom/sigmob/windad/Splash/WindSplashAdRequest;)V

    invoke-virtual {v1, p0}, Lcom/sigmob/windad/Splash/WindSplashAdBridge;->setAdBridgeListener(Lcom/sigmob/windad/Splash/WindSplashAdBridge$WindSplashAdBridgeListener;)V

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getOptions()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sigmob/windad/Splash/WindSplashAdBridge;->a(Ljava/util/Map;)V

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sigmob/windad/Splash/WindSplashAdAdapter;

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/Splash/WindSplashAdAdapter;->initWithWAdConnector(Lcom/sigmob/windad/Splash/WindSplashAdBridge;)V

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getOptions()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getOptions()Ljava/util/Map;

    move-result-object v1

    const-string v2, "appid"

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getOptions()Ljava/util/Map;

    move-result-object v1

    const-string v2, "appid"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v1, v3

    :goto_0
    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getOptions()Ljava/util/Map;

    move-result-object v2

    const-string v4, "appkey"

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getOptions()Ljava/util/Map;

    move-result-object v2

    const-string v3, "appkey"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v1, v3

    :cond_3
    :goto_1
    invoke-virtual {v0}, Lcom/sigmob/windad/Splash/WindSplashAdAdapter;->isInited()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {}, Lcom/sigmob/windad/WindAds;->sharedAds()Lcom/sigmob/windad/WindAds;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sigmob/windad/WindAds;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1, v3}, Lcom/sigmob/windad/Splash/WindSplashAdAdapter;->initializeSdk(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->g:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeChannelAdapter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " cls: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "success"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->i(Ljava/lang/String;)V

    return-object v0

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initializeChannelAdapter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " cls: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " failed!"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    return-object v3
.end method

.method private a()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method private a(Lcom/sigmob/sdk/common/models/ADStrategy;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x35ca9371

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "sigmob"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_2

    const/4 p1, 0x0

    return-object p1

    :cond_2
    const-class p1, Lcom/sigmob/windad/Adapter/SigmobSplashAdAdapter;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroid/app/Activity;Landroid/view/ViewGroup;ILcom/sigmob/windad/Splash/WindSplashAdRequest;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/ViewGroup;",
            "I",
            "Lcom/sigmob/windad/Splash/WindSplashAdRequest;",
            "Ljava/util/List<",
            "Lcom/sigmob/sdk/common/models/ADStrategy;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->d:Ljava/util/List;

    invoke-interface {v0, p5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object p5, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->d:Ljava/util/List;

    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :cond_0
    :goto_0
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/sigmob/sdk/common/models/ADStrategy;

    invoke-direct {p0, v3}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Lcom/sigmob/sdk/common/models/ADStrategy;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "load"

    sget-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_FOUD_ADAPTER:Lcom/sigmob/windad/WindAdError;

    invoke-virtual {v0}, Lcom/sigmob/windad/WindAdError;->getErrorCode()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/sigmob/sdk/common/models/ADStrategy;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " can\'t get class String"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v2, p4

    invoke-static/range {v1 .. v6}, Lcom/sigmob/windad/common/PointEntityWindUtils;->WindError(Ljava/lang/String;Lcom/sigmob/windad/WindAdRequest;Lcom/sigmob/sdk/common/models/ADStrategy;ILjava/lang/String;Lcom/sigmob/windad/common/PointEntityWindUtils$OnPointEntityExtraInfo;)V

    sget-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_FOUD_ADAPTER:Lcom/sigmob/windad/WindAdError;

    iget-object v1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    invoke-virtual {v1}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1
    invoke-direct {p0, v3, v0}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Lcom/sigmob/sdk/common/models/ADStrategy;Ljava/lang/String;)Lcom/sigmob/windad/Splash/WindSplashAdAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getOptions()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p4, v1}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->setOptions(Ljava/util/Map;)V

    :cond_2
    new-instance v1, Ljava/util/HashMap;

    invoke-virtual {p4}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getOptions()Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {p4}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getAppTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "APP_TITLE"

    invoke-virtual {p4}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getAppTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-virtual {p4}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getAppDesc()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "APP_DESC"

    invoke-virtual {p4}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getAppDesc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-virtual {p4}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->isDisableAutoHideAd()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "SPLASH_DISABLE_AD_HIDE"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-virtual {p4, v1}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->setOptions(Ljava/util/Map;)V

    invoke-static {}, Lcom/sigmob/windad/WindAdLifecycleManager;->getInstance()Lcom/sigmob/windad/WindAdLifecycleManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sigmob/windad/WindAdLifecycleManager;->addLifecycleListener(Lcom/sigmob/windad/LifecycleListener;)V

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sigmob/windad/Splash/WindSplashAdAdapter;->show(Landroid/app/Activity;Landroid/view/ViewGroup;ILcom/sigmob/windad/WindAdRequest;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_NOT_FOUD_ADAPTER:Lcom/sigmob/windad/WindAdError;

    iget-object v1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    invoke-virtual {v1}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    :goto_1
    iget-object p1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->g:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->e:Landroid/os/Handler;

    new-instance p2, Lcom/sigmob/windad/Splash/WindSplashAD$1;

    invoke-direct {p2, p0}, Lcom/sigmob/windad/Splash/WindSplashAD$1;-><init>(Lcom/sigmob/windad/Splash/WindSplashAD;)V

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_7
    return-void
.end method

.method private a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/sigmob/windad/Splash/WindSplashAdRequest;I)V
    .locals 4

    iget v0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->c:I

    const-string v0, "request"

    iget-object v1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    new-instance v2, Lcom/sigmob/windad/Splash/WindSplashAD$2;

    invoke-direct {v2, p0}, Lcom/sigmob/windad/Splash/WindSplashAD$2;-><init>(Lcom/sigmob/windad/Splash/WindSplashAD;)V

    invoke-static {v0, v1, v2}, Lcom/sigmob/windad/common/PointEntityWindUtils;->WindInit(Ljava/lang/String;Lcom/sigmob/windad/WindAdRequest;Lcom/sigmob/windad/common/PointEntityWindUtils$OnPointEntityExtraInfo;)V

    invoke-static {}, Lcom/sigmob/sdk/common/SDKConfig;->sharedInstance()Lcom/sigmob/sdk/common/SDKConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/SDKConfig;->getStrategyUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getUserId()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sigmob/windad/Splash/WindSplashAD$3;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/sigmob/windad/Splash/WindSplashAD$3;-><init>(Lcom/sigmob/windad/Splash/WindSplashAD;Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/sigmob/windad/Splash/WindSplashAdRequest;)V

    invoke-static {v0, p4, v1, v2, v3}, Lcom/sigmob/sdk/common/e/o;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/common/e/o$a;)V

    return-void
.end method

.method static synthetic a(Lcom/sigmob/windad/Splash/WindSplashAD;Landroid/app/Activity;Landroid/view/ViewGroup;ILcom/sigmob/windad/Splash/WindSplashAdRequest;Ljava/util/List;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Landroid/app/Activity;Landroid/view/ViewGroup;ILcom/sigmob/windad/Splash/WindSplashAdRequest;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/sigmob/windad/Splash/WindSplashAD;Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSplashError: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " :placementId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->e:Landroid/os/Handler;

    new-instance v1, Lcom/sigmob/windad/Splash/WindSplashAD$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/sigmob/windad/Splash/WindSplashAD$4;-><init>(Lcom/sigmob/windad/Splash/WindSplashAD;Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic b(Lcom/sigmob/windad/Splash/WindSplashAD;)Lcom/sigmob/windad/Splash/WindSplashAdRequest;
    .locals 0

    iget-object p0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    return-object p0
.end method

.method static synthetic c(Lcom/sigmob/windad/Splash/WindSplashAD;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->f:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic d(Lcom/sigmob/windad/Splash/WindSplashAD;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/sigmob/windad/Splash/WindSplashAD;->a()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public adapterDidAdClickWithStrategy(Lcom/sigmob/sdk/common/models/ADStrategy;)V
    .locals 2

    const-string p1, "click"

    iget-object v0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    invoke-virtual {v0}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/sigmob/sdk/common/mta/PointEntityWind;->WindTracking(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sigmob/sdk/common/mta/PointEntityWind;

    move-result-object p1

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/mta/PointEntityWind;->commit()V

    iget-object p1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->b:Lcom/sigmob/windad/Splash/WindSplashADListener;

    invoke-interface {p1}, Lcom/sigmob/windad/Splash/WindSplashADListener;->onSplashAdClicked()V

    return-void
.end method

.method public adapterDidCloseSplashAdWithStrategy(Lcom/sigmob/sdk/common/models/ADStrategy;)V
    .locals 2

    const-string p1, "close"

    iget-object v0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1, v1}, Lcom/sigmob/windad/common/PointEntityWindUtils;->WindTracking(Ljava/lang/String;Lcom/sigmob/windad/WindAdRequest;Lcom/sigmob/sdk/common/models/ADStrategy;Lcom/sigmob/windad/common/PointEntityWindUtils$OnPointEntityExtraInfo;)V

    iget-object p1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->b:Lcom/sigmob/windad/Splash/WindSplashADListener;

    invoke-interface {p1}, Lcom/sigmob/windad/Splash/WindSplashADListener;->onSplashClosed()V

    return-void
.end method

.method public adapterDidFailToLoadSplashAdWithStrategy(Lcom/sigmob/sdk/common/models/ADStrategy;Lcom/sigmob/windad/WindAdAdapterError;)V
    .locals 6

    const-string v0, "load"

    iget-object v1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    invoke-virtual {p2}, Lcom/sigmob/windad/WindAdAdapterError;->getErrorCode()I

    move-result v3

    invoke-virtual {p2}, Lcom/sigmob/windad/WindAdAdapterError;->getMessage()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/sigmob/windad/common/PointEntityWindUtils;->WindError(Ljava/lang/String;Lcom/sigmob/windad/WindAdRequest;Lcom/sigmob/sdk/common/models/ADStrategy;ILjava/lang/String;Lcom/sigmob/windad/common/PointEntityWindUtils$OnPointEntityExtraInfo;)V

    sget-object v0, Lcom/sigmob/windad/WindAdError;->RTB_SIG_DSP_NO_ADS_ERROR:Lcom/sigmob/windad/WindAdError;

    const-string v1, "{error_code: %s, message: %s}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/sigmob/windad/WindAdAdapterError;->getErrorCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p2}, Lcom/sigmob/windad/WindAdAdapterError;->getMessage()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sigmob/windad/WindAdError;->setMessage(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/models/ADStrategy;->getPlacement_id()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "adapterDidFailToLoadSplashAdWithStrategy() called with: strategy = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "], error = ["

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    return-void
.end method

.method public adapterDidFailToSetupSplashAdWithStrategy(Lcom/sigmob/sdk/common/models/ADStrategy;Lcom/sigmob/windad/WindAdAdapterError;)V
    .locals 0

    return-void
.end method

.method public adapterDidReceiveSplashAdWithStrategy(Lcom/sigmob/sdk/common/models/ADStrategy;)V
    .locals 0

    return-void
.end method

.method public adapterDidSetupSplashAdWithStrategy(Lcom/sigmob/sdk/common/models/ADStrategy;)V
    .locals 0

    return-void
.end method

.method public adapterDidStartPlayingSplashAdWithStrategy(Lcom/sigmob/sdk/common/models/ADStrategy;)V
    .locals 2

    const-string p1, "start"

    iget-object v0, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->a:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1, v1}, Lcom/sigmob/windad/common/PointEntityWindUtils;->WindTracking(Ljava/lang/String;Lcom/sigmob/windad/WindAdRequest;Lcom/sigmob/sdk/common/models/ADStrategy;Lcom/sigmob/windad/common/PointEntityWindUtils$OnPointEntityExtraInfo;)V

    iget-object p1, p0, Lcom/sigmob/windad/Splash/WindSplashAD;->b:Lcom/sigmob/windad/Splash/WindSplashADListener;

    invoke-interface {p1}, Lcom/sigmob/windad/Splash/WindSplashADListener;->onSplashAdSuccessPresentScreen()V

    return-void
.end method
