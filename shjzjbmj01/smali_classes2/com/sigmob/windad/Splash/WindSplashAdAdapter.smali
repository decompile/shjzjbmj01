.class public abstract Lcom/sigmob/windad/Splash/WindSplashAdAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/windad/LifecycleListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract initWithWAdConnector(Lcom/sigmob/windad/Splash/WindSplashAdBridge;)V
.end method

.method public abstract initializeSdk(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract isInited()Z
.end method

.method public abstract show(Landroid/app/Activity;Landroid/view/ViewGroup;ILcom/sigmob/windad/WindAdRequest;)V
.end method
