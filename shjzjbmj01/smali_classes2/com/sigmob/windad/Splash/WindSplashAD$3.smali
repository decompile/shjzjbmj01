.class Lcom/sigmob/windad/Splash/WindSplashAD$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/sdk/common/e/o$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sigmob/windad/Splash/WindSplashAD;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/sigmob/windad/Splash/WindSplashAdRequest;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

.field final synthetic d:Lcom/sigmob/windad/Splash/WindSplashAD;


# direct methods
.method constructor <init>(Lcom/sigmob/windad/Splash/WindSplashAD;Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/sigmob/windad/Splash/WindSplashAdRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->d:Lcom/sigmob/windad/Splash/WindSplashAD;

    iput-object p2, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->a:Landroid/app/Activity;

    iput-object p3, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->b:Landroid/view/ViewGroup;

    iput-object p4, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->c:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Ljava/lang/String;IILjava/lang/String;)V
    .locals 6

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "error_code  "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " msg: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    const-string v0, "request"

    iget-object p2, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->d:Lcom/sigmob/windad/Splash/WindSplashAD;

    invoke-static {p2}, Lcom/sigmob/windad/Splash/WindSplashAD;->b(Lcom/sigmob/windad/Splash/WindSplashAD;)Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    move-result-object v1

    new-instance v5, Lcom/sigmob/windad/Splash/WindSplashAD$3$2;

    invoke-direct {v5, p0, p1}, Lcom/sigmob/windad/Splash/WindSplashAD$3$2;-><init>(Lcom/sigmob/windad/Splash/WindSplashAD$3;Ljava/lang/String;)V

    const/4 v2, 0x0

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/sigmob/windad/common/PointEntityWindUtils;->WindError(Ljava/lang/String;Lcom/sigmob/windad/WindAdRequest;Lcom/sigmob/sdk/common/models/ADStrategy;ILjava/lang/String;Lcom/sigmob/windad/common/PointEntityWindUtils$OnPointEntityExtraInfo;)V

    invoke-static {p3}, Lcom/sigmob/windad/WindAdError;->getWindAdError(I)Lcom/sigmob/windad/WindAdError;

    move-result-object p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/sigmob/windad/WindAdError;->ERROR_SIGMOB_STRATEGY_EMPTY:Lcom/sigmob/windad/WindAdError;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "error_code  "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " msg: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/sigmob/windad/WindAdError;->setMessage(Ljava/lang/String;)V

    :cond_0
    iget-object p2, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->d:Lcom/sigmob/windad/Splash/WindSplashAD;

    iget-object p3, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->d:Lcom/sigmob/windad/Splash/WindSplashAD;

    invoke-static {p3}, Lcom/sigmob/windad/Splash/WindSplashAD;->b(Lcom/sigmob/windad/Splash/WindSplashAD;)Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    move-result-object p3

    invoke-virtual {p3}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p1, p3}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Lcom/sigmob/windad/Splash/WindSplashAD;Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;Lcom/sigmob/sdk/common/e/n;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/sigmob/sdk/common/models/ADStrategy;",
            ">;",
            "Lcom/sigmob/sdk/common/e/n;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string p2, "respond"

    iget-object v0, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->d:Lcom/sigmob/windad/Splash/WindSplashAD;

    invoke-static {v0}, Lcom/sigmob/windad/Splash/WindSplashAD;->b(Lcom/sigmob/windad/Splash/WindSplashAD;)Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    move-result-object v0

    new-instance v1, Lcom/sigmob/windad/Splash/WindSplashAD$3$1;

    invoke-direct {v1, p0, p3}, Lcom/sigmob/windad/Splash/WindSplashAD$3$1;-><init>(Lcom/sigmob/windad/Splash/WindSplashAD$3;Ljava/lang/String;)V

    invoke-static {p2, v0, v1}, Lcom/sigmob/windad/common/PointEntityWindUtils;->WindInit(Ljava/lang/String;Lcom/sigmob/windad/WindAdRequest;Lcom/sigmob/windad/common/PointEntityWindUtils$OnPointEntityExtraInfo;)V

    iget-object v2, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->d:Lcom/sigmob/windad/Splash/WindSplashAD;

    iget-object v3, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->a:Landroid/app/Activity;

    iget-object v4, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->b:Landroid/view/ViewGroup;

    iget-object p2, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->c:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    invoke-virtual {p2}, Lcom/sigmob/windad/Splash/WindSplashAdRequest;->getFetchDelay()I

    move-result v5

    iget-object v6, p0, Lcom/sigmob/windad/Splash/WindSplashAD$3;->c:Lcom/sigmob/windad/Splash/WindSplashAdRequest;

    move-object v7, p1

    invoke-static/range {v2 .. v7}, Lcom/sigmob/windad/Splash/WindSplashAD;->a(Lcom/sigmob/windad/Splash/WindSplashAD;Landroid/app/Activity;Landroid/view/ViewGroup;ILcom/sigmob/windad/Splash/WindSplashAdRequest;Ljava/util/List;)V

    return-void
.end method
