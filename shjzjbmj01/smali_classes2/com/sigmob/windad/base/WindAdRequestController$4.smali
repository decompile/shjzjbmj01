.class Lcom/sigmob/windad/base/WindAdRequestController$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/windad/common/PointEntityWindUtils$OnPointEntityExtraInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sigmob/windad/base/WindAdRequestController;->getReadyAdapter(Lcom/sigmob/windad/base/WindAdRequestController$onFindReadyAdapterLister;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sigmob/windad/base/WindVideoAdAdapter;

.field final synthetic b:Z

.field final synthetic c:Lcom/sigmob/windad/base/WindAdRequestController;


# direct methods
.method constructor <init>(Lcom/sigmob/windad/base/WindAdRequestController;Lcom/sigmob/windad/base/WindVideoAdAdapter;Z)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/windad/base/WindAdRequestController$4;->c:Lcom/sigmob/windad/base/WindAdRequestController;

    iput-object p2, p0, Lcom/sigmob/windad/base/WindAdRequestController$4;->a:Lcom/sigmob/windad/base/WindVideoAdAdapter;

    iput-boolean p3, p0, Lcom/sigmob/windad/base/WindAdRequestController$4;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddExtra(Ljava/lang/Object;)V
    .locals 3

    instance-of v0, p1, Lcom/sigmob/sdk/common/mta/PointEntityWind;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/sigmob/sdk/common/mta/PointEntityWind;

    iget-object v0, p0, Lcom/sigmob/windad/base/WindAdRequestController$4;->c:Lcom/sigmob/windad/base/WindAdRequestController;

    iget-object v0, v0, Lcom/sigmob/windad/base/WindAdRequestController;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntityWind;->setLoad_id(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/windad/base/WindAdRequestController$4;->a:Lcom/sigmob/windad/base/WindVideoAdAdapter;

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntityWind;->setSub_category(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    const-string v0, "0"

    goto :goto_0

    :goto_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "expired"

    iget-boolean v2, p0, Lcom/sigmob/windad/base/WindAdRequestController$4;->b:Z

    if-eqz v2, :cond_1

    const-string v2, "1"

    goto :goto_2

    :cond_1
    const-string v2, "0"

    :goto_2
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntityWind;->setOptions(Ljava/util/Map;)V

    iget-object v0, p0, Lcom/sigmob/windad/base/WindAdRequestController$4;->c:Lcom/sigmob/windad/base/WindAdRequestController;

    invoke-virtual {v0}, Lcom/sigmob/windad/base/WindAdRequestController;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/common/mta/PointEntityWind;->setVlist(Ljava/lang/String;)V

    :cond_2
    return-void
.end method
