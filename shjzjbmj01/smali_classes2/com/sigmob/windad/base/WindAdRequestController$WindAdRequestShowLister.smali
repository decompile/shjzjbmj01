.class public interface abstract Lcom/sigmob/windad/base/WindAdRequestController$WindAdRequestShowLister;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sigmob/windad/base/WindAdRequestController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WindAdRequestShowLister"
.end annotation


# virtual methods
.method public abstract onVideoAdClicked(Ljava/lang/String;)V
.end method

.method public abstract onVideoAdClosed(Lcom/sigmob/windad/rewardedVideo/WindRewardInfo;Ljava/lang/String;)V
.end method

.method public abstract onVideoAdPlayComplete(Ljava/lang/String;)V
.end method

.method public abstract onVideoAdPlayEnd(Ljava/lang/String;)V
.end method

.method public abstract onVideoAdPlayError(Lcom/sigmob/windad/WindAdError;Ljava/lang/String;)V
.end method

.method public abstract onVideoAdPlayStart(Ljava/lang/String;)V
.end method
