.class public final enum Lcom/sigmob/volley/toolbox/d$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sigmob/volley/toolbox/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/volley/toolbox/d$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sigmob/volley/toolbox/d$a;

.field public static final enum b:Lcom/sigmob/volley/toolbox/d$a;

.field public static final enum c:Lcom/sigmob/volley/toolbox/d$a;

.field public static final enum d:Lcom/sigmob/volley/toolbox/d$a;

.field public static final enum e:Lcom/sigmob/volley/toolbox/d$a;

.field private static final synthetic g:[Lcom/sigmob/volley/toolbox/d$a;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/sigmob/volley/toolbox/d$a;

    const-string v1, "VIDEO"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/sigmob/volley/toolbox/d$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/volley/toolbox/d$a;->a:Lcom/sigmob/volley/toolbox/d$a;

    new-instance v0, Lcom/sigmob/volley/toolbox/d$a;

    const-string v1, "PICTURE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v3, v4}, Lcom/sigmob/volley/toolbox/d$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/volley/toolbox/d$a;->b:Lcom/sigmob/volley/toolbox/d$a;

    new-instance v0, Lcom/sigmob/volley/toolbox/d$a;

    const-string v1, "FILE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v4, v5}, Lcom/sigmob/volley/toolbox/d$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/volley/toolbox/d$a;->c:Lcom/sigmob/volley/toolbox/d$a;

    new-instance v0, Lcom/sigmob/volley/toolbox/d$a;

    const-string v1, "APK"

    const/16 v6, 0x8

    invoke-direct {v0, v1, v5, v6}, Lcom/sigmob/volley/toolbox/d$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/volley/toolbox/d$a;->d:Lcom/sigmob/volley/toolbox/d$a;

    new-instance v0, Lcom/sigmob/volley/toolbox/d$a;

    const-string v1, "OTHER"

    const/4 v6, 0x4

    const/16 v7, 0x9

    invoke-direct {v0, v1, v6, v7}, Lcom/sigmob/volley/toolbox/d$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/volley/toolbox/d$a;->e:Lcom/sigmob/volley/toolbox/d$a;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sigmob/volley/toolbox/d$a;

    sget-object v1, Lcom/sigmob/volley/toolbox/d$a;->a:Lcom/sigmob/volley/toolbox/d$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/volley/toolbox/d$a;->b:Lcom/sigmob/volley/toolbox/d$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/volley/toolbox/d$a;->c:Lcom/sigmob/volley/toolbox/d$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sigmob/volley/toolbox/d$a;->d:Lcom/sigmob/volley/toolbox/d$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sigmob/volley/toolbox/d$a;->e:Lcom/sigmob/volley/toolbox/d$a;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sigmob/volley/toolbox/d$a;->g:[Lcom/sigmob/volley/toolbox/d$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sigmob/volley/toolbox/d$a;->f:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/volley/toolbox/d$a;
    .locals 1

    const-class v0, Lcom/sigmob/volley/toolbox/d$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/volley/toolbox/d$a;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/volley/toolbox/d$a;
    .locals 1

    sget-object v0, Lcom/sigmob/volley/toolbox/d$a;->g:[Lcom/sigmob/volley/toolbox/d$a;

    invoke-virtual {v0}, [Lcom/sigmob/volley/toolbox/d$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/volley/toolbox/d$a;

    return-object v0
.end method
