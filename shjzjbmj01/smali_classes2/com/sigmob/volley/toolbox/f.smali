.class public Lcom/sigmob/volley/toolbox/f;
.super Lcom/sigmob/volley/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sigmob/volley/toolbox/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sigmob/volley/m<",
        "Lcom/sigmob/volley/toolbox/d;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Ljava/io/File;

.field private c:Ljava/io/File;

.field private d:Lcom/sigmob/volley/toolbox/d;

.field private e:J

.field private final f:Ljava/lang/Object;

.field private g:Lcom/sigmob/volley/toolbox/f$a;


# direct methods
.method public constructor <init>(Lcom/sigmob/volley/toolbox/d;Lcom/sigmob/volley/toolbox/f$a;)V
    .locals 5

    iget-object v0, p1, Lcom/sigmob/volley/toolbox/d;->a:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v2, v0, v1}, Lcom/sigmob/volley/m;-><init>(ILjava/lang/String;Lcom/sigmob/volley/o$a;)V

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/sigmob/volley/toolbox/f;->e:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sigmob/volley/toolbox/f;->f:Ljava/lang/Object;

    iput-object v1, p0, Lcom/sigmob/volley/toolbox/f;->g:Lcom/sigmob/volley/toolbox/f$a;

    new-instance v0, Ljava/io/File;

    iget-object v1, p1, Lcom/sigmob/volley/toolbox/d;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_1
    iput-object p1, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/sigmob/volley/toolbox/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".tmp"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    iget-boolean v0, p1, Lcom/sigmob/volley/toolbox/d;->h:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_2
    iput-object p2, p0, Lcom/sigmob/volley/toolbox/f;->g:Lcom/sigmob/volley/toolbox/f$a;

    new-instance p2, Lcom/sigmob/volley/d;

    const/16 v0, 0x2710

    const/4 v1, 0x0

    const/4 v3, 0x2

    invoke-direct {p2, v0, v3, v1}, Lcom/sigmob/volley/d;-><init>(IIF)V

    invoke-virtual {p0, p2}, Lcom/sigmob/volley/toolbox/f;->a(Lcom/sigmob/volley/q;)Lcom/sigmob/volley/m;

    invoke-virtual {p0, v2}, Lcom/sigmob/volley/toolbox/f;->a(Z)Lcom/sigmob/volley/m;

    const-string p2, "FileDownloadRequest()  [ %s ], url = [%s]"

    new-array v0, v3, [Ljava/lang/Object;

    iget-object v1, p1, Lcom/sigmob/volley/toolbox/d;->c:Ljava/lang/String;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget-object p1, p1, Lcom/sigmob/volley/toolbox/d;->a:Ljava/lang/String;

    aput-object p1, v0, v1

    invoke-static {p2, v0}, Lcom/sigmob/volley/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/sigmob/volley/j;)Lcom/sigmob/volley/o;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sigmob/volley/j;",
            ")",
            "Lcom/sigmob/volley/o<",
            "Lcom/sigmob/volley/toolbox/d;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    iget-wide v1, p1, Lcom/sigmob/volley/j;->f:J

    iput-wide v1, v0, Lcom/sigmob/volley/toolbox/d;->f:J

    invoke-virtual {p0}, Lcom/sigmob/volley/toolbox/f;->m()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object p1, p1, Lcom/sigmob/volley/j;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/sigmob/volley/toolbox/e;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object p1

    const-string v0, "Transfer-Encoding"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "Transfer-Encoding"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "chunked"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    goto :goto_0

    :cond_0
    const-string v0, "content-length"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "content-length"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    iget-wide v5, p0, Lcom/sigmob/volley/toolbox/f;->e:J

    add-long/2addr v3, v5

    goto :goto_0

    :cond_1
    move-wide v3, v1

    :goto_0
    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    iput-wide v3, v0, Lcom/sigmob/volley/toolbox/d;->e:J

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v0

    const/4 v5, 0x0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v0, v6, v1

    if-lez v0, :cond_b

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v0, v6, v3

    if-eqz v0, :cond_2

    cmp-long v0, v3, v1

    if-nez v0, :cond_b

    :cond_2
    invoke-static {p1}, Lcom/sigmob/volley/toolbox/h;->b(Ljava/util/Map;)Z

    move-result p1

    if-eqz p1, :cond_9

    const/4 p1, 0x0

    :try_start_0
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v2, 0x1000

    :try_start_2
    new-array v3, v2, [B

    :goto_1
    invoke-virtual {v0, v3, p1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_3

    invoke-virtual {v1, v3, p1, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    :cond_3
    const/4 v2, 0x1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v3, p1, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/sigmob/volley/u;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {v0, p1}, Lcom/sigmob/volley/u;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_3
    const/4 p1, 0x1

    goto :goto_6

    :catch_2
    move-exception v2

    goto :goto_4

    :catchall_0
    move-exception v2

    goto :goto_7

    :catch_3
    move-exception v2

    move-object v1, v5

    goto :goto_4

    :catchall_1
    move-exception v2

    move-object v0, v5

    goto :goto_7

    :catch_4
    move-exception v2

    move-object v0, v5

    move-object v1, v0

    :goto_4
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    new-array v3, p1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/sigmob/volley/u;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v0, :cond_4

    :try_start_6
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_5

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v2, p1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/sigmob/volley/u;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    :goto_5
    if-eqz v1, :cond_5

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto :goto_6

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v1, p1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/sigmob/volley/u;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    :goto_6
    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    invoke-static {p1, v5}, Lcom/sigmob/volley/o;->a(Ljava/lang/Object;Lcom/sigmob/volley/b$a;)Lcom/sigmob/volley/o;

    move-result-object p1

    return-object p1

    :cond_6
    new-instance p1, Lcom/sigmob/volley/t;

    const-string v0, "error gzip unzip the download temporary file!"

    invoke-direct {p1, v0}, Lcom/sigmob/volley/t;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sigmob/volley/o;->a(Lcom/sigmob/volley/t;)Lcom/sigmob/volley/o;

    move-result-object p1

    return-object p1

    :catchall_2
    move-exception v2

    move-object v5, v1

    :goto_7
    if-eqz v0, :cond_7

    :try_start_8
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    goto :goto_8

    :catch_7
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v1, p1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/sigmob/volley/u;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    :goto_8
    if-eqz v5, :cond_8

    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    goto :goto_9

    :catch_8
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {v0, p1}, Lcom/sigmob/volley/u;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    :goto_9
    throw v2

    :cond_9
    iget-object p1, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {p1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result p1

    if-eqz p1, :cond_a

    iget-object p1, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    invoke-static {p1, v5}, Lcom/sigmob/volley/o;->a(Ljava/lang/Object;Lcom/sigmob/volley/b$a;)Lcom/sigmob/volley/o;

    move-result-object p1

    return-object p1

    :cond_a
    new-instance p1, Lcom/sigmob/volley/t;

    const-string v0, "Can\'t rename the download temporary file!"

    invoke-direct {p1, v0}, Lcom/sigmob/volley/t;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sigmob/volley/o;->a(Lcom/sigmob/volley/t;)Lcom/sigmob/volley/o;

    move-result-object p1

    return-object p1

    :cond_b
    iget-object p1, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result p1

    if-eqz p1, :cond_c

    iget-object p1, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    cmp-long p1, v0, v3

    if-nez p1, :cond_c

    iget-object p1, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    invoke-static {p1, v5}, Lcom/sigmob/volley/o;->a(Ljava/lang/Object;Lcom/sigmob/volley/b$a;)Lcom/sigmob/volley/o;

    move-result-object p1

    return-object p1

    :cond_c
    new-instance p1, Lcom/sigmob/volley/t;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Download temporary file was invalid!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/sigmob/volley/t;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sigmob/volley/o;->a(Lcom/sigmob/volley/t;)Lcom/sigmob/volley/o;

    move-result-object p1

    return-object p1

    :cond_d
    new-instance p1, Lcom/sigmob/volley/t;

    const-string v0, "Request was Canceled!"

    invoke-direct {p1, v0}, Lcom/sigmob/volley/t;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sigmob/volley/o;->a(Lcom/sigmob/volley/t;)Lcom/sigmob/volley/o;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/sigmob/volley/t;)V
    .locals 2

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sigmob/volley/toolbox/d;->g:I

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_1
    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    iput-object p1, v0, Lcom/sigmob/volley/toolbox/d;->i:Lcom/sigmob/volley/t;

    iget-object p1, p0, Lcom/sigmob/volley/toolbox/f;->g:Lcom/sigmob/volley/toolbox/f$a;

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    invoke-interface {p1, v0}, Lcom/sigmob/volley/toolbox/f$a;->c(Lcom/sigmob/volley/toolbox/d;)V

    return-void
.end method

.method protected a(Lcom/sigmob/volley/toolbox/d;)V
    .locals 2

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    const/4 v1, 0x1

    iput v1, v0, Lcom/sigmob/volley/toolbox/d;->g:I

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->g:Lcom/sigmob/volley/toolbox/f$a;

    invoke-interface {v0, p1}, Lcom/sigmob/volley/toolbox/f$a;->a(Lcom/sigmob/volley/toolbox/d;)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/sigmob/volley/toolbox/d;

    invoke-virtual {p0, p1}, Lcom/sigmob/volley/toolbox/f;->a(Lcom/sigmob/volley/toolbox/d;)V

    return-void
.end method

.method public a(Lcom/sigmob/volley/toolbox/i;)[B
    .locals 18

    move-object/from16 v1, p0

    invoke-virtual/range {p1 .. p1}, Lcom/sigmob/volley/toolbox/i;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/volley/toolbox/e;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/volley/toolbox/h;->a(Ljava/util/Map;)Z

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/sigmob/volley/toolbox/i;->c()I

    move-result v3

    int-to-long v3, v3

    const-string v5, "Transfer-Encoding"

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, -0x1

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    if-eqz v5, :cond_0

    const-string v5, "Transfer-Encoding"

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v10, "chunked"

    invoke-virtual {v5, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v0, "Response doesn\'t present Content-Length!"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lcom/sigmob/volley/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_0
    cmp-long v5, v3, v7

    if-ltz v5, :cond_4

    const-string v5, "Content-Length"

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    cmp-long v5, v3, v7

    if-nez v5, :cond_1

    iget-object v5, v1, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, v1, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v10

    cmp-long v5, v10, v3

    if-nez v5, :cond_1

    iget-object v0, v1, Lcom/sigmob/volley/toolbox/f;->b:Ljava/io/File;

    iget-object v2, v1, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    invoke-virtual/range {p1 .. p1}, Lcom/sigmob/volley/toolbox/i;->d()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    new-array v0, v9, [B

    return-object v0

    :cond_1
    if-eqz v2, :cond_3

    iget-object v5, v1, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v10

    add-long/2addr v3, v10

    const-string v5, "Content-Range"

    invoke-static {v0, v5}, Lcom/sigmob/volley/toolbox/h;->b(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "Content-Range %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v0, v12, v9

    invoke-static {v5, v12}, Lcom/sigmob/volley/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "bytes "

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v12, "-"

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v12, 0x1

    sub-long v12, v3, v12

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v12

    if-eq v12, v6, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/sigmob/volley/toolbox/i;->d()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    iget-object v2, v1, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The Content-Range Header is invalid Assume["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "] vs Real["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "], has remove the temporary file ["

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v1, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "]."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    :goto_0
    move-wide v10, v7

    goto :goto_1

    :cond_4
    iget-object v0, v1, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v10

    :cond_5
    :goto_1
    const/4 v5, 0x0

    :try_start_0
    new-instance v14, Ljava/io/RandomAccessFile;

    iget-object v0, v1, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    const-string v12, "rw"

    invoke-direct {v14, v0, v12}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    if-eqz v2, :cond_6

    :try_start_1
    invoke-virtual {v14, v10, v11}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-wide v7, v10

    goto :goto_4

    :catchall_0
    move-exception v0

    move-object v2, v5

    :goto_2
    move-object v11, v14

    goto/16 :goto_d

    :catch_0
    move-exception v0

    :goto_3
    move-object v11, v14

    goto/16 :goto_b

    :cond_6
    :try_start_2
    invoke-virtual {v14, v7, v8}, Ljava/io/RandomAccessFile;->setLength(J)V

    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/sigmob/volley/toolbox/i;->d()Ljava/io/InputStream;

    move-result-object v2
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    const/16 v0, 0x2000

    :try_start_3
    new-array v5, v0, [B

    :goto_5
    invoke-virtual {v2, v5}, Ljava/io/InputStream;->read([B)I

    move-result v10

    if-eq v10, v6, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/sigmob/volley/toolbox/f;->m()Z

    move-result v0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v0, :cond_7

    :try_start_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;

    iget-object v5, v5, Lcom/sigmob/volley/toolbox/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " download  is cancel"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lcom/sigmob/volley/u;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_8

    :catch_1
    move-exception v0

    move-object v5, v2

    goto :goto_3

    :cond_7
    :try_start_5
    invoke-virtual {v14, v5, v9, v10}, Ljava/io/RandomAccessFile;->write([BII)V

    int-to-long v11, v10

    add-long/2addr v7, v11

    iget-object v0, v1, Lcom/sigmob/volley/toolbox/f;->g:Lcom/sigmob/volley/toolbox/f$a;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v0, :cond_8

    :try_start_6
    iget-object v12, v1, Lcom/sigmob/volley/toolbox/f;->g:Lcom/sigmob/volley/toolbox/f$a;

    iget-object v13, v1, Lcom/sigmob/volley/toolbox/f;->d:Lcom/sigmob/volley/toolbox/d;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-object v11, v14

    move-wide v14, v3

    move-wide/from16 v16, v7

    :try_start_7
    invoke-interface/range {v12 .. v17}, Lcom/sigmob/volley/toolbox/f$a;->a(Lcom/sigmob/volley/toolbox/d;JJ)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_7

    :catch_2
    move-exception v0

    goto :goto_6

    :catch_3
    move-exception v0

    move-object v11, v14

    :goto_6
    :try_start_8
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "callback downloadProgress  error "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v12, v9, [Ljava/lang/Object;

    invoke-static {v0, v12}, Lcom/sigmob/volley/u;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_7

    :cond_8
    move-object v11, v14

    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "recv: "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v12, " total: "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v12, " offset "

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v10, v9, [Ljava/lang/Object;

    invoke-static {v0, v10}, Lcom/sigmob/volley/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v14, v11

    goto/16 :goto_5

    :cond_9
    :goto_8
    move-object v11, v14

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "recv: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, " total: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, " offset "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/sigmob/volley/u;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-eqz v2, :cond_a

    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4

    goto :goto_9

    :catch_4
    const-string v0, "Error occured when calling consumingContent"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/sigmob/volley/u;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_a
    :goto_9
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V

    new-array v0, v9, [B

    return-object v0

    :catchall_1
    move-exception v0

    goto :goto_d

    :catch_5
    move-exception v0

    goto :goto_a

    :catchall_2
    move-exception v0

    goto/16 :goto_2

    :catch_6
    move-exception v0

    move-object v11, v14

    :goto_a
    move-object v5, v2

    goto :goto_b

    :catchall_3
    move-exception v0

    move-object v11, v14

    goto :goto_c

    :catchall_4
    move-exception v0

    move-object v2, v5

    move-object v11, v2

    goto :goto_d

    :catch_7
    move-exception v0

    move-object v11, v5

    :goto_b
    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    :catchall_5
    move-exception v0

    :goto_c
    move-object v2, v5

    :goto_d
    if-eqz v2, :cond_b

    :try_start_b
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_8

    goto :goto_e

    :catch_8
    const-string v2, "Error occured when calling consumingContent"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/sigmob/volley/u;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_b
    :goto_e
    if-eqz v11, :cond_c

    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V

    :cond_c
    throw v0
.end method

.method public c()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/sigmob/volley/toolbox/f;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sigmob/volley/toolbox/f;->e:J

    const-string v1, "Connection"

    const-string v2, "Keep-Alive"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Accept-Encoding"

    const-string v2, "gzip"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "Range"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bytes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/sigmob/volley/toolbox/f;->e:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Range,bytes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/sigmob/volley/toolbox/f;->e:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sigmob/volley/toolbox/f;->a(Ljava/lang/String;)V

    return-object v0
.end method

.method public l()V
    .locals 2

    invoke-super {p0}, Lcom/sigmob/volley/m;->l()V

    iget-object v0, p0, Lcom/sigmob/volley/toolbox/f;->f:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/sigmob/volley/toolbox/f;->g:Lcom/sigmob/volley/toolbox/f$a;

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public r()Lcom/sigmob/volley/m$b;
    .locals 1

    sget-object v0, Lcom/sigmob/volley/m$b;->a:Lcom/sigmob/volley/m$b;

    return-object v0
.end method
