.class public final enum Lcom/sigmob/sdk/base/views/d;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/sdk/base/views/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sigmob/sdk/base/views/d;

.field public static final enum b:Lcom/sigmob/sdk/base/views/d;

.field public static final enum c:Lcom/sigmob/sdk/base/views/d;

.field public static final enum d:Lcom/sigmob/sdk/base/views/d;

.field public static final enum e:Lcom/sigmob/sdk/base/views/d;

.field public static final enum f:Lcom/sigmob/sdk/base/views/d;

.field private static final synthetic i:[Lcom/sigmob/sdk/base/views/d;


# instance fields
.field private final g:Ljava/lang/String;

.field private h:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/sigmob/sdk/base/views/d;

    const-string v1, "LOADING"

    const-string v2, "iVBORw0KGgoAAAANSUhEUgAAAHsAAAB7CAYAAABUx/9/AAACfklEQVR42u3dUW7qQAwFUPa/A9bCWlgL/asQEnrolbE9c8+V+Gyi+GDiSVLlcgnL4yUXyYAGHgYNHLbAFtgCW2Bn5n6/P54/p2Jfr9fH8yceejV4F/YrdBz4O+iV4B3Y76CjwGHDXgpejf0vaNgLwSuxP4F23g7CNpEvBK/CBg0bdiV4BTboIcPaamxD2aDunoB9kZruXomtq4d1dzc24ULwVdigYcPuBF+BDXrosPZtbEPZ4O7uwCbZ1N3fxNbVG3e3rn7J7XZ7PH9OAd8Vetk+X6GrwDueaNkBe9l+30FXoe+GvfUX7RPsleiwPx8IS7FXoU+BXv4z+gfkNuwK8O6BdSJ02Xm7a4g7Lf+LXDKRQ+9HXjYYQg9ABh4IDT0MGXogMvBAaMu0DZZT09Bhb36LFHbhte2dwNOxjzpoA1rg40uWXp5RExERERERERERGZy/3lz3b6yb1L1qh8B7oH/rXr1D4D3QsGHDhg17f2wDWtCAZullySsiIiIiIiIiIiJtSb2UF3XcydduY449/WJ9zPG7MxNQA7fhAmrhnmtATdxcD6kP6IA6QQ6oG+SQOoIOqCfkEHTIIcu0KcjTvkRHvuppInQ3+LEvcev+yZ52mjj69Yzd52XYxRbTXh08GXvrVyp3DkgrCvvtvz/uZelTlyBdHWHJ2TCQdJ1rXWPYtKt192Fd3YUNvKljOrYJu6l4XdsFDhv0yqJ1bht48bDTjQ28sDO6tw+7sCsmYMeDVxVowj6isSuLM2U/seCwQS8pyqR9RYF3DDPTsGPAJz202PmwIOyDHlyEDdt5e3UBugpvIn9TiNN+TTqOU/ykwoYNW2ALbIEtYWv7afkBhefbcgmFrrkAAAAASUVORK5CYII="

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/sigmob/sdk/base/views/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/views/d;->a:Lcom/sigmob/sdk/base/views/d;

    new-instance v0, Lcom/sigmob/sdk/base/views/d;

    const-string v1, "SOUND_MUTE"

    const-string v2, "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAAPFBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADQLyYwAAAAE3RSTlMAgEBWq8EoaOgK05qR3hXzonY2pCHkzQAAAVNJREFUWMPt1sluwzAMRVFqlueB//+vrYUYD4oXJVl0U+QtE98TIIkh0z9fDiWTfUPk76X8i75tNApr4teiqc+ptWFk5sHSXyHvRI6Zvb23Audy9RNZAV/vHoC+n8kKhA29HBi8u8fo5UCp3M2RDgj83uuA4dErgczMi7sXMmkB//apfwCsZX8oIQU5EJg5PN7YVjHgGQJeZzlAEQJ6R2IAQtdrAAjoNQAE9CoAAnodAAG9CoCAXgtAcPZ7YcRJYwIctwUDgB6CBkDvIgQVgN8fggZAD0EDoIegA9BDkAPoe0EOePQEocqB8riiCVkM5Gl5XDDxRAJAfDIlf48w29lY56wFqHC35dQCFHuhei1A/kj3LmELPaDagTveAOAbKUYATzyHHSDfBGcH6GzCbAcob5cw2QFa6yXsHjeddsPCryUybsQhYFzCH8K4MMeS6bOf9gXo1jBFNvmRhAAAAABJRU5ErkJggg=="

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sigmob/sdk/base/views/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/views/d;->b:Lcom/sigmob/sdk/base/views/d;

    new-instance v0, Lcom/sigmob/sdk/base/views/d;

    const-string v1, "SOUND_ON"

    const-string v2, "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAAP1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACzJYIvAAAAFHRSTlMAgUC/cVgr8OBMEdIbnMiPN7Jkqf9iVGcAAAIXSURBVFjD7ZbblqsgEEQbkavgLfz/t550G20mcCbCPM7Ug8ssrS1UNwT40/cS0shR9/u3hJp9p93LdOjR5w/kn83zEuCQEkODfz4+Pjyv4yuR5228m4ie0B+BACKPRNzyrxbfXeALwFkiyNDgZwAqRCKYj9MYLI+VASg3E2L93u8M+zMAJ8l1qUol1Ah1AOz41Priq9EkFvtLADh8vgGLB8baoQCwhjNhlir8BcBnTUjFcG+rhmWlggKgUjLxYmAtpjwG8/xdKw0DZMrHvZq3SWB/1f08AtLmuRQmtABAzHkfe4uVagCgVkpq0oBacKm3ALjY8qBRR7cAmDBehVuaARCvNh5xOu0AP51D0FiHdgCMV3hYh9AO8NhCVx3WJgB38XAGqjoAC/pOgOgA7GeKAm86AC4H7J0jUD/JIGKI543rKKM9yygPUn8jYU/6dgC18rWL/WQx6a7V6Gg5c0O1AvbEL9LBow2wxmxLU3T6aAG4R2L/sSG5FoBIX/w7VbMFsJFfvvyBd3UekJUvCV0CKH97WZarmvU/182XGQxizdqREshFCbFkqJax7AaWmNjOYdUBiRIspIeXKHC7/g+gE8pDXTxF4+oA9emYxi+pCkDHewdFR4TxHRCEofnfOaoSQVSPuvGjm6NaysP2pOCegqGv5YAFbz3cFg1YSt44hjEO0KKrtwJ0ak5czz5Jk9ID/f3S8Av0DxRSQcrjlRWPAAAAAElFTkSuQmCC"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/sigmob/sdk/base/views/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/views/d;->c:Lcom/sigmob/sdk/base/views/d;

    new-instance v0, Lcom/sigmob/sdk/base/views/d;

    const-string v1, "CLOSE"

    const-string v2, "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAAM1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACjBUbJAAAAEHRSTlMAqyjUgcO3FcwGno4dDZZmlq1c/AAAAQZJREFUWMPt1sFuwjAQANHaJIRAaP3/X1tFDZqlUsA7cGSPq8yT4kOcr8+8bYZxSD1fL/fBobVWMv0aBKG2dcqc6NeJAEJ/38LqjNDf/8Rl+dsd5/7+/tTnm3BN9EKgv7BKCPTfrBCOmzA978+sdoRMz1w3YZwe94VVSqDfnwlB9VFI9gjjJiyuj4Lqo1Bdj3CquZ5ZEFQfBdWvDUKuRzjdBNcjHOilQG8E33P69F7w/etvgCB7BN2XgiDPzwjx+43geiPE+8sIsVcC9y+DIHsE2yO4/48o2B7B9gi2R7A9gu0RbI9gewTbI+z1ThjplVDpMwKLhT4jxFeg7xT+B8PQ3RN8Zptfb3gqESeo0NkAAAAASUVORK5CYII="

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/sigmob/sdk/base/views/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/views/d;->d:Lcom/sigmob/sdk/base/views/d;

    new-instance v0, Lcom/sigmob/sdk/base/views/d;

    const-string v1, "GRAYCLOSE"

    const-string v2, "iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAAAJ1BMVEVHcEw3NzdWVlZRUVFVVVXJycnv7++ampqXl5fMzMz////6+vr6+vqCBLknAAAADXRSTlMAARAeMnK8S0p1/+XjZ7RwtQAAAKBJREFUeNqF00EOAyEIBdABrFj1/udtlCk/1KmykfgiJIqXB7GkESJMvuk4zEMoKoMs+O9RFIBuHBp97bv2p5m+smK/5PdcyUvn2tS11fwtT7alzb0gJesMR2LdJQUPmsQYRU3Bc4FPRQyG9woFo/2O9aG4BNUSXMA3BJf1WkqH80Xh7I/T45N0PMn2QffjcBim8yjCVz1/g/Mnwhc0Y+AHZ9YN/vFQrGwAAAAASUVORK5CYII="

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/sigmob/sdk/base/views/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/views/d;->e:Lcom/sigmob/sdk/base/views/d;

    new-instance v0, Lcom/sigmob/sdk/base/views/d;

    const-string v1, "APPICON"

    const-string v2, "iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAMAAABiM0N1AAAAeFBMVEVHcEwuo5gMKSYvpZpKrKMEEREEEQ8edWUlpJgTQzwhkIY5o5kgfXUhkogfVVA6qZ4voZYtnZM4n5YeamM0k4ompZkln5Qqp5tUs6owqZ0+sKQkm5BJtKkqoJVYurA8p5xmvbV6xr/J5+SW0cu23dr9/f3s9vXb7+2tVwSjAAAAFXRSTlMA/kLq/hooA/0P6kS1JGG8pnDhg75YgU7hAAAEvElEQVR4XqXYiZKqOBQG4EEQAy6g9snCvqj9/m84OZzQIeFOo3d+u5RqU1/9WbS1/9nMYcrGoG3DAf8XckjOOsnhL7FJSY7XPLuFAWNBGGb59Zig9Qk1KdcsKoqikfquwoumKKLsOlkftDnmIQjBGKsKBsCYYEXFQAgR5sd3WyGTNQq0AgBVAZSiAvqVypB6p845B2gUKj4EaKkG8jOO26pzZcCYbrSGKBoCdvVKrZ0kA2AAv0MMIEtcyXfOITAGWxCOCc9WWjtHgXW2IWzNjlZaOY1iYkqjBKUqBKWoBEU1AqMHWmnl9I1J3/kXXb++IGm9PkJRke1G5iKgdfKcJATWCIA31ogiGnHDvfOhDJj4EGLwhZDrXIHBp5Bg/EqSdc7sryAoaZkslGvncwjcyeHOo/MXEAB3z0C2hIQqfoO6QlnIVLJHkWLO2vAafjmQw+uxHC2pEkG5EgrTKUzzfD4fhZoiK0WppHkc9LPVYnSQI2TPop01qG+U1Dw1oQExT009tPMt7GgG6Z4kc4YWEHRW6vrxgRn7Dp1icipnNL/+QJkHQTNJRTUOr+ec19BX5Eiw0LTc8/tuEvmQ6BEYNOfke0CwFx6UJvOeFai456iziIc17jkyR4mWqGArqBr+AxoqH2JwJeiQ/wGyjp/HGsppkQ5ZU5n05rGzzjpD542Wmdm1W1OYdPTg9/ElOY82udGniyT0pzY+f8/o7BoToT6S+nYOPKh5bqVZQKChs4Hc7S/miT3Gb3fnx/mZoXCgNv4jNM5OXUZL6SXL+mGkkfvQempqrjGWZbuEhrYsx7lc4Ewtng7S3l3s7jX3j+r+uWzUBz8NXx1fQnt6qd2WkHjYDtjHoV524ziHH+iWEJQtz1GFg7fzitq2K0wyA+XLk90930sXReaERzJP6LXmvGjfhngtawY6rL4fJihx3kb6N6EeIT5J9cW8aPfh4o1tfBMaJ0hLDNJ4/pKQL6D+gRkef459jhppicEpmaHrAmIlJqpLStSaizYyF4HkFII443cNkRQv/xwxB2qXEMdb7UE0M2duBAH7LwjDud+InxIL7RSlo4e6lkE9UXXU1pQ2wvsgCFoZUFSj8L68EET7ljl/+6XsZETpmkhOt66TJr178bUnhypdaufNv5beYnM9NU5rFNCMdLumrjlQIVvp5H7Qks4acVojzrm/RoyfqJBdpdL76LduNDELSEjgkO6okK105y7EbCN0EELHbcTKOxWySeKT//H4pxGfGxmonhvpicUJAs7k4tT/wM4XjbidGkKANwlpjBNzk+x3qdMIJduIrxvVTbrbUyFPupT+d5HaOZAUhYcVasFUdCHHy0FLnf/tSM7n0F40fSMl/lhnJcWXVAgwjShRyzFOIyzEIL3YBVpLu5R7X0VbetF7a8R4unMdf3a7UwmaslBBm2UhJTmD8rTbe44vxfeUA5MWAgIMBLoR8PQe++vjR0tYyvkHgoUwQuo6mw6VupyiAJgOQWAhxsvy6xKbaW1LmkpLAI1VBYIE6YcyPV1irIPQdpDa3bXFgb4cA+jtL7Vy38Vune1WaF3up6/b7RaG+u7rdL+g4jHbFFlao2iDFGI+tTQ2cXtMgop1PuYwbxT5F5RDYLHX81i6AAAAAElFTkSuQmCC"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lcom/sigmob/sdk/base/views/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/base/views/d;->f:Lcom/sigmob/sdk/base/views/d;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sigmob/sdk/base/views/d;

    sget-object v1, Lcom/sigmob/sdk/base/views/d;->a:Lcom/sigmob/sdk/base/views/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/sdk/base/views/d;->b:Lcom/sigmob/sdk/base/views/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sigmob/sdk/base/views/d;->c:Lcom/sigmob/sdk/base/views/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sigmob/sdk/base/views/d;->d:Lcom/sigmob/sdk/base/views/d;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sigmob/sdk/base/views/d;->e:Lcom/sigmob/sdk/base/views/d;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sigmob/sdk/base/views/d;->f:Lcom/sigmob/sdk/base/views/d;

    aput-object v1, v0, v8

    sput-object v0, Lcom/sigmob/sdk/base/views/d;->i:[Lcom/sigmob/sdk/base/views/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/sigmob/sdk/base/views/d;->g:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/sdk/base/views/d;
    .locals 1

    const-class v0, Lcom/sigmob/sdk/base/views/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/sdk/base/views/d;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/sdk/base/views/d;
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/base/views/d;->i:[Lcom/sigmob/sdk/base/views/d;

    invoke-virtual {v0}, [Lcom/sigmob/sdk/base/views/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/sdk/base/views/d;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 3

    iget-object v0, p0, Lcom/sigmob/sdk/base/views/d;->h:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/base/views/d;->g:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sigmob/sdk/base/views/d;->h:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/sigmob/sdk/base/views/d;->h:Landroid/graphics/Bitmap;

    return-object v0
.end method
