.class public Lcom/sigmob/sdk/base/views/SkipButtonWidget;
.super Landroid/widget/RelativeLayout;


# instance fields
.field a:I

.field b:Z

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->a:I

    iput-boolean v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->b:Z

    const/high16 v0, 0x41f00000    # 30.0f

    invoke-static {v0, p1}, Lcom/sigmob/sdk/common/f/d;->c(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->a:I

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->a:I

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->a:I

    iget v1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->a:I

    invoke-direct {p1, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    :cond_0
    iget-object v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->d:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->d:Landroid/widget/TextView;

    invoke-static {}, Lcom/sigmob/sdk/common/a;->an()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setId(I)V

    iget-object v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->d:Landroid/widget/TextView;

    const/4 v2, 0x1

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1}, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget p1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->a:I

    div-int/lit8 p1, p1, 0x2

    const-string v0, "#ffffff"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    int-to-float p1, p1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    const/16 p1, 0x66

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/GradientDrawable;->setAlpha(I)V

    invoke-virtual {p0, v1}, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string p1, "skip"

    invoke-virtual {p0, p1}, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    iput p1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->e:I

    iget-boolean v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->b:Z

    if-eqz v0, :cond_1

    if-lez p1, :cond_2

    const/16 v0, 0x9

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->getMeasuredWidthAndState()I

    move-result p1

    if-lez p1, :cond_0

    const/16 v0, 0x3e8

    if-ge p1, v0, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-virtual {p0}, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->getMeasuredWidth()I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    :cond_0
    iget-object p1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget v1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/sigmob/sdk/base/b;->d([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    if-lez p1, :cond_2

    iget-object v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->d:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->b:Z

    return v0
.end method

.method public b()V
    .locals 7

    iget-boolean v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->b:Z

    iget-boolean v1, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "show skip widget"

    invoke-static {v1}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    iget v3, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->a:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v2, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->d:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    iget v4, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->a:I

    int-to-float v4, v4

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    iget v6, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->a:I

    int-to-float v6, v6

    div-float/2addr v6, v5

    float-to-int v5, v6

    invoke-virtual {v2, v4, v3, v5, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    iget v2, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->e:I

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    new-array v0, v0, [Ljava/lang/Object;

    iget v4, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    invoke-static {v0}, Lcom/sigmob/sdk/base/b;->d([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/sigmob/sdk/base/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1}, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public getTime()I
    .locals 1

    iget v0, p0, Lcom/sigmob/sdk/base/views/SkipButtonWidget;->e:I

    return v0
.end method
