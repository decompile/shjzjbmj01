.class public Lcom/sigmob/sdk/base/services/j;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sigmob/sdk/base/services/j$a;
    }
.end annotation


# direct methods
.method public static a(Ljava/lang/String;)Lcom/sigmob/sdk/base/services/j$a;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "DownloadService"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x4

    goto :goto_1

    :sswitch_1
    const-string v0, "LocationService"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v0, "AppInstallService"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v0, "networkMonitor"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_1

    :sswitch_4
    const-string v0, "WifiScanService"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x3

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p0, -0x1

    :goto_1
    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    return-object v0

    :pswitch_0
    new-instance p0, Lcom/sigmob/sdk/base/services/d;

    invoke-direct {p0}, Lcom/sigmob/sdk/base/services/d;-><init>()V

    return-object p0

    :pswitch_1
    new-instance p0, Lcom/sigmob/sdk/base/services/l;

    invoke-direct {p0}, Lcom/sigmob/sdk/base/services/l;-><init>()V

    return-object p0

    :pswitch_2
    new-instance p0, Lcom/sigmob/sdk/base/services/b;

    invoke-direct {p0}, Lcom/sigmob/sdk/base/services/b;-><init>()V

    return-object p0

    :pswitch_3
    new-instance p0, Lcom/sigmob/sdk/base/services/e;

    invoke-direct {p0}, Lcom/sigmob/sdk/base/services/e;-><init>()V

    return-object p0

    :pswitch_4
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x39ad5cfd -> :sswitch_4
        -0x1bdb34b4 -> :sswitch_3
        0x15187cfb -> :sswitch_2
        0x32f35120 -> :sswitch_1
        0x526324ed -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
