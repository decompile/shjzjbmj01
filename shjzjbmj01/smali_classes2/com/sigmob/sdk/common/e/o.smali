.class public Lcom/sigmob/sdk/common/e/o;
.super Lcom/sigmob/sdk/common/e/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sigmob/sdk/common/e/o$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sigmob/sdk/common/e/k<",
        "Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lcom/sigmob/sdk/common/e/o$a;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private e:Lcom/sigmob/sdk/common/models/sigdsp/pb/Network$Builder;

.field private f:Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;

.field private g:Lcom/sigmob/sdk/common/models/sigdsp/pb/Device$Builder;

.field private h:Lcom/sigmob/sdk/common/models/sigdsp/pb/App$Builder;

.field private final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/common/e/o$a;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sigmob/sdk/common/e/k;-><init>(Ljava/lang/String;ILcom/sigmob/volley/o$a;)V

    invoke-static {p5}, Lcom/sigmob/sdk/common/f/m$a;->a(Ljava/lang/Object;)Z

    iput-object p3, p0, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/sigmob/sdk/common/e/o;->b:Lcom/sigmob/sdk/common/e/o$a;

    iput p2, p0, Lcom/sigmob/sdk/common/e/o;->c:I

    iput-object p4, p0, Lcom/sigmob/sdk/common/e/o;->i:Ljava/lang/String;

    new-instance p1, Lcom/sigmob/volley/d;

    const/4 p2, 0x0

    const/16 p3, 0xbb8

    const/4 p4, 0x0

    invoke-direct {p1, p3, p2, p4}, Lcom/sigmob/volley/d;-><init>(IIF)V

    invoke-virtual {p0, p1}, Lcom/sigmob/sdk/common/e/o;->a(Lcom/sigmob/volley/q;)Lcom/sigmob/volley/m;

    invoke-virtual {p0, p2}, Lcom/sigmob/sdk/common/e/o;->a(Z)Lcom/sigmob/volley/m;

    return-void
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/common/e/o$a;)V
    .locals 8

    invoke-static {}, Lcom/sigmob/sdk/common/e/f;->a()Lcom/sigmob/sdk/common/e/l;

    move-result-object v0

    const v1, 0x92824

    if-nez v0, :cond_1

    if-eqz p4, :cond_0

    const-string p0, "RequestQueue is null"

    invoke-interface {p4, p2, p1, v1, p0}, Lcom/sigmob/sdk/common/e/o$a;->onErrorResponse(Ljava/lang/String;IILjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/sdk/common/e/m;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p4, :cond_2

    const-string p0, "can\'t connection server"

    invoke-interface {p4, p2, p1, v1, p0}, Lcom/sigmob/sdk/common/e/o$a;->onErrorResponse(Ljava/lang/String;IILjava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    new-instance v0, Lcom/sigmob/sdk/common/e/o;

    move-object v2, v0

    move-object v3, p0

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v2 .. v7}, Lcom/sigmob/sdk/common/e/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/common/e/o$a;)V

    invoke-static {}, Lcom/sigmob/sdk/common/e/f;->a()Lcom/sigmob/sdk/common/e/l;

    move-result-object p0

    invoke-virtual {p0, v0}, Lcom/sigmob/sdk/common/e/l;->a(Lcom/sigmob/volley/m;)Lcom/sigmob/volley/m;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    if-eqz p4, :cond_4

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p4, p2, p1, v1, p0}, Lcom/sigmob/sdk/common/e/o$a;->onErrorResponse(Ljava/lang/String;IILjava/lang/String;)V

    :cond_4
    return-void
.end method

.method private e()V
    .locals 2

    invoke-static {}, Lcom/sigmob/sdk/common/models/ModelBuilderCreator;->createApp()Lcom/sigmob/sdk/common/models/sigdsp/pb/App$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sigmob/sdk/common/e/o;->h:Lcom/sigmob/sdk/common/models/sigdsp/pb/App$Builder;

    invoke-static {}, Lcom/sigmob/sdk/common/models/ModelBuilderCreator;->createDevice()Lcom/sigmob/sdk/common/models/sigdsp/pb/Device$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sigmob/sdk/common/e/o;->g:Lcom/sigmob/sdk/common/models/sigdsp/pb/Device$Builder;

    invoke-static {}, Lcom/sigmob/sdk/common/models/ModelBuilderCreator;->createDeviceId()Lcom/sigmob/sdk/common/models/sigdsp/pb/DeviceId$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sigmob/sdk/common/e/o;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sigmob/sdk/common/e/o;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sigmob/sdk/common/models/sigdsp/pb/DeviceId$Builder;->user_id(Ljava/lang/String;)Lcom/sigmob/sdk/common/models/sigdsp/pb/DeviceId$Builder;

    :cond_0
    iget-object v1, p0, Lcom/sigmob/sdk/common/e/o;->g:Lcom/sigmob/sdk/common/models/sigdsp/pb/Device$Builder;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/DeviceId$Builder;->build()Lcom/sigmob/sdk/common/models/sigdsp/pb/DeviceId;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/Device$Builder;->did(Lcom/sigmob/sdk/common/models/sigdsp/pb/DeviceId;)Lcom/sigmob/sdk/common/models/sigdsp/pb/Device$Builder;

    invoke-static {}, Lcom/sigmob/sdk/common/models/ModelBuilderCreator;->createNetwork()Lcom/sigmob/sdk/common/models/sigdsp/pb/Network$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sigmob/sdk/common/e/o;->e:Lcom/sigmob/sdk/common/models/sigdsp/pb/Network$Builder;

    invoke-static {}, Lcom/sigmob/sdk/common/models/ModelBuilderCreator;->createAdSlot()Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sigmob/sdk/common/e/o;->f:Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;

    iget-object v0, p0, Lcom/sigmob/sdk/common/e/o;->f:Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;

    iget-object v0, v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;->adslot_type:Ljava/util/List;

    iget v1, p0, Lcom/sigmob/sdk/common/e/o;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sigmob/sdk/common/e/o;->f:Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;

    iget-object v1, p0, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;->adslot_id(Ljava/lang/String;)Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;

    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Lcom/sigmob/volley/j;)Lcom/sigmob/volley/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sigmob/volley/j;",
            ")",
            "Lcom/sigmob/volley/o<",
            "Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;",
            ">;"
        }
    .end annotation

    :try_start_0
    sget-object v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->ADAPTER:Lcom/sigmob/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/sigmob/volley/j;->b:[B

    invoke-virtual {v0, v1}, Lcom/sigmob/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;

    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object v1

    iget-object v2, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->uid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sigmob/sdk/common/a;->b(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sigmob/volley/toolbox/h;->a(Lcom/sigmob/volley/j;)Lcom/sigmob/volley/b$a;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/sigmob/volley/o;->a(Ljava/lang/Object;Lcom/sigmob/volley/b$a;)Lcom/sigmob/volley/o;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lcom/sigmob/volley/l;

    invoke-direct {v0, p1}, Lcom/sigmob/volley/l;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcom/sigmob/volley/o;->a(Lcom/sigmob/volley/t;)Lcom/sigmob/volley/o;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "application/octet-stream"

    return-object v0
.end method

.method protected a(Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;)V
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "strategy response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    :try_start_0
    iget-object v3, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->strategy:Ljava/util/List;

    if-eqz v3, :cond_8

    iget-object v3, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->strategy:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_8

    iget-object v3, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->strategy:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->strategy:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_7

    :try_start_1
    iget-object v6, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->strategy:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    iget-object v7, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->options:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    if-lez v7, :cond_0

    :try_start_2
    iget-object v7, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->options:Ljava/util/Map;

    invoke-interface {v10, v7}, Ljava/util/Map;->putAll(Ljava/util/Map;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :try_start_3
    const-string v7, ""

    const-string v8, "appId"

    invoke-interface {v10, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    if-eqz v8, :cond_2

    :try_start_4
    const-string v7, "appId"

    invoke-interface {v10, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    :cond_1
    :goto_1
    move-object v12, v7

    goto :goto_2

    :cond_2
    :try_start_5
    iget-object v8, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->name:Ljava/lang/String;

    const-string v9, "sigmob"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    if-eqz v8, :cond_1

    :try_start_6
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sigmob/sdk/common/a;->R()Ljava/lang/String;

    move-result-object v7
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    :goto_2
    :try_start_7
    const-string v7, ""

    iget-object v8, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->channel_id:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0

    if-nez v8, :cond_3

    :try_start_8
    iget-object v7, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->channel_id:Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2

    :cond_3
    move-object v11, v7

    :try_start_9
    const-string v7, ""

    const-string v8, "apiKey"

    invoke-interface {v10, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0

    if-eqz v8, :cond_4

    :try_start_a
    const-string v7, "apiKey"

    invoke-interface {v10, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2

    :cond_4
    move-object v13, v7

    :try_start_b
    const-string v7, ""

    const-string v8, "placementId"

    invoke-interface {v10, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0

    if-eqz v8, :cond_6

    :try_start_c
    const-string v7, "placementId"

    invoke-interface {v10, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2

    :cond_5
    :goto_3
    move-object v15, v7

    goto :goto_4

    :cond_6
    :try_start_d
    iget-object v8, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->name:Ljava/lang/String;

    const-string v9, "sigmob"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_0

    if-eqz v8, :cond_5

    :try_start_e
    iget-object v7, v1, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2

    goto :goto_3

    :goto_4
    :try_start_f
    new-instance v14, Lcom/sigmob/sdk/common/models/ADStrategy;

    iget-object v8, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->adapter:Ljava/lang/String;

    iget-object v9, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->name:Ljava/lang/String;

    iget v7, v1, Lcom/sigmob/sdk/common/e/o;->c:I

    iget-object v2, v1, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    move/from16 v18, v3

    iget-object v3, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->ad_expire_time:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move v3, v7

    move-object v7, v14

    move-object v1, v14

    move v14, v3

    move-object/from16 v16, v2

    invoke-direct/range {v7 .. v17}, Lcom/sigmob/sdk/common/models/ADStrategy;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    iget-object v2, v6, Lcom/sigmob/sdk/common/models/ssp/pb/Strategy;->enable_extra_close_callback:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sigmob/sdk/common/models/ADStrategy;->setExtraCloseCallBack(Z)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    move/from16 v3, v18

    move-object/from16 v1, p0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object/from16 v2, p0

    goto :goto_5

    :cond_7
    new-instance v1, Lcom/sigmob/sdk/common/e/n;

    invoke-direct {v1}, Lcom/sigmob/sdk/common/e/n;-><init>()V

    iget-object v2, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->max_concurrent_operation_count:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/sigmob/sdk/common/e/n;->a:I

    iget-object v0, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->single_channel_timeout:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Lcom/sigmob/sdk/common/e/n;->b:I
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_0

    move-object/from16 v2, p0

    :try_start_10
    iget-object v0, v2, Lcom/sigmob/sdk/common/e/o;->b:Lcom/sigmob/sdk/common/e/o$a;

    iget-object v3, v2, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    invoke-interface {v0, v4, v1, v3}, Lcom/sigmob/sdk/common/e/o$a;->onSuccess(Ljava/util/List;Lcom/sigmob/sdk/common/e/n;Ljava/lang/String;)V

    goto :goto_6

    :cond_8
    move-object v2, v1

    iget-object v1, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->code:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_9

    iget-object v0, v2, Lcom/sigmob/sdk/common/e/o;->b:Lcom/sigmob/sdk/common/e/o$a;

    iget-object v1, v2, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    iget v3, v2, Lcom/sigmob/sdk/common/e/o;->c:I

    const-string v4, "strategyResponse code 0"

    const v5, 0x92827

    invoke-interface {v0, v1, v3, v5, v4}, Lcom/sigmob/sdk/common/e/o$a;->onErrorResponse(Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_6

    :cond_9
    iget-object v1, v2, Lcom/sigmob/sdk/common/e/o;->b:Lcom/sigmob/sdk/common/e/o$a;

    iget-object v3, v2, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    iget v4, v2, Lcom/sigmob/sdk/common/e/o;->c:I

    iget-object v5, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->code:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->error_message:Ljava/lang/String;

    invoke-interface {v1, v3, v4, v5, v6}, Lcom/sigmob/sdk/common/e/o$a;->onErrorResponse(Ljava/lang/String;IILjava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v2, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " ERROR_SIGMOB_STRATEGY "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;->code:Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_1

    goto :goto_6

    :catch_1
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    move-object v2, v1

    :goto_5
    const-string v1, "strategy exception "

    invoke-static {v1, v0}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, v2, Lcom/sigmob/sdk/common/e/o;->b:Lcom/sigmob/sdk/common/e/o$a;

    iget-object v3, v2, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    iget v4, v2, Lcom/sigmob/sdk/common/e/o;->c:I

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    const v5, 0x92827

    invoke-interface {v1, v3, v4, v5, v0}, Lcom/sigmob/sdk/common/e/o$a;->onErrorResponse(Ljava/lang/String;IILjava/lang/String;)V

    :goto_6
    return-void
.end method

.method public a(Lcom/sigmob/volley/t;)V
    .locals 4

    iget-object v0, p0, Lcom/sigmob/sdk/common/e/o;->b:Lcom/sigmob/sdk/common/e/o$a;

    iget-object v1, p0, Lcom/sigmob/sdk/common/e/o;->d:Ljava/lang/String;

    iget v2, p0, Lcom/sigmob/sdk/common/e/o;->c:I

    invoke-virtual {p1}, Lcom/sigmob/volley/t;->getMessage()Ljava/lang/String;

    move-result-object p1

    const v3, 0x92824

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/sigmob/sdk/common/e/o$a;->onErrorResponse(Ljava/lang/String;IILjava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;

    invoke-virtual {p0, p1}, Lcom/sigmob/sdk/common/e/o;->a(Lcom/sigmob/sdk/common/models/ssp/pb/StrategyResponse;)V

    return-void
.end method

.method public b()[B
    .locals 5

    invoke-direct {p0}, Lcom/sigmob/sdk/common/e/o;->e()V

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/sigmob/sdk/common/models/ModelBuilderCreator;->createBidRequest()Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sigmob/sdk/common/e/o;->h:Lcom/sigmob/sdk/common/models/sigdsp/pb/App$Builder;

    invoke-virtual {v2}, Lcom/sigmob/sdk/common/models/sigdsp/pb/App$Builder;->build()Lcom/sigmob/sdk/common/models/sigdsp/pb/App;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;->app(Lcom/sigmob/sdk/common/models/sigdsp/pb/App;)Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;

    iget-object v2, v1, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;->slots:Ljava/util/List;

    iget-object v3, p0, Lcom/sigmob/sdk/common/e/o;->f:Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;

    invoke-virtual {v3}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot$Builder;->build()Lcom/sigmob/sdk/common/models/sigdsp/pb/AdSlot;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/sigmob/sdk/common/e/o;->g:Lcom/sigmob/sdk/common/models/sigdsp/pb/Device$Builder;

    invoke-virtual {v2}, Lcom/sigmob/sdk/common/models/sigdsp/pb/Device$Builder;->build()Lcom/sigmob/sdk/common/models/sigdsp/pb/Device;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;->device(Lcom/sigmob/sdk/common/models/sigdsp/pb/Device;)Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;

    iget-object v2, p0, Lcom/sigmob/sdk/common/e/o;->e:Lcom/sigmob/sdk/common/models/sigdsp/pb/Network$Builder;

    invoke-virtual {v2}, Lcom/sigmob/sdk/common/models/sigdsp/pb/Network$Builder;->build()Lcom/sigmob/sdk/common/models/sigdsp/pb/Network;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;->network(Lcom/sigmob/sdk/common/models/sigdsp/pb/Network;)Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;

    iget-object v2, v1, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;->options:Ljava/util/Map;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, v1, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;->options:Ljava/util/Map;

    :cond_0
    iget-object v2, v1, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;->options:Ljava/util/Map;

    const-string v3, "gdpr_consent_status"

    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    invoke-static {}, Lcom/sigmob/sdk/common/a;->V()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest$Builder;->build()Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    const-string v1, "builder Ads Post entry fail "

    invoke-static {v1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/e/o;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " send strategy request: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/models/sigdsp/pb/BidRequest;->encode()[B

    move-result-object v0

    return-object v0
.end method
