.class public final enum Lcom/sigmob/sdk/common/e/i;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/sdk/common/e/i;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sigmob/sdk/common/e/i;

.field public static final enum b:Lcom/sigmob/sdk/common/e/i;

.field public static final enum c:Lcom/sigmob/sdk/common/e/i;

.field private static final synthetic e:[Lcom/sigmob/sdk/common/e/i;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/sigmob/sdk/common/e/i;

    const-string v1, "LOCATION"

    const-string v2, "Location"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/sigmob/sdk/common/e/i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/common/e/i;->a:Lcom/sigmob/sdk/common/e/i;

    new-instance v0, Lcom/sigmob/sdk/common/e/i;

    const-string v1, "USER_AGENT"

    const-string v2, "User-Agent"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sigmob/sdk/common/e/i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/common/e/i;->b:Lcom/sigmob/sdk/common/e/i;

    new-instance v0, Lcom/sigmob/sdk/common/e/i;

    const-string v1, "ACCEPT_LANGUAGE"

    const-string v2, "Accept-Language"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/sigmob/sdk/common/e/i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/common/e/i;->c:Lcom/sigmob/sdk/common/e/i;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sigmob/sdk/common/e/i;

    sget-object v1, Lcom/sigmob/sdk/common/e/i;->a:Lcom/sigmob/sdk/common/e/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/sdk/common/e/i;->b:Lcom/sigmob/sdk/common/e/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sigmob/sdk/common/e/i;->c:Lcom/sigmob/sdk/common/e/i;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sigmob/sdk/common/e/i;->e:[Lcom/sigmob/sdk/common/e/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/sigmob/sdk/common/e/i;->d:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/sdk/common/e/i;
    .locals 1

    const-class v0, Lcom/sigmob/sdk/common/e/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/sdk/common/e/i;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/sdk/common/e/i;
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/common/e/i;->e:[Lcom/sigmob/sdk/common/e/i;

    invoke-virtual {v0}, [Lcom/sigmob/sdk/common/e/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/sdk/common/e/i;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sigmob/sdk/common/e/i;->d:Ljava/lang/String;

    return-object v0
.end method
