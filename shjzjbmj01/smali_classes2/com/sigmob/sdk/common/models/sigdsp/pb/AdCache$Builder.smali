.class public final Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;
.super Lcom/sigmob/wire/Message$Builder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sigmob/wire/Message$Builder<",
        "Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache;",
        "Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ad_type:Ljava/lang/Integer;

.field public crids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/Message$Builder;-><init>()V

    invoke-static {}, Lcom/sigmob/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;->crids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public ad_type(Ljava/lang/Integer;)Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;
    .locals 0

    iput-object p1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;->ad_type:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache;
    .locals 4

    new-instance v0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache;

    iget-object v1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;->ad_type:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;->crids:Ljava/util/List;

    invoke-super {p0}, Lcom/sigmob/wire/Message$Builder;->buildUnknownFields()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache;-><init>(Ljava/lang/Integer;Ljava/util/List;Lcom/sigmob/wire/okio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/sigmob/wire/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;->build()Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache;

    move-result-object v0

    return-object v0
.end method

.method public crids(Ljava/util/List;)Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;"
        }
    .end annotation

    invoke-static {p1}, Lcom/sigmob/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    iput-object p1, p0, Lcom/sigmob/sdk/common/models/sigdsp/pb/AdCache$Builder;->crids:Ljava/util/List;

    return-object p0
.end method
