.class public Lcom/sigmob/sdk/common/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/sdk/common/f/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sigmob/sdk/common/a$a;
    }
.end annotation


# static fields
.field public static a:Ljava/lang/String; = "2.22.1"

.field private static c:Ljava/lang/String; = "-1"

.field private static j:Ljava/lang/String; = "0"

.field private static l:I = 0x0

.field private static m:I = 0x0

.field private static n:Z = true

.field private static o:I

.field private static volatile p:Lcom/sigmob/sdk/common/a;

.field private static final r:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final b:I

.field private d:Landroid/location/Location;

.field private final e:Lcom/sigmob/sdk/common/f/g;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private k:Z

.field private final q:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/sigmob/sdk/common/a;->r:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    iput v0, p0, Lcom/sigmob/sdk/common/a;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sigmob/sdk/common/a;->f:Ljava/lang/String;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sigmob/sdk/common/a;->k:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    iget-object p1, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {p1}, Lcom/sigmob/sdk/common/f/o;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v1, "uid"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/sigmob/sdk/common/a;->h:Ljava/lang/String;

    new-instance p1, Lcom/sigmob/sdk/common/f/g;

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-direct {p1, v0, p0}, Lcom/sigmob/sdk/common/f/g;-><init>(Landroid/content/Context;Lcom/sigmob/sdk/common/f/g$a;)V

    iput-object p1, p0, Lcom/sigmob/sdk/common/a;->e:Lcom/sigmob/sdk/common/f/g;

    return-void
.end method

.method public static H()Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static L()Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->c()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static M()Ljava/lang/String;
    .locals 1

    sget-object v0, Landroid/os/Build;->BOARD:Ljava/lang/String;

    return-object v0
.end method

.method public static P()Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {}, Lcom/sigmob/devicehelper/DeviceHelper;->getVAID()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static V()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/common/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public static W()I
    .locals 1

    sget v0, Lcom/sigmob/sdk/common/a;->m:I

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sigmob/sdk/common/a;
    .locals 1

    sget-object p2, Lcom/sigmob/sdk/common/a;->p:Lcom/sigmob/sdk/common/a;

    if-nez p2, :cond_1

    const-class v0, Lcom/sigmob/sdk/common/a;

    monitor-enter v0

    :try_start_0
    sget-object p2, Lcom/sigmob/sdk/common/a;->p:Lcom/sigmob/sdk/common/a;

    if-nez p2, :cond_0

    new-instance p2, Lcom/sigmob/sdk/common/a;

    invoke-direct {p2, p0}, Lcom/sigmob/sdk/common/a;-><init>(Landroid/content/Context;)V

    sput-object p2, Lcom/sigmob/sdk/common/a;->p:Lcom/sigmob/sdk/common/a;

    sget-object p0, Lcom/sigmob/sdk/common/a;->p:Lcom/sigmob/sdk/common/a;

    iput-object p1, p0, Lcom/sigmob/sdk/common/a;->g:Ljava/lang/String;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    return-object p2
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/common/a;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "-1"

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/sigmob/sdk/common/a;->c:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public static a(Landroid/net/Uri;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/sigmob/sdk/common/a;->c:Ljava/lang/String;

    return-void
.end method

.method public static ab()Z
    .locals 1

    :try_start_0
    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->d()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method public static ac()Z
    .locals 1

    :try_start_0
    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->l()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method public static ai()Z
    .locals 1

    sget-boolean v0, Lcom/sigmob/sdk/common/a;->n:Z

    return v0
.end method

.method public static aj()Lcom/sigmob/sdk/common/a;
    .locals 2

    sget-object v0, Lcom/sigmob/sdk/common/a;->p:Lcom/sigmob/sdk/common/a;

    if-nez v0, :cond_0

    const-class v1, Lcom/sigmob/sdk/common/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sigmob/sdk/common/a;->p:Lcom/sigmob/sdk/common/a;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :goto_0
    return-object v0
.end method

.method public static an()I
    .locals 3

    :cond_0
    sget-object v0, Lcom/sigmob/sdk/common/a;->r:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    const v2, 0xffffff

    if-le v1, v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    sget-object v2, Lcom/sigmob/sdk/common/a;->r:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0
.end method

.method public static b()Z
    .locals 2

    invoke-static {}, Lcom/sigmob/sdk/common/a;->V()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sigmob/sdk/common/a;->ai()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "androidx.core.content.ContextCompat"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :catch_0
    :try_start_1
    const-string v1, "androidx.core.content.ContextCompat"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    :catch_1
    if-eqz v0, :cond_0

    :try_start_2
    new-instance v1, Lcom/sigmob/sdk/common/f/n$a;

    const-string v2, "checkSelfPermission"

    invoke-direct {v1, v0, v2}, Lcom/sigmob/sdk/common/f/n$a;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    const-class v0, Landroid/content/Context;

    invoke-virtual {v1, v0, p0}, Lcom/sigmob/sdk/common/f/n$a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/sigmob/sdk/common/f/n$a;

    const-class p0, Ljava/lang/String;

    invoke-virtual {v1, p0, p1}, Lcom/sigmob/sdk/common/f/n$a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/sigmob/sdk/common/f/n$a;

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/f/n$a;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    if-nez p0, :cond_0

    const/4 p0, 0x1

    return p0

    :catch_2
    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static q()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static r()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static u()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static v()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static w()Ljava/lang/Long;
    .locals 2

    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static x()Ljava/lang/Integer;
    .locals 1

    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()Ljava/lang/Integer;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->A(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/b;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public D()Ljava/lang/Integer;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->m(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public E()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public F()Lcom/sigmob/sdk/common/f/c$a;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->q(Landroid/content/Context;)Lcom/sigmob/sdk/common/f/c$a;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public G()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->u(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public I()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public J()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public K()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->w(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public N()Ljava/util/Locale;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->v(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public O()Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/sigmob/sdk/common/SDKConfig;->sharedInstance()Lcom/sigmob/sdk/common/SDKConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/SDKConfig;->getDisable_up_OAid()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/devicehelper/DeviceHelper;->getOAID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/devicehelper/DeviceHelper;->getOAID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public Q()Landroid/util/DisplayMetrics;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->y(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public R()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public S()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public T()I
    .locals 1

    iget v0, p0, Lcom/sigmob/sdk/common/a;->i:I

    return v0
.end method

.method public U()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sigmob/sdk/common/a;->k:Z

    return v0
.end method

.method public X()I
    .locals 1

    sget v0, Lcom/sigmob/sdk/common/a;->l:I

    return v0
.end method

.method public Y()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    return-object v0
.end method

.method public Z()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->x(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/content/Context;)I
    .locals 1

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/WindowManager;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Display;->getRotation()I

    move-result p1

    return p1
.end method

.method public a(I)Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sigmob/devicehelper/DeviceHelper;->getIMEI(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "string"

    iget-object v2, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    return-object p2
.end method

.method public varargs a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "string"

    iget-object v2, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getStringResources resid"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    return-object p2
.end method

.method public a(Landroid/location/Location;)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/sdk/common/a;->d:Landroid/location/Location;

    return-void
.end method

.method public a(Landroid/view/WindowInsets;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/WindowInsets;->isRound()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I

    move-result p1

    iput p1, p0, Lcom/sigmob/sdk/common/a;->i:I

    :cond_0
    return-void
.end method

.method public a(Lcom/sigmob/sdk/common/f/a;Lcom/sigmob/sdk/common/f/a;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onIdChanged() called with: oldId = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "], newId = ["

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sigmob/sdk/common/a;->k:Z

    return-void
.end method

.method public aa()Ljava/lang/Long;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->r(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public ad()Landroid/util/DisplayMetrics;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->D(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public ae()Z
    .locals 2

    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-static {}, Lcom/sigmob/sdk/common/SDKConfig;->getConfigUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/sdk/common/e/m;->b(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method public af()Lcom/sigmob/sdk/common/f/c$a;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->l(Landroid/content/Context;)Lcom/sigmob/sdk/common/f/c$a;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    sget-object v0, Lcom/sigmob/sdk/common/f/c$a;->a:Lcom/sigmob/sdk/common/f/c$a;

    return-object v0
.end method

.method public ag()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public ah()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/b;->b(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public ak()Landroid/location/LocationManager;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public al()Landroid/location/Location;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->ak()Landroid/location/LocationManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sigmob/sdk/common/a;->ak()Landroid/location/LocationManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lcom/sigmob/sdk/common/a;->d:Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->d:Landroid/location/Location;

    return-object v0
.end method

.method public am()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    const-string v0, ""

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    const/16 v2, 0x1000

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v3, 0x0

    move-object v4, v0

    const/4 v0, 0x0

    :goto_0
    :try_start_1
    array-length v5, v2

    if-ge v0, v5, :cond_3

    aget-object v5, v2, v0

    invoke-virtual {v1, v5, p1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    if-nez v5, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_2

    array-length v5, v2

    sub-int/2addr v5, v6

    if-ne v0, v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v6, v2, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    goto :goto_3

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v6, v2, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "permissionReq:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    const/4 v0, 0x2

    invoke-static {p1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    goto :goto_4

    :catch_1
    move-exception p1

    move-object v4, v0

    :goto_4
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_4
    return-object v4
.end method

.method public b(I)V
    .locals 3

    sput p1, Lcom/sigmob/sdk/common/a;->m:I

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/o;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "wind_agerestricted_status"

    sget v2, Lcom/sigmob/sdk/common/a;->m:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance v0, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;

    invoke-direct {v0}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setAge_restricted(Ljava/lang/String;)V

    const-string p1, "401"

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setAc_type(Ljava/lang/String;)V

    const-string p1, "coppa"

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setSub_category(Ljava/lang/String;)V

    const-string p1, "privacy"

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setCategory(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->commit()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/sigmob/sdk/common/a;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/o;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "uid"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/o;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_gdpr_region"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    sput p1, Lcom/sigmob/sdk/common/a;->o:I

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {}, Lcom/sigmob/devicehelper/DeviceHelper;->getAAID()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(I)V
    .locals 3

    sput p1, Lcom/sigmob/sdk/common/a;->l:I

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/o;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "wind_user_age"

    sget v2, Lcom/sigmob/sdk/common/a;->l:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance v0, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;

    invoke-direct {v0}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setAge(Ljava/lang/String;)V

    const-string p1, "401"

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setAc_type(Ljava/lang/String;)V

    const-string p1, "coppa"

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setSub_category(Ljava/lang/String;)V

    const-string p1, "privacy"

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setCategory(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->commit()V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    sput-object p1, Lcom/sigmob/sdk/common/a;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/o;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "wind_consent_status"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/o;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ext_gdpr_region"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/sigmob/sdk/common/SDKConfig;->isGDPRRegion()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    sput-boolean p1, Lcom/sigmob/sdk/common/a;->n:Z

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;

    invoke-direct {v0}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setUser_consent(Ljava/lang/String;)V

    invoke-static {}, Lcom/sigmob/sdk/common/a;->ai()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "1"

    goto :goto_0

    :cond_0
    const-string p1, "0"

    :goto_0
    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setGdpr_region(Ljava/lang/String;)V

    :try_start_0
    iget-object p1, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {p1}, Lcom/sigmob/windad/consent/WindAdConsentInformation;->getInstance(Landroid/content/Context;)Lcom/sigmob/windad/consent/WindAdConsentInformation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/sigmob/windad/consent/WindAdConsentInformation;->isRequestLocationInEeaOrUnknown()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "1"

    goto :goto_1

    :cond_1
    const-string p1, "0"

    :goto_1
    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setGdpr_dialog_region(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string p1, "consent"

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setSub_category(Ljava/lang/String;)V

    const-string p1, "gdpr"

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setCategory(Ljava/lang/String;)V

    const-string p1, "401"

    invoke-virtual {v0, p1}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->setAc_type(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/mta/PointEntityGDPR;->commit()V

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    const-string v1, "SHA1"

    invoke-static {v0, v1}, Lcom/sigmob/sdk/common/f/c;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    const-string v1, "MD5"

    invoke-static {v0, v1}, Lcom/sigmob/sdk/common/f/c;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->e:Lcom/sigmob/sdk/common/f/g;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/f/g;->a()Lcom/sigmob/sdk/common/f/a;

    move-result-object v0

    iget-object v0, v0, Lcom/sigmob/sdk/common/f/a;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/devicehelper/DeviceHelper;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->f()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/devicehelper/DeviceHelper;->getIMSI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/sigmob/sdk/common/f/c;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->j(Landroid/content/Context;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method public m()Ljava/lang/Float;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->n(Landroid/content/Context;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/lang/Integer;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->p(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/lang/Boolean;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->o(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public p()I
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->s(Landroid/content/Context;)F

    move-result v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    float-to-int v0, v0

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method public s()Ljava/lang/Integer;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->B(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public t()Ljava/lang/Integer;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->z(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public z()Ljava/lang/Integer;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sigmob/sdk/common/a;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/sigmob/sdk/common/f/c;->C(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method
