.class public Lcom/sigmob/sdk/common/mta/PointType;
.super Ljava/lang/Object;


# static fields
.field public static final ANTI_SPAM:Ljava/lang/String; = "101"

.field public static final ANTI_SPAM_TOUCH:Ljava/lang/String; = "102"

.field public static final DOWNLOAD_TRACKING:Ljava/lang/String; = "30"

.field public static final GDPR_CONSENT:Ljava/lang/String; = "401"

.field public static final LOGGER:Ljava/lang/String; = "201"

.field public static final SIGMOB_APP:Ljava/lang/String; = "10"

.field public static final SIGMOB_CRASH:Ljava/lang/String; = "402"

.field public static final SIGMOB_ERROR:Ljava/lang/String; = "9"

.field public static final SIGMOB_INIT:Ljava/lang/String; = "1"

.field public static final SIGMOB_REPORT_TRACKING:Ljava/lang/String; = "13"

.field public static final SIGMOB_TRACKING:Ljava/lang/String; = "5"

.field public static final WIND_ACTIVE:Ljava/lang/String; = "107"

.field public static final WIND_ERROR:Ljava/lang/String; = "7"

.field public static final WIND_INIT:Ljava/lang/String; = "2"

.field public static final WIND_TRACKING:Ljava/lang/String; = "6"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
