.class public final Lcom/sigmob/sdk/common/mta/PointCategory;
.super Ljava/lang/Object;


# static fields
.field public static final APP:Ljava/lang/String; = "app"

.field public static final APP_INSTALL_END:Ljava/lang/String; = "app_install_end"

.field public static final APP_INSTALL_MONITOR:Ljava/lang/String; = "app_install_Monitor"

.field public static final APP_INSTALL_START:Ljava/lang/String; = "app_install_start"

.field public static final ASKAD:Ljava/lang/String; = "askad"

.field public static final ASKREADY:Ljava/lang/String; = "askready"

.field public static final CLICK:Ljava/lang/String; = "click"

.field public static final CLOSE:Ljava/lang/String; = "close"

.field public static final CLOSE_DRIFT:Ljava/lang/String; = "close_drift"

.field public static final COMPANION:Ljava/lang/String; = "companion"

.field public static final COMPLETE:Ljava/lang/String; = "complete"

.field public static final CONSENT:Ljava/lang/String; = "consent"

.field public static final COPPA:Ljava/lang/String; = "coppa"

.field public static final CRASH:Ljava/lang/String; = "crash"

.field public static final DOWNLOAD_END:Ljava/lang/String; = "download_end"

.field public static final DOWNLOAD_FAILED:Ljava/lang/String; = "download_failed"

.field public static final DOWNLOAD_START:Ljava/lang/String; = "download_start"

.field public static final ENDCARD:Ljava/lang/String; = "endcard"

.field public static final ENDCARDCLICK:Ljava/lang/String; = "endcard_click"

.field public static final FINISH:Ljava/lang/String; = "finish"

.field public static final GDPR:Ljava/lang/String; = "gdpr"

.field public static final INIT:Ljava/lang/String; = "init"

.field public static final LOAD:Ljava/lang/String; = "load"

.field public static final LOADEND:Ljava/lang/String; = "loadend"

.field public static final LOADSTART:Ljava/lang/String; = "loadstart"

.field public static final NETWORK:Ljava/lang/String; = "network"

.field public static final NOTREADY:Ljava/lang/String; = "notready"

.field public static final PERMISSION:Ljava/lang/String; = "permission"

.field public static final PLAY:Ljava/lang/String; = "play"

.field public static final PLAY_LOADING:Ljava/lang/String; = "play_loading"

.field public static final PRIVACY:Ljava/lang/String; = "privacy"

.field public static final READY:Ljava/lang/String; = "ready"

.field public static final REPLAY:Ljava/lang/String; = "replay"

.field public static final REQUEST:Ljava/lang/String; = "request"

.field public static final RESPOND:Ljava/lang/String; = "respond"

.field public static final REWARD:Ljava/lang/String; = "reward"

.field public static final SCREENSWITCH:Ljava/lang/String; = "screenswitch"

.field public static final SDKOPEN:Ljava/lang/String; = "sdkopen"

.field public static final SERVER_ERROR:Ljava/lang/String; = "server_error"

.field public static final SESSION_END:Ljava/lang/String; = "session_end"

.field public static final SESSION_START:Ljava/lang/String; = "session_start"

.field public static final SHOW:Ljava/lang/String; = "show"

.field public static final SILENT:Ljava/lang/String; = "silent"

.field public static final SKIP:Ljava/lang/String; = "skip"

.field public static final SPLASHADBLOCK:Ljava/lang/String; = "splashAdBlock"

.field public static final START:Ljava/lang/String; = "start"

.field public static final TARGET_URL:Ljava/lang/String; = "target_url"

.field public static final TIMEOUT:Ljava/lang/String; = "timeout"

.field public static final VCLOSE:Ljava/lang/String; = "vclose"

.field public static final VIDEO:Ljava/lang/String; = "video"

.field public static final VIDEO_CLICK:Ljava/lang/String; = "full_video_click"

.field public static final VOPEN:Ljava/lang/String; = "vopen"

.field public static final WIFI_LIST:Ljava/lang/String; = "wifi_list"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
