.class public final enum Lcom/sigmob/sdk/common/f/c$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sigmob/sdk/common/f/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/sdk/common/f/c$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sigmob/sdk/common/f/c$a;

.field public static final enum b:Lcom/sigmob/sdk/common/f/c$a;

.field public static final enum c:Lcom/sigmob/sdk/common/f/c$a;

.field public static final enum d:Lcom/sigmob/sdk/common/f/c$a;

.field public static final enum e:Lcom/sigmob/sdk/common/f/c$a;

.field public static final enum f:Lcom/sigmob/sdk/common/f/c$a;

.field public static final enum g:Lcom/sigmob/sdk/common/f/c$a;

.field public static final enum h:Lcom/sigmob/sdk/common/f/c$a;

.field private static final synthetic j:[Lcom/sigmob/sdk/common/f/c$a;


# instance fields
.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/sigmob/sdk/common/f/c$a;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/sigmob/sdk/common/f/c$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/common/f/c$a;->a:Lcom/sigmob/sdk/common/f/c$a;

    new-instance v0, Lcom/sigmob/sdk/common/f/c$a;

    const-string v1, "ETHERNET"

    const/4 v3, 0x1

    const/16 v4, 0x65

    invoke-direct {v0, v1, v3, v4}, Lcom/sigmob/sdk/common/f/c$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/common/f/c$a;->b:Lcom/sigmob/sdk/common/f/c$a;

    new-instance v0, Lcom/sigmob/sdk/common/f/c$a;

    const-string v1, "WIFI"

    const/4 v4, 0x2

    const/16 v5, 0x64

    invoke-direct {v0, v1, v4, v5}, Lcom/sigmob/sdk/common/f/c$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/common/f/c$a;->c:Lcom/sigmob/sdk/common/f/c$a;

    new-instance v0, Lcom/sigmob/sdk/common/f/c$a;

    const-string v1, "MOBILE"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5, v3}, Lcom/sigmob/sdk/common/f/c$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/common/f/c$a;->d:Lcom/sigmob/sdk/common/f/c$a;

    new-instance v0, Lcom/sigmob/sdk/common/f/c$a;

    const-string v1, "MOBILE_2G"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6, v4}, Lcom/sigmob/sdk/common/f/c$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/common/f/c$a;->e:Lcom/sigmob/sdk/common/f/c$a;

    new-instance v0, Lcom/sigmob/sdk/common/f/c$a;

    const-string v1, "MOBILE_3G"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7, v5}, Lcom/sigmob/sdk/common/f/c$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/common/f/c$a;->f:Lcom/sigmob/sdk/common/f/c$a;

    new-instance v0, Lcom/sigmob/sdk/common/f/c$a;

    const-string v1, "MOBILE_4G"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8, v6}, Lcom/sigmob/sdk/common/f/c$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/common/f/c$a;->g:Lcom/sigmob/sdk/common/f/c$a;

    new-instance v0, Lcom/sigmob/sdk/common/f/c$a;

    const-string v1, "MOBILE_5G"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9, v7}, Lcom/sigmob/sdk/common/f/c$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sigmob/sdk/common/f/c$a;->h:Lcom/sigmob/sdk/common/f/c$a;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sigmob/sdk/common/f/c$a;

    sget-object v1, Lcom/sigmob/sdk/common/f/c$a;->a:Lcom/sigmob/sdk/common/f/c$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sigmob/sdk/common/f/c$a;->b:Lcom/sigmob/sdk/common/f/c$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/sdk/common/f/c$a;->c:Lcom/sigmob/sdk/common/f/c$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sigmob/sdk/common/f/c$a;->d:Lcom/sigmob/sdk/common/f/c$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sigmob/sdk/common/f/c$a;->e:Lcom/sigmob/sdk/common/f/c$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sigmob/sdk/common/f/c$a;->f:Lcom/sigmob/sdk/common/f/c$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sigmob/sdk/common/f/c$a;->g:Lcom/sigmob/sdk/common/f/c$a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sigmob/sdk/common/f/c$a;->h:Lcom/sigmob/sdk/common/f/c$a;

    aput-object v1, v0, v9

    sput-object v0, Lcom/sigmob/sdk/common/f/c$a;->j:[Lcom/sigmob/sdk/common/f/c$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sigmob/sdk/common/f/c$a;->i:I

    return-void
.end method

.method static synthetic a(Landroid/content/Context;I)Lcom/sigmob/sdk/common/f/c$a;
    .locals 0

    invoke-static {p0, p1}, Lcom/sigmob/sdk/common/f/c$a;->b(Landroid/content/Context;I)Lcom/sigmob/sdk/common/f/c$a;

    move-result-object p0

    return-object p0
.end method

.method private static b(Landroid/content/Context;I)Lcom/sigmob/sdk/common/f/c$a;
    .locals 1

    const/16 v0, 0x9

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    sget-object p0, Lcom/sigmob/sdk/common/f/c$a;->a:Lcom/sigmob/sdk/common/f/c$a;

    return-object p0

    :pswitch_0
    sget-object p0, Lcom/sigmob/sdk/common/f/c$a;->c:Lcom/sigmob/sdk/common/f/c$a;

    return-object p0

    :pswitch_1
    invoke-static {p0}, Lcom/sigmob/sdk/common/f/c;->l(Landroid/content/Context;)Lcom/sigmob/sdk/common/f/c$a;

    move-result-object p0

    return-object p0

    :cond_0
    sget-object p0, Lcom/sigmob/sdk/common/f/c$a;->b:Lcom/sigmob/sdk/common/f/c$a;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/sdk/common/f/c$a;
    .locals 1

    const-class v0, Lcom/sigmob/sdk/common/f/c$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/sdk/common/f/c$a;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/sdk/common/f/c$a;
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/common/f/c$a;->j:[Lcom/sigmob/sdk/common/f/c$a;

    invoke-virtual {v0}, [Lcom/sigmob/sdk/common/f/c$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/sdk/common/f/c$a;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/sigmob/sdk/common/f/c$a;->i:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/sigmob/sdk/common/f/c$a;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
