.class Lcom/sigmob/sdk/splash/g$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sigmob/sdk/splash/g;->a(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sigmob/sdk/splash/g;


# direct methods
.method constructor <init>(Lcom/sigmob/sdk/splash/g;)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/sdk/splash/g$2;->a:Lcom/sigmob/sdk/splash/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g$2;->a:Lcom/sigmob/sdk/splash/g;

    invoke-static {v0}, Lcom/sigmob/sdk/splash/g;->a(Lcom/sigmob/sdk/splash/g;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g$2;->a:Lcom/sigmob/sdk/splash/g;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sigmob/sdk/splash/g;->a(Lcom/sigmob/sdk/splash/g;Landroid/os/Handler;)Landroid/os/Handler;

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g$2;->a:Lcom/sigmob/sdk/splash/g;

    const-string v3, "timeout"

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g$2;->a:Lcom/sigmob/sdk/splash/g;

    invoke-static {v0}, Lcom/sigmob/sdk/splash/g;->g(Lcom/sigmob/sdk/splash/g;)Lcom/sigmob/sdk/base/models/LoadAdRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v5

    sget-object v0, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_SPLASH_TIMEOUT:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v6

    const-string v7, "load ad timeout"

    const/4 v4, 0x0

    invoke-static/range {v2 .. v7}, Lcom/sigmob/sdk/splash/g;->a(Lcom/sigmob/sdk/splash/g;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g$2;->a:Lcom/sigmob/sdk/splash/g;

    sget-object v1, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_SPLASH_TIMEOUT:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v1

    const-string v2, "time out"

    invoke-static {v0, v1, v2}, Lcom/sigmob/sdk/splash/g;->a(Lcom/sigmob/sdk/splash/g;ILjava/lang/String;)V

    return-void
.end method
