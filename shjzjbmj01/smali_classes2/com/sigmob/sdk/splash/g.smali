.class public final Lcom/sigmob/sdk/splash/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/sdk/base/a/b$a;
.implements Lcom/sigmob/sdk/splash/f$a;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/sigmob/sdk/splash/f;

.field private final c:Landroid/content/Context;

.field private d:Lcom/sigmob/sdk/splash/b;

.field private final e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

.field private f:Landroid/view/ViewGroup;

.field private g:I

.field private h:Lcom/sigmob/sdk/common/models/AdStatus;

.field private i:Lcom/sigmob/sdk/splash/SplashAdView;

.field private j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

.field private final k:Ljava/lang/Runnable;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/RelativeLayout;

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private t:Z

.field private u:Z

.field private v:I

.field private w:Z


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/sigmob/sdk/base/models/LoadAdRequest;ILcom/sigmob/sdk/splash/b;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sigmob/sdk/splash/g;->t:Z

    iput-boolean v0, p0, Lcom/sigmob/sdk/splash/g;->u:Z

    iput v0, p0, Lcom/sigmob/sdk/splash/g;->v:I

    iput-boolean v0, p0, Lcom/sigmob/sdk/splash/g;->w:Z

    new-instance v1, Lcom/sigmob/sdk/splash/g$1;

    invoke-direct {v1, p0}, Lcom/sigmob/sdk/splash/g$1;-><init>(Lcom/sigmob/sdk/splash/g;)V

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->k:Ljava/lang/Runnable;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRootWindowInsets()Landroid/view/WindowInsets;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sigmob/sdk/common/a;->a(Landroid/view/WindowInsets;)V

    :cond_0
    iput-object p1, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    sget-object v1, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusNone:Lcom/sigmob/sdk/common/models/AdStatus;

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    iput-object p3, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    iput-object p2, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    iput-object p5, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    new-instance p2, Lcom/sigmob/sdk/splash/f;

    invoke-direct {p2, p0}, Lcom/sigmob/sdk/splash/f;-><init>(Lcom/sigmob/sdk/base/common/k$a;)V

    iput-object p2, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->f()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 p3, 0x1

    if-nez p2, :cond_1

    invoke-direct {p0, p1}, Lcom/sigmob/sdk/splash/g;->c(Landroid/app/Activity;)V

    :goto_0
    iput-boolean p3, p0, Lcom/sigmob/sdk/splash/g;->w:Z

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    if-nez p2, :cond_2

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    goto :goto_0

    :cond_2
    :goto_1
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/a;->D()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p3, p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 p3, 0x0

    :goto_2
    if-nez p3, :cond_4

    invoke-virtual {p0}, Lcom/sigmob/sdk/splash/g;->e()V

    return-void

    :cond_4
    invoke-virtual {p0, p4, v0}, Lcom/sigmob/sdk/splash/g;->a(IZ)V

    return-void
.end method

.method private a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/sigmob/sdk/splash/g;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic a(Lcom/sigmob/sdk/splash/g;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    iput-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    return-object p1
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusNone:Lcom/sigmob/sdk/common/models/AdStatus;

    iput-object v0, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    :cond_0
    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->h()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {v0}, Lcom/sigmob/sdk/splash/SplashAdView;->a()V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    invoke-interface {v0, p1, p2}, Lcom/sigmob/sdk/splash/b;->onSplashAdFailToPresent(ILjava/lang/String;)V

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    :cond_3
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    invoke-virtual {p1}, Lcom/sigmob/sdk/splash/f;->b()V

    :cond_4
    invoke-static {}, Lcom/sigmob/sdk/base/common/d;->e()Lcom/sigmob/sdk/base/common/d;

    invoke-static {}, Lcom/sigmob/sdk/base/common/d;->f()V

    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    const-string v0, "server_error"

    new-instance v1, Lcom/sigmob/sdk/splash/g$3;

    invoke-direct {v1, p0, p3, p2, p1}, Lcom/sigmob/sdk/splash/g$3;-><init>(Lcom/sigmob/sdk/splash/g;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 p1, 0x0

    invoke-static {v0, p4, p5, p1, v1}, Lcom/sigmob/sdk/base/common/s;->a(Ljava/lang/String;ILjava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Lcom/sigmob/sdk/base/common/s$a;)V

    return-void
.end method

.method private a(Landroid/widget/RelativeLayout;I)V
    .locals 4

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sigmob/sdk/splash/g;->n:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->n:Landroid/widget/ImageView;

    invoke-static {}, Lcom/sigmob/sdk/common/a;->an()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->n:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sigmob/sdk/splash/g;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    const/high16 v0, 0x42700000    # 60.0f

    const/4 v1, 0x2

    if-eq p2, v1, :cond_1

    const/high16 v0, 0x42a00000    # 80.0f

    :cond_1
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sigmob/sdk/common/f/d;->b(FLandroid/content/Context;)I

    move-result v2

    iget-object v3, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/sigmob/sdk/common/f/d;->b(FLandroid/content/Context;)I

    move-result v0

    invoke-direct {v1, v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    packed-switch p2, :pswitch_data_0

    const/16 p2, 0xf

    :goto_0
    invoke-virtual {v1, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_1

    :pswitch_0
    const/high16 p2, 0x41200000    # 10.0f

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-static {p2, v0}, Lcom/sigmob/sdk/common/f/d;->c(FLandroid/content/Context;)I

    move-result p2

    iput p2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    const/16 p2, 0xe

    goto :goto_0

    :pswitch_1
    const/16 p2, 0xd

    goto :goto_0

    :goto_1
    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->n:Landroid/widget/ImageView;

    invoke-virtual {p1, p2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/widget/RelativeLayout;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    invoke-static {}, Lcom/sigmob/sdk/common/a;->an()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setId(I)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    const/16 v0, 0x30

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setGravity(I)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    const-string v0, "#535353"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    const/4 v0, 0x1

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p2, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    const/16 v0, 0xa

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setMaxEms(I)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->setSingleLine()V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {p2, v1, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {p2, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    invoke-virtual {p1, v0, p2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic a(Lcom/sigmob/sdk/splash/g;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sigmob/sdk/splash/g;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sigmob/sdk/splash/g;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sigmob/sdk/splash/g;Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/sigmob/sdk/splash/g$4;

    invoke-direct {v0, p0, p3, p4}, Lcom/sigmob/sdk/splash/g$4;-><init>(Lcom/sigmob/sdk/splash/g;Ljava/lang/String;I)V

    invoke-static {p1, p4, p5, p2, v0}, Lcom/sigmob/sdk/base/common/s;->a(Ljava/lang/String;ILjava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Lcom/sigmob/sdk/base/common/s$a;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/sigmob/sdk/splash/g$5;

    invoke-direct {v0, p0, p4, p5}, Lcom/sigmob/sdk/splash/g$5;-><init>(Lcom/sigmob/sdk/splash/g;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p2, p1, p3, v0}, Lcom/sigmob/sdk/base/common/s;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Lcom/sigmob/sdk/base/common/s$a;)V

    return-void
.end method

.method private a(Lcom/sigmob/sdk/base/models/BaseAdUnit;J)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    new-instance v1, Lcom/sigmob/sdk/splash/SplashAdView;

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sigmob/sdk/splash/SplashAdView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    if-nez v1, :cond_1

    return v0

    :cond_1
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {v2, v3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {v1, p2, p3}, Lcom/sigmob/sdk/splash/SplashAdView;->setBroadcastIdentifier(J)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    if-nez p2, :cond_2

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {p2}, Lcom/sigmob/sdk/splash/SplashAdView;->b()V

    :cond_2
    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    iget p3, p0, Lcom/sigmob/sdk/splash/g;->s:I

    if-eqz p3, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-virtual {p2, v0}, Lcom/sigmob/sdk/splash/SplashAdView;->setShowAppLogo(Z)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {p2, p1}, Lcom/sigmob/sdk/splash/SplashAdView;->a(Lcom/sigmob/sdk/base/models/BaseAdUnit;)Z

    move-result p1

    return p1
.end method

.method static synthetic b(Lcom/sigmob/sdk/splash/g;)Lcom/sigmob/sdk/splash/SplashAdView;
    .locals 0

    iget-object p0, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    return-object p0
.end method

.method private b(Landroid/widget/RelativeLayout;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sigmob/sdk/splash/g;->l:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->l:Landroid/widget/TextView;

    const/16 v0, 0x11

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setGravity(I)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->l:Landroid/widget/TextView;

    const-string v0, "#aaaaaa"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->l:Landroid/widget/TextView;

    const/4 v0, 0x2

    const/high16 v1, 0x41500000    # 13.0f

    invoke-virtual {p2, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->l:Landroid/widget/TextView;

    const/16 v0, 0x14

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setMaxEms(I)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->l:Landroid/widget/TextView;

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->l:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->setSingleLine()V

    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {p2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->m:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    const/4 v1, 0x3

    invoke-virtual {p2, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xe

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->l:Landroid/widget/TextView;

    invoke-virtual {p1, v0, p2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic c(Lcom/sigmob/sdk/splash/g;)I
    .locals 0

    iget p0, p0, Lcom/sigmob/sdk/splash/g;->g:I

    return p0
.end method

.method private c(Landroid/app/Activity;)V
    .locals 11

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/sigmob/sdk/common/a;->an()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setId(I)V

    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    invoke-static {}, Lcom/sigmob/sdk/common/a;->an()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setId(I)V

    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getId()I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    iput v3, p0, Lcom/sigmob/sdk/splash/g;->s:I

    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v4, 0x1

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput v2, p0, Lcom/sigmob/sdk/splash/g;->s:I

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iput v4, p0, Lcom/sigmob/sdk/splash/g;->s:I

    :cond_2
    :goto_0
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-direct {v2, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iget v5, p0, Lcom/sigmob/sdk/splash/g;->s:I

    invoke-direct {p0, v2, v5}, Lcom/sigmob/sdk/splash/g;->a(Landroid/widget/RelativeLayout;I)V

    new-instance v5, Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->f()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/sigmob/sdk/splash/g;->a(Landroid/widget/RelativeLayout;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->g()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/sigmob/sdk/splash/g;->b(Landroid/widget/RelativeLayout;Ljava/lang/String;)V

    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v6, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget v8, p0, Lcom/sigmob/sdk/splash/g;->s:I

    const/high16 v9, 0x41200000    # 10.0f

    if-eq v8, v3, :cond_3

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v4, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-static {v9, v4}, Lcom/sigmob/sdk/common/f/d;->c(FLandroid/content/Context;)I

    move-result v4

    iput v4, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    const/4 v4, 0x3

    :goto_1
    iget-object v7, p0, Lcom/sigmob/sdk/splash/g;->n:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getId()I

    move-result v7

    invoke-virtual {v6, v4, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_2

    :cond_3
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v8, 0x425c0000    # 55.0f

    iget-object v10, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-static {v8, v10}, Lcom/sigmob/sdk/common/f/d;->b(FLandroid/content/Context;)I

    move-result v8

    invoke-direct {v3, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v7, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-static {v9, v7}, Lcom/sigmob/sdk/common/f/d;->c(FLandroid/content/Context;)I

    move-result v7

    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_1

    :goto_2
    invoke-virtual {v2, v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v5, 0x42c80000    # 100.0f

    iget-object v6, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-static {v5, v6}, Lcom/sigmob/sdk/common/f/d;->b(FLandroid/content/Context;)I

    move-result v5

    invoke-direct {v4, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xc

    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xd

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, p1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic d(Lcom/sigmob/sdk/splash/g;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/sigmob/sdk/splash/g;->k:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic e(Lcom/sigmob/sdk/splash/g;)I
    .locals 2

    iget v0, p0, Lcom/sigmob/sdk/splash/g;->g:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sigmob/sdk/splash/g;->g:I

    return v0
.end method

.method static synthetic f(Lcom/sigmob/sdk/splash/g;)Lcom/sigmob/sdk/base/models/BaseAdUnit;
    .locals 0

    iget-object p0, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    return-object p0
.end method

.method private f()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {v0}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getOptions()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v1, "APP_TITLE"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic g(Lcom/sigmob/sdk/splash/g;)Lcom/sigmob/sdk/base/models/LoadAdRequest;
    .locals 0

    iget-object p0, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    return-object p0
.end method

.method private g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {v0}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getOptions()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v1, "APP_DESC"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private h()Z
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {v0}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getOptions()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "SPLASH_DISABLE_AD_HIDE"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    const-string v1, "SPLASH_DISABLE_AD_HIDE"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->e(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private i()V
    .locals 4

    iget-boolean v0, p0, Lcom/sigmob/sdk/splash/g;->p:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/sigmob/sdk/splash/g;->p:Z

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    iget v2, p0, Lcom/sigmob/sdk/splash/g;->q:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/sigmob/sdk/splash/g;->u:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    iget v3, p0, Lcom/sigmob/sdk/splash/g;->q:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    iget v2, p0, Lcom/sigmob/sdk/splash/g;->v:I

    invoke-virtual {v0, v2}, Landroid/view/Window;->setNavigationBarColor(I)V

    goto :goto_0

    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_2

    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    iget-object v0, v0, Lcom/sigmob/sdk/splash/f;->e:Lcom/sigmob/sdk/splash/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    iget-object v0, v0, Lcom/sigmob/sdk/splash/f;->e:Lcom/sigmob/sdk/splash/d;

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {v0, v2, v1, v3}, Lcom/sigmob/sdk/splash/d;->c(Landroid/content/Context;ILcom/sigmob/sdk/base/models/BaseAdUnit;)V

    :cond_3
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    invoke-virtual {v0}, Lcom/sigmob/sdk/splash/f;->b()V

    :cond_4
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    :cond_5
    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->h()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {v0}, Lcom/sigmob/sdk/splash/SplashAdView;->a()V

    :cond_7
    :goto_1
    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-static {}, Lcom/sigmob/sdk/base/common/d;->e()Lcom/sigmob/sdk/base/common/d;

    invoke-static {}, Lcom/sigmob/sdk/base/common/d;->f()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    const-string v1, "play"

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object v0, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_RESOURCE:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v4

    const-string v5, ""

    const/4 v3, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;ILjava/lang/String;)V

    sget-object v0, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_RESOURCE:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v0

    sget-object v1, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_RESOURCE:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/models/SigmobError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sigmob/sdk/splash/g;->a(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->i()V

    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/LoadAdRequest;)V
    .locals 11

    const-string v1, "0"

    const-string v2, "respond"

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {v0}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "request"

    invoke-virtual {p4}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v8

    const/4 v7, 0x0

    move-object v5, p0

    move v9, p1

    move-object v10, p2

    invoke-direct/range {v5 .. v10}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;ILjava/lang/String;)V

    sget-object v0, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_NETWORK:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-virtual {p4}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getAdType()I

    move-result v2

    invoke-virtual {p4}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v4, p3

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/sigmob/sdk/splash/g;->a(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sigmob/sdk/splash/g;->a(ILjava/lang/String;)V

    return-void
.end method

.method protected a(IZ)V
    .locals 8

    const/4 v0, 0x0

    if-nez p2, :cond_2

    invoke-static {}, Lcom/sigmob/sdk/common/f/e;->d()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/sigmob/sdk/common/f/e;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    instance-of v1, p2, Lcom/sigmob/sdk/base/models/BaseAdUnit;

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sigmob/sdk/common/f/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sigmob/sdk/common/f/e;->b(Ljava/lang/String;)Z

    move-object v1, p2

    check-cast v1, Lcom/sigmob/sdk/base/models/BaseAdUnit;

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {v3}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getSplashFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v2

    invoke-virtual {v1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getAd()Lcom/sigmob/sdk/common/models/sigdsp/pb/Ad;

    move-result-object v3

    iget-object v3, v3, Lcom/sigmob/sdk/common/models/sigdsp/pb/Ad;->expired_time:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getCreate_time()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {v1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getAdExpiredTime()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v5, v5

    cmp-long v7, v3, v5

    if-lez v7, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    sget-object v2, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusLoading:Lcom/sigmob/sdk/common/models/AdStatus;

    iput-object v2, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getRequest_id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    int-to-long v3, v1

    const-string v1, "AD_UNIT_KEY"

    invoke-interface {v2, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "broadcastIdentifier"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-direct {p0, p2, v3, v4}, Lcom/sigmob/sdk/splash/g;->a(Lcom/sigmob/sdk/base/models/BaseAdUnit;J)Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    if-eqz p2, :cond_1

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {p1, p2, v2, v0}, Lcom/sigmob/sdk/splash/f;->a(Landroid/content/Context;Ljava/util/Map;Lcom/sigmob/sdk/base/models/BaseAdUnit;)V

    return-void

    :cond_1
    const-string p2, "init"

    const-string v1, "request"

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {v2}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p2, v1, v0, v2}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {p2}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getOptions()Ljava/util/Map;

    move-result-object p2

    const-string v0, "request_scene_type"

    sget-object v1, Lcom/sigmob/sdk/rewardVideoAd/a;->c:Lcom/sigmob/sdk/rewardVideoAd/a;

    invoke-virtual {v1}, Lcom/sigmob/sdk/rewardVideoAd/a;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusLoading:Lcom/sigmob/sdk/common/models/AdStatus;

    iput-object p2, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    new-instance v0, Lcom/sigmob/sdk/splash/g$2;

    invoke-direct {v0, p0}, Lcom/sigmob/sdk/splash/g$2;-><init>(Lcom/sigmob/sdk/splash/g;)V

    mul-int/lit16 p1, p1, 0x3e8

    int-to-long v1, p1

    invoke-virtual {p2, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getOptions()Ljava/util/Map;

    move-result-object p1

    const-string p2, "request_scene_type"

    sget-object v1, Lcom/sigmob/sdk/rewardVideoAd/a;->e:Lcom/sigmob/sdk/rewardVideoAd/a;

    invoke-virtual {v1}, Lcom/sigmob/sdk/rewardVideoAd/a;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "play"

    const-string p2, "request"

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {v1}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;)V

    :goto_1
    invoke-static {}, Lcom/sigmob/sdk/common/SDKConfig;->sharedInstance()Lcom/sigmob/sdk/common/SDKConfig;

    move-result-object p1

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/SDKConfig;->getAdsUrl()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-static {p1, p2, p0}, Lcom/sigmob/sdk/base/a/b;->a(Ljava/lang/String;Lcom/sigmob/sdk/base/models/LoadAdRequest;Lcom/sigmob/sdk/base/a/b$a;)V

    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    if-ne v0, p1, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {p1}, Lcom/sigmob/sdk/splash/SplashAdView;->d()V

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method public a(Lcom/sigmob/sdk/base/models/BaseAdUnit;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/sigmob/sdk/base/models/BaseAdUnit;Lcom/sigmob/sdk/base/models/LoadAdRequest;)V
    .locals 4

    const-string p2, "1"

    const-string v0, "respond"

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {v1}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getPlacementId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v0, p1, v1}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;)V

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Lcom/sigmob/sdk/base/models/BaseAdUnit;->getRequest_id()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    iput-object p1, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    const-string v2, "AD_UNIT_KEY"

    invoke-interface {p2, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "broadcastIdentifier"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p2, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    sget-object v3, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusLoading:Lcom/sigmob/sdk/common/models/AdStatus;

    if-ne v2, v3, :cond_1

    invoke-direct {p0, p1, v0, v1}, Lcom/sigmob/sdk/splash/g;->a(Lcom/sigmob/sdk/base/models/BaseAdUnit;J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sigmob/sdk/splash/g;->a()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    sget-object v1, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusClose:Lcom/sigmob/sdk/common/models/AdStatus;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    if-eqz v0, :cond_2

    :goto_0
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p1}, Lcom/sigmob/sdk/splash/f;->a(Landroid/content/Context;Ljava/util/Map;Lcom/sigmob/sdk/base/models/BaseAdUnit;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public a(Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;)V
    .locals 7

    sget-object v0, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_FILE_DOWNLOAD:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/sigmob/sdk/splash/g;->a(ILjava/lang/String;)V

    const-string v2, "load"

    sget-object v0, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_FILE_DOWNLOAD:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v5

    const/4 v4, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public b()V
    .locals 4

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    iget-object v0, v0, Lcom/sigmob/sdk/splash/f;->e:Lcom/sigmob/sdk/splash/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    iget-object v0, v0, Lcom/sigmob/sdk/splash/f;->e:Lcom/sigmob/sdk/splash/d;

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    invoke-virtual {v0, v2, v1, v3}, Lcom/sigmob/sdk/splash/d;->d(Landroid/content/Context;ILcom/sigmob/sdk/base/models/BaseAdUnit;)V

    :cond_0
    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {v0, v1}, Lcom/sigmob/sdk/splash/SplashAdView;->setDuration(I)V

    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    if-ne v0, p1, :cond_1

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {p1}, Lcom/sigmob/sdk/splash/SplashAdView;->e()V

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    if-nez p1, :cond_0

    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    :cond_0
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->k:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public b(Lcom/sigmob/sdk/base/models/BaseAdUnit;)V
    .locals 5

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    sget-object v1, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusClose:Lcom/sigmob/sdk/common/models/AdStatus;

    if-ne v0, v1, :cond_0

    const-string v0, " next load"

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    invoke-static {}, Lcom/sigmob/sdk/common/f/e;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sigmob/sdk/common/f/e;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    sget-object v0, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusLoading:Lcom/sigmob/sdk/common/models/AdStatus;

    if-eq p1, v0, :cond_1

    return-void

    :cond_1
    invoke-static {}, Lcom/sigmob/sdk/common/a;->aj()Lcom/sigmob/sdk/common/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/sigmob/sdk/common/a;->D()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne v1, p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/sigmob/sdk/splash/g;->e()V

    return-void

    :cond_3
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_4
    iget-boolean p1, p0, Lcom/sigmob/sdk/splash/g;->w:Z

    if-eqz p1, :cond_8

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt p1, v2, :cond_8

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result p1

    iput p1, p0, Lcom/sigmob/sdk/splash/g;->q:I

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p1

    iget p1, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    iput p1, p0, Lcom/sigmob/sdk/splash/g;->r:I

    iget p1, p0, Lcom/sigmob/sdk/splash/g;->q:I

    and-int/lit16 p1, p1, 0x200

    if-nez p1, :cond_7

    iget p1, p0, Lcom/sigmob/sdk/splash/g;->q:I

    and-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_5

    goto :goto_1

    :cond_5
    iget p1, p0, Lcom/sigmob/sdk/splash/g;->r:I

    const/high16 v3, 0x8000000

    and-int/2addr p1, v3

    if-nez p1, :cond_8

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    iput-boolean v1, p0, Lcom/sigmob/sdk/splash/g;->u:Z

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v1, v4, :cond_6

    invoke-virtual {p1, v3}, Landroid/view/Window;->clearFlags(I)V

    invoke-virtual {p1}, Landroid/view/Window;->getNavigationBarColor()I

    move-result v1

    iput v1, p0, Lcom/sigmob/sdk/splash/g;->v:I

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    or-int/lit8 v1, v1, 0x4

    or-int/lit16 v1, v1, 0x100

    or-int/lit16 v1, v1, 0x200

    invoke-virtual {v2, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    const/high16 v1, -0x80000000

    invoke-virtual {p1, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p1, v0}, Landroid/view/Window;->setNavigationBarColor(I)V

    goto :goto_2

    :cond_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_8

    const/high16 v0, 0x4000000

    invoke-virtual {p1, v0}, Landroid/view/Window;->addFlags(I)V

    goto :goto_2

    :cond_7
    :goto_1
    iput-boolean v1, p0, Lcom/sigmob/sdk/splash/g;->p:Z

    const/16 p1, 0x1206

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    :cond_8
    :goto_2
    sget-object p1, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusReady:Lcom/sigmob/sdk/common/models/AdStatus;

    iput-object p1, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->e:Lcom/sigmob/sdk/base/models/LoadAdRequest;

    invoke-virtual {v2}, Lcom/sigmob/sdk/base/models/LoadAdRequest;->getOptions()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/sigmob/sdk/splash/f;->a(Landroid/content/Context;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/util/Map;)V

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->b:Lcom/sigmob/sdk/splash/f;

    iget-object p1, p1, Lcom/sigmob/sdk/splash/f;->e:Lcom/sigmob/sdk/splash/d;

    invoke-virtual {p1}, Lcom/sigmob/sdk/splash/d;->d()I

    move-result p1

    iput p1, p0, Lcom/sigmob/sdk/splash/g;->g:I

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    iget v0, p0, Lcom/sigmob/sdk/splash/g;->g:I

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/splash/SplashAdView;->setDuration(I)V

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {p1}, Lcom/sigmob/sdk/splash/SplashAdView;->c()Z

    move-result p1

    if-nez p1, :cond_9

    invoke-virtual {p0}, Lcom/sigmob/sdk/splash/g;->a()V

    :cond_9
    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public c(Lcom/sigmob/sdk/base/models/BaseAdUnit;)V
    .locals 2

    sget-object v0, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusPlaying:Lcom/sigmob/sdk/common/models/AdStatus;

    iput-object v0, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    invoke-static {p1}, Lcom/sigmob/sdk/base/common/d;->c(Lcom/sigmob/sdk/base/models/BaseAdUnit;)V

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {p1}, Lcom/sigmob/sdk/splash/SplashAdView;->getDuration()I

    move-result p1

    if-lez p1, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {p1}, Lcom/sigmob/sdk/splash/SplashAdView;->getDuration()I

    move-result p1

    iget v0, p0, Lcom/sigmob/sdk/splash/g;->g:I

    if-ge p1, v0, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {p1}, Lcom/sigmob/sdk/splash/SplashAdView;->getDuration()I

    move-result p1

    iput p1, p0, Lcom/sigmob/sdk/splash/g;->g:I

    :cond_0
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->o:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_1
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    iget v1, p0, Lcom/sigmob/sdk/splash/g;->g:I

    invoke-virtual {p1, v1}, Lcom/sigmob/sdk/splash/SplashAdView;->setDuration(I)V

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->i:Lcom/sigmob/sdk/splash/SplashAdView;

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/splash/SplashAdView;->setVisibility(I)V

    :cond_2
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    invoke-interface {p1}, Lcom/sigmob/sdk/splash/b;->onSplashAdSuccessPresentScreen()V

    :cond_3
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    if-nez p1, :cond_4

    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    :cond_4
    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->k:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sigmob/sdk/splash/g;->a:Landroid/os/Handler;

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sigmob/sdk/splash/g;->t:Z

    return-void
.end method

.method public d(Lcom/sigmob/sdk/base/models/BaseAdUnit;)V
    .locals 2

    iget-boolean p1, p0, Lcom/sigmob/sdk/splash/g;->p:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/sigmob/sdk/splash/g;->p:Z

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->f:Landroid/view/ViewGroup;

    iget v0, p0, Lcom/sigmob/sdk/splash/g;->q:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    :cond_0
    iget-boolean p1, p0, Lcom/sigmob/sdk/splash/g;->u:Z

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->c:Landroid/content/Context;

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/sigmob/sdk/splash/g;->q:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    const/high16 v0, -0x80000000

    invoke-virtual {p1, v0}, Landroid/view/Window;->clearFlags(I)V

    iget v0, p0, Lcom/sigmob/sdk/splash/g;->v:I

    invoke-virtual {p1, v0}, Landroid/view/Window;->setNavigationBarColor(I)V

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    const/high16 v0, 0x4000000

    invoke-virtual {p1, v0}, Landroid/view/Window;->clearFlags(I)V

    :cond_2
    :goto_0
    sget-object p1, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusClick:Lcom/sigmob/sdk/common/models/AdStatus;

    iput-object p1, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    invoke-interface {p1}, Lcom/sigmob/sdk/splash/b;->onSplashAdClicked()V

    :cond_3
    return-void
.end method

.method public e()V
    .locals 6

    const-string v1, "play"

    iget-object v2, p0, Lcom/sigmob/sdk/splash/g;->j:Lcom/sigmob/sdk/base/models/BaseAdUnit;

    sget-object v0, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_ORIENTATION:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v4

    const-string v5, ""

    const/4 v3, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sigmob/sdk/splash/g;->a(Ljava/lang/String;Lcom/sigmob/sdk/base/models/BaseAdUnit;Ljava/lang/String;ILjava/lang/String;)V

    sget-object v0, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_ORIENTATION:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v0}, Lcom/sigmob/sdk/common/models/SigmobError;->getErrorCode()I

    move-result v0

    sget-object v1, Lcom/sigmob/sdk/common/models/SigmobError;->ERROR_SIGMOB_SPLASH_UNSUPPORT_ORIENTATION:Lcom/sigmob/sdk/common/models/SigmobError;

    invoke-virtual {v1}, Lcom/sigmob/sdk/common/models/SigmobError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sigmob/sdk/splash/g;->a(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->i()V

    return-void
.end method

.method public e(Lcom/sigmob/sdk/base/models/BaseAdUnit;)V
    .locals 1

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/splash/g;->d:Lcom/sigmob/sdk/splash/b;

    invoke-interface {p1}, Lcom/sigmob/sdk/splash/b;->onSplashClosed()V

    :cond_0
    invoke-direct {p0}, Lcom/sigmob/sdk/splash/g;->i()V

    sget-object p1, Lcom/sigmob/sdk/common/models/AdStatus;->AdStatusClose:Lcom/sigmob/sdk/common/models/AdStatus;

    iput-object p1, p0, Lcom/sigmob/sdk/splash/g;->h:Lcom/sigmob/sdk/common/models/AdStatus;

    const/4 p1, 0x0

    invoke-static {p1}, Lcom/sigmob/sdk/base/common/d;->c(Lcom/sigmob/sdk/base/models/BaseAdUnit;)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sigmob/sdk/splash/g;->a(IZ)V

    return-void
.end method
