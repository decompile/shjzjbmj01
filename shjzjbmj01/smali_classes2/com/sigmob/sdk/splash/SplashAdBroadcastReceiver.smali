.class public Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;
.super Lcom/sigmob/sdk/base/common/BaseBroadcastReceiver;


# static fields
.field private static a:Landroid/content/IntentFilter;


# instance fields
.field private b:Lcom/sigmob/sdk/splash/f$a;


# direct methods
.method public constructor <init>(Lcom/sigmob/sdk/splash/f$a;J)V
    .locals 0

    invoke-direct {p0, p2, p3}, Lcom/sigmob/sdk/base/common/BaseBroadcastReceiver;-><init>(J)V

    iput-object p1, p0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->b:Lcom/sigmob/sdk/splash/f$a;

    invoke-virtual {p0}, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->a()Landroid/content/IntentFilter;

    return-void
.end method


# virtual methods
.method public a()Landroid/content/IntentFilter;
    .locals 2

    sget-object v0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->a:Landroid/content/IntentFilter;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->a:Landroid/content/IntentFilter;

    sget-object v0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->a:Landroid/content/IntentFilter;

    const-string v1, "com.sigmob.action.splash.stoptime"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->a:Landroid/content/IntentFilter;

    const-string v1, "com.sigmob.action.splash.playFail"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->a:Landroid/content/IntentFilter;

    const-string v1, "com.sigmob.action.splash.skip"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->a:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public a(Landroid/content/BroadcastReceiver;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/sigmob/sdk/base/common/BaseBroadcastReceiver;->a(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-static {p1}, Lcom/sigmob/sdk/common/f/m$a;->a(Ljava/lang/Object;)Z

    invoke-static {p2}, Lcom/sigmob/sdk/common/f/m$a;->a(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->b:Lcom/sigmob/sdk/splash/f$a;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p2}, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->a(Landroid/content/Intent;)Z

    move-result p1

    if-nez p1, :cond_1

    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const/4 p2, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x7e78cdb0

    if-eq v0, v1, :cond_5

    const v1, -0x74c83b0d

    if-eq v0, v1, :cond_4

    const v1, -0x39a663cb

    if-eq v0, v1, :cond_3

    const v1, -0x39a50980    # -14013.625f

    if-eq v0, v1, :cond_2

    goto :goto_0

    :cond_2
    const-string v0, "com.sigmob.action.splash.skip"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    const/4 p2, 0x2

    goto :goto_0

    :cond_3
    const-string v0, "com.sigmob.action.splash.play"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    const/4 p2, 0x0

    goto :goto_0

    :cond_4
    const-string v0, "com.sigmob.action.splash.playFail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    const/4 p2, 0x1

    goto :goto_0

    :cond_5
    const-string v0, "com.sigmob.action.splash.stoptime"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    const/4 p2, 0x3

    :cond_6
    :goto_0
    packed-switch p2, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-object p1, p0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->b:Lcom/sigmob/sdk/splash/f$a;

    invoke-interface {p1}, Lcom/sigmob/sdk/splash/f$a;->d()V

    goto :goto_1

    :pswitch_1
    iget-object p1, p0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->b:Lcom/sigmob/sdk/splash/f$a;

    invoke-interface {p1}, Lcom/sigmob/sdk/splash/f$a;->b()V

    goto :goto_1

    :pswitch_2
    iget-object p1, p0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->b:Lcom/sigmob/sdk/splash/f$a;

    invoke-interface {p1}, Lcom/sigmob/sdk/splash/f$a;->a()V

    goto :goto_1

    :pswitch_3
    iget-object p1, p0, Lcom/sigmob/sdk/splash/SplashAdBroadcastReceiver;->b:Lcom/sigmob/sdk/splash/f$a;

    invoke-interface {p1}, Lcom/sigmob/sdk/splash/f$a;->c()V

    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
