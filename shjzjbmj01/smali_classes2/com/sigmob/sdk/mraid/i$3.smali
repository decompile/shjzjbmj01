.class Lcom/sigmob/sdk/mraid/i$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sigmob/sdk/mraid/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sigmob/sdk/mraid/i;->k()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sigmob/sdk/mraid/i;


# direct methods
.method constructor <init>(Lcom/sigmob/sdk/mraid/i;)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, "MraidActivity failed to load. Finishing the activity"

    invoke-static {v0}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {v0}, Lcom/sigmob/sdk/mraid/i;->g(Lcom/sigmob/sdk/mraid/i;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const-string v1, "com.sigmob.action.interstitial.fail"

    invoke-static {v0, v1}, Lcom/sigmob/sdk/mraid/i;->d(Lcom/sigmob/sdk/mraid/i;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {v0}, Lcom/sigmob/sdk/mraid/i;->h(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/base/common/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/sigmob/sdk/base/common/h;->a()V

    return-void
.end method

.method public a(F)V
    .locals 1

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->e(Lcom/sigmob/sdk/mraid/i;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/mraid/i;Z)Z

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->b:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const-string v0, "com.sigmob.action.rewardedvideo.complete"

    invoke-static {p1, v0}, Lcom/sigmob/sdk/mraid/i;->h(Lcom/sigmob/sdk/mraid/i;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->a:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V

    const-string p1, "onLoaded() called"

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/sigmob/sdk/common/models/SigmobError;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Finishing the activity due to a problem: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/sigmob/logger/SigmobLog;->d(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->E:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->i(Lcom/sigmob/sdk/mraid/i;)Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const-string v0, "com.sigmob.action.interstitial.fail"

    invoke-static {p1, v0}, Lcom/sigmob/sdk/mraid/i;->e(Lcom/sigmob/sdk/mraid/i;Ljava/lang/String;)V

    :cond_0
    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->j(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/base/common/h;

    move-result-object p1

    invoke-interface {p1}, Lcom/sigmob/sdk/base/common/h;->a()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 10

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "type"

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p1

    const-string v2, "x"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "y"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iget-object v3, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {v3}, Lcom/sigmob/sdk/mraid/i;->c(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/mraid/c;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Lcom/sigmob/sdk/mraid/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eq p1, v1, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->o:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x0

    :goto_0
    move v9, v1

    goto :goto_1

    :catch_0
    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->c(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/mraid/c;

    move-result-object p1

    const-string v0, "0"

    const-string v2, "0"

    invoke-virtual {p1, v0, v2}, Lcom/sigmob/sdk/mraid/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->o:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V

    :cond_1
    const/4 v9, 0x1

    :goto_1
    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->d(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/videoAd/a;

    move-result-object v2

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/base/models/BaseAdUnit;

    move-result-object v3

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->m(Lcom/sigmob/sdk/mraid/i;)Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->c(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/mraid/c;

    move-result-object p1

    invoke-virtual {p1}, Lcom/sigmob/sdk/mraid/c;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v2 .. v9}, Lcom/sigmob/sdk/videoAd/a;->a(Lcom/sigmob/sdk/base/models/BaseAdUnit;Landroid/content/Context;Ljava/lang/String;Lcom/sigmob/sdk/videoAd/f;ZLjava/lang/String;Z)V

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const-string v0, "com.sigmob.action.interstitial.click"

    invoke-static {p1, v0}, Lcom/sigmob/sdk/mraid/i;->j(Lcom/sigmob/sdk/mraid/i;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/net/URI;Lcom/sigmob/sdk/videoAd/f;Ljava/lang/String;)V
    .locals 10

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p3, "type"

    invoke-virtual {v0, p3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p3

    const-string v2, "x"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "y"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iget-object v3, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {v3}, Lcom/sigmob/sdk/mraid/i;->c(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/mraid/c;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Lcom/sigmob/sdk/mraid/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eq p3, v1, :cond_0

    iget-object p3, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->c:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {p3, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    const/4 v1, 0x0

    :goto_0
    move v9, v1

    goto :goto_1

    :catch_0
    iget-object p3, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p3}, Lcom/sigmob/sdk/mraid/i;->c(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/mraid/c;

    move-result-object p3

    const-string v0, "0"

    const-string v2, "0"

    invoke-virtual {p3, v0, v2}, Lcom/sigmob/sdk/mraid/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p3, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->c:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {p3, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V

    :cond_1
    const/4 v9, 0x1

    :goto_1
    iget-object p3, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p3}, Lcom/sigmob/sdk/mraid/i;->d(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/videoAd/a;

    move-result-object v2

    iget-object p3, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p3}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/base/models/BaseAdUnit;

    move-result-object v3

    iget-object p3, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p3}, Lcom/sigmob/sdk/mraid/i;->n(Lcom/sigmob/sdk/mraid/i;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->c(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/mraid/c;

    move-result-object p1

    invoke-virtual {p1}, Lcom/sigmob/sdk/mraid/c;->a()Ljava/lang/String;

    move-result-object v8

    move-object v6, p2

    invoke-virtual/range {v2 .. v9}, Lcom/sigmob/sdk/videoAd/a;->a(Lcom/sigmob/sdk/base/models/BaseAdUnit;Landroid/content/Context;Ljava/lang/String;Lcom/sigmob/sdk/videoAd/f;ZLjava/lang/String;Z)V

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const-string p2, "com.sigmob.action.interstitial.click"

    invoke-static {p1, p2}, Lcom/sigmob/sdk/mraid/i;->k(Lcom/sigmob/sdk/mraid/i;Ljava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->v:Lcom/sigmob/sdk/base/common/a;

    :goto_0
    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->w:Lcom/sigmob/sdk/base/common/a;

    goto :goto_0

    :goto_1
    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {v0}, Lcom/sigmob/sdk/mraid/i;->c(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/mraid/c;

    move-result-object v0

    new-instance v1, Lcom/sigmob/sdk/mraid/i$3$1;

    invoke-direct {v1, p0}, Lcom/sigmob/sdk/mraid/i$3$1;-><init>(Lcom/sigmob/sdk/mraid/i$3;)V

    invoke-virtual {v0, v1}, Lcom/sigmob/sdk/mraid/c;->a(Landroid/webkit/ValueCallback;)V

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const-string v1, "com.sigmob.action.rewardedvideo.Close"

    invoke-static {v0, v1}, Lcom/sigmob/sdk/mraid/i;->g(Lcom/sigmob/sdk/mraid/i;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sigmob/sdk/mraid/i;->c(Lcom/sigmob/sdk/mraid/i;Z)Z

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {v0}, Lcom/sigmob/sdk/mraid/i;->l(Lcom/sigmob/sdk/mraid/i;)Lcom/sigmob/sdk/base/common/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/sigmob/sdk/base/common/h;->a()V

    return-void
.end method

.method public b(F)V
    .locals 1

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    invoke-static {p1}, Lcom/sigmob/sdk/mraid/i;->k(Lcom/sigmob/sdk/mraid/i;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/sigmob/sdk/mraid/i;->b(Lcom/sigmob/sdk/mraid/i;Z)Z

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v0, Lcom/sigmob/sdk/base/common/a;->m:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {p1, v0}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V

    iget-object p1, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    const-string v0, "com.sigmob.action.rewardedvideo.skip"

    invoke-static {p1, v0}, Lcom/sigmob/sdk/mraid/i;->i(Lcom/sigmob/sdk/mraid/i;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->d:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {v0, v1}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/i$3;->a:Lcom/sigmob/sdk/mraid/i;

    sget-object v1, Lcom/sigmob/sdk/base/common/a;->F:Lcom/sigmob/sdk/base/common/a;

    invoke-virtual {v0, v1}, Lcom/sigmob/sdk/mraid/i;->a(Lcom/sigmob/sdk/base/common/a;)V

    return-void
.end method
