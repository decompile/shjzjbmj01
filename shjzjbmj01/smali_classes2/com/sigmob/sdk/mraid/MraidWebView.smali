.class public Lcom/sigmob/sdk/mraid/MraidWebView;
.super Lcom/sigmob/sdk/base/views/BaseWebView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sigmob/sdk/mraid/MraidWebView$a;
    }
.end annotation


# instance fields
.field private a:Lcom/sigmob/sdk/mraid/MraidWebView$a;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sigmob/sdk/base/views/BaseWebView;-><init>(Landroid/content/Context;)V

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x16

    if-gt p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/sigmob/sdk/mraid/MraidWebView;->getVisibility()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/sigmob/sdk/mraid/MraidWebView;->b:Z

    :cond_1
    return-void
.end method

.method private setMraidViewable(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/sigmob/sdk/mraid/MraidWebView;->b:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/sigmob/sdk/mraid/MraidWebView;->b:Z

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/MraidWebView;->a:Lcom/sigmob/sdk/mraid/MraidWebView$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/MraidWebView;->a:Lcom/sigmob/sdk/mraid/MraidWebView$a;

    invoke-interface {v0, p1}, Lcom/sigmob/sdk/mraid/MraidWebView$a;->a(Z)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sigmob/sdk/mraid/MraidWebView;->b:Z

    return v0
.end method

.method public destroy()V
    .locals 1

    invoke-super {p0}, Lcom/sigmob/sdk/base/views/BaseWebView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sigmob/sdk/mraid/MraidWebView;->a:Lcom/sigmob/sdk/mraid/MraidWebView$a;

    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/sigmob/sdk/base/views/BaseWebView;->onVisibilityChanged(Landroid/view/View;I)V

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/sigmob/sdk/mraid/MraidWebView;->setMraidViewable(Z)V

    :goto_0
    return-void
.end method

.method setVisibilityChangedListener(Lcom/sigmob/sdk/mraid/MraidWebView$a;)V
    .locals 0

    iput-object p1, p0, Lcom/sigmob/sdk/mraid/MraidWebView;->a:Lcom/sigmob/sdk/mraid/MraidWebView$a;

    return-void
.end method
