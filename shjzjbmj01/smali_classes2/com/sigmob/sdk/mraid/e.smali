.class public enum Lcom/sigmob/sdk/mraid/e;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/sigmob/sdk/mraid/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sigmob/sdk/mraid/e;

.field public static final enum b:Lcom/sigmob/sdk/mraid/e;

.field public static final enum c:Lcom/sigmob/sdk/mraid/e;

.field public static final enum d:Lcom/sigmob/sdk/mraid/e;

.field public static final enum e:Lcom/sigmob/sdk/mraid/e;

.field public static final enum f:Lcom/sigmob/sdk/mraid/e;

.field public static final enum g:Lcom/sigmob/sdk/mraid/e;

.field public static final enum h:Lcom/sigmob/sdk/mraid/e;

.field public static final enum i:Lcom/sigmob/sdk/mraid/e;

.field public static final enum j:Lcom/sigmob/sdk/mraid/e;

.field public static final enum k:Lcom/sigmob/sdk/mraid/e;

.field public static final enum l:Lcom/sigmob/sdk/mraid/e;

.field private static final synthetic n:[Lcom/sigmob/sdk/mraid/e;


# instance fields
.field private final m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    new-instance v0, Lcom/sigmob/sdk/mraid/e;

    const-string v1, "CLOSE"

    const-string v2, "close"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/sigmob/sdk/mraid/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->a:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e$1;

    const-string v1, "EXPAND"

    const-string v2, "expand"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sigmob/sdk/mraid/e$1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->b:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e;

    const-string v1, "USE_CUSTOM_CLOSE"

    const-string v2, "usecustomclose"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/sigmob/sdk/mraid/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->c:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e$2;

    const-string v1, "OPEN"

    const-string v2, "open"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/sigmob/sdk/mraid/e$2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->d:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e$3;

    const-string v1, "RESIZE"

    const-string v2, "resize"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/sigmob/sdk/mraid/e$3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->e:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e;

    const-string v1, "SET_ORIENTATION_PROPERTIES"

    const-string v2, "setOrientationProperties"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lcom/sigmob/sdk/mraid/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->f:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e$4;

    const-string v1, "PLAY_VIDEO"

    const-string v2, "playVideo"

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2}, Lcom/sigmob/sdk/mraid/e$4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->g:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e$5;

    const-string v1, "STORE_PICTURE"

    const-string v2, "storePicture"

    const/4 v10, 0x7

    invoke-direct {v0, v1, v10, v2}, Lcom/sigmob/sdk/mraid/e$5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->h:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e$6;

    const-string v1, "CREATE_CALENDAR_EVENT"

    const-string v2, "createCalendarEvent"

    const/16 v11, 0x8

    invoke-direct {v0, v1, v11, v2}, Lcom/sigmob/sdk/mraid/e$6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->i:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e$7;

    const-string v1, "VPAID"

    const-string v2, "vpaid"

    const/16 v12, 0x9

    invoke-direct {v0, v1, v12, v2}, Lcom/sigmob/sdk/mraid/e$7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->j:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e$8;

    const-string v1, "EXTENSION"

    const-string v2, "extension"

    const/16 v13, 0xa

    invoke-direct {v0, v1, v13, v2}, Lcom/sigmob/sdk/mraid/e$8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->k:Lcom/sigmob/sdk/mraid/e;

    new-instance v0, Lcom/sigmob/sdk/mraid/e;

    const-string v1, "UNSPECIFIED"

    const-string v2, ""

    const/16 v14, 0xb

    invoke-direct {v0, v1, v14, v2}, Lcom/sigmob/sdk/mraid/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->l:Lcom/sigmob/sdk/mraid/e;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/sigmob/sdk/mraid/e;

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->a:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->b:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->c:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->d:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->e:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->f:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->g:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v9

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->h:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->i:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->j:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v12

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->k:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v13

    sget-object v1, Lcom/sigmob/sdk/mraid/e;->l:Lcom/sigmob/sdk/mraid/e;

    aput-object v1, v0, v14

    sput-object v0, Lcom/sigmob/sdk/mraid/e;->n:[Lcom/sigmob/sdk/mraid/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/sigmob/sdk/mraid/e;->m:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/sigmob/sdk/mraid/e$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sigmob/sdk/mraid/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/sigmob/sdk/mraid/e;
    .locals 5

    invoke-static {}, Lcom/sigmob/sdk/mraid/e;->values()[Lcom/sigmob/sdk/mraid/e;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget-object v4, v3, Lcom/sigmob/sdk/mraid/e;->m:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/sigmob/sdk/mraid/e;->l:Lcom/sigmob/sdk/mraid/e;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sigmob/sdk/mraid/e;
    .locals 1

    const-class v0, Lcom/sigmob/sdk/mraid/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/sigmob/sdk/mraid/e;

    return-object p0
.end method

.method public static values()[Lcom/sigmob/sdk/mraid/e;
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/mraid/e;->n:[Lcom/sigmob/sdk/mraid/e;

    invoke-virtual {v0}, [Lcom/sigmob/sdk/mraid/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sigmob/sdk/mraid/e;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sigmob/sdk/mraid/e;->m:Ljava/lang/String;

    return-object v0
.end method
