.class public Lcom/sigmob/sdk/videoAd/e;
.super Lcom/sigmob/sdk/base/common/e;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sigmob/sdk/base/common/e;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable<",
        "Lcom/sigmob/sdk/videoAd/e;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final a:F


# direct methods
.method constructor <init>(Lcom/sigmob/sdk/base/common/e$a;Ljava/lang/String;F)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/sigmob/sdk/base/common/e;-><init>(Lcom/sigmob/sdk/base/common/e$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    cmpl-float p1, p3, p1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Lcom/sigmob/sdk/common/f/m$a;->a(Z)Z

    iput p3, p0, Lcom/sigmob/sdk/videoAd/e;->a:F

    return-void
.end method

.method constructor <init>(Ljava/lang/String;F)V
    .locals 1

    sget-object v0, Lcom/sigmob/sdk/base/common/e$a;->b:Lcom/sigmob/sdk/base/common/e$a;

    invoke-direct {p0, v0, p1, p2}, Lcom/sigmob/sdk/videoAd/e;-><init>(Lcom/sigmob/sdk/base/common/e$a;Ljava/lang/String;F)V

    return-void
.end method

.method private l()F
    .locals 1

    iget v0, p0, Lcom/sigmob/sdk/videoAd/e;->a:F

    return v0
.end method


# virtual methods
.method public a(Lcom/sigmob/sdk/videoAd/e;)I
    .locals 4

    invoke-direct {p1}, Lcom/sigmob/sdk/videoAd/e;->l()F

    move-result p1

    invoke-direct {p0}, Lcom/sigmob/sdk/videoAd/e;->l()F

    move-result v0

    float-to-double v0, v0

    float-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result p1

    return p1
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/sigmob/sdk/videoAd/e;

    invoke-virtual {p0, p1}, Lcom/sigmob/sdk/videoAd/e;->a(Lcom/sigmob/sdk/videoAd/e;)I

    move-result p1

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%2f: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/sigmob/sdk/videoAd/e;->a:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lcom/sigmob/sdk/videoAd/e;->g()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
