.class public final Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange$Builder;
.super Lcom/sigmob/wire/Message$Builder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sigmob/wire/Message$Builder<",
        "Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange;",
        "Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public end:Ljava/lang/Integer;

.field public start:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sigmob/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/sigmob/wire/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange$Builder;->build()Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange;
    .locals 4

    new-instance v0, Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange;

    iget-object v1, p0, Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange$Builder;->start:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange$Builder;->end:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/sigmob/wire/Message$Builder;->buildUnknownFields()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/sigmob/wire/okio/ByteString;)V

    return-object v0
.end method

.method public end(Ljava/lang/Integer;)Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange$Builder;
    .locals 0

    iput-object p1, p0, Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange$Builder;->end:Ljava/lang/Integer;

    return-object p0
.end method

.method public start(Ljava/lang/Integer;)Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange$Builder;
    .locals 0

    iput-object p1, p0, Lcom/sigmob/wire/protobuf/DescriptorProto$ExtensionRange$Builder;->start:Ljava/lang/Integer;

    return-object p0
.end method
