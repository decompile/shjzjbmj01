.class public final Lcom/sigmob/wire/protobuf/OneofDescriptorProto$Builder;
.super Lcom/sigmob/wire/Message$Builder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sigmob/wire/protobuf/OneofDescriptorProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sigmob/wire/Message$Builder<",
        "Lcom/sigmob/wire/protobuf/OneofDescriptorProto;",
        "Lcom/sigmob/wire/protobuf/OneofDescriptorProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sigmob/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/sigmob/wire/Message;
    .locals 1

    invoke-virtual {p0}, Lcom/sigmob/wire/protobuf/OneofDescriptorProto$Builder;->build()Lcom/sigmob/wire/protobuf/OneofDescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sigmob/wire/protobuf/OneofDescriptorProto;
    .locals 3

    new-instance v0, Lcom/sigmob/wire/protobuf/OneofDescriptorProto;

    iget-object v1, p0, Lcom/sigmob/wire/protobuf/OneofDescriptorProto$Builder;->name:Ljava/lang/String;

    invoke-super {p0}, Lcom/sigmob/wire/Message$Builder;->buildUnknownFields()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sigmob/wire/protobuf/OneofDescriptorProto;-><init>(Ljava/lang/String;Lcom/sigmob/wire/okio/ByteString;)V

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/sigmob/wire/protobuf/OneofDescriptorProto$Builder;
    .locals 0

    iput-object p1, p0, Lcom/sigmob/wire/protobuf/OneofDescriptorProto$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
