.class public final Lcom/sigmob/wire/ProtoReader;
.super Ljava/lang/Object;


# static fields
.field private static final FIELD_ENCODING_MASK:I = 0x7

.field private static final RECURSION_LIMIT:I = 0x41

.field private static final STATE_END_GROUP:I = 0x4

.field private static final STATE_FIXED32:I = 0x5

.field private static final STATE_FIXED64:I = 0x1

.field private static final STATE_LENGTH_DELIMITED:I = 0x2

.field private static final STATE_PACKED_TAG:I = 0x7

.field private static final STATE_START_GROUP:I = 0x3

.field private static final STATE_TAG:I = 0x6

.field private static final STATE_VARINT:I = 0x0

.field static final TAG_FIELD_ENCODING_BITS:I = 0x3


# instance fields
.field private limit:J

.field private nextFieldEncoding:Lcom/sigmob/wire/FieldEncoding;

.field private pos:J

.field private pushedLimit:J

.field private recursionDepth:I

.field private final source:Lcom/sigmob/wire/okio/BufferedSource;

.field private state:I

.field private tag:I


# direct methods
.method public constructor <init>(Lcom/sigmob/wire/okio/BufferedSource;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    const/4 v0, 0x2

    iput v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sigmob/wire/ProtoReader;->tag:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    iput-object p1, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    return-void
.end method

.method private afterPackableScalar(I)V
    .locals 6

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x6

    if-ne v0, p1, :cond_0

    :goto_0
    iput v1, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    goto :goto_1

    :cond_0
    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-wide v4, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    cmp-long p1, v2, v4

    if-gtz p1, :cond_2

    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-wide v4, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    cmp-long p1, v2, v4

    if-nez p1, :cond_1

    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    iput-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    goto :goto_0

    :cond_1
    const/4 p1, 0x7

    iput p1, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    :goto_1
    return-void

    :cond_2
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected to end at "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " but was "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private beforeLengthDelimitedScalar()J
    .locals 4

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v2, v0, v1}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    const/4 v2, 0x6

    iput v2, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    iput-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    iput-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    return-wide v0

    :cond_0
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected LENGTH_DELIMITED but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private copyTag(Lcom/sigmob/wire/ProtoWriter;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/sigmob/wire/ProtoReader;->peekFieldEncoding()Lcom/sigmob/wire/FieldEncoding;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/FieldEncoding;->rawProtoAdapter()Lcom/sigmob/wire/ProtoAdapter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sigmob/wire/ProtoAdapter;->decode(Lcom/sigmob/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v1

    :try_start_0
    invoke-virtual {v0, p1, p2, v1}, Lcom/sigmob/wire/ProtoAdapter;->encodeWithTag(Lcom/sigmob/wire/ProtoWriter;ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/AssertionError;

    invoke-direct {p2, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p2
.end method

.method private internalReadVarint32()I
    .locals 6

    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    const-wide/16 v1, 0x1

    invoke-interface {v0, v1, v2}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v0}, Lcom/sigmob/wire/okio/BufferedSource;->readByte()B

    move-result v0

    if-ltz v0, :cond_0

    return v0

    :cond_0
    and-int/lit8 v0, v0, 0x7f

    iget-object v3, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v3, v1, v2}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v3, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v3}, Lcom/sigmob/wire/okio/BufferedSource;->readByte()B

    move-result v3

    if-ltz v3, :cond_1

    shl-int/lit8 v1, v3, 0x7

    :goto_0
    or-int/2addr v0, v1

    goto :goto_2

    :cond_1
    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0x7

    or-int/2addr v0, v3

    iget-object v3, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v3, v1, v2}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v3, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v3}, Lcom/sigmob/wire/okio/BufferedSource;->readByte()B

    move-result v3

    if-ltz v3, :cond_2

    shl-int/lit8 v1, v3, 0xe

    goto :goto_0

    :cond_2
    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0xe

    or-int/2addr v0, v3

    iget-object v3, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v3, v1, v2}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v3, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v3}, Lcom/sigmob/wire/okio/BufferedSource;->readByte()B

    move-result v3

    if-ltz v3, :cond_3

    shl-int/lit8 v1, v3, 0x15

    goto :goto_0

    :cond_3
    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0x15

    or-int/2addr v0, v3

    iget-object v3, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v3, v1, v2}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v3, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v3}, Lcom/sigmob/wire/okio/BufferedSource;->readByte()B

    move-result v3

    shl-int/lit8 v4, v3, 0x1c

    or-int/2addr v0, v4

    if-gez v3, :cond_6

    const/4 v3, 0x0

    :goto_1
    const/4 v4, 0x5

    if-ge v3, v4, :cond_5

    iget-object v4, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v4, v1, v2}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-wide v4, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    add-long/2addr v4, v1

    iput-wide v4, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v4, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v4}, Lcom/sigmob/wire/okio/BufferedSource;->readByte()B

    move-result v4

    if-ltz v4, :cond_4

    return v0

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Malformed VARINT"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_2
    return v0
.end method

.method private skipGroup(I)V
    .locals 5

    :goto_0
    iget-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    cmp-long v4, v0, v2

    if-gez v4, :cond_2

    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v0}, Lcom/sigmob/wire/okio/BufferedSource;->exhausted()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/sigmob/wire/ProtoReader;->internalReadVarint32()I

    move-result v0

    if-eqz v0, :cond_1

    shr-int/lit8 v1, v0, 0x3

    and-int/lit8 v0, v0, 0x7

    packed-switch v0, :pswitch_data_0

    new-instance p1, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected field encoding: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    invoke-virtual {p0}, Lcom/sigmob/wire/ProtoReader;->readFixed32()I

    goto :goto_0

    :pswitch_1
    if-ne v1, p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/net/ProtocolException;

    const-string v0, "Unexpected end group"

    invoke-direct {p1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_2
    invoke-direct {p0, v1}, Lcom/sigmob/wire/ProtoReader;->skipGroup(I)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/sigmob/wire/ProtoReader;->internalReadVarint32()I

    move-result v0

    iget-wide v1, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v0, v3, v4}, Lcom/sigmob/wire/okio/BufferedSource;->skip(J)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x1

    iput v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    invoke-virtual {p0}, Lcom/sigmob/wire/ProtoReader;->readFixed64()J

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x0

    iput v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    invoke-virtual {p0}, Lcom/sigmob/wire/ProtoReader;->readVarint64()J

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/net/ProtocolException;

    const-string v0, "Unexpected tag 0"

    invoke-direct {p1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public beginMessage()J
    .locals 4

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->recursionDepth:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sigmob/wire/ProtoReader;->recursionDepth:I

    const/16 v1, 0x41

    if-gt v0, v1, :cond_0

    iget-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    const/4 v2, 0x6

    iput v2, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    return-wide v0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Wire recursion limit exceeded"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call to beginMessage()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public endMessage(J)V
    .locals 5

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->recursionDepth:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sigmob/wire/ProtoReader;->recursionDepth:I

    if-ltz v0, :cond_2

    iget-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    iget-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->recursionDepth:I

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected to end at "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " but was "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iput-wide p1, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No corresponding call to beginMessage()"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unexpected call to endMessage()"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public forEachTag(Lcom/sigmob/wire/TagHandler;)Lcom/sigmob/wire/okio/ByteString;
    .locals 7

    invoke-virtual {p0}, Lcom/sigmob/wire/ProtoReader;->beginMessage()J

    move-result-wide v0

    const/4 v2, 0x0

    move-object v3, v2

    :goto_0
    invoke-virtual {p0}, Lcom/sigmob/wire/ProtoReader;->nextTag()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    invoke-interface {p1, v4}, Lcom/sigmob/wire/TagHandler;->decodeMessage(I)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lcom/sigmob/wire/TagHandler;->UNKNOWN_TAG:Ljava/lang/Object;

    if-eq v5, v6, :cond_0

    goto :goto_0

    :cond_0
    if-nez v2, :cond_1

    new-instance v2, Lcom/sigmob/wire/okio/Buffer;

    invoke-direct {v2}, Lcom/sigmob/wire/okio/Buffer;-><init>()V

    new-instance v3, Lcom/sigmob/wire/ProtoWriter;

    invoke-direct {v3, v2}, Lcom/sigmob/wire/ProtoWriter;-><init>(Lcom/sigmob/wire/okio/BufferedSink;)V

    :cond_1
    invoke-direct {p0, v3, v4}, Lcom/sigmob/wire/ProtoReader;->copyTag(Lcom/sigmob/wire/ProtoWriter;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0, v1}, Lcom/sigmob/wire/ProtoReader;->endMessage(J)V

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/sigmob/wire/okio/Buffer;->readByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object p1

    goto :goto_1

    :cond_3
    sget-object p1, Lcom/sigmob/wire/okio/ByteString;->EMPTY:Lcom/sigmob/wire/okio/ByteString;

    :goto_1
    return-object p1
.end method

.method public nextTag()I
    .locals 6

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x2

    const/4 v2, 0x7

    if-ne v0, v2, :cond_0

    iput v1, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    :goto_0
    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->tag:I

    return v0

    :cond_0
    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_6

    :goto_1
    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-wide v4, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_5

    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v0}, Lcom/sigmob/wire/okio/BufferedSource;->exhausted()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/sigmob/wire/ProtoReader;->internalReadVarint32()I

    move-result v0

    if-eqz v0, :cond_4

    shr-int/lit8 v2, v0, 0x3

    iput v2, p0, Lcom/sigmob/wire/ProtoReader;->tag:I

    and-int/lit8 v0, v0, 0x7

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected field encoding: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    sget-object v0, Lcom/sigmob/wire/FieldEncoding;->FIXED32:Lcom/sigmob/wire/FieldEncoding;

    iput-object v0, p0, Lcom/sigmob/wire/ProtoReader;->nextFieldEncoding:Lcom/sigmob/wire/FieldEncoding;

    const/4 v0, 0x5

    :goto_2
    iput v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    goto :goto_0

    :pswitch_1
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Unexpected end group"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->tag:I

    invoke-direct {p0, v0}, Lcom/sigmob/wire/ProtoReader;->skipGroup(I)V

    goto :goto_1

    :pswitch_3
    sget-object v0, Lcom/sigmob/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/sigmob/wire/FieldEncoding;

    iput-object v0, p0, Lcom/sigmob/wire/ProtoReader;->nextFieldEncoding:Lcom/sigmob/wire/FieldEncoding;

    iput v1, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    invoke-direct {p0}, Lcom/sigmob/wire/ProtoReader;->internalReadVarint32()I

    move-result v0

    if-ltz v0, :cond_3

    iget-wide v1, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    const-wide/16 v3, -0x1

    cmp-long v5, v1, v3

    if-nez v5, :cond_2

    iget-wide v1, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    iput-wide v1, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    iget-wide v1, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    iget-wide v0, p0, Lcom/sigmob/wire/ProtoReader;->limit:J

    iget-wide v2, p0, Lcom/sigmob/wire/ProtoReader;->pushedLimit:J

    cmp-long v4, v0, v2

    if-gtz v4, :cond_1

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->tag:I

    return v0

    :cond_1
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Negative length: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_4
    sget-object v0, Lcom/sigmob/wire/FieldEncoding;->FIXED64:Lcom/sigmob/wire/FieldEncoding;

    iput-object v0, p0, Lcom/sigmob/wire/ProtoReader;->nextFieldEncoding:Lcom/sigmob/wire/FieldEncoding;

    const/4 v0, 0x1

    goto :goto_2

    :pswitch_5
    sget-object v0, Lcom/sigmob/wire/FieldEncoding;->VARINT:Lcom/sigmob/wire/FieldEncoding;

    iput-object v0, p0, Lcom/sigmob/wire/ProtoReader;->nextFieldEncoding:Lcom/sigmob/wire/FieldEncoding;

    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Unexpected tag 0"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/4 v0, -0x1

    return v0

    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call to nextTag()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public peekFieldEncoding()Lcom/sigmob/wire/FieldEncoding;
    .locals 1

    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->nextFieldEncoding:Lcom/sigmob/wire/FieldEncoding;

    return-object v0
.end method

.method public readBytes()Lcom/sigmob/wire/okio/ByteString;
    .locals 3

    invoke-direct {p0}, Lcom/sigmob/wire/ProtoReader;->beforeLengthDelimitedScalar()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v2, v0, v1}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-object v2, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v2, v0, v1}, Lcom/sigmob/wire/okio/BufferedSource;->readByteString(J)Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public readFixed32()I
    .locals 6

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected FIXED32 or LENGTH_DELIMITED but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    const-wide/16 v2, 0x4

    invoke-interface {v0, v2, v3}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-wide v4, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v0}, Lcom/sigmob/wire/okio/BufferedSource;->readIntLe()I

    move-result v0

    invoke-direct {p0, v1}, Lcom/sigmob/wire/ProtoReader;->afterPackableScalar(I)V

    return v0
.end method

.method public readFixed64()J
    .locals 6

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected FIXED64 or LENGTH_DELIMITED but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    const-wide/16 v2, 0x8

    invoke-interface {v0, v2, v3}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-wide v4, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v0, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v0}, Lcom/sigmob/wire/okio/BufferedSource;->readLongLe()J

    move-result-wide v2

    invoke-direct {p0, v1}, Lcom/sigmob/wire/ProtoReader;->afterPackableScalar(I)V

    return-wide v2
.end method

.method public readString()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/sigmob/wire/ProtoReader;->beforeLengthDelimitedScalar()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v2, v0, v1}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-object v2, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v2, v0, v1}, Lcom/sigmob/wire/okio/BufferedSource;->readUtf8(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readVarint32()I
    .locals 3

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected VARINT or LENGTH_DELIMITED but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sigmob/wire/ProtoReader;->internalReadVarint32()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sigmob/wire/ProtoReader;->afterPackableScalar(I)V

    return v0
.end method

.method public readVarint64()J
    .locals 9

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected VARINT or LENGTH_DELIMITED but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    move-wide v3, v0

    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0x40

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    const-wide/16 v5, 0x1

    invoke-interface {v1, v5, v6}, Lcom/sigmob/wire/okio/BufferedSource;->require(J)V

    iget-wide v7, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    add-long/2addr v7, v5

    iput-wide v7, p0, Lcom/sigmob/wire/ProtoReader;->pos:J

    iget-object v1, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v1}, Lcom/sigmob/wire/okio/BufferedSource;->readByte()B

    move-result v1

    and-int/lit8 v5, v1, 0x7f

    int-to-long v5, v5

    shl-long/2addr v5, v0

    or-long/2addr v3, v5

    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_2

    invoke-direct {p0, v2}, Lcom/sigmob/wire/ProtoReader;->afterPackableScalar(I)V

    return-wide v3

    :cond_2
    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "WireInput encountered a malformed varint"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public skip()V
    .locals 3

    iget v0, p0, Lcom/sigmob/wire/ProtoReader;->state:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call to skip()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-direct {p0}, Lcom/sigmob/wire/ProtoReader;->beforeLengthDelimitedScalar()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sigmob/wire/ProtoReader;->source:Lcom/sigmob/wire/okio/BufferedSource;

    invoke-interface {v2, v0, v1}, Lcom/sigmob/wire/okio/BufferedSource;->skip(J)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/sigmob/wire/ProtoReader;->readFixed64()J

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/sigmob/wire/ProtoReader;->readVarint64()J

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/sigmob/wire/ProtoReader;->readFixed32()I

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
