.class public Lcom/sigmob/wire/okio/AsyncTimeout;
.super Lcom/sigmob/wire/okio/Timeout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sigmob/wire/okio/AsyncTimeout$Watchdog;
    }
.end annotation


# static fields
.field private static final TIMEOUT_WRITE_SIZE:I = 0x10000

.field private static head:Lcom/sigmob/wire/okio/AsyncTimeout;


# instance fields
.field private inQueue:Z

.field private next:Lcom/sigmob/wire/okio/AsyncTimeout;

.field private timeoutAt:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sigmob/wire/okio/Timeout;-><init>()V

    return-void
.end method

.method static declared-synchronized awaitTimeout()Lcom/sigmob/wire/okio/AsyncTimeout;
    .locals 9

    const-class v0, Lcom/sigmob/wire/okio/AsyncTimeout;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sigmob/wire/okio/AsyncTimeout;->head:Lcom/sigmob/wire/okio/AsyncTimeout;

    iget-object v1, v1, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const-class v1, Lcom/sigmob/wire/okio/AsyncTimeout;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v2

    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Lcom/sigmob/wire/okio/AsyncTimeout;->remainingNanos(J)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-lez v7, :cond_1

    const-wide/32 v5, 0xf4240

    div-long v7, v3, v5

    mul-long v5, v5, v7

    const/4 v1, 0x0

    sub-long/2addr v3, v5

    const-class v1, Lcom/sigmob/wire/okio/AsyncTimeout;

    long-to-int v3, v3

    invoke-virtual {v1, v7, v8, v3}, Ljava/lang/Object;->wait(JI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-object v2

    :cond_1
    :try_start_2
    sget-object v3, Lcom/sigmob/wire/okio/AsyncTimeout;->head:Lcom/sigmob/wire/okio/AsyncTimeout;

    iget-object v4, v1, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    iput-object v4, v3, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    iput-object v2, v1, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static declared-synchronized cancelScheduledTimeout(Lcom/sigmob/wire/okio/AsyncTimeout;)Z
    .locals 3

    const-class v0, Lcom/sigmob/wire/okio/AsyncTimeout;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sigmob/wire/okio/AsyncTimeout;->head:Lcom/sigmob/wire/okio/AsyncTimeout;

    :goto_0
    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    if-ne v2, p0, :cond_0

    iget-object v2, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    iput-object v2, v1, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p0, 0x0

    :goto_1
    monitor-exit v0

    return p0

    :cond_0
    :try_start_1
    iget-object v1, v1, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    goto :goto_1

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private remainingNanos(J)J
    .locals 2

    iget-wide v0, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->timeoutAt:J

    sub-long/2addr v0, p1

    return-wide v0
.end method

.method private static declared-synchronized scheduleTimeout(Lcom/sigmob/wire/okio/AsyncTimeout;JZ)V
    .locals 6

    const-class v0, Lcom/sigmob/wire/okio/AsyncTimeout;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sigmob/wire/okio/AsyncTimeout;->head:Lcom/sigmob/wire/okio/AsyncTimeout;

    if-nez v1, :cond_0

    new-instance v1, Lcom/sigmob/wire/okio/AsyncTimeout;

    invoke-direct {v1}, Lcom/sigmob/wire/okio/AsyncTimeout;-><init>()V

    sput-object v1, Lcom/sigmob/wire/okio/AsyncTimeout;->head:Lcom/sigmob/wire/okio/AsyncTimeout;

    new-instance v1, Lcom/sigmob/wire/okio/AsyncTimeout$Watchdog;

    invoke-direct {v1}, Lcom/sigmob/wire/okio/AsyncTimeout$Watchdog;-><init>()V

    invoke-virtual {v1}, Lcom/sigmob/wire/okio/AsyncTimeout$Watchdog;->start()V

    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, p1, v3

    if-eqz v5, :cond_1

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/AsyncTimeout;->deadlineNanoTime()J

    move-result-wide v3

    const/4 p3, 0x0

    sub-long/2addr v3, v1

    invoke-static {p1, p2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    const/4 p3, 0x0

    add-long/2addr p1, v1

    iput-wide p1, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->timeoutAt:J

    goto :goto_0

    :cond_1
    cmp-long v5, p1, v3

    if-eqz v5, :cond_2

    add-long/2addr p1, v1

    iput-wide p1, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->timeoutAt:J

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_6

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/AsyncTimeout;->deadlineNanoTime()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->timeoutAt:J

    :goto_0
    invoke-direct {p0, v1, v2}, Lcom/sigmob/wire/okio/AsyncTimeout;->remainingNanos(J)J

    move-result-wide p1

    sget-object p3, Lcom/sigmob/wire/okio/AsyncTimeout;->head:Lcom/sigmob/wire/okio/AsyncTimeout;

    :goto_1
    iget-object v3, p3, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    if-eqz v3, :cond_4

    iget-object v3, p3, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    invoke-direct {v3, v1, v2}, Lcom/sigmob/wire/okio/AsyncTimeout;->remainingNanos(J)J

    move-result-wide v3

    cmp-long v5, p1, v3

    if-gez v5, :cond_3

    goto :goto_2

    :cond_3
    iget-object p3, p3, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    goto :goto_1

    :cond_4
    :goto_2
    iget-object p1, p3, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    iput-object p1, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    iput-object p0, p3, Lcom/sigmob/wire/okio/AsyncTimeout;->next:Lcom/sigmob/wire/okio/AsyncTimeout;

    sget-object p0, Lcom/sigmob/wire/okio/AsyncTimeout;->head:Lcom/sigmob/wire/okio/AsyncTimeout;

    if-ne p3, p0, :cond_5

    const-class p0, Lcom/sigmob/wire/okio/AsyncTimeout;

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    monitor-exit v0

    return-void

    :cond_6
    :try_start_1
    new-instance p0, Ljava/lang/AssertionError;

    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public final enter()V
    .locals 6

    iget-boolean v0, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->inQueue:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/AsyncTimeout;->timeoutNanos()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/AsyncTimeout;->hasDeadline()Z

    move-result v2

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-nez v5, :cond_0

    if-nez v2, :cond_0

    return-void

    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->inQueue:Z

    invoke-static {p0, v0, v1, v2}, Lcom/sigmob/wire/okio/AsyncTimeout;->scheduleTimeout(Lcom/sigmob/wire/okio/AsyncTimeout;JZ)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unbalanced enter/exit"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final exit(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 1

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/AsyncTimeout;->exit()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/sigmob/wire/okio/AsyncTimeout;->newTimeoutException(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    return-object p1
.end method

.method final exit(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/AsyncTimeout;->exit()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/sigmob/wire/okio/AsyncTimeout;->newTimeoutException(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public final exit()Z
    .locals 2

    iget-boolean v0, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->inQueue:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iput-boolean v1, p0, Lcom/sigmob/wire/okio/AsyncTimeout;->inQueue:Z

    invoke-static {p0}, Lcom/sigmob/wire/okio/AsyncTimeout;->cancelScheduledTimeout(Lcom/sigmob/wire/okio/AsyncTimeout;)Z

    move-result v0

    return v0
.end method

.method protected newTimeoutException(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2

    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_0
    return-object v0
.end method

.method public final sink(Lcom/sigmob/wire/okio/Sink;)Lcom/sigmob/wire/okio/Sink;
    .locals 1

    new-instance v0, Lcom/sigmob/wire/okio/AsyncTimeout$1;

    invoke-direct {v0, p0, p1}, Lcom/sigmob/wire/okio/AsyncTimeout$1;-><init>(Lcom/sigmob/wire/okio/AsyncTimeout;Lcom/sigmob/wire/okio/Sink;)V

    return-object v0
.end method

.method public final source(Lcom/sigmob/wire/okio/Source;)Lcom/sigmob/wire/okio/Source;
    .locals 1

    new-instance v0, Lcom/sigmob/wire/okio/AsyncTimeout$2;

    invoke-direct {v0, p0, p1}, Lcom/sigmob/wire/okio/AsyncTimeout$2;-><init>(Lcom/sigmob/wire/okio/AsyncTimeout;Lcom/sigmob/wire/okio/Source;)V

    return-object v0
.end method

.method protected timedOut()V
    .locals 0

    return-void
.end method
