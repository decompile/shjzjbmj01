.class final Lcom/sigmob/wire/okio/SegmentedByteString;
.super Lcom/sigmob/wire/okio/ByteString;


# instance fields
.field final transient directory:[I

.field final transient segments:[[B


# direct methods
.method constructor <init>(Lcom/sigmob/wire/okio/Buffer;I)V
    .locals 7

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sigmob/wire/okio/ByteString;-><init>([B)V

    iget-wide v1, p1, Lcom/sigmob/wire/okio/Buffer;->size:J

    int-to-long v5, p2

    const-wide/16 v3, 0x0

    invoke-static/range {v1 .. v6}, Lcom/sigmob/wire/okio/Util;->checkOffsetAndCount(JJJ)V

    iget-object v0, p1, Lcom/sigmob/wire/okio/Buffer;->head:Lcom/sigmob/wire/okio/Segment;

    const/4 v1, 0x0

    move-object v2, v0

    const/4 v0, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    iget v4, v2, Lcom/sigmob/wire/okio/Segment;->limit:I

    iget v5, v2, Lcom/sigmob/wire/okio/Segment;->pos:I

    if-eq v4, v5, :cond_0

    iget v4, v2, Lcom/sigmob/wire/okio/Segment;->limit:I

    iget v5, v2, Lcom/sigmob/wire/okio/Segment;->pos:I

    sub-int/2addr v4, v5

    add-int/2addr v0, v4

    add-int/lit8 v3, v3, 0x1

    iget-object v2, v2, Lcom/sigmob/wire/okio/Segment;->next:Lcom/sigmob/wire/okio/Segment;

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "s.limit == s.pos"

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    new-array v0, v3, [[B

    iput-object v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    mul-int/lit8 v3, v3, 0x2

    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    iget-object p1, p1, Lcom/sigmob/wire/okio/Buffer;->head:Lcom/sigmob/wire/okio/Segment;

    move-object v0, p1

    const/4 p1, 0x0

    :goto_1
    if-ge v1, p2, :cond_3

    iget-object v2, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    iget-object v3, v0, Lcom/sigmob/wire/okio/Segment;->data:[B

    aput-object v3, v2, p1

    iget v2, v0, Lcom/sigmob/wire/okio/Segment;->limit:I

    iget v3, v0, Lcom/sigmob/wire/okio/Segment;->pos:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    if-le v1, p2, :cond_2

    move v1, p2

    :cond_2
    iget-object v2, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    aput v1, v2, p1

    iget-object v2, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    iget-object v3, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v3, v3

    add-int/2addr v3, p1

    iget v4, v0, Lcom/sigmob/wire/okio/Segment;->pos:I

    aput v4, v2, v3

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/sigmob/wire/okio/Segment;->shared:Z

    add-int/lit8 p1, p1, 0x1

    iget-object v0, v0, Lcom/sigmob/wire/okio/Segment;->next:Lcom/sigmob/wire/okio/Segment;

    goto :goto_1

    :cond_3
    return-void
.end method

.method private segment(I)I
    .locals 3

    iget-object v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/lit8 p1, p1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1, p1}, Ljava/util/Arrays;->binarySearch([IIII)I

    move-result p1

    if-ltz p1, :cond_0

    goto :goto_0

    :cond_0
    xor-int/lit8 p1, p1, -0x1

    :goto_0
    return p1
.end method

.method private toByteString()Lcom/sigmob/wire/okio/ByteString;
    .locals 2

    new-instance v0, Lcom/sigmob/wire/okio/ByteString;

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sigmob/wire/okio/ByteString;-><init>([B)V

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public asByteBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asReadOnlyBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public base64()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->base64()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public base64Url()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->base64Url()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/sigmob/wire/okio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/sigmob/wire/okio/ByteString;

    invoke-virtual {p1}, Lcom/sigmob/wire/okio/ByteString;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->size()I

    move-result v3

    if-ne v1, v3, :cond_1

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->size()I

    move-result v1

    invoke-virtual {p0, v2, p1, v2, v1}, Lcom/sigmob/wire/okio/SegmentedByteString;->rangeEquals(ILcom/sigmob/wire/okio/ByteString;II)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getByte(I)B
    .locals 7

    iget-object v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    int-to-long v1, v0

    int-to-long v3, p1

    const-wide/16 v5, 0x1

    invoke-static/range {v1 .. v6}, Lcom/sigmob/wire/okio/Util;->checkOffsetAndCount(JJJ)V

    invoke-direct {p0, p1}, Lcom/sigmob/wire/okio/SegmentedByteString;->segment(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    add-int/lit8 v2, v0, -0x1

    aget v1, v1, v2

    :goto_0
    iget-object v2, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    iget-object v3, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v3, v3

    add-int/2addr v3, v0

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    aget-object v0, v3, v0

    sub-int/2addr p1, v1

    add-int/2addr p1, v2

    aget-byte p1, v0, p1

    return p1
.end method

.method public hashCode()I
    .locals 8

    iget v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->hashCode:I

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v4, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    aget-object v4, v4, v1

    iget-object v5, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    add-int v6, v0, v1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    aget v6, v6, v1

    sub-int v2, v6, v2

    add-int/2addr v2, v5

    :goto_1
    if-ge v5, v2, :cond_1

    mul-int/lit8 v3, v3, 0x1f

    aget-byte v7, v4, v5

    add-int/2addr v3, v7

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    move v2, v6

    goto :goto_0

    :cond_2
    iput v3, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->hashCode:I

    return v3
.end method

.method public hex()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->hex()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public indexOf([BI)I
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sigmob/wire/okio/ByteString;->indexOf([BI)I

    move-result p1

    return p1
.end method

.method internalArray()[B
    .locals 1

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf([BI)I
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sigmob/wire/okio/ByteString;->lastIndexOf([BI)I

    move-result p1

    return p1
.end method

.method public md5()Lcom/sigmob/wire/okio/ByteString;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->md5()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public rangeEquals(ILcom/sigmob/wire/okio/ByteString;II)Z
    .locals 6

    const/4 v0, 0x0

    if-ltz p1, :cond_4

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->size()I

    move-result v1

    sub-int/2addr v1, p4

    if-le p1, v1, :cond_0

    goto :goto_2

    :cond_0
    invoke-direct {p0, p1}, Lcom/sigmob/wire/okio/SegmentedByteString;->segment(I)I

    move-result v1

    :goto_0
    if-lez p4, :cond_3

    if-nez v1, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    add-int/lit8 v3, v1, -0x1

    aget v2, v2, v3

    :goto_1
    iget-object v3, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    aget v3, v3, v1

    sub-int/2addr v3, v2

    add-int/2addr v3, v2

    sub-int/2addr v3, p1

    invoke-static {p4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v4, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    iget-object v5, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v5, v5

    add-int/2addr v5, v1

    aget v4, v4, v5

    sub-int v2, p1, v2

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    aget-object v4, v4, v1

    invoke-virtual {p2, p3, v4, v2, v3}, Lcom/sigmob/wire/okio/ByteString;->rangeEquals(I[BII)Z

    move-result v2

    if-nez v2, :cond_2

    return v0

    :cond_2
    add-int/2addr p1, v3

    add-int/2addr p3, v3

    sub-int/2addr p4, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    return p1

    :cond_4
    :goto_2
    return v0
.end method

.method public rangeEquals(I[BII)Z
    .locals 6

    const/4 v0, 0x0

    if-ltz p1, :cond_4

    invoke-virtual {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->size()I

    move-result v1

    sub-int/2addr v1, p4

    if-gt p1, v1, :cond_4

    if-ltz p3, :cond_4

    array-length v1, p2

    sub-int/2addr v1, p4

    if-le p3, v1, :cond_0

    goto :goto_2

    :cond_0
    invoke-direct {p0, p1}, Lcom/sigmob/wire/okio/SegmentedByteString;->segment(I)I

    move-result v1

    :goto_0
    if-lez p4, :cond_3

    if-nez v1, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    add-int/lit8 v3, v1, -0x1

    aget v2, v2, v3

    :goto_1
    iget-object v3, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    aget v3, v3, v1

    sub-int/2addr v3, v2

    add-int/2addr v3, v2

    sub-int/2addr v3, p1

    invoke-static {p4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v4, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    iget-object v5, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v5, v5

    add-int/2addr v5, v1

    aget v4, v4, v5

    sub-int v2, p1, v2

    add-int/2addr v2, v4

    iget-object v4, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    aget-object v4, v4, v1

    invoke-static {v4, v2, p2, p3, v3}, Lcom/sigmob/wire/okio/Util;->arrayRangeEquals([BI[BII)Z

    move-result v2

    if-nez v2, :cond_2

    return v0

    :cond_2
    add-int/2addr p1, v3

    add-int/2addr p3, v3

    sub-int/2addr p4, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    return p1

    :cond_4
    :goto_2
    return v0
.end method

.method public sha256()Lcom/sigmob/wire/okio/ByteString;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->sha256()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 2

    iget-object v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method public substring(I)Lcom/sigmob/wire/okio/ByteString;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sigmob/wire/okio/ByteString;->substring(I)Lcom/sigmob/wire/okio/ByteString;

    move-result-object p1

    return-object p1
.end method

.method public substring(II)Lcom/sigmob/wire/okio/ByteString;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sigmob/wire/okio/ByteString;->substring(II)Lcom/sigmob/wire/okio/ByteString;

    move-result-object p1

    return-object p1
.end method

.method public toAsciiLowercase()Lcom/sigmob/wire/okio/ByteString;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->toAsciiLowercase()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public toAsciiUppercase()Lcom/sigmob/wire/okio/ByteString;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->toAsciiUppercase()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public toByteArray()[B
    .locals 8

    iget-object v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    new-array v0, v0, [B

    iget-object v1, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    iget-object v4, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    add-int v5, v1, v2

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    aget v5, v5, v2

    iget-object v6, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    aget-object v6, v6, v2

    sub-int v7, v5, v3

    invoke-static {v6, v4, v0, v3, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v2, 0x1

    move v3, v5

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public utf8()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sigmob/wire/okio/SegmentedByteString;->toByteString()Lcom/sigmob/wire/okio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sigmob/wire/okio/ByteString;->utf8()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method write(Lcom/sigmob/wire/okio/Buffer;)V
    .locals 8

    iget-object v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    add-int v4, v0, v1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    aget v4, v4, v1

    new-instance v5, Lcom/sigmob/wire/okio/Segment;

    iget-object v6, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    aget-object v6, v6, v1

    add-int v7, v3, v4

    sub-int/2addr v7, v2

    invoke-direct {v5, v6, v3, v7}, Lcom/sigmob/wire/okio/Segment;-><init>([BII)V

    iget-object v2, p1, Lcom/sigmob/wire/okio/Buffer;->head:Lcom/sigmob/wire/okio/Segment;

    if-nez v2, :cond_0

    iput-object v5, v5, Lcom/sigmob/wire/okio/Segment;->prev:Lcom/sigmob/wire/okio/Segment;

    iput-object v5, v5, Lcom/sigmob/wire/okio/Segment;->next:Lcom/sigmob/wire/okio/Segment;

    iput-object v5, p1, Lcom/sigmob/wire/okio/Buffer;->head:Lcom/sigmob/wire/okio/Segment;

    goto :goto_1

    :cond_0
    iget-object v2, p1, Lcom/sigmob/wire/okio/Buffer;->head:Lcom/sigmob/wire/okio/Segment;

    iget-object v2, v2, Lcom/sigmob/wire/okio/Segment;->prev:Lcom/sigmob/wire/okio/Segment;

    invoke-virtual {v2, v5}, Lcom/sigmob/wire/okio/Segment;->push(Lcom/sigmob/wire/okio/Segment;)Lcom/sigmob/wire/okio/Segment;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v2, v4

    goto :goto_0

    :cond_1
    iget-wide v0, p1, Lcom/sigmob/wire/okio/Buffer;->size:J

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p1, Lcom/sigmob/wire/okio/Buffer;->size:J

    return-void
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 6

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    add-int v4, v0, v1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->directory:[I

    aget v4, v4, v1

    iget-object v5, p0, Lcom/sigmob/wire/okio/SegmentedByteString;->segments:[[B

    aget-object v5, v5, v1

    sub-int v2, v4, v2

    invoke-virtual {p1, v5, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    add-int/lit8 v1, v1, 0x1

    move v2, v4

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "out == null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
