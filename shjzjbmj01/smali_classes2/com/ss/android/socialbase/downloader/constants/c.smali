.class public interface abstract Lcom/ss/android/socialbase/downloader/constants/c;
.super Ljava/lang/Object;
.source "DBDefinition.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field public static final d:[Ljava/lang/String;

.field public static final e:[Ljava/lang/String;

.field public static final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/16 v0, 0x27

    .line 113
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "url"

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const-string v1, "savePath"

    const/4 v4, 0x2

    aput-object v1, v0, v4

    const-string v1, "tempPath"

    const/4 v5, 0x3

    aput-object v1, v0, v5

    const-string v1, "name"

    const/4 v6, 0x4

    aput-object v1, v0, v6

    const-string v1, "chunkCount"

    const/4 v7, 0x5

    aput-object v1, v0, v7

    const-string v1, "status"

    const/4 v8, 0x6

    aput-object v1, v0, v8

    const-string v1, "curBytes"

    const/4 v9, 0x7

    aput-object v1, v0, v9

    const-string v1, "totalBytes"

    const/16 v10, 0x8

    aput-object v1, v0, v10

    const-string v1, "eTag"

    const/16 v10, 0x9

    aput-object v1, v0, v10

    const-string v1, "onlyWifi"

    const/16 v10, 0xa

    aput-object v1, v0, v10

    const-string v1, "force"

    const/16 v10, 0xb

    aput-object v1, v0, v10

    const-string v1, "retryCount"

    const/16 v10, 0xc

    aput-object v1, v0, v10

    const-string v1, "extra"

    const/16 v10, 0xd

    aput-object v1, v0, v10

    const-string v1, "mimeType"

    const/16 v10, 0xe

    aput-object v1, v0, v10

    const-string v1, "title"

    const/16 v10, 0xf

    aput-object v1, v0, v10

    const-string v1, "notificationEnable"

    const/16 v10, 0x10

    aput-object v1, v0, v10

    const-string v1, "notificationVisibility"

    const/16 v10, 0x11

    aput-object v1, v0, v10

    const-string v1, "isFirstDownload"

    const/16 v10, 0x12

    aput-object v1, v0, v10

    const-string v1, "isFirstSuccess"

    const/16 v10, 0x13

    aput-object v1, v0, v10

    const-string v1, "needHttpsToHttpRetry"

    const/16 v10, 0x14

    aput-object v1, v0, v10

    const-string v1, "downloadTime"

    const/16 v10, 0x15

    aput-object v1, v0, v10

    const-string v1, "packageName"

    const/16 v10, 0x16

    aput-object v1, v0, v10

    const-string v1, "md5"

    const/16 v10, 0x17

    aput-object v1, v0, v10

    const-string v1, "retryDelay"

    const/16 v10, 0x18

    aput-object v1, v0, v10

    const-string v1, "curRetryTime"

    const/16 v10, 0x19

    aput-object v1, v0, v10

    const-string v1, "retryDelayStatus"

    const/16 v10, 0x1a

    aput-object v1, v0, v10

    const-string v1, "defaultHttpServiceBackUp"

    const/16 v10, 0x1b

    aput-object v1, v0, v10

    const-string v1, "chunkRunnableReuse"

    const/16 v10, 0x1c

    aput-object v1, v0, v10

    const-string v1, "retryDelayTimeArray"

    const/16 v10, 0x1d

    aput-object v1, v0, v10

    const-string v1, "chunkDowngradeRetry"

    const/16 v10, 0x1e

    aput-object v1, v0, v10

    const-string v1, "backUpUrlsStr"

    const/16 v10, 0x1f

    aput-object v1, v0, v10

    const-string v1, "backUpUrlRetryCount"

    const/16 v10, 0x20

    aput-object v1, v0, v10

    const-string v1, "realDownloadTime"

    const/16 v10, 0x21

    aput-object v1, v0, v10

    const-string v1, "retryScheduleMinutes"

    const/16 v10, 0x22

    aput-object v1, v0, v10

    const-string v1, "independentProcess"

    const/16 v10, 0x23

    aput-object v1, v0, v10

    const-string v1, "auxiliaryJsonobjectString"

    const/16 v10, 0x24

    aput-object v1, v0, v10

    const-string v1, "iconUrl"

    const/16 v10, 0x25

    aput-object v1, v0, v10

    const-string v1, "appVersionCode"

    const/16 v10, 0x26

    aput-object v1, v0, v10

    sput-object v0, Lcom/ss/android/socialbase/downloader/constants/c;->a:[Ljava/lang/String;

    .line 155
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/ss/android/socialbase/downloader/constants/c;->b:[Ljava/lang/String;

    .line 181
    new-array v0, v9, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "chunkIndex"

    aput-object v1, v0, v3

    const-string v1, "startOffset"

    aput-object v1, v0, v4

    const-string v1, "curOffset"

    aput-object v1, v0, v5

    const-string v1, "endOffset"

    aput-object v1, v0, v6

    const-string v1, "chunkContentLen"

    aput-object v1, v0, v7

    const-string v1, "hostChunkIndex"

    aput-object v1, v0, v8

    sput-object v0, Lcom/ss/android/socialbase/downloader/constants/c;->c:[Ljava/lang/String;

    .line 191
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/ss/android/socialbase/downloader/constants/c;->d:[Ljava/lang/String;

    .line 206
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "info"

    aput-object v1, v0, v3

    sput-object v0, Lcom/ss/android/socialbase/downloader/constants/c;->e:[Ljava/lang/String;

    .line 211
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/ss/android/socialbase/downloader/constants/c;->f:[Ljava/lang/String;

    return-void
.end method
