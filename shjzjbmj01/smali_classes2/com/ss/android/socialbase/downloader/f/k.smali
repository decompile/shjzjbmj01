.class public Lcom/ss/android/socialbase/downloader/f/k;
.super Ljava/lang/Object;
.source "SegmentDispatcher.java"

# interfaces
.implements Lcom/ss/android/socialbase/downloader/f/f;
.implements Lcom/ss/android/socialbase/downloader/network/c$a;


# instance fields
.field private final A:Z

.field private final B:Lcom/ss/android/socialbase/downloader/h/e$b;

.field private final C:Lcom/ss/android/socialbase/downloader/h/e$b;

.field private final a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

.field private final b:Lcom/ss/android/socialbase/downloader/f/n;

.field private final c:Lcom/ss/android/socialbase/downloader/f/b;

.field private final d:Lcom/ss/android/socialbase/downloader/h/f;

.field private final e:Lcom/ss/android/socialbase/downloader/f/g;

.field private volatile f:Z

.field private volatile g:Z

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/m;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/q;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/ss/android/socialbase/downloader/model/d;

.field private k:Lcom/ss/android/socialbase/downloader/model/d;

.field private volatile l:Z

.field private m:J

.field private final n:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/ss/android/socialbase/downloader/f/i;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/i;",
            ">;"
        }
    .end annotation
.end field

.field private p:I

.field private q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

.field private volatile r:Z

.field private final s:Ljava/lang/Object;

.field private final t:Lcom/ss/android/socialbase/downloader/i/d;

.field private final u:Lcom/ss/android/socialbase/downloader/h/e;

.field private v:J

.field private w:J

.field private x:J

.field private y:F

.field private z:I


# direct methods
.method public constructor <init>(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/f/n;Lcom/ss/android/socialbase/downloader/h/f;)V
    .locals 4
    .param p1    # Lcom/ss/android/socialbase/downloader/model/DownloadInfo;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/ss/android/socialbase/downloader/f/n;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    .line 50
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    .line 52
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    .line 53
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    const/4 v1, 0x1

    .line 57
    iput-boolean v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->l:Z

    .line 64
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    .line 69
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    .line 77
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->s:Ljava/lang/Object;

    .line 1308
    new-instance v2, Lcom/ss/android/socialbase/downloader/f/k$1;

    invoke-direct {v2, p0}, Lcom/ss/android/socialbase/downloader/f/k$1;-><init>(Lcom/ss/android/socialbase/downloader/f/k;)V

    iput-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->B:Lcom/ss/android/socialbase/downloader/h/e$b;

    .line 1350
    new-instance v2, Lcom/ss/android/socialbase/downloader/f/k$2;

    invoke-direct {v2, p0}, Lcom/ss/android/socialbase/downloader/f/k$2;-><init>(Lcom/ss/android/socialbase/downloader/f/k;)V

    iput-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->C:Lcom/ss/android/socialbase/downloader/h/e$b;

    .line 92
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 93
    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    .line 94
    new-instance p2, Lcom/ss/android/socialbase/downloader/f/b;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/f/n;->d()I

    move-result v2

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/f/n;->e()I

    move-result v3

    invoke-direct {p2, v2, v3}, Lcom/ss/android/socialbase/downloader/f/b;-><init>(II)V

    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->c:Lcom/ss/android/socialbase/downloader/f/b;

    .line 95
    iput-object p3, p0, Lcom/ss/android/socialbase/downloader/f/k;->d:Lcom/ss/android/socialbase/downloader/h/f;

    .line 96
    new-instance p2, Lcom/ss/android/socialbase/downloader/f/g;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->c:Lcom/ss/android/socialbase/downloader/f/b;

    invoke-direct {p2, p1, p3, v2}, Lcom/ss/android/socialbase/downloader/f/g;-><init>(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/h/f;Lcom/ss/android/socialbase/downloader/f/c;)V

    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->e:Lcom/ss/android/socialbase/downloader/f/g;

    .line 97
    new-instance p2, Lcom/ss/android/socialbase/downloader/h/e;

    invoke-direct {p2}, Lcom/ss/android/socialbase/downloader/h/e;-><init>()V

    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->u:Lcom/ss/android/socialbase/downloader/h/e;

    .line 98
    new-instance p2, Lcom/ss/android/socialbase/downloader/i/d;

    invoke-direct {p2}, Lcom/ss/android/socialbase/downloader/i/d;-><init>()V

    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->t:Lcom/ss/android/socialbase/downloader/i/d;

    .line 99
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result p1

    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object p1

    const-string p2, "debug"

    invoke-virtual {p1, p2}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;)I

    move-result p1

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->A:Z

    return-void
.end method

.method private a(J)I
    .locals 6

    .line 1021
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    .line 1023
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/f/i;

    .line 1024
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v3

    cmp-long v5, v3, p1

    if-nez v5, :cond_0

    return v1

    .line 1026
    :cond_0
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v2

    cmp-long v4, v2, p1

    if-lez v4, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    const/4 p1, -0x1

    return p1
.end method

.method private a(Ljava/lang/String;)I
    .locals 3

    .line 391
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 393
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/f/q;

    .line 394
    iget-object v2, v2, Lcom/ss/android/socialbase/downloader/f/q;->a:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method private a(II)J
    .locals 5

    .line 1257
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ss/android/socialbase/downloader/f/i;

    .line 1258
    invoke-direct {p0, v0}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/f/i;)J

    move-result-wide v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    .line 1261
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ss/android/socialbase/downloader/f/i;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    return-wide v1

    .line 1266
    :cond_1
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide p1

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v3

    sub-long/2addr p1, v3

    const-wide/16 v3, -0x1

    cmp-long v0, v1, v3

    if-nez v0, :cond_2

    return-wide p1

    .line 1271
    :cond_2
    invoke-static {v1, v2, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method private a(Lcom/ss/android/socialbase/downloader/f/i;)J
    .locals 7

    .line 1275
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/i;->b()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 1276
    iget-wide v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    .line 1277
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v2

    sub-long/2addr v0, v2

    :cond_0
    return-wide v0
.end method

.method private a(JJJI)Lcom/ss/android/socialbase/downloader/f/m;
    .locals 16

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    .line 1448
    iget-object v3, v0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const-wide v5, 0x7fffffffffffffffL

    const/4 v7, 0x0

    move-wide v8, v5

    const/4 v5, 0x0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/ss/android/socialbase/downloader/f/m;

    .line 1449
    iget-wide v10, v6, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    const-wide/16 v12, 0x0

    cmp-long v14, v10, v12

    if-lez v14, :cond_2

    add-int/lit8 v7, v7, 0x1

    .line 1451
    iget-wide v10, v6, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    cmp-long v14, v10, v1

    if-gez v14, :cond_2

    move-wide/from16 v10, p3

    .line 1452
    invoke-virtual {v6, v1, v2, v10, v11}, Lcom/ss/android/socialbase/downloader/f/m;->a(JJ)J

    move-result-wide v14

    .line 1453
    iget-boolean v4, v0, Lcom/ss/android/socialbase/downloader/f/k;->A:Z

    if-eqz v4, :cond_1

    const-string v4, "SegmentDispatcher"

    .line 1454
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "findPoorReadThread: speed = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v13, ", threadIndex = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v13, v6, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v4, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v12, 0x0

    :cond_1
    cmp-long v4, v14, v12

    if-ltz v4, :cond_0

    cmp-long v4, v14, v8

    if-gez v4, :cond_0

    move-object v5, v6

    move-wide v8, v14

    goto :goto_0

    :cond_2
    move-wide/from16 v10, p3

    goto :goto_0

    :cond_3
    if-eqz v5, :cond_4

    move/from16 v1, p7

    if-lt v7, v1, :cond_4

    cmp-long v3, v8, p5

    if-gez v3, :cond_4

    const-string v1, "SegmentDispatcher"

    .line 1465
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "findPoorReadThread: ----------- minSpeed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, ", threadIndex = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v5, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5

    :cond_4
    const/4 v1, 0x0

    return-object v1
.end method

.method static synthetic a(Lcom/ss/android/socialbase/downloader/f/k;ZJJ)Lcom/ss/android/socialbase/downloader/f/m;
    .locals 0

    .line 42
    invoke-direct/range {p0 .. p5}, Lcom/ss/android/socialbase/downloader/f/k;->a(ZJJ)Lcom/ss/android/socialbase/downloader/f/m;

    move-result-object p0

    return-object p0
.end method

.method private a(ZJJ)Lcom/ss/android/socialbase/downloader/f/m;
    .locals 8

    .line 1287
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/f/m;

    .line 1288
    iget v3, v2, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    if-nez v3, :cond_1

    if-nez p1, :cond_1

    goto :goto_0

    .line 1294
    :cond_1
    iget-wide v3, v2, Lcom/ss/android/socialbase/downloader/f/m;->c:J

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-lez v7, :cond_0

    iget-wide v3, v2, Lcom/ss/android/socialbase/downloader/f/m;->d:J

    cmp-long v7, v3, v5

    if-gtz v7, :cond_0

    iget-wide v3, v2, Lcom/ss/android/socialbase/downloader/f/m;->c:J

    sub-long v3, p2, v3

    cmp-long v5, v3, p4

    if-lez v5, :cond_0

    if-nez v1, :cond_2

    :goto_1
    move-object v1, v2

    goto :goto_0

    .line 1300
    :cond_2
    iget-wide v3, v2, Lcom/ss/android/socialbase/downloader/f/m;->c:J

    iget-wide v5, v1, Lcom/ss/android/socialbase/downloader/f/m;->c:J

    cmp-long v7, v3, v5

    if-gez v7, :cond_0

    goto :goto_1

    :cond_3
    return-object v1
.end method

.method private a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V
    .locals 3

    const-string v0, "SegmentDispatcher"

    .line 709
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError, e = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 711
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->c:Lcom/ss/android/socialbase/downloader/f/b;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/b;->c()V

    .line 712
    monitor-enter p0

    .line 713
    :try_start_0
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ss/android/socialbase/downloader/f/m;

    .line 714
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/m;->e()V

    goto :goto_0

    .line 716
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private a(Lcom/ss/android/socialbase/downloader/f/q;)V
    .locals 7

    .line 520
    new-instance v6, Lcom/ss/android/socialbase/downloader/f/m;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/f/k;->c:Lcom/ss/android/socialbase/downloader/f/b;

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move-object v0, v6

    move-object v2, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/ss/android/socialbase/downloader/f/m;-><init>(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/f/k;Lcom/ss/android/socialbase/downloader/f/c;Lcom/ss/android/socialbase/downloader/f/q;I)V

    .line 521
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {p1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 522
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->q()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    .line 523
    invoke-interface {p1, v6}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    invoke-virtual {v6, p1}, Lcom/ss/android/socialbase/downloader/f/m;->a(Ljava/util/concurrent/Future;)V

    return-void
.end method

.method private a(Lcom/ss/android/socialbase/downloader/model/d;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 968
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->j:Lcom/ss/android/socialbase/downloader/model/d;

    if-nez v0, :cond_0

    .line 971
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->k:Lcom/ss/android/socialbase/downloader/model/d;

    if-nez v0, :cond_0

    return-void

    .line 977
    :cond_0
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/d;->j()J

    move-result-wide v1

    .line 978
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/d;->j()J

    move-result-wide v3

    cmp-long v5, v1, v3

    const/16 v6, 0x432

    if-nez v5, :cond_2

    .line 988
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/d;->c()Ljava/lang/String;

    move-result-object p1

    .line 989
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/d;->c()Ljava/lang/String;

    move-result-object v0

    .line 991
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 992
    :cond_1
    new-instance p1, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const-string v0, "etag not equals with main url"

    invoke-direct {p1, v6, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 982
    :cond_2
    new-instance v5, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "total len not equals,len="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ",sLen="

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ",code="

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/ss/android/socialbase/downloader/model/d;->c:I

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",sCode="

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, v0, Lcom/ss/android/socialbase/downloader/model/d;->c:I

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",range="

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 985
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",sRange = "

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",url = "

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/ss/android/socialbase/downloader/model/d;->a:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ",sUrl="

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, v0, Lcom/ss/android/socialbase/downloader/model/d;->a:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v5, v6, p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v5
.end method

.method private a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/f/i;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/i;",
            ">;",
            "Lcom/ss/android/socialbase/downloader/f/i;",
            "Z)V"
        }
    .end annotation

    .line 465
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v0

    .line 467
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 469
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ss/android/socialbase/downloader/f/i;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v4

    cmp-long v6, v0, v4

    if-ltz v6, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 475
    :cond_0
    invoke-interface {p1, v3, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    if-eqz p3, :cond_1

    .line 477
    invoke-virtual {p2, v2}, Lcom/ss/android/socialbase/downloader/f/i;->a(I)V

    :cond_1
    return-void
.end method

.method private a(JJ)Z
    .locals 9

    .line 1413
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->t:Lcom/ss/android/socialbase/downloader/i/d;

    sub-long v2, p1, p3

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/ss/android/socialbase/downloader/i/d;->b(JJ)J

    move-result-wide v0

    .line 1414
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    int-to-long v5, v4

    .line 1416
    div-long/2addr v0, v5

    :cond_0
    const/high16 v5, 0x41200000    # 10.0f

    long-to-float v0, v0

    .line 1419
    iget v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->y:F

    mul-float v0, v0, v1

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-long v6, v0

    .line 1423
    div-int/lit8 v8, v4, 0x2

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v8}, Lcom/ss/android/socialbase/downloader/f/k;->a(JJJI)Lcom/ss/android/socialbase/downloader/f/m;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 1425
    invoke-direct {p0, v0}, Lcom/ss/android/socialbase/downloader/f/k;->c(Lcom/ss/android/socialbase/downloader/f/m;)Z

    const-string p1, "SegmentDispatcher"

    .line 1426
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "handlePoorReadThread: reconnect for poor speed, threadIndex = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, v0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/m;->f()V

    return v1

    :cond_1
    const/4 v3, 0x1

    move-object v2, p0

    move-wide v4, p1

    move-wide v6, p3

    .line 1432
    invoke-direct/range {v2 .. v7}, Lcom/ss/android/socialbase/downloader/f/k;->a(ZJJ)Lcom/ss/android/socialbase/downloader/f/m;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 1434
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->c(Lcom/ss/android/socialbase/downloader/f/m;)Z

    const-string p2, "SegmentDispatcher"

    .line 1435
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "handlePoorReadThread: reconnect for connect timeout, threadIndex = "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p4, p1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/m;->f()V

    return v1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic a(Lcom/ss/android/socialbase/downloader/f/k;)Z
    .locals 0

    .line 42
    iget-boolean p0, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    return p0
.end method

.method static synthetic a(Lcom/ss/android/socialbase/downloader/f/k;Lcom/ss/android/socialbase/downloader/f/m;)Z
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->c(Lcom/ss/android/socialbase/downloader/f/m;)Z

    move-result p0

    return p0
.end method

.method private a(Lcom/ss/android/socialbase/downloader/f/m;JJJD)Z
    .locals 6

    .line 1392
    iget-wide v0, p1, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_2

    .line 1393
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->t:Lcom/ss/android/socialbase/downloader/i/d;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/ss/android/socialbase/downloader/i/d;->b(JJ)J

    move-result-wide v0

    .line 1394
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    int-to-long v2, v2

    .line 1396
    div-long v2, v0, v2

    goto :goto_0

    :cond_0
    move-wide v2, v0

    .line 1398
    :goto_0
    invoke-virtual {p1, p2, p3, p4, p5}, Lcom/ss/android/socialbase/downloader/f/m;->a(JJ)J

    move-result-wide p2

    cmp-long p4, p2, p6

    if-ltz p4, :cond_1

    long-to-double p4, p2

    long-to-double v4, v2

    .line 1399
    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v4, v4, p8

    cmpg-double p8, p4, v4

    if-gez p8, :cond_2

    :cond_1
    const-string p4, "SegmentDispatcher"

    .line 1400
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string p8, "isDownloadSpeedPoor: totalSpeed = "

    invoke-virtual {p5, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p8, ", threadAvgSpeed = "

    invoke-virtual {p5, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p8, ", poorSpeed = "

    invoke-virtual {p5, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p6, ", speed = "

    invoke-virtual {p5, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, ",threadIndex = "

    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p4, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method private b(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;)Lcom/ss/android/socialbase/downloader/f/i;
    .locals 6

    .line 556
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    if-nez v0, :cond_3

    .line 557
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ss/android/socialbase/downloader/f/i;

    if-nez v0, :cond_1

    goto :goto_0

    .line 561
    :cond_1
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-direct {p0, v4, v0, v1}, Lcom/ss/android/socialbase/downloader/f/k;->a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/f/i;Z)V

    .line 562
    invoke-direct {p0, v0}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/f/i;)J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-gtz v1, :cond_2

    iget-wide v4, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    cmp-long v1, v4, v2

    if-gtz v1, :cond_0

    :cond_2
    return-object v0

    .line 568
    :cond_3
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->m()V

    .line 570
    invoke-direct {p0, p1, p2}, Lcom/ss/android/socialbase/downloader/f/k;->c(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;)Lcom/ss/android/socialbase/downloader/f/i;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 571
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/f/i;)J

    move-result-wide v4

    cmp-long p2, v4, v2

    if-lez p2, :cond_4

    .line 572
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-direct {p0, p2, p1, v1}, Lcom/ss/android/socialbase/downloader/f/k;->a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/f/i;Z)V

    return-object p1

    .line 576
    :cond_4
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->p()Lcom/ss/android/socialbase/downloader/f/i;

    move-result-object p1

    if-eqz p1, :cond_5

    return-object p1

    :cond_5
    const/4 p1, 0x0

    return-object p1
.end method

.method private b(J)V
    .locals 3

    .line 1384
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->t:Lcom/ss/android/socialbase/downloader/i/d;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/ss/android/socialbase/downloader/i/d;->a(JJ)Z

    .line 1385
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/m;

    .line 1387
    invoke-virtual {v1, p1, p2}, Lcom/ss/android/socialbase/downloader/f/m;->b(J)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/f/q;Lcom/ss/android/socialbase/downloader/model/d;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Lcom/ss/android/socialbase/downloader/exception/h;
        }
    .end annotation

    .line 786
    iget-boolean p1, p3, Lcom/ss/android/socialbase/downloader/f/q;->d:Z

    const-wide/16 v0, 0x0

    if-eqz p1, :cond_3

    .line 788
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->j:Lcom/ss/android/socialbase/downloader/model/d;

    if-nez p1, :cond_5

    .line 789
    iput-object p4, p0, Lcom/ss/android/socialbase/downloader/f/k;->j:Lcom/ss/android/socialbase/downloader/model/d;

    .line 790
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->s:Ljava/lang/Object;

    monitor-enter p1

    .line 791
    :try_start_0
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->s:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 792
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->d:Lcom/ss/android/socialbase/downloader/h/f;

    if-eqz p1, :cond_0

    .line 795
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->d:Lcom/ss/android/socialbase/downloader/h/f;

    iget-object p3, p3, Lcom/ss/android/socialbase/downloader/f/q;->a:Ljava/lang/String;

    iget-object v2, p4, Lcom/ss/android/socialbase/downloader/model/d;->b:Lcom/ss/android/socialbase/downloader/network/g;

    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/f/i;->d()J

    move-result-wide v3

    invoke-interface {p1, p3, v2, v3, v4}, Lcom/ss/android/socialbase/downloader/h/f;->a(Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/g;J)V

    .line 797
    :cond_0
    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/model/d;->j()J

    move-result-wide p1

    cmp-long p3, p1, v0

    if-lez p3, :cond_5

    .line 799
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_1
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/ss/android/socialbase/downloader/f/i;

    .line 800
    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v2

    cmp-long v4, v2, v0

    const-wide/16 v2, 0x1

    if-lez v4, :cond_2

    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v4

    sub-long v6, p1, v2

    cmp-long v8, v4, v6

    if-lez v8, :cond_1

    :cond_2
    const/4 v4, 0x0

    sub-long v2, p1, v2

    .line 801
    invoke-virtual {p4, v2, v3}, Lcom/ss/android/socialbase/downloader/f/i;->c(J)V

    goto :goto_0

    :catchall_0
    move-exception p2

    .line 792
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p2

    .line 807
    :cond_3
    invoke-direct {p0, p4}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/model/d;)V

    .line 808
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->k:Lcom/ss/android/socialbase/downloader/model/d;

    if-nez p1, :cond_5

    .line 809
    iput-object p4, p0, Lcom/ss/android/socialbase/downloader/f/k;->k:Lcom/ss/android/socialbase/downloader/model/d;

    .line 810
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide p1

    cmp-long p3, p1, v0

    if-gtz p3, :cond_4

    .line 811
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/model/d;->j()J

    move-result-wide p2

    invoke-virtual {p1, p2, p3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setTotalBytes(J)V

    .line 813
    :cond_4
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->s:Ljava/lang/Object;

    monitor-enter p1

    .line 814
    :try_start_2
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->s:Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Object;->notify()V

    .line 815
    monitor-exit p1

    goto :goto_1

    :catchall_1
    move-exception p2

    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p2

    :cond_5
    :goto_1
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/q;",
            ">;)V"
        }
    .end annotation

    .line 373
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->A:Z

    if-eqz v0, :cond_0

    .line 374
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/q;

    const-string v2, "SegmentDispatcher"

    .line 375
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addIpListLocked: urlRecord = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/n;->k()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 380
    :cond_1
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->a(Ljava/lang/String;)I

    move-result p1

    if-ltz p1, :cond_2

    .line 381
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 382
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    add-int/2addr p1, v1

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    return-void

    .line 386
    :cond_2
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/i;",
            ">;)V"
        }
    .end annotation

    .line 169
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    .line 171
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 172
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getExpectFileLength()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    .line 175
    :cond_0
    monitor-enter p0

    .line 176
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 177
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 183
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/i;

    .line 190
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    new-instance v3, Lcom/ss/android/socialbase/downloader/f/i;

    invoke-direct {v3, v1}, Lcom/ss/android/socialbase/downloader/f/i;-><init>(Lcom/ss/android/socialbase/downloader/f/i;)V

    invoke-direct {p0, v2, v3, v0}, Lcom/ss/android/socialbase/downloader/f/k;->a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/f/i;Z)V

    goto :goto_0

    .line 194
    :cond_2
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->d(Ljava/util/List;)V

    .line 196
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->c(Ljava/util/List;)V

    goto :goto_2

    .line 179
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    new-instance v1, Lcom/ss/android/socialbase/downloader/f/i;

    const-wide/16 v4, -0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/ss/android/socialbase/downloader/f/i;-><init>(JJ)V

    invoke-direct {p0, p1, v1, v0}, Lcom/ss/android/socialbase/downloader/f/k;->a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/f/i;Z)V

    :goto_2
    const-string p1, "SegmentDispatcher"

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initSegments: totalLength = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic b(Lcom/ss/android/socialbase/downloader/f/k;)Z
    .locals 0

    .line 42
    iget-boolean p0, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    return p0
.end method

.method private c(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;)Lcom/ss/android/socialbase/downloader/f/i;
    .locals 16

    move-object/from16 v0, p0

    .line 1095
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const-wide/16 v2, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v1, :cond_1

    .line 1097
    invoke-direct {v0, v5, v1}, Lcom/ss/android/socialbase/downloader/f/k;->a(II)J

    move-result-wide v6

    cmp-long v8, v6, v2

    if-lez v8, :cond_0

    move v4, v5

    move-wide v2, v6

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1103
    :cond_1
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/n;->g()J

    move-result-wide v5

    .line 1104
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/n;->h()J

    move-result-wide v7

    if-ltz v4, :cond_7

    cmp-long v1, v2, v5

    if-lez v1, :cond_7

    .line 1106
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/i;

    .line 1108
    iget-object v4, v0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/f/n;->m()Z

    move-result v4

    const-wide/16 v9, 0x2

    if-eqz v4, :cond_6

    .line 1109
    iget-wide v11, v0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    iget-object v4, v0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v13

    sub-long/2addr v11, v13

    .line 1110
    invoke-direct/range {p0 .. p2}, Lcom/ss/android/socialbase/downloader/f/k;->d(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;)F

    move-result v4

    long-to-float v11, v11

    mul-float v11, v11, v4

    float-to-long v11, v11

    cmp-long v13, v11, v5

    if-gez v13, :cond_2

    move-wide v11, v5

    :cond_2
    const-wide/16 v13, 0x0

    cmp-long v15, v7, v13

    if-lez v15, :cond_3

    cmp-long v13, v11, v7

    if-lez v13, :cond_3

    goto :goto_1

    :cond_3
    move-wide v7, v11

    .line 1119
    :goto_1
    div-long/2addr v5, v9

    const/4 v9, 0x0

    sub-long v9, v2, v5

    cmp-long v11, v7, v9

    if-lez v11, :cond_4

    move-wide v5, v9

    goto :goto_2

    :cond_4
    cmp-long v9, v7, v5

    if-gez v9, :cond_5

    goto :goto_2

    :cond_5
    move-wide v5, v7

    .line 1126
    :goto_2
    new-instance v7, Lcom/ss/android/socialbase/downloader/f/i;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v8

    sub-long v10, v2, v5

    add-long/2addr v8, v10

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v10

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/ss/android/socialbase/downloader/f/i;-><init>(JJ)V

    const-string v8, "SegmentDispatcher"

    .line 1127
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "obtainSegment: parent = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", child = "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxRemainBytes = "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", childLength = "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", ratio = "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", threadIndex = "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v1, p1

    iget v1, v1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1131
    :cond_6
    new-instance v7, Lcom/ss/android/socialbase/downloader/f/i;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v4

    div-long/2addr v2, v9

    add-long/2addr v4, v2

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v2

    invoke-direct {v7, v4, v5, v2, v3}, Lcom/ss/android/socialbase/downloader/f/i;-><init>(JJ)V

    const-string v2, "SegmentDispatcher"

    .line 1132
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "obtainSegment: parent = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",child = "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    return-object v7

    :cond_7
    const/4 v1, 0x0

    return-object v1
.end method

.method static synthetic c(Lcom/ss/android/socialbase/downloader/f/k;)Lcom/ss/android/socialbase/downloader/model/d;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/ss/android/socialbase/downloader/f/k;->j:Lcom/ss/android/socialbase/downloader/model/d;

    return-object p0
.end method

.method private c(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/net/InetAddress;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/q;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_a

    .line 402
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_2

    .line 406
    :cond_0
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 408
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v2, 0x0

    const/4 v3, 0x0

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/InetAddress;

    if-eqz v4, :cond_1

    .line 410
    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v4

    .line 411
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 412
    iget-boolean v5, p0, Lcom/ss/android/socialbase/downloader/f/k;->A:Z

    if-eqz v5, :cond_2

    const-string v5, "SegmentDispatcher"

    .line 413
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onDnsResolved: ip = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    :cond_2
    new-instance v5, Lcom/ss/android/socialbase/downloader/f/q;

    invoke-direct {v5, p1, v4}, Lcom/ss/android/socialbase/downloader/f/q;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    iget-object v4, v5, Lcom/ss/android/socialbase/downloader/f/q;->c:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/LinkedList;

    if-nez v4, :cond_3

    .line 418
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 419
    iget-object v6, v5, Lcom/ss/android/socialbase/downloader/f/q;->c:Ljava/lang/String;

    invoke-interface {v1, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    :cond_3
    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    if-lez v3, :cond_9

    .line 428
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 432
    :cond_5
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    :cond_6
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 433
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/LinkedList;

    if-eqz v4, :cond_6

    .line 434
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    .line 436
    invoke-virtual {v4}, Ljava/util/LinkedList;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ss/android/socialbase/downloader/f/q;

    .line 437
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, -0x1

    const/4 v0, 0x1

    goto :goto_1

    :cond_7
    if-lez v3, :cond_8

    if-nez v0, :cond_5

    :cond_8
    return-object p1

    :cond_9
    return-object v0

    :cond_a
    :goto_2
    return-object v0
.end method

.method private c()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 203
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->s:Ljava/lang/Object;

    monitor-enter v0

    .line 204
    :try_start_0
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->j:Lcom/ss/android/socialbase/downloader/model/d;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->k:Lcom/ss/android/socialbase/downloader/model/d;

    if-nez v1, :cond_0

    .line 205
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->s:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 207
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->j:Lcom/ss/android/socialbase/downloader/model/d;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->k:Lcom/ss/android/socialbase/downloader/model/d;

    if-nez v0, :cond_2

    .line 211
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    if-nez v0, :cond_1

    goto :goto_0

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    throw v0

    :cond_2
    :goto_0
    return-void

    :catchall_0
    move-exception v1

    .line 207
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private c(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/i;",
            ">;)V"
        }
    .end annotation

    .line 252
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/f/o;->b(Ljava/util/List;)J

    move-result-wide v0

    const-string p1, "SegmentDispatcher"

    .line 253
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkDownloadBytes: getCurBytes = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, ", totalBytes = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 254
    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, ", downloadedBytes = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 253
    invoke-static {p1, v2}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v2

    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v4

    cmp-long p1, v2, v4

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v2

    cmp-long p1, v2, v0

    if-eqz p1, :cond_0

    .line 257
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1, v0, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setCurBytes(J)V

    :cond_0
    return-void
.end method

.method private c(Lcom/ss/android/socialbase/downloader/f/m;)Z
    .locals 1

    .line 741
    monitor-enter p0

    .line 742
    :try_start_0
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->d(Lcom/ss/android/socialbase/downloader/f/m;)Lcom/ss/android/socialbase/downloader/f/q;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 745
    monitor-exit p0

    return p1

    .line 748
    :cond_0
    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/f/m;->a(Lcom/ss/android/socialbase/downloader/f/q;)Z

    move-result p1

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    .line 749
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private d(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;)F
    .locals 6

    .line 1141
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/m;->b()J

    move-result-wide v0

    .line 1142
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 v2, 0x1

    if-gt p2, v2, :cond_0

    .line 1144
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/f/n;->a()I

    move-result p2

    :cond_0
    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    const/high16 v3, 0x3f800000    # 1.0f

    if-gtz v5, :cond_5

    .line 1147
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/n;->l()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-lez v1, :cond_1

    cmpl-float v1, v0, v3

    if-ltz v1, :cond_2

    :cond_1
    int-to-float v0, p2

    div-float v0, v3, v0

    .line 1151
    :cond_2
    iget p1, p1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    if-nez p1, :cond_3

    return v0

    :cond_3
    if-le p2, v2, :cond_4

    sub-float/2addr v3, v0

    sub-int/2addr p2, v2

    int-to-float p1, p2

    div-float/2addr v3, p1

    return v3

    :cond_4
    int-to-float p1, p2

    div-float/2addr v3, p1

    return v3

    .line 1162
    :cond_5
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->o()J

    move-result-wide v4

    cmp-long p1, v4, v0

    if-lez p1, :cond_6

    long-to-float p1, v0

    long-to-float p2, v4

    div-float/2addr p1, p2

    return p1

    :cond_6
    int-to-float p1, p2

    div-float/2addr v3, p1

    return v3
.end method

.method private d(Lcom/ss/android/socialbase/downloader/f/m;)Lcom/ss/android/socialbase/downloader/f/q;
    .locals 5

    .line 757
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move-object v2, v1

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ss/android/socialbase/downloader/f/q;

    .line 758
    iget-object v4, p1, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    if-eq v3, v4, :cond_0

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/f/q;->d()Z

    move-result v4

    if-nez v4, :cond_0

    if-nez v2, :cond_1

    move-object v2, v3

    .line 762
    :cond_1
    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/f/q;->a()I

    move-result v4

    if-gtz v4, :cond_0

    goto :goto_0

    :cond_2
    move-object v3, v1

    .line 768
    :goto_0
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/n;->b()Z

    move-result p1

    if-eqz p1, :cond_5

    if-eqz v3, :cond_3

    return-object v3

    .line 773
    :cond_3
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/n;->c()Z

    move-result p1

    if-eqz p1, :cond_4

    return-object v1

    :cond_4
    return-object v2

    :cond_5
    return-object v2
.end method

.method static synthetic d(Lcom/ss/android/socialbase/downloader/f/k;)Lcom/ss/android/socialbase/downloader/model/d;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/ss/android/socialbase/downloader/f/k;->k:Lcom/ss/android/socialbase/downloader/model/d;

    return-object p0
.end method

.method private d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 219
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->e:Lcom/ss/android/socialbase/downloader/f/g;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->c:Lcom/ss/android/socialbase/downloader/f/b;

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/f/g;->a(Lcom/ss/android/socialbase/downloader/f/d;)V
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/f/p; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dispatchSegments: loopAndWrite e = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SegmentDispatcher"

    invoke-static {v2, v1}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    invoke-direct {p0, v0}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    .line 228
    throw v0

    .line 231
    :catch_1
    :goto_0
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-eqz v0, :cond_0

    goto :goto_2

    .line 236
    :cond_0
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->r:Z

    if-nez v0, :cond_2

    .line 237
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    if-nez v0, :cond_1

    goto :goto_1

    .line 238
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dispatchSegments: loopAndWrite  failedException = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SegmentDispatcher"

    invoke-static {v1, v0}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    throw v0

    :cond_2
    :goto_1
    const-string v0, "SegmentDispatcher"

    const-string v1, "dispatchSegments::download finished"

    .line 246
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_3
    :goto_2
    return-void
.end method

.method private d(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/i;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 483
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/i;

    .line 485
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    const-wide/16 v7, 0x1

    if-lez v6, :cond_0

    .line 488
    new-instance v6, Lcom/ss/android/socialbase/downloader/f/i;

    sub-long/2addr v2, v7

    invoke-direct {v6, v4, v5, v2, v3}, Lcom/ss/android/socialbase/downloader/f/i;-><init>(JJ)V

    const-string v2, "SegmentDispatcher"

    .line 489
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "fixSegmentsLocked: first = "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", add new first = "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    invoke-interface {p1, v0, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 492
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 493
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 495
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/i;

    .line 496
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 497
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/f/i;

    .line 500
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v9

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v11

    sub-long/2addr v11, v7

    cmp-long v3, v9, v11

    if-gez v3, :cond_1

    const-string v3, "SegmentDispatcher"

    .line 501
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "fixSegment: segment = "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v9, ", new end = "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v9

    sub-long/2addr v9, v7

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v9

    sub-long/2addr v9, v7

    invoke-virtual {v1, v9, v10}, Lcom/ss/android/socialbase/downloader/f/i;->c(J)V

    :cond_1
    move-object v1, v2

    goto :goto_0

    .line 509
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 510
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ss/android/socialbase/downloader/f/i;

    .line 511
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v0

    cmp-long v2, v0, v4

    const-wide/16 v3, -0x1

    if-lez v2, :cond_3

    .line 513
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v5

    cmp-long v2, v5, v3

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v5

    sub-long/2addr v0, v7

    cmp-long v2, v5, v0

    if-gez v2, :cond_4

    :cond_3
    const-string v0, "SegmentDispatcher"

    .line 514
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fixSegment: last segment = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", new end=-1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    invoke-virtual {p1, v3, v4}, Lcom/ss/android/socialbase/downloader/f/i;->c(J)V

    :cond_4
    return-void
.end method

.method static synthetic e(Lcom/ss/android/socialbase/downloader/f/k;)J
    .locals 2

    .line 42
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->v:J

    return-wide v0
.end method

.method private e()V
    .locals 6

    .line 263
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    const/4 v0, 0x1

    if-lez v4, :cond_1

    iget-boolean v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->l:Z

    if-eqz v1, :cond_0

    goto :goto_0

    .line 267
    :cond_0
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/n;->a()I

    move-result v1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    const-string v2, "SegmentDispatcher"

    .line 269
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dispatchReadThread: totalLength = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, ", threadCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-gtz v1, :cond_2

    goto :goto_2

    :cond_2
    move v0, v1

    .line 273
    :goto_2
    monitor-enter p0

    .line 274
    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v0, :cond_6

    .line 275
    iget-boolean v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-eqz v1, :cond_4

    goto :goto_3

    .line 280
    :cond_4
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->j()Lcom/ss/android/socialbase/downloader/f/q;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/f/q;)V

    .line 282
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/n;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_4

    .line 276
    :cond_5
    :goto_3
    monitor-exit p0

    return-void

    .line 289
    :cond_6
    :goto_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/f/j;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "SegmentDispatcher"

    .line 827
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "applySegment: start "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    iget-object v3, v2, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    if-ne v3, v1, :cond_0

    const-string v3, "SegmentDispatcher"

    .line 831
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "applySegment: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " is already the owner of "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 835
    :cond_0
    iget-object v3, v2, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    const/4 v4, 0x1

    if-nez v3, :cond_15

    .line 841
    invoke-virtual/range {p1 .. p1}, Lcom/ss/android/socialbase/downloader/f/m;->h()J

    move-result-wide v5

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/f/i;->d()J

    move-result-wide v7

    cmp-long v3, v5, v7

    if-nez v3, :cond_14

    .line 848
    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v5

    .line 849
    invoke-direct {v0, v5, v6}, Lcom/ss/android/socialbase/downloader/f/k;->a(J)I

    move-result v3

    const/4 v7, 0x2

    const/4 v8, -0x1

    if-eq v3, v8, :cond_13

    .line 851
    iget-object v8, v0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ss/android/socialbase/downloader/f/i;

    if-eqz v8, :cond_13

    const-wide/16 v9, 0x0

    if-eq v8, v2, :cond_6

    const/4 v11, 0x0

    .line 859
    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v12

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v14

    cmp-long v16, v12, v14

    if-nez v16, :cond_4

    .line 860
    invoke-direct {v0, v8}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/f/i;)J

    move-result-wide v12

    cmp-long v14, v12, v9

    if-lez v14, :cond_4

    .line 861
    iget-object v12, v8, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    if-eqz v12, :cond_2

    .line 863
    iget-wide v13, v1, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    iget-wide v9, v12, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    sub-long/2addr v13, v9

    const-wide/16 v9, 0x3e8

    cmp-long v15, v13, v9

    if-lez v15, :cond_1

    .line 867
    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v9

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v13

    sub-long/2addr v9, v13

    invoke-direct {v0, v2}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/f/i;)J

    move-result-wide v13

    const-wide/16 v15, 0x2

    div-long/2addr v13, v15

    cmp-long v15, v9, v13

    if-gez v15, :cond_1

    goto :goto_0

    :cond_1
    const-string v9, "SegmentDispatcher"

    .line 881
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "applySegmentLocked: has same segment, but owner is normal, abort. segmentInList = "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    :goto_0
    if-eqz v12, :cond_3

    const-string v9, "SegmentDispatcher"

    .line 870
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "applySegmentLocked: has same segment,and owner too slow, segmentInList = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    invoke-virtual {v12, v4}, Lcom/ss/android/socialbase/downloader/f/m;->a(Z)V

    goto :goto_1

    :cond_3
    const-string v9, "SegmentDispatcher"

    .line 873
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "applySegmentLocked: has same segment and no owner, segmentInList = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    :goto_1
    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v9

    invoke-virtual {v2, v9, v10}, Lcom/ss/android/socialbase/downloader/f/i;->c(J)V

    .line 878
    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/f/i;->j()I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/ss/android/socialbase/downloader/f/i;->b(I)V

    .line 879
    iget-object v9, v0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v9, v3, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 v11, 0x1

    :cond_4
    :goto_2
    if-eqz v11, :cond_5

    goto :goto_3

    .line 887
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "applySegment: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " not exist! but has another same segment, segmentInList = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SegmentDispatcher"

    invoke-static {v2, v1}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    new-instance v1, Lcom/ss/android/socialbase/downloader/f/j;

    const-string v2, "segment not exist, but has another same segment"

    invoke-direct {v1, v7, v2}, Lcom/ss/android/socialbase/downloader/f/j;-><init>(ILjava/lang/String;)V

    throw v1

    .line 893
    :cond_6
    :goto_3
    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/f/i;->a()J

    move-result-wide v7

    add-int/lit8 v9, v3, -0x1

    :goto_4
    const-wide/16 v10, 0x1

    if-ltz v9, :cond_d

    .line 898
    iget-object v12, v0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v12, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/ss/android/socialbase/downloader/f/i;

    .line 899
    invoke-virtual {v12}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v13

    const-wide/16 v15, 0x0

    cmp-long v17, v13, v15

    if-lez v17, :cond_7

    cmp-long v17, v13, v5

    if-ltz v17, :cond_a

    :cond_7
    cmp-long v13, v7, v15

    if-gtz v13, :cond_9

    .line 905
    invoke-virtual {v12}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v13

    cmp-long v15, v13, v5

    if-gtz v15, :cond_8

    goto :goto_5

    .line 907
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "applySegment:prev\'s current has over this start, prev = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ", segment = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "SegmentDispatcher"

    invoke-static {v3, v1}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 910
    new-instance v1, Lcom/ss/android/socialbase/downloader/f/j;

    const/4 v2, 0x3

    const-string v3, "prev overstep"

    invoke-direct {v1, v2, v3}, Lcom/ss/android/socialbase/downloader/f/j;-><init>(ILjava/lang/String;)V

    throw v1

    .line 914
    :cond_9
    :goto_5
    iget-object v13, v12, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    if-nez v13, :cond_b

    sub-long v13, v5, v10

    .line 917
    invoke-virtual {v12, v13, v14}, Lcom/ss/android/socialbase/downloader/f/i;->c(J)V

    const-string v13, "SegmentDispatcher"

    .line 918
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "applySegment: prev set end, prev = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    invoke-virtual {v12}, Lcom/ss/android/socialbase/downloader/f/i;->a()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v16, v12, v14

    if-lez v16, :cond_a

    const-string v5, "SegmentDispatcher"

    const-string v6, "applySegmentLocked:q break"

    .line 921
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :cond_a
    add-int/lit8 v9, v9, -0x1

    goto/16 :goto_4

    .line 926
    :cond_b
    iget-object v7, v12, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    sub-long/2addr v5, v10

    invoke-virtual {v7, v5, v6}, Lcom/ss/android/socialbase/downloader/f/m;->a(J)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 927
    invoke-virtual {v12, v5, v6}, Lcom/ss/android/socialbase/downloader/f/i;->c(J)V

    const-string v5, "SegmentDispatcher"

    .line 928
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "applySegment: adjustSegmentEndOffset succeed, prev = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 933
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applySegment: adjustSegmentEndOffset filed, prev = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SegmentDispatcher"

    invoke-static {v2, v1}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    new-instance v1, Lcom/ss/android/socialbase/downloader/f/j;

    const/4 v2, 0x4

    const-string v3, "prev end adjust fail"

    invoke-direct {v1, v2, v3}, Lcom/ss/android/socialbase/downloader/f/j;-><init>(ILjava/lang/String;)V

    throw v1

    :cond_d
    :goto_6
    add-int/2addr v3, v4

    .line 943
    iget-object v4, v0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    :goto_7
    if-ge v3, v4, :cond_12

    .line 945
    iget-object v5, v0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ss/android/socialbase/downloader/f/i;

    .line 946
    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/f/i;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v12, v6, v8

    if-gtz v12, :cond_f

    iget-object v6, v5, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    if-eqz v6, :cond_e

    goto :goto_8

    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 948
    :cond_f
    :goto_8
    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v3

    .line 949
    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v6

    cmp-long v12, v3, v8

    if-lez v12, :cond_10

    cmp-long v8, v3, v6

    if-ltz v8, :cond_11

    :cond_10
    const/4 v3, 0x0

    sub-long/2addr v6, v10

    .line 952
    invoke-virtual {v2, v6, v7}, Lcom/ss/android/socialbase/downloader/f/i;->c(J)V

    const-string v3, "SegmentDispatcher"

    .line 953
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "applySegment: segment set end:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, ", later = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    const-string v3, "SegmentDispatcher"

    const-string v4, "applySegmentLocked: break 2"

    .line 956
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    :cond_12
    iput-object v1, v2, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    const-string v1, "SegmentDispatcher"

    .line 964
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "applySegment: OK "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 853
    :cond_13
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "applySegment: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " not exist! segmentIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SegmentDispatcher"

    invoke-static {v2, v1}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    new-instance v1, Lcom/ss/android/socialbase/downloader/f/j;

    const-string v2, "segment not exist"

    invoke-direct {v1, v7, v2}, Lcom/ss/android/socialbase/downloader/f/j;-><init>(ILjava/lang/String;)V

    throw v1

    .line 845
    :cond_14
    new-instance v1, Lcom/ss/android/socialbase/downloader/f/j;

    const/4 v2, 0x5

    const-string v3, "applySegment"

    invoke-direct {v1, v2, v3}, Lcom/ss/android/socialbase/downloader/f/j;-><init>(ILjava/lang/String;)V

    throw v1

    .line 837
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "applySegment: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " is already has an owner:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v2, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SegmentDispatcher"

    invoke-static {v2, v1}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    new-instance v1, Lcom/ss/android/socialbase/downloader/f/j;

    const-string v2, "segment already has an owner"

    invoke-direct {v1, v4, v2}, Lcom/ss/android/socialbase/downloader/f/j;-><init>(ILjava/lang/String;)V

    throw v1
.end method

.method static synthetic f(Lcom/ss/android/socialbase/downloader/f/k;)Ljava/util/List;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    return-object p0
.end method

.method private f()V
    .locals 5

    .line 295
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    new-instance v1, Lcom/ss/android/socialbase/downloader/f/q;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/ss/android/socialbase/downloader/f/q;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getBackUpUrls()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 298
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 299
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 300
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    new-instance v3, Lcom/ss/android/socialbase/downloader/f/q;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lcom/ss/android/socialbase/downloader/f/q;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/f/n;->a(I)V

    return-void
.end method

.method static synthetic g(Lcom/ss/android/socialbase/downloader/f/k;)J
    .locals 2

    .line 42
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->r()J

    move-result-wide v0

    return-wide v0
.end method

.method private g()V
    .locals 5

    .line 310
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    .line 311
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/n;->i()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->v:J

    .line 312
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/n;->j()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->w:J

    .line 313
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/n;->n()F

    move-result v0

    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->y:F

    .line 315
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->z:I

    if-lez v0, :cond_0

    .line 317
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->u:Lcom/ss/android/socialbase/downloader/h/e;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->B:Lcom/ss/android/socialbase/downloader/h/e$b;

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/ss/android/socialbase/downloader/h/e;->a(Lcom/ss/android/socialbase/downloader/h/e$b;J)V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 5

    .line 322
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->w:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 324
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->x:J

    .line 325
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->u:Lcom/ss/android/socialbase/downloader/h/e;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->C:Lcom/ss/android/socialbase/downloader/h/e$b;

    invoke-virtual {v0, v1, v2, v3}, Lcom/ss/android/socialbase/downloader/h/e;->a(Lcom/ss/android/socialbase/downloader/h/e$b;J)V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 6

    .line 330
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/n;->k()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    .line 332
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->l:Z

    .line 333
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->e()V

    return-void

    .line 336
    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/network/c;->a()Lcom/ss/android/socialbase/downloader/network/c;

    move-result-object v1

    .line 337
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, p0, v3, v4}, Lcom/ss/android/socialbase/downloader/network/c;->a(Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/c$a;J)V

    const/4 v2, 0x2

    if-le v0, v2, :cond_2

    .line 339
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getBackUpUrls()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 341
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 342
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 343
    invoke-virtual {v1, v2, p0, v3, v4}, Lcom/ss/android/socialbase/downloader/network/c;->a(Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/c$a;J)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private j()Lcom/ss/android/socialbase/downloader/f/q;
    .locals 2

    .line 448
    monitor-enter p0

    .line 449
    :try_start_0
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->p:I

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    .line 450
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/n;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452
    iget v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->p:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->p:I

    .line 454
    :cond_0
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ss/android/socialbase/downloader/f/q;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 455
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private k()V
    .locals 2

    const-string v0, "SegmentDispatcher"

    const-string v1, "onComplete"

    .line 720
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->c:Lcom/ss/android/socialbase/downloader/f/b;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/b;->c()V

    .line 723
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->s:Ljava/lang/Object;

    monitor-enter v0

    .line 724
    :try_start_0
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->s:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 725
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private l()Z
    .locals 2

    .line 732
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/m;

    .line 733
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/m;->g()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private m()V
    .locals 13

    .line 1034
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    return-void

    .line 1038
    :cond_0
    monitor-enter p0

    .line 1039
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 1041
    monitor-exit p0

    return-void

    .line 1043
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    :goto_0
    if-ge v1, v0, :cond_4

    .line 1047
    iget-object v6, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/ss/android/socialbase/downloader/f/i;

    .line 1048
    iget-object v7, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ss/android/socialbase/downloader/f/i;

    .line 1049
    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v8

    invoke-virtual {v7}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v10

    cmp-long v12, v8, v10

    if-lez v12, :cond_2

    .line 1051
    invoke-virtual {v7}, Lcom/ss/android/socialbase/downloader/f/i;->a()J

    move-result-wide v8

    cmp-long v10, v8, v2

    if-gtz v10, :cond_2

    iget-object v8, v7, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    if-nez v8, :cond_2

    .line 1052
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v8, "SegmentDispatcher"

    .line 1053
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "clearCovered, covered = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v7, ", prev = "

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1058
    :cond_2
    invoke-virtual {v7}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v7

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v9

    cmp-long v6, v7, v9

    if-lez v6, :cond_3

    add-int/lit8 v5, v5, 0x1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1063
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1064
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1067
    :cond_5
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private n()Z
    .locals 8

    .line 1074
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    const/4 v2, 0x0

    if-gtz v4, :cond_0

    .line 1076
    iput-boolean v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->r:Z

    return v2

    .line 1079
    :cond_0
    monitor-enter p0

    .line 1080
    :try_start_0
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-static {v3}, Lcom/ss/android/socialbase/downloader/f/o;->a(Ljava/util/List;)J

    move-result-wide v3

    const-string v5, "SegmentDispatcher"

    .line 1081
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isAllContentDownloaded: firstOffset = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    cmp-long v5, v3, v0

    if-ltz v5, :cond_1

    const/4 v0, 0x1

    .line 1084
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->r:Z

    .line 1085
    monitor-exit p0

    return v0

    .line 1087
    :cond_1
    iput-boolean v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->r:Z

    .line 1088
    monitor-exit p0

    return v2

    :catchall_0
    move-exception v0

    .line 1089
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private o()J
    .locals 5

    .line 1171
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ss/android/socialbase/downloader/f/m;

    .line 1172
    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/f/m;->b()J

    move-result-wide v3

    add-long/2addr v1, v3

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method private p()Lcom/ss/android/socialbase/downloader/f/i;
    .locals 15

    const/4 v0, 0x0

    .line 1186
    :goto_0
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->q()Lcom/ss/android/socialbase/downloader/f/i;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 1190
    :cond_0
    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    if-nez v13, :cond_1

    return-object v1

    .line 1195
    :cond_1
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/i;->j()I

    move-result v3

    const/4 v14, 0x2

    if-lt v3, v14, :cond_2

    return-object v2

    .line 1200
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 1201
    invoke-direct {p0, v7, v8}, Lcom/ss/android/socialbase/downloader/f/k;->b(J)V

    .line 1204
    iget-wide v3, v13, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    sub-long v3, v7, v3

    const-wide/16 v5, 0x7d0

    cmp-long v9, v3, v5

    if-lez v9, :cond_4

    sub-long v5, v7, v5

    const-wide/16 v9, 0x1f4

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    move-object v3, p0

    move-object v4, v13

    .line 1208
    invoke-direct/range {v3 .. v12}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/f/m;JJJD)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1212
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->A:Z

    if-eqz v0, :cond_3

    const-string v0, "SegmentDispatcher"

    .line 1213
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "obtainSegmentWhenNoNewSegment: isDownloadSpeedPoor segment = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ", owner.threadIndex = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v13, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-object v1

    :cond_4
    add-int/lit8 v3, v0, 0x1

    if-le v0, v14, :cond_6

    .line 1222
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->A:Z

    if-eqz v0, :cond_5

    const-string v0, "SegmentDispatcher"

    .line 1223
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "obtainSegmentWhenNoNewSegment: waitCount > 2, return segment = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-object v1

    .line 1228
    :cond_6
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v0, 0x1f4

    .line 1229
    :try_start_1
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 1230
    monitor-exit p0

    move v0, v3

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    return-object v2
.end method

.method private q()Lcom/ss/android/socialbase/downloader/f/i;
    .locals 9

    .line 1241
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7fffffff

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ss/android/socialbase/downloader/f/i;

    .line 1242
    invoke-direct {p0, v3}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/f/i;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-lez v8, :cond_0

    .line 1243
    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/f/i;->j()I

    move-result v4

    if-ge v4, v2, :cond_0

    move-object v1, v3

    move v2, v4

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private r()J
    .locals 9

    .line 1359
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1362
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1363
    monitor-enter p0

    .line 1365
    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/ss/android/socialbase/downloader/f/k;->b(J)V

    .line 1367
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/f/n;->j()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-lez v6, :cond_1

    .line 1370
    iget-wide v6, p0, Lcom/ss/android/socialbase/downloader/f/k;->x:J

    cmp-long v8, v6, v4

    if-lez v8, :cond_1

    sub-long v4, v0, v6

    cmp-long v6, v4, v2

    if-lez v6, :cond_1

    .line 1372
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/ss/android/socialbase/downloader/f/k;->a(JJ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1373
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->x:J

    .line 1374
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->z:I

    :cond_1
    const-wide/16 v0, 0x7d0

    .line 1378
    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    .line 1379
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :goto_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method


# virtual methods
.method public a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;)Lcom/ss/android/socialbase/downloader/f/i;
    .locals 1

    .line 533
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 536
    :cond_0
    monitor-enter p0

    .line 537
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/ss/android/socialbase/downloader/f/k;->b(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;)Lcom/ss/android/socialbase/downloader/f/i;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 539
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/i;->h()V

    .line 540
    iget-object p2, p1, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    if-eqz p2, :cond_1

    .line 541
    new-instance p2, Lcom/ss/android/socialbase/downloader/f/i;

    invoke-direct {p2, p1}, Lcom/ss/android/socialbase/downloader/f/i;-><init>(Lcom/ss/android/socialbase/downloader/f/i;)V

    monitor-exit p0

    return-object p2

    .line 544
    :cond_1
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 545
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public a()V
    .locals 2

    const-string v0, "SegmentDispatcher"

    const-string v1, "cancel"

    .line 997
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 998
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    .line 999
    monitor-enter p0

    .line 1000
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/m;

    .line 1001
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/m;->e()V

    goto :goto_0

    .line 1003
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1004
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->e:Lcom/ss/android/socialbase/downloader/f/g;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/g;->a()V

    .line 1005
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->c:Lcom/ss/android/socialbase/downloader/f/b;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/b;->c()V

    return-void

    :catchall_0
    move-exception v0

    .line 1003
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/ss/android/socialbase/downloader/f/m;)V
    .locals 3

    .line 528
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->A:Z

    if-eqz v0, :cond_0

    const-string v0, "SegmentDispatcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReaderRun, threadIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V
    .locals 0

    .line 550
    monitor-enter p0

    .line 551
    :try_start_0
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/f/i;->i()V

    .line 552
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/f/q;Lcom/ss/android/socialbase/downloader/model/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Lcom/ss/android/socialbase/downloader/exception/h;
        }
    .end annotation

    .line 587
    monitor-enter p0

    .line 588
    :try_start_0
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-nez v0, :cond_3

    .line 592
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/ss/android/socialbase/downloader/f/k;->b(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/f/q;Lcom/ss/android/socialbase/downloader/model/d;)V

    const/4 p2, 0x0

    .line 594
    invoke-virtual {p1, p2}, Lcom/ss/android/socialbase/downloader/f/m;->b(Z)V

    .line 596
    iget-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    const-wide/16 v0, 0x0

    cmp-long p3, p1, v0

    if-gtz p3, :cond_1

    .line 598
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    .line 599
    iget-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    cmp-long p3, p1, v0

    if-gtz p3, :cond_0

    .line 600
    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/model/d;->j()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    .line 602
    :cond_0
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->e()V

    goto :goto_0

    .line 604
    :cond_1
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/n;->f()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 606
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->e()V

    .line 609
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    .line 589
    :cond_3
    new-instance p1, Lcom/ss/android/socialbase/downloader/f/p;

    const-string p2, "connected"

    invoke-direct {p1, p2}, Lcom/ss/android/socialbase/downloader/f/p;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 609
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/exception/BaseException;)V
    .locals 2

    .line 665
    monitor-enter p0

    :try_start_0
    const-string p2, "SegmentDispatcher"

    .line 666
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSegmentFailed: segment = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, ", e = "

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x1

    .line 667
    invoke-virtual {p1, p2}, Lcom/ss/android/socialbase/downloader/f/m;->b(Z)V

    .line 669
    iget p1, p1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    if-nez p1, :cond_0

    .line 671
    iput-object p4, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 674
    :cond_0
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->l()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 677
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    if-nez p1, :cond_1

    .line 678
    iput-object p4, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 681
    :cond_1
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->q:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    .line 683
    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/exception/BaseException;II)V
    .locals 0

    .line 646
    invoke-static {p4}, Lcom/ss/android/socialbase/downloader/i/e;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result p2

    .line 650
    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result p3

    const/16 p4, 0x417

    if-eq p3, p4, :cond_0

    const/16 p4, 0x432

    if-ne p3, p4, :cond_1

    :cond_0
    const/4 p2, 0x1

    :cond_1
    if-nez p2, :cond_2

    if-lt p5, p6, :cond_3

    .line 658
    :cond_2
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->c(Lcom/ss/android/socialbase/downloader/f/m;)Z

    :cond_3
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/net/InetAddress;",
            ">;)V"
        }
    .end annotation

    .line 352
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-eqz v0, :cond_0

    goto :goto_3

    :cond_0
    const/4 v0, 0x0

    .line 357
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/ss/android/socialbase/downloader/f/k;->c(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 359
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    move-object p2, v0

    .line 361
    :goto_0
    monitor-enter p0

    if-eqz p2, :cond_1

    .line 363
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/ss/android/socialbase/downloader/f/k;->b(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :cond_1
    :goto_1
    const/4 p1, 0x0

    .line 365
    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->l:Z

    .line 366
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->b:Lcom/ss/android/socialbase/downloader/f/n;

    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->i:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/ss/android/socialbase/downloader/f/n;->a(I)V

    const-string p1, "SegmentDispatcher"

    const-string p2, "onDnsResolved: dispatchReadThread"

    .line 367
    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->e()V

    .line 369
    monitor-exit p0

    return-void

    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_2
    :goto_3
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/i;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 105
    :try_start_0
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->f()V

    .line 108
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->b(Ljava/util/List;)V

    .line 111
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->e()V

    .line 114
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->g()V

    .line 117
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->i()V

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 122
    :try_start_1
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 124
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/4 p1, 0x0

    sub-long/2addr v3, v1

    .line 125
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1, v3, v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->increaseAllConnectTime(J)V

    .line 128
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1, v3, v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setFirstSpeedTime(J)V

    .line 131
    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-nez p1, :cond_4

    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-eqz p1, :cond_0

    goto :goto_2

    .line 135
    :cond_0
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->d:Lcom/ss/android/socialbase/downloader/h/f;

    iget-wide v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->m:J

    invoke-interface {p1, v1, v2}, Lcom/ss/android/socialbase/downloader/h/f;->a(J)V

    .line 137
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->h()V

    .line 140
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 143
    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-nez p1, :cond_1

    const-string p1, "SegmentDispatcher"

    const-string v1, "finally pause"

    .line 144
    invoke-static {p1, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/k;->b()V

    .line 150
    :cond_1
    :try_start_3
    monitor-enter p0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 152
    :goto_0
    :try_start_4
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    .line 153
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ss/android/socialbase/downloader/f/i;

    if-nez p1, :cond_2

    goto :goto_0

    .line 157
    :cond_2
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-direct {p0, v1, p1, v0}, Lcom/ss/android/socialbase/downloader/f/k;->a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/f/i;Z)V

    goto :goto_0

    .line 159
    :cond_3
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->c(Ljava/util/List;)V

    .line 160
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw p1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-exception p1

    .line 162
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 164
    :goto_1
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->u:Lcom/ss/android/socialbase/downloader/h/e;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/h/e;->b()V

    return-void

    .line 143
    :cond_4
    :goto_2
    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-nez p1, :cond_5

    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-nez p1, :cond_5

    const-string p1, "SegmentDispatcher"

    const-string v1, "finally pause"

    .line 144
    invoke-static {p1, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/k;->b()V

    .line 150
    :cond_5
    :try_start_6
    monitor-enter p0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 152
    :goto_3
    :try_start_7
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_7

    .line 153
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ss/android/socialbase/downloader/f/i;

    if-nez p1, :cond_6

    goto :goto_3

    .line 157
    :cond_6
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-direct {p0, v1, p1, v0}, Lcom/ss/android/socialbase/downloader/f/k;->a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/f/i;Z)V

    goto :goto_3

    .line 159
    :cond_7
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/k;->c(Ljava/util/List;)V

    .line 160
    monitor-exit p0

    goto :goto_4

    :catchall_1
    move-exception p1

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw p1
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1

    :catch_1
    move-exception p1

    .line 162
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 164
    :goto_4
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->u:Lcom/ss/android/socialbase/downloader/h/e;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/h/e;->b()V

    return-void

    :catchall_2
    move-exception p1

    .line 124
    :try_start_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/4 v5, 0x0

    sub-long/2addr v3, v1

    .line 125
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1, v3, v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->increaseAllConnectTime(J)V

    .line 128
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1, v3, v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setFirstSpeedTime(J)V

    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :catchall_3
    move-exception p1

    .line 143
    iget-boolean v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->f:Z

    if-nez v1, :cond_8

    const-string v1, "SegmentDispatcher"

    const-string v2, "finally pause"

    .line 144
    invoke-static {v1, v2}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/k;->b()V

    .line 150
    :cond_8
    :try_start_a
    monitor-enter p0
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2

    .line 152
    :goto_5
    :try_start_b
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 153
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->n:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/i;

    if-nez v1, :cond_9

    goto :goto_5

    .line 157
    :cond_9
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-direct {p0, v2, v1, v0}, Lcom/ss/android/socialbase/downloader/f/k;->a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/f/i;Z)V

    goto :goto_5

    .line 159
    :cond_a
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->o:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/ss/android/socialbase/downloader/f/k;->c(Ljava/util/List;)V

    .line 160
    monitor-exit p0

    goto :goto_6

    :catchall_4
    move-exception v0

    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :try_start_c
    throw v0
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2

    :catch_2
    move-exception v0

    .line 162
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 164
    :goto_6
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->u:Lcom/ss/android/socialbase/downloader/h/e;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/h/e;->b()V

    throw p1
.end method

.method public b()V
    .locals 2

    const-string v0, "SegmentDispatcher"

    const-string v1, "pause"

    .line 1009
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1010
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->g:Z

    .line 1011
    monitor-enter p0

    .line 1012
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/f/m;

    .line 1013
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/f/m;->e()V

    goto :goto_0

    .line 1015
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1016
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->e:Lcom/ss/android/socialbase/downloader/f/g;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/g;->b()V

    .line 1017
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->c:Lcom/ss/android/socialbase/downloader/f/b;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/b;->c()V

    return-void

    :catchall_0
    move-exception v0

    .line 1015
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lcom/ss/android/socialbase/downloader/f/m;)V
    .locals 3

    const-string v0, "SegmentDispatcher"

    .line 688
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReaderExit: threadIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    monitor-enter p0

    const/4 v0, 0x1

    .line 690
    :try_start_0
    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/f/m;->c(Z)V

    .line 691
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 692
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->m()V

    .line 693
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 695
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->k()V

    goto :goto_1

    .line 697
    :cond_0
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->n()Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "SegmentDispatcher"

    const-string v0, "onReaderExit: allContentDownloaded"

    .line 698
    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/k;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ss/android/socialbase/downloader/f/m;

    .line 700
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/m;->e()V

    goto :goto_0

    .line 702
    :cond_1
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/k;->k()V

    .line 705
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public b(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 614
    monitor-enter p0

    .line 616
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/ss/android/socialbase/downloader/f/k;->e(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V

    .line 617
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public c(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V
    .locals 3

    .line 622
    monitor-enter p0

    .line 623
    :try_start_0
    iget-object v0, p2, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    if-ne v0, p1, :cond_0

    const-string v0, "SegmentDispatcher"

    .line 624
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unApplySegment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/m;->d()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/ss/android/socialbase/downloader/f/i;->d(J)V

    const/4 v0, 0x0

    .line 627
    iput-object v0, p2, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    .line 628
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/m;->a()V

    .line 630
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public d(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)Lcom/ss/android/socialbase/downloader/f/e;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 635
    monitor-enter p0

    .line 636
    :try_start_0
    new-instance p1, Lcom/ss/android/socialbase/downloader/f/l;

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/k;->a:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/k;->c:Lcom/ss/android/socialbase/downloader/f/b;

    invoke-direct {p1, v0, v1, p2}, Lcom/ss/android/socialbase/downloader/f/l;-><init>(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/f/b;Lcom/ss/android/socialbase/downloader/f/i;)V

    .line 637
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/f/k;->e:Lcom/ss/android/socialbase/downloader/f/g;

    invoke-virtual {p2, p1}, Lcom/ss/android/socialbase/downloader/f/g;->a(Lcom/ss/android/socialbase/downloader/f/l;)V

    .line 638
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/l;->a()Lcom/ss/android/socialbase/downloader/f/e;

    move-result-object p1

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 639
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
