.class public final Lcom/ss/android/socialbase/downloader/f/i;
.super Ljava/lang/Object;
.source "Segment.java"


# instance fields
.field volatile a:Lcom/ss/android/socialbase/downloader/f/m;

.field b:I

.field private final c:J

.field private final d:Ljava/util/concurrent/atomic/AtomicLong;

.field private volatile e:J

.field private f:J

.field private g:I


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    const/4 v0, 0x0

    .line 44
    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->b:I

    .line 53
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    .line 54
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 55
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/i;->e:J

    cmp-long v0, p3, p1

    if-ltz v0, :cond_0

    .line 57
    iput-wide p3, p0, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    goto :goto_0

    :cond_0
    const-wide/16 p1, -0x1

    .line 59
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    :goto_0
    return-void
.end method

.method public constructor <init>(Lcom/ss/android/socialbase/downloader/f/i;)V
    .locals 3

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    const/4 v0, 0x0

    .line 44
    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->b:I

    .line 64
    iget-wide v0, p1, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    .line 65
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p1, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 66
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->e:J

    .line 67
    iget-wide v0, p1, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    .line 68
    iget p1, p1, Lcom/ss/android/socialbase/downloader/f/i;->g:I

    iput p1, p0, Lcom/ss/android/socialbase/downloader/f/i;->g:I

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 2

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    const/4 v0, 0x0

    .line 44
    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->b:I

    const-string v0, "st"

    .line 72
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    const-string v0, "en"

    .line 73
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/ss/android/socialbase/downloader/f/i;->c(J)V

    const-string v0, "cu"

    .line 74
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/ss/android/socialbase/downloader/f/i;->a(J)V

    .line 75
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/i;->d()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/ss/android/socialbase/downloader/f/i;->d(J)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method a(I)V
    .locals 0

    .line 164
    iput p1, p0, Lcom/ss/android/socialbase/downloader/f/i;->g:I

    return-void
.end method

.method public a(J)V
    .locals 3

    .line 129
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 130
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    :cond_0
    return-void
.end method

.method public b()J
    .locals 5

    .line 102
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    iget-wide v2, p0, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    .line 103
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method b(I)V
    .locals 0

    .line 180
    iput p1, p0, Lcom/ss/android/socialbase/downloader/f/i;->b:I

    return-void
.end method

.method b(J)V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    return-void
.end method

.method public c()J
    .locals 2

    .line 109
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    return-wide v0
.end method

.method c(J)V
    .locals 3

    .line 143
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 144
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    goto :goto_0

    :cond_0
    const-string v0, "Segment"

    .line 146
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEndOffset: endOffset = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", segment = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, -0x1

    cmp-long v2, p1, v0

    if-nez v2, :cond_1

    .line 148
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    :cond_1
    :goto_0
    return-void
.end method

.method public d()J
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public d(J)V
    .locals 3

    .line 154
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 155
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/i;->e:J

    :cond_0
    return-void
.end method

.method public e()J
    .locals 5

    .line 117
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->a:Lcom/ss/android/socialbase/downloader/f/m;

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/m;->d()J

    move-result-wide v0

    .line 120
    iget-wide v2, p0, Lcom/ss/android/socialbase/downloader/f/i;->e:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    return-wide v0

    .line 125
    :cond_0
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->e:J

    return-wide v0
.end method

.method public f()J
    .locals 2

    .line 139
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    return-wide v0
.end method

.method public g()I
    .locals 1

    .line 160
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->g:I

    return v0
.end method

.method h()V
    .locals 1

    .line 168
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->b:I

    return-void
.end method

.method i()V
    .locals 1

    .line 172
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->b:I

    return-void
.end method

.method j()I
    .locals 1

    .line 176
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/i;->b:I

    return v0
.end method

.method public k()Lorg/json/JSONObject;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 211
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "st"

    .line 212
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/i;->c()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "cu"

    .line 213
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/i;->d()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "en"

    .line 214
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Segment{startOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/ss/android/socialbase/downloader/f/i;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ",\t currentOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",\t currentOffsetRead="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/i;->e()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ",\t endOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/ss/android/socialbase/downloader/f/i;->f:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
