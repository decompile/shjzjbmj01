.class Lcom/ss/android/socialbase/downloader/f/m;
.super Ljava/lang/Object;
.source "SegmentReader.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private volatile A:Z

.field private B:I

.field private C:I

.field private D:I

.field private E:J

.field private F:I

.field private G:Z

.field private H:Lcom/ss/android/socialbase/downloader/exception/BaseException;

.field private I:Z

.field private J:Z

.field private K:Lcom/ss/android/socialbase/downloader/i/d;

.field a:Lcom/ss/android/socialbase/downloader/f/q;

.field final b:I

.field volatile c:J

.field volatile d:J

.field volatile e:J

.field volatile f:J

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field private final j:Lcom/ss/android/socialbase/downloader/f/f;

.field private final k:Lcom/ss/android/socialbase/downloader/f/c;

.field private final l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

.field private final m:Lcom/ss/android/socialbase/downloader/g/a;

.field private n:Lcom/ss/android/socialbase/downloader/network/i;

.field private o:Lcom/ss/android/socialbase/downloader/model/d;

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/f/i;",
            ">;"
        }
    .end annotation
.end field

.field private q:J

.field private volatile r:J

.field private volatile s:J

.field private volatile t:J

.field private volatile u:J

.field private v:Ljava/util/concurrent/Future;

.field private volatile w:Z

.field private volatile x:Z

.field private volatile y:Z

.field private z:Ljava/lang/Thread;


# direct methods
.method constructor <init>(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/f/k;Lcom/ss/android/socialbase/downloader/f/c;Lcom/ss/android/socialbase/downloader/f/q;I)V
    .locals 2

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->p:Ljava/util/List;

    const-wide/16 v0, -0x1

    .line 66
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->t:J

    .line 116
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 117
    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    .line 118
    iput-object p3, p0, Lcom/ss/android/socialbase/downloader/f/m;->k:Lcom/ss/android/socialbase/downloader/f/c;

    .line 119
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result p1

    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object p1

    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->m:Lcom/ss/android/socialbase/downloader/g/a;

    .line 120
    iput-object p4, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    .line 121
    iput p5, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    return-void
.end method

.method private a(Lcom/ss/android/socialbase/downloader/f/c;Ljava/io/InputStream;)Lcom/ss/android/socialbase/downloader/f/a;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 609
    invoke-interface {p1}, Lcom/ss/android/socialbase/downloader/f/c;->b()Lcom/ss/android/socialbase/downloader/f/a;

    move-result-object v0

    .line 611
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/f/a;->a:[B

    array-length v1, v1

    const/16 v2, 0x400

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v2, -0x1

    .line 614
    :try_start_0
    iget-object v3, v0, Lcom/ss/android/socialbase/downloader/f/a;->a:[B

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eq p2, v2, :cond_1

    .line 618
    :try_start_1
    iput p2, v0, Lcom/ss/android/socialbase/downloader/f/a;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne p2, v2, :cond_0

    .line 622
    invoke-interface {p1, v0}, Lcom/ss/android/socialbase/downloader/f/c;->a(Lcom/ss/android/socialbase/downloader/f/a;)V

    :cond_0
    return-object v0

    .line 616
    :cond_1
    :try_start_2
    new-instance v1, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v3, 0x431

    const-string v4, "probe"

    invoke-direct {v1, v3, v4}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    goto :goto_0

    :catchall_1
    move-exception v1

    const/4 p2, -0x1

    :goto_0
    if-ne p2, v2, :cond_2

    .line 622
    invoke-interface {p1, v0}, Lcom/ss/android/socialbase/downloader/f/c;->a(Lcom/ss/android/socialbase/downloader/f/a;)V

    :cond_2
    throw v1
.end method

.method private a(Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/model/d;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 419
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/model/d;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 420
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/i;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    const/16 v0, 0x3ec

    if-gtz v4, :cond_1

    const-string v1, "SegmentReader"

    .line 424
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseHttpResponse: segment.getCurrentOffset = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/i;->d()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/model/d;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 428
    :cond_0
    new-instance v1, Lcom/ss/android/socialbase/downloader/exception/b;

    iget v2, p2, Lcom/ss/android/socialbase/downloader/model/d;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "2: response code error : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p2, Lcom/ss/android/socialbase/downloader/model/d;->c:I

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " segment="

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v0, v2, p1}, Lcom/ss/android/socialbase/downloader/exception/b;-><init>(IILjava/lang/String;)V

    throw v1

    .line 422
    :cond_1
    new-instance v1, Lcom/ss/android/socialbase/downloader/exception/b;

    iget v2, p2, Lcom/ss/android/socialbase/downloader/model/d;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "1: response code error : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p2, Lcom/ss/android/socialbase/downloader/model/d;->c:I

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " segment="

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v0, v2, p1}, Lcom/ss/android/socialbase/downloader/exception/b;-><init>(IILjava/lang/String;)V

    throw v1

    :cond_2
    :goto_0
    return-void
.end method

.method private a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z
    .locals 2

    .line 321
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/e;->c(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 326
    :cond_0
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    iget-object p1, p1, Lcom/ss/android/socialbase/downloader/f/q;->a:Ljava/lang/String;

    .line 327
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "https"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    .line 331
    :cond_1
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isNeedHttpsToHttpRetry()Z

    move-result p1

    if-nez p1, :cond_2

    return v0

    .line 335
    :cond_2
    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->G:Z

    if-eqz p1, :cond_3

    return v0

    :cond_3
    const/4 p1, 0x1

    .line 341
    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->G:Z

    .line 342
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->l()V

    return p1

    :cond_4
    :goto_0
    return v0
.end method

.method private a(Lcom/ss/android/socialbase/downloader/f/i;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 188
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->k()V

    .line 193
    :goto_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/m;->b(Lcom/ss/android/socialbase/downloader/f/i;)V

    .line 196
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/m;->d(Lcom/ss/android/socialbase/downloader/f/i;)V
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/f/j; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x1

    .line 236
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->i()V

    return p1

    :catchall_0
    move-exception p1

    goto/16 :goto_4

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "SegmentReader"

    .line 203
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "download: e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ", threadIndex = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ", reconnect = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/ss/android/socialbase/downloader/f/m;->x:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, ", closed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/ss/android/socialbase/downloader/f/m;->w:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-boolean v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->w:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 236
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->i()V

    return v2

    .line 207
    :cond_0
    :try_start_2
    iget-boolean v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->x:Z

    if-eqz v1, :cond_2

    .line 208
    iput-boolean v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->x:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 210
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v0

    .line 212
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 214
    :goto_1
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->y:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-nez v0, :cond_1

    .line 236
    :goto_2
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->i()V

    goto :goto_0

    .line 215
    :cond_1
    :try_start_5
    iput-boolean v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->y:Z

    .line 216
    new-instance p1, Lcom/ss/android/socialbase/downloader/f/j;

    const/4 v0, 0x5

    const-string v1, "download"

    invoke-direct {p1, v0, v1}, Lcom/ss/android/socialbase/downloader/f/j;-><init>(ILjava/lang/String;)V

    throw p1

    .line 220
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v1, 0x0

    .line 222
    instance-of v3, v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    if-eqz v3, :cond_3

    .line 223
    move-object v1, v0

    check-cast v1, Lcom/ss/android/socialbase/downloader/exception/BaseException;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    :cond_3
    :try_start_6
    const-string v3, "download"

    .line 226
    invoke-static {v0, v3}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_6
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v1, v0

    :goto_3
    if-eqz v1, :cond_4

    .line 231
    :try_start_7
    invoke-direct {p0, p1, v1}, Lcom/ss/android/socialbase/downloader/f/m;->a(Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v0, :cond_4

    goto :goto_2

    .line 236
    :cond_4
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->i()V

    return v2

    :catch_3
    move-exception p1

    .line 200
    :try_start_8
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->H:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 201
    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 236
    :goto_4
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->i()V

    throw p1
.end method

.method private a(Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z
    .locals 8

    const-string v0, "SegmentReader"

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleDownloadFailed:  e = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", curRetryCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->D:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", retryCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->C:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/f/m;->H:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 300
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/q;->b()V

    .line 302
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    iget v6, p0, Lcom/ss/android/socialbase/downloader/f/m;->D:I

    iget v7, p0, Lcom/ss/android/socialbase/downloader/f/m;->C:I

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    invoke-interface/range {v1 .. v7}, Lcom/ss/android/socialbase/downloader/f/f;->a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/exception/BaseException;II)V

    .line 303
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->D:I

    iget v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->C:I

    const/4 v2, 0x1

    if-ge v0, v1, :cond_0

    .line 304
    iget p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->D:I

    add-int/2addr p1, v2

    iput p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->D:I

    return v2

    .line 310
    :cond_0
    invoke-direct {p0, p2}, Lcom/ss/android/socialbase/downloader/f/m;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    invoke-interface {v0, p0, v1, p1, p2}, Lcom/ss/android/socialbase/downloader/f/f;->a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    const/4 p1, 0x0

    return p1
.end method

.method private b(Lcom/ss/android/socialbase/downloader/f/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Lcom/ss/android/socialbase/downloader/exception/h;
        }
    .end annotation

    .line 348
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/f/m;->c(Lcom/ss/android/socialbase/downloader/f/i;)V

    .line 351
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->o:Lcom/ss/android/socialbase/downloader/model/d;

    invoke-direct {p0, p1, v0}, Lcom/ss/android/socialbase/downloader/f/m;->a(Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/model/d;)V

    .line 354
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->o:Lcom/ss/android/socialbase/downloader/model/d;

    invoke-interface {v0, p0, p1, v1, v2}, Lcom/ss/android/socialbase/downloader/f/f;->a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;Lcom/ss/android/socialbase/downloader/f/q;Lcom/ss/android/socialbase/downloader/model/d;)V

    .line 356
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/f/q;->c()V

    return-void
.end method

.method private c(Lcom/ss/android/socialbase/downloader/f/i;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    move-object/from16 v1, p0

    .line 361
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    .line 362
    iput-wide v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->d:J

    .line 363
    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->c:J

    .line 364
    invoke-virtual/range {p1 .. p1}, Lcom/ss/android/socialbase/downloader/f/i;->d()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->q:J

    .line 365
    invoke-virtual/range {p1 .. p1}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->s:J

    .line 366
    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->q:J

    iput-wide v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->t:J

    .line 367
    new-instance v0, Lcom/ss/android/socialbase/downloader/i/d;

    invoke-direct {v0}, Lcom/ss/android/socialbase/downloader/i/d;-><init>()V

    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->K:Lcom/ss/android/socialbase/downloader/i/d;

    .line 369
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getExtraHeaders()Ljava/util/List;

    move-result-object v4

    .line 370
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->geteTag()Ljava/lang/String;

    move-result-object v5

    iget-wide v6, v1, Lcom/ss/android/socialbase/downloader/f/m;->q:J

    iget-wide v8, v1, Lcom/ss/android/socialbase/downloader/f/m;->s:J

    invoke-static/range {v4 .. v9}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/util/List;Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v14

    .line 374
    new-instance v0, Lcom/ss/android/socialbase/downloader/model/c;

    const-string v4, "Segment-Index"

    invoke-virtual/range {p1 .. p1}, Lcom/ss/android/socialbase/downloader/f/i;->g()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lcom/ss/android/socialbase/downloader/model/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    new-instance v0, Lcom/ss/android/socialbase/downloader/model/c;

    const-string v4, "Thread-Index"

    iget v5, v1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lcom/ss/android/socialbase/downloader/model/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v14, v0}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    .line 379
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    iget-object v0, v0, Lcom/ss/android/socialbase/downloader/f/q;->a:Ljava/lang/String;

    .line 380
    iget-boolean v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->G:Z

    if-eqz v4, :cond_0

    .line 381
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "https"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "https"

    const-string v5, "http"

    .line 382
    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 385
    :cond_0
    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    iget-object v4, v4, Lcom/ss/android/socialbase/downloader/f/q;->b:Ljava/lang/String;

    const-string v5, "SegmentReader"

    .line 386
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "createConnectionBegin: url = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ", ip = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ", segment = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v7, p1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v7, ", threadIndex = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, v1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->g:Ljava/lang/String;

    .line 389
    iput-object v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->h:Ljava/lang/String;

    .line 391
    iget-object v5, v1, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 392
    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isNeedDefaultHttpServiceBackUp()Z

    move-result v10

    iget-object v5, v1, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getMaxBytes()I

    move-result v11

    const/4 v15, 0x0

    iget-wide v5, v1, Lcom/ss/android/socialbase/downloader/f/m;->E:J

    const/4 v7, 0x0

    sub-long/2addr v2, v5

    const-wide/16 v5, 0xbb8

    cmp-long v7, v2, v5

    if-lez v7, :cond_1

    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->m:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v3, "monitor_download_connect"

    .line 393
    invoke-virtual {v2, v3}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    const/16 v16, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    const/16 v16, 0x0

    :goto_0
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    move-object v12, v0

    move-object v13, v4

    move-object/from16 v17, v2

    .line 391
    invoke-static/range {v10 .. v17}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(ZILjava/lang/String;Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/i;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 398
    iput-object v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->n:Lcom/ss/android/socialbase/downloader/network/i;

    .line 399
    new-instance v3, Lcom/ss/android/socialbase/downloader/model/d;

    invoke-direct {v3, v0, v2}, Lcom/ss/android/socialbase/downloader/model/d;-><init>(Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/g;)V

    iput-object v3, v1, Lcom/ss/android/socialbase/downloader/f/m;->o:Lcom/ss/android/socialbase/downloader/model/d;

    .line 401
    iget-boolean v3, v1, Lcom/ss/android/socialbase/downloader/f/m;->w:Z

    if-nez v3, :cond_3

    .line 404
    instance-of v3, v2, Lcom/ss/android/socialbase/downloader/network/a;

    if-eqz v3, :cond_2

    .line 405
    check-cast v2, Lcom/ss/android/socialbase/downloader/network/a;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/network/a;->e()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->i:Ljava/lang/String;

    :cond_2
    const-string v2, "SegmentReader"

    .line 407
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createConnectionSuccess: url = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", ip = "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", hostRealIp = "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->i:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", threadIndex = "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 402
    :cond_3
    new-instance v0, Lcom/ss/android/socialbase/downloader/f/p;

    const-string v2, "createConn"

    invoke-direct {v0, v2}, Lcom/ss/android/socialbase/downloader/f/p;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396
    :cond_4
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v2, 0x3fe

    new-instance v3, Ljava/io/IOException;

    const-string v4, "download can\'t continue, chunk connection is null"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2, v3}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw v0
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "createConn"

    .line 412
    invoke-static {v0, v2}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->d:J

    return-void

    :catch_1
    move-exception v0

    .line 410
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 414
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->d:J

    throw v0
.end method

.method private d(Lcom/ss/android/socialbase/downloader/f/i;)V
    .locals 33
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 435
    iget-object v3, v1, Lcom/ss/android/socialbase/downloader/f/m;->k:Lcom/ss/android/socialbase/downloader/f/c;

    .line 440
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    .line 441
    invoke-virtual/range {p1 .. p1}, Lcom/ss/android/socialbase/downloader/f/i;->d()J

    move-result-wide v4

    const/4 v8, 0x0

    .line 447
    :try_start_0
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->n:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->a()Ljava/io/InputStream;

    move-result-object v10
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_0 .. :try_end_0} :catch_21
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1e
    .catchall {:try_start_0 .. :try_end_0} :catchall_f

    if-eqz v10, :cond_11

    .line 452
    :try_start_1
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->o:Lcom/ss/android/socialbase/downloader/model/d;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/d;->h()J

    move-result-wide v11

    const-wide/16 v13, 0x0

    cmp-long v0, v11, v13

    if-eqz v0, :cond_10

    .line 463
    invoke-direct {v1, v3, v10}, Lcom/ss/android/socialbase/downloader/f/m;->a(Lcom/ss/android/socialbase/downloader/f/c;Ljava/io/InputStream;)Lcom/ss/android/socialbase/downloader/f/a;

    move-result-object v15
    :try_end_1
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_1 .. :try_end_1} :catch_1d
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1c
    .catchall {:try_start_1 .. :try_end_1} :catchall_e

    .line 465
    :try_start_2
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->x:Z

    if-nez v0, :cond_f

    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->w:Z

    if-nez v0, :cond_f

    .line 470
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    invoke-interface {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/f/f;->b(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V

    .line 473
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    invoke-interface {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/f/f;->d(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)Lcom/ss/android/socialbase/downloader/f/e;

    move-result-object v9
    :try_end_2
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_2 .. :try_end_2} :catch_1b
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1a
    .catchall {:try_start_2 .. :try_end_2} :catchall_d

    .line 477
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v17
    :try_end_3
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_3 .. :try_end_3} :catch_19
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_18
    .catchall {:try_start_3 .. :try_end_3} :catchall_c

    cmp-long v0, v17, v13

    const-wide v17, 0x7fffffffffffffffL

    const-wide/16 v19, 0x1

    if-lez v0, :cond_0

    .line 479
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Lcom/ss/android/socialbase/downloader/f/i;->f()J

    move-result-wide v11
    :try_end_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    move-wide v6, v4

    :goto_0
    move-object v8, v9

    goto/16 :goto_20

    :catch_0
    move-exception v0

    move-wide v6, v4

    :goto_1
    move-object v8, v9

    goto/16 :goto_15

    :catch_1
    move-exception v0

    move-wide v6, v4

    move-object/from16 v30, v9

    goto/16 :goto_17

    :cond_0
    cmp-long v0, v11, v13

    if-lez v0, :cond_1

    add-long/2addr v11, v4

    sub-long v11, v11, v19

    goto :goto_2

    :cond_1
    move-wide/from16 v11, v17

    .line 490
    :goto_2
    :try_start_5
    iget v0, v15, Lcom/ss/android/socialbase/downloader/f/a;->c:I
    :try_end_5
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_5 .. :try_end_5} :catch_19
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_18
    .catchall {:try_start_5 .. :try_end_5} :catchall_c

    int-to-long v6, v0

    add-long v23, v4, v6

    cmp-long v0, v23, v11

    if-lez v0, :cond_2

    add-long v25, v11, v19

    cmp-long v0, v23, v25

    if-lez v0, :cond_2

    sub-long v27, v23, v25

    sub-long v6, v6, v27

    long-to-int v0, v6

    .line 495
    :try_start_6
    iput v0, v15, Lcom/ss/android/socialbase/downloader/f/a;->c:I
    :try_end_6
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-wide/from16 v6, v25

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object v8, v9

    move-object v9, v15

    move-wide/from16 v6, v23

    goto/16 :goto_25

    :catch_2
    move-exception v0

    move-object v8, v9

    move-object v9, v10

    move-wide/from16 v6, v23

    goto/16 :goto_1a

    :catch_3
    move-exception v0

    move-object/from16 v30, v9

    move-object v9, v10

    move-wide/from16 v6, v23

    goto/16 :goto_23

    :cond_2
    move-wide/from16 v6, v23

    .line 499
    :goto_3
    :try_start_7
    iput-wide v6, v1, Lcom/ss/android/socialbase/downloader/f/m;->t:J

    .line 500
    invoke-interface {v9, v15}, Lcom/ss/android/socialbase/downloader/f/e;->b(Lcom/ss/android/socialbase/downloader/f/a;)V
    :try_end_7
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_7 .. :try_end_7} :catch_17
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_16
    .catchall {:try_start_7 .. :try_end_7} :catchall_b

    .line 505
    :goto_4
    :try_start_8
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->A:Z
    :try_end_8
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_8 .. :try_end_8} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_14
    .catchall {:try_start_8 .. :try_end_8} :catchall_a

    if-eqz v0, :cond_6

    .line 508
    :try_start_9
    monitor-enter p0
    :try_end_9
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_9 .. :try_end_9} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_8
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 509
    :try_start_a
    iput-boolean v8, v1, Lcom/ss/android/socialbase/downloader/f/m;->A:Z

    .line 510
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->x:Z

    if-nez v0, :cond_5

    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->w:Z

    if-nez v0, :cond_5

    .line 513
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    move-object/from16 v30, v9

    .line 515
    :try_start_b
    iget-wide v8, v1, Lcom/ss/android/socialbase/downloader/f/m;->r:J

    cmp-long v0, v8, v13

    if-eqz v0, :cond_7

    const-string v0, "SegmentReader"

    .line 517
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "loopAndRead:  change readEnd = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v9, ", segmentNewEndOffset = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v11, v1, Lcom/ss/android/socialbase/downloader/f/m;->r:J

    invoke-virtual {v8, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v9, ", segment = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/f/m;->m()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    add-long v11, v8, v19

    move-wide/from16 v31, v8

    sub-long v8, v6, v11

    cmp-long v0, v8, v13

    if-lez v0, :cond_3

    const-string v0, "SegmentReader"

    .line 523
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "loopAndRead: redundant = "

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_b .. :try_end_b} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_9

    .line 526
    :cond_3
    :try_start_c
    iput-wide v11, v1, Lcom/ss/android/socialbase/downloader/f/m;->t:J
    :try_end_c
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_c .. :try_end_c} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    move-wide v6, v11

    move-object/from16 v9, v30

    move-wide/from16 v11, v31

    goto :goto_a

    :catchall_2
    move-exception v0

    move-wide v6, v11

    goto :goto_6

    :catch_4
    move-exception v0

    move-object v9, v10

    move-wide v6, v11

    goto :goto_8

    :catch_5
    move-exception v0

    move-object v9, v10

    move-wide v6, v11

    goto/16 :goto_14

    :cond_4
    move-wide/from16 v31, v8

    move-wide/from16 v11, v31

    goto :goto_9

    :cond_5
    move-object/from16 v30, v9

    .line 511
    :try_start_d
    new-instance v0, Lcom/ss/android/socialbase/downloader/f/p;

    const-string v8, "loopAndRead"

    invoke-direct {v0, v8}, Lcom/ss/android/socialbase/downloader/f/p;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_3
    move-exception v0

    move-object/from16 v30, v9

    .line 513
    :goto_5
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    :try_start_e
    throw v0
    :try_end_e
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_e .. :try_end_e} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_9

    :catch_6
    move-exception v0

    goto :goto_7

    :catch_7
    move-exception v0

    goto/16 :goto_13

    :catchall_4
    move-exception v0

    goto :goto_5

    :catchall_5
    move-exception v0

    move-object/from16 v30, v9

    :goto_6
    move-object/from16 v8, v30

    goto/16 :goto_18

    :catch_8
    move-exception v0

    move-object/from16 v30, v9

    :goto_7
    move-object v9, v10

    :goto_8
    move-object/from16 v8, v30

    goto/16 :goto_19

    :catch_9
    move-exception v0

    move-object/from16 v30, v9

    goto/16 :goto_13

    :cond_6
    move-object/from16 v30, v9

    .line 532
    :cond_7
    :goto_9
    :try_start_f
    invoke-interface {v3}, Lcom/ss/android/socialbase/downloader/f/c;->b()Lcom/ss/android/socialbase/downloader/f/a;

    move-result-object v9
    :try_end_f
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_f .. :try_end_f} :catch_13
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_12
    .catchall {:try_start_f .. :try_end_f} :catchall_9

    .line 533
    :try_start_10
    iget-object v0, v9, Lcom/ss/android/socialbase/downloader/f/a;->a:[B

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 534
    iput v0, v9, Lcom/ss/android/socialbase/downloader/f/a;->c:I
    :try_end_10
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_10 .. :try_end_10} :catch_11
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_8

    const/4 v8, -0x1

    if-ne v0, v8, :cond_8

    move-object/from16 v8, v30

    .line 536
    :try_start_11
    invoke-interface {v8, v9}, Lcom/ss/android/socialbase/downloader/f/e;->b(Lcom/ss/android/socialbase/downloader/f/a;)V

    const/4 v9, 0x0

    :goto_a
    const-wide/16 v13, 0x0

    goto :goto_b

    :catchall_6
    move-exception v0

    goto/16 :goto_25

    :catch_a
    move-exception v0

    goto/16 :goto_f

    :catch_b
    move-exception v0

    move-object/from16 v30, v8

    goto/16 :goto_10

    :cond_8
    move-object/from16 v8, v30

    int-to-long v13, v0

    add-long/2addr v6, v13

    cmp-long v0, v6, v11

    if-lez v0, :cond_e

    add-long v19, v11, v19

    cmp-long v0, v6, v19

    if-lez v0, :cond_9

    sub-long v23, v6, v19

    sub-long v13, v13, v23

    long-to-int v0, v13

    .line 546
    iput v0, v9, Lcom/ss/android/socialbase/downloader/f/a;->c:I

    move-wide/from16 v6, v19

    .line 549
    :cond_9
    iput-wide v6, v1, Lcom/ss/android/socialbase/downloader/f/m;->t:J

    .line 550
    invoke-interface {v8, v9}, Lcom/ss/android/socialbase/downloader/f/e;->b(Lcom/ss/android/socialbase/downloader/f/a;)V
    :try_end_11
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_11 .. :try_end_11} :catch_b
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_a
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    :try_start_12
    const-string v0, "SegmentReader"

    .line 552
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "loopAndRead: bytesRead = "

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v13, 0x0

    sub-long v13, v6, v4

    invoke-virtual {v9, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v13, ", url = "

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    iget-object v13, v13, Lcom/ss/android/socialbase/downloader/f/q;->a:Ljava/lang/String;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_12
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_12 .. :try_end_12} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_e
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    move-object v9, v8

    goto :goto_a

    :goto_b
    cmp-long v0, v11, v13

    if-lez v0, :cond_b

    cmp-long v0, v11, v17

    if-eqz v0, :cond_b

    cmp-long v0, v6, v11

    if-lez v0, :cond_a

    goto :goto_c

    .line 564
    :cond_a
    :try_start_13
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v8, 0x41b

    const-string v13, "range[%d, %d] , but readCurrent[%d] , readStart[%d]"

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/Object;

    .line 565
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    const/16 v17, 0x0

    aput-object v15, v14, v17

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    const/4 v12, 0x1

    aput-object v11, v14, v12

    const/4 v11, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v14, v11

    const/4 v11, 0x3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v14, v11

    .line 564
    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v0, v8, v11}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_13
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_13 .. :try_end_13} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_14
    .catchall {:try_start_13 .. :try_end_13} :catchall_a

    .line 580
    :cond_b
    :goto_c
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    invoke-interface {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/f/f;->c(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V

    const/4 v2, 0x1

    .line 581
    new-array v0, v2, [Ljava/io/Closeable;

    const/4 v2, 0x0

    aput-object v10, v0, v2

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    if-eqz v9, :cond_c

    .line 586
    :try_start_14
    invoke-interface {v3}, Lcom/ss/android/socialbase/downloader/f/c;->b()Lcom/ss/android/socialbase/downloader/f/a;

    move-result-object v0
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_d

    const/4 v2, -0x1

    .line 588
    :try_start_15
    iput v2, v0, Lcom/ss/android/socialbase/downloader/f/a;->c:I

    .line 589
    invoke-interface {v9, v0}, Lcom/ss/android/socialbase/downloader/f/e;->b(Lcom/ss/android/socialbase/downloader/f/a;)V
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_c

    goto :goto_d

    :catch_c
    move-object v9, v0

    goto :goto_e

    :catch_d
    :cond_c
    :goto_d
    const/4 v9, 0x0

    :goto_e
    if-eqz v9, :cond_d

    .line 596
    invoke-interface {v3, v9}, Lcom/ss/android/socialbase/downloader/f/c;->a(Lcom/ss/android/socialbase/downloader/f/a;)V

    .line 598
    :cond_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->f:J

    cmp-long v0, v6, v4

    if-lez v0, :cond_17

    .line 600
    iget-object v8, v1, Lcom/ss/android/socialbase/downloader/f/m;->m:Lcom/ss/android/socialbase/downloader/g/a;

    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/f/m;->g:Ljava/lang/String;

    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/f/m;->h:Ljava/lang/String;

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/f/m;->i:Ljava/lang/String;

    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->w:Z

    if-nez v0, :cond_16

    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->x:Z

    if-eqz v0, :cond_15

    goto/16 :goto_1e

    :catchall_7
    move-exception v0

    goto/16 :goto_18

    :catch_e
    move-exception v0

    goto :goto_11

    :catch_f
    move-exception v0

    goto :goto_12

    :cond_e
    const-wide/16 v13, 0x0

    .line 556
    :try_start_16
    iput-wide v6, v1, Lcom/ss/android/socialbase/downloader/f/m;->t:J

    .line 557
    invoke-interface {v8, v9}, Lcom/ss/android/socialbase/downloader/f/e;->b(Lcom/ss/android/socialbase/downloader/f/a;)V
    :try_end_16
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_16 .. :try_end_16} :catch_b
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_a
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    move-object v9, v8

    const/4 v8, 0x0

    goto/16 :goto_4

    :catchall_8
    move-exception v0

    goto/16 :goto_24

    :catch_10
    move-exception v0

    move-object/from16 v8, v30

    :goto_f
    move-object v15, v9

    goto :goto_15

    :catch_11
    move-exception v0

    move-object/from16 v8, v30

    :goto_10
    move-object v15, v9

    goto :goto_17

    :catchall_9
    move-exception v0

    goto/16 :goto_6

    :catch_12
    move-exception v0

    move-object/from16 v8, v30

    goto :goto_11

    :catch_13
    move-exception v0

    move-object/from16 v8, v30

    goto :goto_13

    :catchall_a
    move-exception v0

    move-object v8, v9

    goto/16 :goto_18

    :catch_14
    move-exception v0

    move-object v8, v9

    :goto_11
    move-object v9, v10

    goto/16 :goto_19

    :catch_15
    move-exception v0

    move-object v8, v9

    :goto_12
    move-object/from16 v30, v8

    :goto_13
    move-object v9, v10

    :goto_14
    const/4 v15, 0x0

    goto/16 :goto_23

    :catchall_b
    move-exception v0

    goto/16 :goto_0

    :catch_16
    move-exception v0

    goto/16 :goto_1

    :catch_17
    move-exception v0

    move-object v8, v9

    goto :goto_16

    :catchall_c
    move-exception v0

    move-object v8, v9

    move-wide v6, v4

    goto/16 :goto_20

    :catch_18
    move-exception v0

    move-object v8, v9

    move-wide v6, v4

    :goto_15
    move-object v9, v10

    goto/16 :goto_1a

    :catch_19
    move-exception v0

    move-object v8, v9

    move-wide v6, v4

    :goto_16
    move-object/from16 v30, v8

    :goto_17
    move-object v9, v10

    goto/16 :goto_23

    .line 466
    :cond_f
    :try_start_17
    new-instance v0, Lcom/ss/android/socialbase/downloader/f/p;

    const-string v6, "probe"

    invoke-direct {v0, v6}, Lcom/ss/android/socialbase/downloader/f/p;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_17
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_17 .. :try_end_17} :catch_1b
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_1a
    .catchall {:try_start_17 .. :try_end_17} :catchall_d

    :catchall_d
    move-exception v0

    move-wide v6, v4

    move-object v9, v15

    const/4 v8, 0x0

    goto/16 :goto_25

    :catch_1a
    move-exception v0

    move-wide v6, v4

    move-object v9, v10

    const/4 v8, 0x0

    goto :goto_1a

    :catch_1b
    move-exception v0

    move-wide v6, v4

    move-object v9, v10

    goto/16 :goto_22

    .line 454
    :cond_10
    :try_start_18
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/f;

    const/16 v6, 0x3ec

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "the content-length is 0, contentLength = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v6, v7}, Lcom/ss/android/socialbase/downloader/exception/f;-><init>(ILjava/lang/String;)V

    throw v0

    .line 449
    :cond_11
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v6, 0x412

    new-instance v7, Ljava/io/IOException;

    const-string v8, "inputStream is null"

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v6, v7}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw v0
    :try_end_18
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_18 .. :try_end_18} :catch_1d
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_1c
    .catchall {:try_start_18 .. :try_end_18} :catchall_e

    :catchall_e
    move-exception v0

    move-wide v6, v4

    const/4 v8, 0x0

    :goto_18
    const/4 v9, 0x0

    goto/16 :goto_25

    :catch_1c
    move-exception v0

    move-wide v6, v4

    move-object v9, v10

    const/4 v8, 0x0

    goto :goto_19

    :catch_1d
    move-exception v0

    move-wide v6, v4

    move-object v9, v10

    goto/16 :goto_21

    :catchall_f
    move-exception v0

    move-wide v6, v4

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    goto/16 :goto_25

    :catch_1e
    move-exception v0

    move-wide v6, v4

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_19
    const/4 v15, 0x0

    :goto_1a
    :try_start_19
    const-string v10, "SegmentReader"

    .line 571
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "loopAndRead: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v12, ",stack = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_10

    :try_start_1a
    const-string v10, "loopAndRead"

    .line 574
    invoke-static {v0, v10}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1a
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_1a .. :try_end_1a} :catch_20
    .catchall {:try_start_1a .. :try_end_1a} :catchall_10

    .line 580
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    invoke-interface {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/f/f;->c(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V

    const/4 v2, 0x1

    .line 581
    new-array v0, v2, [Ljava/io/Closeable;

    const/4 v2, 0x0

    aput-object v9, v0, v2

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    if-eqz v8, :cond_13

    if-nez v15, :cond_12

    .line 586
    :try_start_1b
    invoke-interface {v3}, Lcom/ss/android/socialbase/downloader/f/c;->b()Lcom/ss/android/socialbase/downloader/f/a;

    move-result-object v0

    move-object v15, v0

    goto :goto_1b

    :catch_1f
    move-object v9, v15

    goto :goto_1c

    :cond_12
    :goto_1b
    const/4 v2, -0x1

    .line 588
    iput v2, v15, Lcom/ss/android/socialbase/downloader/f/a;->c:I

    .line 589
    invoke-interface {v8, v15}, Lcom/ss/android/socialbase/downloader/f/e;->b(Lcom/ss/android/socialbase/downloader/f/a;)V
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_1f

    const/4 v15, 0x0

    goto :goto_1d

    :goto_1c
    move-object v15, v9

    :cond_13
    :goto_1d
    if-eqz v15, :cond_14

    .line 596
    invoke-interface {v3, v15}, Lcom/ss/android/socialbase/downloader/f/c;->a(Lcom/ss/android/socialbase/downloader/f/a;)V

    .line 598
    :cond_14
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->f:J

    cmp-long v0, v6, v4

    if-lez v0, :cond_17

    .line 600
    iget-object v8, v1, Lcom/ss/android/socialbase/downloader/f/m;->m:Lcom/ss/android/socialbase/downloader/g/a;

    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/f/m;->g:Ljava/lang/String;

    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/f/m;->h:Ljava/lang/String;

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/f/m;->i:Ljava/lang/String;

    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->w:Z

    if-nez v0, :cond_16

    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->x:Z

    if-eqz v0, :cond_15

    goto :goto_1e

    :cond_15
    const/4 v13, 0x0

    goto :goto_1f

    :cond_16
    :goto_1e
    const/4 v13, 0x1

    :goto_1f
    iget-object v14, v1, Lcom/ss/android/socialbase/downloader/f/m;->n:Lcom/ss/android/socialbase/downloader/network/i;

    iget-object v15, v1, Lcom/ss/android/socialbase/downloader/f/m;->H:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    sub-long v16, v6, v4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->f:J

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    sub-long/2addr v2, v4

    .line 602
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v18

    .line 600
    invoke-static/range {v8 .. v19}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/g/a;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/ss/android/socialbase/downloader/network/i;Lcom/ss/android/socialbase/downloader/exception/BaseException;JJ)V

    :cond_17
    return-void

    :catch_20
    move-exception v0

    .line 576
    :try_start_1c
    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->H:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 577
    throw v0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_10

    :catchall_10
    move-exception v0

    move-object v10, v9

    :goto_20
    move-object v9, v15

    goto :goto_25

    :catch_21
    move-exception v0

    move-wide v6, v4

    const/4 v9, 0x0

    :goto_21
    const/4 v15, 0x0

    :goto_22
    const/16 v30, 0x0

    .line 568
    :goto_23
    :try_start_1d
    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/f/m;->H:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 569
    throw v0
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_11

    :catchall_11
    move-exception v0

    move-object v10, v9

    move-object v9, v15

    :goto_24
    move-object/from16 v8, v30

    .line 580
    :goto_25
    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    invoke-interface {v11, v1, v2}, Lcom/ss/android/socialbase/downloader/f/f;->c(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V

    const/4 v2, 0x1

    .line 581
    new-array v11, v2, [Ljava/io/Closeable;

    const/16 v17, 0x0

    aput-object v10, v11, v17

    invoke-static {v11}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    if-eqz v8, :cond_19

    if-nez v9, :cond_18

    .line 586
    :try_start_1e
    invoke-interface {v3}, Lcom/ss/android/socialbase/downloader/f/c;->b()Lcom/ss/android/socialbase/downloader/f/a;

    move-result-object v10

    move-object v9, v10

    :cond_18
    const/4 v10, -0x1

    .line 588
    iput v10, v9, Lcom/ss/android/socialbase/downloader/f/a;->c:I

    .line 589
    invoke-interface {v8, v9}, Lcom/ss/android/socialbase/downloader/f/e;->b(Lcom/ss/android/socialbase/downloader/f/a;)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_22

    const/4 v9, 0x0

    :catch_22
    :cond_19
    if-eqz v9, :cond_1a

    .line 596
    invoke-interface {v3, v9}, Lcom/ss/android/socialbase/downloader/f/c;->a(Lcom/ss/android/socialbase/downloader/f/a;)V

    .line 598
    :cond_1a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, v1, Lcom/ss/android/socialbase/downloader/f/m;->f:J

    cmp-long v3, v6, v4

    if-lez v3, :cond_1d

    .line 600
    iget-object v3, v1, Lcom/ss/android/socialbase/downloader/f/m;->m:Lcom/ss/android/socialbase/downloader/g/a;

    iget-object v8, v1, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/f/m;->g:Ljava/lang/String;

    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/f/m;->h:Ljava/lang/String;

    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/f/m;->i:Ljava/lang/String;

    iget-boolean v12, v1, Lcom/ss/android/socialbase/downloader/f/m;->w:Z

    if-nez v12, :cond_1c

    iget-boolean v12, v1, Lcom/ss/android/socialbase/downloader/f/m;->x:Z

    if-eqz v12, :cond_1b

    goto :goto_26

    :cond_1b
    const/16 v23, 0x0

    goto :goto_27

    :cond_1c
    :goto_26
    const/16 v23, 0x1

    :goto_27
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/f/m;->n:Lcom/ss/android/socialbase/downloader/network/i;

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/f/m;->H:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    sub-long v26, v6, v4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v5, v1, Lcom/ss/android/socialbase/downloader/f/m;->f:J

    iget-wide v13, v1, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    sub-long/2addr v5, v13

    .line 602
    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v28

    move-object/from16 v18, v3

    move-object/from16 v19, v8

    move-object/from16 v20, v9

    move-object/from16 v21, v10

    move-object/from16 v22, v11

    move-object/from16 v24, v2

    move-object/from16 v25, v12

    .line 600
    invoke-static/range {v18 .. v29}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/g/a;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/ss/android/socialbase/downloader/network/i;Lcom/ss/android/socialbase/downloader/exception/BaseException;JJ)V

    :cond_1d
    throw v0
.end method

.method private i()V
    .locals 2

    .line 242
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->c:J

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->E:J

    const-wide/16 v0, -0x1

    .line 243
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->c:J

    .line 244
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->d:J

    .line 245
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->e:J

    .line 246
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->f:J

    .line 247
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->j()V

    return-void
.end method

.method private j()V
    .locals 4

    .line 251
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->n:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v0, :cond_0

    :try_start_0
    const-string v1, "SegmentReader"

    .line 254
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "closeConnection: thread = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->d()V

    .line 256
    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->c()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private k()V
    .locals 1

    const/4 v0, 0x0

    .line 264
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->G:Z

    .line 265
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->l()V

    return-void
.end method

.method private l()V
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    iget-boolean v0, v0, Lcom/ss/android/socialbase/downloader/f/q;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRetryCount()I

    move-result v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->l:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getBackUpUrlRetryCount()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->C:I

    const/4 v0, 0x0

    .line 271
    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->D:I

    return-void
.end method

.method private m()J
    .locals 5

    .line 628
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->r:J

    const-wide/16 v2, 0x0

    .line 629
    iput-wide v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->r:J

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    return-wide v0

    :cond_0
    return-wide v0
.end method


# virtual methods
.method a(JJ)J
    .locals 1

    .line 778
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->K:Lcom/ss/android/socialbase/downloader/i/d;

    if-nez v0, :cond_0

    const-wide/16 p1, -0x1

    return-wide p1

    .line 782
    :cond_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/ss/android/socialbase/downloader/i/d;->b(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method a()V
    .locals 7

    .line 652
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    .line 654
    :try_start_0
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 655
    :try_start_1
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/m;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    .line 657
    iget-wide v4, p0, Lcom/ss/android/socialbase/downloader/f/m;->u:J

    const/4 v6, 0x0

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/ss/android/socialbase/downloader/f/m;->u:J

    .line 658
    invoke-virtual {v0, v2, v3}, Lcom/ss/android/socialbase/downloader/f/q;->a(J)V

    :cond_0
    const-wide/16 v2, -0x1

    .line 660
    iput-wide v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->t:J

    .line 661
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :goto_0
    return-void
.end method

.method a(Ljava/util/concurrent/Future;)V
    .locals 0

    .line 741
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->v:Ljava/util/concurrent/Future;

    return-void
.end method

.method public a(Z)V
    .locals 3

    const-string v0, "SegmentReader"

    .line 722
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reconnect: threadIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    monitor-enter p0

    .line 724
    :try_start_0
    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->y:Z

    const/4 p1, 0x1

    .line 725
    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->x:Z

    .line 726
    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->A:Z

    .line 727
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 728
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->j()V

    .line 729
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->z:Ljava/lang/Thread;

    if-eqz p1, :cond_0

    :try_start_1
    const-string v0, "SegmentReader"

    .line 732
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reconnect: t.interrupt threadIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_0
    return-void

    :catchall_0
    move-exception p1

    .line 727
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public a(J)Z
    .locals 6

    .line 637
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->s:J

    const-wide/16 v2, 0x0

    cmp-long v4, p1, v2

    const/4 v5, 0x0

    if-gtz v4, :cond_0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    return v5

    :cond_0
    cmp-long v4, p1, v0

    if-lez v4, :cond_1

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    return v5

    .line 645
    :cond_1
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->r:J

    const/4 p1, 0x1

    .line 646
    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->A:Z

    return p1
.end method

.method a(Lcom/ss/android/socialbase/downloader/f/q;)Z
    .locals 2

    .line 275
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->F:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 278
    :cond_0
    iget v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->F:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->F:I

    .line 279
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    if-eqz v0, :cond_1

    .line 281
    invoke-virtual {v0, p0}, Lcom/ss/android/socialbase/downloader/f/q;->b(Lcom/ss/android/socialbase/downloader/f/m;)V

    .line 283
    :cond_1
    invoke-virtual {p1, p0}, Lcom/ss/android/socialbase/downloader/f/q;->a(Lcom/ss/android/socialbase/downloader/f/m;)V

    .line 284
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    .line 285
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->l()V

    return v1
.end method

.method public b()J
    .locals 6

    .line 671
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    monitor-enter v0

    .line 672
    :try_start_0
    iget-wide v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->u:J

    .line 673
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/f/m;->c()J

    move-result-wide v3

    const/4 v5, 0x0

    add-long/2addr v1, v3

    .line 674
    monitor-exit v0

    return-wide v1

    :catchall_0
    move-exception v1

    .line 675
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method b(J)V
    .locals 6

    .line 765
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->t:J

    .line 766
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->K:Lcom/ss/android/socialbase/downloader/i/d;

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-ltz v5, :cond_1

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v3, "SegmentReader"

    .line 770
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "markProgress: curSegmentReadOffset = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, ", threadIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    invoke-virtual {v2, v0, v1, p1, p2}, Lcom/ss/android/socialbase/downloader/i/d;->a(JJ)Z

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method b(Z)V
    .locals 0

    .line 745
    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->I:Z

    return-void
.end method

.method public c()J
    .locals 8

    .line 682
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    monitor-enter v0

    .line 683
    :try_start_0
    iget-wide v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->t:J

    .line 684
    iget-wide v3, p0, Lcom/ss/android/socialbase/downloader/f/m;->q:J

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-ltz v7, :cond_0

    cmp-long v7, v1, v3

    if-lez v7, :cond_0

    sub-long/2addr v1, v3

    .line 686
    monitor-exit v0

    return-wide v1

    .line 688
    :cond_0
    monitor-exit v0

    return-wide v5

    :catchall_0
    move-exception v1

    .line 689
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public c(Z)V
    .locals 0

    .line 757
    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/f/m;->J:Z

    return-void
.end method

.method public d()J
    .locals 2

    .line 693
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->t:J

    return-wide v0
.end method

.method public e()V
    .locals 3

    const-string v0, "SegmentReader"

    .line 697
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "close: threadIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    monitor-enter p0

    const/4 v0, 0x1

    .line 699
    :try_start_0
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->w:Z

    .line 700
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->A:Z

    .line 701
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 702
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/f/m;->j()V

    .line 703
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->v:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    .line 705
    iput-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->v:Ljava/util/concurrent/Future;

    .line 707
    :try_start_1
    invoke-interface {v1, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 709
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    .line 701
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public f()V
    .locals 1

    const/4 v0, 0x0

    .line 715
    invoke-virtual {p0, v0}, Lcom/ss/android/socialbase/downloader/f/m;->a(Z)V

    return-void
.end method

.method g()Z
    .locals 1

    .line 749
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->I:Z

    return v0
.end method

.method h()J
    .locals 2

    .line 806
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->q:J

    return-wide v0
.end method

.method public run()V
    .locals 5

    const/16 v0, 0xa

    .line 126
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    const/4 v0, 0x0

    .line 129
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iput-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->z:Ljava/lang/Thread;

    .line 130
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    invoke-interface {v1, p0}, Lcom/ss/android/socialbase/downloader/f/f;->a(Lcom/ss/android/socialbase/downloader/f/m;)V

    .line 131
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    invoke-virtual {v1, p0}, Lcom/ss/android/socialbase/downloader/f/q;->a(Lcom/ss/android/socialbase/downloader/f/m;)V

    .line 135
    :goto_0
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    invoke-interface {v1, p0, v2}, Lcom/ss/android/socialbase/downloader/f/f;->a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/q;)Lcom/ss/android/socialbase/downloader/f/i;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "SegmentReader"

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no more segment, thread_index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto/16 :goto_3

    .line 144
    :cond_0
    :try_start_1
    invoke-direct {p0, v1}, Lcom/ss/android/socialbase/downloader/f/m;->a(Lcom/ss/android/socialbase/downloader/f/i;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 146
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->p:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/ss/android/socialbase/downloader/f/j; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170
    :try_start_2
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    :goto_1
    invoke-interface {v2, p0, v1}, Lcom/ss/android/socialbase/downloader/f/f;->a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 150
    :cond_1
    :try_start_3
    iget-boolean v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->w:Z

    if-nez v2, :cond_2

    const-string v2, "SegmentReader"

    .line 151
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "download segment failed, segment = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, ", thread_index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", failedException = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/f/m;->H:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/ss/android/socialbase/downloader/f/j; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 170
    :cond_2
    :try_start_4
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    :goto_2
    invoke-interface {v2, p0, v1}, Lcom/ss/android/socialbase/downloader/f/f;->a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    :catchall_0
    move-exception v2

    goto :goto_4

    .line 160
    :catch_0
    :try_start_5
    iget v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->B:I

    const/16 v3, 0x32

    if-lt v2, v3, :cond_3

    const-string v2, "SegmentReader"

    .line 162
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "segment apply failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/ss/android/socialbase/downloader/f/m;->B:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "times, thread_index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/ss/android/socialbase/downloader/f/m;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 170
    :try_start_6
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 177
    :goto_3
    :try_start_7
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    invoke-virtual {v1, p0}, Lcom/ss/android/socialbase/downloader/f/q;->b(Lcom/ss/android/socialbase/downloader/f/m;)V

    .line 178
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_5

    .line 167
    :cond_3
    :try_start_8
    iget v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->B:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->B:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 170
    :try_start_9
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    goto :goto_1

    :goto_4
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    invoke-interface {v3, p0, v1}, Lcom/ss/android/socialbase/downloader/f/f;->a(Lcom/ss/android/socialbase/downloader/f/m;Lcom/ss/android/socialbase/downloader/f/i;)V

    throw v2
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    .line 174
    :try_start_a
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 177
    :try_start_b
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    invoke-virtual {v1, p0}, Lcom/ss/android/socialbase/downloader/f/q;->b(Lcom/ss/android/socialbase/downloader/f/m;)V

    .line 178
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    :goto_5
    invoke-interface {v1, p0}, Lcom/ss/android/socialbase/downloader/f/f;->b(Lcom/ss/android/socialbase/downloader/f/m;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_2

    .line 182
    :catch_2
    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->z:Ljava/lang/Thread;

    return-void

    .line 177
    :goto_6
    :try_start_c
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->a:Lcom/ss/android/socialbase/downloader/f/q;

    invoke-virtual {v2, p0}, Lcom/ss/android/socialbase/downloader/f/q;->b(Lcom/ss/android/socialbase/downloader/f/m;)V

    .line 178
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/f/m;->j:Lcom/ss/android/socialbase/downloader/f/f;

    invoke-interface {v2, p0}, Lcom/ss/android/socialbase/downloader/f/f;->b(Lcom/ss/android/socialbase/downloader/f/m;)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_3

    .line 182
    :catch_3
    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/f/m;->z:Ljava/lang/Thread;

    throw v1
.end method
