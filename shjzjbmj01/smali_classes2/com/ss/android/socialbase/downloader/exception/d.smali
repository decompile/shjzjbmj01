.class public Lcom/ss/android/socialbase/downloader/exception/d;
.super Lcom/ss/android/socialbase/downloader/exception/BaseException;
.source "DownloadOutOfSpaceException.java"


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 4

    const-string v0, "space is not enough required space is : %s but available space is :%s"

    const/4 v1, 0x2

    .line 11
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x3ee

    invoke-direct {p0, v1, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    .line 13
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/exception/d;->a:J

    .line 14
    iput-wide p3, p0, Lcom/ss/android/socialbase/downloader/exception/d;->b:J

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .line 19
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/exception/d;->a:J

    return-wide v0
.end method

.method public b()J
    .locals 2

    .line 23
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/exception/d;->b:J

    return-wide v0
.end method
