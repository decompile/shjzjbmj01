.class public Lcom/ss/android/socialbase/downloader/h/c;
.super Ljava/lang/Object;
.source "DownloadRunnable.java"

# interfaces
.implements Lcom/ss/android/socialbase/downloader/h/f;
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Ljava/lang/String; = "c"


# instance fields
.field private A:Lcom/ss/android/socialbase/downloader/depend/r;

.field private volatile B:I

.field private C:Z

.field private D:Z

.field private E:Ljava/lang/String;

.field private F:Z

.field private G:J

.field private H:J

.field private final I:Lcom/ss/android/socialbase/downloader/g/a;

.field private J:I

.field private volatile K:Lcom/ss/android/socialbase/downloader/f/k;

.field private b:Ljava/util/concurrent/Future;

.field private final c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

.field private volatile d:Z

.field private e:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ss/android/socialbase/downloader/h/b;",
            ">;"
        }
    .end annotation
.end field

.field private volatile g:Lcom/ss/android/socialbase/downloader/downloader/d;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private final m:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile n:Lcom/ss/android/socialbase/downloader/constants/i;

.field private final o:Lcom/ss/android/socialbase/downloader/downloader/i;

.field private p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

.field private q:Lcom/ss/android/socialbase/downloader/downloader/g;

.field private final r:Lcom/ss/android/socialbase/downloader/downloader/g;

.field private s:Lcom/ss/android/socialbase/downloader/downloader/f;

.field private final t:Lcom/ss/android/socialbase/downloader/downloader/f;

.field private u:Lcom/ss/android/socialbase/downloader/downloader/q;

.field private final v:Lcom/ss/android/socialbase/downloader/downloader/e;

.field private volatile w:Lcom/ss/android/socialbase/downloader/exception/BaseException;

.field private x:Lcom/ss/android/socialbase/downloader/network/i;

.field private y:Lcom/ss/android/socialbase/downloader/network/g;

.field private z:Lcom/ss/android/socialbase/downloader/depend/x;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/ss/android/socialbase/downloader/model/DownloadTask;Landroid/os/Handler;)V
    .locals 2

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 95
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->d:Z

    .line 97
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    .line 106
    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->a:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    const/4 v1, 0x5

    .line 127
    iput v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->B:I

    .line 129
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->C:Z

    .line 130
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->D:Z

    .line 133
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->F:Z

    .line 139
    iput v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->J:I

    const/4 v0, 0x0

    .line 141
    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    .line 145
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    if-eqz p1, :cond_0

    .line 147
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getDownloadInfo()Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 148
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getChunkStrategy()Lcom/ss/android/socialbase/downloader/downloader/g;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->q:Lcom/ss/android/socialbase/downloader/downloader/g;

    .line 149
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getChunkAdjustCalculator()Lcom/ss/android/socialbase/downloader/downloader/f;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->s:Lcom/ss/android/socialbase/downloader/downloader/f;

    .line 150
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getForbiddenHandler()Lcom/ss/android/socialbase/downloader/depend/x;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->z:Lcom/ss/android/socialbase/downloader/depend/x;

    .line 151
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getDiskSpaceHandler()Lcom/ss/android/socialbase/downloader/depend/r;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->A:Lcom/ss/android/socialbase/downloader/depend/r;

    .line 152
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/model/DownloadTask;)Lcom/ss/android/socialbase/downloader/downloader/q;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->u:Lcom/ss/android/socialbase/downloader/downloader/q;

    .line 153
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->I:Lcom/ss/android/socialbase/downloader/g/a;

    goto :goto_0

    .line 155
    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/g/a;->c()Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->I:Lcom/ss/android/socialbase/downloader/g/a;

    .line 158
    :goto_0
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->h()V

    .line 159
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->x()Lcom/ss/android/socialbase/downloader/downloader/i;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    .line 160
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->H()Lcom/ss/android/socialbase/downloader/downloader/g;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->r:Lcom/ss/android/socialbase/downloader/downloader/g;

    .line 161
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->J()Lcom/ss/android/socialbase/downloader/downloader/f;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->t:Lcom/ss/android/socialbase/downloader/downloader/f;

    .line 162
    new-instance v0, Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-direct {v0, p1, p2}, Lcom/ss/android/socialbase/downloader/downloader/e;-><init>(Lcom/ss/android/socialbase/downloader/model/DownloadTask;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    .line 164
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private A()J
    .locals 3

    .line 1017
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->u:Lcom/ss/android/socialbase/downloader/downloader/q;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurRetryTimeInTotal()I

    move-result v1

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalRetryCount()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/downloader/q;->a(II)J

    move-result-wide v0

    return-wide v0
.end method

.method private B()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/h;,
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 1022
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v0

    .line 1023
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v1}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)I

    move-result v1

    .line 1024
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isDownloaded()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isExpiredRedownload()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->F:Z

    if-eqz v2, :cond_0

    goto :goto_0

    .line 1025
    :cond_0
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v1, 0x3f1

    const-string v2, "file has downloaded"

    invoke-direct {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1028
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v2, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->b(I)Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1031
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->C()Lcom/ss/android/socialbase/downloader/impls/a;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 1033
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v4

    if-eq v4, v0, :cond_4

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2, v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->equalsTask(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1035
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/ss/android/socialbase/downloader/impls/a;->a(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1040
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v3, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->c(I)Ljava/util/List;

    move-result-object v3

    .line 1041
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v4}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    .line 1042
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v4, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->f(I)Z

    if-eqz v2, :cond_4

    .line 1044
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isBreakpointAvailable()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1045
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->copyFromCacheData(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Z)V

    .line 1046
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {v1, v2}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z

    if-eqz v3, :cond_2

    .line 1049
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/model/b;

    .line 1050
    invoke-virtual {v2, v0}, Lcom/ss/android/socialbase/downloader/model/b;->b(I)V

    .line 1051
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v3, v2}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/b;)V

    goto :goto_1

    .line 1054
    :cond_2
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/h;

    const-string v1, "retry task because id generator changed"

    invoke-direct {v0, v1}, Lcom/ss/android/socialbase/downloader/exception/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1036
    :cond_3
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/i;->f(I)Z

    .line 1037
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v1, 0x401

    const-string v2, "another same task is downloading"

    invoke-direct {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_4
    return-void
.end method

.method private C()Z
    .locals 3

    .line 1226
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isExpiredRedownload()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->i:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 1227
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result v0

    if-le v0, v2, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isChunkDowngradeRetryUsed()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 1231
    :cond_1
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->j:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->l:Z

    if-nez v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1

    :cond_3
    :goto_0
    return v1
.end method

.method private D()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    const-wide/16 v0, 0x0

    .line 1338
    :try_start_0
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTempPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->d(Ljava/lang/String;)J

    move-result-wide v2
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-wide v2, v0

    .line 1341
    :goto_0
    sget-object v4, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkSpaceOverflowInProgress: available = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2, v3}, Lcom/ss/android/socialbase/downloader/i/e;->a(J)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v6, "MB"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    cmp-long v4, v2, v0

    if-lez v4, :cond_1

    .line 1343
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v4

    iget-object v6, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v6

    sub-long/2addr v4, v6

    cmp-long v6, v2, v4

    if-gez v6, :cond_1

    .line 1346
    iget-object v6, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v6

    invoke-static {v6}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v6

    const-string v7, "space_fill_min_keep_mb"

    const/16 v8, 0x64

    invoke-virtual {v6, v7, v8}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v6

    if-lez v6, :cond_1

    int-to-long v7, v6

    const-wide/32 v9, 0x100000

    mul-long v7, v7, v9

    sub-long v7, v2, v7

    .line 1351
    sget-object v11, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "checkSpaceOverflowInProgress: minKeep  = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, "MB, canDownload = "

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1352
    invoke-static {v7, v8}, Lcom/ss/android/socialbase/downloader/i/e;->a(J)D

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v6, "MB"

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1351
    invoke-static {v11, v6}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    cmp-long v6, v7, v0

    if-lez v6, :cond_0

    .line 1358
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v0

    add-long/2addr v7, v9

    add-long/2addr v0, v7

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->G:J

    return-void

    .line 1354
    :cond_0
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->G:J

    .line 1355
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/d;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/ss/android/socialbase/downloader/exception/d;-><init>(JJ)V

    throw v0

    .line 1365
    :cond_1
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->G:J

    return-void
.end method

.method private E()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/f;
        }
    .end annotation

    .line 1370
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isOnlyWifi()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->L()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/i/e;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1371
    :cond_0
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/f;

    const/16 v1, 0x3fb

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "android.permission.ACCESS_NETWORK_STATE"

    aput-object v4, v2, v3

    const-string v3, "download task need permission:%s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/exception/f;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1374
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isDownloadWithWifiValid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1378
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isPauseReserveWithWifiValid()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 1379
    :cond_2
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/e;

    invoke-direct {v0}, Lcom/ss/android/socialbase/downloader/exception/e;-><init>()V

    throw v0

    .line 1375
    :cond_3
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/c;

    invoke-direct {v0}, Lcom/ss/android/socialbase/downloader/exception/c;-><init>()V

    throw v0
.end method

.method private F()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 1385
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getSavePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1388
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1391
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getSavePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1392
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1394
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1396
    :cond_0
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v1, 0x406

    const-string v2, "download savePath directory can not created"

    invoke-direct {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1398
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_0
    return-void

    .line 1399
    :cond_2
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v1, 0x407

    const-string v2, "download savePath is not a directory"

    invoke-direct {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1389
    :cond_3
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v1, 0x405

    const-string v2, "download name can not be empty"

    invoke-direct {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1386
    :cond_4
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v1, 0x404

    const-string v2, "download savePath can not be empty"

    invoke-direct {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method private G()V
    .locals 7

    .line 1561
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->e(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)J

    move-result-wide v0

    .line 1562
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 1564
    sget-object v4, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkTaskCanResume: offset = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, ", curBytes = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    :cond_0
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2, v0, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setCurBytes(J)V

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 1567
    :goto_0
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->i:Z

    .line 1568
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->i:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->F:Z

    if-nez v0, :cond_2

    .line 1569
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v1, "checkTaskCanResume: deleteAllDownloadFiles"

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1570
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->d(I)V

    .line 1571
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->m(I)V

    .line 1572
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    :cond_2
    return-void
.end method

.method private H()V
    .locals 3

    .line 1622
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearCurrentDownloadData::"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->d(I)V

    .line 1625
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->m(I)V

    .line 1626
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    const/4 v0, 0x0

    .line 1627
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->i:Z

    .line 1628
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->resetDataForEtagEndure(Ljava/lang/String;)V

    .line 1629
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1631
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private I()V
    .locals 4

    .line 1662
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1663
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/h/b;

    if-eqz v1, :cond_0

    .line 1665
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/h/b;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1669
    sget-object v1, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancelAllChunkRunnable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private J()V
    .locals 2

    .line 1683
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1684
    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->C()Lcom/ss/android/socialbase/downloader/impls/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1686
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/impls/a;->l(I)Z

    :cond_1
    return-void
.end method

.method private K()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method private a(JLjava/util/List;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/b;",
            ">;)I"
        }
    .end annotation

    .line 229
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->C()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    .line 230
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->i:Z

    if-eqz v0, :cond_1

    if-eqz p3, :cond_0

    .line 232
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    goto :goto_1

    .line 234
    :cond_0
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result p3

    goto :goto_1

    .line 237
    :cond_1
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->q:Lcom/ss/android/socialbase/downloader/downloader/g;

    if-eqz p3, :cond_2

    .line 238
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->q:Lcom/ss/android/socialbase/downloader/downloader/g;

    invoke-interface {p3, p1, p2}, Lcom/ss/android/socialbase/downloader/downloader/g;->a(J)I

    move-result p3

    goto :goto_0

    .line 240
    :cond_2
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->r:Lcom/ss/android/socialbase/downloader/downloader/g;

    invoke-interface {p3, p1, p2}, Lcom/ss/android/socialbase/downloader/downloader/g;->a(J)I

    move-result p3

    .line 242
    :goto_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/network/k;->a()Lcom/ss/android/socialbase/downloader/network/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/network/k;->b()Lcom/ss/android/socialbase/downloader/network/l;

    move-result-object v0

    .line 243
    sget-object v3, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v4, "NetworkQuality is : %s"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/network/l;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/network/l;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setNetworkQuality(Ljava/lang/String;)V

    .line 246
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->s:Lcom/ss/android/socialbase/downloader/downloader/f;

    if-eqz v3, :cond_3

    .line 247
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->s:Lcom/ss/android/socialbase/downloader/downloader/f;

    invoke-interface {v3, p3, v0}, Lcom/ss/android/socialbase/downloader/downloader/f;->a(ILcom/ss/android/socialbase/downloader/network/l;)I

    move-result p3

    goto :goto_1

    .line 249
    :cond_3
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->t:Lcom/ss/android/socialbase/downloader/downloader/f;

    invoke-interface {v3, p3, v0}, Lcom/ss/android/socialbase/downloader/downloader/f;->a(ILcom/ss/android/socialbase/downloader/network/l;)I

    move-result p3

    :goto_1
    if-gtz p3, :cond_5

    :cond_4
    const/4 p3, 0x1

    .line 258
    :cond_5
    invoke-static {}, Lcom/ss/android/socialbase/downloader/c/a;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 259
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v3, "chunk count : %s for %s contentLen:%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    const/4 v1, 0x2

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    return p3
.end method

.method private a(Lcom/ss/android/socialbase/downloader/model/DownloadTask;)Lcom/ss/android/socialbase/downloader/downloader/q;
    .locals 1

    .line 1979
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getRetryDelayTimeCalculator()Lcom/ss/android/socialbase/downloader/downloader/q;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 1983
    :cond_0
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getDownloadInfo()Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 1985
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRetryDelayTimeArray()Ljava/lang/String;

    move-result-object p1

    .line 1986
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1987
    new-instance v0, Lcom/ss/android/socialbase/downloader/impls/q;

    invoke-direct {v0, p1}, Lcom/ss/android/socialbase/downloader/impls/q;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 1990
    :cond_1
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->K()Lcom/ss/android/socialbase/downloader/downloader/q;

    move-result-object p1

    return-object p1
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;J)Lcom/ss/android/socialbase/downloader/model/b;
    .locals 3

    .line 848
    new-instance v0, Lcom/ss/android/socialbase/downloader/model/b$a;

    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/ss/android/socialbase/downloader/model/b$a;-><init>(I)V

    const/4 v1, -0x1

    .line 849
    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/model/b$a;->a(I)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v0

    const-wide/16 v1, 0x0

    .line 850
    invoke-virtual {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/model/b$a;->a(J)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v0

    .line 851
    invoke-virtual {v0, p1, p2}, Lcom/ss/android/socialbase/downloader/model/b$a;->e(J)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v0

    .line 852
    invoke-virtual {v0, p1, p2}, Lcom/ss/android/socialbase/downloader/model/b$a;->b(J)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v0

    .line 853
    invoke-virtual {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/model/b$a;->c(J)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v0

    .line 854
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v1

    sub-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/model/b$a;->d(J)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object p0

    .line 855
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/model/b$a;->a()Lcom/ss/android/socialbase/downloader/model/b;

    move-result-object p0

    return-object p0
.end method

.method private a(Lcom/ss/android/socialbase/downloader/model/b;I)Lcom/ss/android/socialbase/downloader/model/b;
    .locals 8

    .line 1921
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/b;->d()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x1

    .line 1924
    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/model/b;->c(Z)J

    move-result-wide v2

    .line 1926
    sget-object v4, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reuseChunk retainLen:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " chunkIndex:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    .line 1929
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/b;->f()Z

    move-result v5

    if-nez v5, :cond_1

    sget-wide v5, Lcom/ss/android/socialbase/downloader/constants/e;->f:J

    cmp-long v7, v2, v5

    if-lez v7, :cond_1

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isNeedReuseChunkRunnable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1930
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result v2

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/ss/android/socialbase/downloader/model/b;->a(IJ)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1932
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ss/android/socialbase/downloader/model/b;

    .line 1933
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v4, v3}, Lcom/ss/android/socialbase/downloader/downloader/i;->b(Lcom/ss/android/socialbase/downloader/model/b;)V

    goto :goto_0

    .line 1937
    :cond_1
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/b;->f()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v4, 0x1

    :cond_3
    if-eqz v4, :cond_8

    .line 1940
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/b;->f()Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    .line 1942
    :goto_1
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/b;->g()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 1943
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/b;->g()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ss/android/socialbase/downloader/model/b;

    if-eqz v3, :cond_5

    .line 1945
    sget-object v4, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "check can checkUnCompletedChunk -- chunkIndex:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " currentOffset:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/b;->n()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, "  startOffset:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/b;->l()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " contentLen:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/b;->q()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1946
    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result v4

    if-ltz v4, :cond_4

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/b;->i()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/b;->c()Z

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    move-object v1, v3

    goto :goto_2

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    :goto_2
    if-eqz v1, :cond_7

    .line 1954
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/b;->n()J

    move-result-wide v2

    .line 1956
    sget-object v4, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unComplete chunk "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result p1

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " curOffset:"

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " reuseChunk chunkIndex:"

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " for subChunk:"

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result p1

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v2

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result v3

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/b;->b()I

    move-result v4

    invoke-interface {p1, v2, v3, v4, p2}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(IIII)V

    .line 1958
    invoke-virtual {v1, p2}, Lcom/ss/android/socialbase/downloader/model/b;->c(I)V

    .line 1959
    invoke-virtual {v1, v0}, Lcom/ss/android/socialbase/downloader/model/b;->a(Z)V

    :cond_7
    return-object v1

    :cond_8
    return-object v1
.end method

.method private a(Lcom/ss/android/socialbase/downloader/model/b;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/ss/android/socialbase/downloader/model/b;",
            ")",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;"
        }
    .end annotation

    .line 859
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getExtraHeaders()Ljava/util/List;

    move-result-object v0

    .line 860
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->geteTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/util/List;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/model/b;)Ljava/util/List;

    move-result-object p1

    .line 861
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isExpiredRedownload()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getLastModified()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 862
    new-instance v0, Lcom/ss/android/socialbase/downloader/model/c;

    const-string v1, "if-modified-since"

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getLastModified()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/model/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 863
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dcache::add head IF_MODIFIED_SINCE="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getLastModified()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object p1
.end method

.method private a(ILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/b;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 1062
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 1065
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v0

    invoke-direct {p0, p2, v0, v1}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/util/List;J)V

    return-void

    .line 1063
    :cond_0
    new-instance p1, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 p2, 0x409

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    invoke-direct {p1, p2, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw p1
.end method

.method private a(JI)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move/from16 v3, p3

    int-to-long v4, v3

    .line 1070
    div-long v4, v1, v4

    .line 1071
    iget-object v6, v0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v6

    .line 1073
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-wide v11, v8

    :goto_0
    if-ge v10, v3, :cond_1

    add-int/lit8 v13, v3, -0x1

    if-ne v10, v13, :cond_0

    move-wide v13, v8

    goto :goto_1

    :cond_0
    const/4 v13, 0x0

    add-long v13, v11, v4

    const-wide/16 v15, 0x1

    sub-long/2addr v13, v15

    .line 1085
    :goto_1
    new-instance v15, Lcom/ss/android/socialbase/downloader/model/b$a;

    invoke-direct {v15, v6}, Lcom/ss/android/socialbase/downloader/model/b$a;-><init>(I)V

    .line 1086
    invoke-virtual {v15, v10}, Lcom/ss/android/socialbase/downloader/model/b$a;->a(I)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v15

    .line 1087
    invoke-virtual {v15, v11, v12}, Lcom/ss/android/socialbase/downloader/model/b$a;->a(J)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v15

    .line 1088
    invoke-virtual {v15, v11, v12}, Lcom/ss/android/socialbase/downloader/model/b$a;->e(J)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v15

    .line 1089
    invoke-virtual {v15, v11, v12}, Lcom/ss/android/socialbase/downloader/model/b$a;->b(J)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v15

    .line 1090
    invoke-virtual {v15, v13, v14}, Lcom/ss/android/socialbase/downloader/model/b$a;->c(J)Lcom/ss/android/socialbase/downloader/model/b$a;

    move-result-object v13

    .line 1091
    invoke-virtual {v13}, Lcom/ss/android/socialbase/downloader/model/b$a;->a()Lcom/ss/android/socialbase/downloader/model/b;

    move-result-object v13

    .line 1092
    invoke-interface {v7, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1093
    iget-object v14, v0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v14, v13}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/b;)V

    add-long/2addr v11, v4

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1097
    :cond_1
    iget-object v4, v0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4, v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setChunkCount(I)V

    .line 1098
    iget-object v4, v0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v4, v6, v3}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(II)Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 1100
    invoke-direct {v0, v7, v1, v2}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/util/List;J)V

    return-void
.end method

.method static synthetic a(Lcom/ss/android/socialbase/downloader/h/c;)V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->J()V

    return-void
.end method

.method static synthetic a(Lcom/ss/android/socialbase/downloader/h/c;Ljava/util/List;)V
    .locals 0

    .line 87
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Lcom/ss/android/socialbase/downloader/model/b;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/i;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 1217
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/b;->n()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/ss/android/socialbase/downloader/model/b;->a(J)V

    .line 1218
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setChunkCount(I)V

    .line 1219
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v2

    invoke-interface {v0, v2, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(II)Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 1220
    new-instance v0, Lcom/ss/android/socialbase/downloader/downloader/d;

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    move-object v3, v0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p1

    move-object v8, p0

    invoke-direct/range {v3 .. v8}, Lcom/ss/android/socialbase/downloader/downloader/d;-><init>(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/i;Lcom/ss/android/socialbase/downloader/model/b;Lcom/ss/android/socialbase/downloader/h/f;)V

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    .line 1221
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->t()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/h;
        }
    .end annotation

    .line 1414
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->d(I)V

    .line 1415
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->m(I)V

    .line 1416
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    const/4 v0, 0x0

    .line 1417
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->i:Z

    .line 1418
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0, p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->resetDataForEtagEndure(Ljava/lang/String;)V

    .line 1419
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {p1, v0}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z

    .line 1420
    new-instance p1, Lcom/ss/android/socialbase/downloader/exception/h;

    invoke-direct {p1, p2}, Lcom/ss/android/socialbase/downloader/exception/h;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Lcom/ss/android/socialbase/downloader/exception/h;
        }
    .end annotation

    .line 790
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 794
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 796
    invoke-static {}, Lcom/ss/android/socialbase/downloader/network/a/a;->a()Lcom/ss/android/socialbase/downloader/network/a/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/ss/android/socialbase/downloader/network/a/a;->b(Ljava/lang/String;Ljava/util/List;)Lcom/ss/android/socialbase/downloader/network/a/d;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    .line 799
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-virtual {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/network/g;)V

    .line 800
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setPreconnectLevel(I)V

    .line 801
    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    goto :goto_2

    .line 804
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isNeedDefaultHttpServiceBackUp()Z

    move-result v3

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 805
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getMaxBytes()I

    move-result v4

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->I:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v1, "net_lib_strategy"

    .line 806
    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;)I

    move-result v8

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->I:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v1, "monitor_download_connect"

    const/4 v5, 0x0

    .line 807
    invoke-virtual {v0, v1, v5}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_3

    const/4 v9, 0x1

    goto :goto_0

    :cond_3
    const/4 v9, 0x0

    :goto_0
    iget-object v10, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    move-object v5, p1

    move-object v7, p2

    .line 804
    invoke-static/range {v3 .. v10}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(ZILjava/lang/String;Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/i;

    move-result-object p1

    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_0
    move-exception p1

    .line 812
    :try_start_1
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/e;->f(Ljava/lang/Throwable;)Z

    move-result p2

    if-eqz p2, :cond_4

    const-string p1, ""

    const-string p2, "http code 416"

    .line 813
    invoke-direct {p0, p1, p2}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 814
    :cond_4
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/e;->e(Ljava/lang/Throwable;)Z

    move-result p2

    if-eqz p2, :cond_5

    const-string p1, ""

    const-string p2, "http code 412"

    .line 815
    invoke-direct {p0, p1, p2}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string p2, "CreateFirstConnection"

    .line 817
    invoke-static {p1, p2}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 820
    :goto_1
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-virtual {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/network/g;)V

    .line 824
    :goto_2
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz p1, :cond_6

    return-void

    .line 825
    :cond_6
    new-instance p1, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 p2, 0x3fe

    new-instance v0, Ljava/io/IOException;

    const-string v1, "download can\'t continue, firstConnection is null"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p1, p2, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw p1

    :catch_1
    move-exception p1

    .line 810
    :try_start_2
    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 820
    :goto_3
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-virtual {p0, p2}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/network/g;)V

    throw p1
.end method

.method private a(Ljava/lang/String;Ljava/util/List;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;J)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Lcom/ss/android/socialbase/downloader/exception/h;
        }
    .end annotation

    .line 706
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/ss/android/socialbase/downloader/h/c;->b(Ljava/lang/String;Ljava/util/List;J)V

    .line 707
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->y:Lcom/ss/android/socialbase/downloader/network/g;

    if-eqz v0, :cond_0

    .line 710
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->y:Lcom/ss/android/socialbase/downloader/network/g;

    invoke-virtual {p0, p1, v0, p3, p4}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/g;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x1

    .line 713
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->D:Z

    .line 718
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->y:Lcom/ss/android/socialbase/downloader/network/g;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->D:Z

    if-eqz v0, :cond_2

    .line 720
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 721
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/g;J)V

    :cond_2
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    .line 1674
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 1676
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v2, Lcom/ss/android/socialbase/downloader/constants/i;->g:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setForbiddenBackupUrls(Ljava/util/List;Z)V

    .line 1677
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->C()Lcom/ss/android/socialbase/downloader/impls/a;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 1679
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/impls/a;->l(I)Z

    :cond_2
    return-void

    :cond_3
    :goto_1
    return-void
.end method

.method private a(Ljava/util/List;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/b;",
            ">;J)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 1111
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ss/android/socialbase/downloader/model/b;

    if-nez v0, :cond_1

    goto :goto_0

    .line 1117
    :cond_1
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/b;->p()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-nez v5, :cond_2

    .line 1118
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/b;->n()J

    move-result-wide v1

    sub-long v1, p2, v1

    goto :goto_1

    .line 1120
    :cond_2
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/b;->p()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/b;->n()J

    move-result-wide v5

    sub-long/2addr v1, v5

    const-wide/16 v5, 0x1

    add-long/2addr v1, v5

    :goto_1
    cmp-long v5, v1, v3

    if-gtz v5, :cond_3

    goto :goto_0

    .line 1125
    :cond_3
    invoke-virtual {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/model/b;->a(J)V

    .line 1127
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isNeedReuseFirstConnection()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isHeadConnectionAvailable()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->D:Z

    if-eqz v1, :cond_6

    .line 1128
    :cond_4
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result v1

    if-nez v1, :cond_5

    .line 1129
    new-instance v1, Lcom/ss/android/socialbase/downloader/h/b;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-direct {v1, v0, v2, v3, p0}, Lcom/ss/android/socialbase/downloader/h/b;-><init>(Lcom/ss/android/socialbase/downloader/model/b;Lcom/ss/android/socialbase/downloader/model/DownloadTask;Lcom/ss/android/socialbase/downloader/network/i;Lcom/ss/android/socialbase/downloader/h/f;)V

    .line 1130
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1131
    :cond_5
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result v1

    if-lez v1, :cond_0

    .line 1132
    new-instance v1, Lcom/ss/android/socialbase/downloader/h/b;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    invoke-direct {v1, v0, v2, p0}, Lcom/ss/android/socialbase/downloader/h/b;-><init>(Lcom/ss/android/socialbase/downloader/model/b;Lcom/ss/android/socialbase/downloader/model/DownloadTask;Lcom/ss/android/socialbase/downloader/h/f;)V

    .line 1133
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1136
    :cond_6
    new-instance v1, Lcom/ss/android/socialbase/downloader/h/b;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    invoke-direct {v1, v0, v2, p0}, Lcom/ss/android/socialbase/downloader/h/b;-><init>(Lcom/ss/android/socialbase/downloader/model/b;Lcom/ss/android/socialbase/downloader/model/DownloadTask;Lcom/ss/android/socialbase/downloader/h/f;)V

    .line 1137
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    const/16 p1, 0x40

    .line 1141
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/a;->a(I)Z

    move-result p1

    if-eqz p1, :cond_e

    .line 1142
    new-instance p1, Ljava/util/ArrayList;

    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1143
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_a

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/ss/android/socialbase/downloader/h/b;

    .line 1144
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_8

    .line 1145
    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/h/b;->b()V

    goto :goto_2

    .line 1147
    :cond_8
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_9

    .line 1148
    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/h/b;->a()V

    goto :goto_2

    .line 1151
    :cond_9
    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_a
    const/4 p2, 0x0

    .line 1153
    invoke-interface {p1, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Runnable;

    .line 1157
    :try_start_0
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/impls/e;->d(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    :goto_3
    if-eqz p2, :cond_c

    .line 1161
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result p3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz p3, :cond_b

    return-void

    .line 1164
    :cond_b
    :try_start_1
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    move-exception p2

    .line 1166
    :try_start_2
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 1169
    :goto_4
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/impls/e;->e(Ljava/util/List;)Ljava/lang/Runnable;

    move-result-object p2

    goto :goto_3

    :cond_c
    if-eqz p1, :cond_13

    .line 1173
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_13

    .line 1174
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :catch_1
    :cond_d
    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_13

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/concurrent/Future;

    if-eqz p2, :cond_d

    .line 1176
    invoke-interface {p2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result p3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    if-nez p3, :cond_d

    .line 1178
    :try_start_3
    invoke-interface {p2}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_5

    .line 1189
    :cond_e
    new-instance p1, Ljava/util/ArrayList;

    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1190
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_6
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_11

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/ss/android/socialbase/downloader/h/b;

    .line 1191
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_f

    .line 1192
    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/h/b;->b()V

    goto :goto_6

    .line 1194
    :cond_f
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_10

    .line 1195
    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/h/b;->a()V

    goto :goto_6

    .line 1199
    :cond_10
    invoke-static {p3}, Ljava/util/concurrent/Executors;->callable(Ljava/lang/Runnable;)Ljava/util/concurrent/Callable;

    move-result-object p3

    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1203
    :cond_11
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result p2

    if-eqz p2, :cond_12

    return-void

    .line 1207
    :cond_12
    :try_start_4
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/impls/e;->c(Ljava/util/List;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_2
    :cond_13
    return-void

    :catch_3
    move-exception p1

    .line 1209
    new-instance p2, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 p3, 0x3fc

    invoke-direct {p2, p3, p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw p2
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x1

    const/16 v1, 0x19c

    if-ne p1, v1, :cond_0

    return v0

    .line 1407
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    iget-boolean p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->k:Z

    if-nez p2, :cond_1

    iget-boolean p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->j:Z

    if-eqz p2, :cond_2

    :cond_1
    return v0

    :cond_2
    const/16 p2, 0xc9

    if-eq p1, p2, :cond_3

    const/16 p2, 0x1a0

    if-ne p1, p2, :cond_4

    .line 1410
    :cond_3
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide p1

    const-wide/16 v1, 0x0

    cmp-long p3, p1, v1

    if-lez p3, :cond_4

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private b(Ljava/lang/String;Ljava/util/List;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;J)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Lcom/ss/android/socialbase/downloader/exception/h;
        }
    .end annotation

    .line 769
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result p3

    const/4 p4, 0x1

    if-ne p3, p4, :cond_0

    .line 771
    invoke-static {}, Lcom/ss/android/socialbase/downloader/network/a/a;->a()Lcom/ss/android/socialbase/downloader/network/a/a;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/ss/android/socialbase/downloader/network/a/a;->a(Ljava/lang/String;Ljava/util/List;)Lcom/ss/android/socialbase/downloader/network/a/c;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 773
    iput-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->y:Lcom/ss/android/socialbase/downloader/network/g;

    .line 774
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p3, p4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setPreconnectLevel(I)V

    .line 777
    :cond_0
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->y:Lcom/ss/android/socialbase/downloader/network/g;

    if-nez p3, :cond_2

    iget-boolean p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->D:Z

    if-nez p3, :cond_2

    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isHeadConnectionAvailable()Z

    move-result p3

    if-eqz p3, :cond_2

    .line 779
    :try_start_0
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->I:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v0, "net_lib_strategy"

    .line 780
    invoke-virtual {p3, v0}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;)I

    move-result p3

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->I:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v1, "monitor_download_connect"

    const/4 v2, 0x0

    .line 781
    invoke-virtual {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p4, 0x0

    :goto_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 779
    invoke-static {p1, p2, p3, p4, v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/g;

    move-result-object p1

    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->y:Lcom/ss/android/socialbase/downloader/network/g;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 784
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/e;->i(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setHeadConnectionException(Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private d(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z
    .locals 7

    .line 1852
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    if-eqz v0, :cond_6

    .line 1853
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v2, 0x0

    if-lez v0, :cond_0

    if-eqz p1, :cond_3

    .line 1855
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v0

    const/16 v3, 0x42e

    if-eq v0, v3, :cond_0

    goto :goto_1

    .line 1857
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->trySwitchToNextBackupUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1858
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getBackUpUrlRetryCount()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 1859
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->updateCurRetryTime(I)V

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_5

    .line 1860
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v0

    const/16 v3, 0x3f3

    if-eq v0, v3, :cond_2

    .line 1861
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v0, :cond_5

    :cond_2
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->canReplaceHttpForRetry()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1862
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRetryCount()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 1863
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->updateCurRetryTime(I)V

    .line 1864
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setHttpsToHttpRetryUsed(Z)V

    :goto_0
    const/4 v1, 0x0

    .line 1874
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->f:Lcom/ss/android/socialbase/downloader/constants/i;

    if-eq p1, v0, :cond_4

    if-eqz v1, :cond_4

    .line 1875
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->updateCurRetryTime(I)V

    :cond_4
    return v2

    .line 1866
    :cond_5
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v3

    const-string v4, "retry for exception, but current retry time : %s , retry Time %s all used, last error is %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRetryCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v2, 0x2

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v3, p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    return v1

    .line 1870
    :cond_6
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v2, 0x413

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "retry for exception, but retain retry time is null, last error is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v2, p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    return v1
.end method

.method private h()V
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-nez v0, :cond_0

    return-void

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRetryCount()I

    move-result v0

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurRetryTime()I

    move-result v1

    sub-int/2addr v0, v1

    if-gez v0, :cond_1

    const/4 v0, 0x0

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v1, :cond_2

    .line 174
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    goto :goto_0

    .line 176
    :cond_2
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    :goto_0
    return-void
.end method

.method private i()Z
    .locals 5

    .line 266
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 267
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->canSkipStatusHandler()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    const/4 v1, -0x4

    if-eq v0, v1, :cond_0

    .line 269
    new-instance v1, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v2, 0x3e8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The download Task can\'t start, because its status is not prepare:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    return v1
.end method

.method private j()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/a;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 280
    :try_start_0
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v2

    .line 281
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    if-eqz v3, :cond_7

    const/16 v3, 0x800

    .line 282
    invoke-static {v3}, Lcom/ss/android/socialbase/downloader/i/a;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 283
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v3}, Lcom/ss/android/socialbase/downloader/downloader/i;->d()Z

    .line 285
    :cond_0
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v3, v2}, Lcom/ss/android/socialbase/downloader/downloader/i;->b(I)Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 286
    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isNewTask()Z

    move-result v4

    if-eqz v4, :cond_1

    goto/16 :goto_2

    .line 290
    :cond_1
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getSavePath()Ljava/lang/String;

    move-result-object v4

    .line 291
    iget-object v5, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getMd5()Ljava/lang/String;

    move-result-object v5

    .line 292
    iget-object v6, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v6, v3, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->copyFromCacheData(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Z)V

    const/16 v6, 0x1000

    .line 293
    invoke-static {v6}, Lcom/ss/android/socialbase/downloader/i/a;->a(I)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    if-eq v3, v6, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    .line 296
    :goto_0
    :try_start_1
    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getSavePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v3, v0, v5}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;ZLjava/lang/String;)Z

    move-result v4
    :try_end_1
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v4, :cond_3

    goto :goto_1

    .line 298
    :cond_3
    :try_start_2
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/a;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/ss/android/socialbase/downloader/exception/a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    move-object v1, v0

    move v0, v6

    const/4 v2, 0x1

    goto/16 :goto_c

    :catch_0
    move-exception v0

    move-object v1, v0

    move v0, v6

    const/4 v2, 0x1

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v1, v0

    move v0, v6

    const/4 v2, 0x1

    goto/16 :goto_b

    .line 300
    :cond_4
    :goto_1
    :try_start_3
    invoke-static {v3}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)I

    move-result v3
    :try_end_3
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eq v3, v2, :cond_5

    .line 303
    :try_start_4
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    invoke-interface {v3, v2}, Lcom/ss/android/socialbase/downloader/downloader/i;->f(I)Z
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    :catch_2
    move-exception v2

    .line 305
    :try_start_5
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_5
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3

    :cond_5
    move v1, v6

    goto :goto_3

    :catchall_1
    move-exception v1

    move v0, v6

    goto :goto_4

    :catch_3
    move-exception v1

    move v0, v6

    goto :goto_5

    :catch_4
    move-exception v1

    move v0, v6

    goto/16 :goto_a

    .line 287
    :cond_6
    :goto_2
    :try_start_6
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->reset()V
    :try_end_6
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    .line 311
    :goto_3
    :try_start_7
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->h()V
    :try_end_7
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v1, :cond_a

    .line 322
    :try_start_8
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_8

    goto :goto_9

    :catchall_2
    move-exception v2

    move v0, v1

    move-object v1, v2

    goto :goto_4

    :catch_5
    move-exception v2

    move v0, v1

    move-object v1, v2

    goto :goto_5

    :catch_6
    move-exception v2

    move v0, v1

    move-object v1, v2

    goto :goto_a

    :catchall_3
    move-exception v1

    :goto_4
    const/4 v2, 0x0

    goto :goto_c

    :catch_7
    move-exception v1

    :goto_5
    const/4 v2, 0x0

    .line 315
    :goto_6
    :try_start_9
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-eqz v3, :cond_8

    .line 316
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getMonitorDepend()Lcom/ss/android/socialbase/downloader/depend/z;

    move-result-object v3

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    new-instance v5, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v6, 0x3eb

    const-string v7, "checkTaskCache"

    invoke-static {v1, v7}, Lcom/ss/android/socialbase/downloader/i/e;->b(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v6, v1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v1

    invoke-static {v3, v4, v5, v1}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/depend/z;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/exception/BaseException;I)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :cond_8
    if-eqz v0, :cond_a

    if-eqz v2, :cond_9

    .line 321
    :try_start_a
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->I:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v1, "fix_file_exist_update_download_info"

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    goto :goto_7

    :catch_8
    move-exception v0

    goto :goto_8

    .line 322
    :cond_9
    :goto_7
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_8

    goto :goto_9

    .line 325
    :goto_8
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    :cond_a
    :goto_9
    return-void

    :catch_9
    move-exception v1

    :goto_a
    const/4 v2, 0x0

    .line 313
    :goto_b
    :try_start_b
    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :catchall_4
    move-exception v1

    :goto_c
    if-eqz v0, :cond_c

    if-eqz v2, :cond_b

    .line 321
    :try_start_c
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->I:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v2, "fix_file_exist_update_download_info"

    invoke-virtual {v0, v2}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    goto :goto_d

    :catch_a
    move-exception v0

    goto :goto_e

    .line 322
    :cond_b
    :goto_d
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {v0, v2}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z
    :try_end_c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_c} :catch_a

    goto :goto_f

    .line 325
    :goto_e
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 326
    :cond_c
    :goto_f
    throw v1
.end method

.method private k()V
    .locals 8

    const/16 v0, 0xa

    .line 347
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    const-wide/16 v0, 0x0

    .line 350
    :try_start_0
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->H:J

    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    .line 351
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/ss/android/socialbase/downloader/h/c;->H:J

    const/4 v7, 0x0

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->increaseDownloadPrepareTime(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :catch_0
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getInterceptor()Lcom/ss/android/socialbase/downloader/depend/y;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 360
    invoke-interface {v2}, Lcom/ss/android/socialbase/downloader/depend/y;->a()Z

    move-result v2
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_1

    .line 368
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/e;->e()V

    return-void

    :catchall_0
    move-exception v0

    goto/16 :goto_3

    :catch_1
    move-exception v2

    .line 365
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 372
    :cond_1
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->i()Z

    move-result v2

    if-nez v2, :cond_3

    .line 373
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getMonitorDepend()Lcom/ss/android/socialbase/downloader/depend/z;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    new-instance v2, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v3, 0x3eb

    const-string v4, "task status is invalid"

    invoke-direct {v2, v3, v4}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v3

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    :goto_0
    invoke-static {v0, v1, v2, v3}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/depend/z;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/exception/BaseException;I)V

    return-void

    .line 379
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->l()V

    .line 380
    iget-boolean v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->C:Z

    if-nez v2, :cond_4

    goto/16 :goto_2

    .line 382
    :cond_4
    iget v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->B:I

    if-lez v2, :cond_5

    .line 383
    iget v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->B:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->B:I

    goto :goto_1

    .line 385
    :cond_5
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v2

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-eqz v6, :cond_6

    .line 386
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getErrorBytesLog()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    new-instance v1, Lcom/ss/android/socialbase/downloader/exception/f;

    const/16 v2, 0x403

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current bytes is not equals to total bytes, bytes invalid retry status is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getByteInvalidRetryStatus()Lcom/ss/android/socialbase/downloader/constants/b;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/ss/android/socialbase/downloader/exception/f;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto :goto_2

    .line 389
    :cond_6
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v2

    cmp-long v4, v2, v0

    if-gtz v4, :cond_7

    .line 390
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getErrorBytesLog()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    new-instance v1, Lcom/ss/android/socialbase/downloader/exception/f;

    const/16 v2, 0x402

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "curBytes is 0, bytes invalid retry status is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getByteInvalidRetryStatus()Lcom/ss/android/socialbase/downloader/constants/b;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/ss/android/socialbase/downloader/exception/f;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto :goto_2

    .line 393
    :cond_7
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v2

    cmp-long v4, v2, v0

    if-gtz v4, :cond_3

    .line 394
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getErrorBytesLog()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    new-instance v1, Lcom/ss/android/socialbase/downloader/exception/f;

    const/16 v2, 0x414

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TotalBytes is 0, bytes invalid retry status is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getByteInvalidRetryStatus()Lcom/ss/android/socialbase/downloader/constants/b;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/ss/android/socialbase/downloader/exception/f;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    :goto_2
    return-void

    .line 368
    :goto_3
    throw v0
.end method

.method private l()V
    .locals 17

    move-object/from16 v1, p0

    .line 408
    :try_start_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->a:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    .line 409
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->updateStartDownloadTime()V

    .line 410
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->resetRealStartDownloadTime()V

    .line 412
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 415
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v4, v5}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setFirstSpeedTime(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 419
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->j()V
    :try_end_1
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v6, v0

    .line 421
    :try_start_2
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file exist "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/exception/a;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/exception/a;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->E:Ljava/lang/String;

    const/4 v0, 0x1

    .line 427
    :goto_0
    iget-boolean v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->C:Z

    if-nez v6, :cond_0

    .line 428
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/downloader/e;->b()V

    .line 430
    :cond_0
    iput-boolean v5, v1, Lcom/ss/android/socialbase/downloader/h/c;->C:Z

    .line 432
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    if-eqz v6, :cond_1

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 435
    :cond_1
    :try_start_3
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->E:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    if-eqz v0, :cond_4

    .line 437
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isExpiredRedownload()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 438
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isDeleteCacheIfCheckFailed()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getLastModified()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 440
    iput-boolean v5, v1, Lcom/ss/android/socialbase/downloader/h/c;->F:Z

    .line 441
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v6, "dcache::last modify is emtpy, so just return cache"

    invoke-static {v0, v6}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 443
    :cond_2
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dcache::curt="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v7, " expired="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v7}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCacheExpiredTime()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCacheExpiredTime()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    .line 446
    iput-boolean v4, v1, Lcom/ss/android/socialbase/downloader/h/c;->F:Z

    .line 447
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v6, "dcache::expired=true"

    invoke-static {v0, v6}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :cond_3
    :goto_1
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->F:Z

    if-nez v0, :cond_4

    .line 453
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->m()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 459
    :cond_4
    :goto_2
    :try_start_4
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    if-eqz v0, :cond_5

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 464
    :cond_5
    :try_start_5
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->F()V

    .line 466
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->B()V

    .line 468
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->E()V

    .line 470
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v8, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v8

    invoke-interface {v0, v8}, Lcom/ss/android/socialbase/downloader/downloader/i;->c(I)Ljava/util/List;

    move-result-object v0

    .line 472
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->G()V

    .line 475
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->n()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 476
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v8, "downloadSegments return"

    invoke-static {v0, v8}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_5 .. :try_end_5} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 612
    :try_start_6
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 480
    :cond_6
    :try_start_7
    iget-object v8, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getConnectionUrl()Ljava/lang/String;

    move-result-object v8

    .line 482
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v9
    :try_end_7
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_7 .. :try_end_7} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v9, :cond_7

    .line 612
    :try_start_8
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 486
    :cond_7
    :try_start_9
    iget-boolean v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->i:Z

    if-eqz v9, :cond_8

    .line 487
    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v9}, Lcom/ss/android/socialbase/downloader/i/e;->d(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)J

    move-result-wide v9

    goto :goto_3

    :cond_8
    const-wide/16 v9, 0x0

    .line 491
    :goto_3
    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v11, v9, v10}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;J)Lcom/ss/android/socialbase/downloader/model/b;

    move-result-object v11

    .line 494
    invoke-direct {v1, v11}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/model/b;)Ljava/util/List;

    move-result-object v12

    .line 496
    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v12, v13}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/util/List;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    .line 497
    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v13, v5}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setPreconnectLevel(I)V

    .line 499
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13
    :try_end_9
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_9 .. :try_end_9} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 503
    :try_start_a
    invoke-direct {v1, v8, v12, v9, v10}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/lang/String;Ljava/util/List;J)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 505
    :try_start_b
    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    const/4 v10, 0x0

    sub-long v6, v15, v13

    invoke-virtual {v9, v6, v7}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->increaseAllConnectTime(J)V

    .line 508
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v6
    :try_end_b
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_b .. :try_end_b} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_b .. :try_end_b} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    if-eqz v6, :cond_9

    .line 612
    :try_start_c
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 511
    :cond_9
    :try_start_d
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v6

    .line 512
    invoke-virtual {v1, v6, v7}, Lcom/ss/android/socialbase/downloader/h/c;->a(J)V

    .line 514
    invoke-direct {v1, v6, v7, v0}, Lcom/ss/android/socialbase/downloader/h/c;->a(JLjava/util/List;)I

    move-result v9

    .line 516
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v10
    :try_end_d
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_d .. :try_end_d} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_d .. :try_end_d} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    if-eqz v10, :cond_a

    .line 612
    :try_start_e
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    :cond_a
    if-lez v9, :cond_13

    if-ne v9, v4, :cond_b

    const/4 v10, 0x1

    goto :goto_4

    :cond_b
    const/4 v10, 0x0

    .line 523
    :goto_4
    :try_start_f
    iput-boolean v10, v1, Lcom/ss/android/socialbase/downloader/h/c;->h:Z

    .line 525
    iget-boolean v10, v1, Lcom/ss/android/socialbase/downloader/h/c;->h:Z

    if-eqz v10, :cond_e

    .line 527
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;
    :try_end_f
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_f .. :try_end_f} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_f .. :try_end_f} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    if-nez v0, :cond_c

    .line 529
    :try_start_10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 530
    :try_start_11
    invoke-direct {v1, v8, v12}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/lang/String;Ljava/util/List;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 532
    :try_start_12
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const/4 v12, 0x0

    sub-long/2addr v9, v6

    invoke-virtual {v0, v9, v10}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->increaseAllConnectTime(J)V

    goto :goto_6

    :catchall_0
    move-exception v0

    goto :goto_5

    :catchall_1
    move-exception v0

    move-wide v6, v13

    :goto_5
    iget-object v8, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const/4 v11, 0x0

    sub-long/2addr v9, v6

    invoke-virtual {v8, v9, v10}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->increaseAllConnectTime(J)V

    throw v0

    .line 536
    :cond_c
    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v0
    :try_end_12
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_12 .. :try_end_12} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_12 .. :try_end_12} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_12 .. :try_end_12} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_1
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    if-eqz v0, :cond_d

    .line 612
    :try_start_13
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 540
    :cond_d
    :try_start_14
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const/4 v9, 0x0

    sub-long/2addr v6, v2

    invoke-virtual {v0, v6, v7}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setFirstSpeedTime(J)V

    .line 542
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->o()V

    .line 544
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-direct {v1, v11, v8, v0}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/model/b;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/i;)V

    goto :goto_7

    .line 547
    :cond_e
    iget-object v8, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isNeedReuseFirstConnection()Z

    move-result v8

    if-nez v8, :cond_f

    .line 548
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->r()V

    .line 550
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v8
    :try_end_14
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_14 .. :try_end_14} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_14 .. :try_end_14} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_14 .. :try_end_14} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_1
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    if-eqz v8, :cond_10

    .line 612
    :try_start_15
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 553
    :cond_10
    :try_start_16
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->o()V

    .line 556
    iget-object v8, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const/4 v12, 0x0

    sub-long/2addr v10, v2

    invoke-virtual {v8, v10, v11}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setFirstSpeedTime(J)V

    .line 557
    iget-boolean v8, v1, Lcom/ss/android/socialbase/downloader/h/c;->i:Z

    if-eqz v8, :cond_11

    .line 558
    invoke-direct {v1, v9, v0}, Lcom/ss/android/socialbase/downloader/h/c;->a(ILjava/util/List;)V

    goto :goto_7

    .line 560
    :cond_11
    invoke-direct {v1, v6, v7, v9}, Lcom/ss/android/socialbase/downloader/h/c;->a(JI)V
    :try_end_16
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_16 .. :try_end_16} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_16 .. :try_end_16} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_16 .. :try_end_16} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_1
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    .line 612
    :cond_12
    :goto_7
    :try_start_17
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_4

    goto/16 :goto_a

    .line 520
    :cond_13
    :try_start_18
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v6, 0x408

    const-string v7, "chunkCount is 0"

    invoke-direct {v0, v6, v7}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    :catchall_2
    move-exception v0

    move-object v6, v0

    .line 505
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    const/4 v9, 0x0

    sub-long/2addr v7, v13

    invoke-virtual {v0, v7, v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->increaseAllConnectTime(J)V

    throw v6
    :try_end_18
    .catch Lcom/ss/android/socialbase/downloader/exception/a; {:try_start_18 .. :try_end_18} :catch_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_18 .. :try_end_18} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_18 .. :try_end_18} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_1
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    :catchall_3
    move-exception v0

    goto/16 :goto_b

    :catch_1
    move-exception v0

    .line 607
    :try_start_19
    sget-object v2, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadInner: throwable =  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v3, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    if-eq v2, v3, :cond_12

    .line 609
    new-instance v2, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v3, 0x415

    invoke-direct {v2, v3, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    invoke-virtual {v1, v2}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto :goto_7

    :catch_2
    move-exception v0

    .line 587
    sget-object v6, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "downloadInner: retry throwable for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/exception/h;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v7, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    if-eq v6, v7, :cond_12

    .line 589
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v7, 0x5

    if-eqz v6, :cond_15

    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    if-lez v6, :cond_15

    .line 590
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->updateCurRetryTime(I)V

    .line 591
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0, v7}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setStatus(I)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_3

    .line 612
    :cond_14
    :goto_8
    :try_start_1a
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_4

    goto/16 :goto_2

    .line 593
    :cond_15
    :try_start_1b
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v6, :cond_17

    .line 594
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->trySwitchToNextBackupUrl()Z

    move-result v6

    if-eqz v6, :cond_16

    .line 595
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0, v7}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setStatus(I)V

    .line 596
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRetryCount()I

    move-result v6

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 597
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->updateCurRetryTime(I)V

    goto :goto_8

    .line 600
    :cond_16
    new-instance v2, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v3, 0x3fa

    const-string v6, "retry for Throwable, but retry Time %s all used, last error is %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRetryCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/exception/h;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v4

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto/16 :goto_7

    .line 603
    :cond_17
    new-instance v2, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v3, 0x413

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "retry for Throwable, but retain retry time is NULL, last error is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/exception/h;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto/16 :goto_7

    :catch_3
    move-exception v0

    .line 567
    sget-object v6, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "downloadInner: baseException = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v7, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    if-eq v6, v7, :cond_12

    .line 569
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v6

    const/16 v7, 0x401

    if-eq v6, v7, :cond_1b

    .line 570
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v6

    const/16 v7, 0x3f1

    if-ne v6, v7, :cond_18

    goto :goto_9

    .line 573
    :cond_18
    invoke-virtual {v1, v0}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 574
    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 575
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->H()V

    :cond_19
    const-wide/16 v6, 0x0

    .line 577
    invoke-virtual {v1, v0, v6, v7}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;J)Lcom/ss/android/socialbase/downloader/exception/g;

    move-result-object v0

    .line 578
    sget-object v6, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_3

    if-ne v0, v6, :cond_14

    .line 612
    :try_start_1c
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_4

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 583
    :cond_1a
    :try_start_1d
    invoke-virtual {v1, v0}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto/16 :goto_7

    .line 571
    :cond_1b
    :goto_9
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->e:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_3

    .line 612
    :try_start_1e
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_4

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 565
    :catch_4
    :try_start_1f
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->m()V
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_3

    goto/16 :goto_7

    .line 618
    :goto_a
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    return-void

    .line 612
    :goto_b
    :try_start_20
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V

    throw v0
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_4

    :catchall_4
    move-exception v0

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    throw v0
.end method

.method private m()V
    .locals 3

    .line 623
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v1, "finishWithFileExist"

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    invoke-static {}, Lcom/ss/android/socialbase/downloader/g/a;->c()Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v0

    const-string v1, "fix_end_for_file_exist_error"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 627
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->E:Ljava/lang/String;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->e:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    goto :goto_0

    .line 632
    :cond_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->h:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    goto :goto_0

    .line 635
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->E:Ljava/lang/String;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTargetFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 636
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->e:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    goto :goto_0

    .line 638
    :cond_2
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->h:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    :goto_0
    return-void
.end method

.method private n()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 644
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isExpiredRedownload()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_8

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getThrottleNetSpeed()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    goto :goto_3

    .line 654
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v0

    const-string v3, "segment_config"

    invoke-virtual {v0, v3}, Lcom/ss/android/socialbase/downloader/g/a;->d(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 656
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/ss/android/socialbase/downloader/downloader/i;->n(I)Ljava/util/List;

    move-result-object v3

    .line 657
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v7

    cmp-long v4, v7, v5

    if-lez v4, :cond_4

    if-eqz v3, :cond_3

    .line 660
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_2
    if-nez v0, :cond_4

    .line 668
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    goto :goto_1

    :cond_3
    :goto_0
    return v1

    :cond_4
    :goto_1
    if-nez v0, :cond_5

    return v1

    .line 678
    :cond_5
    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/f/n;->a(Lorg/json/JSONObject;)Lcom/ss/android/socialbase/downloader/f/n;

    move-result-object v0

    .line 679
    new-instance v1, Lcom/ss/android/socialbase/downloader/f/k;

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-direct {v1, v4, v0, p0}, Lcom/ss/android/socialbase/downloader/f/k;-><init>(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/f/n;Lcom/ss/android/socialbase/downloader/h/f;)V

    iput-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    .line 681
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 682
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v1, "downloadSegments: is stopped by user"

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_6

    .line 684
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/k;->a()V

    goto :goto_2

    .line 686
    :cond_6
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/k;->b()V

    :goto_2
    return v2

    .line 691
    :cond_7
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    invoke-virtual {v0, v3}, Lcom/ss/android/socialbase/downloader/f/k;->a(Ljava/util/List;)V

    return v2

    :cond_8
    :goto_3
    return v1
.end method

.method private o()V
    .locals 3

    .line 697
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v0

    const-string v1, "reset_retain_retry_times"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->J:I

    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    .line 699
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isBackUpUrlUsed()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getBackUpUrlRetryCount()I

    move-result v2

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRetryCount()I

    move-result v2

    :goto_0
    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 700
    iget v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->J:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->J:I

    :cond_1
    return-void
.end method

.method private p()V
    .locals 7

    .line 726
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "endDownloadRunnable::runStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 732
    :goto_0
    :try_start_0
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->w()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x0

    goto :goto_2

    :catch_0
    move-exception v1

    .line 734
    instance-of v4, v1, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    if-eqz v4, :cond_1

    .line 735
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    check-cast v1, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    invoke-virtual {v4, v1}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto :goto_1

    .line 737
    :cond_1
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    new-instance v5, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v6, 0x416

    invoke-direct {v5, v6, v1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    invoke-virtual {v4, v5}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    :goto_1
    const/4 v1, 0x1

    const/4 v4, 0x1

    :goto_2
    if-nez v1, :cond_3

    if-eqz v4, :cond_2

    goto :goto_3

    .line 763
    :cond_2
    iput-boolean v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->C:Z

    .line 764
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v1, "jump to restart"

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 743
    :cond_3
    :goto_3
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-eqz v0, :cond_5

    .line 746
    :try_start_1
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->C()Lcom/ss/android/socialbase/downloader/impls/a;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 755
    invoke-virtual {v0, p0}, Lcom/ss/android/socialbase/downloader/impls/a;->a(Lcom/ss/android/socialbase/downloader/h/c;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    .line 758
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 759
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadTask;->getMonitorDepend()Lcom/ss/android/socialbase/downloader/depend/z;

    move-result-object v1

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    new-instance v4, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v5, 0x3f6

    const-string v6, "removeDownloadRunnable"

    invoke-static {v0, v6}, Lcom/ss/android/socialbase/downloader/i/e;->b(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v2

    :cond_4
    invoke-static {v1, v3, v4, v2}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/depend/z;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/exception/BaseException;I)V

    :cond_5
    :goto_4
    return-void
.end method

.method private q()V
    .locals 1

    .line 829
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->y:Lcom/ss/android/socialbase/downloader/network/g;

    if-eqz v0, :cond_0

    .line 830
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->y:Lcom/ss/android/socialbase/downloader/network/g;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/g;->c()V

    const/4 v0, 0x0

    .line 831
    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->y:Lcom/ss/android/socialbase/downloader/network/g;

    :cond_0
    return-void
.end method

.method private r()V
    .locals 1

    .line 836
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->d()V

    const/4 v0, 0x0

    .line 838
    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    :cond_0
    return-void
.end method

.method private s()V
    .locals 0

    .line 843
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->q()V

    .line 844
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->r()V

    return-void
.end method

.method private t()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 869
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    if-eqz v0, :cond_2

    .line 870
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_0

    .line 871
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const/4 v1, -0x4

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setStatus(I)V

    .line 872
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->c()V

    goto :goto_0

    .line 873
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_1

    .line 874
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setStatus(I)V

    .line 875
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->b()V

    goto :goto_0

    .line 877
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->d()V

    :cond_2
    :goto_0
    return-void
.end method

.method private u()Z
    .locals 2

    .line 883
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private v()Z
    .locals 2

    .line 887
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->u()Z

    move-result v0

    const/4 v1, -0x2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    .line 888
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->u()Z

    move-result v0

    if-nez v0, :cond_3

    .line 889
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 890
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    goto :goto_1

    .line 891
    :cond_2
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v0

    const/4 v1, -0x4

    if-ne v0, v1, :cond_3

    .line 892
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    :cond_3
    :goto_1
    const/4 v0, 0x1

    return v0
.end method

.method private w()Z
    .locals 5

    .line 902
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->d:Lcom/ss/android/socialbase/downloader/constants/i;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    .line 903
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->w:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto/16 :goto_1

    .line 904
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_1

    .line 905
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/e;->c()V

    goto/16 :goto_1

    .line 906
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_2

    .line 907
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/e;->d()V

    goto/16 :goto_1

    .line 908
    :cond_2
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->e:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_3

    .line 910
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/e;->g()V
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    .line 912
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto/16 :goto_1

    .line 914
    :cond_3
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->h:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_4

    .line 916
    :try_start_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 918
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    goto :goto_1

    .line 920
    :cond_4
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->i:Lcom/ss/android/socialbase/downloader/constants/i;

    const/4 v3, 0x0

    if-ne v0, v1, :cond_5

    .line 921
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->w:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    invoke-virtual {v0, v1, v3}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;Z)V

    return v3

    .line 923
    :cond_5
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->g:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_6

    return v2

    .line 925
    :cond_6
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->f:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_8

    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->x()Z

    move-result v0

    if-nez v0, :cond_8

    .line 926
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v1, "doTaskStatusHandle retryDelay"

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->z()V

    .line 928
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/i;->f:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v0, v1, :cond_7

    goto :goto_0

    :cond_7
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 931
    :cond_8
    :try_start_2
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->y()Z

    move-result v0

    if-nez v0, :cond_9

    return v3

    .line 933
    :cond_9
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/e;->f()V

    .line 934
    invoke-static {}, Lcom/ss/android/socialbase/downloader/impls/r;->a()Lcom/ss/android/socialbase/downloader/impls/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/impls/r;->d()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    .line 936
    new-instance v1, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v3, 0x3f0

    const-string v4, "doTaskStatusHandle onComplete"

    invoke-static {v0, v4}, Lcom/ss/android/socialbase/downloader/i/e;->b(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    :goto_1
    return v2
.end method

.method private x()Z
    .locals 7

    .line 944
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_1

    .line 945
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v3

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-nez v0, :cond_0

    return v2

    :cond_0
    return v1

    .line 950
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v3

    invoke-interface {v0, v3}, Lcom/ss/android/socialbase/downloader/downloader/i;->c(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 951
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v2, :cond_2

    goto :goto_0

    .line 953
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ss/android/socialbase/downloader/model/b;

    if-eqz v3, :cond_4

    .line 954
    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/b;->i()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_4
    return v1

    :cond_5
    return v2

    :cond_6
    :goto_0
    return v1
.end method

.method private y()Z
    .locals 5

    .line 961
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isChunked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setTotalBytes(J)V

    .line 964
    :cond_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkCompletedByteValid: downloadInfo.getCurBytes() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ",  downloadInfo.getTotalBytes() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_2

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isIgnoreDataVerify()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-lez v4, :cond_2

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v0

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    return v0

    .line 966
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/b;->b:Lcom/ss/android/socialbase/downloader/constants/b;

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setByteInvalidRetryStatus(Lcom/ss/android/socialbase/downloader/constants/b;)V

    .line 967
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->reset()V

    .line 968
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z

    .line 969
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->d(I)V

    .line 970
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->m(I)V

    .line 971
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    const/4 v0, 0x0

    return v0
.end method

.method private z()V
    .locals 1

    .line 978
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->a:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    return-void
.end method


# virtual methods
.method public a(Lcom/ss/android/socialbase/downloader/exception/BaseException;J)Lcom/ss/android/socialbase/downloader/exception/g;
    .locals 7

    .line 1744
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->w:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 1745
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    neg-long p2, p2

    invoke-virtual {v0, p2, p3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->increaseCurBytes(J)V

    .line 1746
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {p2, p3}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z

    .line 1747
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->u()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1748
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    return-object p1

    :cond_0
    const/4 p2, 0x1

    const/4 p3, 0x0

    if-eqz p1, :cond_3

    .line 1751
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v0

    const/16 v1, 0x417

    if-ne v0, v1, :cond_3

    .line 1752
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->z:Lcom/ss/android/socialbase/downloader/depend/x;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isForbiddenRetryed()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 1756
    :cond_1
    new-instance v0, Lcom/ss/android/socialbase/downloader/h/c$1;

    invoke-direct {v0, p0}, Lcom/ss/android/socialbase/downloader/h/c$1;-><init>(Lcom/ss/android/socialbase/downloader/h/c;)V

    .line 1763
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->z:Lcom/ss/android/socialbase/downloader/depend/x;

    invoke-interface {v1, v0}, Lcom/ss/android/socialbase/downloader/depend/x;->a(Lcom/ss/android/socialbase/downloader/depend/w;)Z

    move-result v1

    .line 1764
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setForbiddenRetryed()V

    if-eqz v1, :cond_d

    .line 1766
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/depend/b;->a()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1767
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->I()V

    .line 1768
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/downloader/e;->h()V

    .line 1769
    sget-object p1, Lcom/ss/android/socialbase/downloader/constants/i;->g:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    .line 1770
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    return-object p1

    .line 1753
    :cond_2
    :goto_0
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->d(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1754
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    return-object p1

    .line 1776
    :cond_3
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/e;->g(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1778
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->A:Lcom/ss/android/socialbase/downloader/depend/r;

    if-nez v0, :cond_4

    .line 1779
    invoke-virtual {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    .line 1780
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    return-object p1

    .line 1782
    :cond_4
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, p3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 1783
    new-instance v6, Lcom/ss/android/socialbase/downloader/h/c$2;

    invoke-direct {v6, p0, v0}, Lcom/ss/android/socialbase/downloader/h/c$2;-><init>(Lcom/ss/android/socialbase/downloader/h/c;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 1794
    instance-of v1, p1, Lcom/ss/android/socialbase/downloader/exception/d;

    if-eqz v1, :cond_5

    .line 1795
    move-object v1, p1

    check-cast v1, Lcom/ss/android/socialbase/downloader/exception/d;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/exception/d;->a()J

    move-result-wide v2

    .line 1796
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/exception/d;->b()J

    move-result-wide v4

    goto :goto_1

    :cond_5
    const-wide/16 v1, -0x1

    .line 1799
    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v3

    move-wide v4, v3

    move-wide v2, v1

    .line 1801
    :goto_1
    monitor-enter p0

    .line 1802
    :try_start_0
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->A:Lcom/ss/android/socialbase/downloader/depend/r;

    invoke-interface/range {v1 .. v6}, Lcom/ss/android/socialbase/downloader/depend/r;->a(JJLcom/ss/android/socialbase/downloader/depend/q;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1804
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-static {v1}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v1

    const-string v2, "not_delete_when_clean_space"

    invoke-virtual {v1, v2, p3}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1807
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->y()Z

    .line 1809
    :cond_6
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1810
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object p2, Lcom/ss/android/socialbase/downloader/constants/i;->g:Lcom/ss/android/socialbase/downloader/constants/i;

    if-eq p1, p2, :cond_7

    .line 1811
    sget-object p1, Lcom/ss/android/socialbase/downloader/constants/i;->g:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    .line 1812
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->I()V

    .line 1813
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/downloader/e;->h()V

    .line 1815
    :cond_7
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    monitor-exit p0

    return-object p1

    .line 1825
    :cond_8
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1827
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->d(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1828
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    return-object p1

    :cond_9
    const/4 v0, 0x1

    goto :goto_2

    .line 1819
    :cond_a
    :try_start_1
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object p3, Lcom/ss/android/socialbase/downloader/constants/i;->g:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne p2, p3, :cond_b

    .line 1820
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    monitor-exit p0

    return-object p1

    .line 1822
    :cond_b
    invoke-virtual {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V

    .line 1823
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    .line 1825
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    .line 1830
    :cond_c
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->d(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1831
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    return-object p1

    :cond_d
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_e

    .line 1835
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->K()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1837
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->I()V

    .line 1840
    :cond_e
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v2, Lcom/ss/android/socialbase/downloader/constants/i;->f:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne v1, v2, :cond_f

    goto :goto_3

    :cond_f
    const/4 p2, 0x0

    :goto_3
    invoke-virtual {v0, p1, p2}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;Z)V

    .line 1841
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object p2, Lcom/ss/android/socialbase/downloader/constants/i;->f:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne p1, p2, :cond_10

    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    goto :goto_4

    :cond_10
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->b:Lcom/ss/android/socialbase/downloader/exception/g;

    :goto_4
    return-object p1
.end method

.method public a(Lcom/ss/android/socialbase/downloader/model/b;Lcom/ss/android/socialbase/downloader/exception/BaseException;J)Lcom/ss/android/socialbase/downloader/exception/g;
    .locals 2

    .line 1699
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1700
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    return-object p1

    :cond_0
    if-eqz p2, :cond_2

    .line 1703
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v0

    const/16 v1, 0x417

    if-eq v0, v1, :cond_1

    invoke-static {p2}, Lcom/ss/android/socialbase/downloader/i/e;->g(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1705
    :cond_1
    invoke-virtual {p0, p2, p3, p4}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;J)Lcom/ss/android/socialbase/downloader/exception/g;

    move-result-object p1

    return-object p1

    .line 1709
    :cond_2
    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/h/c;->w:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 1711
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    neg-long p3, p3

    invoke-virtual {v0, p3, p4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->increaseCurBytes(J)V

    .line 1713
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object p4, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {p3, p4}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z

    .line 1715
    invoke-direct {p0, p2}, Lcom/ss/android/socialbase/downloader/h/c;->d(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 1717
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->a:Lcom/ss/android/socialbase/downloader/exception/g;

    return-object p1

    .line 1720
    :cond_3
    iget-object p3, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    iget-object p4, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->f:Lcom/ss/android/socialbase/downloader/constants/i;

    if-ne p4, v0, :cond_4

    const/4 p4, 0x1

    goto :goto_0

    :cond_4
    const/4 p4, 0x0

    :goto_0
    invoke-virtual {p3, p1, p2, p4}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(Lcom/ss/android/socialbase/downloader/model/b;Lcom/ss/android/socialbase/downloader/exception/BaseException;Z)V

    .line 1722
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    sget-object p2, Lcom/ss/android/socialbase/downloader/constants/i;->f:Lcom/ss/android/socialbase/downloader/constants/i;

    if-eq p1, p2, :cond_5

    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isNeedRetryDelay()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 1723
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->A()J

    move-result-wide p1

    const-wide/16 p3, 0x0

    cmp-long v0, p1, p3

    if-lez v0, :cond_5

    .line 1725
    sget-object p3, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onSingleChunkRetry with delay time "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/ss/android/socialbase/downloader/c/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1730
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 1732
    sget-object p2, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "onSingleChunkRetry:"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1736
    :cond_5
    :goto_1
    sget-object p1, Lcom/ss/android/socialbase/downloader/exception/g;->b:Lcom/ss/android/socialbase/downloader/exception/g;

    return-object p1
.end method

.method public declared-synchronized a(I)Lcom/ss/android/socialbase/downloader/model/b;
    .locals 4

    monitor-enter p0

    .line 1882
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    .line 1883
    monitor-exit p0

    return-object v2

    .line 1885
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->o:Lcom/ss/android/socialbase/downloader/downloader/i;

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/i;->c(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1886
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    .line 1889
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 1890
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ss/android/socialbase/downloader/model/b;

    if-nez v3, :cond_2

    goto :goto_1

    .line 1893
    :cond_2
    invoke-direct {p0, v3, p1}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/model/b;I)Lcom/ss/android/socialbase/downloader/model/b;

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_3

    .line 1895
    monitor-exit p0

    return-object v3

    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1898
    :cond_4
    monitor-exit p0

    return-object v2

    .line 1887
    :cond_5
    :goto_2
    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception p1

    .line 1881
    monitor-exit p0

    throw p1
.end method

.method public a()V
    .locals 2

    .line 181
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    .line 182
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/k;->b()V

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->b()V

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    if-nez v0, :cond_2

    .line 189
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V

    .line 190
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->b:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    .line 191
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    .line 194
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 195
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ss/android/socialbase/downloader/h/b;

    if-eqz v1, :cond_3

    .line 197
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/h/b;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 201
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_4
    return-void
.end method

.method public a(J)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-wide/from16 v2, p1

    .line 1236
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTempPath()Ljava/lang/String;

    move-result-object v0

    .line 1237
    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTempName()Ljava/lang/String;

    move-result-object v4

    .line 1239
    invoke-static/range {p1 .. p2}, Lcom/ss/android/socialbase/downloader/i/e;->b(J)Z

    move-result v5

    if-eqz v5, :cond_0

    return-void

    :cond_0
    const/4 v5, -0x1

    .line 1243
    invoke-static {v0, v4, v5}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/ss/android/socialbase/downloader/model/e;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 1245
    :try_start_0
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v8

    const/4 v4, 0x0

    sub-long v10, v2, v8

    .line 1247
    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->d(Ljava/lang/String;)J

    move-result-wide v12

    .line 1251
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v0

    const-string v4, "space_fill_part_download"

    .line 1252
    invoke-virtual {v0, v4, v6}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v4

    const-wide/16 v14, 0x0

    if-ne v4, v7, :cond_7

    .line 1254
    iput-wide v14, v1, Lcom/ss/android/socialbase/downloader/h/c;->G:J

    cmp-long v4, v10, v14

    if-gtz v4, :cond_1

    .line 1256
    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v10

    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v16

    const/4 v4, 0x0

    sub-long v10, v10, v16

    :cond_1
    cmp-long v4, v12, v10

    if-gez v4, :cond_a

    .line 1259
    sget-object v4, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkSpaceOverflow: contentLength = "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p2}, Lcom/ss/android/socialbase/downloader/i/e;->a(J)D

    move-result-wide v14

    invoke-virtual {v7, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v6, "MB, downloaded = "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1260
    invoke-static {v8, v9}, Lcom/ss/android/socialbase/downloader/i/e;->a(J)D

    move-result-wide v14

    invoke-virtual {v7, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v6, "MB, required = "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1261
    invoke-static {v10, v11}, Lcom/ss/android/socialbase/downloader/i/e;->a(J)D

    move-result-wide v14

    invoke-virtual {v7, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v6, "MB, available = "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1262
    invoke-static {v12, v13}, Lcom/ss/android/socialbase/downloader/i/e;->a(J)D

    move-result-wide v14

    invoke-virtual {v7, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v6, "MB"

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1259
    invoke-static {v4, v6}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v6, 0x0

    cmp-long v4, v12, v6

    if-lez v4, :cond_4

    const-string v4, "space_fill_min_keep_mb"

    const/16 v6, 0x64

    .line 1264
    invoke-virtual {v0, v4, v6}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_3

    int-to-long v6, v0

    const-wide/32 v14, 0x100000

    mul-long v6, v6, v14

    sub-long v6, v12, v6

    .line 1270
    sget-object v4, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "checkSpaceOverflow: minKeep = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "MB, canDownload = "

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1271
    invoke-static {v6, v7}, Lcom/ss/android/socialbase/downloader/i/e;->a(J)D

    move-result-wide v2

    invoke-virtual {v14, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, "MB"

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1270
    invoke-static {v4, v0}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    cmp-long v0, v6, v2

    if-lez v0, :cond_2

    .line 1275
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v2

    const/4 v0, 0x0

    add-long/2addr v2, v6

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/h/c;->G:J

    goto :goto_0

    .line 1273
    :cond_2
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/d;

    invoke-direct {v0, v12, v13, v10, v11}, Lcom/ss/android/socialbase/downloader/exception/d;-><init>(JJ)V

    throw v0

    :cond_3
    move-wide v6, v12

    :goto_0
    move-wide/from16 v2, p1

    cmp-long v0, v8, v2

    if-gez v0, :cond_a

    add-long/2addr v6, v8

    cmp-long v0, v6, v2

    if-lez v0, :cond_b

    goto :goto_3

    :cond_4
    const-string v4, "download_when_space_negative"

    const/4 v6, 0x0

    .line 1288
    invoke-virtual {v0, v4, v6}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_5

    move-wide v6, v2

    const/4 v4, 0x0

    goto :goto_4

    .line 1291
    :cond_5
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "availableSpace "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v3, 0x0

    cmp-long v6, v12, v3

    if-nez v6, :cond_6

    const-string v3, "="

    goto :goto_1

    :cond_6
    const-string v3, "<"

    :goto_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x41c

    invoke-direct {v0, v3, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_7
    move-wide v6, v14

    cmp-long v0, v12, v6

    if-gtz v0, :cond_9

    .line 1299
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "availableSpace "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v3, 0x0

    cmp-long v6, v12, v3

    if-nez v6, :cond_8

    const-string v3, "="

    goto :goto_2

    :cond_8
    const-string v3, "<"

    :goto_2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x41c

    invoke-direct {v0, v3, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_9
    cmp-long v0, v12, v10

    if-ltz v0, :cond_f

    :cond_a
    :goto_3
    move-wide v6, v2

    :cond_b
    const/4 v4, 0x1

    .line 1310
    :goto_4
    :try_start_1
    invoke-virtual {v5, v2, v3}, Lcom/ss/android/socialbase/downloader/model/e;->b(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_5
    const/4 v2, 0x1

    goto :goto_6

    :catch_0
    move-exception v0

    move-object v10, v0

    .line 1312
    :try_start_2
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "checkSpaceOverflow: setLength1 e = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v12, ", mustSetLength = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v0, v11}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    cmp-long v0, v6, v2

    const/16 v2, 0x410

    if-gez v0, :cond_d

    const-wide/16 v11, 0x0

    cmp-long v0, v6, v11

    if-lez v0, :cond_d

    cmp-long v0, v6, v8

    if-lez v0, :cond_d

    .line 1315
    :try_start_3
    invoke-virtual {v5, v6, v7}, Lcom/ss/android/socialbase/downloader/model/e;->b(J)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v3, v0

    .line 1317
    :try_start_4
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "checkSpaceOverflow: setLength2 ex = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v7, ", mustSetLength = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v4, :cond_c

    goto :goto_5

    .line 1319
    :cond_c
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    invoke-direct {v0, v2, v3}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_d
    if-nez v4, :cond_e

    goto :goto_5

    .line 1331
    :goto_6
    new-array v0, v2, [Ljava/io/Closeable;

    const/4 v2, 0x0

    aput-object v5, v0, v2

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    return-void

    .line 1324
    :cond_e
    :try_start_5
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    invoke-direct {v0, v2, v10}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw v0

    .line 1304
    :cond_f
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/d;

    invoke-direct {v0, v12, v13, v10, v11}, Lcom/ss/android/socialbase/downloader/exception/d;-><init>(JJ)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    const/4 v2, 0x1

    .line 1331
    new-array v2, v2, [Ljava/io/Closeable;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    throw v0
.end method

.method public a(Lcom/ss/android/socialbase/downloader/exception/BaseException;Z)V
    .locals 2

    .line 1645
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    const-string v1, "onAllChunkRetryWithReset"

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->i:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    .line 1647
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->w:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 1648
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->I()V

    if-eqz p2, :cond_0

    .line 1652
    invoke-direct {p0, p1}, Lcom/ss/android/socialbase/downloader/h/c;->d(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    .line 1656
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->H()V

    :cond_1
    return-void
.end method

.method public a(Lcom/ss/android/socialbase/downloader/h/b;)V
    .locals 1

    .line 1587
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->h:Z

    if-nez v0, :cond_0

    .line 1588
    monitor-enter p0

    .line 1589
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1590
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_0
    :goto_0
    return-void
.end method

.method public a(Lcom/ss/android/socialbase/downloader/network/g;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1906
    :try_start_0
    invoke-interface {p1}, Lcom/ss/android/socialbase/downloader/network/g;->b()I

    move-result p1

    .line 1907
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0, p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setHttpStatusCode(I)V

    .line 1908
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/b;->a(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setHttpStatusMessage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1911
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    .line 1915
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setHttpStatusCode(I)V

    .line 1916
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setHttpStatusMessage(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/g;J)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Lcom/ss/android/socialbase/downloader/exception/h;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p2

    if-nez v0, :cond_0

    return-void

    .line 1430
    :cond_0
    :try_start_0
    new-instance v4, Lcom/ss/android/socialbase/downloader/model/d;

    move-object/from16 v5, p1

    invoke-direct {v4, v5, v0}, Lcom/ss/android/socialbase/downloader/model/d;-><init>(Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/g;)V

    .line 1431
    iget v5, v4, Lcom/ss/android/socialbase/downloader/model/d;->c:I

    .line 1433
    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->d()Ljava/lang/String;

    move-result-object v6

    .line 1434
    iget-object v7, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v7}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getMimeType()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1435
    iget-object v7, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v7, v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setMimeType(Ljava/lang/String;)V

    .line 1438
    :cond_1
    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->b()Z

    move-result v6

    iput-boolean v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->j:Z

    .line 1439
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-boolean v7, v1, Lcom/ss/android/socialbase/downloader/h/c;->j:Z

    invoke-virtual {v6, v7}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setSupportPartial(Z)V

    .line 1441
    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->a()Z

    move-result v6

    iput-boolean v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->k:Z

    .line 1443
    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->geteTag()Ljava/lang/String;

    move-result-object v6

    .line 1444
    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->c()Ljava/lang/String;

    move-result-object v7

    .line 1446
    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->f()Ljava/lang/String;

    move-result-object v8

    .line 1447
    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->g()Ljava/lang/String;

    move-result-object v9

    .line 1448
    sget-object v10, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "dcache=last_modified="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, " CACHE_CONTROL="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, " max-age="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->k()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1449
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    const-wide/16 v11, 0x0

    if-nez v10, :cond_2

    .line 1451
    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v10, v9}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setCacheControl(Ljava/lang/String;)V

    .line 1452
    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->k()J

    move-result-wide v9

    cmp-long v13, v9, v11

    if-lez v13, :cond_2

    .line 1453
    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->k()J

    move-result-wide v15

    const-wide/16 v17, 0x3e8

    mul-long v15, v15, v17

    add-long/2addr v13, v15

    invoke-virtual {v9, v13, v14}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setCacheExpiredTime(J)V

    .line 1457
    :cond_2
    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v9}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isExpiredRedownload()Z

    move-result v9

    const/4 v10, 0x1

    if-eqz v9, :cond_6

    iget-boolean v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->F:Z

    if-eqz v9, :cond_6

    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->E:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    const/16 v9, 0x130

    if-eq v5, v9, :cond_4

    .line 1459
    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v9}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getLastModified()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    goto :goto_0

    :cond_3
    const/4 v9, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v9, 0x1

    :goto_1
    if-nez v9, :cond_5

    .line 1466
    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {v9}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    goto :goto_2

    .line 1462
    :cond_5
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dcache=responseCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " lastModified not changed, use local file  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1463
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/a;

    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/h/c;->E:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/ss/android/socialbase/downloader/exception/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1469
    :cond_6
    :goto_2
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 1471
    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v9, v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setLastModified(Ljava/lang/String;)V

    .line 1474
    :cond_7
    invoke-direct {v1, v5, v6, v7}, Lcom/ss/android/socialbase/downloader/h/c;->a(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1477
    instance-of v8, v0, Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v8, :cond_9

    .line 1478
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    const-string v7, ""

    :cond_8
    const-string v6, "eTag of server file changed"

    .line 1480
    invoke-direct {v1, v7, v6}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1482
    :cond_9
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/b;

    const/16 v2, 0x3ea

    const-string v3, ""

    invoke-direct {v0, v2, v5, v3}, Lcom/ss/android/socialbase/downloader/exception/b;-><init>(IILjava/lang/String;)V

    throw v0

    .line 1486
    :cond_a
    :goto_3
    iget-boolean v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->j:Z

    const/16 v8, 0x3ec

    if-nez v6, :cond_d

    iget-boolean v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->k:Z

    if-eqz v6, :cond_b

    goto :goto_4

    :cond_b
    const/16 v0, 0x193

    if-ne v5, v0, :cond_c

    .line 1534
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v2, 0x417

    const-string v3, "response code error : 403"

    invoke-direct {v0, v2, v3}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1536
    :cond_c
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "response code error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v8, v5, v2}, Lcom/ss/android/socialbase/downloader/exception/b;-><init>(IILjava/lang/String;)V

    throw v0

    .line 1488
    :cond_d
    :goto_4
    iget-boolean v5, v1, Lcom/ss/android/socialbase/downloader/h/c;->k:Z

    if-eqz v5, :cond_f

    cmp-long v5, p3, v11

    if-lez v5, :cond_f

    .line 1489
    instance-of v5, v0, Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v5, :cond_e

    const-string v5, ""

    const-string v6, "http head request not support"

    .line 1490
    invoke-direct {v1, v5, v6}, Lcom/ss/android/socialbase/downloader/h/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 1492
    :cond_e
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const-string v2, "isResponseFromBegin but firstOffset > 0"

    invoke-direct {v0, v8, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1495
    :cond_f
    :goto_5
    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->h()J

    move-result-wide v5

    const-string v9, ""

    .line 1497
    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v13}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 1498
    iget-object v9, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v9}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/network/g;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1500
    :cond_10
    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/d;->i()Z

    move-result v4

    iput-boolean v4, v1, Lcom/ss/android/socialbase/downloader/h/c;->l:Z

    .line 1502
    iget-boolean v4, v1, Lcom/ss/android/socialbase/downloader/h/c;->l:Z

    if-nez v4, :cond_12

    cmp-long v4, v5, v11

    if-nez v4, :cond_12

    instance-of v4, v0, Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v4, :cond_11

    goto :goto_6

    .line 1503
    :cond_11
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const-string v2, ""

    invoke-direct {v0, v8, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1506
    :cond_12
    :goto_6
    iget-boolean v4, v1, Lcom/ss/android/socialbase/downloader/h/c;->l:Z

    if-nez v4, :cond_14

    const-string v4, "Content-Range"

    .line 1507
    invoke-static {v0, v4}, Lcom/ss/android/socialbase/downloader/i/e;->b(Lcom/ss/android/socialbase/downloader/network/g;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1508
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_13

    const/4 v4, 0x2

    invoke-static {v4}, Lcom/ss/android/socialbase/downloader/i/a;->a(I)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1510
    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->b(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_7

    :cond_13
    const/4 v0, 0x0

    add-long v2, p3, v5

    goto :goto_7

    :cond_14
    const-wide/16 v2, -0x1

    .line 1519
    :goto_7
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/h/c;->v()Z

    move-result v0

    if-eqz v0, :cond_15

    return-void

    .line 1523
    :cond_15
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getExpectFileLength()J

    move-result-wide v4

    cmp-long v0, v4, v11

    if-lez v0, :cond_17

    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 1524
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v0

    const-string v4, "force_check_file_length"

    invoke-virtual {v0, v4}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v10, :cond_17

    .line 1525
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getExpectFileLength()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-nez v0, :cond_16

    goto :goto_8

    .line 1526
    :cond_16
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v4, 0x42e

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "expectFileLength = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v1, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 1527
    invoke-virtual {v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getExpectFileLength()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, " , totalLength = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v4, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1531
    :cond_17
    :goto_8
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v0, v2, v3, v7, v9}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(JLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/ss/android/socialbase/downloader/exception/h; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_9

    :catch_0
    move-exception v0

    const-string v2, "HandleFirstConnection"

    .line 1543
    invoke-static {v0, v2}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_9
    return-void

    :catch_1
    move-exception v0

    .line 1541
    throw v0

    :catch_2
    move-exception v0

    .line 1539
    throw v0
.end method

.method public a(Ljava/util/concurrent/Future;)V
    .locals 0

    .line 1998
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->b:Ljava/util/concurrent/Future;

    return-void
.end method

.method public a(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z
    .locals 4

    .line 1596
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/e;->h(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRetryCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    return v1

    .line 1600
    :cond_0
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/e;->b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 1601
    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->h:Z

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->d:Z

    if-nez p1, :cond_1

    .line 1602
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    .line 1603
    iput-boolean v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->d:Z

    :cond_1
    return v2

    .line 1607
    :cond_2
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-gtz v0, :cond_5

    :cond_3
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->hasNextBackupUrl()Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz p1, :cond_6

    .line 1608
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v0

    const/16 v3, 0x3f3

    if-eq v0, v3, :cond_4

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v0, :cond_6

    :cond_4
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->canReplaceHttpForRetry()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    instance-of p1, p1, Lcom/ss/android/socialbase/downloader/exception/f;

    if-nez p1, :cond_6

    const/4 v1, 0x1

    :cond_6
    return v1
.end method

.method public b()V
    .locals 1

    .line 206
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    .line 207
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/f/k;->a()V

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->c()V

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->K:Lcom/ss/android/socialbase/downloader/f/k;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->g:Lcom/ss/android/socialbase/downloader/downloader/d;

    if-nez v0, :cond_2

    .line 214
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->s()V

    .line 215
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->c:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    .line 216
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->p()V

    .line 219
    :cond_2
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->I()V

    return-void
.end method

.method public b(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V
    .locals 3

    .line 1614
    sget-object v0, Lcom/ss/android/socialbase/downloader/h/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1615
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/i;->d:Lcom/ss/android/socialbase/downloader/constants/i;

    iput-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->n:Lcom/ss/android/socialbase/downloader/constants/i;

    .line 1616
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/h/c;->w:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 1618
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->I()V

    return-void
.end method

.method public b(J)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    .line 1578
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->G:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 1579
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/ss/android/socialbase/downloader/h/c;->G:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 1580
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->D()V

    .line 1582
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v0, p1, p2}, Lcom/ss/android/socialbase/downloader/downloader/e;->a(J)Z

    move-result p1

    return p1
.end method

.method public c()Lcom/ss/android/socialbase/downloader/model/DownloadTask;
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    return-object v0
.end method

.method public c(J)V
    .locals 1

    .line 2002
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    if-nez v0, :cond_0

    return-void

    .line 2007
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    instance-of v0, v0, Lcom/ss/android/socialbase/downloader/network/a;

    if-eqz v0, :cond_1

    .line 2009
    :try_start_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->x:Lcom/ss/android/socialbase/downloader/network/i;

    check-cast v0, Lcom/ss/android/socialbase/downloader/network/a;

    invoke-virtual {v0, p1, p2}, Lcom/ss/android/socialbase/downloader/network/a;->a(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 2011
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method

.method public c(Lcom/ss/android/socialbase/downloader/exception/BaseException;)V
    .locals 2

    .line 1637
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-eqz v0, :cond_0

    .line 1638
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setChunkDowngradeRetryUsed(Z)V

    :cond_0
    const/4 v0, 0x0

    .line 1640
    invoke-virtual {p0, p1, v0}, Lcom/ss/android/socialbase/downloader/h/c;->a(Lcom/ss/android/socialbase/downloader/exception/BaseException;Z)V

    return-void
.end method

.method public d()Z
    .locals 1

    .line 1548
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public e()I
    .locals 1

    .line 1552
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->p:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()V
    .locals 2

    .line 1556
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->H:J

    .line 1557
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->v:Lcom/ss/android/socialbase/downloader/downloader/e;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/downloader/e;->a()V

    return-void
.end method

.method public g()Ljava/util/concurrent/Future;
    .locals 1

    .line 1994
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->b:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public run()V
    .locals 2

    .line 333
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/model/DownloadTask;I)V

    .line 336
    :try_start_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/network/b;->a()Lcom/ss/android/socialbase/downloader/network/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/network/b;->b()V

    .line 337
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/h/c;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    invoke-static {}, Lcom/ss/android/socialbase/downloader/network/b;->a()Lcom/ss/android/socialbase/downloader/network/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/network/b;->c()V

    .line 342
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/h/c;->c:Lcom/ss/android/socialbase/downloader/model/DownloadTask;

    invoke-static {v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/b;->b(Lcom/ss/android/socialbase/downloader/model/DownloadTask;I)V

    return-void

    :catchall_0
    move-exception v0

    .line 339
    invoke-static {}, Lcom/ss/android/socialbase/downloader/network/b;->a()Lcom/ss/android/socialbase/downloader/network/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/network/b;->c()V

    throw v0
.end method
