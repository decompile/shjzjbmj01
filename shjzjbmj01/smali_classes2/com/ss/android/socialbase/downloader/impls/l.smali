.class public Lcom/ss/android/socialbase/downloader/impls/l;
.super Ljava/lang/Object;
.source "DownloadProxy.java"


# static fields
.field private static volatile a:Lcom/ss/android/socialbase/downloader/downloader/l;

.field private static volatile b:Lcom/ss/android/socialbase/downloader/downloader/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Z)Lcom/ss/android/socialbase/downloader/downloader/l;
    .locals 1

    if-eqz p0, :cond_2

    .line 22
    sget-object p0, Lcom/ss/android/socialbase/downloader/impls/l;->b:Lcom/ss/android/socialbase/downloader/downloader/l;

    if-nez p0, :cond_1

    .line 23
    const-class p0, Lcom/ss/android/socialbase/downloader/impls/l;

    monitor-enter p0

    .line 24
    :try_start_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/impls/l;->b:Lcom/ss/android/socialbase/downloader/downloader/l;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/ss/android/socialbase/downloader/impls/o;

    invoke-direct {v0}, Lcom/ss/android/socialbase/downloader/impls/o;-><init>()V

    sput-object v0, Lcom/ss/android/socialbase/downloader/impls/l;->b:Lcom/ss/android/socialbase/downloader/downloader/l;

    .line 27
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 29
    :cond_1
    :goto_0
    sget-object p0, Lcom/ss/android/socialbase/downloader/impls/l;->b:Lcom/ss/android/socialbase/downloader/downloader/l;

    return-object p0

    .line 31
    :cond_2
    sget-object p0, Lcom/ss/android/socialbase/downloader/impls/l;->a:Lcom/ss/android/socialbase/downloader/downloader/l;

    if-nez p0, :cond_4

    .line 32
    const-class p0, Lcom/ss/android/socialbase/downloader/impls/l;

    monitor-enter p0

    .line 33
    :try_start_1
    sget-object v0, Lcom/ss/android/socialbase/downloader/impls/l;->a:Lcom/ss/android/socialbase/downloader/downloader/l;

    if-nez v0, :cond_3

    .line 34
    new-instance v0, Lcom/ss/android/socialbase/downloader/impls/p;

    invoke-direct {v0}, Lcom/ss/android/socialbase/downloader/impls/p;-><init>()V

    sput-object v0, Lcom/ss/android/socialbase/downloader/impls/l;->a:Lcom/ss/android/socialbase/downloader/downloader/l;

    .line 36
    :cond_3
    monitor-exit p0

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 38
    :cond_4
    :goto_1
    sget-object p0, Lcom/ss/android/socialbase/downloader/impls/l;->a:Lcom/ss/android/socialbase/downloader/downloader/l;

    return-object p0
.end method
