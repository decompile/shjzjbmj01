.class Lcom/ss/android/socialbase/downloader/b/e$1;
.super Ljava/lang/Object;
.source "SqlDownloadCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ss/android/socialbase/downloader/b/e;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Lcom/ss/android/socialbase/downloader/b/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/util/SparseArray;

.field final synthetic b:Landroid/util/SparseArray;

.field final synthetic c:Lcom/ss/android/socialbase/downloader/b/d;

.field final synthetic d:Lcom/ss/android/socialbase/downloader/b/e;


# direct methods
.method constructor <init>(Lcom/ss/android/socialbase/downloader/b/e;Landroid/util/SparseArray;Landroid/util/SparseArray;Lcom/ss/android/socialbase/downloader/b/d;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/b/e$1;->a:Landroid/util/SparseArray;

    iput-object p3, p0, Lcom/ss/android/socialbase/downloader/b/e$1;->b:Landroid/util/SparseArray;

    iput-object p4, p0, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    move-object/from16 v1, p0

    .line 108
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;)V

    .line 110
    invoke-static {}, Lcom/ss/android/socialbase/downloader/b/e;->f()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 113
    :cond_0
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->a:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    .line 115
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    :cond_1
    move-object v8, v0

    .line 117
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->b:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    .line 119
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    :cond_2
    move-object v9, v0

    .line 122
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->B()Lcom/ss/android/socialbase/downloader/downloader/j;

    move-result-object v0

    .line 124
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    .line 125
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 126
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    const/16 v10, 0x2000

    const/4 v11, -0x3

    const/4 v12, 0x0

    const/4 v13, 0x1

    .line 130
    :try_start_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/b/e;->f()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, "SELECT * FROM downloader"

    invoke-virtual {v6, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 131
    :goto_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 133
    new-instance v2, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-direct {v2, v6}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;-><init>(Landroid/database/Cursor;)V

    .line 135
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRealStatus()I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setStatusAtDbInit(I)V

    .line 136
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isCanResumeFromBreakPointStatus()Z

    move-result v7

    const/4 v14, -0x5

    if-eqz v7, :cond_3

    .line 137
    invoke-virtual {v2, v14}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setStatus(I)V

    .line 139
    invoke-static {}, Lcom/ss/android/socialbase/downloader/g/a;->c()Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v7

    const-string v15, "status_not_update_to_db"

    invoke-virtual {v7, v15, v13}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 140
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v7

    invoke-virtual {v5, v7, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 143
    :cond_3
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getSavePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_9

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    goto :goto_2

    .line 148
    :cond_4
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v7

    const/16 v15, 0xb

    if-ne v7, v13, :cond_5

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v7, v16, v18

    if-gtz v7, :cond_5

    const/4 v7, 0x1

    goto :goto_1

    .line 150
    :cond_5
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v7

    if-eq v7, v11, :cond_6

    .line 151
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v7

    if-eq v7, v15, :cond_6

    .line 152
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isFileDataValid()Z

    move-result v7

    if-nez v7, :cond_6

    .line 154
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->reset()V

    :cond_6
    const/4 v7, 0x0

    .line 156
    :goto_1
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v11

    if-ne v11, v15, :cond_7

    .line 157
    invoke-virtual {v2, v14}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setStatus(I)V

    .line 160
    :cond_7
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v11

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getSavePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-static {v11, v14, v15}, Lcom/ss/android/socialbase/downloader/i/e;->a(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_a

    const/high16 v11, 0x2000000

    .line 161
    invoke-static {v11}, Lcom/ss/android/socialbase/downloader/i/a;->a(I)Z

    move-result v11

    if-eqz v11, :cond_8

    goto :goto_2

    .line 164
    :cond_8
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->erase()V

    goto :goto_3

    :cond_9
    :goto_2
    const/4 v7, 0x1

    :cond_a
    :goto_3
    if-eqz v7, :cond_b

    .line 170
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 173
    :cond_b
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v7

    if-eqz v0, :cond_c

    .line 176
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getSavePath()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v0, v11, v14}, Lcom/ss/android/socialbase/downloader/downloader/j;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    goto :goto_4

    :cond_c
    const/4 v11, 0x0

    :goto_4
    if-eq v11, v7, :cond_d

    .line 178
    invoke-virtual {v2, v11}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setId(I)V

    .line 179
    invoke-virtual {v5, v7, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_d
    if-eqz v8, :cond_e

    .line 183
    monitor-enter v8
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 184
    :try_start_2
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v7

    invoke-virtual {v8, v7, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185
    monitor-exit v8

    goto :goto_5

    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :cond_e
    :goto_5
    const/4 v11, -0x3

    goto/16 :goto_0

    .line 192
    :cond_f
    new-array v0, v13, [Ljava/io/Closeable;

    aput-object v6, v0, v12

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    .line 195
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-static {v0, v3}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Ljava/util/List;)V

    .line 197
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    move-object v6, v8

    move-object v7, v9

    invoke-static/range {v2 .. v7}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Ljava/util/List;Ljava/util/List;Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    if-eqz v8, :cond_15

    .line 201
    :try_start_4
    monitor-enter v8
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 202
    :try_start_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 203
    :goto_6
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v12, v2, :cond_12

    .line 204
    invoke-virtual {v8, v12}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    if-eqz v2, :cond_11

    .line 206
    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 207
    invoke-static {v10}, Lcom/ss/android/socialbase/downloader/i/a;->a(I)Z

    move-result v3

    if-eqz v3, :cond_10

    if-eqz v2, :cond_11

    .line 209
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isChunkBreakpointAvailable()Z

    move-result v3

    if-nez v3, :cond_11

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v3

    const/4 v4, -0x3

    if-eq v3, v4, :cond_11

    .line 210
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v2

    invoke-virtual {v9, v2}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_7

    :cond_10
    if-eqz v2, :cond_11

    .line 214
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isChunkBreakpointAvailable()Z

    move-result v3

    if-nez v3, :cond_11

    .line 215
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v2

    invoke-virtual {v9, v2}, Landroid/util/SparseArray;->remove(I)V

    :cond_11
    :goto_7
    add-int/lit8 v12, v12, 0x1

    goto :goto_6

    .line 221
    :cond_12
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 222
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_8

    .line 224
    :cond_13
    monitor-exit v8

    goto :goto_a

    :catchall_1
    move-exception v0

    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception v0

    goto :goto_9

    :catch_0
    move-exception v0

    .line 228
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 230
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    if-eqz v0, :cond_16

    goto :goto_b

    :goto_9
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    if-eqz v2, :cond_14

    .line 231
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    invoke-interface {v2}, Lcom/ss/android/socialbase/downloader/b/d;->a()V

    .line 232
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-static {v2, v13}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Z)Z

    .line 234
    :cond_14
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-virtual {v2, v8, v9}, Lcom/ss/android/socialbase/downloader/b/e;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    throw v0

    .line 230
    :cond_15
    :goto_a
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    if-eqz v0, :cond_16

    .line 231
    :goto_b
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/b/d;->a()V

    .line 232
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-static {v0, v13}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Z)Z

    .line 234
    :cond_16
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-virtual {v0, v8, v9}, Lcom/ss/android/socialbase/downloader/b/e;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    goto/16 :goto_12

    :catchall_3
    move-exception v0

    move-object v11, v0

    goto/16 :goto_13

    :catch_1
    move-exception v0

    move-object v2, v6

    goto :goto_c

    :catchall_4
    move-exception v0

    move-object v11, v0

    move-object v6, v2

    goto/16 :goto_13

    :catch_2
    move-exception v0

    .line 190
    :goto_c
    :try_start_8
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 192
    new-array v0, v13, [Ljava/io/Closeable;

    aput-object v2, v0, v12

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    .line 195
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-static {v0, v3}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Ljava/util/List;)V

    .line 197
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    move-object v6, v8

    move-object v7, v9

    invoke-static/range {v2 .. v7}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Ljava/util/List;Ljava/util/List;Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    if-eqz v8, :cond_1c

    .line 201
    :try_start_9
    monitor-enter v8
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .line 202
    :try_start_a
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 203
    :goto_d
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v12, v2, :cond_19

    .line 204
    invoke-virtual {v8, v12}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    if-eqz v2, :cond_18

    .line 206
    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 207
    invoke-static {v10}, Lcom/ss/android/socialbase/downloader/i/a;->a(I)Z

    move-result v3

    if-eqz v3, :cond_17

    if-eqz v2, :cond_18

    .line 209
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isChunkBreakpointAvailable()Z

    move-result v3

    if-nez v3, :cond_18

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v3

    const/4 v4, -0x3

    if-eq v3, v4, :cond_18

    .line 210
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v2

    invoke-virtual {v9, v2}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_e

    :cond_17
    if-eqz v2, :cond_18

    .line 214
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isChunkBreakpointAvailable()Z

    move-result v3

    if-nez v3, :cond_18

    .line 215
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v2

    invoke-virtual {v9, v2}, Landroid/util/SparseArray;->remove(I)V

    :cond_18
    :goto_e
    add-int/lit8 v12, v12, 0x1

    goto :goto_d

    .line 221
    :cond_19
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 222
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_f

    .line 224
    :cond_1a
    monitor-exit v8

    goto :goto_11

    :catchall_5
    move-exception v0

    monitor-exit v8
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    :try_start_b
    throw v0
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    :catchall_6
    move-exception v0

    goto :goto_10

    :catch_3
    move-exception v0

    .line 228
    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    .line 230
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    if-eqz v0, :cond_16

    goto/16 :goto_b

    :goto_10
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    if-eqz v2, :cond_1b

    .line 231
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    invoke-interface {v2}, Lcom/ss/android/socialbase/downloader/b/d;->a()V

    .line 232
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-static {v2, v13}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Z)Z

    .line 234
    :cond_1b
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-virtual {v2, v8, v9}, Lcom/ss/android/socialbase/downloader/b/e;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    throw v0

    .line 230
    :cond_1c
    :goto_11
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    if-eqz v0, :cond_16

    goto/16 :goto_b

    :goto_12
    return-void

    .line 192
    :goto_13
    new-array v0, v13, [Ljava/io/Closeable;

    aput-object v6, v0, v12

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    .line 195
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-static {v0, v3}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Ljava/util/List;)V

    .line 197
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    move-object v6, v8

    move-object v7, v9

    invoke-static/range {v2 .. v7}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Ljava/util/List;Ljava/util/List;Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    if-eqz v8, :cond_23

    .line 201
    :try_start_d
    monitor-enter v8
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_8

    .line 202
    :try_start_e
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 203
    :goto_14
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v12, v2, :cond_20

    .line 204
    invoke-virtual {v8, v12}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    if-eqz v2, :cond_1e

    .line 206
    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 207
    invoke-static {v10}, Lcom/ss/android/socialbase/downloader/i/a;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1d

    if-eqz v2, :cond_1e

    .line 209
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isChunkBreakpointAvailable()Z

    move-result v3

    if-nez v3, :cond_1e

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getStatus()I

    move-result v3

    const/4 v4, -0x3

    if-eq v3, v4, :cond_1f

    .line 210
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v2

    invoke-virtual {v9, v2}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_15

    :cond_1d
    const/4 v4, -0x3

    if-eqz v2, :cond_1f

    .line 214
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isChunkBreakpointAvailable()Z

    move-result v3

    if-nez v3, :cond_1f

    .line 215
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v2

    invoke-virtual {v9, v2}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_15

    :cond_1e
    const/4 v4, -0x3

    :cond_1f
    :goto_15
    add-int/lit8 v12, v12, 0x1

    goto :goto_14

    .line 221
    :cond_20
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_16
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 222
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_16

    .line 224
    :cond_21
    monitor-exit v8

    goto :goto_18

    :catchall_7
    move-exception v0

    monitor-exit v8
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    :try_start_f
    throw v0
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_8

    :catchall_8
    move-exception v0

    goto :goto_17

    :catch_4
    move-exception v0

    .line 228
    :try_start_10
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_8

    .line 230
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    if-eqz v0, :cond_24

    goto :goto_19

    :goto_17
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    if-eqz v2, :cond_22

    .line 231
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    invoke-interface {v2}, Lcom/ss/android/socialbase/downloader/b/d;->a()V

    .line 232
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-static {v2, v13}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Z)Z

    .line 234
    :cond_22
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-virtual {v2, v8, v9}, Lcom/ss/android/socialbase/downloader/b/e;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    throw v0

    .line 230
    :cond_23
    :goto_18
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    if-eqz v0, :cond_24

    .line 231
    :goto_19
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->c:Lcom/ss/android/socialbase/downloader/b/d;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/b/d;->a()V

    .line 232
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-static {v0, v13}, Lcom/ss/android/socialbase/downloader/b/e;->a(Lcom/ss/android/socialbase/downloader/b/e;Z)Z

    .line 234
    :cond_24
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/b/e$1;->d:Lcom/ss/android/socialbase/downloader/b/e;

    invoke-virtual {v0, v8, v9}, Lcom/ss/android/socialbase/downloader/b/e;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    throw v11
.end method
