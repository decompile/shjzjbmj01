.class public Lcom/ss/android/socialbase/downloader/downloader/d;
.super Ljava/lang/Object;
.source "DownloadResponseHandler.java"


# static fields
.field private static b:Ljava/lang/String; = "ResponseHandler"


# instance fields
.field private A:J

.field private B:J

.field private C:J

.field private D:J

.field private volatile E:J

.field private volatile F:J

.field a:Z

.field private final c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/ss/android/socialbase/downloader/model/b;

.field private final f:Lcom/ss/android/socialbase/downloader/network/i;

.field private g:Lcom/ss/android/socialbase/downloader/downloader/i;

.field private h:Lcom/ss/android/socialbase/downloader/impls/k;

.field private i:Lcom/ss/android/socialbase/downloader/downloader/r;

.field private j:Lcom/ss/android/socialbase/downloader/model/e;

.field private k:Lcom/ss/android/socialbase/downloader/exception/BaseException;

.field private volatile l:Z

.field private volatile m:Z

.field private final n:Lcom/ss/android/socialbase/downloader/h/f;

.field private o:J

.field private p:J

.field private volatile q:J

.field private volatile r:J

.field private final s:Z

.field private final t:Lcom/ss/android/socialbase/downloader/g/a;

.field private final u:Lcom/ss/android/socialbase/downloader/a/a;

.field private final v:Z

.field private final w:J

.field private final x:J

.field private final y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/i;Lcom/ss/android/socialbase/downloader/model/b;Lcom/ss/android/socialbase/downloader/h/f;)V
    .locals 5

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 88
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->a:Z

    const-wide/16 v1, 0x0

    .line 438
    iput-wide v1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->E:J

    .line 439
    iput-wide v1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->F:J

    .line 101
    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 102
    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->d:Ljava/lang/String;

    .line 103
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->x()Lcom/ss/android/socialbase/downloader/downloader/i;

    move-result-object p2

    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->g:Lcom/ss/android/socialbase/downloader/downloader/i;

    .line 104
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->g:Lcom/ss/android/socialbase/downloader/downloader/i;

    instance-of p2, p2, Lcom/ss/android/socialbase/downloader/impls/d;

    if-eqz p2, :cond_0

    .line 105
    iget-object p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->g:Lcom/ss/android/socialbase/downloader/downloader/i;

    check-cast p2, Lcom/ss/android/socialbase/downloader/impls/d;

    .line 106
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/impls/d;->a()Lcom/ss/android/socialbase/downloader/impls/k;

    move-result-object v3

    iput-object v3, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    .line 107
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/impls/d;->e()Lcom/ss/android/socialbase/downloader/downloader/r;

    move-result-object p2

    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->i:Lcom/ss/android/socialbase/downloader/downloader/r;

    .line 110
    :cond_0
    iput-object p3, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    .line 111
    iput-object p4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    .line 112
    iput-object p5, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    .line 113
    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/model/b;->n()J

    move-result-wide p2

    iput-wide p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    .line 114
    iget-wide p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iput-wide p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    .line 115
    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/model/b;->d()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 116
    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/model/b;->q()J

    move-result-wide p2

    iput-wide p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    goto :goto_0

    .line 118
    :cond_1
    invoke-virtual {p4, v0}, Lcom/ss/android/socialbase/downloader/model/b;->c(Z)J

    move-result-wide p2

    iput-wide p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    .line 119
    :goto_0
    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/model/b;->p()J

    move-result-wide p2

    iput-wide p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->q:J

    .line 120
    invoke-static {}, Lcom/ss/android/socialbase/downloader/a/a;->a()Lcom/ss/android/socialbase/downloader/a/a;

    move-result-object p2

    iput-object p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->u:Lcom/ss/android/socialbase/downloader/a/a;

    .line 121
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result p1

    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/g/a;->a(I)Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object p1

    iput-object p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    .line 122
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    const-string p2, "sync_strategy"

    invoke-virtual {p1, p2, v0}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->v:Z

    .line 123
    iget-boolean p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->v:Z

    if-eqz p1, :cond_3

    .line 124
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    const-string p3, "sync_interval_ms_fg"

    const/16 p4, 0x1388

    invoke-virtual {p1, p3, p4}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result p1

    int-to-long p3, p1

    .line 126
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    const-string p5, "sync_interval_ms_bg"

    const/16 v1, 0x3e8

    invoke-virtual {p1, p5, v1}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result p1

    int-to-long v1, p1

    const-wide/16 v3, 0x1f4

    .line 128
    invoke-static {p3, p4, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p3

    iput-wide p3, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->w:J

    .line 129
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p3

    iput-wide p3, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->x:J

    goto :goto_2

    .line 131
    :cond_3
    iput-wide v1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->w:J

    .line 132
    iput-wide v1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->x:J

    .line 134
    :goto_2
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    const-string p3, "monitor_rw"

    invoke-virtual {p1, p3}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;)I

    move-result p1

    if-ne p1, p2, :cond_4

    goto :goto_3

    :cond_4
    const/4 p2, 0x0

    :goto_3
    iput-boolean p2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->y:Z

    const/high16 p1, 0x10000

    .line 135
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/i/a;->a(I)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->s:Z

    return-void
.end method

.method private a(Ljava/io/InputStream;)Lcom/ss/android/socialbase/downloader/e/b;
    .locals 8

    .line 415
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->G()I

    move-result v0

    .line 416
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v2, "rw_concurrent"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 418
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result v1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 419
    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v4

    const-wide/32 v6, 0x1400000

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 420
    iget-object v1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v4, "rw_concurrent_max_buffer_count"

    const/4 v5, 0x4

    invoke-virtual {v1, v4, v5}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v1

    .line 424
    :try_start_0
    new-instance v4, Lcom/ss/android/socialbase/downloader/e/a;

    invoke-direct {v4, p1, v0, v1}, Lcom/ss/android/socialbase/downloader/e/a;-><init>(Ljava/io/InputStream;II)V

    .line 425
    iput-boolean v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->z:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    :catch_0
    move-exception v1

    .line 428
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 433
    :cond_0
    new-instance v1, Lcom/ss/android/socialbase/downloader/e/c;

    invoke-direct {v1, p1, v0}, Lcom/ss/android/socialbase/downloader/e/c;-><init>(Ljava/io/InputStream;I)V

    .line 434
    iput-boolean v3, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->z:Z

    return-object v1
.end method

.method static synthetic a(Lcom/ss/android/socialbase/downloader/downloader/d;)Lcom/ss/android/socialbase/downloader/network/i;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    return-object p0
.end method

.method private a(Lcom/ss/android/socialbase/downloader/downloader/i;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    if-nez v7, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 511
    instance-of v8, v7, Lcom/ss/android/socialbase/downloader/b/e;

    if-eqz v8, :cond_1

    .line 512
    invoke-static {}, Lcom/ss/android/socialbase/downloader/i/e;->b()Z

    move-result v1

    invoke-static {v1}, Lcom/ss/android/socialbase/downloader/impls/l;->a(Z)Lcom/ss/android/socialbase/downloader/downloader/l;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    move-object v15, v1

    .line 518
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/b;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 519
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/b;->e()Lcom/ss/android/socialbase/downloader/model/b;

    move-result-object v1

    :goto_0
    move-object v5, v1

    goto :goto_1

    .line 521
    :cond_2
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    goto :goto_0

    :goto_1
    if-eqz v5, :cond_7

    .line 526
    iget-wide v1, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    invoke-virtual {v5, v1, v2}, Lcom/ss/android/socialbase/downloader/model/b;->b(J)V

    if-eqz v8, :cond_3

    if-eqz v15, :cond_3

    .line 528
    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v10

    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result v11

    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/model/b;->b()I

    move-result v12

    iget-wide v13, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    move-object v9, v15

    invoke-interface/range {v9 .. v14}, Lcom/ss/android/socialbase/downloader/downloader/l;->a(IIIJ)V

    move-object v11, v5

    goto :goto_2

    .line 530
    :cond_3
    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v2

    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result v3

    invoke-virtual {v5}, Lcom/ss/android/socialbase/downloader/model/b;->b()I

    move-result v4

    iget-wide v9, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    move-object/from16 v1, p1

    move-object v11, v5

    move-wide v5, v9

    invoke-interface/range {v1 .. v6}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(IIIJ)V

    .line 532
    :goto_2
    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->h()Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x0

    .line 534
    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->i()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 535
    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->j()J

    move-result-wide v2

    .line 536
    iget-wide v4, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    cmp-long v6, v2, v4

    if-lez v6, :cond_5

    if-eqz v8, :cond_4

    if-eqz v15, :cond_4

    .line 538
    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v1

    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->b()I

    move-result v4

    invoke-interface {v15, v1, v4, v2, v3}, Lcom/ss/android/socialbase/downloader/downloader/l;->a(IIJ)V

    goto :goto_3

    .line 540
    :cond_4
    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v1

    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->b()I

    move-result v4

    invoke-interface {v7, v1, v4, v2, v3}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(IIJ)V

    :goto_3
    const/4 v1, 0x1

    :cond_5
    if-nez v1, :cond_9

    if-eqz v8, :cond_6

    if-eqz v15, :cond_6

    .line 546
    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v1

    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->b()I

    move-result v2

    iget-wide v3, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    invoke-interface {v15, v1, v2, v3, v4}, Lcom/ss/android/socialbase/downloader/downloader/l;->a(IIJ)V

    goto :goto_4

    .line 548
    :cond_6
    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v1

    invoke-virtual {v11}, Lcom/ss/android/socialbase/downloader/model/b;->b()I

    move-result v2

    iget-wide v3, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    invoke-interface {v7, v1, v2, v3, v4}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(IIJ)V

    goto :goto_4

    .line 553
    :cond_7
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/b;->d()Z

    move-result v1

    if-eqz v1, :cond_9

    if-eqz v8, :cond_8

    if-eqz v15, :cond_8

    .line 555
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v1

    iget-object v2, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result v2

    iget-wide v3, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    invoke-interface {v15, v1, v2, v3, v4}, Lcom/ss/android/socialbase/downloader/downloader/l;->a(IIJ)V

    goto :goto_4

    .line 557
    :cond_8
    iget-object v1, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v1

    iget-object v2, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    invoke-virtual {v2}, Lcom/ss/android/socialbase/downloader/model/b;->s()I

    move-result v2

    iget-wide v3, v0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    invoke-interface {v7, v1, v2, v3, v4}, Lcom/ss/android/socialbase/downloader/downloader/i;->a(IIJ)V

    :cond_9
    :goto_4
    return-void
.end method

.method private a(Z)V
    .locals 8

    .line 442
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 443
    iget-wide v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->F:J

    sub-long v2, v0, v2

    .line 445
    iget-boolean v4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->v:Z

    if-eqz v4, :cond_1

    .line 446
    iget-object p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->u:Lcom/ss/android/socialbase/downloader/a/a;

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/a/a;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-wide v4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->w:J

    goto :goto_0

    :cond_0
    iget-wide v4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->x:J

    :goto_0
    cmp-long p1, v2, v4

    if-lez p1, :cond_3

    .line 447
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V

    .line 448
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->F:J

    goto :goto_1

    .line 451
    :cond_1
    iget-wide v4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iget-wide v6, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->E:J

    sub-long/2addr v4, v6

    if-nez p1, :cond_2

    .line 452
    invoke-direct {p0, v4, v5, v2, v3}, Lcom/ss/android/socialbase/downloader/downloader/d;->b(JJ)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 453
    :cond_2
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V

    .line 454
    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->F:J

    :cond_3
    :goto_1
    return-void
.end method

.method private b(JJ)Z
    .locals 3

    const-wide/32 v0, 0x10000

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const-wide/16 p1, 0x1f4

    cmp-long v0, p3, p1

    if-lez v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private f()Z
    .locals 1

    .line 143
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->l:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private g()V
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    if-nez v0, :cond_0

    return-void

    .line 163
    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 165
    new-instance v1, Lcom/ss/android/socialbase/downloader/downloader/d$1;

    invoke-direct {v1, p0}, Lcom/ss/android/socialbase/downloader/downloader/d$1;-><init>(Lcom/ss/android/socialbase/downloader/downloader/d;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method private h()V
    .locals 6

    .line 469
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->y:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 472
    :try_start_0
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/e;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x1

    goto :goto_1

    :catch_0
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_5

    .line 479
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4, v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->updateRealDownloadTime(Z)V

    .line 480
    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getChunkCount()I

    move-result v4

    if-le v4, v3, :cond_1

    const/4 v2, 0x1

    .line 482
    :cond_1
    invoke-static {}, Lcom/ss/android/socialbase/downloader/i/e;->b()Z

    move-result v3

    invoke-static {v3}, Lcom/ss/android/socialbase/downloader/impls/l;->a(Z)Lcom/ss/android/socialbase/downloader/downloader/l;

    move-result-object v3

    if-eqz v2, :cond_3

    .line 484
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->i:Lcom/ss/android/socialbase/downloader/downloader/r;

    invoke-direct {p0, v2}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    if-eqz v3, :cond_2

    .line 486
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {v3, v2}, Lcom/ss/android/socialbase/downloader/downloader/l;->c(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z

    goto :goto_2

    .line 488
    :cond_2
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->i:Lcom/ss/android/socialbase/downloader/downloader/r;

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v3

    iget-object v4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Lcom/ss/android/socialbase/downloader/downloader/r;->a(IJ)Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    goto :goto_2

    :cond_3
    if-eqz v3, :cond_4

    .line 491
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-interface {v3, v2}, Lcom/ss/android/socialbase/downloader/downloader/l;->c(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Z

    goto :goto_2

    .line 493
    :cond_4
    iget-object v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->i:Lcom/ss/android/socialbase/downloader/downloader/r;

    iget-object v3, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/b;->k()I

    move-result v3

    iget-wide v4, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    invoke-interface {v2, v3, v4, v5}, Lcom/ss/android/socialbase/downloader/downloader/r;->a(IJ)Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    .line 495
    :goto_2
    iget-wide v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iput-wide v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->E:J

    .line 497
    :cond_5
    iget-boolean v2, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->y:Z

    if-eqz v2, :cond_6

    .line 498
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    .line 499
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->C:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->C:J

    :cond_6
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .line 139
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    return-wide v0
.end method

.method public a(JJ)V
    .locals 0

    .line 190
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->q:J

    .line 191
    iput-wide p3, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    return-void
.end method

.method public a(JJJ)V
    .locals 0

    .line 183
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    .line 184
    iput-wide p1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    .line 185
    iput-wide p3, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->q:J

    .line 186
    iput-wide p5, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    return-void
.end method

.method public b()V
    .locals 1

    .line 147
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->l:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 148
    iput-boolean v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->l:Z

    .line 149
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->g()V

    return-void
.end method

.method public c()V
    .locals 2

    .line 153
    iget-boolean v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    if-eqz v0, :cond_0

    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    monitor-enter v0

    const/4 v1, 0x1

    .line 155
    :try_start_0
    iput-boolean v1, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    .line 156
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->g()V

    return-void

    :catchall_0
    move-exception v1

    .line 156
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public d()V
    .locals 38
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;
        }
    .end annotation

    move-object/from16 v1, p0

    .line 196
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->f()Z

    move-result v0

    if-nez v0, :cond_39

    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    if-nez v0, :cond_0

    goto/16 :goto_1b

    .line 198
    :cond_0
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a(Lcom/ss/android/socialbase/downloader/network/g;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_38

    .line 203
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->e:Lcom/ss/android/socialbase/downloader/model/b;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/b;->l()J

    move-result-wide v6

    .line 207
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 208
    iget-boolean v15, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->y:Z

    .line 211
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    instance-of v11, v0, Lcom/ss/android/socialbase/downloader/network/a;

    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 214
    :try_start_0
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTempPath()Ljava/lang/String;

    move-result-object v0

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v12}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTempName()Ljava/lang/String;

    move-result-object v12

    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v4, "flush_buffer_size_byte"

    const/4 v5, -0x1

    .line 215
    invoke-virtual {v10, v4, v5}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v4

    .line 214
    invoke-static {v0, v12, v4}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/ss/android/socialbase/downloader/model/e;

    move-result-object v0

    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;
    :try_end_0
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_d
    .catchall {:try_start_0 .. :try_end_0} :catchall_9

    .line 217
    :try_start_1
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_9

    move-wide/from16 v29, v6

    :try_start_2
    iget-wide v5, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    invoke-virtual {v0, v5, v6}, Lcom/ss/android/socialbase/downloader/model/e;->a(J)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_9

    .line 222
    :try_start_3
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->a()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 227
    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->updateRealStartDownloadTime()V

    .line 229
    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Ljava/io/InputStream;)Lcom/ss/android/socialbase/downloader/e/b;

    move-result-object v10
    :try_end_3
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_3 .. :try_end_3} :catch_f
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_9

    .line 231
    :try_start_4
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-boolean v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->z:Z

    invoke-virtual {v0, v4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->setIsRwConcurrent(Z)V

    .line 236
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getOpenLimitSpeed()Z

    move-result v0

    iput-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->a:Z

    .line 237
    sget-wide v4, Lcom/ss/android/socialbase/downloader/constants/e;->d:J

    .line 238
    sget-wide v6, Lcom/ss/android/socialbase/downloader/constants/e;->e:J

    const-wide/16 v16, 0x3e8

    .line 240
    div-long v16, v16, v6

    div-long v4, v4, v16

    .line 243
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v20, v16

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    .line 247
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->f()Z

    move-result v0
    :try_end_4
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_7

    if-eqz v0, :cond_6

    .line 351
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->d()V

    :cond_1
    if-eqz v10, :cond_2

    .line 355
    invoke-interface {v10}, Lcom/ss/android/socialbase/downloader/e/b;->b()V

    .line 359
    :cond_2
    :try_start_5
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->s:Z

    if-eqz v0, :cond_4

    .line 360
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    monitor-enter v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 361
    :try_start_6
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    if-nez v0, :cond_3

    .line 363
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 364
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_3

    .line 365
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V

    .line 368
    :cond_3
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0

    .line 370
    :cond_4
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 371
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_5

    .line 372
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 377
    :cond_5
    :goto_1
    new-array v0, v14, [Ljava/io/Closeable;

    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v2, v0, v13

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    .line 380
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, v8

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    .line 393
    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->d:Ljava/lang/String;

    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    iget-boolean v14, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->l:Z

    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    move/from16 v31, v15

    move v15, v0

    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->k:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    move-object/from16 v16, v0

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    sub-long v17, v2, v4

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    move-wide/from16 v19, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->A:J

    move-wide/from16 v22, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->B:J

    move-wide/from16 v24, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->C:J

    move-wide/from16 v26, v2

    const/16 v28, 0x0

    move/from16 v21, v31

    invoke-static/range {v10 .. v28}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/g/a;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/i;ZZLcom/ss/android/socialbase/downloader/exception/BaseException;JJZJJJLorg/json/JSONObject;)V

    return-void

    :catchall_1
    move-exception v0

    .line 377
    new-array v2, v14, [Ljava/io/Closeable;

    iget-object v3, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v3, v2, v13

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    throw v0

    :cond_6
    move/from16 v31, v15

    if-eqz v31, :cond_7

    .line 249
    :try_start_8
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v16

    goto :goto_2

    :catchall_2
    move-exception v0

    goto/16 :goto_a

    :catch_0
    move-exception v0

    move-wide/from16 v34, v2

    goto/16 :goto_b

    :catch_1
    move-exception v0

    goto/16 :goto_c

    .line 250
    :cond_7
    :goto_2
    invoke-interface {v10}, Lcom/ss/android/socialbase/downloader/e/b;->a()Lcom/ss/android/socialbase/downloader/f/a;

    move-result-object v0

    if-eqz v31, :cond_8

    .line 251
    iget-wide v14, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->A:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v22

    const/4 v12, 0x0

    sub-long v22, v22, v16

    add-long v14, v14, v22

    iput-wide v14, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->A:J

    .line 253
    :cond_8
    iget v12, v0, Lcom/ss/android/socialbase/downloader/f/a;->c:I

    const/4 v14, -0x1

    if-ne v12, v14, :cond_9

    move-wide/from16 v34, v2

    goto/16 :goto_6

    .line 257
    :cond_9
    iget-object v15, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v15}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isIgnoreDataVerify()Z

    move-result v15

    if-nez v15, :cond_a

    iget-wide v14, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    move-wide/from16 v32, v14

    iget-wide v13, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J
    :try_end_8
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-wide/from16 v34, v2

    :try_start_9
    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    const/4 v15, 0x0

    sub-long/2addr v13, v2

    cmp-long v2, v32, v13

    if-lez v2, :cond_b

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    iget-wide v13, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    move-wide/from16 v36, v6

    iget-wide v6, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    const/4 v15, 0x0

    sub-long/2addr v13, v6

    int-to-long v6, v12

    add-long/2addr v13, v6

    cmp-long v6, v2, v13

    if-gez v6, :cond_c

    .line 258
    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    iget-wide v6, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iget-wide v12, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    const/4 v14, 0x0

    sub-long/2addr v6, v12

    sub-long/2addr v2, v6

    long-to-int v12, v2

    goto :goto_3

    :cond_a
    move-wide/from16 v34, v2

    :cond_b
    move-wide/from16 v36, v6

    :cond_c
    :goto_3
    if-eqz v31, :cond_d

    .line 261
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    move-wide/from16 v16, v2

    goto :goto_4

    :catch_2
    move-exception v0

    goto/16 :goto_b

    .line 262
    :cond_d
    :goto_4
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    iget-object v3, v0, Lcom/ss/android/socialbase/downloader/f/a;->a:[B

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6, v12}, Lcom/ss/android/socialbase/downloader/model/e;->a([BII)V

    if-eqz v31, :cond_e

    .line 263
    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->B:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    const/4 v13, 0x0

    sub-long v6, v6, v16

    add-long/2addr v2, v6

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->B:J

    .line 265
    :cond_e
    invoke-interface {v10, v0}, Lcom/ss/android/socialbase/downloader/e/b;->a(Lcom/ss/android/socialbase/downloader/f/a;)V

    .line 267
    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    int-to-long v6, v12

    add-long/2addr v2, v6

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    const/4 v0, 0x0

    add-long v18, v18, v6

    .line 270
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    monitor-enter v2
    :try_end_9
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 271
    :try_start_a
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->s:Z

    if-eqz v0, :cond_f

    .line 273
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    if-nez v0, :cond_10

    .line 275
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    invoke-interface {v0, v6, v7}, Lcom/ss/android/socialbase/downloader/h/f;->b(J)Z

    move-result v0

    .line 278
    iget-object v3, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v3}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 280
    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Z)V

    goto :goto_5

    .line 284
    :cond_f
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    invoke-interface {v0, v6, v7}, Lcom/ss/android/socialbase/downloader/h/f;->b(J)Z

    move-result v0

    .line 286
    iget-object v3, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v3}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 288
    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Z)V

    .line 290
    :cond_10
    :goto_5
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 292
    :try_start_b
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isDownloadWithWifiValid()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 297
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isPauseReserveWithWifiValid()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 302
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isIgnoreDataVerify()Z

    move-result v0

    if-nez v0, :cond_16

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-ltz v0, :cond_16

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    iget-wide v6, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iget-wide v12, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J
    :try_end_b
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    const/4 v0, 0x0

    sub-long/2addr v6, v12

    cmp-long v0, v2, v6

    if-gtz v0, :cond_16

    .line 351
    :goto_6
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v0, :cond_11

    .line 352
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->d()V

    :cond_11
    if-eqz v10, :cond_12

    .line 355
    invoke-interface {v10}, Lcom/ss/android/socialbase/downloader/e/b;->b()V

    .line 359
    :cond_12
    :try_start_c
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->s:Z

    if-eqz v0, :cond_14

    .line 360
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    monitor-enter v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 361
    :try_start_d
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    if-nez v0, :cond_13

    .line 363
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 364
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_13

    .line 365
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V

    .line 368
    :cond_13
    monitor-exit v2

    goto :goto_7

    :catchall_3
    move-exception v0

    monitor-exit v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    throw v0

    .line 370
    :cond_14
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 371
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_15

    .line 372
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :cond_15
    :goto_7
    const/4 v2, 0x1

    .line 377
    new-array v0, v2, [Ljava/io/Closeable;

    iget-object v3, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    const/4 v4, 0x0

    aput-object v3, v0, v4

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    .line 380
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v5

    sub-long/2addr v5, v8

    iput-wide v5, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    .line 393
    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->d:Ljava/lang/String;

    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    const/4 v3, 0x0

    iget-boolean v14, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->l:Z

    const/4 v6, 0x1

    goto/16 :goto_14

    :catchall_4
    move-exception v0

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 377
    new-array v2, v6, [Ljava/io/Closeable;

    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    throw v0

    :cond_16
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 307
    :try_start_f
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->a:Z

    if-eqz v0, :cond_18

    cmp-long v0, v18, v4

    if-lez v0, :cond_18

    .line 308
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12
    :try_end_f
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_f .. :try_end_f} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_10

    const/4 v0, 0x0

    sub-long v12, v12, v20

    cmp-long v0, v12, v36

    if-gez v0, :cond_17

    sub-long v12, v36, v12

    .line 311
    :try_start_10
    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_10
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_10

    goto :goto_8

    :catch_3
    move-exception v0

    move-object v2, v0

    .line 313
    :try_start_11
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 317
    :cond_17
    :goto_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-wide/from16 v20, v12

    const-wide/16 v18, 0x0

    :cond_18
    move/from16 v15, v31

    move-wide/from16 v2, v34

    move-wide/from16 v6, v36

    const/4 v13, 0x0

    const/4 v14, 0x1

    goto/16 :goto_0

    :cond_19
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 299
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/e;

    invoke-direct {v0}, Lcom/ss/android/socialbase/downloader/exception/e;-><init>()V

    throw v0

    :cond_1a
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 293
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/c;

    invoke-direct {v0}, Lcom/ss/android/socialbase/downloader/exception/c;-><init>()V

    throw v0
    :try_end_11
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_11 .. :try_end_11} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_4
    .catchall {:try_start_11 .. :try_end_11} :catchall_10

    :catchall_5
    move-exception v0

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 290
    :goto_9
    :try_start_12
    monitor-exit v2
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    :try_start_13
    throw v0
    :try_end_13
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_13 .. :try_end_13} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_4
    .catchall {:try_start_13 .. :try_end_13} :catchall_10

    :catch_4
    move-exception v0

    goto/16 :goto_11

    :catch_5
    move-exception v0

    goto/16 :goto_17

    :catchall_6
    move-exception v0

    goto :goto_9

    :catchall_7
    move-exception v0

    move/from16 v31, v15

    :goto_a
    const/4 v3, 0x0

    const/4 v6, 0x1

    goto/16 :goto_19

    :catch_6
    move-exception v0

    move-wide/from16 v34, v2

    move/from16 v31, v15

    :goto_b
    const/4 v3, 0x0

    const/4 v6, 0x1

    goto :goto_11

    :catch_7
    move-exception v0

    move/from16 v31, v15

    :goto_c
    const/4 v3, 0x0

    const/4 v6, 0x1

    goto/16 :goto_17

    :cond_1b
    move-wide/from16 v34, v2

    move/from16 v31, v15

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 225
    :try_start_14
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v2, 0x412

    new-instance v4, Ljava/io/IOException;

    const-string v5, "inputStream is null"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2, v4}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw v0

    :catch_8
    move-exception v0

    move-wide/from16 v34, v2

    goto :goto_f

    :catch_9
    move-exception v0

    move-wide/from16 v34, v2

    goto :goto_d

    :catch_a
    move-exception v0

    move-wide/from16 v34, v2

    move-wide/from16 v29, v6

    :goto_d
    move/from16 v31, v15

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 219
    new-instance v2, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v4, 0x41e

    invoke-direct {v2, v4, v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw v2
    :try_end_14
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_14 .. :try_end_14} :catch_c
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_b
    .catchall {:try_start_14 .. :try_end_14} :catchall_8

    :catchall_8
    move-exception v0

    goto :goto_e

    :catch_b
    move-exception v0

    goto :goto_10

    :catch_c
    move-exception v0

    goto/16 :goto_16

    :catchall_9
    move-exception v0

    move/from16 v31, v15

    const/4 v3, 0x0

    const/4 v6, 0x1

    :goto_e
    const/4 v10, 0x0

    goto/16 :goto_19

    :catch_d
    move-exception v0

    move-wide/from16 v34, v2

    move-wide/from16 v29, v6

    :goto_f
    move/from16 v31, v15

    const/4 v3, 0x0

    const/4 v6, 0x1

    :goto_10
    const/4 v10, 0x0

    :goto_11
    if-eqz v11, :cond_1c

    .line 335
    :try_start_15
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    check-cast v2, Lcom/ss/android/socialbase/downloader/network/a;

    invoke-virtual {v2, v0}, Lcom/ss/android/socialbase/downloader/network/a;->a(Ljava/lang/Throwable;)V

    .line 337
    :cond_1c
    sget-object v2, Lcom/ss/android/socialbase/downloader/downloader/d;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleResponse: e = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->f()Z

    move-result v2
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_10

    if-eqz v2, :cond_22

    .line 351
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v0, :cond_1d

    .line 352
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->d()V

    :cond_1d
    if-eqz v10, :cond_1e

    .line 355
    invoke-interface {v10}, Lcom/ss/android/socialbase/downloader/e/b;->b()V

    .line 359
    :cond_1e
    :try_start_16
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->s:Z

    if-eqz v0, :cond_20

    .line 360
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    monitor-enter v2
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_b

    .line 361
    :try_start_17
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    if-nez v0, :cond_1f

    .line 363
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 364
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_1f

    .line 365
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V

    .line 368
    :cond_1f
    monitor-exit v2

    goto :goto_12

    :catchall_a
    move-exception v0

    monitor-exit v2
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_a

    :try_start_18
    throw v0

    .line 370
    :cond_20
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 371
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_21

    .line 372
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_b

    .line 377
    :cond_21
    :goto_12
    new-array v0, v6, [Ljava/io/Closeable;

    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v2, v0, v3

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    .line 380
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, v8

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    .line 393
    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->d:Ljava/lang/String;

    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    iget-boolean v14, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->l:Z

    iget-boolean v15, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->k:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    move-object/from16 v16, v0

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    sub-long v17, v2, v4

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    move-wide/from16 v19, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->A:J

    move-wide/from16 v22, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->B:J

    move-wide/from16 v24, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->C:J

    move-wide/from16 v26, v2

    const/16 v28, 0x0

    move/from16 v21, v31

    invoke-static/range {v10 .. v28}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/g/a;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/i;ZZLcom/ss/android/socialbase/downloader/exception/BaseException;JJZJJJLorg/json/JSONObject;)V

    return-void

    :catchall_b
    move-exception v0

    .line 377
    new-array v2, v6, [Ljava/io/Closeable;

    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    throw v0

    .line 341
    :cond_22
    :try_start_19
    invoke-static {}, Lcom/ss/android/socialbase/downloader/c/a;->a()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 342
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_10

    :cond_23
    :try_start_1a
    const-string v2, "ResponseHandler"

    .line 345
    invoke-static {v0, v2}, Lcom/ss/android/socialbase/downloader/i/e;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1a
    .catch Lcom/ss/android/socialbase/downloader/exception/BaseException; {:try_start_1a .. :try_end_1a} :catch_e
    .catchall {:try_start_1a .. :try_end_1a} :catchall_10

    .line 351
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v0, :cond_24

    .line 352
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->d()V

    :cond_24
    if-eqz v10, :cond_25

    .line 355
    invoke-interface {v10}, Lcom/ss/android/socialbase/downloader/e/b;->b()V

    .line 359
    :cond_25
    :try_start_1b
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->s:Z

    if-eqz v0, :cond_27

    .line 360
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    monitor-enter v2
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_d

    .line 361
    :try_start_1c
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    if-nez v0, :cond_26

    .line 363
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 364
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_26

    .line 365
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V

    .line 368
    :cond_26
    monitor-exit v2

    goto :goto_13

    :catchall_c
    move-exception v0

    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_c

    :try_start_1d
    throw v0

    .line 370
    :cond_27
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 371
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_28

    .line 372
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_d

    .line 377
    :cond_28
    :goto_13
    new-array v0, v6, [Ljava/io/Closeable;

    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v2, v0, v3

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    .line 380
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v8

    iput-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    .line 393
    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->d:Ljava/lang/String;

    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    iget-boolean v14, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->l:Z

    :goto_14
    iget-boolean v15, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->k:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    move-object/from16 v16, v0

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iget-wide v7, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    sub-long v17, v4, v7

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    move-wide/from16 v19, v4

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->A:J

    move-wide/from16 v22, v4

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->B:J

    move-wide/from16 v24, v4

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->C:J

    move-wide/from16 v26, v4

    const/16 v28, 0x0

    move/from16 v21, v31

    invoke-static/range {v10 .. v28}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/g/a;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/i;ZZLcom/ss/android/socialbase/downloader/exception/BaseException;JJZJJJLorg/json/JSONObject;)V

    .line 398
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isIgnoreDataVerify()Z

    move-result v0

    if-eqz v0, :cond_29

    return-void

    .line 401
    :cond_29
    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iget-wide v7, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    sub-long/2addr v4, v7

    const-wide/16 v7, 0x0

    cmp-long v0, v4, v7

    if-ltz v0, :cond_2b

    .line 404
    iget-wide v9, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    cmp-long v0, v9, v7

    if-ltz v0, :cond_2b

    iget-wide v7, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    cmp-long v0, v7, v4

    if-nez v0, :cond_2a

    goto :goto_15

    .line 405
    :cond_2a
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v2, 0x41b

    const/4 v7, 0x7

    new-array v7, v7, [Ljava/lang/Object;

    .line 407
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v7, v6

    const/4 v3, 0x2

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->r:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x3

    .line 408
    invoke-static/range {v29 .. v30}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x4

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->q:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x5

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    const/4 v3, 0x6

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    const-string v3, "handle data length[%d] != content length[%d] downloadChunkContentLen[%d], range[%d, %d) , current offset[%d] , handle start from %d"

    .line 405
    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_2b
    :goto_15
    return-void

    :catchall_d
    move-exception v0

    .line 377
    new-array v2, v6, [Ljava/io/Closeable;

    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    throw v0

    :catch_e
    move-exception v0

    .line 347
    :try_start_1e
    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->k:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 348
    throw v0

    :catch_f
    move-exception v0

    move/from16 v31, v15

    const/4 v3, 0x0

    const/4 v6, 0x1

    :goto_16
    const/4 v10, 0x0

    .line 322
    :goto_17
    sget-object v2, Lcom/ss/android/socialbase/downloader/downloader/d;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleResponse: BaseException e = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/ss/android/socialbase/downloader/c/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    const-string v4, "ignore_base_ex_on_stop_status"

    invoke-virtual {v2, v4}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 324
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->f()Z

    move-result v2
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_10

    if-eqz v2, :cond_31

    .line 351
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v0, :cond_2c

    .line 352
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/network/i;->d()V

    :cond_2c
    if-eqz v10, :cond_2d

    .line 355
    invoke-interface {v10}, Lcom/ss/android/socialbase/downloader/e/b;->b()V

    .line 359
    :cond_2d
    :try_start_1f
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->s:Z

    if-eqz v0, :cond_2f

    .line 360
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    monitor-enter v2
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_f

    .line 361
    :try_start_20
    iget-boolean v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    if-nez v0, :cond_2e

    .line 363
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 364
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_2e

    .line 365
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V

    .line 368
    :cond_2e
    monitor-exit v2

    goto :goto_18

    :catchall_e
    move-exception v0

    monitor-exit v2
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_e

    :try_start_21
    throw v0

    .line 370
    :cond_2f
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v0}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 371
    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v0, :cond_30

    .line 372
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_f

    .line 377
    :cond_30
    :goto_18
    new-array v0, v6, [Ljava/io/Closeable;

    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v2, v0, v3

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    .line 380
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, v8

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    .line 393
    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->d:Ljava/lang/String;

    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    iget-boolean v14, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->l:Z

    iget-boolean v15, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    iget-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->k:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    move-object/from16 v16, v0

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    sub-long v17, v2, v4

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    move-wide/from16 v19, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->A:J

    move-wide/from16 v22, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->B:J

    move-wide/from16 v24, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->C:J

    move-wide/from16 v26, v2

    const/16 v28, 0x0

    move/from16 v21, v31

    invoke-static/range {v10 .. v28}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/g/a;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/i;ZZLcom/ss/android/socialbase/downloader/exception/BaseException;JJZJJJLorg/json/JSONObject;)V

    return-void

    :catchall_f
    move-exception v0

    .line 377
    new-array v2, v6, [Ljava/io/Closeable;

    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    throw v0

    .line 328
    :cond_31
    :try_start_22
    invoke-static {}, Lcom/ss/android/socialbase/downloader/c/a;->a()Z

    move-result v2

    if-eqz v2, :cond_32

    .line 329
    invoke-virtual {v0}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->printStackTrace()V

    .line 331
    :cond_32
    iput-object v0, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->k:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    .line 332
    throw v0
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_10

    :catchall_10
    move-exception v0

    .line 351
    :goto_19
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    if-eqz v2, :cond_33

    .line 352
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    invoke-interface {v2}, Lcom/ss/android/socialbase/downloader/network/i;->d()V

    :cond_33
    if-eqz v10, :cond_34

    .line 355
    invoke-interface {v10}, Lcom/ss/android/socialbase/downloader/e/b;->b()V

    .line 359
    :cond_34
    :try_start_23
    iget-boolean v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->s:Z

    if-eqz v2, :cond_36

    .line 360
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->n:Lcom/ss/android/socialbase/downloader/h/f;

    monitor-enter v2
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_12

    .line 361
    :try_start_24
    iget-boolean v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    if-nez v4, :cond_35

    .line 363
    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v4}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 364
    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v4, :cond_35

    .line 365
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V

    .line 368
    :cond_35
    monitor-exit v2

    goto :goto_1a

    :catchall_11
    move-exception v0

    monitor-exit v2
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_11

    :try_start_25
    throw v0

    .line 370
    :cond_36
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->h:Lcom/ss/android/socialbase/downloader/impls/k;

    invoke-direct {v1, v2}, Lcom/ss/android/socialbase/downloader/downloader/d;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 371
    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    if-eqz v2, :cond_37

    .line 372
    invoke-direct/range {p0 .. p0}, Lcom/ss/android/socialbase/downloader/downloader/d;->h()V
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_12

    .line 377
    :cond_37
    :goto_1a
    new-array v2, v6, [Ljava/io/Closeable;

    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    .line 380
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, v8

    iput-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    .line 393
    iget-object v10, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->t:Lcom/ss/android/socialbase/downloader/g/a;

    iget-object v11, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->c:Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    iget-object v12, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->d:Ljava/lang/String;

    iget-object v13, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->f:Lcom/ss/android/socialbase/downloader/network/i;

    iget-boolean v14, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->l:Z

    iget-boolean v15, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->m:Z

    iget-object v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->k:Lcom/ss/android/socialbase/downloader/exception/BaseException;

    move-object/from16 v16, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->o:J

    iget-wide v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->p:J

    sub-long v17, v2, v4

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->D:J

    move-wide/from16 v19, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->A:J

    move-wide/from16 v22, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->B:J

    move-wide/from16 v24, v2

    iget-wide v2, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->C:J

    move-wide/from16 v26, v2

    const/16 v28, 0x0

    move/from16 v21, v31

    invoke-static/range {v10 .. v28}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/g/a;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Ljava/lang/String;Lcom/ss/android/socialbase/downloader/network/i;ZZLcom/ss/android/socialbase/downloader/exception/BaseException;JJZJJJLorg/json/JSONObject;)V

    throw v0

    :catchall_12
    move-exception v0

    .line 377
    new-array v2, v6, [Ljava/io/Closeable;

    iget-object v4, v1, Lcom/ss/android/socialbase/downloader/downloader/d;->j:Lcom/ss/android/socialbase/downloader/model/e;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    throw v0

    .line 200
    :cond_38
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/f;

    const/16 v2, 0x3ec

    const-string v3, "the content-length is 0"

    invoke-direct {v0, v2, v3}, Lcom/ss/android/socialbase/downloader/exception/f;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_39
    :goto_1b
    return-void
.end method

.method public e()J
    .locals 2

    .line 464
    iget-wide v0, p0, Lcom/ss/android/socialbase/downloader/downloader/d;->E:J

    return-wide v0
.end method
