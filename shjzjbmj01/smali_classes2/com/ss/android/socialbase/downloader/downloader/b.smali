.class public Lcom/ss/android/socialbase/downloader/downloader/b;
.super Ljava/lang/Object;
.source "DownloadComponentManager.java"


# static fields
.field private static volatile A:Lcom/ss/android/socialbase/downloader/depend/ac;

.field private static volatile B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/depend/al;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile C:Z

.field private static volatile D:Lokhttp3/OkHttpClient;

.field private static volatile E:Lcom/ss/android/socialbase/downloader/network/f;

.field private static volatile F:Lcom/ss/android/socialbase/downloader/network/f;

.field private static final G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/depend/m;",
            ">;"
        }
    .end annotation
.end field

.field private static H:Z

.field private static I:I

.field private static final J:I

.field private static final K:I

.field private static final L:I

.field private static final M:I

.field private static N:I

.field private static O:Z

.field private static final P:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/depend/k;",
            ">;"
        }
    .end annotation
.end field

.field private static final Q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/depend/ad;",
            ">;"
        }
    .end annotation
.end field

.field private static R:I

.field private static S:Z

.field private static T:Z

.field private static U:Lcom/ss/android/socialbase/downloader/downloader/p;

.field private static V:Lcom/ss/android/socialbase/downloader/d/c;

.field private static volatile W:Z

.field private static volatile a:Landroid/content/Context;

.field private static volatile b:Lcom/ss/android/socialbase/downloader/downloader/i;

.field private static volatile c:Lcom/ss/android/socialbase/downloader/downloader/j;

.field private static volatile d:Lcom/ss/android/socialbase/downloader/downloader/g;

.field private static volatile e:Lcom/ss/android/socialbase/downloader/depend/ah;

.field private static volatile f:Lcom/ss/android/socialbase/downloader/impls/a;

.field private static volatile g:Lcom/ss/android/socialbase/downloader/downloader/n;

.field private static volatile h:Lcom/ss/android/socialbase/downloader/downloader/n;

.field private static volatile i:Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

.field private static volatile j:Lcom/ss/android/socialbase/downloader/network/h;

.field private static volatile k:Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

.field private static volatile l:Lcom/ss/android/socialbase/downloader/network/h;

.field private static volatile m:Lcom/ss/android/socialbase/downloader/downloader/k;

.field private static volatile n:Ljava/util/concurrent/ExecutorService;

.field private static volatile o:Ljava/util/concurrent/ExecutorService;

.field private static volatile p:Ljava/util/concurrent/ExecutorService;

.field private static volatile q:Ljava/util/concurrent/ExecutorService;

.field private static volatile r:Ljava/util/concurrent/ExecutorService;

.field private static volatile s:Ljava/util/concurrent/ExecutorService;

.field private static volatile t:Ljava/util/concurrent/ExecutorService;

.field private static volatile u:Ljava/util/concurrent/ExecutorService;

.field private static volatile v:Lcom/ss/android/socialbase/downloader/downloader/f;

.field private static volatile w:Lcom/ss/android/socialbase/downloader/downloader/DownloadReceiver;

.field private static volatile x:Lcom/ss/android/socialbase/downloader/downloader/q;

.field private static volatile y:Lcom/ss/android/socialbase/downloader/downloader/o;

.field private static volatile z:Lcom/ss/android/socialbase/downloader/d/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->B:Ljava/util/List;

    const/4 v0, 0x0

    .line 107
    sput-boolean v0, Lcom/ss/android/socialbase/downloader/downloader/b;->C:Z

    const/4 v1, 0x0

    .line 108
    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->D:Lokhttp3/OkHttpClient;

    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->G:Ljava/util/List;

    .line 115
    sput-boolean v0, Lcom/ss/android/socialbase/downloader/downloader/b;->H:Z

    .line 117
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    const/4 v2, 0x1

    add-int/2addr v1, v2

    sput v1, Lcom/ss/android/socialbase/downloader/downloader/b;->J:I

    .line 118
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    sput v1, Lcom/ss/android/socialbase/downloader/downloader/b;->K:I

    .line 119
    sget v1, Lcom/ss/android/socialbase/downloader/downloader/b;->J:I

    sput v1, Lcom/ss/android/socialbase/downloader/downloader/b;->L:I

    .line 120
    sget v1, Lcom/ss/android/socialbase/downloader/downloader/b;->J:I

    sput v1, Lcom/ss/android/socialbase/downloader/downloader/b;->M:I

    const/16 v1, 0x2000

    .line 121
    sput v1, Lcom/ss/android/socialbase/downloader/downloader/b;->N:I

    .line 123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->Q:Ljava/util/List;

    .line 127
    sput-boolean v2, Lcom/ss/android/socialbase/downloader/downloader/b;->S:Z

    .line 128
    sput-boolean v0, Lcom/ss/android/socialbase/downloader/downloader/b;->T:Z

    .line 143
    sput-boolean v0, Lcom/ss/android/socialbase/downloader/downloader/b;->W:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static A()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/depend/m;",
            ">;"
        }
    .end annotation

    .line 950
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->G:Ljava/util/List;

    return-object v0
.end method

.method public static B()Lcom/ss/android/socialbase/downloader/downloader/j;
    .locals 2

    .line 954
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->c:Lcom/ss/android/socialbase/downloader/downloader/j;

    if-nez v0, :cond_1

    .line 955
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 956
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->c:Lcom/ss/android/socialbase/downloader/downloader/j;

    if-nez v1, :cond_0

    .line 957
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/i;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/i;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->c:Lcom/ss/android/socialbase/downloader/downloader/j;

    .line 958
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 960
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->c:Lcom/ss/android/socialbase/downloader/downloader/j;

    return-object v0
.end method

.method public static C()Lcom/ss/android/socialbase/downloader/impls/a;
    .locals 2

    .line 964
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->f:Lcom/ss/android/socialbase/downloader/impls/a;

    if-nez v0, :cond_1

    .line 965
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 966
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->f:Lcom/ss/android/socialbase/downloader/impls/a;

    if-nez v1, :cond_0

    .line 967
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/e;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/e;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->f:Lcom/ss/android/socialbase/downloader/impls/a;

    .line 968
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 970
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->f:Lcom/ss/android/socialbase/downloader/impls/a;

    return-object v0
.end method

.method public static D()I
    .locals 1

    .line 996
    sget v0, Lcom/ss/android/socialbase/downloader/downloader/b;->R:I

    return v0
.end method

.method public static E()Lorg/json/JSONObject;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1001
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->A:Lcom/ss/android/socialbase/downloader/depend/ac;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->A:Lcom/ss/android/socialbase/downloader/depend/ac;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/depend/ac;->a()Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1004
    :cond_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->A:Lcom/ss/android/socialbase/downloader/depend/ac;

    invoke-interface {v0}, Lcom/ss/android/socialbase/downloader/depend/ac;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 1005
    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lorg/json/JSONObject;)V

    return-object v0

    .line 1002
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/e;->i:Lorg/json/JSONObject;

    return-object v0
.end method

.method public static F()Z
    .locals 1

    .line 1041
    sget-boolean v0, Lcom/ss/android/socialbase/downloader/downloader/b;->S:Z

    return v0
.end method

.method public static declared-synchronized G()I
    .locals 2

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 1046
    :try_start_0
    sget v1, Lcom/ss/android/socialbase/downloader/downloader/b;->N:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static H()Lcom/ss/android/socialbase/downloader/downloader/g;
    .locals 2

    .line 1050
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->d:Lcom/ss/android/socialbase/downloader/downloader/g;

    if-nez v0, :cond_1

    .line 1051
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 1052
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->d:Lcom/ss/android/socialbase/downloader/downloader/g;

    if-nez v1, :cond_0

    .line 1053
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/c;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/c;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->d:Lcom/ss/android/socialbase/downloader/downloader/g;

    .line 1054
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1056
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->d:Lcom/ss/android/socialbase/downloader/downloader/g;

    return-object v0
.end method

.method public static I()Lcom/ss/android/socialbase/downloader/depend/ah;
    .locals 1

    .line 1071
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->e:Lcom/ss/android/socialbase/downloader/depend/ah;

    return-object v0
.end method

.method public static J()Lcom/ss/android/socialbase/downloader/downloader/f;
    .locals 2

    .line 1075
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->v:Lcom/ss/android/socialbase/downloader/downloader/f;

    if-nez v0, :cond_1

    .line 1076
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 1077
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->v:Lcom/ss/android/socialbase/downloader/downloader/f;

    if-nez v1, :cond_0

    .line 1078
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/b;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/b;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->v:Lcom/ss/android/socialbase/downloader/downloader/f;

    .line 1079
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1081
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->v:Lcom/ss/android/socialbase/downloader/downloader/f;

    return-object v0
.end method

.method public static K()Lcom/ss/android/socialbase/downloader/downloader/q;
    .locals 2

    .line 1090
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->x:Lcom/ss/android/socialbase/downloader/downloader/q;

    if-nez v0, :cond_1

    .line 1091
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 1092
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->x:Lcom/ss/android/socialbase/downloader/downloader/q;

    if-nez v1, :cond_0

    .line 1093
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/j;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/j;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->x:Lcom/ss/android/socialbase/downloader/downloader/q;

    .line 1094
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1096
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->x:Lcom/ss/android/socialbase/downloader/downloader/q;

    return-object v0
.end method

.method public static declared-synchronized L()Landroid/content/Context;
    .locals 2

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 1100
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->a:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized M()Z
    .locals 2

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 1125
    :try_start_0
    sget-boolean v1, Lcom/ss/android/socialbase/downloader/downloader/b;->O:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static N()Lcom/ss/android/socialbase/downloader/d/c;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1130
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->V:Lcom/ss/android/socialbase/downloader/d/c;

    if-nez v0, :cond_0

    .line 1131
    new-instance v0, Lcom/ss/android/socialbase/downloader/downloader/b$3;

    invoke-direct {v0}, Lcom/ss/android/socialbase/downloader/downloader/b$3;-><init>()V

    sput-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->V:Lcom/ss/android/socialbase/downloader/d/c;

    .line 1143
    :cond_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->V:Lcom/ss/android/socialbase/downloader/d/c;

    return-object v0
.end method

.method public static O()Lcom/ss/android/socialbase/downloader/downloader/p;
    .locals 1

    .line 1146
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->U:Lcom/ss/android/socialbase/downloader/downloader/p;

    return-object v0
.end method

.method public static P()Z
    .locals 1

    .line 1159
    sget-boolean v0, Lcom/ss/android/socialbase/downloader/downloader/b;->W:Z

    return v0
.end method

.method private static Q()V
    .locals 3

    .line 269
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->w:Lcom/ss/android/socialbase/downloader/downloader/DownloadReceiver;

    if-nez v0, :cond_0

    .line 270
    new-instance v0, Lcom/ss/android/socialbase/downloader/downloader/DownloadReceiver;

    invoke-direct {v0}, Lcom/ss/android/socialbase/downloader/downloader/DownloadReceiver;-><init>()V

    sput-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->w:Lcom/ss/android/socialbase/downloader/downloader/DownloadReceiver;

    .line 273
    :cond_0
    sget-boolean v0, Lcom/ss/android/socialbase/downloader/downloader/b;->H:Z

    if-eqz v0, :cond_1

    return-void

    .line 277
    :cond_1
    :try_start_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    .line 278
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 279
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->a:Landroid/content/Context;

    sget-object v2, Lcom/ss/android/socialbase/downloader/downloader/b;->w:Lcom/ss/android/socialbase/downloader/downloader/DownloadReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    .line 280
    sput-boolean v0, Lcom/ss/android/socialbase/downloader/downloader/b;->H:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 282
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)I
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 1114
    :cond_0
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getSavePath()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 1118
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->B()Lcom/ss/android/socialbase/downloader/downloader/j;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 1121
    :cond_0
    invoke-interface {v0, p0, p1}, Lcom/ss/android/socialbase/downloader/downloader/j;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)Lcom/ss/android/socialbase/downloader/network/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;)",
            "Lcom/ss/android/socialbase/downloader/network/g;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 516
    invoke-static {p0, p1, v0, v0, v1}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/g;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/g;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;IZ",
            "Lcom/ss/android/socialbase/downloader/model/DownloadInfo;",
            ")",
            "Lcom/ss/android/socialbase/downloader/network/g;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 525
    invoke-static {p2}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(I)[I

    move-result-object p2

    .line 526
    array-length v0, p2

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v1

    :goto_0
    if-ge v2, v0, :cond_1

    aget v4, p2, v2

    .line 528
    :try_start_0
    invoke-static {p0, p1, v4, p3, p4}, Lcom/ss/android/socialbase/downloader/downloader/b;->b(Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/g;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_0

    return-object v4

    :catch_0
    move-exception v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-nez v3, :cond_2

    return-object v1

    .line 537
    :cond_2
    throw v3
.end method

.method private static a(ILjava/lang/String;Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/i;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;IZ",
            "Lcom/ss/android/socialbase/downloader/model/DownloadInfo;",
            ")",
            "Lcom/ss/android/socialbase/downloader/network/i;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Ljava/io/IOException;
        }
    .end annotation

    move v7, p4

    const/4 v0, 0x1

    if-ne v7, v0, :cond_0

    .line 490
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->d()Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->h()Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_4

    const/4 v2, 0x0

    const/4 v8, 0x0

    const-wide/16 v3, 0x0

    if-eqz p5, :cond_1

    .line 500
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v3, p0

    move-object v4, p1

    move-wide v9, v5

    move-object v5, p3

    goto :goto_2

    :catchall_0
    move-exception v0

    move-wide v9, v3

    move-object v4, p1

    goto :goto_4

    :catch_0
    move-exception v0

    move-wide v9, v3

    move-object v4, p1

    :goto_1
    move-object v3, v0

    goto :goto_3

    :cond_1
    move-object v5, p3

    move-wide v9, v3

    move v3, p0

    move-object v4, p1

    .line 502
    :goto_2
    :try_start_1
    invoke-interface {v0, p0, p1, p3}, Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;->downloadWithConnection(ILjava/lang/String;Ljava/util/List;)Lcom/ss/android/socialbase/downloader/network/i;

    move-result-object v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p5, :cond_2

    .line 509
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sub-long v5, v1, v9

    const-string v9, "get"

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, v5

    move-object v6, v9

    move v7, p4

    move-object/from16 v9, p6

    invoke-static/range {v1 .. v9}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/network/g;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ILjava/io/IOException;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    :cond_2
    return-object v0

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_1

    .line 506
    :goto_3
    :try_start_2
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :catchall_2
    move-exception v0

    move-object v8, v3

    :goto_4
    if-eqz p5, :cond_3

    .line 509
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v9

    const-string v9, "get"

    move-object v1, v2

    move-object v2, p1

    move-object v3, p2

    move-wide v4, v5

    move-object v6, v9

    move v7, p4

    move-object/from16 v9, p6

    invoke-static/range {v1 .. v9}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/network/g;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ILjava/io/IOException;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    :cond_3
    throw v0

    .line 492
    :cond_4
    new-instance v0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 v1, 0x3fe

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "httpService not exist, netLib = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw v0
.end method

.method public static a(ZILjava/lang/String;Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/i;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;IZ",
            "Lcom/ss/android/socialbase/downloader/model/DownloadInfo;",
            ")",
            "Lcom/ss/android/socialbase/downloader/network/i;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 450
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p4, :cond_0

    .line 452
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_0
    move-object/from16 v0, p4

    .line 454
    :goto_0
    new-instance v1, Lcom/ss/android/socialbase/downloader/model/c;

    const-string v2, "ss_d_request_host_ip_114"

    move-object/from16 v10, p3

    invoke-direct {v1, v2, v10}, Lcom/ss/android/socialbase/downloader/model/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    move-object v2, v0

    goto :goto_1

    :cond_1
    move-object/from16 v10, p3

    if-nez p0, :cond_2

    const/4 v1, 0x2

    move-object/from16 v2, p4

    goto :goto_1

    :cond_2
    move-object/from16 v2, p4

    move/from16 v1, p5

    .line 470
    :goto_1
    invoke-static {v1}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(I)[I

    move-result-object v1

    .line 471
    array-length v11, v1

    const/4 v0, 0x0

    const/4 v12, 0x0

    move-object v0, v12

    const/4 v13, 0x0

    :goto_2
    if-ge v13, v11, :cond_4

    aget v7, v1, v13

    move v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object v6, v2

    move/from16 v8, p6

    move-object/from16 v9, p7

    .line 473
    :try_start_0
    invoke-static/range {v3 .. v9}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/i;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_3

    return-object v3

    :catch_0
    move-exception v0

    :cond_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    :cond_4
    if-nez v0, :cond_5

    return-object v12

    .line 482
    :cond_5
    throw v0
.end method

.method public static a(ZILjava/lang/String;Ljava/util/List;)Lcom/ss/android/socialbase/downloader/network/i;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;)",
            "Lcom/ss/android/socialbase/downloader/network/i;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, p3

    .line 442
    invoke-static/range {v0 .. v7}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(ZILjava/lang/String;Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/i;

    move-result-object p0

    return-object p0
.end method

.method static declared-synchronized a()V
    .locals 3

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 289
    :try_start_0
    sget-boolean v1, Lcom/ss/android/socialbase/downloader/downloader/b;->H:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->w:Lcom/ss/android/socialbase/downloader/downloader/DownloadReceiver;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 290
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->a:Landroid/content/Context;

    sget-object v2, Lcom/ss/android/socialbase/downloader/downloader/b;->w:Lcom/ss/android/socialbase/downloader/downloader/DownloadReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v1, 0x0

    .line 291
    sput-boolean v1, Lcom/ss/android/socialbase/downloader/downloader/b;->H:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    .line 294
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296
    :cond_0
    :goto_0
    monitor-exit v0

    return-void

    .line 288
    :goto_1
    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    if-eqz p0, :cond_0

    .line 1104
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->a:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 1105
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->a:Landroid/content/Context;

    .line 1107
    invoke-static {}, Lcom/ss/android/socialbase/downloader/a/a;->a()Lcom/ss/android/socialbase/downloader/a/a;

    move-result-object p0

    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->a:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/ss/android/socialbase/downloader/a/a;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 1103
    monitor-exit v0

    throw p0

    .line 1109
    :cond_0
    :goto_0
    monitor-exit v0

    return-void
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/constants/d;)V
    .locals 4

    .line 391
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    monitor-enter v0

    .line 392
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 393
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 394
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/depend/k;

    if-eqz v2, :cond_0

    .line 396
    sget-object v3, Lcom/ss/android/socialbase/downloader/constants/d;->b:Lcom/ss/android/socialbase/downloader/constants/d;

    if-ne p0, v3, :cond_1

    .line 397
    invoke-interface {v2}, Lcom/ss/android/socialbase/downloader/depend/k;->a()V

    goto :goto_0

    .line 398
    :cond_1
    sget-object v3, Lcom/ss/android/socialbase/downloader/constants/d;->c:Lcom/ss/android/socialbase/downloader/constants/d;

    if-ne p0, v3, :cond_0

    .line 399
    invoke-interface {v2}, Lcom/ss/android/socialbase/downloader/depend/k;->b()V

    goto :goto_0

    .line 402
    :cond_2
    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/d;->c:Lcom/ss/android/socialbase/downloader/constants/d;

    if-ne p0, v1, :cond_3

    .line 403
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 405
    :cond_3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method private static a(Lcom/ss/android/socialbase/downloader/d/b;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 991
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->z:Lcom/ss/android/socialbase/downloader/d/b;

    :cond_0
    return-void
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/d/c;)V
    .locals 0

    .line 1150
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->V:Lcom/ss/android/socialbase/downloader/d/c;

    return-void
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/depend/ac;)V
    .locals 0

    .line 1023
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->A:Lcom/ss/android/socialbase/downloader/depend/ac;

    .line 1025
    invoke-static {}, Lcom/ss/android/socialbase/downloader/g/a;->a()V

    .line 1026
    invoke-interface {p0}, Lcom/ss/android/socialbase/downloader/depend/ac;->a()Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lorg/json/JSONObject;)V

    return-void
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/depend/ah;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 1066
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->e:Lcom/ss/android/socialbase/downloader/depend/ah;

    :cond_0
    return-void
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/depend/al;)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    .line 331
    :cond_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->B:Ljava/util/List;

    monitor-enter v0

    .line 332
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->B:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/depend/k;)V
    .locals 2

    .line 359
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    monitor-enter v0

    if-eqz p0, :cond_1

    .line 360
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 362
    :cond_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    goto :goto_1

    .line 361
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    .line 363
    :goto_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method static declared-synchronized a(Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;)V
    .locals 3

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 149
    :try_start_0
    sget-boolean v1, Lcom/ss/android/socialbase/downloader/downloader/b;->W:Z

    if-eqz v1, :cond_0

    const-string p0, "DownloadComponentManager"

    const-string v1, "component has init"

    .line 150
    invoke-static {p0, v1}, Lcom/ss/android/socialbase/downloader/c/a;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    monitor-exit v0

    return-void

    .line 154
    :cond_0
    :try_start_1
    sget-boolean v1, Lcom/ss/android/socialbase/downloader/downloader/b;->C:Z

    .line 155
    invoke-static {p0}, Lcom/ss/android/socialbase/downloader/downloader/b;->c(Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;)V

    .line 157
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->b:Lcom/ss/android/socialbase/downloader/downloader/i;

    if-nez p0, :cond_1

    .line 158
    new-instance p0, Lcom/ss/android/socialbase/downloader/impls/d;

    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/impls/d;-><init>()V

    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->b:Lcom/ss/android/socialbase/downloader/downloader/i;

    .line 159
    :cond_1
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->g:Lcom/ss/android/socialbase/downloader/downloader/n;

    if-nez p0, :cond_2

    .line 160
    new-instance p0, Lcom/ss/android/socialbase/downloader/impls/h;

    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/impls/h;-><init>()V

    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->g:Lcom/ss/android/socialbase/downloader/downloader/n;

    .line 161
    :cond_2
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->h:Lcom/ss/android/socialbase/downloader/downloader/n;

    if-nez p0, :cond_3

    .line 162
    new-instance p0, Lcom/ss/android/socialbase/downloader/impls/n;

    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/impls/n;-><init>()V

    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->h:Lcom/ss/android/socialbase/downloader/downloader/n;

    .line 163
    :cond_3
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->c:Lcom/ss/android/socialbase/downloader/downloader/j;

    if-nez p0, :cond_4

    .line 164
    new-instance p0, Lcom/ss/android/socialbase/downloader/impls/i;

    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/impls/i;-><init>()V

    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->c:Lcom/ss/android/socialbase/downloader/downloader/j;

    .line 165
    :cond_4
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->f:Lcom/ss/android/socialbase/downloader/impls/a;

    if-nez p0, :cond_5

    .line 166
    new-instance p0, Lcom/ss/android/socialbase/downloader/impls/e;

    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/impls/e;-><init>()V

    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->f:Lcom/ss/android/socialbase/downloader/impls/a;

    .line 167
    :cond_5
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->d:Lcom/ss/android/socialbase/downloader/downloader/g;

    if-nez p0, :cond_6

    .line 168
    new-instance p0, Lcom/ss/android/socialbase/downloader/impls/c;

    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/impls/c;-><init>()V

    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->d:Lcom/ss/android/socialbase/downloader/downloader/g;

    .line 169
    :cond_6
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->v:Lcom/ss/android/socialbase/downloader/downloader/f;

    if-nez p0, :cond_7

    .line 170
    new-instance p0, Lcom/ss/android/socialbase/downloader/impls/b;

    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/impls/b;-><init>()V

    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->v:Lcom/ss/android/socialbase/downloader/downloader/f;

    .line 171
    :cond_7
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->x:Lcom/ss/android/socialbase/downloader/downloader/q;

    if-nez p0, :cond_8

    .line 172
    new-instance p0, Lcom/ss/android/socialbase/downloader/impls/j;

    invoke-direct {p0}, Lcom/ss/android/socialbase/downloader/impls/j;-><init>()V

    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->x:Lcom/ss/android/socialbase/downloader/downloader/q;

    .line 173
    :cond_8
    sget p0, Lcom/ss/android/socialbase/downloader/downloader/b;->I:I

    if-lez p0, :cond_9

    sget p0, Lcom/ss/android/socialbase/downloader/downloader/b;->I:I

    sget v2, Lcom/ss/android/socialbase/downloader/downloader/b;->J:I

    if-le p0, v2, :cond_a

    .line 174
    :cond_9
    sget p0, Lcom/ss/android/socialbase/downloader/downloader/b;->J:I

    sput p0, Lcom/ss/android/socialbase/downloader/downloader/b;->I:I

    .line 176
    :cond_a
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->Q()V

    .line 177
    sget-boolean p0, Lcom/ss/android/socialbase/downloader/downloader/b;->C:Z

    const/4 v2, 0x1

    if-eqz p0, :cond_b

    if-nez v1, :cond_b

    invoke-static {}, Lcom/ss/android/socialbase/downloader/i/e;->c()Z

    move-result p0

    if-nez p0, :cond_b

    .line 178
    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/impls/l;->a(Z)Lcom/ss/android/socialbase/downloader/downloader/l;

    move-result-object p0

    invoke-interface {p0}, Lcom/ss/android/socialbase/downloader/downloader/l;->d()V

    goto :goto_0

    .line 181
    :cond_b
    invoke-static {}, Lcom/ss/android/socialbase/downloader/i/e;->d()Z

    move-result p0

    if-eqz p0, :cond_c

    .line 182
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->m()Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    if-eqz p0, :cond_d

    .line 184
    new-instance v1, Lcom/ss/android/socialbase/downloader/downloader/b$1;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/downloader/b$1;-><init>()V

    invoke-interface {p0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 195
    :cond_c
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->L()Landroid/content/Context;

    move-result-object p0

    if-eqz p0, :cond_d

    .line 197
    invoke-static {p0}, Lcom/ss/android/socialbase/downloader/i/e;->d(Landroid/content/Context;)Ljava/lang/String;

    .line 201
    :cond_d
    :goto_0
    sput-boolean v2, Lcom/ss/android/socialbase/downloader/downloader/b;->W:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    .line 148
    monitor-exit v0

    throw p0
.end method

.method private static a(Lcom/ss/android/socialbase/downloader/downloader/f;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 1086
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->v:Lcom/ss/android/socialbase/downloader/downloader/f;

    :cond_0
    return-void
.end method

.method private static a(Lcom/ss/android/socialbase/downloader/downloader/g;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 1061
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->d:Lcom/ss/android/socialbase/downloader/downloader/g;

    :cond_0
    return-void
.end method

.method private static a(Lcom/ss/android/socialbase/downloader/downloader/i;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 924
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->b:Lcom/ss/android/socialbase/downloader/downloader/i;

    :cond_0
    return-void
.end method

.method private static a(Lcom/ss/android/socialbase/downloader/downloader/j;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 975
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->c:Lcom/ss/android/socialbase/downloader/downloader/j;

    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Lcom/ss/android/socialbase/downloader/downloader/k;)V
    .locals 1

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    if-eqz p0, :cond_0

    .line 624
    :try_start_0
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->m:Lcom/ss/android/socialbase/downloader/downloader/k;

    .line 625
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->b:Lcom/ss/android/socialbase/downloader/downloader/i;

    instance-of p0, p0, Lcom/ss/android/socialbase/downloader/impls/d;

    if-eqz p0, :cond_0

    .line 626
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->b:Lcom/ss/android/socialbase/downloader/downloader/i;

    check-cast p0, Lcom/ss/android/socialbase/downloader/impls/d;

    .line 627
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/impls/d;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    .line 622
    monitor-exit v0

    throw p0

    .line 630
    :cond_0
    :goto_0
    monitor-exit v0

    return-void
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/downloader/p;)V
    .locals 0

    return-void
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/model/DownloadTask;I)V
    .locals 3

    .line 409
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->Q:Ljava/util/List;

    monitor-enter v0

    .line 410
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->Q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 411
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 412
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/depend/ad;

    if-eqz v2, :cond_0

    .line 414
    invoke-interface {v2, p0, p1}, Lcom/ss/android/socialbase/downloader/depend/ad;->a(Lcom/ss/android/socialbase/downloader/model/DownloadTask;I)V

    goto :goto_0

    .line 417
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 902
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->i:Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    .line 903
    :cond_0
    sget-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->i:Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    sput-boolean p0, Lcom/ss/android/socialbase/downloader/downloader/b;->O:Z

    return-void
.end method

.method public static a(Lcom/ss/android/socialbase/downloader/network/h;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 908
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->j:Lcom/ss/android/socialbase/downloader/network/h;

    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 1

    const/4 v0, 0x0

    .line 701
    invoke-static {p0, v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->b(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public static a(Ljava/lang/Runnable;Z)V
    .locals 0

    if-nez p0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 711
    invoke-static {}, Lcom/ss/android/socialbase/downloader/i/e;->d()Z

    move-result p1

    if-nez p1, :cond_1

    .line 712
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 714
    :cond_1
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    invoke-interface {p1, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/depend/m;",
            ">;)V"
        }
    .end annotation

    .line 675
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 676
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->G:Ljava/util/List;

    monitor-enter v0

    .line 677
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->G:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 678
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_0
    :goto_0
    return-void
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 664
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->t:Ljava/util/concurrent/ExecutorService;

    :cond_0
    return-void
.end method

.method private static a(Lorg/json/JSONObject;)V
    .locals 1

    if-eqz p0, :cond_0

    .line 1010
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/e;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "kllk"

    .line 1011
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/ss/android/socialbase/downloader/constants/e;->c:Ljava/lang/String;

    .line 1012
    sget-object p0, Lcom/ss/android/socialbase/downloader/constants/e;->c:Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1013
    sget-object p0, Lcom/ss/android/socialbase/downloader/constants/e;->c:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/ss/android/socialbase/downloader/constants/e;->b:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public static a(Z)V
    .locals 0

    .line 683
    sput-boolean p0, Lcom/ss/android/socialbase/downloader/downloader/b;->T:Z

    return-void
.end method

.method private static a(I)[I
    .locals 3

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch p0, :pswitch_data_0

    .line 584
    new-array p0, v0, [I

    fill-array-data p0, :array_0

    return-object p0

    .line 580
    :pswitch_0
    new-array p0, v0, [I

    fill-array-data p0, :array_1

    return-object p0

    .line 577
    :pswitch_1
    new-array p0, v2, [I

    aput v2, p0, v1

    return-object p0

    .line 574
    :pswitch_2
    new-array p0, v2, [I

    aput v1, p0, v1

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x1
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method private static b(Ljava/lang/String;Ljava/util/List;IZLcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/socialbase/downloader/network/g;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/model/c;",
            ">;IZ",
            "Lcom/ss/android/socialbase/downloader/model/DownloadInfo;",
            ")",
            "Lcom/ss/android/socialbase/downloader/network/g;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ss/android/socialbase/downloader/exception/BaseException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 545
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->f()Lcom/ss/android/socialbase/downloader/network/h;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->i()Lcom/ss/android/socialbase/downloader/network/h;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_4

    const/4 v1, 0x0

    const/4 v9, 0x0

    const-wide/16 v2, 0x0

    if-eqz p3, :cond_1

    .line 555
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-wide v2, v4

    goto :goto_1

    :catchall_0
    move-exception p1

    move-object v8, v9

    goto :goto_3

    :catch_0
    move-exception p1

    goto :goto_2

    .line 557
    :cond_1
    :goto_1
    invoke-interface {v0, p0, p1}, Lcom/ss/android/socialbase/downloader/network/h;->a(Ljava/lang/String;Ljava/util/List;)Lcom/ss/android/socialbase/downloader/network/g;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p3, :cond_2

    const/4 v4, 0x0

    .line 564
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v5, v0, v2

    const-string v7, "head"

    move-object v2, p1

    move-object v3, p0

    move v8, p2

    move-object v10, p4

    invoke-static/range {v2 .. v10}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/network/g;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ILjava/io/IOException;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    :cond_2
    return-object p1

    .line 561
    :goto_2
    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception v0

    move-object v8, p1

    move-object p1, v0

    :goto_3
    if-eqz p3, :cond_3

    const/4 p3, 0x0

    .line 564
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    const-string v6, "head"

    move-object v2, p0

    move-object v3, p3

    move v7, p2

    move-object v9, p4

    invoke-static/range {v1 .. v9}, Lcom/ss/android/socialbase/downloader/d/a;->a(Lcom/ss/android/socialbase/downloader/network/g;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ILjava/io/IOException;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    :cond_3
    throw p1

    .line 547
    :cond_4
    new-instance p0, Lcom/ss/android/socialbase/downloader/exception/BaseException;

    const/16 p1, 0x3fe

    new-instance p3, Ljava/io/IOException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "httpService not exist, netLib = "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, p3}, Lcom/ss/android/socialbase/downloader/exception/BaseException;-><init>(ILjava/lang/Throwable;)V

    throw p0
.end method

.method public static declared-synchronized b()V
    .locals 5

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 299
    :try_start_0
    sget-boolean v1, Lcom/ss/android/socialbase/downloader/downloader/b;->C:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 300
    monitor-exit v0

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 301
    :try_start_1
    sput-boolean v1, Lcom/ss/android/socialbase/downloader/downloader/b;->C:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 304
    :try_start_2
    new-instance v2, Landroid/content/Intent;

    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->L()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/ss/android/socialbase/downloader/impls/DownloadHandleService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.ss.android.downloader.action.MULTI_PROCESS_NOTIFY"

    .line 305
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->L()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 308
    invoke-static {}, Lcom/ss/android/socialbase/downloader/i/e;->c()Z

    move-result v2

    if-nez v2, :cond_1

    .line 309
    invoke-static {v1}, Lcom/ss/android/socialbase/downloader/impls/l;->a(Z)Lcom/ss/android/socialbase/downloader/downloader/l;

    move-result-object v1

    invoke-interface {v1}, Lcom/ss/android/socialbase/downloader/downloader/l;->d()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v2, 0x0

    .line 313
    :try_start_3
    sput-boolean v2, Lcom/ss/android/socialbase/downloader/downloader/b;->C:Z

    .line 314
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 318
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    .line 298
    monitor-exit v0

    throw v1
.end method

.method private static b(I)V
    .locals 0

    if-lez p0, :cond_0

    .line 986
    sput p0, Lcom/ss/android/socialbase/downloader/downloader/b;->I:I

    :cond_0
    return-void
.end method

.method public static b(Lcom/ss/android/socialbase/downloader/depend/k;)V
    .locals 2

    .line 367
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    monitor-enter v0

    if-eqz p0, :cond_1

    .line 368
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 370
    :cond_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->P:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 371
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    goto :goto_1

    .line 369
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    .line 371
    :goto_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method static declared-synchronized b(Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;)V
    .locals 1

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 205
    :try_start_0
    invoke-static {p0}, Lcom/ss/android/socialbase/downloader/downloader/b;->c(Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    .line 204
    monitor-exit v0

    throw p0
.end method

.method public static b(Lcom/ss/android/socialbase/downloader/model/DownloadTask;I)V
    .locals 3

    .line 421
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->Q:Ljava/util/List;

    monitor-enter v0

    .line 422
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->Q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 423
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 424
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/socialbase/downloader/depend/ad;

    if-eqz v2, :cond_0

    .line 426
    invoke-interface {v2, p0, p1}, Lcom/ss/android/socialbase/downloader/depend/ad;->b(Lcom/ss/android/socialbase/downloader/model/DownloadTask;I)V

    goto :goto_0

    .line 429
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static b(Ljava/lang/Runnable;)V
    .locals 1

    if-nez p0, :cond_0

    return-void

    .line 736
    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/i/e;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 737
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 739
    :cond_1
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->r()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public static b(Ljava/lang/Runnable;Z)V
    .locals 0

    if-nez p0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 725
    invoke-static {}, Lcom/ss/android/socialbase/downloader/i/e;->d()Z

    move-result p1

    if-nez p1, :cond_1

    .line 726
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 728
    :cond_1
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->m()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    invoke-interface {p1, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public static b(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 670
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->u:Ljava/util/concurrent/ExecutorService;

    :cond_0
    return-void
.end method

.method private static b(Z)V
    .locals 0

    .line 1033
    sput-boolean p0, Lcom/ss/android/socialbase/downloader/downloader/b;->S:Z

    return-void
.end method

.method private static c(Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;)V
    .locals 2

    if-eqz p0, :cond_19

    .line 210
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Landroid/content/Context;)V

    .line 212
    :cond_0
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadCache()Lcom/ss/android/socialbase/downloader/downloader/i;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 213
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadCache()Lcom/ss/android/socialbase/downloader/downloader/i;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/downloader/i;)V

    .line 214
    :cond_1
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getIdGenerator()Lcom/ss/android/socialbase/downloader/downloader/j;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 215
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getIdGenerator()Lcom/ss/android/socialbase/downloader/downloader/j;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/downloader/j;)V

    .line 216
    :cond_2
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getChunkCntCalculator()Lcom/ss/android/socialbase/downloader/downloader/g;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 217
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getChunkCntCalculator()Lcom/ss/android/socialbase/downloader/downloader/g;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/downloader/g;)V

    .line 218
    :cond_3
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getNotificationClickCallback()Lcom/ss/android/socialbase/downloader/depend/ah;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 219
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getNotificationClickCallback()Lcom/ss/android/socialbase/downloader/depend/ah;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/depend/ah;)V

    .line 220
    :cond_4
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMaxDownloadPoolSize()I

    move-result v0

    if-eqz v0, :cond_5

    .line 221
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMaxDownloadPoolSize()I

    move-result v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->b(I)V

    .line 222
    :cond_5
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getHttpService()Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 223
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getHttpService()Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;)V

    .line 224
    :cond_6
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getHeadHttpService()Lcom/ss/android/socialbase/downloader/network/h;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 225
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getHeadHttpService()Lcom/ss/android/socialbase/downloader/network/h;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/network/h;)V

    .line 226
    :cond_7
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadLaunchHandler()Lcom/ss/android/socialbase/downloader/downloader/k;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 227
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadLaunchHandler()Lcom/ss/android/socialbase/downloader/downloader/k;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/downloader/k;)V

    .line 228
    :cond_8
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getCPUThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 229
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getCPUThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->c(Ljava/util/concurrent/ExecutorService;)V

    .line 230
    :cond_9
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getIOThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 231
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getIOThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->d(Ljava/util/concurrent/ExecutorService;)V

    .line 232
    :cond_a
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMixDefaultDownloadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 233
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMixDefaultDownloadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->e(Ljava/util/concurrent/ExecutorService;)V

    .line 234
    :cond_b
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMixFrequentDownloadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 235
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMixFrequentDownloadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->f(Ljava/util/concurrent/ExecutorService;)V

    .line 236
    :cond_c
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMixApkDownloadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 237
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMixApkDownloadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->g(Ljava/util/concurrent/ExecutorService;)V

    .line 238
    :cond_d
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDBThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 239
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDBThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->h(Ljava/util/concurrent/ExecutorService;)V

    .line 240
    :cond_e
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getChunkThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 241
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getChunkThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Ljava/util/concurrent/ExecutorService;)V

    .line 242
    :cond_f
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getOkHttpDispatcherExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 243
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getOkHttpDispatcherExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->b(Ljava/util/concurrent/ExecutorService;)V

    .line 245
    :cond_10
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadCompleteHandlers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    .line 246
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadCompleteHandlers()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Ljava/util/List;)V

    .line 247
    :cond_11
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMonitorConfig()Lcom/ss/android/socialbase/downloader/downloader/o;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 248
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getMonitorConfig()Lcom/ss/android/socialbase/downloader/downloader/o;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->y:Lcom/ss/android/socialbase/downloader/downloader/o;

    .line 249
    :cond_12
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getWriteBufferSize()I

    move-result v0

    const/16 v1, 0x400

    if-le v0, v1, :cond_13

    .line 250
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getWriteBufferSize()I

    move-result v0

    sput v0, Lcom/ss/android/socialbase/downloader/downloader/b;->N:I

    .line 251
    :cond_13
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getChunkAdjustCalculator()Lcom/ss/android/socialbase/downloader/downloader/f;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 252
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getChunkAdjustCalculator()Lcom/ss/android/socialbase/downloader/downloader/f;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/downloader/f;)V

    .line 253
    :cond_14
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->isDownloadInMultiProcess()Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    .line 254
    sput-boolean v0, Lcom/ss/android/socialbase/downloader/downloader/b;->C:Z

    .line 256
    :cond_15
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadExpSwitch()I

    move-result v0

    if-eqz v0, :cond_16

    .line 257
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadExpSwitch()I

    move-result v0

    sput v0, Lcom/ss/android/socialbase/downloader/downloader/b;->R:I

    .line 258
    :cond_16
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadSetting()Lcom/ss/android/socialbase/downloader/depend/ac;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 259
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadSetting()Lcom/ss/android/socialbase/downloader/depend/ac;

    move-result-object v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/depend/ac;)V

    .line 260
    :cond_17
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadDns()Lcom/ss/android/socialbase/downloader/network/f;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 261
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadDns()Lcom/ss/android/socialbase/downloader/network/f;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->E:Lcom/ss/android/socialbase/downloader/network/f;

    .line 262
    :cond_18
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->needAutoRefreshUnSuccessTask()Z

    move-result v0

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/downloader/b;->b(Z)V

    .line 263
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadMonitorListener()Lcom/ss/android/socialbase/downloader/d/b;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 264
    invoke-virtual {p0}, Lcom/ss/android/socialbase/downloader/downloader/DownloaderBuilder;->getDownloadMonitorListener()Lcom/ss/android/socialbase/downloader/d/b;

    move-result-object p0

    invoke-static {p0}, Lcom/ss/android/socialbase/downloader/downloader/b;->a(Lcom/ss/android/socialbase/downloader/d/b;)V

    :cond_19
    return-void
.end method

.method private static c(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 634
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->n:Ljava/util/concurrent/ExecutorService;

    :cond_0
    return-void
.end method

.method public static declared-synchronized c()Z
    .locals 2

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 321
    :try_start_0
    sget-boolean v1, Lcom/ss/android/socialbase/downloader/downloader/b;->C:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static d()Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;
    .locals 1

    .line 325
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->i:Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    return-object v0
.end method

.method private static d(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 639
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->o:Ljava/util/concurrent/ExecutorService;

    :cond_0
    return-void
.end method

.method public static e()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/ss/android/socialbase/downloader/depend/al;",
            ">;"
        }
    .end annotation

    .line 337
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->B:Ljava/util/List;

    monitor-enter v0

    .line 338
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->B:Ljava/util/List;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 339
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static e(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 644
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->p:Ljava/util/concurrent/ExecutorService;

    :cond_0
    return-void
.end method

.method public static f()Lcom/ss/android/socialbase/downloader/network/h;
    .locals 1

    .line 433
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->j:Lcom/ss/android/socialbase/downloader/network/h;

    return-object v0
.end method

.method private static f(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 649
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->q:Ljava/util/concurrent/ExecutorService;

    :cond_0
    return-void
.end method

.method public static g()Lcom/ss/android/socialbase/downloader/d/b;
    .locals 1

    .line 437
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->z:Lcom/ss/android/socialbase/downloader/d/b;

    return-object v0
.end method

.method private static g(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 654
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->r:Ljava/util/concurrent/ExecutorService;

    :cond_0
    return-void
.end method

.method public static h()Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;
    .locals 2

    .line 589
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->k:Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    if-nez v0, :cond_1

    .line 590
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 591
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->k:Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    if-nez v1, :cond_0

    .line 592
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/g;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/g;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->k:Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    .line 594
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 596
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->k:Lcom/ss/android/socialbase/downloader/network/IDownloadHttpService;

    return-object v0
.end method

.method private static h(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 659
    sput-object p0, Lcom/ss/android/socialbase/downloader/downloader/b;->s:Ljava/util/concurrent/ExecutorService;

    :cond_0
    return-void
.end method

.method public static i()Lcom/ss/android/socialbase/downloader/network/h;
    .locals 2

    .line 600
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->l:Lcom/ss/android/socialbase/downloader/network/h;

    if-nez v0, :cond_1

    .line 601
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 602
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->l:Lcom/ss/android/socialbase/downloader/network/h;

    if-nez v1, :cond_0

    .line 603
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/f;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/f;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->l:Lcom/ss/android/socialbase/downloader/network/h;

    .line 605
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 607
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->l:Lcom/ss/android/socialbase/downloader/network/h;

    return-object v0
.end method

.method public static j()Z
    .locals 3

    .line 688
    invoke-static {}, Lcom/ss/android/socialbase/downloader/g/a;->c()Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v0

    const-string v1, "switch_not_auto_boot_service"

    sget-boolean v2, Lcom/ss/android/socialbase/downloader/downloader/b;->T:Z

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/socialbase/downloader/g/a;->a(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static declared-synchronized k()Lcom/ss/android/socialbase/downloader/downloader/o;
    .locals 2

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 693
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->y:Lcom/ss/android/socialbase/downloader/downloader/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static l()Ljava/util/concurrent/ExecutorService;
    .locals 11

    .line 747
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->n:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 748
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 749
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->n:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 750
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v3, Lcom/ss/android/socialbase/downloader/downloader/b;->J:I

    sget v4, Lcom/ss/android/socialbase/downloader/downloader/b;->J:I

    const-wide/16 v5, 0xf

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v9, Lcom/ss/android/socialbase/downloader/h/a;

    const-string v2, "DownloadThreadPool-cpu-fixed"

    const/4 v10, 0x1

    invoke-direct {v9, v2, v10}, Lcom/ss/android/socialbase/downloader/h/a;-><init>(Ljava/lang/String;Z)V

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 753
    :try_start_1
    invoke-virtual {v1, v10}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 755
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 758
    :goto_0
    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->n:Ljava/util/concurrent/ExecutorService;

    .line 760
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 762
    :cond_1
    :goto_1
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->n:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static m()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 766
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->o:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->o:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public static n()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 774
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->q:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->q:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->p()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public static o()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 781
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->r:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->r:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->p()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public static p()Ljava/util/concurrent/ExecutorService;
    .locals 11

    .line 788
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->p:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 789
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 790
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->p:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 791
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v3, Lcom/ss/android/socialbase/downloader/downloader/b;->L:I

    sget v4, Lcom/ss/android/socialbase/downloader/downloader/b;->L:I

    const-wide/16 v5, 0xf

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v9, Lcom/ss/android/socialbase/downloader/h/a;

    const-string v2, "DownloadThreadPool-mix-fixed"

    const/4 v10, 0x1

    invoke-direct {v9, v2, v10}, Lcom/ss/android/socialbase/downloader/h/a;-><init>(Ljava/lang/String;Z)V

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794
    :try_start_1
    invoke-virtual {v1, v10}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 796
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 799
    :goto_0
    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->p:Ljava/util/concurrent/ExecutorService;

    .line 801
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 803
    :cond_1
    :goto_1
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->p:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static q()Ljava/util/concurrent/ExecutorService;
    .locals 11

    .line 810
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->t:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 811
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 812
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->t:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 813
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v3, Lcom/ss/android/socialbase/downloader/downloader/b;->K:I

    sget v4, Lcom/ss/android/socialbase/downloader/downloader/b;->K:I

    const-wide/16 v5, 0xf

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v9, Lcom/ss/android/socialbase/downloader/h/a;

    const-string v2, "DownloadThreadPool-chunk-fixed"

    const/4 v10, 0x1

    invoke-direct {v9, v2, v10}, Lcom/ss/android/socialbase/downloader/h/a;-><init>(Ljava/lang/String;Z)V

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 816
    :try_start_1
    invoke-virtual {v1, v10}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 818
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 821
    :goto_0
    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->t:Ljava/util/concurrent/ExecutorService;

    .line 823
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 825
    :cond_1
    :goto_1
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->t:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static r()Ljava/util/concurrent/ExecutorService;
    .locals 11

    .line 832
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->s:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 833
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 834
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->s:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 835
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v3, Lcom/ss/android/socialbase/downloader/downloader/b;->M:I

    sget v4, Lcom/ss/android/socialbase/downloader/downloader/b;->M:I

    const-wide/16 v5, 0xf

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v9, Lcom/ss/android/socialbase/downloader/h/a;

    const-string v2, "DownloadThreadPool-db-fixed"

    const/4 v10, 0x1

    invoke-direct {v9, v2, v10}, Lcom/ss/android/socialbase/downloader/h/a;-><init>(Ljava/lang/String;Z)V

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 838
    :try_start_1
    invoke-virtual {v1, v10}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 840
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 843
    :goto_0
    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->s:Ljava/util/concurrent/ExecutorService;

    .line 845
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 847
    :cond_1
    :goto_1
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->s:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static s()Lokhttp3/OkHttpClient;
    .locals 2

    .line 851
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->D:Lokhttp3/OkHttpClient;

    if-nez v0, :cond_1

    .line 852
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 853
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->D:Lokhttp3/OkHttpClient;

    if-nez v1, :cond_0

    .line 854
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->t()Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    .line 855
    invoke-virtual {v1}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v1

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->D:Lokhttp3/OkHttpClient;

    .line 857
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 859
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->D:Lokhttp3/OkHttpClient;

    return-object v0
.end method

.method public static t()Lokhttp3/OkHttpClient$Builder;
    .locals 5

    .line 863
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 864
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 865
    invoke-virtual {v1, v2, v3, v4}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 866
    invoke-virtual {v1, v2, v3, v4}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 867
    invoke-virtual {v1, v2}, Lokhttp3/OkHttpClient$Builder;->retryOnConnectionFailure(Z)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    .line 868
    invoke-virtual {v1, v2}, Lokhttp3/OkHttpClient$Builder;->followRedirects(Z)Lokhttp3/OkHttpClient$Builder;

    move-result-object v1

    sget-object v2, Lokhttp3/Protocol;->HTTP_1_1:Lokhttp3/Protocol;

    .line 869
    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lokhttp3/OkHttpClient$Builder;->protocols(Ljava/util/List;)Lokhttp3/OkHttpClient$Builder;

    .line 870
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->u:Ljava/util/concurrent/ExecutorService;

    if-eqz v1, :cond_0

    .line 871
    new-instance v1, Lokhttp3/Dispatcher;

    sget-object v2, Lcom/ss/android/socialbase/downloader/downloader/b;->u:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v1, v2}, Lokhttp3/Dispatcher;-><init>(Ljava/util/concurrent/ExecutorService;)V

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->dispatcher(Lokhttp3/Dispatcher;)Lokhttp3/OkHttpClient$Builder;

    :cond_0
    return-object v0
.end method

.method public static u()Lcom/ss/android/socialbase/downloader/network/f;
    .locals 1

    .line 877
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->E:Lcom/ss/android/socialbase/downloader/network/f;

    return-object v0
.end method

.method public static v()Lcom/ss/android/socialbase/downloader/network/f;
    .locals 2

    .line 881
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->F:Lcom/ss/android/socialbase/downloader/network/f;

    if-nez v0, :cond_1

    .line 882
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 883
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->F:Lcom/ss/android/socialbase/downloader/network/f;

    if-nez v1, :cond_0

    .line 884
    new-instance v1, Lcom/ss/android/socialbase/downloader/downloader/b$2;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/downloader/b$2;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->F:Lcom/ss/android/socialbase/downloader/network/f;

    .line 891
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 893
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->F:Lcom/ss/android/socialbase/downloader/network/f;

    return-object v0
.end method

.method public static declared-synchronized w()Lcom/ss/android/socialbase/downloader/downloader/k;
    .locals 2

    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 897
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->m:Lcom/ss/android/socialbase/downloader/downloader/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static x()Lcom/ss/android/socialbase/downloader/downloader/i;
    .locals 2

    .line 912
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->b:Lcom/ss/android/socialbase/downloader/downloader/i;

    if-nez v0, :cond_1

    .line 913
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 914
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->b:Lcom/ss/android/socialbase/downloader/downloader/i;

    if-nez v1, :cond_0

    .line 915
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/d;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/d;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->b:Lcom/ss/android/socialbase/downloader/downloader/i;

    .line 917
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 919
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->b:Lcom/ss/android/socialbase/downloader/downloader/i;

    return-object v0
.end method

.method public static y()Lcom/ss/android/socialbase/downloader/downloader/n;
    .locals 2

    .line 928
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->g:Lcom/ss/android/socialbase/downloader/downloader/n;

    if-nez v0, :cond_1

    .line 929
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 930
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->g:Lcom/ss/android/socialbase/downloader/downloader/n;

    if-nez v1, :cond_0

    .line 931
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/h;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/h;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->g:Lcom/ss/android/socialbase/downloader/downloader/n;

    .line 933
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 935
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->g:Lcom/ss/android/socialbase/downloader/downloader/n;

    return-object v0
.end method

.method public static z()Lcom/ss/android/socialbase/downloader/downloader/n;
    .locals 2

    .line 939
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->h:Lcom/ss/android/socialbase/downloader/downloader/n;

    if-nez v0, :cond_1

    .line 940
    const-class v0, Lcom/ss/android/socialbase/downloader/downloader/b;

    monitor-enter v0

    .line 941
    :try_start_0
    sget-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->h:Lcom/ss/android/socialbase/downloader/downloader/n;

    if-nez v1, :cond_0

    .line 942
    new-instance v1, Lcom/ss/android/socialbase/downloader/impls/n;

    invoke-direct {v1}, Lcom/ss/android/socialbase/downloader/impls/n;-><init>()V

    sput-object v1, Lcom/ss/android/socialbase/downloader/downloader/b;->h:Lcom/ss/android/socialbase/downloader/downloader/n;

    .line 944
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 946
    :cond_1
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/downloader/downloader/b;->h:Lcom/ss/android/socialbase/downloader/downloader/n;

    return-object v0
.end method
