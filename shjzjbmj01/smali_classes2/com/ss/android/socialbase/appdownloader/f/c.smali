.class public Lcom/ss/android/socialbase/appdownloader/f/c;
.super Ljava/lang/Object;
.source "RomUtils.java"


# static fields
.field public static a:Ljava/lang/String; = null

.field public static b:Ljava/lang/String; = ""

.field public static c:Ljava/lang/String; = null

.field private static d:Ljava/lang/String; = ""

.field private static e:Ljava/lang/String;

.field private static f:Ljava/lang/String;

.field private static g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public static a()Z
    .locals 1

    const-string v0, "EMUI"

    .line 61
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .line 159
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->n()V

    .line 160
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 161
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0

    :cond_0
    const-string v0, "ro.miui.ui.version.name"

    .line 164
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "MIUI"

    .line 165
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "com.xiaomi.market"

    .line 166
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    const-string v0, "ro.build.version.emui"

    .line 167
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "EMUI"

    .line 168
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "com.huawei.appmarket"

    .line 169
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 170
    :cond_2
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 171
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->a:Ljava/lang/String;

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    .line 172
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/g;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_3

    .line 174
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->b:Ljava/lang/String;

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    const-string v0, "com.heytap.market"

    .line 176
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    const-string v0, "ro.vivo.os.version"

    .line 178
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "VIVO"

    .line 179
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "com.bbk.appstore"

    .line 180
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    const-string v0, "ro.smartisan.version"

    .line 181
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "SMARTISAN"

    .line 182
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "com.smartisanos.appstore"

    .line 183
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto/16 :goto_0

    :cond_6
    const-string v0, "ro.gn.sv.version"

    .line 184
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "QIONEE"

    .line 185
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "com.gionee.aora.market"

    .line 186
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    const-string v0, "ro.lenovo.lvp.version"

    .line 187
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "LENOVO"

    .line 188
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "com.lenovo.leos.appstore"

    .line 189
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 190
    :cond_8
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SAMSUNG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "SAMSUNG"

    .line 191
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "com.sec.android.app.samsungapps"

    .line 192
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto :goto_0

    .line 193
    :cond_9
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ZTE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "ZTE"

    .line 195
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "zte.com.market"

    .line 196
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto :goto_0

    .line 197
    :cond_a
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NUBIA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "NUBIA"

    .line 198
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "cn.nubia.neostore"

    .line 199
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto :goto_0

    .line 201
    :cond_b
    sget-object v0, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    .line 202
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FLYME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "FLYME"

    .line 203
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    const-string v0, "com.meizu.mstore"

    .line 204
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    goto :goto_0

    :cond_c
    const-string v0, "unknown"

    .line 206
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    .line 207
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    .line 210
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 217
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getprop "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object p0

    .line 218
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object p0

    invoke-direct {v4, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 p0, 0x400

    invoke-direct {v3, v4, p0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 219
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p0

    .line 220
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224
    new-array v0, v2, [Ljava/io/Closeable;

    aput-object v3, v0, v1

    invoke-static {v0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    return-object p0

    :catchall_0
    move-exception p0

    move-object v0, v3

    goto :goto_0

    :catchall_1
    move-exception p0

    :goto_0
    new-array v2, v2, [Ljava/io/Closeable;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    throw p0

    :catch_0
    move-object v3, v0

    :catch_1
    new-array p0, v2, [Ljava/io/Closeable;

    aput-object v3, p0, v1

    invoke-static {p0}, Lcom/ss/android/socialbase/downloader/i/e;->a([Ljava/io/Closeable;)V

    return-object v0
.end method

.method public static b()Z
    .locals 1

    const-string v0, "MIUI"

    .line 66
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 1

    const-string v0, "VIVO"

    .line 71
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static d()Z
    .locals 1

    .line 76
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->n()V

    .line 77
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static e()Z
    .locals 1

    const-string v0, "FLYME"

    .line 82
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static f()Z
    .locals 1

    const-string v0, "SAMSUNG"

    .line 102
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static g()Ljava/lang/String;
    .locals 1

    .line 126
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    .line 127
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->a(Ljava/lang/String;)Z

    .line 129
    :cond_0
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static h()Ljava/lang/String;
    .locals 1

    .line 135
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    .line 136
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->a(Ljava/lang/String;)Z

    .line 138
    :cond_0
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->f:Ljava/lang/String;

    return-object v0
.end method

.method public static i()Ljava/lang/String;
    .locals 1

    .line 144
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    .line 145
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->a(Ljava/lang/String;)Z

    .line 147
    :cond_0
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static final j()Ljava/lang/String;
    .locals 1

    .line 230
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public static k()Z
    .locals 2

    .line 251
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->o()V

    const-string v0, "V10"

    .line 252
    sget-object v1, Lcom/ss/android/socialbase/appdownloader/f/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static l()Z
    .locals 2

    .line 256
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->o()V

    const-string v0, "V11"

    .line 257
    sget-object v1, Lcom/ss/android/socialbase/appdownloader/f/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static m()Z
    .locals 2

    .line 261
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->o()V

    const-string v0, "V12"

    .line 262
    sget-object v1, Lcom/ss/android/socialbase/appdownloader/f/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static n()V
    .locals 2

    .line 151
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    sget-object v0, Lcom/ss/android/socialbase/downloader/constants/e;->b:Ljava/lang/String;

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->a:Ljava/lang/String;

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ro.build.version."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "rom"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->d:Ljava/lang/String;

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/ss/android/socialbase/downloader/constants/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".market"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->b:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private static o()V
    .locals 1

    .line 266
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    :try_start_0
    const-string v0, "ro.miui.ui.version.name"

    .line 268
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/f/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->g:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 270
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 272
    :goto_0
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_1

    :cond_0
    sget-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->g:Ljava/lang/String;

    :goto_1
    sput-object v0, Lcom/ss/android/socialbase/appdownloader/f/c;->g:Ljava/lang/String;

    :cond_1
    return-void
.end method
