.class final Lcom/ss/android/socialbase/appdownloader/f/a/a$a;
.super Ljava/lang/Object;
.source "AXmlResourceParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ss/android/socialbase/appdownloader/f/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private a:[I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x20

    .line 539
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    return-void
.end method

.method private a(I)V
    .locals 3

    .line 652
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    array-length v0, v0

    iget v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    sub-int/2addr v0, v1

    if-gt v0, p1, :cond_0

    .line 654
    iget-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    array-length p1, p1

    add-int/2addr p1, v0

    mul-int/lit8 p1, p1, 0x2

    .line 655
    new-array p1, p1, [I

    .line 656
    iget-object v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    iget v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 657
    iput-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    .line 547
    iput v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    .line 548
    iput v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->c:I

    return-void
.end method

.method public final a(II)V
    .locals 6

    .line 581
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->c:I

    if-nez v0, :cond_0

    .line 582
    invoke-virtual {p0}, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->e()V

    :cond_0
    const/4 v0, 0x2

    .line 585
    invoke-direct {p0, v0}, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a(I)V

    .line 586
    iget v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    add-int/lit8 v1, v1, -0x1

    .line 587
    iget-object v2, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    aget v2, v2, v1

    .line 588
    iget-object v3, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    add-int/lit8 v4, v1, -0x1

    mul-int/lit8 v5, v2, 0x2

    sub-int/2addr v4, v5

    add-int/lit8 v2, v2, 0x1

    aput v2, v3, v4

    .line 589
    iget-object v3, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    aput p1, v3, v1

    .line 590
    iget-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    add-int/lit8 v3, v1, 0x1

    aput p2, p1, v3

    .line 591
    iget-object p1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    add-int/2addr v1, v0

    aput v2, p1, v1

    .line 592
    iget p1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    add-int/2addr p1, v0

    iput p1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    return-void
.end method

.method public final b()I
    .locals 2

    .line 552
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 555
    :cond_0
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    add-int/lit8 v0, v0, -0x1

    .line 556
    iget-object v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    aget v0, v1, v0

    return v0
.end method

.method public final c()Z
    .locals 4

    .line 596
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 599
    :cond_0
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    const/4 v2, 0x1

    sub-int/2addr v0, v2

    .line 600
    iget-object v3, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    aget v3, v3, v0

    if-nez v3, :cond_1

    return v1

    :cond_1
    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v0, v0, -0x2

    .line 606
    iget-object v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    aput v3, v1, v0

    mul-int/lit8 v1, v3, 0x2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 608
    iget-object v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    aput v3, v1, v0

    .line 609
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    return v2
.end method

.method public final d()I
    .locals 1

    .line 628
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->c:I

    return v0
.end method

.method public final e()V
    .locals 4

    const/4 v0, 0x2

    .line 632
    invoke-direct {p0, v0}, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a(I)V

    .line 633
    iget v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    .line 634
    iget-object v2, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    const/4 v3, 0x0

    aput v3, v2, v1

    .line 635
    iget-object v2, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    add-int/lit8 v1, v1, 0x1

    aput v3, v2, v1

    .line 636
    iget v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    .line 637
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->c:I

    return-void
.end method

.method public final f()V
    .locals 2

    .line 641
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    if-eqz v0, :cond_0

    .line 642
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    add-int/lit8 v0, v0, -0x1

    .line 643
    iget-object v1, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->a:[I

    aget v1, v1, v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 645
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    add-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->b:I

    .line 646
    iget v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ss/android/socialbase/appdownloader/f/a/a$a;->c:I

    :cond_0
    return-void
.end method
