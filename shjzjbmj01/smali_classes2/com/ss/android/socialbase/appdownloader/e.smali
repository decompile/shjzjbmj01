.class public Lcom/ss/android/socialbase/appdownloader/e;
.super Ljava/lang/Object;
.source "AppResourceUtils.java"


# direct methods
.method public static a()I
    .locals 1

    const-string v0, "tt_appdownloader_notification_layout"

    .line 12
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static b()I
    .locals 1

    const-string v0, "tt_appdownloader_notification_title_color"

    .line 16
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->f(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static c()I
    .locals 2

    const-string v0, "textColor"

    const-string v1, "android"

    .line 20
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static d()I
    .locals 2

    const-string v0, "textSize"

    const-string v1, "android"

    .line 24
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static e()I
    .locals 1

    const-string v0, "tt_appdownloader_style_notification_title"

    .line 28
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->d(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static f()I
    .locals 1

    const-string v0, "tt_appdownloader_root"

    .line 32
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static g()I
    .locals 1

    const-string v0, "tt_appdownloader_download_progress"

    .line 36
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static h()I
    .locals 1

    const-string v0, "tt_appdownloader_download_progress_new"

    .line 40
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static i()I
    .locals 1

    const-string v0, "tt_appdownloader_download_success"

    .line 44
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static j()I
    .locals 1

    const-string v0, "tt_appdownloader_download_text"

    .line 48
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static k()I
    .locals 1

    const-string v0, "tt_appdownloader_action"

    .line 52
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static l()I
    .locals 1

    const-string v0, "tt_appdownloader_icon"

    .line 56
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static m()I
    .locals 1

    const-string v0, "tt_appdownloader_desc"

    .line 60
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static n()I
    .locals 1

    const-string v0, "tt_appdownloader_download_size"

    .line 64
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static o()I
    .locals 1

    const-string v0, "tt_appdownloader_download_success_size"

    .line 68
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static p()I
    .locals 1

    const-string v0, "tt_appdownloader_download_status"

    .line 72
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static q()I
    .locals 1

    const-string v0, "tt_appdownloader_download_success_status"

    .line 76
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->e(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static r()I
    .locals 1

    const-string v0, "tt_appdownloader_notification_material_background_color"

    .line 80
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->f(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static s()I
    .locals 1

    const-string v0, "tt_appdownloader_action_new_bg"

    .line 84
    invoke-static {v0}, Lcom/ss/android/socialbase/appdownloader/i;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static t()I
    .locals 2

    const-string v0, "stat_sys_download"

    const-string v1, "android"

    .line 88
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static u()I
    .locals 2

    const-string v0, "stat_sys_warning"

    const-string v1, "android"

    .line 92
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static v()I
    .locals 2

    const-string v0, "stat_sys_download_done"

    const-string v1, "android"

    .line 96
    invoke-static {v0, v1}, Lcom/ss/android/socialbase/appdownloader/i;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
