.class public Lcom/ss/android/a/a/c/d$a;
.super Ljava/lang/Object;
.source "DownloadEventModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ss/android/a/a/c/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:J

.field private f:Ljava/lang/String;

.field private g:J

.field private h:Lorg/json/JSONObject;

.field private i:Lorg/json/JSONObject;

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:I

.field private m:Ljava/lang/Object;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 113
    iput-boolean v0, p0, Lcom/ss/android/a/a/c/d$a;->d:Z

    .line 124
    iput-boolean v0, p0, Lcom/ss/android/a/a/c/d$a;->o:Z

    return-void
.end method

.method static synthetic a(Lcom/ss/android/a/a/c/d$a;)Ljava/lang/String;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/ss/android/a/a/c/d$a;)Ljava/lang/String;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/ss/android/a/a/c/d$a;)Ljava/lang/String;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/ss/android/a/a/c/d$a;)Z
    .locals 0

    .line 109
    iget-boolean p0, p0, Lcom/ss/android/a/a/c/d$a;->d:Z

    return p0
.end method

.method static synthetic e(Lcom/ss/android/a/a/c/d$a;)J
    .locals 2

    .line 109
    iget-wide v0, p0, Lcom/ss/android/a/a/c/d$a;->e:J

    return-wide v0
.end method

.method static synthetic f(Lcom/ss/android/a/a/c/d$a;)Ljava/lang/String;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->f:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic g(Lcom/ss/android/a/a/c/d$a;)J
    .locals 2

    .line 109
    iget-wide v0, p0, Lcom/ss/android/a/a/c/d$a;->g:J

    return-wide v0
.end method

.method static synthetic h(Lcom/ss/android/a/a/c/d$a;)Lorg/json/JSONObject;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    return-object p0
.end method

.method static synthetic i(Lcom/ss/android/a/a/c/d$a;)Lorg/json/JSONObject;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->i:Lorg/json/JSONObject;

    return-object p0
.end method

.method static synthetic j(Lcom/ss/android/a/a/c/d$a;)Ljava/util/List;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->k:Ljava/util/List;

    return-object p0
.end method

.method static synthetic k(Lcom/ss/android/a/a/c/d$a;)I
    .locals 0

    .line 109
    iget p0, p0, Lcom/ss/android/a/a/c/d$a;->l:I

    return p0
.end method

.method static synthetic l(Lcom/ss/android/a/a/c/d$a;)Ljava/lang/Object;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->m:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic m(Lcom/ss/android/a/a/c/d$a;)Z
    .locals 0

    .line 109
    iget-boolean p0, p0, Lcom/ss/android/a/a/c/d$a;->o:Z

    return p0
.end method

.method static synthetic n(Lcom/ss/android/a/a/c/d$a;)Ljava/lang/String;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->p:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic o(Lcom/ss/android/a/a/c/d$a;)Lorg/json/JSONObject;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    return-object p0
.end method

.method static synthetic p(Lcom/ss/android/a/a/c/d$a;)Ljava/lang/String;
    .locals 0

    .line 109
    iget-object p0, p0, Lcom/ss/android/a/a/c/d$a;->n:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public a(I)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 189
    iput p1, p0, Lcom/ss/android/a/a/c/d$a;->l:I

    return-object p0
.end method

.method public a(J)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 149
    iput-wide p1, p0, Lcom/ss/android/a/a/c/d$a;->e:J

    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/ss/android/a/a/c/d$a;->m:Ljava/lang/Object;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/ss/android/a/a/c/d$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/ss/android/a/a/c/d$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/ss/android/a/a/c/d$a;"
        }
    .end annotation

    .line 184
    iput-object p1, p0, Lcom/ss/android/a/a/c/d$a;->k:Ljava/util/List;

    return-object p0
.end method

.method public a(Lorg/json/JSONObject;)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    return-object p0
.end method

.method public a(Z)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 129
    iput-boolean p1, p0, Lcom/ss/android/a/a/c/d$a;->o:Z

    return-object p0
.end method

.method public a()Lcom/ss/android/a/a/c/d;
    .locals 8

    .line 203
    iget-object v0, p0, Lcom/ss/android/a/a/c/d$a;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "umeng"

    .line 204
    iput-object v0, p0, Lcom/ss/android/a/a/c/d$a;->a:Ljava/lang/String;

    .line 206
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 207
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    if-nez v1, :cond_1

    .line 208
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    iput-object v1, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    .line 211
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->j:Ljava/util/Map;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 212
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 213
    iget-object v3, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 214
    iget-object v3, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 218
    :cond_3
    iget-boolean v1, p0, Lcom/ss/android/a/a/c/d$a;->o:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_8

    .line 219
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/ss/android/a/a/c/d$a;->p:Ljava/lang/String;

    .line 220
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    iput-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    .line 221
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    .line 222
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 223
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 224
    iget-object v6, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    iget-object v7, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    invoke-virtual {v7, v5}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 226
    :cond_4
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    const-string v5, "category"

    iget-object v6, p0, Lcom/ss/android/a/a/c/d$a;->a:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 227
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    const-string v5, "tag"

    iget-object v6, p0, Lcom/ss/android/a/a/c/d$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 228
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    const-string v5, "value"

    iget-wide v6, p0, Lcom/ss/android/a/a/c/d$a;->e:J

    invoke-virtual {v1, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 229
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    const-string v5, "ext_value"

    iget-wide v6, p0, Lcom/ss/android/a/a/c/d$a;->g:J

    invoke-virtual {v1, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 230
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 231
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    const-string v5, "refer"

    iget-object v6, p0, Lcom/ss/android/a/a/c/d$a;->n:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 233
    :cond_5
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->i:Lorg/json/JSONObject;

    if-eqz v1, :cond_6

    .line 234
    new-array v1, v4, [Lorg/json/JSONObject;

    iget-object v5, p0, Lcom/ss/android/a/a/c/d$a;->i:Lorg/json/JSONObject;

    aput-object v5, v1, v3

    iget-object v5, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    aput-object v5, v1, v2

    invoke-static {v1}, Lcom/ss/android/a/a/d/a;->a([Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v1

    iput-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    .line 236
    :cond_6
    iget-boolean v1, p0, Lcom/ss/android/a/a/c/d$a;->d:Z

    if-eqz v1, :cond_8

    .line 237
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    const-string v5, "log_extra"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 238
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    const-string v5, "log_extra"

    iget-object v6, p0, Lcom/ss/android/a/a/c/d$a;->f:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 240
    :cond_7
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->q:Lorg/json/JSONObject;

    const-string v5, "is_ad_event"

    const-string v6, "1"

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 243
    :cond_8
    iget-boolean v1, p0, Lcom/ss/android/a/a/c/d$a;->d:Z

    if-eqz v1, :cond_a

    const-string v1, "ad_extra_data"

    .line 244
    iget-object v5, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "log_extra"

    .line 245
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "log_extra"

    .line 246
    iget-object v5, p0, Lcom/ss/android/a/a/c/d$a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_9
    const-string v1, "is_ad_event"

    const-string v5, "1"

    .line 248
    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    :cond_a
    const-string v1, "extra"

    .line 250
    iget-object v5, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 252
    :goto_2
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "refer"

    .line 253
    iget-object v5, p0, Lcom/ss/android/a/a/c/d$a;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 255
    :cond_b
    iget-object v1, p0, Lcom/ss/android/a/a/c/d$a;->i:Lorg/json/JSONObject;

    if-eqz v1, :cond_c

    .line 256
    new-array v1, v4, [Lorg/json/JSONObject;

    iget-object v4, p0, Lcom/ss/android/a/a/c/d$a;->i:Lorg/json/JSONObject;

    aput-object v4, v1, v3

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/ss/android/a/a/d/a;->a([Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    .line 258
    :cond_c
    iput-object v0, p0, Lcom/ss/android/a/a/c/d$a;->h:Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :catch_0
    new-instance v0, Lcom/ss/android/a/a/c/d;

    invoke-direct {v0, p0}, Lcom/ss/android/a/a/c/d;-><init>(Lcom/ss/android/a/a/c/d$a;)V

    return-object v0
.end method

.method public b(J)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 154
    iput-wide p1, p0, Lcom/ss/android/a/a/c/d$a;->g:J

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/ss/android/a/a/c/d$a;->c:Ljava/lang/String;

    return-object p0
.end method

.method public b(Lorg/json/JSONObject;)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/ss/android/a/a/c/d$a;->i:Lorg/json/JSONObject;

    return-object p0
.end method

.method public b(Z)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 164
    iput-boolean p1, p0, Lcom/ss/android/a/a/c/d$a;->d:Z

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/ss/android/a/a/c/d$a;->f:Ljava/lang/String;

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/ss/android/a/a/c/d$a;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/ss/android/a/a/c/d$a;->n:Ljava/lang/String;

    return-object p0
.end method
