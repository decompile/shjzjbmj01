.class public final Lcom/ss/android/downloadlib/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ss/android/downloadlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final tt_appdownloader_button_cancel_download:I

.field public static final tt_appdownloader_button_queue_for_wifi:I

.field public static final tt_appdownloader_button_start_now:I

.field public static final tt_appdownloader_download_percent:I

.field public static final tt_appdownloader_download_remaining:I

.field public static final tt_appdownloader_download_unknown_title:I

.field public static final tt_appdownloader_duration_hours:I

.field public static final tt_appdownloader_duration_minutes:I

.field public static final tt_appdownloader_duration_seconds:I

.field public static final tt_appdownloader_jump_unknown_source:I

.field public static final tt_appdownloader_label_cancel:I

.field public static final tt_appdownloader_label_cancel_directly:I

.field public static final tt_appdownloader_label_ok:I

.field public static final tt_appdownloader_label_reserve_wifi:I

.field public static final tt_appdownloader_notification_download:I

.field public static final tt_appdownloader_notification_download_complete_open:I

.field public static final tt_appdownloader_notification_download_complete_with_install:I

.field public static final tt_appdownloader_notification_download_complete_without_install:I

.field public static final tt_appdownloader_notification_download_continue:I

.field public static final tt_appdownloader_notification_download_delete:I

.field public static final tt_appdownloader_notification_download_failed:I

.field public static final tt_appdownloader_notification_download_install:I

.field public static final tt_appdownloader_notification_download_open:I

.field public static final tt_appdownloader_notification_download_pause:I

.field public static final tt_appdownloader_notification_download_restart:I

.field public static final tt_appdownloader_notification_download_resume:I

.field public static final tt_appdownloader_notification_download_space_failed:I

.field public static final tt_appdownloader_notification_download_waiting_net:I

.field public static final tt_appdownloader_notification_download_waiting_wifi:I

.field public static final tt_appdownloader_notification_downloading:I

.field public static final tt_appdownloader_notification_install_finished_open:I

.field public static final tt_appdownloader_notification_insufficient_space_error:I

.field public static final tt_appdownloader_notification_need_wifi_for_size:I

.field public static final tt_appdownloader_notification_no_internet_error:I

.field public static final tt_appdownloader_notification_no_wifi_and_in_net:I

.field public static final tt_appdownloader_notification_paused_in_background:I

.field public static final tt_appdownloader_notification_pausing:I

.field public static final tt_appdownloader_notification_prepare:I

.field public static final tt_appdownloader_notification_request_btn_no:I

.field public static final tt_appdownloader_notification_request_btn_yes:I

.field public static final tt_appdownloader_notification_request_message:I

.field public static final tt_appdownloader_notification_request_title:I

.field public static final tt_appdownloader_notification_waiting_download_complete_handler:I

.field public static final tt_appdownloader_resume_in_wifi:I

.field public static final tt_appdownloader_tip:I

.field public static final tt_appdownloader_wifi_recommended_body:I

.field public static final tt_appdownloader_wifi_recommended_title:I

.field public static final tt_appdownloader_wifi_required_body:I

.field public static final tt_appdownloader_wifi_required_title:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 78
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_button_cancel_download:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_button_cancel_download:I

    .line 79
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_button_queue_for_wifi:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_button_queue_for_wifi:I

    .line 80
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_button_start_now:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_button_start_now:I

    .line 81
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_download_percent:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_download_percent:I

    .line 82
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_download_remaining:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_download_remaining:I

    .line 83
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_download_unknown_title:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_download_unknown_title:I

    .line 84
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_duration_hours:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_duration_hours:I

    .line 85
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_duration_minutes:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_duration_minutes:I

    .line 86
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_duration_seconds:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_duration_seconds:I

    .line 87
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_jump_unknown_source:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_jump_unknown_source:I

    .line 88
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_label_cancel:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_label_cancel:I

    .line 89
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_label_cancel_directly:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_label_cancel_directly:I

    .line 90
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_label_ok:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_label_ok:I

    .line 91
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_label_reserve_wifi:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_label_reserve_wifi:I

    .line 92
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download:I

    .line 93
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_complete_open:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_complete_open:I

    .line 94
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_complete_with_install:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_complete_with_install:I

    .line 95
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_complete_without_install:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_complete_without_install:I

    .line 96
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_continue:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_continue:I

    .line 97
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_delete:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_delete:I

    .line 98
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_failed:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_failed:I

    .line 99
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_install:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_install:I

    .line 100
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_open:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_open:I

    .line 101
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_pause:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_pause:I

    .line 102
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_restart:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_restart:I

    .line 103
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_resume:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_resume:I

    .line 104
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_space_failed:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_space_failed:I

    .line 105
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_waiting_net:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_waiting_net:I

    .line 106
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_download_waiting_wifi:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_download_waiting_wifi:I

    .line 107
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_downloading:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_downloading:I

    .line 108
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_install_finished_open:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_install_finished_open:I

    .line 109
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_insufficient_space_error:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_insufficient_space_error:I

    .line 110
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_need_wifi_for_size:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_need_wifi_for_size:I

    .line 111
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_no_internet_error:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_no_internet_error:I

    .line 112
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_no_wifi_and_in_net:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_no_wifi_and_in_net:I

    .line 113
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_paused_in_background:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_paused_in_background:I

    .line 114
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_pausing:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_pausing:I

    .line 115
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_prepare:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_prepare:I

    .line 116
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_request_btn_no:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_request_btn_no:I

    .line 117
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_request_btn_yes:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_request_btn_yes:I

    .line 118
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_request_message:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_request_message:I

    .line 119
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_request_title:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_request_title:I

    .line 120
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_notification_waiting_download_complete_handler:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_notification_waiting_download_complete_handler:I

    .line 121
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_resume_in_wifi:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_resume_in_wifi:I

    .line 122
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_tip:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_tip:I

    .line 123
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_wifi_recommended_body:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_wifi_recommended_body:I

    .line 124
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_wifi_recommended_title:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_wifi_recommended_title:I

    .line 125
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_wifi_required_body:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_wifi_required_body:I

    .line 126
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$string;->tt_appdownloader_wifi_required_title:I

    sput v0, Lcom/ss/android/downloadlib/R$string;->tt_appdownloader_wifi_required_title:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
