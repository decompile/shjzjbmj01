.class Lcom/ss/android/downloadlib/d$1;
.super Ljava/lang/Object;
.source "DownloadComponentManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ss/android/downloadlib/d;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/ss/android/downloadlib/d;


# direct methods
.method constructor <init>(Lcom/ss/android/downloadlib/d;)V
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/ss/android/downloadlib/d$1;->a:Lcom/ss/android/downloadlib/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 154
    const-class v0, Lcom/ss/android/downloadlib/d;

    monitor-enter v0

    const/16 v1, 0xd

    .line 156
    :try_start_0
    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "sp_ad_download_event"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "sp_download_finish_cache"

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v5, "sp_delay_operation_info"

    aput-object v5, v1, v2

    const/4 v2, 0x3

    const-string v5, "sp_ttdownloader_md5"

    aput-object v5, v1, v2

    const/4 v2, 0x4

    const-string v5, "sp_name_installed_app"

    aput-object v5, v1, v2

    const/4 v2, 0x5

    const-string v5, "misc_config"

    aput-object v5, v1, v2

    const/4 v2, 0x6

    const-string v5, "sp_ad_install_back_dialog"

    aput-object v5, v1, v2

    const/4 v2, 0x7

    const-string v5, "sp_ttdownloader_clean"

    aput-object v5, v1, v2

    const/16 v2, 0x8

    const-string v5, "sp_order_download"

    aput-object v5, v1, v2

    const/16 v2, 0x9

    const-string v5, "sp_a_b_c"

    aput-object v5, v1, v2

    const/16 v2, 0xa

    const-string v5, "sp_ah_config"

    aput-object v5, v1, v2

    const/16 v2, 0xb

    const-string v5, "sp_download_info"

    aput-object v5, v1, v2

    const/16 v2, 0xc

    const-string v5, "sp_appdownloader"

    aput-object v5, v1, v2

    .line 170
    array-length v2, v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v2, :cond_1

    aget-object v6, v1, v5

    .line 171
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v6, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 173
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 176
    :cond_1
    invoke-static {}, Lcom/ss/android/socialbase/downloader/downloader/b;->x()Lcom/ss/android/socialbase/downloader/downloader/i;

    move-result-object v1

    .line 177
    instance-of v2, v1, Lcom/ss/android/socialbase/downloader/impls/d;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    .line 178
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 180
    :cond_2
    :try_start_2
    check-cast v1, Lcom/ss/android/socialbase/downloader/impls/d;

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/impls/d;->a()Lcom/ss/android/socialbase/downloader/impls/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ss/android/socialbase/downloader/impls/k;->a()Landroid/util/SparseArray;

    move-result-object v1

    .line 181
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    sub-int/2addr v2, v4

    :goto_1
    if-ltz v2, :cond_4

    .line 182
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-eqz v3, :cond_3

    .line 184
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/ss/android/socialbase/downloader/downloader/Downloader;->getInstance(Landroid/content/Context;)Lcom/ss/android/socialbase/downloader/downloader/Downloader;

    move-result-object v4

    invoke-virtual {v3}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/ss/android/socialbase/downloader/downloader/Downloader;->clearDownloadData(I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :catchall_0
    move-exception v1

    goto :goto_2

    .line 190
    :catch_0
    :cond_4
    :try_start_3
    monitor-exit v0

    return-void

    :goto_2
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method
