.class public final Lcom/ss/android/downloadlib/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ss/android/downloadlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final cancel_tv:I

.field public static final confirm_tv:I

.field public static final dash_line:I

.field public static final iv_app_icon:I

.field public static final iv_detail_back:I

.field public static final iv_privacy_back:I

.field public static final line:I

.field public static final ll_download:I

.field public static final message_tv:I

.field public static final permission_list:I

.field public static final privacy_webview:I

.field public static final title_bar:I

.field public static final tt_appdownloader_action:I

.field public static final tt_appdownloader_desc:I

.field public static final tt_appdownloader_download_progress:I

.field public static final tt_appdownloader_download_progress_new:I

.field public static final tt_appdownloader_download_size:I

.field public static final tt_appdownloader_download_status:I

.field public static final tt_appdownloader_download_success:I

.field public static final tt_appdownloader_download_success_size:I

.field public static final tt_appdownloader_download_success_status:I

.field public static final tt_appdownloader_download_text:I

.field public static final tt_appdownloader_icon:I

.field public static final tt_appdownloader_root:I

.field public static final tv_app_detail:I

.field public static final tv_app_developer:I

.field public static final tv_app_name:I

.field public static final tv_app_privacy:I

.field public static final tv_app_version:I

.field public static final tv_empty:I

.field public static final tv_give_up:I

.field public static final tv_permission_description:I

.field public static final tv_permission_title:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->cancel_tv:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->cancel_tv:I

    .line 36
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->confirm_tv:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->confirm_tv:I

    .line 37
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->dash_line:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->dash_line:I

    .line 38
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->iv_app_icon:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->iv_app_icon:I

    .line 39
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->iv_detail_back:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->iv_detail_back:I

    .line 40
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->iv_privacy_back:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->iv_privacy_back:I

    .line 41
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->line:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->line:I

    .line 42
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->ll_download:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->ll_download:I

    .line 43
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->message_tv:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->message_tv:I

    .line 44
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->permission_list:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->permission_list:I

    .line 45
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->privacy_webview:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->privacy_webview:I

    .line 46
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->title_bar:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->title_bar:I

    .line 47
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_action:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_action:I

    .line 48
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_desc:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_desc:I

    .line 49
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_progress:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_download_progress:I

    .line 50
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_progress_new:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_download_progress_new:I

    .line 51
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_size:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_download_size:I

    .line 52
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_status:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_download_status:I

    .line 53
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_success:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_download_success:I

    .line 54
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_success_size:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_download_success_size:I

    .line 55
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_success_status:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_download_success_status:I

    .line 56
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_download_text:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_download_text:I

    .line 57
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_icon:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_icon:I

    .line 58
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tt_appdownloader_root:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tt_appdownloader_root:I

    .line 59
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_detail:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tv_app_detail:I

    .line 60
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_developer:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tv_app_developer:I

    .line 61
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_name:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tv_app_name:I

    .line 62
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_privacy:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tv_app_privacy:I

    .line 63
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_app_version:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tv_app_version:I

    .line 64
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_empty:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tv_empty:I

    .line 65
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_give_up:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tv_give_up:I

    .line 66
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_permission_description:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tv_permission_description:I

    .line 67
    sget v0, Lcom/bytedance/sdk/openadsdk/adhost/R$id;->tv_permission_title:I

    sput v0, Lcom/ss/android/downloadlib/R$id;->tv_permission_title:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
