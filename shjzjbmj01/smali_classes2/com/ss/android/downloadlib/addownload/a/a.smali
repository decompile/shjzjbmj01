.class public Lcom/ss/android/downloadlib/addownload/a/a;
.super Ljava/lang/Object;
.source "AdDownloadDialogManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ss/android/downloadlib/addownload/a/a$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "a"

.field private static b:Lcom/ss/android/downloadlib/addownload/a/a;


# instance fields
.field private c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/ss/android/downloadlib/addownload/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Lcom/ss/android/downloadlib/addownload/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 68
    iput-boolean v0, p0, Lcom/ss/android/downloadlib/addownload/a/a;->d:Z

    .line 75
    new-instance v0, Lcom/ss/android/downloadlib/addownload/a/b;

    invoke-direct {v0}, Lcom/ss/android/downloadlib/addownload/a/b;-><init>()V

    iput-object v0, p0, Lcom/ss/android/downloadlib/addownload/a/a;->f:Lcom/ss/android/downloadlib/addownload/a/b;

    .line 76
    iget-object v0, p0, Lcom/ss/android/downloadlib/addownload/a/a;->f:Lcom/ss/android/downloadlib/addownload/a/b;

    const-string v1, "sp_ad_install_back_dialog"

    const-string v2, "key_uninstalled_list"

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/downloadlib/addownload/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-void
.end method

.method public static a()Lcom/ss/android/downloadlib/addownload/a/a;
    .locals 1

    .line 80
    sget-object v0, Lcom/ss/android/downloadlib/addownload/a/a;->b:Lcom/ss/android/downloadlib/addownload/a/a;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lcom/ss/android/downloadlib/addownload/a/a;

    invoke-direct {v0}, Lcom/ss/android/downloadlib/addownload/a/a;-><init>()V

    sput-object v0, Lcom/ss/android/downloadlib/addownload/a/a;->b:Lcom/ss/android/downloadlib/addownload/a/a;

    .line 83
    :cond_0
    sget-object v0, Lcom/ss/android/downloadlib/addownload/a/a;->b:Lcom/ss/android/downloadlib/addownload/a/a;

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/ss/android/downloadlib/addownload/b/a;Lcom/ss/android/downloadlib/addownload/a/a$a;Z)V
    .locals 10

    .line 277
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    iget-wide v1, p2, Lcom/ss/android/downloadlib/addownload/b/a;->b:J

    invoke-virtual {v0, v1, v2}, Lcom/ss/android/downloadlib/addownload/b/f;->d(J)Lcom/ss/android/b/a/b/b;

    move-result-object v0

    if-nez v0, :cond_0

    .line 279
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    return-void

    .line 282
    :cond_0
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->d()Lcom/ss/android/a/a/a/k;

    move-result-object v1

    new-instance v2, Lcom/ss/android/a/a/c/c$a;

    invoke-direct {v2, p1}, Lcom/ss/android/a/a/c/c$a;-><init>(Landroid/content/Context;)V

    if-eqz p4, :cond_1

    const-string v3, "\u5e94\u7528\u5b89\u88c5\u786e\u8ba4"

    goto :goto_0

    :cond_1
    const-string v3, "\u9000\u51fa\u786e\u8ba4"

    .line 283
    :goto_0
    invoke-virtual {v2, v3}, Lcom/ss/android/a/a/c/c$a;->a(Ljava/lang/String;)Lcom/ss/android/a/a/c/c$a;

    move-result-object v2

    const-string v3, "%1$s\u4e0b\u8f7d\u5b8c\u6210\uff0c\u662f\u5426\u7acb\u5373\u5b89\u88c5\uff1f"

    const/4 v9, 0x1

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p2, Lcom/ss/android/downloadlib/addownload/b/a;->e:Ljava/lang/String;

    .line 285
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "\u521a\u521a\u4e0b\u8f7d\u7684\u5e94\u7528"

    goto :goto_1

    :cond_2
    iget-object v5, p2, Lcom/ss/android/downloadlib/addownload/b/a;->e:Ljava/lang/String;

    :goto_1
    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 284
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/ss/android/a/a/c/c$a;->b(Ljava/lang/String;)Lcom/ss/android/a/a/c/c$a;

    move-result-object v2

    const-string v3, "\u7acb\u5373\u5b89\u88c5"

    .line 286
    invoke-virtual {v2, v3}, Lcom/ss/android/a/a/c/c$a;->c(Ljava/lang/String;)Lcom/ss/android/a/a/c/c$a;

    move-result-object v2

    if-eqz p4, :cond_3

    const-string p4, "\u6682\u4e0d\u5b89\u88c5"

    goto :goto_2

    :cond_3
    const-string p4, "\u9000\u51fa%1$s"

    new-array v3, v9, [Ljava/lang/Object;

    .line 288
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->labelRes:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {p4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    .line 287
    :goto_2
    invoke-virtual {v2, p4}, Lcom/ss/android/a/a/c/c$a;->d(Ljava/lang/String;)Lcom/ss/android/a/a/c/c$a;

    move-result-object p4

    .line 289
    invoke-virtual {p4, v6}, Lcom/ss/android/a/a/c/c$a;->a(Z)Lcom/ss/android/a/a/c/c$a;

    move-result-object p4

    iget-object v2, p2, Lcom/ss/android/downloadlib/addownload/b/a;->g:Ljava/lang/String;

    .line 290
    invoke-static {p1, v2}, Lcom/ss/android/downloadlib/h/k;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/ss/android/a/a/c/c$a;->a(Landroid/graphics/drawable/Drawable;)Lcom/ss/android/a/a/c/c$a;

    move-result-object p4

    new-instance v2, Lcom/ss/android/downloadlib/addownload/a/a$1;

    move-object v3, v2

    move-object v4, p0

    move-object v5, v0

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/ss/android/downloadlib/addownload/a/a$1;-><init>(Lcom/ss/android/downloadlib/addownload/a/a;Lcom/ss/android/b/a/b/b;Landroid/content/Context;Lcom/ss/android/downloadlib/addownload/b/a;Lcom/ss/android/downloadlib/addownload/a/a$a;)V

    .line 291
    invoke-virtual {p4, v2}, Lcom/ss/android/a/a/c/c$a;->a(Lcom/ss/android/a/a/c/c$b;)Lcom/ss/android/a/a/c/c$a;

    move-result-object p1

    .line 314
    invoke-virtual {p1, v9}, Lcom/ss/android/a/a/c/c$a;->a(I)Lcom/ss/android/a/a/c/c$a;

    move-result-object p1

    .line 315
    invoke-virtual {p1}, Lcom/ss/android/a/a/c/c$a;->a()Lcom/ss/android/a/a/c/c;

    move-result-object p1

    .line 282
    invoke-interface {v1, p1}, Lcom/ss/android/a/a/a/k;->b(Lcom/ss/android/a/a/c/c;)Landroid/app/Dialog;

    .line 316
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object p1

    const-string p3, "backdialog_show"

    invoke-virtual {p1, p3, v0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)V

    .line 317
    iget-object p1, p2, Lcom/ss/android/downloadlib/addownload/b/a;->d:Ljava/lang/String;

    iput-object p1, p0, Lcom/ss/android/downloadlib/addownload/a/a;->e:Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/app/Activity;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;ZLcom/ss/android/downloadlib/addownload/a/a$a;)Z
    .locals 22

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move/from16 v3, p3

    move-object/from16 v4, p4

    const/4 v5, 0x0

    if-nez p2, :cond_0

    .line 127
    :try_start_0
    iget-object v6, v1, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    return v5

    :catch_0
    move-exception v0

    goto/16 :goto_2

    :cond_0
    if-eqz v0, :cond_9

    .line 130
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v6

    if-eqz v6, :cond_1

    goto/16 :goto_3

    :cond_1
    const/4 v6, 0x1

    if-eqz p2, :cond_2

    .line 134
    iget-object v7, v1, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v7}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 135
    new-instance v7, Lcom/ss/android/downloadlib/addownload/b/a;

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v8

    int-to-long v9, v8

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getPackageName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTitle()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTargetFilePath()Ljava/lang/String;

    move-result-object v18

    move-object v8, v7

    invoke-direct/range {v8 .. v18}, Lcom/ss/android/downloadlib/addownload/b/a;-><init>(JJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {v1, v0, v7, v3, v4}, Lcom/ss/android/downloadlib/addownload/a/a;->a(Landroid/content/Context;Lcom/ss/android/downloadlib/addownload/b/a;ZLcom/ss/android/downloadlib/addownload/a/a$a;)V

    return v6

    :cond_2
    const-wide/16 v7, 0x0

    if-eqz p2, :cond_3

    .line 142
    new-instance v7, Ljava/io/File;

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTargetFilePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    .line 146
    :cond_3
    iget-object v9, v1, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v10, v1, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v10}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/util/concurrent/CopyOnWriteArrayList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v9

    .line 147
    :cond_4
    :goto_0
    invoke-interface {v9}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 148
    invoke-interface {v9}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/ss/android/downloadlib/addownload/b/a;

    if-nez v10, :cond_5

    goto :goto_0

    .line 153
    :cond_5
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v11

    iget-object v12, v10, Lcom/ss/android/downloadlib/addownload/b/a;->d:Ljava/lang/String;

    invoke-static {v11, v12}, Lcom/ss/android/downloadlib/h/k;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_4

    iget-object v11, v10, Lcom/ss/android/downloadlib/addownload/b/a;->g:Ljava/lang/String;

    .line 154
    invoke-static {v11}, Lcom/ss/android/downloadlib/h/k;->a(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_6

    goto :goto_0

    .line 158
    :cond_6
    new-instance v9, Ljava/io/File;

    iget-object v11, v10, Lcom/ss/android/downloadlib/addownload/b/a;->g:Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v9}, Ljava/io/File;->lastModified()J

    move-result-wide v11

    cmp-long v9, v11, v7

    if-ltz v9, :cond_7

    .line 162
    invoke-virtual {v1, v0, v10, v3, v4}, Lcom/ss/android/downloadlib/addownload/a/a;->a(Landroid/content/Context;Lcom/ss/android/downloadlib/addownload/b/a;ZLcom/ss/android/downloadlib/addownload/a/a$a;)V

    goto :goto_1

    .line 164
    :cond_7
    new-instance v7, Lcom/ss/android/downloadlib/addownload/b/a;

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v8

    int-to-long v12, v8

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getPackageName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTitle()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTargetFilePath()Ljava/lang/String;

    move-result-object v21

    move-object v11, v7

    invoke-direct/range {v11 .. v21}, Lcom/ss/android/downloadlib/addownload/b/a;-><init>(JJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-virtual {v1, v0, v7, v3, v4}, Lcom/ss/android/downloadlib/addownload/a/a;->a(Landroid/content/Context;Lcom/ss/android/downloadlib/addownload/b/a;ZLcom/ss/android/downloadlib/addownload/a/a$a;)V

    goto :goto_1

    :cond_8
    const/4 v6, 0x0

    .line 169
    :goto_1
    sget-object v0, Lcom/ss/android/downloadlib/addownload/a/a;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tryShowInstallDialog isShow:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/ss/android/downloadlib/h/j;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v6

    .line 172
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return v5

    :cond_9
    :goto_3
    return v5
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lcom/ss/android/socialbase/downloader/model/DownloadInfo;
    .locals 13

    const/4 v0, 0x0

    .line 211
    :try_start_0
    invoke-static {p1}, Lcom/ss/android/downloadlib/g;->a(Landroid/content/Context;)Lcom/ss/android/downloadlib/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ss/android/downloadlib/g;->b()J

    move-result-wide v1

    .line 213
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->i()Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "enable_miniapp_dialog"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_0

    return-object v0

    .line 216
    :cond_0
    invoke-static {p1}, Lcom/ss/android/socialbase/downloader/downloader/Downloader;->getInstance(Landroid/content/Context;)Lcom/ss/android/socialbase/downloader/downloader/Downloader;

    move-result-object v3

    const-string v4, "application/vnd.android.package-archive"

    invoke-virtual {v3, v4}, Lcom/ss/android/socialbase/downloader/downloader/Downloader;->getSuccessedDownloadInfosWithMimeType(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 217
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    goto/16 :goto_2

    .line 220
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const-wide/16 v4, 0x0

    move-wide v6, v4

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    if-nez v8, :cond_3

    goto :goto_0

    .line 224
    :cond_3
    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-static {p1, v9}, Lcom/ss/android/downloadlib/h/k;->d(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 225
    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTargetFilePath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/ss/android/downloadlib/h/k;->a(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    goto :goto_0

    .line 228
    :cond_4
    new-instance v9, Ljava/io/File;

    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTargetFilePath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v9}, Ljava/io/File;->lastModified()J

    move-result-wide v9

    cmp-long v11, v9, v1

    if-gez v11, :cond_5

    goto :goto_0

    .line 234
    :cond_5
    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getExtra()Ljava/lang/String;

    move-result-object v11
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v11, :cond_6

    goto :goto_0

    .line 238
    :cond_6
    :try_start_1
    new-instance v11, Lorg/json/JSONObject;

    invoke-virtual {v8}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getExtra()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v12, "isMiniApp"

    .line 239
    invoke-virtual {v11, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v11, :cond_7

    goto :goto_0

    :cond_7
    cmp-long v11, v6, v4

    if-nez v11, :cond_8

    goto :goto_1

    :cond_8
    cmp-long v11, v9, v6

    if-lez v11, :cond_2

    :goto_1
    move-object v0, v8

    move-wide v6, v9

    goto :goto_0

    :catch_0
    move-exception v8

    .line 253
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :cond_9
    :goto_2
    return-object v0

    :catch_1
    move-exception p1

    .line 257
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_a
    return-object v0
.end method

.method public a(JJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15

    move-object v0, p0

    const/4 v1, 0x0

    .line 264
    :goto_0
    iget-object v2, v0, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 265
    iget-object v2, v0, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ss/android/downloadlib/addownload/b/a;

    if-eqz v2, :cond_0

    .line 266
    iget-wide v2, v2, Lcom/ss/android/downloadlib/addownload/b/a;->b:J

    cmp-long v4, v2, p3

    if-nez v4, :cond_0

    .line 267
    iget-object v2, v0, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v3, Lcom/ss/android/downloadlib/addownload/b/a;

    move-object v4, v3

    move-wide/from16 v5, p1

    move-wide/from16 v7, p3

    move-wide/from16 v9, p5

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    invoke-direct/range {v4 .. v14}, Lcom/ss/android/downloadlib/addownload/b/a;-><init>(JJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 268
    iget-object v1, v0, Lcom/ss/android/downloadlib/addownload/a/a;->f:Lcom/ss/android/downloadlib/addownload/a/b;

    const-string v2, "sp_ad_install_back_dialog"

    const-string v3, "key_uninstalled_list"

    iget-object v4, v0, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v2, v3, v4}, Lcom/ss/android/downloadlib/addownload/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList;)V

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 272
    :cond_1
    iget-object v1, v0, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v2, Lcom/ss/android/downloadlib/addownload/b/a;

    move-object v4, v2

    move-wide/from16 v5, p1

    move-wide/from16 v7, p3

    move-wide/from16 v9, p5

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    invoke-direct/range {v4 .. v14}, Lcom/ss/android/downloadlib/addownload/b/a;-><init>(JJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    iget-object v1, v0, Lcom/ss/android/downloadlib/addownload/a/a;->f:Lcom/ss/android/downloadlib/addownload/a/b;

    const-string v2, "sp_ad_install_back_dialog"

    const-string v3, "key_uninstalled_list"

    iget-object v4, v0, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v2, v3, v4}, Lcom/ss/android/downloadlib/addownload/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/CopyOnWriteArrayList;)V

    return-void
.end method

.method public a(Landroid/content/Context;Lcom/ss/android/downloadlib/addownload/b/a;ZLcom/ss/android/downloadlib/addownload/a/a$a;)V
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/ss/android/downloadlib/addownload/a/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 197
    invoke-direct {p0, p1, p2, p4, p3}, Lcom/ss/android/downloadlib/addownload/a/a;->a(Landroid/content/Context;Lcom/ss/android/downloadlib/addownload/b/a;Lcom/ss/android/downloadlib/addownload/a/a$a;Z)V

    const/4 p2, 0x1

    .line 198
    iput-boolean p2, p0, Lcom/ss/android/downloadlib/addownload/a/a;->d:Z

    .line 199
    invoke-static {p1}, Lcom/ss/android/downloadlib/g;->a(Landroid/content/Context;)Lcom/ss/android/downloadlib/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/downloadlib/g;->c()V

    .line 200
    iget-object p1, p0, Lcom/ss/android/downloadlib/addownload/a/a;->f:Lcom/ss/android/downloadlib/addownload/a/b;

    const-string p2, "sp_ad_install_back_dialog"

    const-string p3, "key_uninstalled_list"

    invoke-virtual {p1, p2, p3}, Lcom/ss/android/downloadlib/addownload/a/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    sget-object p1, Lcom/ss/android/downloadlib/addownload/a/a;->a:Ljava/lang/String;

    const-string p2, "tryShowInstallDialog isShow:true"

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lcom/ss/android/downloadlib/h/j;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    return-void
.end method

.method public a(Lcom/ss/android/b/a/b/b;)V
    .locals 3

    .line 336
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "enable_open_app_dialog"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    return-void

    .line 339
    :cond_0
    invoke-virtual {p1}, Lcom/ss/android/b/a/b/b;->Y()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/ss/android/b/a/b/b;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    invoke-virtual {p1, v1}, Lcom/ss/android/b/a/b/b;->k(Z)V

    .line 341
    invoke-static {p1}, Lcom/ss/android/downloadlib/activity/TTDelegateActivity;->a(Lcom/ss/android/b/a/b/a;)V

    :cond_1
    return-void
.end method

.method public a(Landroid/app/Activity;ZLcom/ss/android/downloadlib/addownload/a/a$a;)Z
    .locals 2
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 182
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "disable_install_app_dialog"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/ss/android/downloadlib/addownload/a/a;->d:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 186
    :cond_0
    invoke-virtual {p0, p1}, Lcom/ss/android/downloadlib/addownload/a/a;->a(Landroid/content/Context;)Lcom/ss/android/socialbase/downloader/model/DownloadInfo;

    move-result-object v0

    .line 187
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/ss/android/downloadlib/addownload/a/a;->a(Landroid/app/Activity;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;ZLcom/ss/android/downloadlib/addownload/a/a$a;)Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/ss/android/downloadlib/addownload/a/a;->e:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 325
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    .line 326
    iput-object p1, p0, Lcom/ss/android/downloadlib/addownload/a/a;->e:Ljava/lang/String;

    goto :goto_0

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/ss/android/downloadlib/addownload/a/a;->e:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, ""

    .line 328
    iput-object p1, p0, Lcom/ss/android/downloadlib/addownload/a/a;->e:Ljava/lang/String;

    :cond_1
    :goto_0
    return-void
.end method
