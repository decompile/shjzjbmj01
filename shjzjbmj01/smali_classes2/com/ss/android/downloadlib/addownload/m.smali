.class public Lcom/ss/android/downloadlib/addownload/m;
.super Ljava/lang/Object;
.source "ReverseWifiHelper.java"


# static fields
.field private static a:Lcom/ss/android/downloadlib/addownload/a/c;


# direct methods
.method public static a()Lcom/ss/android/downloadlib/addownload/a/c;
    .locals 1

    .line 30
    sget-object v0, Lcom/ss/android/downloadlib/addownload/m;->a:Lcom/ss/android/downloadlib/addownload/a/c;

    return-object v0
.end method

.method public static a(Lcom/ss/android/downloadlib/addownload/a/c;)V
    .locals 0

    .line 34
    sput-object p0, Lcom/ss/android/downloadlib/addownload/m;->a:Lcom/ss/android/downloadlib/addownload/a/c;

    return-void
.end method

.method public static a(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-eq p0, v1, :cond_1

    const/4 v1, 0x3

    if-eq p0, v1, :cond_1

    const/4 v1, 0x4

    if-eq p0, v1, :cond_1

    const/4 v1, 0x5

    if-eq p0, v1, :cond_1

    const/4 v1, 0x7

    if-eq p0, v1, :cond_1

    const/16 v1, 0x8

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method public static a(Lcom/ss/android/b/a/b/b;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;ILcom/ss/android/downloadlib/addownload/d/c;)Z
    .locals 6

    const/4 v0, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_0

    goto :goto_2

    .line 42
    :cond_0
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result v1

    .line 43
    invoke-static {p0}, Lcom/ss/android/downloadlib/h/e;->b(Lcom/ss/android/b/a/b/a;)Z

    move-result v2

    .line 44
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v4, "switch_status"

    .line 46
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 48
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 50
    :goto_0
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v4

    const-string v5, "pause_reserve_wifi_switch_status"

    invoke-virtual {v4, v5, v3, p0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/b;)V

    if-nez v2, :cond_1

    return v0

    .line 55
    :cond_1
    invoke-static {p2}, Lcom/ss/android/downloadlib/addownload/m;->a(I)Z

    move-result p2

    if-nez p2, :cond_2

    return v0

    .line 58
    :cond_2
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/ss/android/socialbase/downloader/i/e;->b(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_3

    .line 61
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->isPauseReserveOnWifi()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 62
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->stopPauseReserveOnWifi()V

    .line 63
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object p1

    const-string p2, "pause_reserve_wifi_cancel_on_wifi"

    invoke-virtual {p1, p2, p0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)V

    goto :goto_1

    .line 65
    :cond_3
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->hasPauseReservedOnWifi()Z

    move-result p1

    if-nez p1, :cond_4

    .line 67
    new-instance p1, Lcom/ss/android/downloadlib/addownload/m$1;

    invoke-direct {p1, v1, p0, p3}, Lcom/ss/android/downloadlib/addownload/m$1;-><init>(ILcom/ss/android/b/a/b/b;Lcom/ss/android/downloadlib/addownload/d/c;)V

    .line 92
    invoke-static {p1}, Lcom/ss/android/downloadlib/addownload/m;->a(Lcom/ss/android/downloadlib/addownload/a/c;)V

    .line 93
    invoke-static {p0}, Lcom/ss/android/downloadlib/activity/TTDelegateActivity;->b(Lcom/ss/android/b/a/b/a;)V

    const/4 p0, 0x1

    return p0

    :cond_4
    :goto_1
    return v0

    .line 39
    :cond_5
    :goto_2
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    return v0
.end method
