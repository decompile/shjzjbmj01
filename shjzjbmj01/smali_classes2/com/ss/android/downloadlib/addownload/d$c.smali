.class Lcom/ss/android/downloadlib/addownload/d$c;
.super Ljava/lang/Object;
.source "ChunkCalculatorFactory.java"

# interfaces
.implements Lcom/ss/android/socialbase/downloader/downloader/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ss/android/downloadlib/addownload/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "[I>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ss/android/downloadlib/addownload/d$c;->b:Ljava/util/ArrayList;

    .line 86
    invoke-direct {p0, p1}, Lcom/ss/android/downloadlib/addownload/d$c;->a(Lorg/json/JSONObject;)V

    return-void
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 2

    const-string v0, "is_open_exp"

    const/4 v1, 0x0

    .line 90
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/ss/android/downloadlib/addownload/d$c;->a:I

    .line 91
    invoke-direct {p0, p1}, Lcom/ss/android/downloadlib/addownload/d$c;->b(Lorg/json/JSONObject;)V

    return-void
.end method

.method private b(ILcom/ss/android/socialbase/downloader/network/l;)I
    .locals 4

    .line 159
    iget-object v0, p0, Lcom/ss/android/downloadlib/addownload/d$c;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    return p1

    :cond_0
    const/4 v0, 0x0

    .line 163
    sget-object v1, Lcom/ss/android/downloadlib/addownload/d$1;->a:[I

    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/network/l;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 177
    :pswitch_0
    iget-object p2, p0, Lcom/ss/android/downloadlib/addownload/d$c;->b:Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    move-object v0, p2

    check-cast v0, [I

    goto :goto_0

    .line 174
    :pswitch_1
    iget-object p2, p0, Lcom/ss/android/downloadlib/addownload/d$c;->b:Ljava/util/ArrayList;

    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    move-object v0, p2

    check-cast v0, [I

    goto :goto_0

    .line 171
    :pswitch_2
    iget-object p2, p0, Lcom/ss/android/downloadlib/addownload/d$c;->b:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    move-object v0, p2

    check-cast v0, [I

    goto :goto_0

    .line 168
    :pswitch_3
    iget-object p2, p0, Lcom/ss/android/downloadlib/addownload/d$c;->b:Ljava/util/ArrayList;

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    move-object v0, p2

    check-cast v0, [I

    goto :goto_0

    .line 165
    :pswitch_4
    iget-object p2, p0, Lcom/ss/android/downloadlib/addownload/d$c;->b:Ljava/util/ArrayList;

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    move-object v0, p2

    check-cast v0, [I

    :goto_0
    if-eqz v0, :cond_3

    .line 182
    array-length p2, v0

    if-ge p2, v1, :cond_1

    goto :goto_3

    .line 185
    :cond_1
    aget p2, v0, v2

    packed-switch p2, :pswitch_data_1

    goto :goto_1

    .line 193
    :pswitch_5
    aget p1, v0, v3

    goto :goto_1

    .line 190
    :pswitch_6
    aget p2, v0, v3

    sub-int/2addr p1, p2

    goto :goto_1

    .line 187
    :pswitch_7
    aget p2, v0, v3

    add-int/2addr p1, p2

    :goto_1
    if-le p1, v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 p1, 0x1

    :goto_2
    return p1

    :cond_3
    :goto_3
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method private b(Lorg/json/JSONObject;)V
    .locals 8

    if-eqz p1, :cond_8

    const-string v0, "download_chunk_config"

    .line 95
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    :cond_0
    const-string v0, "download_chunk_config"

    .line 111
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 112
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    .line 117
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 119
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    if-nez v0, :cond_2

    return-void

    :cond_2
    const-string p1, "network_quality_operation"

    .line 125
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "network_quality_operand"

    .line 126
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_3

    :cond_3
    :try_start_1
    const-string v1, ","

    .line 131
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const-string v1, ","

    .line 132
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 133
    array-length v1, p1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_5

    array-length v1, v0

    if-ge v1, v2, :cond_4

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_6

    .line 137
    aget-object v4, p1, v3

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 138
    aget-object v5, v0, v3

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 139
    iget-object v6, p0, Lcom/ss/android/downloadlib/addownload/d$c;->b:Ljava/util/ArrayList;

    const/4 v7, 0x2

    new-array v7, v7, [I

    aput v4, v7, v1

    const/4 v4, 0x1

    aput v5, v7, v4

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    :goto_2
    return-void

    :catch_1
    :cond_6
    return-void

    :cond_7
    :goto_3
    return-void

    :cond_8
    :goto_4
    return-void
.end method


# virtual methods
.method public a(ILcom/ss/android/socialbase/downloader/network/l;)I
    .locals 1

    .line 147
    invoke-virtual {p0}, Lcom/ss/android/downloadlib/addownload/d$c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-direct {p0, p1, p2}, Lcom/ss/android/downloadlib/addownload/d$c;->b(ILcom/ss/android/socialbase/downloader/network/l;)I

    move-result p1

    return p1

    :cond_0
    return p1
.end method

.method a()Z
    .locals 2

    .line 154
    iget v0, p0, Lcom/ss/android/downloadlib/addownload/d$c;->a:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/ss/android/downloadlib/addownload/d$c;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
