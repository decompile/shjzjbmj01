.class public Lcom/ss/android/downloadlib/addownload/compliance/b;
.super Ljava/lang/Object;
.source "AdLpComplianceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ss/android/downloadlib/addownload/compliance/b$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/ss/android/downloadlib/addownload/compliance/b$1;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/ss/android/downloadlib/addownload/compliance/b;-><init>()V

    return-void
.end method

.method public static a()Lcom/ss/android/downloadlib/addownload/compliance/b;
    .locals 1

    .line 45
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/compliance/b$a;->a()Lcom/ss/android/downloadlib/addownload/compliance/b;

    move-result-object v0

    return-object v0
.end method

.method private a(JJLjava/lang/String;)Z
    .locals 9

    const/4 v0, 0x7

    const/4 v1, 0x0

    .line 184
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p5, "package"

    .line 185
    invoke-virtual {v2, p5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p5

    if-eqz p5, :cond_2

    .line 186
    invoke-virtual {p5}, Lorg/json/JSONObject;->length()I

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_1

    .line 190
    :cond_0
    new-instance v2, Lcom/ss/android/downloadlib/addownload/b/b;

    invoke-direct {v2}, Lcom/ss/android/downloadlib/addownload/b/b;-><init>()V

    .line 191
    iput-wide p1, v2, Lcom/ss/android/downloadlib/addownload/b/b;->a:J

    .line 192
    iput-wide p3, v2, Lcom/ss/android/downloadlib/addownload/b/b;->b:J

    const-string p1, "icon_url"

    .line 193
    invoke-virtual {p5, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v2, Lcom/ss/android/downloadlib/addownload/b/b;->d:Ljava/lang/String;

    const-string p1, "app_name"

    .line 196
    invoke-virtual {p5, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v2, Lcom/ss/android/downloadlib/addownload/b/b;->e:Ljava/lang/String;

    const-string p1, "package_name"

    .line 197
    invoke-virtual {p5, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v2, Lcom/ss/android/downloadlib/addownload/b/b;->c:Ljava/lang/String;

    const-string p1, "version_name"

    .line 198
    invoke-virtual {p5, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v2, Lcom/ss/android/downloadlib/addownload/b/b;->f:Ljava/lang/String;

    const-string p1, "developer_name"

    .line 199
    invoke-virtual {p5, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v2, Lcom/ss/android/downloadlib/addownload/b/b;->g:Ljava/lang/String;

    const-string p1, "policy_url"

    .line 200
    invoke-virtual {p5, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v2, Lcom/ss/android/downloadlib/addownload/b/b;->i:Ljava/lang/String;

    const-string p1, "permissions"

    .line 202
    invoke-virtual {p5, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 p2, 0x0

    .line 204
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result p5

    if-ge p2, p5, :cond_1

    .line 205
    invoke-virtual {p1, p2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lorg/json/JSONObject;

    .line 206
    iget-object v3, v2, Lcom/ss/android/downloadlib/addownload/b/b;->h:Ljava/util/List;

    new-instance v4, Landroid/util/Pair;

    const-string v5, "permission_name"

    .line 207
    invoke-virtual {p5, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "permission_desc"

    .line 208
    invoke-virtual {p5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    invoke-direct {v4, v5, p5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 206
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 212
    :cond_1
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/compliance/c;->a()Lcom/ss/android/downloadlib/addownload/compliance/c;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/ss/android/downloadlib/addownload/compliance/c;->a(Lcom/ss/android/downloadlib/addownload/b/b;)V

    .line 213
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/compliance/d;->a()Lcom/ss/android/downloadlib/addownload/compliance/d;

    move-result-object v3

    invoke-virtual {v2}, Lcom/ss/android/downloadlib/addownload/b/b;->a()J

    move-result-wide v4

    iget-object v8, v2, Lcom/ss/android/downloadlib/addownload/b/b;->d:Ljava/lang/String;

    move-wide v6, p3

    invoke-virtual/range {v3 .. v8}, Lcom/ss/android/downloadlib/addownload/compliance/d;->a(JJLjava/lang/String;)V

    const/4 p1, 0x1

    return p1

    .line 187
    :cond_2
    :goto_1
    invoke-static {v0, p3, p4}, Lcom/ss/android/downloadlib/addownload/compliance/e;->a(IJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    move-exception p1

    .line 216
    invoke-static {p1}, Lcom/ss/android/downloadlib/f/a;->a(Ljava/lang/Throwable;)V

    .line 217
    invoke-static {v0, p3, p4}, Lcom/ss/android/downloadlib/addownload/compliance/e;->a(IJ)V

    return v1
.end method

.method static synthetic a(Lcom/ss/android/downloadlib/addownload/compliance/b;JJLjava/lang/String;)Z
    .locals 0

    .line 38
    invoke-direct/range {p0 .. p5}, Lcom/ss/android/downloadlib/addownload/compliance/b;->a(JJLjava/lang/String;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public a(J)V
    .locals 0

    .line 55
    invoke-static {p1, p2}, Lcom/ss/android/downloadlib/activity/TTDelegateActivity;->a(J)V

    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .line 235
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/ss/android/downloadlib/addownload/compliance/b;->a:Ljava/lang/ref/SoftReference;

    return-void
.end method

.method public a(Lcom/ss/android/a/a/b/c;)Z
    .locals 4

    .line 62
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->t()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 65
    :cond_0
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "ad_lp_show_app_dialog"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    return v1

    .line 68
    :cond_1
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->x()Lcom/ss/android/a/a/c/b;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 p1, 0x0

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->x()Lcom/ss/android/a/a/c/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ss/android/a/a/c/b;->a()Ljava/lang/String;

    move-result-object p1

    .line 69
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    return v1

    .line 72
    :cond_3
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "ad_allow_web_url_regex"

    const-string v3, ".+(www.chengzijianzhan.com|www.toutiaopage.com/tetris/page|ad.toutiao.com/tetris/page).+"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->matches()Z

    move-result p1

    if-eqz p1, :cond_4

    return v1

    :cond_4
    const/4 p1, 0x1

    return p1
.end method

.method public a(Lcom/ss/android/downloadlib/addownload/b/e;)Z
    .locals 14
    .param p1    # Lcom/ss/android/downloadlib/addownload/b/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 84
    iget-object v0, p1, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v0}, Lcom/ss/android/a/a/b/c;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-wide/16 v1, 0x0

    if-nez v0, :cond_1

    .line 86
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v3, p1, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v3}, Lcom/ss/android/a/a/b/c;->u()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "convert_id"

    .line 87
    invoke-static {v0, v3}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 89
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-wide v3, v1

    :goto_0
    cmp-long v0, v3, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x3

    .line 92
    invoke-static {v0, p1}, Lcom/ss/android/downloadlib/addownload/compliance/e;->a(ILcom/ss/android/downloadlib/addownload/b/e;)V

    :cond_0
    move-wide v9, v3

    goto :goto_1

    :cond_1
    const/16 v0, 0x9

    .line 95
    invoke-static {v0, p1}, Lcom/ss/android/downloadlib/addownload/compliance/e;->a(ILcom/ss/android/downloadlib/addownload/b/e;)V

    .line 96
    invoke-static {}, Lcom/ss/android/downloadlib/f/a;->a()V

    move-wide v9, v1

    .line 100
    :goto_1
    iget-wide v11, p1, Lcom/ss/android/downloadlib/addownload/b/e;->a:J

    .line 102
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/compliance/c;->a()Lcom/ss/android/downloadlib/addownload/compliance/c;

    move-result-object v0

    invoke-virtual {v0, v9, v10, v11, v12}, Lcom/ss/android/downloadlib/addownload/compliance/c;->a(JJ)Lcom/ss/android/downloadlib/addownload/b/b;

    move-result-object v0

    const/4 v13, 0x1

    if-eqz v0, :cond_2

    .line 105
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/compliance/d;->a()Lcom/ss/android/downloadlib/addownload/compliance/d;

    move-result-object v3

    invoke-virtual {v0}, Lcom/ss/android/downloadlib/addownload/b/b;->a()J

    move-result-wide v4

    iget-object v8, v0, Lcom/ss/android/downloadlib/addownload/b/b;->d:Ljava/lang/String;

    move-wide v6, v11

    invoke-virtual/range {v3 .. v8}, Lcom/ss/android/downloadlib/addownload/compliance/d;->a(JJLjava/lang/String;)V

    .line 106
    invoke-virtual {v0}, Lcom/ss/android/downloadlib/addownload/b/b;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/ss/android/downloadlib/addownload/compliance/b;->a(J)V

    const-string v0, "lp_app_dialog_try_show"

    .line 107
    invoke-static {v0, p1}, Lcom/ss/android/downloadlib/addownload/compliance/e;->a(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/e;)V

    return v13

    .line 111
    :cond_2
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->e()Lcom/ss/android/a/a/a/g;

    move-result-object v0

    const/4 v3, 0x0

    if-nez v0, :cond_3

    .line 112
    invoke-static {}, Lcom/ss/android/downloadlib/f/a;->a()V

    .line 113
    invoke-static {v13, p1}, Lcom/ss/android/downloadlib/addownload/compliance/e;->a(ILcom/ss/android/downloadlib/addownload/b/e;)V

    return v3

    .line 117
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    cmp-long v4, v9, v1

    if-lez v4, :cond_4

    const-string v1, "convert_id="

    .line 119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 121
    :cond_4
    iget-object v1, p1, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v1}, Lcom/ss/android/a/a/b/c;->v()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 122
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_5

    const-string v1, "&"

    .line 123
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, "package_name="

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v1}, Lcom/ss/android/a/a/b/c;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-gtz v1, :cond_7

    const/4 v0, 0x6

    .line 129
    invoke-static {v0, p1}, Lcom/ss/android/downloadlib/addownload/compliance/e;->a(ILcom/ss/android/downloadlib/addownload/b/e;)V

    return v3

    .line 134
    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://apps.oceanengine.com/customer/api/app/pkg_info?"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 136
    new-instance v0, Lcom/ss/android/downloadlib/addownload/compliance/b$2;

    move-object v3, v0

    move-object v4, p0

    move-wide v5, v9

    move-wide v7, v11

    invoke-direct/range {v3 .. v8}, Lcom/ss/android/downloadlib/addownload/compliance/b$2;-><init>(Lcom/ss/android/downloadlib/addownload/compliance/b;JJ)V

    invoke-static {v0, p1}, Lcom/ss/android/downloadlib/h/c;->a(Lcom/ss/android/downloadlib/h/c$a;Ljava/lang/Object;)Lcom/ss/android/downloadlib/h/c;

    move-result-object p1

    new-instance v0, Lcom/ss/android/downloadlib/addownload/compliance/b$1;

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Lcom/ss/android/downloadlib/addownload/compliance/b$1;-><init>(Lcom/ss/android/downloadlib/addownload/compliance/b;JJ)V

    .line 161
    invoke-virtual {p1, v0}, Lcom/ss/android/downloadlib/h/c;->a(Lcom/ss/android/downloadlib/h/c$a;)Lcom/ss/android/downloadlib/h/c;

    move-result-object p1

    .line 172
    invoke-virtual {p1}, Lcom/ss/android/downloadlib/h/c;->a()V

    return v13
.end method

.method public b()Landroid/app/Activity;
    .locals 2

    .line 239
    iget-object v0, p0, Lcom/ss/android/downloadlib/addownload/compliance/b;->a:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x0

    .line 240
    iput-object v1, p0, Lcom/ss/android/downloadlib/addownload/compliance/b;->a:Ljava/lang/ref/SoftReference;

    return-object v0
.end method

.method public b(J)V
    .locals 2

    .line 224
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/ss/android/downloadlib/addownload/b/f;->e(J)Lcom/ss/android/downloadlib/addownload/b/e;

    move-result-object v0

    .line 225
    invoke-static {}, Lcom/ss/android/downloadlib/f;->a()Lcom/ss/android/downloadlib/f;

    move-result-object v1

    iget-object v0, v0, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v0}, Lcom/ss/android/a/a/b/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/ss/android/downloadlib/f;->a(Ljava/lang/String;)Lcom/ss/android/downloadlib/addownload/f;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .line 227
    invoke-virtual {v0, p1}, Lcom/ss/android/downloadlib/addownload/f;->b(Z)V

    goto :goto_0

    :cond_0
    const/16 v0, 0xb

    .line 229
    invoke-static {v0, p1, p2}, Lcom/ss/android/downloadlib/addownload/compliance/e;->a(IJ)V

    .line 230
    invoke-static {}, Lcom/ss/android/downloadlib/f/a;->a()V

    :goto_0
    return-void
.end method
