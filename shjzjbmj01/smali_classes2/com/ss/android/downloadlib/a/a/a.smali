.class public abstract Lcom/ss/android/downloadlib/a/a/a;
.super Ljava/lang/Object;
.source "AutoParcelable.java"

# interfaces
.implements Landroid/os/Parcelable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;I)I
    .locals 1

    const/high16 v0, -0x10000

    or-int/2addr p1, v0

    .line 17
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 p1, 0x0

    .line 18
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 19
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result p0

    return p0
.end method

.method private static a(Landroid/os/Parcel;II)V
    .locals 1

    const v0, 0xffff

    if-lt p2, v0, :cond_0

    const/high16 v0, -0x10000

    or-int/2addr p1, v0

    .line 24
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 25
    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    shl-int/lit8 p2, p2, 0x10

    or-int/2addr p1, p2

    .line 28
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public static a(Landroid/os/Parcel;Ljava/lang/reflect/Field;ILandroid/os/Parcelable;IZ)V
    .locals 0

    if-nez p3, :cond_1

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    .line 72
    invoke-static {p0, p2, p1}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;II)V

    :cond_0
    return-void

    .line 76
    :cond_1
    invoke-static {p0, p2}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;I)I

    move-result p1

    .line 78
    invoke-interface {p3, p0, p4}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    .line 79
    invoke-static {p0, p1}, Lcom/ss/android/downloadlib/a/a/a;->b(Landroid/os/Parcel;I)V

    return-void
.end method

.method public static a(Landroid/os/Parcel;Ljava/lang/reflect/Field;ILjava/lang/Integer;IZ)V
    .locals 0

    if-nez p3, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x4

    .line 64
    invoke-static {p0, p2, p1}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;II)V

    .line 66
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public static a(Landroid/os/Parcel;Ljava/lang/reflect/Field;ILjava/lang/Object;IZ)V
    .locals 11

    .line 84
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/lang/String;

    if-ne v0, v1, :cond_0

    .line 85
    move-object v5, p3

    check-cast v5, Ljava/lang/String;

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v6, p4

    move/from16 v7, p5

    invoke-static/range {v2 .. v7}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;Ljava/lang/reflect/Field;ILjava/lang/String;IZ)V

    goto :goto_1

    .line 86
    :cond_0
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/lang/Integer;

    if-eq v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 88
    :cond_1
    const-class v0, Landroid/os/Parcelable;

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    move-object v4, p3

    check-cast v4, Landroid/os/Parcelable;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v5, p4

    move/from16 v6, p5

    invoke-static/range {v1 .. v6}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;Ljava/lang/reflect/Field;ILandroid/os/Parcelable;IZ)V

    goto :goto_1

    .line 87
    :cond_2
    :goto_0
    move-object v8, p3

    check-cast v8, Ljava/lang/Integer;

    move-object v5, p0

    move-object v6, p1

    move v7, p2

    move v9, p4

    move/from16 v10, p5

    invoke-static/range {v5 .. v10}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;Ljava/lang/reflect/Field;ILjava/lang/Integer;IZ)V

    :cond_3
    :goto_1
    return-void
.end method

.method public static a(Landroid/os/Parcel;Ljava/lang/reflect/Field;ILjava/lang/String;IZ)V
    .locals 0

    .line 54
    invoke-static {p0, p2}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;I)I

    move-result p1

    .line 56
    invoke-virtual {p0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 57
    invoke-static {p0, p1}, Lcom/ss/android/downloadlib/a/a/a;->b(Landroid/os/Parcel;I)V

    return-void
.end method

.method private static a(Lcom/ss/android/downloadlib/a/a/a;Landroid/os/Parcel;Ljava/lang/Class;)V
    .locals 4

    .line 39
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object p0

    const/4 p2, 0x0

    .line 41
    invoke-static {p1, p2}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;I)I

    move-result p2

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    array-length v1, p0

    if-lez v1, :cond_0

    .line 44
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->i()Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "s"

    .line 45
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "aw"

    .line 46
    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/ss/android/socialbase/appdownloader/f/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_0
    array-length p0, p0

    new-array p0, p0, [Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/String;

    invoke-virtual {p1, p0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 50
    invoke-static {p1, p2}, Lcom/ss/android/downloadlib/a/a/a;->b(Landroid/os/Parcel;I)V

    return-void
.end method

.method private static a(Lcom/ss/android/downloadlib/a/a/a;Landroid/os/Parcel;Ljava/lang/reflect/Field;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .line 96
    const-class v0, Lcom/ss/android/downloadlib/a/a/h;

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/ss/android/downloadlib/a/a/h;

    invoke-interface {v0}, Lcom/ss/android/downloadlib/a/a/h;->b()I

    move-result v3

    .line 97
    const-class v0, Lcom/ss/android/downloadlib/a/a/h;

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/ss/android/downloadlib/a/a/h;

    invoke-interface {v0}, Lcom/ss/android/downloadlib/a/a/h;->a()Z

    move-result v6

    .line 98
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->isAccessible()Z

    move-result v0

    const/4 v1, 0x1

    .line 99
    invoke-virtual {p2, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 100
    invoke-virtual {p2, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-static/range {v1 .. v6}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;Ljava/lang/reflect/Field;ILjava/lang/Object;IZ)V

    .line 101
    invoke-virtual {p2, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    return-void
.end method

.method private static b(Landroid/os/Parcel;I)V
    .locals 2

    .line 32
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    add-int/lit8 v1, p1, -0x4

    .line 33
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    sub-int p1, v0, p1

    .line 34
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 35
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .line 105
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/16 v1, 0x4f45

    .line 106
    invoke-static {p1, v1}, Lcom/ss/android/downloadlib/a/a/a;->a(Landroid/os/Parcel;I)I

    move-result v1

    .line 107
    invoke-static {p0, p1, v0}, Lcom/ss/android/downloadlib/a/a/a;->a(Lcom/ss/android/downloadlib/a/a/a;Landroid/os/Parcel;Ljava/lang/Class;)V

    :goto_0
    if-eqz v0, :cond_2

    .line 109
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    const/4 v3, 0x0

    .line 111
    :goto_1
    array-length v4, v2

    if-ge v3, v4, :cond_1

    .line 112
    aget-object v4, v2, v3

    .line 113
    const-class v5, Lcom/ss/android/downloadlib/a/a/h;

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 115
    :try_start_0
    invoke-static {p0, p1, v4, p2}, Lcom/ss/android/downloadlib/a/a/a;->a(Lcom/ss/android/downloadlib/a/a/a;Landroid/os/Parcel;Ljava/lang/reflect/Field;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 121
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 123
    :cond_2
    invoke-static {p1, v1}, Lcom/ss/android/downloadlib/a/a/a;->b(Landroid/os/Parcel;I)V

    return-void
.end method
