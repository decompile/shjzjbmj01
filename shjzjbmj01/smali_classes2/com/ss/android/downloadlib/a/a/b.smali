.class public Lcom/ss/android/downloadlib/a/a/b;
.super Ljava/lang/Object;
.source "HiddenAPIEnforcementPolicyUtils.java"


# static fields
.field private static a:Ljava/lang/reflect/Method; = null

.field private static b:Ljava/lang/Object; = null

.field private static c:Z = false


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public static a()V
    .locals 1

    .line 65
    sget-boolean v0, Lcom/ss/android/downloadlib/a/a/b;->c:Z

    if-eqz v0, :cond_0

    return-void

    .line 68
    :cond_0
    invoke-static {}, Lcom/ss/android/downloadlib/a/a/b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    :try_start_0
    invoke-static {}, Lcom/ss/android/downloadlib/a/a/b;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 73
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 76
    sput-boolean v0, Lcom/ss/android/downloadlib/a/a/b;->c:Z

    return-void
.end method

.method private static varargs a([Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 45
    sget-object v0, Lcom/ss/android/downloadlib/a/a/b;->a:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/ss/android/downloadlib/a/a/b;->b:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static b()V
    .locals 9

    .line 24
    :try_start_0
    const-class v0, Ljava/lang/Class;

    const-string v1, "getDeclaredMethod"

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-class v4, [Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 25
    const-class v1, Ljava/lang/Class;

    const-string v3, "forName"

    new-array v4, v6, [Ljava/lang/Class;

    const-class v7, Ljava/lang/String;

    aput-object v7, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 26
    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "dalvik.system.VMRuntime"

    aput-object v4, v3, v5

    const/4 v4, 0x0

    invoke-virtual {v1, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 27
    new-array v3, v2, [Ljava/lang/Object;

    const-string v7, "setHiddenApiExemptions"

    aput-object v7, v3, v5

    new-array v7, v6, [Ljava/lang/Class;

    const-class v8, [Ljava/lang/String;

    aput-object v8, v7, v5

    aput-object v7, v3, v6

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/reflect/Method;

    sput-object v3, Lcom/ss/android/downloadlib/a/a/b;->a:Ljava/lang/reflect/Method;

    .line 28
    new-array v3, v2, [Ljava/lang/Object;

    const-string v7, "getRuntime"

    aput-object v7, v3, v5

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Method;

    .line 29
    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Lcom/ss/android/downloadlib/a/a/b;->b:Ljava/lang/Object;

    const/4 v0, 0x6

    .line 31
    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Landroid/"

    aput-object v1, v0, v5

    const-string v1, "Lcom/android/"

    aput-object v1, v0, v6

    const-string v1, "Ljava/lang/"

    aput-object v1, v0, v2

    const/4 v1, 0x3

    const-string v2, "Ldalvik/system/"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Llibcore/io/"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Lsun/misc/"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/ss/android/downloadlib/a/a/b;->a([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private static c()I
    .locals 2

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 51
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->PREVIEW_SDK_INT:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private static d()Z
    .locals 2

    .line 61
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1b

    if-gt v0, v1, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/ss/android/downloadlib/a/a/b;->c()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
