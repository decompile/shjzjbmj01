.class public Lcom/ss/android/downloadlib/b/a;
.super Ljava/lang/Object;
.source "AdAppLinkUtils.java"


# direct methods
.method public static a(Lcom/ss/android/b/a/b/b;)V
    .locals 5
    .param p0    # Lcom/ss/android/b/a/b/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 107
    invoke-virtual {p0}, Lcom/ss/android/b/a/b/b;->f()Ljava/lang/String;

    move-result-object v0

    .line 108
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    invoke-static {v1, p0}, Lcom/ss/android/downloadlib/h/f;->a(Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "applink_source"

    const-string v3, "notify_click_by_sdk"

    .line 111
    invoke-static {v1, v2, v3}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 114
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v2

    const-string v3, "applink_click"

    invoke-virtual {v2, v3, v1, p0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 116
    invoke-static {v0, p0}, Lcom/ss/android/downloadlib/h/h;->b(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)Lcom/ss/android/downloadlib/addownload/b/g;

    move-result-object v2

    .line 117
    invoke-virtual {v2}, Lcom/ss/android/downloadlib/addownload/b/g;->a()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 118
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "notify_by_url"

    .line 119
    invoke-static {v0, v2, v1, p0}, Lcom/ss/android/downloadlib/b/a;->b(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 121
    :cond_0
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/ss/android/b/a/b/b;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p0}, Lcom/ss/android/downloadlib/h/h;->a(Landroid/content/Context;Ljava/lang/String;Lcom/ss/android/b/a/b/a;)Lcom/ss/android/downloadlib/addownload/b/g;

    move-result-object v2

    .line 124
    :cond_1
    invoke-virtual {v2}, Lcom/ss/android/downloadlib/addownload/b/g;->a()I

    move-result v0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_2

    packed-switch v0, :pswitch_data_0

    .line 135
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    goto :goto_0

    :pswitch_0
    const-string v0, "notify_by_package"

    .line 132
    invoke-static {v0, v2, v1, p0}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "notify_by_package"

    .line 126
    invoke-static {v0, v1, p0}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    goto :goto_0

    :cond_2
    const-string v0, "notify_by_url"

    .line 129
    invoke-static {v0, v1, p0}, Lcom/ss/android/downloadlib/b/a;->b(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/ss/android/downloadlib/addownload/b/g;Lcom/ss/android/downloadlib/addownload/b/e;)V
    .locals 4

    const/4 v0, 0x2

    .line 370
    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/ss/android/downloadlib/addownload/b/g;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "open_market"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/ss/android/downloadlib/h/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 371
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "ttdownloader_type"

    const-string v3, "backup"

    .line 372
    invoke-static {v1, v2, v3}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 373
    invoke-virtual {p0}, Lcom/ss/android/downloadlib/addownload/b/g;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "error_code"

    .line 378
    invoke-virtual {p0}, Lcom/ss/android/downloadlib/addownload/b/g;->b()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-static {v1, v0, p0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 379
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object p0

    const-string v0, "market_open_failed"

    invoke-virtual {p0, v0, v1, p1}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    goto :goto_0

    .line 375
    :pswitch_1
    invoke-static {v0, v1, p1}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/downloadlib/addownload/b/e;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V
    .locals 1
    .param p1    # Lcom/ss/android/downloadlib/addownload/b/g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/ss/android/b/a/b/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "applink_source"

    .line 306
    invoke-static {p2, v0, p0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string p0, "error_code"

    .line 307
    invoke-virtual {p1}, Lcom/ss/android/downloadlib/addownload/b/g;->b()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2, p0, p1}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 308
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object p0

    const-string p1, "deeplink_app_open_fail"

    invoke-virtual {p0, p1, p2, p3}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V
    .locals 9
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/ss/android/b/a/b/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "applink_source"

    .line 234
    invoke-static {p1, v0, p0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 235
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v0

    const-string v1, "deeplink_app_open"

    invoke-virtual {v0, v1, p1, p2}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 237
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x4c6ad8ec

    const/4 v2, 0x1

    if-eq v0, v1, :cond_3

    const v1, -0x1a50fb12

    if-eq v0, v1, :cond_2

    const v1, -0xb155fa2

    if-eq v0, v1, :cond_1

    const v1, 0x15f55bb5

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_by_package"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_1
    const-string v0, "by_package"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    goto :goto_1

    :cond_2
    const-string v0, "auto_by_package"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const-string v0, "notify_by_package"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 240
    :pswitch_0
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "download_sdk_optimize_mode"

    .line 241
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v2, :cond_5

    const-string p0, "check_applink_result_opt"

    .line 242
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p0, v0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 243
    invoke-static {}, Lcom/ss/android/downloadlib/b/e;->a()Lcom/ss/android/downloadlib/b/e;

    move-result-object p0

    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/ss/android/downloadlib/b/a$1;

    invoke-direct {v1, p1, p2}, Lcom/ss/android/downloadlib/b/a$1;-><init>(Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    invoke-virtual {p0, v0, v1}, Lcom/ss/android/downloadlib/b/e;->a(Ljava/lang/String;Lcom/ss/android/downloadlib/b/d;)V

    goto :goto_2

    .line 260
    :cond_5
    :pswitch_1
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->c()Lcom/ss/android/a/a/a/c;

    move-result-object v2

    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v3

    .line 261
    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->t()Lcom/ss/android/a/a/b/c;

    move-result-object v4

    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->v()Lcom/ss/android/a/a/b/a;

    move-result-object v5

    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->u()Lcom/ss/android/a/a/b/b;

    move-result-object v6

    .line 262
    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->e()Ljava/lang/String;

    move-result-object v7

    move-object v8, p0

    .line 260
    invoke-interface/range {v2 .. v8}, Lcom/ss/android/a/a/a/c;->a(Landroid/content/Context;Lcom/ss/android/a/a/b/c;Lcom/ss/android/a/a/b/a;Lcom/ss/android/a/a/b/b;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/downloadlib/addownload/b/e;)V
    .locals 9
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    goto :goto_0

    .line 348
    :cond_0
    :try_start_0
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    :goto_0
    const-string v0, "applink_source"

    .line 349
    invoke-static {p1, v0, p0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 351
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v0

    const-string v1, "market_open_success"

    invoke-virtual {v0, v1, p1, p2}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 352
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->c()Lcom/ss/android/a/a/a/c;

    move-result-object v2

    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p2, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    iget-object v5, p2, Lcom/ss/android/downloadlib/addownload/b/e;->d:Lcom/ss/android/a/a/b/a;

    iget-object v6, p2, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    iget-object p1, p2, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    .line 353
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->v()Ljava/lang/String;

    move-result-object v7

    move-object v8, p0

    .line 352
    invoke-interface/range {v2 .. v8}, Lcom/ss/android/a/a/a/c;->a(Landroid/content/Context;Lcom/ss/android/a/a/b/c;Lcom/ss/android/a/a/b/a;Lcom/ss/android/a/a/b/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    new-instance p0, Lcom/ss/android/b/a/b/b;

    iget-object p1, p2, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    iget-object v0, p2, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    iget-object p2, p2, Lcom/ss/android/downloadlib/addownload/b/e;->d:Lcom/ss/android/a/a/b/a;

    invoke-direct {p0, p1, v0, p2}, Lcom/ss/android/b/a/b/b;-><init>(Lcom/ss/android/a/a/b/c;Lcom/ss/android/a/a/b/b;Lcom/ss/android/a/a/b/a;)V

    const/4 p1, 0x2

    .line 356
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/b/b;->e(I)V

    .line 357
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/ss/android/b/a/b/b;->f(J)V

    const/4 p2, 0x4

    .line 358
    invoke-virtual {p0, p2}, Lcom/ss/android/b/a/b/b;->h(I)V

    .line 359
    invoke-virtual {p0, p1}, Lcom/ss/android/b/a/b/b;->i(I)V

    .line 360
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/ss/android/downloadlib/addownload/b/f;->a(Lcom/ss/android/b/a/b/b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 362
    invoke-static {p0}, Lcom/ss/android/downloadlib/h/k;->a(Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method public static a(J)Z
    .locals 1

    .line 394
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/ss/android/downloadlib/addownload/b/f;->d(J)Lcom/ss/android/b/a/b/b;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static a(Lcom/ss/android/downloadlib/addownload/b/e;)Z
    .locals 8
    .param p0    # Lcom/ss/android/downloadlib/addownload/b/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 42
    iget-object v0, p0, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v0}, Lcom/ss/android/a/a/b/c;->x()Lcom/ss/android/a/a/c/b;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {v0}, Lcom/ss/android/a/a/c/b;->b()Ljava/lang/String;

    move-result-object v0

    .line 44
    :goto_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    invoke-static {v1, p0}, Lcom/ss/android/downloadlib/h/f;->a(Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "applink_source"

    const-string v3, "click_by_sdk"

    .line 47
    invoke-static {v1, v2, v3}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 50
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v2

    const-string v3, "applink_click"

    invoke-virtual {v2, v3, v1, p0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 52
    invoke-static {v0, p0}, Lcom/ss/android/downloadlib/h/h;->b(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)Lcom/ss/android/downloadlib/addownload/b/g;

    move-result-object v2

    .line 53
    invoke-virtual {v2}, Lcom/ss/android/downloadlib/addownload/b/g;->a()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 54
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "by_url"

    .line 55
    invoke-static {v0, v2, v1, p0}, Lcom/ss/android/downloadlib/b/a;->b(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 57
    :cond_1
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v2}, Lcom/ss/android/a/a/b/c;->v()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p0}, Lcom/ss/android/downloadlib/h/h;->a(Landroid/content/Context;Ljava/lang/String;Lcom/ss/android/b/a/b/a;)Lcom/ss/android/downloadlib/addownload/b/g;

    move-result-object v2

    .line 61
    :cond_2
    iget-wide v5, p0, Lcom/ss/android/downloadlib/addownload/b/e;->a:J

    invoke-static {v5, v6}, Lcom/ss/android/downloadlib/b/a;->a(J)Z

    move-result v0

    const/4 v3, 0x0

    const/4 v5, 0x1

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v6, "link_ad_click_event"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v5, :cond_4

    .line 62
    iget-object v0, p0, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    instance-of v0, v0, Lcom/ss/android/b/a/a/c;

    if-eqz v0, :cond_3

    .line 63
    iget-object v0, p0, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    check-cast v0, Lcom/ss/android/b/a/a/c;

    const/4 v6, 0x4

    .line 64
    invoke-virtual {v0, v6}, Lcom/ss/android/b/a/a/c;->a(I)Lcom/ss/android/b/a/a/c;

    .line 66
    :cond_3
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v0

    iget-wide v6, p0, Lcom/ss/android/downloadlib/addownload/b/e;->a:J

    invoke-virtual {v0, v6, v7, v3}, Lcom/ss/android/downloadlib/e/a;->a(JI)V

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    .line 71
    :goto_1
    invoke-virtual {v2}, Lcom/ss/android/downloadlib/addownload/b/g;->a()I

    move-result v6

    if-eq v6, v5, :cond_5

    packed-switch v6, :pswitch_data_0

    .line 85
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    goto :goto_3

    :pswitch_0
    const-string v5, "by_package"

    .line 81
    invoke-static {v5, v2, v1, p0}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    goto :goto_3

    :pswitch_1
    const-string v2, "by_package"

    .line 73
    invoke-static {v2, v1, p0}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    goto :goto_2

    :cond_5
    const-string v2, "by_url"

    .line 77
    invoke-static {v2, v1, p0}, Lcom/ss/android/downloadlib/b/a;->b(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    :goto_2
    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_8

    if-nez v0, :cond_8

    .line 92
    invoke-static {}, Lcom/ss/android/downloadlib/e/c;->a()Lcom/ss/android/downloadlib/e/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/downloadlib/e/c;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 93
    invoke-static {}, Lcom/ss/android/downloadlib/e/c;->a()Lcom/ss/android/downloadlib/e/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/ss/android/downloadlib/addownload/b/e;->a:J

    iget-object v5, p0, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v5}, Lcom/ss/android/a/a/b/c;->u()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v2, v5}, Lcom/ss/android/downloadlib/e/c;->b(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 94
    :cond_6
    invoke-static {}, Lcom/ss/android/downloadlib/e/c;->a()Lcom/ss/android/downloadlib/e/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/downloadlib/e/c;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 95
    :cond_7
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/ss/android/downloadlib/addownload/b/e;->a:J

    invoke-virtual {v0, v1, v2, v4}, Lcom/ss/android/downloadlib/e/a;->a(JI)V

    :cond_8
    return v3

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/ss/android/downloadlib/addownload/b/e;I)Z
    .locals 6
    .param p0    # Lcom/ss/android/downloadlib/addownload/b/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 321
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 323
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v1

    const-string v2, "market_click_open"

    invoke-virtual {v1, v2, v0, p0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 324
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v2}, Lcom/ss/android/a/a/b/c;->v()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p0, v2}, Lcom/ss/android/downloadlib/h/h;->a(Landroid/content/Context;Lcom/ss/android/downloadlib/addownload/b/e;Ljava/lang/String;)Lcom/ss/android/downloadlib/addownload/b/g;

    move-result-object v1

    const/4 v2, 0x2

    .line 326
    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1}, Lcom/ss/android/downloadlib/addownload/b/g;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "open_market"

    const/4 v5, 0x1

    aput-object v3, v2, v5

    invoke-static {v2}, Lcom/ss/android/downloadlib/h/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 327
    invoke-virtual {v1}, Lcom/ss/android/downloadlib/addownload/b/g;->a()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    return v4

    :pswitch_0
    const-string p1, "error_code"

    .line 334
    invoke-virtual {v1}, Lcom/ss/android/downloadlib/addownload/b/g;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 335
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object p1

    const-string v1, "market_open_failed"

    invoke-virtual {p1, v1, v0, p0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return v4

    .line 329
    :pswitch_1
    invoke-static {v2, v0, p0}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/downloadlib/addownload/b/e;)V

    .line 331
    :pswitch_2
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v0

    iget-wide v1, p0, Lcom/ss/android/downloadlib/addownload/b/e;->a:J

    invoke-virtual {v0, v1, v2, p1}, Lcom/ss/android/downloadlib/e/a;->a(JI)V

    return v5

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Lcom/ss/android/b/a/b/b;)Z
    .locals 4
    .param p1    # Lcom/ss/android/b/a/b/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 196
    invoke-virtual {p1}, Lcom/ss/android/b/a/b/b;->N()I

    move-result v0

    invoke-static {v0}, Lcom/ss/android/downloadlib/addownload/i;->b(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 199
    :cond_0
    invoke-virtual {p1}, Lcom/ss/android/b/a/b/b;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    return v1

    .line 203
    :cond_1
    invoke-static {}, Lcom/ss/android/socialbase/downloader/notification/b;->a()Lcom/ss/android/socialbase/downloader/notification/b;

    move-result-object p0

    invoke-virtual {p1}, Lcom/ss/android/b/a/b/b;->s()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ss/android/socialbase/downloader/notification/b;->f(I)V

    .line 205
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 206
    invoke-static {p0, p1}, Lcom/ss/android/downloadlib/h/f;->a(Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)Lorg/json/JSONObject;

    const-string v0, "applink_source"

    const-string v2, "auto_click"

    .line 208
    invoke-static {p0, v0, v2}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 210
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v0

    const-string v2, "applink_click"

    invoke-virtual {v0, v2, p1}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)V

    .line 211
    invoke-virtual {p1}, Lcom/ss/android/b/a/b/b;->f()Ljava/lang/String;

    move-result-object v0

    .line 212
    invoke-virtual {p1}, Lcom/ss/android/b/a/b/b;->e()Ljava/lang/String;

    move-result-object v2

    .line 211
    invoke-static {p1, v0, v2}, Lcom/ss/android/downloadlib/h/h;->a(Lcom/ss/android/b/a/b/b;Ljava/lang/String;Ljava/lang/String;)Lcom/ss/android/downloadlib/addownload/b/g;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Lcom/ss/android/downloadlib/addownload/b/g;->a()I

    move-result v2

    const/4 v3, 0x1

    packed-switch v2, :pswitch_data_0

    return v1

    :pswitch_0
    const-string v2, "auto_by_package"

    .line 222
    invoke-static {v2, v0, p0, p1}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return v1

    :pswitch_1
    const-string v0, "auto_by_package"

    .line 216
    invoke-static {v0, p0, p1}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return v3

    :pswitch_2
    const-string v2, "auto_by_url"

    .line 225
    invoke-static {v2, v0, p0, p1}, Lcom/ss/android/downloadlib/b/a;->b(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return v1

    :pswitch_3
    const-string v0, "auto_by_url"

    .line 219
    invoke-static {v0, p0, p1}, Lcom/ss/android/downloadlib/b/a;->b(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return v3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Lcom/ss/android/b/a/b/b;)V
    .locals 6

    if-nez p0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 149
    invoke-static {}, Lcom/ss/android/socialbase/downloader/g/a;->c()Lcom/ss/android/socialbase/downloader/g/a;

    move-result-object v1

    const-string v2, "app_link_opt"

    invoke-virtual {v1, v2}, Lcom/ss/android/socialbase/downloader/g/a;->b(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 150
    invoke-virtual {p0}, Lcom/ss/android/b/a/b/b;->f()Ljava/lang/String;

    move-result-object v0

    .line 152
    :cond_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    invoke-static {v1, p0}, Lcom/ss/android/downloadlib/h/f;->a(Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v3, "applink_source"

    const-string v4, "dialog_click_by_sdk"

    .line 155
    invoke-static {v1, v3, v4}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v3

    const-string v4, "applink_click"

    invoke-virtual {v3, v4, v1, p0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 160
    invoke-static {v0, p0}, Lcom/ss/android/downloadlib/h/h;->b(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)Lcom/ss/android/downloadlib/addownload/b/g;

    move-result-object v3

    .line 161
    invoke-virtual {v3}, Lcom/ss/android/downloadlib/addownload/b/g;->a()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 162
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "dialog_by_url"

    .line 163
    invoke-static {v0, v3, v1, p0}, Lcom/ss/android/downloadlib/b/a;->b(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 165
    :cond_2
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/ss/android/b/a/b/b;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, p0}, Lcom/ss/android/downloadlib/h/h;->a(Landroid/content/Context;Ljava/lang/String;Lcom/ss/android/b/a/b/a;)Lcom/ss/android/downloadlib/addownload/b/g;

    move-result-object v3

    .line 168
    :cond_3
    invoke-virtual {v3}, Lcom/ss/android/downloadlib/addownload/b/g;->a()I

    move-result v0

    if-eq v0, v2, :cond_4

    packed-switch v0, :pswitch_data_0

    .line 179
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    goto :goto_0

    :pswitch_0
    const-string v0, "dialog_by_package"

    .line 176
    invoke-static {v0, v3, v1, p0}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "dialog_by_package"

    .line 170
    invoke-static {v0, v1, p0}, Lcom/ss/android/downloadlib/b/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    goto :goto_0

    :cond_4
    const-string v0, "dialog_by_url"

    .line 173
    invoke-static {v0, v1, p0}, Lcom/ss/android/downloadlib/b/a;->b(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;Lcom/ss/android/downloadlib/addownload/b/g;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V
    .locals 1
    .param p1    # Lcom/ss/android/downloadlib/addownload/b/g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/ss/android/b/a/b/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "applink_source"

    .line 312
    invoke-static {p2, v0, p0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string p0, "error_code"

    .line 313
    invoke-virtual {p1}, Lcom/ss/android/downloadlib/addownload/b/g;->b()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2, p0, p1}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 314
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object p0

    const-string p1, "deeplink_url_open_fail"

    invoke-virtual {p0, p1, p2, p3}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public static b(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V
    .locals 9
    .param p1    # Lorg/json/JSONObject;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/ss/android/b/a/b/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "applink_source"

    .line 271
    invoke-static {p1, v0, p0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 272
    invoke-static {}, Lcom/ss/android/downloadlib/e/a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v0

    const-string v1, "deeplink_url_open"

    invoke-virtual {v0, v1, p1, p2}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 274
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x66a1d5e9

    const/4 v2, 0x1

    if-eq v0, v1, :cond_3

    const v1, -0x51ef0279

    if-eq v0, v1, :cond_2

    const v1, -0x7b8b0c3

    if-eq v0, v1, :cond_1

    const v1, 0x3174fc5e

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "dialog_by_url"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_1
    const-string v0, "notify_by_url"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const-string v0, "by_url"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    goto :goto_1

    :cond_3
    const-string v0, "auto_by_url"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 277
    :pswitch_0
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->i()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "download_sdk_optimize_mode"

    .line 278
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v2, :cond_5

    const-string p0, "check_applink_result_opt"

    .line 279
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, p0, v0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    .line 280
    invoke-static {}, Lcom/ss/android/downloadlib/b/e;->a()Lcom/ss/android/downloadlib/b/e;

    move-result-object p0

    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/ss/android/downloadlib/b/a$2;

    invoke-direct {v1, p1, p2}, Lcom/ss/android/downloadlib/b/a$2;-><init>(Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    invoke-virtual {p0, v0, v1}, Lcom/ss/android/downloadlib/b/e;->a(Ljava/lang/String;Lcom/ss/android/downloadlib/b/d;)V

    goto :goto_2

    .line 296
    :cond_5
    :pswitch_1
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->c()Lcom/ss/android/a/a/a/c;

    move-result-object v2

    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v3

    .line 297
    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->t()Lcom/ss/android/a/a/b/c;

    move-result-object v4

    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->v()Lcom/ss/android/a/a/b/a;

    move-result-object v5

    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->u()Lcom/ss/android/a/a/b/b;

    move-result-object v6

    .line 298
    invoke-interface {p2}, Lcom/ss/android/b/a/b/a;->e()Ljava/lang/String;

    move-result-object v7

    move-object v8, p0

    .line 296
    invoke-interface/range {v2 .. v8}, Lcom/ss/android/a/a/a/c;->a(Landroid/content/Context;Lcom/ss/android/a/a/b/c;Lcom/ss/android/a/a/b/a;Lcom/ss/android/a/a/b/b;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
