.class public Lcom/ss/android/downloadlib/c/b;
.super Ljava/lang/Object;
.source "DefaultPermissionChecker.java"

# interfaces
.implements Lcom/ss/android/a/a/a/h;


# instance fields
.field private a:Lcom/ss/android/a/a/a/q;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;I[Ljava/lang/String;[I)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # [I
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 44
    array-length p1, p4

    if-lez p1, :cond_1

    iget-object p1, p0, Lcom/ss/android/downloadlib/c/b;->a:Lcom/ss/android/a/a/a/q;

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 45
    aget p2, p4, p1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 46
    iget-object p2, p0, Lcom/ss/android/downloadlib/c/b;->a:Lcom/ss/android/a/a/a/q;

    aget-object p1, p3, p1

    invoke-interface {p2, p1}, Lcom/ss/android/a/a/a/q;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_0
    aget p1, p4, p1

    if-nez p1, :cond_1

    .line 48
    iget-object p1, p0, Lcom/ss/android/downloadlib/c/b;->a:Lcom/ss/android/a/a/a/q;

    invoke-interface {p1}, Lcom/ss/android/a/a/a/q;->a()V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Landroid/app/Activity;[Ljava/lang/String;Lcom/ss/android/a/a/a/q;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # [Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 24
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 25
    iput-object p3, p0, Lcom/ss/android/downloadlib/c/b;->a:Lcom/ss/android/a/a/a/q;

    const/4 p3, 0x1

    .line 26
    invoke-virtual {p1, p2, p3}, Landroid/app/Activity;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    .line 29
    invoke-interface {p3}, Lcom/ss/android/a/a/a/q;->a()V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 37
    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    return v0
.end method
