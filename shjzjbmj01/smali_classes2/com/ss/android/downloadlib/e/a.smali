.class public Lcom/ss/android/downloadlib/e/a;
.super Ljava/lang/Object;
.source "AdEventHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ss/android/downloadlib/e/a$a;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/ss/android/downloadlib/e/a$1;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/ss/android/downloadlib/e/a;-><init>()V

    return-void
.end method

.method public static a()Lcom/ss/android/downloadlib/e/a;
    .locals 1

    .line 56
    invoke-static {}, Lcom/ss/android/downloadlib/e/a$a;->a()Lcom/ss/android/downloadlib/e/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/ss/android/b/a/b/a;)Lorg/json/JSONObject;
    .locals 3

    .line 439
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 441
    :try_start_0
    invoke-interface {p1}, Lcom/ss/android/b/a/b/a;->g()Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 442
    invoke-interface {p1}, Lcom/ss/android/b/a/b/a;->p()Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    const-string v1, "download_url"

    .line 443
    invoke-interface {p1}, Lcom/ss/android/b/a/b/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "package_name"

    .line 444
    invoke-interface {p1}, Lcom/ss/android/b/a/b/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "android_int"

    .line 445
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "rom_name"

    .line 446
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "rom_version"

    .line 447
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/f/c;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "ttdownloader"

    const/4 v2, 0x1

    .line 448
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "funnel_type"

    .line 449
    invoke-interface {p1}, Lcom/ss/android/b/a/b/a;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 450
    invoke-interface {p1}, Lcom/ss/android/b/a/b/a;->h()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 451
    invoke-static {v0, p1}, Lcom/ss/android/downloadlib/h/f;->b(Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-object v0
.end method

.method private a(Lcom/ss/android/a/a/c/d;)V
    .locals 1

    .line 459
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->b()Lcom/ss/android/a/a/a/f;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 462
    :cond_0
    invoke-virtual {p1}, Lcom/ss/android/a/a/c/d;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->b()Lcom/ss/android/a/a/a/f;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/ss/android/a/a/a/f;->a(Lcom/ss/android/a/a/c/d;)V

    goto :goto_0

    .line 465
    :cond_1
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->b()Lcom/ss/android/a/a/a/f;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/ss/android/a/a/a/f;->b(Lcom/ss/android/a/a/c/d;)V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;JILcom/ss/android/b/a/b/a;)V
    .locals 5

    if-eqz p7, :cond_3

    .line 413
    instance-of v0, p7, Lcom/ss/android/downloadlib/addownload/b/e;

    if-eqz v0, :cond_0

    move-object v0, p7

    check-cast v0, Lcom/ss/android/downloadlib/addownload/b/e;

    invoke-virtual {v0}, Lcom/ss/android/downloadlib/addownload/b/e;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_3

    .line 418
    :cond_0
    :try_start_0
    new-instance v0, Lcom/ss/android/a/a/c/d$a;

    invoke-direct {v0}, Lcom/ss/android/a/a/c/d$a;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 419
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->j()Ljava/lang/String;

    move-result-object p1

    const/4 v3, 0x1

    aput-object p1, v1, v3

    const-string p1, "embeded_ad"

    const/4 v4, 0x2

    aput-object p1, v1, v4

    invoke-static {v1}, Lcom/ss/android/downloadlib/h/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/ss/android/a/a/c/d$a;->a(Ljava/lang/String;)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 420
    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/c/d$a;->b(Ljava/lang/String;)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 421
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->c()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/c/d$a;->b(Z)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 422
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->b()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/ss/android/a/a/c/d$a;->a(J)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 423
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->d()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/c/d$a;->c(Ljava/lang/String;)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    const-wide/16 v0, 0x0

    cmp-long p2, p4, v0

    if-lez p2, :cond_1

    goto :goto_0

    .line 424
    :cond_1
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->l()J

    move-result-wide p4

    :goto_0
    invoke-virtual {p1, p4, p5}, Lcom/ss/android/a/a/c/d$a;->b(J)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 425
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->i()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/c/d$a;->d(Ljava/lang/String;)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 426
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->n()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/c/d$a;->a(Ljava/util/List;)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    new-array p2, v4, [Lorg/json/JSONObject;

    .line 427
    invoke-direct {p0, p7}, Lcom/ss/android/downloadlib/e/a;->a(Lcom/ss/android/b/a/b/a;)Lorg/json/JSONObject;

    move-result-object p4

    aput-object p4, p2, v2

    aput-object p3, p2, v3

    invoke-static {p2}, Lcom/ss/android/downloadlib/h/k;->a([Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/c/d$a;->a(Lorg/json/JSONObject;)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 428
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->k()Lorg/json/JSONObject;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/c/d$a;->b(Lorg/json/JSONObject;)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 429
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->o()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/c/d$a;->a(Ljava/lang/Object;)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    if-lez p6, :cond_2

    goto :goto_1

    :cond_2
    const/4 p6, 0x2

    .line 430
    :goto_1
    invoke-virtual {p1, p6}, Lcom/ss/android/a/a/c/d$a;->a(I)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 431
    invoke-interface {p7}, Lcom/ss/android/b/a/b/a;->m()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/ss/android/a/a/c/d$a;->a(Z)Lcom/ss/android/a/a/c/d$a;

    move-result-object p1

    .line 432
    invoke-virtual {p1}, Lcom/ss/android/a/a/c/d$a;->a()Lcom/ss/android/a/a/c/d;

    move-result-object p1

    .line 418
    invoke-direct {p0, p1}, Lcom/ss/android/downloadlib/e/a;->a(Lcom/ss/android/a/a/c/d;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 434
    invoke-static {p1}, Lcom/ss/android/downloadlib/h/k;->a(Ljava/lang/Throwable;)V

    :goto_2
    return-void

    .line 414
    :cond_3
    :goto_3
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    return-void
.end method


# virtual methods
.method public a(JI)V
    .locals 7

    .line 67
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/ss/android/downloadlib/addownload/b/f;->e(J)Lcom/ss/android/downloadlib/addownload/b/e;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/ss/android/downloadlib/addownload/b/e;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    return-void

    .line 72
    :cond_0
    iget-object v1, v0, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    invoke-interface {v1}, Lcom/ss/android/a/a/b/b;->l()Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne p3, v1, :cond_2

    .line 75
    iget-object v2, v0, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    invoke-interface {v2}, Lcom/ss/android/a/a/b/b;->c()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    iget-object v2, v0, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    invoke-interface {v2}, Lcom/ss/android/a/a/b/b;->b()Ljava/lang/String;

    move-result-object v2

    :goto_0
    const/4 v3, 0x2

    .line 76
    new-array v4, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    invoke-interface {v6}, Lcom/ss/android/a/a/b/b;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v5, "click"

    aput-object v5, v4, v1

    invoke-static {v4}, Lcom/ss/android/downloadlib/h/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 77
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v6, "download_click_type"

    .line 79
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {v5, v6, p3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p3, "permission_notification"

    .line 80
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/e/d;->a()Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    goto :goto_1

    :cond_3
    const/4 v6, 0x2

    :goto_1
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, p3, v6}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p3, "network_available"

    .line 81
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/k;->a()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/ss/android/socialbase/downloader/i/e;->c(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    goto :goto_2

    :cond_4
    const/4 v1, 0x2

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, p3, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p3

    .line 83
    invoke-virtual {p3}, Lorg/json/JSONException;->printStackTrace()V

    .line 85
    :goto_3
    invoke-virtual {p0, v2, v4, v5, v0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    const-string p3, "click"

    .line 87
    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_5

    .line 89
    iget-object p3, v0, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    if-eqz p3, :cond_5

    .line 90
    invoke-static {}, Lcom/ss/android/downloadlib/e/c;->a()Lcom/ss/android/downloadlib/e/c;

    move-result-object p3

    iget-object v0, v0, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {v0}, Lcom/ss/android/a/a/b/c;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, p1, p2, v0}, Lcom/ss/android/downloadlib/e/c;->a(JLjava/lang/String;)V

    :cond_5
    return-void
.end method

.method public a(JILcom/ss/android/socialbase/downloader/model/DownloadInfo;)V
    .locals 9

    .line 99
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/ss/android/downloadlib/addownload/b/f;->e(J)Lcom/ss/android/downloadlib/addownload/b/e;

    move-result-object v8

    .line 100
    invoke-virtual {v8}, Lcom/ss/android/downloadlib/addownload/b/e;->w()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 101
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    return-void

    :cond_0
    const/4 p1, 0x0

    .line 105
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const/4 v7, 0x1

    const/4 p2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x2

    packed-switch p3, :pswitch_data_0

    :goto_0
    move-object v3, p1

    goto/16 :goto_1

    :pswitch_0
    if-eqz p4, :cond_1

    .line 124
    :try_start_0
    invoke-virtual {p4}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getId()I

    move-result p1

    invoke-static {v4, p1}, Lcom/ss/android/downloadlib/h/f;->a(Lorg/json/JSONObject;I)V

    .line 125
    invoke-static {v4, p4}, Lcom/ss/android/downloadlib/a;->b(Lorg/json/JSONObject;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :catch_0
    :cond_1
    new-array p1, v1, [Ljava/lang/String;

    iget-object p3, v8, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    invoke-interface {p3}, Lcom/ss/android/a/a/b/b;->h()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, v0

    const-string p3, "click_install"

    aput-object p3, p1, p2

    invoke-static {p1}, Lcom/ss/android/downloadlib/h/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 118
    :pswitch_1
    new-array p1, v1, [Ljava/lang/String;

    iget-object p3, v8, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    invoke-interface {p3}, Lcom/ss/android/a/a/b/b;->g()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, v0

    const-string p3, "click_continue"

    aput-object p3, p1, p2

    invoke-static {p1}, Lcom/ss/android/downloadlib/h/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 119
    invoke-static {p4, v4}, Lcom/ss/android/downloadlib/h/f;->c(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lorg/json/JSONObject;)V

    goto :goto_0

    .line 114
    :pswitch_2
    new-array p1, v1, [Ljava/lang/String;

    iget-object p3, v8, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    invoke-interface {p3}, Lcom/ss/android/a/a/b/b;->f()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, v0

    const-string p3, "click_pause"

    aput-object p3, p1, p2

    invoke-static {p1}, Lcom/ss/android/downloadlib/h/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 115
    invoke-static {p4, v4}, Lcom/ss/android/downloadlib/h/f;->b(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lorg/json/JSONObject;)V

    goto :goto_0

    .line 110
    :pswitch_3
    new-array p1, v1, [Ljava/lang/String;

    iget-object p3, v8, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    invoke-interface {p3}, Lcom/ss/android/a/a/b/b;->e()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, v0

    const-string p3, "click_start"

    aput-object p3, p1, p2

    invoke-static {p1}, Lcom/ss/android/downloadlib/h/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 111
    invoke-static {p4, v4}, Lcom/ss/android/downloadlib/h/f;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lorg/json/JSONObject;)V

    goto :goto_0

    .line 133
    :pswitch_4
    new-array p1, v1, [Ljava/lang/String;

    iget-object p3, v8, Lcom/ss/android/downloadlib/addownload/b/e;->c:Lcom/ss/android/a/a/b/b;

    invoke-interface {p3}, Lcom/ss/android/a/a/b/b;->i()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, v0

    const-string p3, "storage_deny"

    aput-object p3, p1, p2

    invoke-static {p1}, Lcom/ss/android/downloadlib/h/k;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :goto_1
    const/4 v2, 0x0

    const-wide/16 v5, 0x0

    move-object v1, p0

    .line 136
    invoke-direct/range {v1 .. v8}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;JILcom/ss/android/b/a/b/a;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(JLcom/ss/android/socialbase/downloader/exception/BaseException;)V
    .locals 2

    .line 177
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/ss/android/downloadlib/addownload/b/f;->e(J)Lcom/ss/android/downloadlib/addownload/b/e;

    move-result-object p1

    .line 178
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "download_time"

    const/4 v1, 0x0

    .line 180
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz p3, :cond_0

    const-string v0, "fail_status"

    .line 182
    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "fail_msg"

    .line 183
    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorMessage()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, v0, p3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p3

    .line 186
    invoke-virtual {p3}, Lorg/json/JSONException;->printStackTrace()V

    :cond_0
    :goto_0
    const-string p3, "download_failed"

    .line 188
    invoke-virtual {p0, p3, p2, p1}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(JLcom/ss/android/socialbase/downloader/exception/BaseException;Lorg/json/JSONObject;)V
    .locals 2

    if-nez p4, :cond_0

    .line 203
    new-instance p4, Lorg/json/JSONObject;

    invoke-direct {p4}, Lorg/json/JSONObject;-><init>()V

    :cond_0
    if-eqz p3, :cond_1

    :try_start_0
    const-string v0, "error_code"

    .line 207
    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "ttdownloader_message"

    .line 208
    invoke-virtual {p3}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorMessage()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p4, v0, p3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p3

    .line 211
    invoke-virtual {p3}, Lorg/json/JSONException;->printStackTrace()V

    :cond_1
    :goto_0
    const-string p3, "download_sdk_error"

    .line 213
    invoke-virtual {p0, p3, p4, p1, p2}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;J)V

    return-void
.end method

.method public a(JZI)V
    .locals 1

    .line 151
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/ss/android/downloadlib/addownload/b/f;->e(J)Lcom/ss/android/downloadlib/addownload/b/e;

    move-result-object p1

    .line 152
    invoke-virtual {p1}, Lcom/ss/android/downloadlib/addownload/b/e;->w()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 153
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    return-void

    .line 156
    :cond_0
    iget-object p2, p1, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    invoke-interface {p2}, Lcom/ss/android/a/a/b/c;->B()Lcom/ss/android/a/a/c/f;

    move-result-object p2

    if-nez p2, :cond_1

    return-void

    .line 159
    :cond_1
    iget-object p2, p1, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    instance-of p2, p2, Lcom/ss/android/b/a/a/c;

    if-eqz p2, :cond_2

    .line 160
    iget-object p2, p1, Lcom/ss/android/downloadlib/addownload/b/e;->b:Lcom/ss/android/a/a/b/c;

    check-cast p2, Lcom/ss/android/b/a/a/c;

    const/4 v0, 0x3

    .line 161
    invoke-virtual {p2, v0}, Lcom/ss/android/b/a/a/c;->a(I)Lcom/ss/android/b/a/a/c;

    .line 163
    :cond_2
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v0, "download_click_type"

    .line 165
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {p2, v0, p4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p4

    .line 167
    invoke-virtual {p4}, Lorg/json/JSONException;->printStackTrace()V

    :goto_0
    if-eqz p3, :cond_3

    const-string p3, "deeplink_quickapp_success"

    goto :goto_1

    :cond_3
    const-string p3, "deeplink_quickapp_failed"

    .line 170
    :goto_1
    invoke-virtual {p0, p3, p2, p1}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)V
    .locals 4

    .line 217
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ss/android/downloadlib/addownload/b/f;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/b/a/b/b;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 222
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 223
    invoke-static {p1, v1}, Lcom/ss/android/downloadlib/h/f;->c(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lorg/json/JSONObject;)V

    .line 224
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/ss/android/b/a/b/b;->a(J)V

    .line 225
    invoke-virtual {v0}, Lcom/ss/android/b/a/b/b;->j()Ljava/lang/String;

    move-result-object p1

    const-string v2, "download_resume"

    invoke-virtual {p0, p1, v2, v1, v0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 226
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/i;->a()Lcom/ss/android/downloadlib/addownload/b/i;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/ss/android/downloadlib/addownload/b/i;->a(Lcom/ss/android/b/a/b/b;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 228
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/exception/BaseException;)V
    .locals 9

    if-nez p1, :cond_0

    return-void

    .line 242
    :cond_0
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/ss/android/downloadlib/addownload/b/f;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/b/a/b/b;

    move-result-object p2

    if-nez p2, :cond_1

    .line 244
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    return-void

    .line 247
    :cond_1
    iget-object v0, p2, Lcom/ss/android/b/a/b/b;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 250
    :cond_2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 252
    :try_start_0
    invoke-static {v0, p1}, Lcom/ss/android/downloadlib/a;->a(Lorg/json/JSONObject;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lorg/json/JSONObject;

    const-string v1, "fail_status"

    .line 253
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->D()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "fail_msg"

    .line 254
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->E()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "download_failed_times"

    .line 255
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->w()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 256
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_3

    const-string v1, "download_percent"

    .line 257
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v5

    long-to-double v5, v5

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v7
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    long-to-double v7, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    invoke-static {v7, v8}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v5, v7

    :try_start_1
    invoke-virtual {v0, v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    :cond_3
    const-string v1, "download_status"

    .line 259
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getRealStatus()I

    move-result p1

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 260
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 261
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->G()J

    move-result-wide v5

    cmp-long p1, v5, v3

    if-lez p1, :cond_4

    const-string p1, "time_from_start_download"

    .line 262
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->G()J

    move-result-wide v5

    const/4 v7, 0x0

    sub-long v5, v1, v5

    invoke-virtual {v0, p1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 264
    :cond_4
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->A()J

    move-result-wide v5

    cmp-long p1, v5, v3

    if-lez p1, :cond_5

    const-string p1, "time_from_download_resume"

    .line 265
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->A()J

    move-result-wide v3

    const/4 v5, 0x0

    sub-long/2addr v1, v3

    invoke-virtual {v0, p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    :cond_5
    const-string p1, "is_update_download"

    .line 267
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->U()Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    goto :goto_0

    :cond_6
    const/4 v1, 0x2

    :goto_0
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "can_show_notification"

    .line 269
    invoke-static {}, Lcom/ss/android/socialbase/appdownloader/e/d;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    goto :goto_1

    :cond_7
    const/4 v1, 0x2

    :goto_1
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string p1, "has_send_download_failed_finally"

    .line 270
    iget-object v1, p2, Lcom/ss/android/b/a/b/b;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v2, 0x1

    :cond_8
    invoke-virtual {v0, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    .line 272
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 274
    :goto_2
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->j()Ljava/lang/String;

    move-result-object p1

    const-string v1, "download_cancel"

    invoke-virtual {p0, p1, v1, v0, p2}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;ILcom/ss/android/downloadlib/addownload/b/e;)V
    .locals 8

    int-to-long v4, p2

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v7, p3

    .line 144
    invoke-direct/range {v0 .. v7}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;JILcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;J)V
    .locals 1

    .line 386
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/ss/android/downloadlib/addownload/b/f;->d(J)Lcom/ss/android/b/a/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 388
    invoke-virtual {p0, p1, v0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)V

    goto :goto_0

    .line 390
    :cond_0
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/ss/android/downloadlib/addownload/b/f;->e(J)Lcom/ss/android/downloadlib/addownload/b/e;

    move-result-object p2

    .line 391
    invoke-virtual {p0, p1, p2}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)V

    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/ss/android/a/a/b/c;Lcom/ss/android/a/a/b/b;Lcom/ss/android/a/a/b/a;)V
    .locals 7
    .param p2    # Lcom/ss/android/a/a/b/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/ss/android/a/a/b/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/ss/android/a/a/b/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 378
    new-instance v6, Lcom/ss/android/downloadlib/addownload/b/e;

    invoke-interface {p2}, Lcom/ss/android/a/a/b/c;->d()J

    move-result-wide v1

    move-object v0, v6

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/ss/android/downloadlib/addownload/b/e;-><init>(JLcom/ss/android/a/a/b/c;Lcom/ss/android/a/a/b/b;Lcom/ss/android/a/a/b/a;)V

    invoke-virtual {p0, p1, v6}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/ss/android/b/a/b/a;)V
    .locals 1

    const/4 v0, 0x0

    .line 396
    invoke-virtual {p0, v0, p1, p2}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/ss/android/b/a/b/b;)V
    .locals 1

    const/4 v0, 0x0

    .line 348
    invoke-virtual {p0, p1, v0, p2}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/ss/android/b/a/b/a;)V
    .locals 1

    const/4 v0, 0x0

    .line 404
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V
    .locals 8

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v7, p4

    .line 408
    invoke-direct/range {v0 .. v7}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;JILcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;J)V
    .locals 1

    .line 352
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/ss/android/downloadlib/addownload/b/f;->d(J)Lcom/ss/android/b/a/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {p0, p1, p2, v0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/b;)V

    goto :goto_0

    .line 356
    :cond_0
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/ss/android/downloadlib/addownload/b/f;->e(J)Lcom/ss/android/downloadlib/addownload/b/e;

    move-result-object p3

    .line 357
    invoke-virtual {p3}, Lcom/ss/android/downloadlib/addownload/b/e;->w()Z

    move-result p4

    if-eqz p4, :cond_1

    .line 358
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    goto :goto_0

    .line 360
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/downloadlib/addownload/b/e;)V

    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V
    .locals 1

    const/4 v0, 0x0

    .line 400
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/b;)V
    .locals 2

    .line 372
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "unity_label"

    .line 373
    invoke-static {v0, v1, p1}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string p1, "embeded_ad"

    const-string v1, "ttdownloader_unity"

    .line 374
    invoke-static {v0, p2}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p2

    invoke-virtual {p0, p1, v1, p2, p3}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/downloadlib/addownload/b/e;)V
    .locals 2

    .line 366
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "unity_label"

    .line 367
    invoke-static {v0, v1, p1}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string p1, "embeded_ad"

    const-string v1, "ttdownloader_unity"

    .line 368
    invoke-static {v0, p2}, Lcom/ss/android/downloadlib/h/k;->a(Lorg/json/JSONObject;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p2

    invoke-virtual {p0, p1, v1, p2, p3}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public a(Lorg/json/JSONObject;Lcom/ss/android/b/a/b/b;)V
    .locals 2
    .param p2    # Lcom/ss/android/b/a/b/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 235
    invoke-virtual {p2}, Lcom/ss/android/b/a/b/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "install_finish"

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    return-void
.end method

.method public b(JI)V
    .locals 1

    const/4 v0, 0x0

    .line 140
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/ss/android/downloadlib/e/a;->a(JILcom/ss/android/socialbase/downloader/model/DownloadInfo;)V

    return-void
.end method

.method public b(JLcom/ss/android/socialbase/downloader/exception/BaseException;)V
    .locals 1

    const/4 v0, 0x0

    .line 195
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/ss/android/downloadlib/e/a;->a(JLcom/ss/android/socialbase/downloader/exception/BaseException;Lorg/json/JSONObject;)V

    return-void
.end method

.method public b(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lcom/ss/android/socialbase/downloader/exception/BaseException;)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    .line 281
    :cond_0
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/f;->a()Lcom/ss/android/downloadlib/addownload/b/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ss/android/downloadlib/addownload/b/f;->a(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lcom/ss/android/b/a/b/b;

    move-result-object v0

    if-nez v0, :cond_1

    .line 283
    invoke-static {}, Lcom/ss/android/downloadlib/h/k;->b()V

    return-void

    .line 286
    :cond_1
    iget-object v1, v0, Lcom/ss/android/b/a/b/b;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    .line 289
    :cond_2
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 291
    :try_start_0
    invoke-static {p1, v1}, Lcom/ss/android/downloadlib/h/f;->c(Lcom/ss/android/socialbase/downloader/model/DownloadInfo;Lorg/json/JSONObject;)V

    .line 292
    invoke-static {v1, p1}, Lcom/ss/android/downloadlib/a;->a(Lorg/json/JSONObject;Lcom/ss/android/socialbase/downloader/model/DownloadInfo;)Lorg/json/JSONObject;

    if-eqz p2, :cond_3

    const-string v2, "fail_status"

    .line 294
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "fail_msg"

    .line 295
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 296
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorCode()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/ss/android/b/a/b/b;->d(I)V

    .line 297
    invoke-virtual {p2}, Lcom/ss/android/socialbase/downloader/exception/BaseException;->getErrorMessage()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/ss/android/b/a/b/b;->a(Ljava/lang/String;)V

    .line 299
    :cond_3
    invoke-virtual {v0}, Lcom/ss/android/b/a/b/b;->x()V

    const-string p2, "download_failed_times"

    .line 300
    invoke-virtual {v0}, Lcom/ss/android/b/a/b/b;->w()I

    move-result v2

    invoke-virtual {v1, p2, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 301
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long p2, v2, v4

    if-lez p2, :cond_4

    const-string p2, "download_percent"

    .line 302
    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getCurBytes()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-virtual {p1}, Lcom/ss/android/socialbase/downloader/model/DownloadInfo;->getTotalBytes()J

    move-result-wide v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    long-to-double v4, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v2, v4

    :try_start_1
    invoke-virtual {v1, p2, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    :cond_4
    const-string p1, "has_send_download_failed_finally"

    .line 304
    iget-object p2, v0, Lcom/ss/android/b/a/b/b;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p2

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz p2, :cond_5

    const/4 p2, 0x1

    goto :goto_0

    :cond_5
    const/4 p2, 0x2

    :goto_0
    invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 305
    invoke-static {v0, v1}, Lcom/ss/android/downloadlib/h/f;->a(Lcom/ss/android/b/a/b/b;Lorg/json/JSONObject;)V

    const-string p1, "is_update_download"

    .line 306
    invoke-virtual {v0}, Lcom/ss/android/b/a/b/b;->U()Z

    move-result p2

    if-eqz p2, :cond_6

    const/4 v2, 0x1

    :cond_6
    invoke-virtual {v1, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 308
    invoke-virtual {p1}, Lorg/json/JSONException;->printStackTrace()V

    .line 310
    :goto_1
    invoke-virtual {v0}, Lcom/ss/android/b/a/b/b;->j()Ljava/lang/String;

    move-result-object p1

    const-string p2, "download_failed"

    invoke-virtual {p0, p1, p2, v1, v0}, Lcom/ss/android/downloadlib/e/a;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Lcom/ss/android/b/a/b/a;)V

    .line 311
    invoke-static {}, Lcom/ss/android/downloadlib/addownload/b/i;->a()Lcom/ss/android/downloadlib/addownload/b/i;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/ss/android/downloadlib/addownload/b/i;->a(Lcom/ss/android/b/a/b/b;)V

    return-void
.end method
