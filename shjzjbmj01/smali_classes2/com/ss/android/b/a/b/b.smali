.class public Lcom/ss/android/b/a/b/b;
.super Ljava/lang/Object;
.source "NativeDownloadModel.java"

# interfaces
.implements Lcom/ss/android/b/a/b/a;


# instance fields
.field private A:J

.field private B:I

.field private C:I

.field private D:Ljava/lang/String;

.field private E:Z

.field private F:Ljava/lang/String;

.field private G:Z

.field private H:Z

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Z

.field private L:Z

.field private M:I

.field private N:I

.field private O:J

.field private P:J

.field private Q:Z

.field private R:Z

.field private S:Ljava/lang/String;

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:J

.field private transient X:Z

.field private Y:Z

.field private Z:Z

.field public final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field public final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private c:J

.field private d:J

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:J

.field private t:Lorg/json/JSONObject;

.field private u:I

.field private v:Z

.field private w:I

.field private x:I

.field private y:J

.field private z:J


# direct methods
.method private constructor <init>()V
    .locals 3

    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 92
    iput v0, p0, Lcom/ss/android/b/a/b/b;->f:I

    .line 116
    iput-boolean v0, p0, Lcom/ss/android/b/a/b/b;->r:Z

    const/4 v1, 0x0

    .line 124
    iput-boolean v1, p0, Lcom/ss/android/b/a/b/b;->v:Z

    .line 126
    iput v1, p0, Lcom/ss/android/b/a/b/b;->w:I

    .line 128
    iput v1, p0, Lcom/ss/android/b/a/b/b;->x:I

    .line 142
    iput-boolean v1, p0, Lcom/ss/android/b/a/b/b;->E:Z

    .line 146
    iput-boolean v1, p0, Lcom/ss/android/b/a/b/b;->G:Z

    .line 148
    iput-boolean v0, p0, Lcom/ss/android/b/a/b/b;->H:Z

    .line 165
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/ss/android/b/a/b/b;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 167
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/ss/android/b/a/b/b;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 173
    iput v0, p0, Lcom/ss/android/b/a/b/b;->N:I

    .line 182
    iput-boolean v0, p0, Lcom/ss/android/b/a/b/b;->Q:Z

    const-wide/16 v0, -0x1

    .line 197
    iput-wide v0, p0, Lcom/ss/android/b/a/b/b;->W:J

    return-void
.end method

.method public constructor <init>(Lcom/ss/android/a/a/b/c;Lcom/ss/android/a/a/b/b;Lcom/ss/android/a/a/b/a;)V
    .locals 1

    const/4 v0, 0x0

    .line 209
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/ss/android/b/a/b/b;-><init>(Lcom/ss/android/a/a/b/c;Lcom/ss/android/a/a/b/b;Lcom/ss/android/a/a/b/a;I)V

    return-void
.end method

.method public constructor <init>(Lcom/ss/android/a/a/b/c;Lcom/ss/android/a/a/b/b;Lcom/ss/android/a/a/b/a;I)V
    .locals 3

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 92
    iput v0, p0, Lcom/ss/android/b/a/b/b;->f:I

    .line 116
    iput-boolean v0, p0, Lcom/ss/android/b/a/b/b;->r:Z

    const/4 v1, 0x0

    .line 124
    iput-boolean v1, p0, Lcom/ss/android/b/a/b/b;->v:Z

    .line 126
    iput v1, p0, Lcom/ss/android/b/a/b/b;->w:I

    .line 128
    iput v1, p0, Lcom/ss/android/b/a/b/b;->x:I

    .line 142
    iput-boolean v1, p0, Lcom/ss/android/b/a/b/b;->E:Z

    .line 146
    iput-boolean v1, p0, Lcom/ss/android/b/a/b/b;->G:Z

    .line 148
    iput-boolean v0, p0, Lcom/ss/android/b/a/b/b;->H:Z

    .line 165
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/ss/android/b/a/b/b;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 167
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/ss/android/b/a/b/b;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 173
    iput v0, p0, Lcom/ss/android/b/a/b/b;->N:I

    .line 182
    iput-boolean v0, p0, Lcom/ss/android/b/a/b/b;->Q:Z

    const-wide/16 v0, -0x1

    .line 197
    iput-wide v0, p0, Lcom/ss/android/b/a/b/b;->W:J

    .line 213
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/b/a/b/b;->c:J

    .line 214
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ss/android/b/a/b/b;->d:J

    .line 215
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->e:Ljava/lang/String;

    .line 216
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->g:Ljava/lang/String;

    .line 217
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->z()Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->t:Lorg/json/JSONObject;

    .line 218
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->t()Z

    move-result v0

    iput-boolean v0, p0, Lcom/ss/android/b/a/b/b;->r:Z

    .line 219
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->r()I

    move-result v0

    iput v0, p0, Lcom/ss/android/b/a/b/b;->n:I

    .line 220
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->o:Ljava/lang/String;

    .line 221
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->h:Ljava/lang/String;

    .line 222
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->x()Lcom/ss/android/a/a/c/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->x()Lcom/ss/android/a/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/a/a/c/b;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->i:Ljava/lang/String;

    .line 224
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->x()Lcom/ss/android/a/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ss/android/a/a/c/b;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->j:Ljava/lang/String;

    .line 226
    :cond_0
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->A()I

    move-result v0

    iput v0, p0, Lcom/ss/android/b/a/b/b;->k:I

    .line 227
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->p:Ljava/lang/String;

    .line 228
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->q:Ljava/lang/String;

    .line 229
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->F:Ljava/lang/String;

    .line 231
    invoke-interface {p2}, Lcom/ss/android/a/a/b/b;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->I:Ljava/lang/String;

    .line 232
    invoke-interface {p2}, Lcom/ss/android/a/a/b/b;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/b/b;->J:Ljava/lang/String;

    .line 233
    invoke-interface {p2}, Lcom/ss/android/a/a/b/b;->m()Z

    move-result p2

    iput-boolean p2, p0, Lcom/ss/android/b/a/b/b;->K:Z

    .line 235
    invoke-interface {p3}, Lcom/ss/android/a/a/b/a;->c()Z

    move-result p2

    iput-boolean p2, p0, Lcom/ss/android/b/a/b/b;->v:Z

    .line 236
    invoke-interface {p3}, Lcom/ss/android/a/a/b/a;->a()I

    move-result p2

    iput p2, p0, Lcom/ss/android/b/a/b/b;->l:I

    .line 237
    invoke-interface {p3}, Lcom/ss/android/a/a/b/a;->b()I

    move-result p2

    iput p2, p0, Lcom/ss/android/b/a/b/b;->m:I

    .line 238
    invoke-interface {p3}, Lcom/ss/android/a/a/b/a;->e()Z

    move-result p2

    iput-boolean p2, p0, Lcom/ss/android/b/a/b/b;->Q:Z

    .line 239
    invoke-interface {p3}, Lcom/ss/android/a/a/b/a;->f()Z

    move-result p2

    iput-boolean p2, p0, Lcom/ss/android/b/a/b/b;->R:Z

    .line 240
    invoke-interface {p3}, Lcom/ss/android/a/a/b/a;->g()Z

    move-result p2

    iput-boolean p2, p0, Lcom/ss/android/b/a/b/b;->H:Z

    .line 242
    iput p4, p0, Lcom/ss/android/b/a/b/b;->u:I

    .line 243
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    iput-wide p2, p0, Lcom/ss/android/b/a/b/b;->s:J

    .line 244
    iget-wide p2, p0, Lcom/ss/android/b/a/b/b;->s:J

    iput-wide p2, p0, Lcom/ss/android/b/a/b/b;->y:J

    .line 246
    invoke-interface {p1}, Lcom/ss/android/a/a/b/c;->E()Z

    move-result p1

    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->G:Z

    return-void
.end method

.method public static b(Lorg/json/JSONObject;)Lcom/ss/android/b/a/b/b;
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 774
    :cond_0
    new-instance v1, Lcom/ss/android/b/a/b/b;

    invoke-direct {v1}, Lcom/ss/android/b/a/b/b;-><init>()V

    :try_start_0
    const-string v2, "mId"

    .line 776
    invoke-static {p0, v2}, Lcom/ss/android/a/a/d/a;->a(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/ss/android/b/a/b/b;->d(J)V

    const-string v2, "mExtValue"

    .line 777
    invoke-static {p0, v2}, Lcom/ss/android/a/a/d/a;->a(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/ss/android/b/a/b/b;->e(J)V

    const-string v2, "mLogExtra"

    .line 778
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->c(Ljava/lang/String;)V

    const-string v2, "mDownloadStatus"

    .line 779
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->e(I)V

    const-string v2, "mPackageName"

    .line 780
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->b(Ljava/lang/String;)V

    const-string v2, "mIsAd"

    .line 781
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->a(Z)V

    const-string v2, "mTimeStamp"

    .line 782
    invoke-static {p0, v2}, Lcom/ss/android/a/a/d/a;->a(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/ss/android/b/a/b/b;->f(J)V

    const-string v2, "mVersionCode"

    .line 783
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->f(I)V

    const-string v2, "mVersionName"

    .line 784
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->d(Ljava/lang/String;)V

    const-string v2, "mDownloadId"

    .line 785
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->g(I)V

    const-string v2, "mIsV3Event"

    .line 786
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->b(Z)V

    const-string v2, "mScene"

    .line 787
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->h(I)V

    const-string v2, "mEventTag"

    .line 788
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->f(Ljava/lang/String;)V

    const-string v2, "mEventRefer"

    .line 789
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->g(Ljava/lang/String;)V

    const-string v2, "mDownloadUrl"

    .line 790
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->h(Ljava/lang/String;)V

    const-string v2, "mEnableBackDialog"

    .line 791
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->c(Z)V

    .line 792
    iget-object v2, v1, Lcom/ss/android/b/a/b/b;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v3, "hasSendInstallFinish"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 793
    iget-object v2, v1, Lcom/ss/android/b/a/b/b;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v3, "hasSendDownloadFailedFinally"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const-string v2, "mLastFailedErrCode"

    .line 794
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->d(I)V

    const-string v2, "mLastFailedErrMsg"

    .line 795
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->a(Ljava/lang/String;)V

    const-string v2, "mOpenUrl"

    .line 796
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->i(Ljava/lang/String;)V

    const-string v2, "mLinkMode"

    .line 797
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->j(I)V

    const-string v2, "mDownloadMode"

    .line 798
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->k(I)V

    const-string v2, "mModelType"

    .line 799
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->l(I)V

    const-string v2, "mAppName"

    .line 800
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->j(Ljava/lang/String;)V

    const-string v2, "mAppIcon"

    .line 801
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->k(Ljava/lang/String;)V

    const-string v2, "mDownloadFailedTimes"

    const/4 v3, 0x0

    .line 802
    invoke-virtual {p0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->a(I)V

    const-string v2, "mRecentDownloadResumeTime"

    .line 803
    invoke-static {p0, v2}, Lcom/ss/android/a/a/d/a;->a(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/ss/android/b/a/b/b;->a(J)V

    const-string v2, "mClickPauseTimes"

    .line 804
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->b(I)V

    const-string v2, "mJumpInstallTime"

    .line 805
    invoke-static {p0, v2}, Lcom/ss/android/a/a/d/a;->a(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/ss/android/b/a/b/b;->b(J)V

    const-string v2, "mCancelInstallTime"

    .line 806
    invoke-static {p0, v2}, Lcom/ss/android/a/a/d/a;->a(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/ss/android/b/a/b/b;->c(J)V

    const-string v2, "mLastFailedResumeCount"

    .line 807
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->c(I)V

    const-string v2, "downloadFinishReason"

    .line 808
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->l(Ljava/lang/String;)V

    const-string v2, "clickDownloadSize"

    .line 809
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/ss/android/b/a/b/b;->i(J)V

    const-string v2, "clickDownloadTime"

    .line 810
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/ss/android/b/a/b/b;->h(J)V

    const-string v2, "mIsUpdateDownload"

    .line 811
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->h(Z)V

    const-string v2, "mOriginMimeType"

    .line 812
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->m(Ljava/lang/String;)V

    const-string v2, "mIsPatchApplyHandled"

    .line 813
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->i(Z)V

    const-string v2, "installAfterCleanSpace"

    .line 814
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->f(Z)V

    const-string v2, "funnelType"

    const/4 v4, 0x1

    .line 815
    invoke-virtual {p0, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->i(I)V

    const-string v2, "webUrl"

    .line 816
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->e(Ljava/lang/String;)V

    const-string v2, "enableShowComplianceDialog"

    .line 817
    invoke-virtual {p0, v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->p(Z)V

    const-string v2, "isAutoDownloadOnCardShow"

    .line 818
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ss/android/b/a/b/b;->q(Z)V

    const-string v2, "enable_new_activity"

    .line 819
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-virtual {v1, v3}, Lcom/ss/android/b/a/b/b;->r(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 821
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    :try_start_1
    const-string v2, "mExtras"

    .line 824
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/ss/android/b/a/b/b;->a(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 826
    :catch_1
    invoke-virtual {v1, v0}, Lcom/ss/android/b/a/b/b;->a(Lorg/json/JSONObject;)V

    :goto_1
    return-object v1
.end method


# virtual methods
.method public A()J
    .locals 5

    .line 274
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->y:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 275
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->s:J

    return-wide v0

    .line 277
    :cond_0
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->y:J

    return-wide v0
.end method

.method public B()J
    .locals 2

    .line 285
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->z:J

    return-wide v0
.end method

.method public C()I
    .locals 1

    .line 301
    iget v0, p0, Lcom/ss/android/b/a/b/b;->B:I

    return v0
.end method

.method public D()I
    .locals 1

    .line 309
    iget v0, p0, Lcom/ss/android/b/a/b/b;->C:I

    return v0
.end method

.method public E()Ljava/lang/String;
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->D:Ljava/lang/String;

    return-object v0
.end method

.method public F()I
    .locals 1

    .line 342
    iget v0, p0, Lcom/ss/android/b/a/b/b;->f:I

    return v0
.end method

.method public G()J
    .locals 2

    .line 359
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->s:J

    return-wide v0
.end method

.method public H()I
    .locals 1

    .line 404
    iget v0, p0, Lcom/ss/android/b/a/b/b;->n:I

    return v0
.end method

.method public I()Ljava/lang/String;
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->o:Ljava/lang/String;

    return-object v0
.end method

.method public J()I
    .locals 1

    .line 458
    iget v0, p0, Lcom/ss/android/b/a/b/b;->M:I

    return v0
.end method

.method public K()Z
    .locals 1

    .line 479
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->v:Z

    return v0
.end method

.method public L()J
    .locals 2

    .line 520
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->W:J

    return-wide v0
.end method

.method public M()Ljava/lang/String;
    .locals 1

    .line 528
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->p:Ljava/lang/String;

    return-object v0
.end method

.method public N()I
    .locals 1

    .line 544
    iget v0, p0, Lcom/ss/android/b/a/b/b;->l:I

    return v0
.end method

.method public O()Ljava/lang/String;
    .locals 1

    .line 572
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->S:Ljava/lang/String;

    return-object v0
.end method

.method public P()Z
    .locals 1

    .line 580
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->U:Z

    return v0
.end method

.method public Q()Z
    .locals 1

    .line 588
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->L:Z

    return v0
.end method

.method public R()Z
    .locals 1

    .line 596
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->V:Z

    return v0
.end method

.method public S()J
    .locals 2

    .line 604
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->O:J

    return-wide v0
.end method

.method public T()J
    .locals 2

    .line 612
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->P:J

    return-wide v0
.end method

.method public U()Z
    .locals 1

    .line 620
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->E:Z

    return v0
.end method

.method public V()Ljava/lang/String;
    .locals 1

    .line 628
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->F:Ljava/lang/String;

    return-object v0
.end method

.method public W()Z
    .locals 1

    .line 636
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->G:Z

    return v0
.end method

.method public X()Z
    .locals 1

    .line 644
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->X:Z

    return v0
.end method

.method public Y()Z
    .locals 1

    .line 652
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->Y:Z

    return v0
.end method

.method public Z()Z
    .locals 1

    .line 660
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->aa:Z

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->h:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .line 254
    iput p1, p0, Lcom/ss/android/b/a/b/b;->w:I

    return-void
.end method

.method public a(J)V
    .locals 0

    .line 281
    iput-wide p1, p0, Lcom/ss/android/b/a/b/b;->y:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->D:Ljava/lang/String;

    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
    .locals 0

    .line 392
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->t:Lorg/json/JSONObject;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 383
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->r:Z

    return-void
.end method

.method public aa()Z
    .locals 1

    .line 668
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->Z:Z

    return v0
.end method

.method public ab()Z
    .locals 1

    .line 676
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->ab:Z

    return v0
.end method

.method public ac()Z
    .locals 1

    .line 684
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->ac:Z

    return v0
.end method

.method public ad()Lorg/json/JSONObject;
    .locals 7

    .line 717
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "mId"

    .line 719
    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "mExtValue"

    .line 720
    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "mLogExtra"

    .line 721
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mDownloadStatus"

    .line 722
    iget v2, p0, Lcom/ss/android/b/a/b/b;->f:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mPackageName"

    .line 723
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mIsAd"

    .line 724
    iget-boolean v2, p0, Lcom/ss/android/b/a/b/b;->r:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "mTimeStamp"

    .line 725
    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->s:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "mExtras"

    .line 726
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->t:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mVersionCode"

    .line 727
    iget v2, p0, Lcom/ss/android/b/a/b/b;->n:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mVersionName"

    .line 728
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mDownloadId"

    .line 729
    iget v2, p0, Lcom/ss/android/b/a/b/b;->u:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mIsV3Event"

    .line 730
    iget-boolean v2, p0, Lcom/ss/android/b/a/b/b;->K:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "mScene"

    .line 731
    iget v2, p0, Lcom/ss/android/b/a/b/b;->M:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mEventTag"

    .line 732
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mEventRefer"

    .line 733
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->J:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mDownloadUrl"

    .line 734
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mEnableBackDialog"

    .line 735
    iget-boolean v2, p0, Lcom/ss/android/b/a/b/b;->v:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "hasSendInstallFinish"

    .line 736
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "hasSendDownloadFailedFinally"

    .line 737
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "mLastFailedErrCode"

    .line 738
    iget v2, p0, Lcom/ss/android/b/a/b/b;->C:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mLastFailedErrMsg"

    .line 739
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->D:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mOpenUrl"

    .line 740
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mLinkMode"

    .line 741
    iget v2, p0, Lcom/ss/android/b/a/b/b;->l:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mDownloadMode"

    .line 742
    iget v2, p0, Lcom/ss/android/b/a/b/b;->m:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mModelType"

    .line 743
    iget v2, p0, Lcom/ss/android/b/a/b/b;->k:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mAppName"

    .line 744
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mAppIcon"

    .line 745
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mDownloadFailedTimes"

    .line 746
    iget v2, p0, Lcom/ss/android/b/a/b/b;->w:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mRecentDownloadResumeTime"

    .line 747
    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->y:J

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->s:J

    goto :goto_0

    :cond_0
    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->y:J

    :goto_0
    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "mClickPauseTimes"

    .line 748
    iget v2, p0, Lcom/ss/android/b/a/b/b;->x:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mJumpInstallTime"

    .line 749
    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->z:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "mCancelInstallTime"

    .line 750
    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->A:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "mLastFailedResumeCount"

    .line 751
    iget v2, p0, Lcom/ss/android/b/a/b/b;->B:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "mIsUpdateDownload"

    .line 752
    iget-boolean v2, p0, Lcom/ss/android/b/a/b/b;->E:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "mOriginMimeType"

    .line 753
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mIsPatchApplyHandled"

    .line 754
    iget-boolean v2, p0, Lcom/ss/android/b/a/b/b;->G:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "downloadFinishReason"

    .line 755
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->S:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "clickDownloadTime"

    .line 756
    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->O:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "clickDownloadSize"

    .line 757
    iget-wide v2, p0, Lcom/ss/android/b/a/b/b;->P:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "installAfterCleanSpace"

    .line 758
    iget-boolean v2, p0, Lcom/ss/android/b/a/b/b;->L:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "funnelType"

    .line 759
    iget v2, p0, Lcom/ss/android/b/a/b/b;->N:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v1, "webUrl"

    .line 760
    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "enableShowComplianceDialog"

    .line 761
    iget-boolean v2, p0, Lcom/ss/android/b/a/b/b;->Q:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "isAutoDownloadOnCardShow"

    .line 762
    iget-boolean v2, p0, Lcom/ss/android/b/a/b/b;->R:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "enable_new_activity"

    .line 763
    iget-boolean v2, p0, Lcom/ss/android/b/a/b/b;->H:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 765
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-object v0
.end method

.method public ae()Lcom/ss/android/b/a/a/c;
    .locals 5

    .line 854
    new-instance v0, Lcom/ss/android/b/a/a/c$a;

    invoke-direct {v0}, Lcom/ss/android/b/a/a/c$a;-><init>()V

    iget-wide v1, p0, Lcom/ss/android/b/a/b/b;->c:J

    .line 855
    invoke-virtual {v0, v1, v2}, Lcom/ss/android/b/a/a/c$a;->a(J)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-wide v1, p0, Lcom/ss/android/b/a/b/b;->d:J

    .line 856
    invoke-virtual {v0, v1, v2}, Lcom/ss/android/b/a/a/c$a;->b(J)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->e:Ljava/lang/String;

    .line 857
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->a(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->g:Ljava/lang/String;

    .line 858
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->b(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->t:Lorg/json/JSONObject;

    .line 859
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->a(Lorg/json/JSONObject;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/ss/android/b/a/b/b;->r:Z

    .line 860
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->a(Z)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget v1, p0, Lcom/ss/android/b/a/b/b;->n:I

    .line 861
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->b(I)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->o:Ljava/lang/String;

    .line 862
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->g(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->h:Ljava/lang/String;

    .line 863
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->d(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget v1, p0, Lcom/ss/android/b/a/b/b;->k:I

    .line 864
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->a(I)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->F:Ljava/lang/String;

    .line 865
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->f(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->p:Ljava/lang/String;

    .line 866
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->e(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->q:Ljava/lang/String;

    .line 867
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->c(Ljava/lang/String;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    new-instance v1, Lcom/ss/android/a/a/c/b;

    iget-object v2, p0, Lcom/ss/android/b/a/b/b;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/ss/android/b/a/b/b;->j:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/ss/android/a/a/c/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/c$a;->a(Lcom/ss/android/a/a/c/b;)Lcom/ss/android/b/a/a/c$a;

    move-result-object v0

    .line 869
    invoke-virtual {v0}, Lcom/ss/android/b/a/a/c$a;->a()Lcom/ss/android/b/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public af()Lcom/ss/android/b/a/a/b;
    .locals 2

    .line 873
    new-instance v0, Lcom/ss/android/b/a/a/b$a;

    invoke-direct {v0}, Lcom/ss/android/b/a/a/b$a;-><init>()V

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->I:Ljava/lang/String;

    .line 874
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/b$a;->a(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object v0

    iget-object v1, p0, Lcom/ss/android/b/a/b/b;->J:Ljava/lang/String;

    .line 875
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/b$a;->i(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/ss/android/b/a/b/b;->K:Z

    .line 876
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/b$a;->c(Z)Lcom/ss/android/b/a/a/b$a;

    move-result-object v0

    .line 877
    invoke-virtual {v0}, Lcom/ss/android/b/a/a/b$a;->a()Lcom/ss/android/b/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method public ag()Lcom/ss/android/b/a/a/a;
    .locals 2

    .line 881
    new-instance v0, Lcom/ss/android/b/a/a/a$a;

    invoke-direct {v0}, Lcom/ss/android/b/a/a/a$a;-><init>()V

    iget-boolean v1, p0, Lcom/ss/android/b/a/b/b;->v:Z

    .line 882
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/a$a;->a(Z)Lcom/ss/android/b/a/a/a$a;

    move-result-object v0

    iget v1, p0, Lcom/ss/android/b/a/b/b;->l:I

    .line 883
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/a$a;->a(I)Lcom/ss/android/b/a/a/a$a;

    move-result-object v0

    iget v1, p0, Lcom/ss/android/b/a/b/b;->m:I

    .line 884
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/a$a;->b(I)Lcom/ss/android/b/a/a/a$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/ss/android/b/a/b/b;->Q:Z

    .line 885
    invoke-virtual {v0, v1}, Lcom/ss/android/b/a/a/a$a;->e(Z)Lcom/ss/android/b/a/a/a$a;

    move-result-object v0

    .line 886
    invoke-virtual {v0}, Lcom/ss/android/b/a/a/a$a;->a()Lcom/ss/android/b/a/a/a;

    move-result-object v0

    return-object v0
.end method

.method public b()J
    .locals 2

    .line 326
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->c:J

    return-wide v0
.end method

.method public b(I)V
    .locals 0

    .line 266
    iput p1, p0, Lcom/ss/android/b/a/b/b;->x:I

    return-void
.end method

.method public b(J)V
    .locals 0

    .line 289
    iput-wide p1, p0, Lcom/ss/android/b/a/b/b;->z:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .line 355
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->g:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 0

    .line 421
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->K:Z

    return-void
.end method

.method public c(I)V
    .locals 0

    .line 305
    iput p1, p0, Lcom/ss/android/b/a/b/b;->B:I

    return-void
.end method

.method public c(J)V
    .locals 0

    .line 297
    iput-wide p1, p0, Lcom/ss/android/b/a/b/b;->A:J

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .line 374
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->e:Ljava/lang/String;

    return-void
.end method

.method public c(Z)V
    .locals 0

    .line 475
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->v:Z

    return-void
.end method

.method public c()Z
    .locals 1

    .line 379
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->r:Z

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d(I)V
    .locals 0

    .line 313
    iput p1, p0, Lcom/ss/android/b/a/b/b;->C:I

    return-void
.end method

.method public d(J)V
    .locals 0

    .line 330
    iput-wide p1, p0, Lcom/ss/android/b/a/b/b;->c:J

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .line 400
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->o:Ljava/lang/String;

    return-void
.end method

.method public d(Z)V
    .locals 0

    .line 568
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->T:Z

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 351
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->g:Ljava/lang/String;

    return-object v0
.end method

.method public e(I)V
    .locals 0

    .line 346
    iput p1, p0, Lcom/ss/android/b/a/b/b;->f:I

    return-void
.end method

.method public e(J)V
    .locals 0

    .line 338
    iput-wide p1, p0, Lcom/ss/android/b/a/b/b;->d:J

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    .line 466
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->j:Ljava/lang/String;

    return-void
.end method

.method public e(Z)V
    .locals 0

    .line 584
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->U:Z

    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 512
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->i:Ljava/lang/String;

    return-object v0
.end method

.method public f(I)V
    .locals 0

    .line 396
    iput p1, p0, Lcom/ss/android/b/a/b/b;->n:I

    return-void
.end method

.method public f(J)V
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    .line 364
    iput-wide p1, p0, Lcom/ss/android/b/a/b/b;->s:J

    :cond_0
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    .line 483
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->I:Ljava/lang/String;

    return-void
.end method

.method public f(Z)V
    .locals 0

    .line 592
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->L:Z

    return-void
.end method

.method public g()Lorg/json/JSONObject;
    .locals 1

    .line 388
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->t:Lorg/json/JSONObject;

    return-object v0
.end method

.method public g(I)V
    .locals 0

    .line 412
    iput p1, p0, Lcom/ss/android/b/a/b/b;->u:I

    return-void
.end method

.method public g(J)V
    .locals 0

    .line 516
    iput-wide p1, p0, Lcom/ss/android/b/a/b/b;->W:J

    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .line 491
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->J:Ljava/lang/String;

    return-void
.end method

.method public g(Z)V
    .locals 0

    .line 600
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->V:Z

    return-void
.end method

.method public h()I
    .locals 1

    .line 471
    iget v0, p0, Lcom/ss/android/b/a/b/b;->N:I

    return v0
.end method

.method public h(I)V
    .locals 0

    .line 454
    iput p1, p0, Lcom/ss/android/b/a/b/b;->M:I

    return-void
.end method

.method public h(J)V
    .locals 0

    .line 608
    iput-wide p1, p0, Lcom/ss/android/b/a/b/b;->O:J

    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    .line 499
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->h:Ljava/lang/String;

    return-void
.end method

.method public h(Z)V
    .locals 0

    .line 624
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->E:Z

    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 495
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->J:Ljava/lang/String;

    return-object v0
.end method

.method public i(I)V
    .locals 0

    .line 462
    iput p1, p0, Lcom/ss/android/b/a/b/b;->N:I

    return-void
.end method

.method public i(J)V
    .locals 0

    .line 616
    iput-wide p1, p0, Lcom/ss/android/b/a/b/b;->P:J

    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    .line 507
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->i:Ljava/lang/String;

    return-void
.end method

.method public i(Z)V
    .locals 0

    .line 640
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->G:Z

    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .line 487
    iget-object v0, p0, Lcom/ss/android/b/a/b/b;->I:Ljava/lang/String;

    return-object v0
.end method

.method public j(I)V
    .locals 0

    .line 540
    iput p1, p0, Lcom/ss/android/b/a/b/b;->l:I

    return-void
.end method

.method public j(Ljava/lang/String;)V
    .locals 0

    .line 524
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->p:Ljava/lang/String;

    return-void
.end method

.method public j(Z)V
    .locals 0

    .line 648
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->X:Z

    return-void
.end method

.method public k()Lorg/json/JSONObject;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public k(I)V
    .locals 0

    .line 548
    iput p1, p0, Lcom/ss/android/b/a/b/b;->m:I

    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 0

    .line 532
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->q:Ljava/lang/String;

    return-void
.end method

.method public k(Z)V
    .locals 0

    .line 656
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->Y:Z

    return-void
.end method

.method public l()J
    .locals 2

    .line 334
    iget-wide v0, p0, Lcom/ss/android/b/a/b/b;->d:J

    return-wide v0
.end method

.method public l(I)V
    .locals 0

    .line 556
    iput p1, p0, Lcom/ss/android/b/a/b/b;->k:I

    return-void
.end method

.method public l(Ljava/lang/String;)V
    .locals 0

    .line 576
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->S:Ljava/lang/String;

    return-void
.end method

.method public l(Z)V
    .locals 0

    .line 664
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->aa:Z

    return-void
.end method

.method public m(Ljava/lang/String;)V
    .locals 0

    .line 632
    iput-object p1, p0, Lcom/ss/android/b/a/b/b;->F:Ljava/lang/String;

    return-void
.end method

.method public m(Z)V
    .locals 0

    .line 672
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->Z:Z

    return-void
.end method

.method public m()Z
    .locals 1

    .line 425
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->K:Z

    return v0
.end method

.method public n()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public n(Z)V
    .locals 0

    .line 680
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->ab:Z

    return-void
.end method

.method public o()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public o(Z)V
    .locals 0

    .line 688
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->ac:Z

    return-void
.end method

.method public p()Lorg/json/JSONObject;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public p(Z)V
    .locals 0

    .line 696
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->Q:Z

    return-void
.end method

.method public q(Z)V
    .locals 0

    .line 704
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->R:Z

    return-void
.end method

.method public q()Z
    .locals 1

    .line 709
    iget-boolean v0, p0, Lcom/ss/android/b/a/b/b;->H:Z

    return v0
.end method

.method public r()Lorg/json/JSONObject;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public r(Z)V
    .locals 0

    .line 713
    iput-boolean p1, p0, Lcom/ss/android/b/a/b/b;->H:Z

    return-void
.end method

.method public s()I
    .locals 1

    .line 417
    iget v0, p0, Lcom/ss/android/b/a/b/b;->u:I

    return v0
.end method

.method public t()Lcom/ss/android/a/a/b/c;
    .locals 1

    .line 840
    invoke-virtual {p0}, Lcom/ss/android/b/a/b/b;->ae()Lcom/ss/android/b/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method public u()Lcom/ss/android/a/a/b/b;
    .locals 1

    .line 845
    invoke-virtual {p0}, Lcom/ss/android/b/a/b/b;->af()Lcom/ss/android/b/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method public v()Lcom/ss/android/a/a/b/a;
    .locals 1

    .line 850
    invoke-virtual {p0}, Lcom/ss/android/b/a/b/b;->ag()Lcom/ss/android/b/a/a/a;

    move-result-object v0

    return-object v0
.end method

.method public w()I
    .locals 1

    .line 250
    iget v0, p0, Lcom/ss/android/b/a/b/b;->w:I

    return v0
.end method

.method public declared-synchronized x()V
    .locals 1

    monitor-enter p0

    .line 258
    :try_start_0
    iget v0, p0, Lcom/ss/android/b/a/b/b;->w:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ss/android/b/a/b/b;->w:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 257
    monitor-exit p0

    throw v0
.end method

.method public y()I
    .locals 1

    .line 262
    iget v0, p0, Lcom/ss/android/b/a/b/b;->x:I

    return v0
.end method

.method public declared-synchronized z()V
    .locals 1

    monitor-enter p0

    .line 270
    :try_start_0
    iget v0, p0, Lcom/ss/android/b/a/b/b;->x:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ss/android/b/a/b/b;->x:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    .line 269
    monitor-exit p0

    throw v0
.end method
