.class public Lcom/ss/android/b/a/a/b;
.super Ljava/lang/Object;
.source "AdDownloadEventConfig.java"

# interfaces
.implements Lcom/ss/android/a/a/b/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ss/android/b/a/a/b$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field private transient m:Ljava/lang/Object;

.field private n:Lorg/json/JSONObject;

.field private o:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/ss/android/b/a/a/b$a;)V
    .locals 1

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->a(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->a:Ljava/lang/String;

    .line 81
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->b(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->b:Ljava/lang/String;

    .line 82
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->c(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->c:Ljava/lang/String;

    .line 83
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->d(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->d:Ljava/lang/String;

    .line 84
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->e(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->e:Ljava/lang/String;

    .line 85
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->f(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->f:Ljava/lang/String;

    .line 86
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->g(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->g:Ljava/lang/String;

    .line 87
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->h(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->h:Ljava/lang/String;

    .line 88
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->i(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->m:Ljava/lang/Object;

    .line 89
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->j(Lcom/ss/android/b/a/a/b$a;)I

    move-result v0

    iput v0, p0, Lcom/ss/android/b/a/a/b;->i:I

    .line 90
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->k(Lcom/ss/android/b/a/a/b$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ss/android/b/a/a/b;->j:Z

    .line 91
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->l(Lcom/ss/android/b/a/a/b$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ss/android/b/a/a/b;->k:Z

    .line 92
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->m(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->l:Ljava/lang/String;

    .line 93
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->n(Lcom/ss/android/b/a/a/b$a;)Lorg/json/JSONObject;

    move-result-object v0

    iput-object v0, p0, Lcom/ss/android/b/a/a/b;->n:Lorg/json/JSONObject;

    .line 94
    invoke-static {p1}, Lcom/ss/android/b/a/a/b$a;->o(Lcom/ss/android/b/a/a/b$a;)Lorg/json/JSONObject;

    move-result-object p1

    iput-object p1, p0, Lcom/ss/android/b/a/a/b;->o:Lorg/json/JSONObject;

    return-void
.end method

.method synthetic constructor <init>(Lcom/ss/android/b/a/a/b$a;Lcom/ss/android/b/a/a/b$1;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/ss/android/b/a/a/b;-><init>(Lcom/ss/android/b/a/a/b$a;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->l:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .line 215
    iput p1, p0, Lcom/ss/android/b/a/a/b;->i:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/ss/android/b/a/a/b;->l:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->g:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->h:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/Object;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->m:Ljava/lang/Object;

    return-object v0
.end method

.method public k()I
    .locals 1

    .line 170
    iget v0, p0, Lcom/ss/android/b/a/a/b;->i:I

    return v0
.end method

.method public l()Z
    .locals 1

    .line 175
    iget-boolean v0, p0, Lcom/ss/android/b/a/a/b;->j:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .line 180
    iget-boolean v0, p0, Lcom/ss/android/b/a/a/b;->k:Z

    return v0
.end method

.method public n()Lorg/json/JSONObject;
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->n:Lorg/json/JSONObject;

    return-object v0
.end method

.method public o()Lorg/json/JSONObject;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/ss/android/b/a/a/b;->o:Lorg/json/JSONObject;

    return-object v0
.end method
