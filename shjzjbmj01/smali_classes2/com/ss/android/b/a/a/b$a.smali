.class public final Lcom/ss/android/b/a/a/b$a;
.super Ljava/lang/Object;
.source "AdDownloadEventConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ss/android/b/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/Object;

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Lorg/json/JSONObject;

.field private o:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 294
    iput-boolean v0, p0, Lcom/ss/android/b/a/a/b$a;->k:Z

    const/4 v0, 0x0

    .line 295
    iput-boolean v0, p0, Lcom/ss/android/b/a/a/b$a;->l:Z

    return-void
.end method

.method static synthetic a(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->d:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic e(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic f(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->f:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic g(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->g:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic h(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->h:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic i(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/Object;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->i:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic j(Lcom/ss/android/b/a/a/b$a;)I
    .locals 0

    .line 282
    iget p0, p0, Lcom/ss/android/b/a/a/b$a;->j:I

    return p0
.end method

.method static synthetic k(Lcom/ss/android/b/a/a/b$a;)Z
    .locals 0

    .line 282
    iget-boolean p0, p0, Lcom/ss/android/b/a/a/b$a;->k:Z

    return p0
.end method

.method static synthetic l(Lcom/ss/android/b/a/a/b$a;)Z
    .locals 0

    .line 282
    iget-boolean p0, p0, Lcom/ss/android/b/a/a/b$a;->l:Z

    return p0
.end method

.method static synthetic m(Lcom/ss/android/b/a/a/b$a;)Ljava/lang/String;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->m:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic n(Lcom/ss/android/b/a/a/b$a;)Lorg/json/JSONObject;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->n:Lorg/json/JSONObject;

    return-object p0
.end method

.method static synthetic o(Lcom/ss/android/b/a/a/b$a;)Lorg/json/JSONObject;
    .locals 0

    .line 282
    iget-object p0, p0, Lcom/ss/android/b/a/a/b$a;->o:Lorg/json/JSONObject;

    return-object p0
.end method


# virtual methods
.method public a(I)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 410
    iput p1, p0, Lcom/ss/android/b/a/a/b$a;->j:I

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/ss/android/b/a/a/b$a;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 415
    iput-boolean p1, p0, Lcom/ss/android/b/a/a/b$a;->k:Z

    return-object p0
.end method

.method public a()Lcom/ss/android/b/a/a/b;
    .locals 2

    .line 455
    new-instance v0, Lcom/ss/android/b/a/a/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/ss/android/b/a/a/b;-><init>(Lcom/ss/android/b/a/a/b$a;Lcom/ss/android/b/a/a/b$1;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/ss/android/b/a/a/b$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/ss/android/b/a/a/b$a;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/ss/android/b/a/a/b$a;->d:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 430
    iput-boolean p1, p0, Lcom/ss/android/b/a/a/b$a;->l:Z

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/ss/android/b/a/a/b$a;->e:Ljava/lang/String;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 375
    iput-object p1, p0, Lcom/ss/android/b/a/a/b$a;->f:Ljava/lang/String;

    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/ss/android/b/a/a/b$a;->g:Ljava/lang/String;

    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 395
    iput-object p1, p0, Lcom/ss/android/b/a/a/b$a;->h:Ljava/lang/String;

    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/ss/android/b/a/a/b$a;
    .locals 0

    .line 440
    iput-object p1, p0, Lcom/ss/android/b/a/a/b$a;->m:Ljava/lang/String;

    return-object p0
.end method
