.class public final Lcom/ss/android/b/a/a/a$a;
.super Ljava/lang/Object;
.source "AdDownloadController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ss/android/b/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:Z

.field private e:Ljava/lang/Object;

.field private f:Z

.field private g:I

.field private h:Lorg/json/JSONObject;

.field private i:Ljava/lang/Object;

.field private j:Z

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 238
    iput-boolean v0, p0, Lcom/ss/android/b/a/a/a$a;->j:Z

    .line 241
    iput-boolean v0, p0, Lcom/ss/android/b/a/a/a$a;->l:Z

    return-void
.end method

.method static synthetic a(Lcom/ss/android/b/a/a/a$a;)I
    .locals 0

    .line 228
    iget p0, p0, Lcom/ss/android/b/a/a/a$a;->a:I

    return p0
.end method

.method static synthetic b(Lcom/ss/android/b/a/a/a$a;)I
    .locals 0

    .line 228
    iget p0, p0, Lcom/ss/android/b/a/a/a$a;->b:I

    return p0
.end method

.method static synthetic c(Lcom/ss/android/b/a/a/a$a;)Z
    .locals 0

    .line 228
    iget-boolean p0, p0, Lcom/ss/android/b/a/a/a$a;->c:Z

    return p0
.end method

.method static synthetic d(Lcom/ss/android/b/a/a/a$a;)Z
    .locals 0

    .line 228
    iget-boolean p0, p0, Lcom/ss/android/b/a/a/a$a;->d:Z

    return p0
.end method

.method static synthetic e(Lcom/ss/android/b/a/a/a$a;)Ljava/lang/Object;
    .locals 0

    .line 228
    iget-object p0, p0, Lcom/ss/android/b/a/a/a$a;->e:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic f(Lcom/ss/android/b/a/a/a$a;)Z
    .locals 0

    .line 228
    iget-boolean p0, p0, Lcom/ss/android/b/a/a/a$a;->f:Z

    return p0
.end method

.method static synthetic g(Lcom/ss/android/b/a/a/a$a;)I
    .locals 0

    .line 228
    iget p0, p0, Lcom/ss/android/b/a/a/a$a;->g:I

    return p0
.end method

.method static synthetic h(Lcom/ss/android/b/a/a/a$a;)Lorg/json/JSONObject;
    .locals 0

    .line 228
    iget-object p0, p0, Lcom/ss/android/b/a/a/a$a;->h:Lorg/json/JSONObject;

    return-object p0
.end method

.method static synthetic i(Lcom/ss/android/b/a/a/a$a;)Ljava/lang/Object;
    .locals 0

    .line 228
    iget-object p0, p0, Lcom/ss/android/b/a/a/a$a;->i:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic j(Lcom/ss/android/b/a/a/a$a;)Z
    .locals 0

    .line 228
    iget-boolean p0, p0, Lcom/ss/android/b/a/a/a$a;->j:Z

    return p0
.end method

.method static synthetic k(Lcom/ss/android/b/a/a/a$a;)Z
    .locals 0

    .line 228
    iget-boolean p0, p0, Lcom/ss/android/b/a/a/a$a;->k:Z

    return p0
.end method

.method static synthetic l(Lcom/ss/android/b/a/a/a$a;)Z
    .locals 0

    .line 228
    iget-boolean p0, p0, Lcom/ss/android/b/a/a/a$a;->l:Z

    return p0
.end method


# virtual methods
.method public a(I)Lcom/ss/android/b/a/a/a$a;
    .locals 0

    .line 248
    iput p1, p0, Lcom/ss/android/b/a/a/a$a;->a:I

    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lcom/ss/android/b/a/a/a$a;
    .locals 0

    .line 278
    iput-object p1, p0, Lcom/ss/android/b/a/a/a$a;->e:Ljava/lang/Object;

    return-object p0
.end method

.method public a(Z)Lcom/ss/android/b/a/a/a$a;
    .locals 0

    .line 258
    iput-boolean p1, p0, Lcom/ss/android/b/a/a/a$a;->c:Z

    return-object p0
.end method

.method public a()Lcom/ss/android/b/a/a/a;
    .locals 2

    .line 327
    new-instance v0, Lcom/ss/android/b/a/a/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/ss/android/b/a/a/a;-><init>(Lcom/ss/android/b/a/a/a$a;Lcom/ss/android/b/a/a/a$1;)V

    return-object v0
.end method

.method public b(I)Lcom/ss/android/b/a/a/a$a;
    .locals 0

    .line 253
    iput p1, p0, Lcom/ss/android/b/a/a/a$a;->b:I

    return-object p0
.end method

.method public b(Z)Lcom/ss/android/b/a/a/a$a;
    .locals 0

    .line 263
    iput-boolean p1, p0, Lcom/ss/android/b/a/a/a$a;->d:Z

    return-object p0
.end method

.method public c(Z)Lcom/ss/android/b/a/a/a$a;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-object p0
.end method

.method public d(Z)Lcom/ss/android/b/a/a/a$a;
    .locals 0

    .line 283
    iput-boolean p1, p0, Lcom/ss/android/b/a/a/a$a;->f:Z

    return-object p0
.end method

.method public e(Z)Lcom/ss/android/b/a/a/a$a;
    .locals 0

    .line 309
    iput-boolean p1, p0, Lcom/ss/android/b/a/a/a$a;->j:Z

    return-object p0
.end method
