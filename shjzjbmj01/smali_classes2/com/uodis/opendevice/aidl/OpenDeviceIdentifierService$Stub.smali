.class public abstract Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub;
.super Landroid/os/Binder;
.source "OpenDeviceIdentifierService.java"

# interfaces
.implements Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.uodis.opendevice.aidl.OpenDeviceIdentifierService"

.field static final TRANSACTION_getBoos:I = 0x2

.field static final TRANSACTION_getIDs:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 31
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.uodis.opendevice.aidl.OpenDeviceIdentifierService"

    .line 32
    invoke-virtual {p0, p0, v0}, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string v0, "com.uodis.opendevice.aidl.OpenDeviceIdentifierService"

    .line 43
    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 44
    instance-of v1, v0, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;

    if-eqz v1, :cond_1

    .line 45
    check-cast v0, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;

    return-object v0

    .line 47
    :cond_1
    new-instance v0, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v0
.end method

.method public static getDefaultImpl()Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;
    .locals 1

    .line 158
    sget-object v0, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub$Proxy;->sDefaultImpl:Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;

    return-object v0
.end method

.method public static setDefaultImpl(Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;)Z
    .locals 1

    .line 148
    sget-object v0, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub$Proxy;->sDefaultImpl:Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    .line 152
    sput-object p0, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub$Proxy;->sDefaultImpl:Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService;

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0

    .line 149
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "setDefaultImpl() called twice"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "com.uodis.opendevice.aidl.OpenDeviceIdentifierService"

    const v1, 0x5f4e5446

    const/4 v2, 0x1

    if-eq p1, v1, :cond_0

    packed-switch p1, :pswitch_data_0

    .line 81
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result p1

    return p1

    .line 73
    :pswitch_0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub;->getBoos()Z

    move-result p1

    .line 75
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeInt(I)V

    return v2

    .line 65
    :pswitch_1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/uodis/opendevice/aidl/OpenDeviceIdentifierService$Stub;->getIDs()Ljava/lang/String;

    move-result-object p1

    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    invoke-virtual {p3, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v2

    .line 60
    :cond_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
