.class public Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;
.super Ljava/lang/Object;


# static fields
.field private static volatile c:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;

.field private static d:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/a;

.field private static e:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/a;

.field private static f:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/a;

.field private static g:Ljava/lang/Object;

.field private static h:Landroid/os/HandlerThread;

.field private static i:Landroid/os/Handler;

.field private static j:Ljava/lang/String;

.field private static k:Ljava/lang/String;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->g:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->h:Landroid/os/HandlerThread;

    sput-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->i:Landroid/os/Handler;

    sput-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->j:Ljava/lang/String;

    sput-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->k:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->b:Z

    iput-object p1, p0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->a:Landroid/content/Context;

    const-string v0, "1"

    const-string v1, "persist.sys.identifierid.supported"

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->b:Z

    iget-boolean v0, p0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "vivo_work"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/c;

    sget-object v1, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->h:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/c;-><init>(Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;Landroid/os/Looper;Landroid/content/Context;)V

    sput-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->i:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;
    .locals 2

    sget-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->c:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;

    if-nez v0, :cond_1

    const-class v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->c:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;

    if-nez v1, :cond_0

    new-instance v1, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;

    invoke-direct {v1, p0}, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->c:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->c:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;

    return-object p0
.end method

.method static synthetic a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->b(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    sput-object p0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->j:Ljava/lang/String;

    return-object p0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    :try_start_0
    const-string v0, "android.os.SystemProperties"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "get"

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-class v4, Ljava/lang/String;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v5

    const-string p0, "unknown"

    aput-object p0, v2, v6

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    return-object p1
.end method

.method private static b(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v2, v0

    goto :goto_2

    :pswitch_0
    :try_start_0
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v1, "content://com.vivo.vms.IdProvider/IdentifierId/AAID_"

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :pswitch_1
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v1, "content://com.vivo.vms.IdProvider/IdentifierId/VAID_"

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_1

    :pswitch_2
    const-string p1, "content://com.vivo.vms.IdProvider/IdentifierId/OAID"

    goto :goto_0

    :goto_1
    move-object v2, p1

    goto :goto_2

    :catchall_0
    move-exception p0

    goto :goto_6

    :catch_0
    move-exception p0

    move-object p1, v0

    goto :goto_4

    :goto_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p0, :cond_0

    :try_start_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "value"

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, p1

    goto :goto_3

    :catchall_1
    move-exception p1

    move-object v0, p0

    move-object p0, p1

    goto :goto_6

    :catch_1
    move-exception p1

    move-object v7, p1

    move-object p1, p0

    move-object p0, v7

    goto :goto_4

    :cond_0
    :goto_3
    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    goto :goto_5

    :goto_4
    :try_start_2
    invoke-static {p0}, Lcom/yxcorp/kuaishou/addfp/android/b/b;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_5
    return-object v0

    :catchall_2
    move-exception p0

    move-object v0, p1

    :goto_6
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic c()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->g:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 7

    :try_start_0
    sget-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->g:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    sget-object v1, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->i:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0x29a

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "type"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v3, 0x1

    if-eq p1, v3, :cond_0

    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    :cond_0
    const-string v3, "appid"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    sget-object p2, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->i:Landroid/os/Handler;

    invoke-virtual {p2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/16 v3, 0x7d0

    :try_start_2
    sget-object p2, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->g:Ljava/lang/Object;

    invoke-virtual {p2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    const/4 p2, 0x0

    sub-long/2addr v5, v1

    cmp-long p2, v5, v3

    if-gez p2, :cond_2

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    sget-object p1, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->j:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :pswitch_1
    sget-object p1, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->j:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    :goto_0
    sput-object p2, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->j:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    sget-object p1, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->j:Ljava/lang/String;

    sput-object p1, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->k:Ljava/lang/String;

    goto :goto_0

    :cond_2
    :goto_1
    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/yxcorp/kuaishou/addfp/android/b/b;->a(Ljava/lang/Throwable;)V

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->b:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    iget-boolean v0, p0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->b:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    sget-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->k:Ljava/lang/String;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->a(ILjava/lang/String;)V

    sget-object v2, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->d:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/a;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->a:Landroid/content/Context;

    new-instance v3, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/a;

    sget-object v4, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->c:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;

    invoke-direct {v3, v4, v0, v1}, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/a;-><init>(Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;ILjava/lang/String;)V

    sput-object v3, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->d:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/a;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.vivo.vms.IdProvider/IdentifierId/OAID"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    sget-object v3, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->d:Lcom/yxcorp/kuaishou/addfp/a/b/h/a/a;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_2
    sget-object v0, Lcom/yxcorp/kuaishou/addfp/a/b/h/a/b;->k:Ljava/lang/String;

    return-object v0
.end method
